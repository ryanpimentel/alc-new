
CREATE TABLE [dbo].[ServiceCompletion](
	[service_completion_id] [int] IDENTITY(1,1) NOT NULL,
	[activity_schedule_id] [int] NULL,
	[service_date] [date] NULL,
	[actual_completion_date] [datetime] NULL,
	[completed_by] [nvarchar](50) NULL,
	[status] [int] NULL,
	[comment] [text] NULL,
	[services] [nvarchar](50) NULL,
 CONSTRAINT [PK_service_completion] PRIMARY KEY CLUSTERED 
(
	[service_completion_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


