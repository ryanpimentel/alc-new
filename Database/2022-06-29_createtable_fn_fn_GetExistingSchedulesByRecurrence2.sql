USE [AssistedLiving]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetExistingSchedulesByRecurrence2]    Script Date: 06/30/2022 10:18:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create FUNCTION [dbo].[fn_GetExistingSchedulesByRecurrence2] 
(
	@ctime datetime = null,
	@activityid int = null,
	@activityscheduleid int = null,
	@residentid int = null,
	@roomid int = null,
	@recday char(1) = null,
	@clientdt datetime = null,
	@isonetime bit = 0
)
RETURNS
	@tbl TABLE (service_time time(7), resident_id int, room_id int, duration int, recurrence varchar(20))
AS
BEGIN
	declare @ttime time(7) = null
	declare @otime time(7) = null
	declare @duration int = 0
	declare @deptid int = 0

	select @duration = duration, @deptid = dept_id from Activity where activity_id = @activityid
	set @otime = cast(@ctime as time(7))
	set @ttime = cast((DATEADD(MINUTE,@duration,@ctime)) as time(7))
			
	;with cte as (
		select 	service_time = cast(a.start_time as time(7)), 
				a.resident_id,
				a.room_id,
				f.duration,
				a.recurrence,
				[user_id] = a.carestaff_id
		from	ActivitySchedule a
		left join	Activity f on a.activity_id = f.activity_id
		where   (@residentid is null or a.resident_id = @residentid) and
				(@roomid is null or a.room_id = @roomid) and 
				(@activityscheduleid is null or a.activity_schedule_id <> @activityscheduleid)

	), cte_matches as (
		select 	service_time, resident_id, room_id, duration, recurrence
		from	cte 
		where	1 = (
					case
						when ([dbo].[fn_IsAvailableTimeSlot](@deptid, @ctime, @duration, @recday, [user_id], @clientdt, @isonetime) > 0) then 1
						when (service_time is not null) and 
								((DATEADD(MINUTE,duration,service_time)) > @otime and (DATEADD(MINUTE,duration,service_time)) < @ttime) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') then 1
						when (service_time is not null) and (service_time = @otime) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') then 1
						else 0
					end
				) and (@residentid is null or @roomid is null)
	)
	insert into @tbl
	select service_time, resident_id, room_id, duration, recurrence 
	from cte_matches
	group by service_time, resident_id, room_id, duration, recurrence 
	RETURN; 
END





GO


