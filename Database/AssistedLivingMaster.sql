USE [AssistedLivingMaster]
GO
/****** Object:  Table [dbo].[AgencyList]    Script Date: 10/25/2021 4:29:48 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyList](
	[agency_id] [varchar](20) NOT NULL,
	[agency_name] [varchar](250) NULL,
	[conn_string] [varchar](2000) NOT NULL,
	[db_name] [varchar](100) NULL,
	[activation_status] [int] NULL,
	[activation_date] [datetime] NULL,
	[timezone_id] [varchar](100) NULL,
	[timezone_utc_offset] [int] NULL,
	[authentication_timeout] [int] NOT NULL CONSTRAINT [auth_default_timeout]  DEFAULT ((240)),
 CONSTRAINT [PK_Agencies] PRIMARY KEY CLUSTERED 
(
	[agency_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[AgencyList] ([agency_id], [agency_name], [conn_string], [db_name], [activation_status], [activation_date], [timezone_id], [timezone_utc_offset], [authentication_timeout]) VALUES (N'clientdemo', N'CLIENT DEMO', N'Data Source=LAPTOP-QPOANU75;Initial Catalog=ALCClientNewRelease;Integrated Security=True', N'ALCClientNewRelease', NULL, NULL, N'Central Standard Time', 480, 240)
INSERT [dbo].[AgencyList] ([agency_id], [agency_name], [conn_string], [db_name], [activation_status], [activation_date], [timezone_id], [timezone_utc_offset], [authentication_timeout]) VALUES (N'demo', N'Assisted Living Demo', N'Data Source=LAPTOP-QPOANU75;Initial Catalog=AssistedLiving;Integrated Security=True', N'AssistedLiving', NULL, NULL, N'Egypt Standard Time', 480, 280)
