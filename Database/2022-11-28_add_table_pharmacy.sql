
CREATE TABLE [dbo].[Pharmacy](
	[pharmacy_id] [int] IDENTITY(1,1) NOT NULL,
	[pharmacy_name] [nvarchar](150) NULL,
	[date_created] [datetime] NULL,
 CONSTRAINT [PK_pharmacy] PRIMARY KEY CLUSTERED 
(
	[pharmacy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


