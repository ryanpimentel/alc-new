USE [ALCClient]
GO
--select * From [dbo].[fn_GetCarestaffByRecurrence_UnMatches] ('02:00 PM',9, null, '1,2')
ALTER FUNCTION [dbo].[fn_GetCarestaffByRecurrence_Unmatches] 
(
	@ctime datetime = null,
	@activityid int = null,
	@carestaffid varchar(20) = null,
	@recday char(1) = null,
	@clientdt datetime = null,
	@isonetime bit = 0
)
RETURNS
	@tbl TABLE (service_time time(7), [user_id] varchar(20), carestaff varchar(255), shift_start time(7), shift_end time(7), recurrence varchar(20))
AS
BEGIN
	declare @ttime time(7) = null
	declare @otime time(7) = null
	declare @duration int = 0
	declare @deptid int = 0

	select @duration = duration, @deptid = dept_id from Activity where activity_id = @activityid
	set @otime = cast(@ctime as time(7))
	set @ttime = cast((DATEADD(MINUTE,@duration,@ctime)) as time(7))
			
	;with cte as (
		select 	service_time = cast(a.start_time as time(7)), 
				service_date = cast(a.start_time as date),
				b.[user_id],
				carestaff = b.first_name + ' ' + b.last_name,
				shift_start = d.start_time, 
				shift_end = d.end_time,
				duration = f.duration,
				recurrence,
				isonetime = isnull(is_onetime,0)
		from	ALCUser b 
		left join	[CarestaffShift] c on b.[user_id] = c.carestaff_id
		left join	[Shift] d on c.shift_id = d.shift_id
		left join	ActivitySchedule a on a.carestaff_id = b.[user_id]
		left join	Activity f on a.activity_id = f.activity_id
		-- changed matching of column for department (alona, 11-22-22)
		-- left join	ALCUserRole e on b.[user_id] = e.[user_id]	
		---where	b.is_active = 1 and e.role_id = @deptid --in (3,4,5,6,7)
		left join	DepartmentUser e on b.[user_id] = e.[user_id]
		where	b.is_active = 1 and e.department_id = @deptid --in (3,4,5,6,7)
	), cte_matches as (
		select 	service_time, [user_id], carestaff, shift_start,  shift_end, recurrence
		from	cte 
		where	0 = (
					case
						when ([dbo].[fn_IsAvailableTimeSlot](@deptid, @ctime, @duration, @recday, [user_id], @clientdt, @isonetime) > 0) then 0
						when ([dbo].[fn_IsPassMidnightOverlap](shift_start, shift_end, @otime, @ttime, service_time, duration) > 0) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') 
								and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								))
								then 0
						when (service_time is not null) and 
								((DATEADD(MINUTE,duration,service_time)) > @otime and  service_time < @ttime) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') 
								and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								))
								then 0
						when (service_time is not null) and (service_time = @otime) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') 
								and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								))
								then 0
						else 1
					end
				) and (@carestaffid is null or @carestaffid <> [user_id])
	), 
	cte_curusermatches as (
		select 	service_time, [user_id], carestaff, shift_start,  shift_end, recurrence
		from	cte 
		where	cast(@ctime as time(7)) = service_time and [user_id] = @carestaffid
	)
	--select * from cte_matches
	, cte_unmatches as (
		select 	service_time, [user_id], carestaff, shift_start, shift_end, recurrence
		from	cte 
		where	
				1 = (
					case
						when (shift_start < shift_end) and (@otime < shift_start) then 0
						when (shift_start < shift_end) and (@otime not between shift_start and shift_end)  then 0
						--when (shift_start > shift_end) and ((@otime >= shift_start or @otime < shift_end)) and (@ttime > (DATEADD(MINUTE,-(duration),shift_end)))
						--	 then 0
						when (shift_start < shift_end) and (@otime between shift_start and shift_end) and (@ttime > (DATEADD(MINUTE,-(duration),shift_end))) then 0
						when (service_time = @otime) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') NOT LIKE '%,' + @recday + ',%') and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								)) then 1
						when ([dbo].[fn_IsPassMidnightOverlap](shift_start, shift_end, @otime, @ttime, service_time, duration) > 0)  then 0
						when shift_start < shift_end and (@ttime > shift_end or @ttime < shift_start) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								)) then 0
								--10:00 - 11:00
								--10:30 - 11:00
						when (service_time >= @otime and service_time < @ttime) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								)) then 0
						else 1
					end
				)
	)
	--select * from cte_unmatches
	insert into @tbl
	select service_time, [user_id], carestaff, shift_start, shift_end, recurrence
	from	cte_unmatches a 
	where  --do not include the matched rows
			not exists(select [user_id] from cte_matches where [user_id] = a.[user_id]) 
	group by service_time, [user_id], carestaff, shift_start, shift_end, recurrence
	union all
	select service_time, [user_id], carestaff, shift_start, shift_end, recurrence
	from	cte_curusermatches
	RETURN; 
END





