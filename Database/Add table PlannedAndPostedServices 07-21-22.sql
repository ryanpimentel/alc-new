USE [ALCClient]
GO

/****** Object:  Table [dbo].[PlannedAndPostedServices]    Script Date: 07/21/2022 10:54:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PlannedAndPostedServices](
	[planned_posted_service_id] [int] IDENTITY(1,1) NOT NULL,
	[planned_service_id] [int] NULL,
	[planned_date] [datetime] NULL,
	[posted_date] [datetime] NULL,
	[actual_start_time] [datetime] NULL,
	[actual_end_time] [datetime] NULL,
	[services_rendered] [varchar](50) NULL,
	[carestaff_id] [nvarchar](20) NULL,
	[is_active] [bit] NULL,
	[remarks] [nvarchar](50) NULL,
	[signature] [image] NULL,
	[location] [nvarchar](50) NULL,
 CONSTRAINT [PK_PlannedAndPostedServices] PRIMARY KEY CLUSTERED 
(
	[planned_posted_service_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


