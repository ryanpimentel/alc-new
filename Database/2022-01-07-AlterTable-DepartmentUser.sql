ALTER TABLE DepartmentUser
ADD is_active tinyint default 1, modified_by VARCHAR(50), date_modified DATETIME;