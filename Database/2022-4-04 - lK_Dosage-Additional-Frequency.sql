
use AssistedLiving
SET IDENTITY_INSERT lk_Dosage ON
insert into lk_Dosage(dosage_id, dosage_description)
values('5', 'Every hour');
insert into lk_Dosage(dosage_id, dosage_description)
values('6', 'Every 2 hours');
insert into lk_Dosage(dosage_id, dosage_description)
values('7', 'Every 4 hours');
insert into lk_Dosage(dosage_id, dosage_description)
values('8', 'Every 6 hours');
insert into lk_Dosage(dosage_id, dosage_description)
values('9', 'Every 8 hours');
insert into lk_Dosage(dosage_id, dosage_description)
values('10', 'Before Meals'); 
insert into lk_Dosage(dosage_id, dosage_description)
values('11', 'After Meals');
insert into lk_Dosage(dosage_id, dosage_description)
values('12','Every Morning');
insert into lk_Dosage(dosage_id, dosage_description)
values('13','Every Afternoon');
insert into lk_Dosage(dosage_id, dosage_description)
values('14', 'Every Night at Bedtime');
insert into lk_Dosage(dosage_id, dosage_description)
values('15', 'Every Other Day');
SET IDENTITY_INSERT lk_Dosage OFF