USE [ALCClient]
GO
/****** Object:  StoredProcedure [dbo].[getActivityScheduleByUser]    Script Date: 10/19/2022 8:08:45 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [getActivityScheduleByUser]  'dancy', 5
-- [getActivityScheduleByUser]  'dancy', 4,'5/9/2017',null,null,null
-- [getActivityScheduleByUser]  'john', '2016-09-12','2017-03-21',1
-- [getActivityScheduleByUser]  'john', '2016-10-05'
-- [getActivityScheduleByUser]  'john', '2017-03-21',null,'nancy'
-- [getActivityScheduleByUser]  'john',1,'10/17/2017'
ALTER proc [dbo].[getActivityScheduleByUser]
	@user_id varchar(50) = null,
	@role_id int = null,
	@clientdate datetime = null,
	@clientdate2 datetime = null,
	@resident_id int = null,
	@room_id int = null
as
begin
	declare @userRoleId int = null
	declare @staffRoleId int = null
	declare @curday varchar(1) = null
	declare @endday varchar(1) = null
	declare @isenddatenull bit = 0

	if(@clientdate2 is null)
	begin
		set @clientdate2 = @clientdate;
		set @isenddatenull = 1
	end

	select @curday = cast(datepart(dw, @clientdate) - 1 as varchar(1))
	select @endday = cast(datepart(dw, @clientdate2) - 1 as varchar(1))
	
	declare @dict table (dept_role_id int, staff_role_id int)

	;with depthead as (  
		select rn = row_number() over( order by role_id), role_id, [description] from ALCRole (nolock)
		where role_id in (9,10,11,12,13)
	),
	staffs as (
		select rn = row_number() over( order by role_id), role_id, [description] from ALCRole (nolock)
		where role_id in (3,4,5,6,7)
	)
	insert into @dict
	select a.role_id, b.role_id  from depthead a full join staffs b on a.rn = b.rn
	
	set @userRoleId = @role_id
	--select	@userRoleId = b.role_id
	--from	ALCUser a
	--join	ALCUserRole b on a.[user_id] = b.[user_id]
	--where	a.[user_id] = @user_id and (@role_id is null or b.role_id = @role_id)

	--if user in dept heads
	declare @isdepthead bit = 0
	if(@userRoleId in (9,10,11,12,13))
	begin
		select @staffRoleId = staff_role_id from @dict where dept_role_id = @userRoleId
		set @isdepthead = 1
	end
	else if(@userRoleId in (1,2)) -- super admins
		select @staffRoleId = @userRoleId
	else
		select @staffRoleId = @userRoleId
	--print @staffRoleId
	;with cte_all as (
		select  b.activity_schedule_id,
				a.activity_id,
				activity_desc,
				activity_proc = activity_procedure,
				resident_id = case when a.dept_id not in (5,7) and b.resident_id is not null then b.resident_id else null end,
				resident = case when a.dept_id not in (5,7) and b.resident_id is not null then isnull(c.first_name,'') + ' ' + isnull(c.last_name,'') else null end,
				room_id = case when a.dept_id in (5,7) and b.room_id is not null then b.room_id else f.room_id end,
				room = case when a.dept_id in (5,7) and b.room_id is not null then m.room_name else f.room_name end,
				start_time = start_time,
				a.dept_id,
				--department = k.[description], **changed value of department for it displays role description not department name 10-18-22
				department = dept.name, 
				recurrence,
				service_duration = a.duration,
				carestaff_name = isnull(u.first_name,'') + ' ' + isnull(u.last_name,''),
				carestaff_id = b.carestaff_id,
				activity_status = 0,
				oactivity_status = 0,
				remarks = '',
				reschedule_dt = null,
				completion_date = null,
				completion_type = null,
				carestaff_activity_id = null,
				acknowledged_by = '',
				is_onetime = isnull(is_onetime,0),
				xref = isnull(b.xref,0),
				create_date = b.date_created,
				actual_completion_date = null,
				c.isAdmitted,
				c.responsible_person_email,
				b.is_active,
				b.active_until,
				dept.is_maintenance,
				b.acknowledgeBy

		from	(select activity_schedule_id = max(activity_schedule_id), activity_id,carestaff_id, resident_id,rec = recurrence 
				 from ActivitySchedule (nolock)
				 where (@isenddatenull = 0 or @curday is null OR (',' + RTRIM(recurrence) + ',') LIKE '%,' + @curday + ',%')
				 group by activity_id,carestaff_id, resident_id,recurrence, cast(start_time as time)) bb 
		join ActivitySchedule b (nolock) on bb.activity_schedule_id = b.activity_schedule_id
		join  Activity a (nolock) on a.activity_id = b.activity_id
		left join Resident c (nolock) on b.resident_id = c.resident_id
		left join Admission d (nolock) on c.resident_id = d.resident_id
		left join Room m (nolock) on m.room_id = b.room_id
		left join Room f (nolock) on f.room_id = d.room_id
		join ALCUser u (nolock) on b.carestaff_id = u.[user_id]
		left join ALCUserRole r (nolock) on u.[user_id] = r.[user_id]
		left join ALCRole k (nolock) on k.role_id = a.dept_id
		left join Department dept (nolock) on dept.department_id = a.dept_id
		--left join (select activity_schedule_id, cai = max(carestaff_activity_id) from CarestaffActivity (nolock) group by activity_schedule_id) v on v.activity_schedule_id = b.activity_schedule_id
		--left join CarestaffActivity e on b.activity_schedule_id = e.activity_schedule_id
		where	1 = (
					case when @staffRoleId IN (1,2) then 1
						 when @isdepthead = 1 AND ((r.role_id = @staffRoleId ) OR (b.carestaff_id = @user_id)) then 1
						 when @isdepthead = 0 AND a.dept_id = @staffRoleId AND b.carestaff_id = @user_id then 1
					end
				) AND
				--exclude onetime schedule that already expired based on client date parameter
				1 = (
					case when isnull(is_onetime,0) = 1 and (@clientdate is not null and @clientdate2 is not null and 
							  (cast(start_time as date) not between cast(@clientdate as date) and cast(@clientdate2 as date))) then 0
						 when isnull(is_onetime,0) = 1 and (@clientdate is not null and @clientdate2 is not null and 
							  (cast(start_time as date) <> cast(@clientdate as date))) then 0
						 else 1
					end
				) AND
				--((@staffRoleId in (1,2)) OR (((r.role_id = @staffRoleId ) OR (b.carestaff_id = @user_id)))) AND
				(@isenddatenull = 0 or @curday is null OR (',' + RTRIM(recurrence) + ',') LIKE '%,' + @curday + ',%') --AND
				--(@endday is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @endday + ',%')
				--and 1 = ( case when @endday is null then 0 else 1 end)
				AND  1 = (case when isnull(b.resident_id,0) <> 0 then (case when c.[status] = 1 then 1 else 0 end) else 1 end)
		
		union

		select  b.activity_schedule_id,
				a.activity_id,
				activity_desc,
				activity_proc = activity_procedure,
				resident_id = case when a.dept_id not in (5,7) and b.resident_id is not null then b.resident_id else null end,
				resident = case when a.dept_id not in (5,7) and b.resident_id is not null then isnull(c.first_name,'') + ' ' + isnull(c.last_name,'') else null end,
				room_id = case when a.dept_id in (5,7) and b.room_id is not null then b.room_id else f.room_id end,
				room = case when a.dept_id in (5,7) and b.room_id is not null then m.room_name else f.room_name end,
				start_time = b.start_time,
				a.dept_id,
				--department = k.[description], **changed value of department for it displays role description not department name 10-18-22
				department = dept.name, 
				b.recurrence,
				service_duration = a.duration,
				carestaff_name = isnull(u.first_name,'') + ' ' + isnull(u.last_name,''),
				carestaff_id = e.carestaff_id,
				activity_status = isnull(e.activity_status,0),
				oactivity_status = isnull(e.activity_status,0),
				remarks = e.remarks,
				reschedule_dt = case when isnull(b.xref, 0) = 0 then null else case when g.start_time < e.completion_date then null else g.start_time end end,
				e.completion_date,
				e.completion_type,
				e.carestaff_activity_id,
				acknowledged_by = case when e.carestaff_id is not null then isnull(cu.first_name,'') + ' ' + isnull(cu.last_name,'') else '' end,--e.carestaff_id,
				is_onetime = isnull(b.is_onetime, 0),
				xref = isnull(b.xref,0),
				create_date = b.date_created,
				e.actual_completion_date,
				c.isAdmitted,
				c.responsible_person_email,
				b.is_active,
				b.active_until,
				dept.is_maintenance,
				g.acknowledgeBy
				
		from	ActivitySchedule b (nolock)
		left join ActivitySchedule g (nolock) on b.xref = g.activity_schedule_id
		join  Activity a (nolock) on a.activity_id = b.activity_id
		left join Resident c (nolock) on b.resident_id = c.resident_id
		left join Admission d (nolock) on c.resident_id = d.resident_id
		left join Room m (nolock) on m.room_id = b.room_id
		left join Room f (nolock) on f.room_id = d.room_id
		join ALCUser u (nolock) on b.carestaff_id = u.[user_id]
		left join ALCUserRole r (nolock) on u.[user_id] = r.[user_id]
		left join ALCRole k (nolock) on k.role_id = a.dept_id
		left join Department dept (nolock) on dept.department_id = a.dept_id
		join (	select activity_schedule_id, cai = max(carestaff_activity_id) 
				from CarestaffActivity (nolock) 
				where 1 = (
					case when	@clientdate is not null and @clientdate2 is not null and
								(cast(completion_date as date) between cast(@clientdate as date) and cast(@clientdate2 as date)) then 1
						 when	@clientdate is not null and cast(completion_date as date) = cast(@clientdate as date) then 1
					end)
				group by activity_schedule_id
			 ) v on v.activity_schedule_id = b.activity_schedule_id
		join CarestaffActivity e on v.activity_schedule_id = e.activity_schedule_id
		join ALCUser cu (nolock) on e.carestaff_id = cu.[user_id]
		where	1 = (
					case when @staffRoleId IN (1,2) then 1
						 when @isdepthead = 1 AND ((r.role_id = @staffRoleId ) OR (b.carestaff_id = @user_id)) then 1
						 when @isdepthead = 0 AND a.dept_id = @staffRoleId AND b.carestaff_id = @user_id then 1
					end
				) AND
				--((@staffRoleId in (1,2)) OR (((r.role_id = @staffRoleId ) OR (b.carestaff_id = @user_id)))) AND 
				(@curday is null or (',' + RTRIM(b.recurrence) + ',') LIKE '%,' + @curday + ',%') AND
				(@endday is null or (',' + RTRIM(b.recurrence) + ',') LIKE '%,' + @endday + ',%') AND
				1 = (
					case when	@clientdate is not null and @clientdate2 is not null and
								(cast(e.completion_date as date) between cast(@clientdate as date) and cast(@clientdate2 as date)) then 1
						 when	@clientdate is not null and cast(e.completion_date as date) = cast(@clientdate as date) then 1
					end
				) AND
				--exclude onetime schedule that already expired based on client date parameter
				1 = (
					case when isnull(b.is_onetime,0) = 1 and (@clientdate is not null and @clientdate2 is not null and 
							  (cast(b.start_time as date) not between cast(@clientdate as date) and cast(@clientdate2 as date))) then 0
						 when isnull(b.is_onetime,0) = 1 and (@clientdate is not null and @clientdate2 is not null and 
							  (cast(b.start_time as date) <> cast(@clientdate as date))) then 0
						 else 1
					end
				)
				AND  1 = (case when isnull(b.resident_id,0) <> 0 then (case when c.[status] = 1 then 1 else 0 end) else 1 end)
	)
	
	,
	cte_partition as (
		select	row_number() over(partition by activity_schedule_id order by carestaff_activity_id desc) as freq, *
		from cte_all
	)
	select activity_schedule_id, activity_id, activity_desc, activity_proc, resident, 
			room, start_time, department, recurrence, service_duration, carestaff_name,
			activity_status, oactivity_status, remarks, reschedule_dt, completion_date, 
			completion_type, carestaff_activity_id, acknowledged_by, is_onetime, xref,
			resident_id, actual_completion_date, isAdmitted, responsible_person_email,is_active,active_until,isAdmitted,is_maintenance,acknowledgeBy 
	from cte_partition
	where freq = 1 and
		  (@resident_id is null or @resident_id = resident_id) and
		  (@clientdate is null or cast(@clientdate as date) >= cast(create_date as date)) and
		  (@room_id is null or 
		  --if role in super admins then show maintenance and housekeeping tasks only
		  (1 = case when isnull(@room_id,0) <> 0 and @userRoleId in (1,2) and dept_id in (5,7) and @room_id = room_id then 1 
					  when isnull(@room_id,0) <> 0 and @userRoleId in (11,13) and @userRoleId = dept_id and @room_id = room_id then 1
					  else 0 end))
	order by department, room, resident, start_time

end



