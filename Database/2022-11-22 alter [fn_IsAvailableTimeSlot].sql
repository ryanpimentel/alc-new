USE [ALCClient]
GO

ALTER FUNCTION [dbo].[fn_IsAvailableTimeSlot]  	
(
	@deptid int,
	@ctime datetime,
	@duration int,
	@recday char(1),
	@userid varchar(20),
	@clientdt datetime = null,
	@isonetime bit = 0
)
RETURNS INT
AS
BEGIN
	declare @ttime time(7)
	declare @otime time(7)
	declare @curdate date

	if(@clientdt is null)
		set @curdate = getdate()
	else
		set @curdate = cast(@clientdt as date)

	set @otime = cast(@ctime as time(7))
	set @ttime = dateadd(minute, @duration,@ctime)
	declare @hasMatch int

	;with cte as (
		select 	service_time = cast(a.start_time as time(7)), 
				service_date = cast(a.start_time as date),
				isonetime = isnull(is_onetime, 0),
				duration = f.duration,
				b.[user_id],
				carestaff = b.first_name + ' ' + b.last_name,
				shift_start = d.start_time, 
				shift_end = d.end_time,
				recurrence
		from	ALCUser b 
		left join	[CarestaffShift] c on b.[user_id] = c.carestaff_id
		left join	[Shift] d on c.shift_id = d.shift_id
		left join	ActivitySchedule a on a.carestaff_id = b.[user_id]
		left join	Activity f on a.activity_id = f.activity_id
		-- changed matching of column for department (alona, 11-22-22)
		-- left join	ALCUserRole e on b.[user_id] = e.[user_id]
		---where	b.is_active = 1 and e.role_id = @deptid --in (3,4,5,6,7)
		left join	DepartmentUser e on b.[user_id] = e.[user_id]	
		where	b.is_active = 1 and e.department_id = @deptid --in (3,4,5,6,7)
	), cte2 as (
		select rn = -1 + row_number() over(order by service_time),
		 * from cte where [user_id] = @userid and
		 (@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%')
	), cte3 as (
		select rn = row_number() over(order by service_time),
		 * from cte where [user_id] = @userid and
		 (@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%')
	),cte4 as (
		select  a.[user_id],
				time_start = dateadd(minute, b.duration,b.service_time),
				time_end = a.service_time,
				[start_date] = a.service_date,
				a.recurrence, a.isonetime
		from	cte2 a
		join	cte3 b on a.rn = b.rn
		where	1 = case 
						--exclude onetime schedule that are already expired
						when a.service_date < @curdate and a.[isonetime] = 1 then 0
						--exclude if client has only one schedule yet
						when dateadd(minute, b.duration,b.service_time) <> a.service_time then 1
				    end
	)

	select top 1 @hasMatch =	
			case 
				when ( case when @isonetime = 1 and isonetime = 1 and [start_date] = @curdate and @ttime > time_end then 1 else 0 end) = 1 then 1
				when ( case when isonetime = 0 and @ttime > time_end then 1 else 0 end ) = 1 then 1				
				else 0
			end
	from cte4
	where	(@otime between time_start and time_end)

	--select @rows = count(1) from cte4
	--where	(@otime between time_start and time_end) and (@ttime > time_end) --(@ttime between time_start and time_end)
			
	RETURN isnull(@hasMatch,0)
END



