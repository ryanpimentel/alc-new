﻿
CREATE proc [dbo].[getMARSchedule]
	@date_administered date = null
as
begin
	select	a.admission_id, 
			a.resident_id,
			b.receive_detail_id,
			resident_name = r.first_name + ' ' + case when r.middle_initial is not null then upper(r.middle_initial) + '. ' else '' end + r.last_name,
			medicine = e.[description],
			dosage_time = CONVERT(VARCHAR(5),s.schedule_time, 108),
			dosage = c.dosage_id,
			s.dosage_schedule_id,
			intake_status = case when CONVERT(VARCHAR(10), m.[actual_date_administered], 120) = CONVERT(VARCHAR(10), @date_administered, 120) then m.is_given else null end,
			daily_med_id = case when CONVERT(VARCHAR(10), m.[actual_date_administered], 120) = CONVERT(VARCHAR(10), @date_administered, 120) then m.daily_med_id else null end,
			b.is_prn,
			b.prn_date,
			e.[status]
	from	[ReceiveMedPrescription] (nolock) a 
	join	ReceiveMedDetail (nolock) b on a.receive_prescription_id = b.receive_prescription_id and b.is_prn is null
	join	Resident (nolock) r on a.resident_id = r.resident_id
	join	lk_dosage (nolock) c on b.dosage = c.dosage_id
	left join [Supply] (nolock) e on b.supply_id = e.supply_id
	left join DosageSchedule (nolock) s on c.dosage_id = s.dosage_id
	left join DailyMedication (nolock) m on s.dosage_schedule_id = m.dosage_schedule_id and m.receive_detail_id = b.receive_detail_id
	--where	(m.[actual_date_administered] is not null or
	--		m.[actual_date_administered] is null) --or 
			--(year(m.[actual_date_administered]) * 100 + month(m.[actual_date_administered]) =  
			--year(@date_administered) * 100 + month(@date_administered)))
	group by	a.admission_id, a.resident_id, b.receive_detail_id,
				e.[description], s.schedule_time, c.dosage_id,m.is_given,m.daily_med_id,
				r.first_name, r.middle_initial,r.last_name,s.dosage_schedule_id,b.is_prn, b.prn_date,m.[actual_date_administered], e.[status]

	union all
	select	a.admission_id, 
			a.resident_id,
			b.receive_detail_id,
			resident_name = r.first_name + ' ' + case when r.middle_initial is not null then upper(r.middle_initial) + '. ' else '' end + r.last_name,
			medicine = e.[description],
			dosage_time = CONVERT(VARCHAR(5),s.schedule_time, 108),
			dosage = c.dosage_id,
			s.dosage_schedule_id,
			intake_status = m.is_given,
			m.daily_med_id,
			b.is_prn,
			b.prn_date,
			e.[status]
	from	[ReceiveMedPrescription] (nolock) a
	join	ReceiveMedDetail (nolock) b on a.receive_prescription_id = b.receive_prescription_id
	join	Resident (nolock) r on a.resident_id = r.resident_id
	join	lk_dosage (nolock) c on b.dosage = c.dosage_id
	left join [Supply] (nolock) e on b.supply_id = e.supply_id
	left join DosageSchedule (nolock) s on c.dosage_id = s.dosage_id
	left join DailyMedication (nolock) m on s.dosage_schedule_id = m.dosage_schedule_id and m.receive_detail_id = b.receive_detail_id
	where	(b.is_prn = 1 and cast(b.[prn_date] as date) = @date_administered)
	group by	a.admission_id, a.resident_id, b.receive_detail_id,
				e.[description], s.schedule_time, c.dosage_id,m.is_given,m.daily_med_id,
				r.first_name, r.middle_initial,r.last_name,s.dosage_schedule_id,b.is_prn, b.prn_date, e.[status]
				

end