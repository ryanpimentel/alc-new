
/****** Object:  Table [dbo].[EmployeeFormRequests]    Script Date: 03/28/2022 10:05:43 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeFormRequests](
	[empformrequest_id] [int] IDENTITY(1,1) NOT NULL,
	[form_title] [varchar](100) NULL,
	[file_name] [varchar](max) NULL,
	[file_path] [varchar](max) NULL,
	[file_type] [varchar](500) NULL,
	[file_size] [int] NULL,
	[is_archieved] [bit] NULL,
	[archived_date] [datetime] NULL,
	[date_created] [datetime] NULL,
	[created_by] [varchar](50) NULL,
	[date_modified] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[UUID] [uniqueidentifier] NULL,
	[is_active] [bit] NULL,
	[is_approved] [bit] NULL,
	[approved_date] [datetime] NULL,
 CONSTRAINT [PK_EmployeeFormRequests] PRIMARY KEY CLUSTERED 
(
	[empformrequest_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
