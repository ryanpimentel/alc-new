USE [ALC_UA]
GO
/****** Object:  StoredProcedure [dbo].[saveAction]    Script Date: 7/18/2022 10:14:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[saveAction]
	
	@action nvarchar(50),
	@userID nvarchar(50),
	@recordID int,
	@source nvarchar(50),
	-- @updated nvarchar(50),
	@updatedRecord nvarchar(max)


	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO ActionAudit(source__name, action_taken, userId, record_id, record) values(@source, @action, @userID, @recordID, @updatedRecord)
END


