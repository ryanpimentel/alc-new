use AssistedLiving
update lk_Dosage
set dosage_description = 'Once Daily'  
where dosage_id = 1;

update lk_Dosage
set dosage_description = 'Twice Daily'  
where dosage_id = 2;
