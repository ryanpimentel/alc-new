USE [AssistedLiving]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCarestaffByRecurrence_Matches]    Script Date: 06/28/2022 3:03:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery2.sql|7|0|C:\Users\user\AppData\Local\Temp\~vs220B.sql
--select * From [dbo].[fn_GetCarestaffByRecurrence_Matches] ('08:50 AM',4, null, '3') --'MYTera'
ALTER FUNCTION [dbo].[fn_GetCarestaffByRecurrence_Matches] 
(
	@ctime datetime = null,
	@activityid int = null,
	@carestaffid varchar(20) = null,
	@recday char(1) = null,
	@clientdt datetime = null,
	@isonetime bit = 0
)
RETURNS
	@tbl TABLE (service_time time(7), [user_id] varchar(20), carestaff varchar(255), shift_start time(7), shift_end time(7), recurrence varchar(20))
AS
BEGIN
	declare @ttime time(7) = null
	declare @otime time(7) = null
	declare @duration int = 0
	declare @deptid int = 0

	select @duration = duration, @deptid = dept_id from Activity where activity_id = @activityid
	set @otime = cast(@ctime as time(7))
	set @ttime = cast((DATEADD(MINUTE,@duration,@ctime)) as time(7))
			
	;with cte as (
		select 	service_time = cast(a.start_time as time(7)), 
				service_date = cast(a.start_time as date),
				b.[user_id],
				carestaff = b.first_name + ' ' + b.last_name,
				shift_start = d.start_time, 
				shift_end = d.end_time,
				duration = f.duration,
				recurrence,
				isonetime = isnull(is_onetime,0)
		from	ALCUser b 
		left join	[CarestaffShift] c on b.[user_id] = c.carestaff_id
		left join	[Shift] d on c.shift_id = d.shift_id
		left join	ActivitySchedule a on a.carestaff_id = b.[user_id]
		left join	Activity f on a.activity_id = f.activity_id
		--left join	DepartmentUser e on b.[user_id] = e.[user_id]		
		--where	b.is_active = 1 and e.department_id = @deptid --in (3,4,5,6,7)
		left join	ALCUserRole e on b.[user_id] = e.[user_id]
	    where	b.is_active = 1 and e.role_id = @deptid --in (3,4,5,6,7)
	), cte_matches as (
		select 	service_time, [user_id], carestaff, shift_start,  shift_end, recurrence
		from	cte 
		where	0 = (
					case
						when ([dbo].[fn_IsAvailableTimeSlot](@deptid, @ctime, @duration, @recday, [user_id], @clientdt, @isonetime) > 0) then 0
						when ([dbo].[fn_IsPassMidnightOverlap](shift_start, shift_end, @otime, @ttime, service_time, duration) > 0) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') 
								and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								))
								then 0
						when (service_time is not null) and 
								((DATEADD(MINUTE,duration,service_time)) > @otime and  service_time < @ttime) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') 
								and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								))
								then 0
						when (service_time is not null) and (service_time = @otime) and
								(@recday is null or recurrence is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @recday + ',%') 
								and
								(@isonetime = 0 or  1 = (
									case 
										when isonetime = 1 and [service_date] = cast(@clientdt as date) then 1
										else 0
									end
								))
								then 0
						else 1
					end
				) and (@carestaffid is null or @carestaffid <> [user_id])
	)
	insert into @tbl
	select service_time, [user_id], carestaff, shift_start, shift_end, recurrence
	from	cte_matches a 
	group by service_time, [user_id], carestaff, shift_start, shift_end, recurrence
	order by carestaff
	RETURN; 
END



