USE [ALCClient]
GO

/****** Object:  Table [dbo].[PlannedServices]    Script Date: 07/21/2022 10:38:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PlannedServices](
	[planned_service_id] [int] IDENTITY(1,1) NOT NULL,
	[client_id] [int] NULL,
	[carestaff_id] [nvarchar](50) NULL,
	[soc] [datetime] NULL,
	[eoc] [datetime] NULL,
	[start_time] [datetime] NULL,
	[end_time] [datetime] NULL,
	[services] [varchar](20) NULL,
	[recurrence] [varchar](20) NULL,
	[date_created] [datetime] NULL,
	[created_by] [varchar](50) NULL,
	[is_active] [bit] NULL,
	[is_onetime] [bit] NULL,
 CONSTRAINT [PK_PlannedServices] PRIMARY KEY CLUSTERED 
(
	[planned_service_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


