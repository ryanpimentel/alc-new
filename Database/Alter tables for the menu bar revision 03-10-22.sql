update TreeSubNode 
set tree_sub_node_name = 'Amenities',
tree_sub_node_description = 'Amenities'

where tree_sub_node_name = 'Facilities'

update TreeSubNode 
set tree_sub_node_id = 'ADMAmenities'
where tree_sub_node_id = 'ADMFacilities'

update TreeNode 
set sort_order = 2
where tree_node_id = 'ADMAssessment'

update TreeNode 
set sort_order = 3
where tree_node_id = 'ADMPOS'

update TreeNode 
set sort_order = 4
where tree_node_id = 'ADMMedication'

update TreeNode 
set sort_order = 5
where tree_node_id = 'ADMSecurity'



update TreeSubNode
set tree_node_id = 'ADMAdministrative',
sort_order = 2
where tree_sub_node_name = 'Settings'

update TreeSubNode
set tree_node_id = 'ADMAdministrative',
sort_order = 3
where tree_sub_node_name = 'Employees'

update TreeSubNode
set tree_node_id = 'ADMAdministrative',
sort_order = 4
where tree_sub_node_name = 'Departments'

update TreeSubNode
set tree_node_id = 'ADMAdministrative',
sort_order = 5
where tree_sub_node_name = 'Amenities'

update TreeSubNode
set tree_node_id = 'ADMAdministrative',
sort_order = 6
where tree_sub_node_name = 'Rooms'

update TreeSubNode
set tree_node_id = 'ADMAdministrative',
sort_order = 7
where tree_sub_node_name = 'Services'

update TreeSubNode
set tree_node_id = 'ADMAdministrative',
sort_order = 8
where tree_sub_node_name = 'Shifts'

update TreeSubNode
set tree_node_id = 'ADMAdministrative',
sort_order = 9
where tree_sub_node_name = 'Score Settings'



update TreeSubNode
set tree_node_id = 'ADMPOS',
sort_order = 1
where tree_sub_node_name = 'Residents'

update TreeSubNode
set tree_node_id = 'ADMPOS',
sort_order = 2
where tree_sub_node_name = 'Marketing'

update TreeSubNode
set tree_node_id = 'ADMSecurity',
sort_order = 1
where tree_sub_node_name = 'Daily Tasks and Schedules'