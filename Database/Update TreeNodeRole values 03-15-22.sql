
DECLARE @role_id INT
DECLARE @getid CURSOR

SET @getid = CURSOR FOR
SELECT DISTINCT TreeNodeRole.role_id
FROM TreeNodeRole

OPEN @getid
FETCH NEXT
FROM @getid INTO @role_id
WHILE @@FETCH_STATUS = 0
BEGIN
    INSERT INTO TreeNodeRole(role_id, tree_node_id, access_type_id, date_modified, modified_by) values(@role_id, 'ADMAssessment', 3, '2022-03-11 18:29:59.940', 'system')
    FETCH NEXT
    FROM @getid INTO @role_id
END

CLOSE @getid
DEALLOCATE @getid 