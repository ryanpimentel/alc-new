USE [ALCClient]
GO
/****** Object:  Table [dbo].[UpdateDetails]    Script Date: 08/23/2022 2:10:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UpdateDetails](
	[alc_update_id] [int] IDENTITY(1,1) NOT NULL,
	[date_updated] [datetime] NULL,
	[version_number] [int] NULL,
 CONSTRAINT [PK_UpdateDetails] PRIMARY KEY CLUSTERED 
(
	[alc_update_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[UpdateDetails] ON 

INSERT [dbo].[UpdateDetails] ([alc_update_id], [date_updated], [version_number]) VALUES (1, CAST(N'2022-08-23 00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[UpdateDetails] OFF
