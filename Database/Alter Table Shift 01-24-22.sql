USE [AssistedLiving]
GO
ALTER TABLE Shift
ADD is_active tinyint DEFAULT 1,
 deleted_by nvarchar(50),
 date_deleted date