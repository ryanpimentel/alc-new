
/****** Object:  Table [dbo].[ActivityScheduleHMC]    Script Date: 05/19/2022 3:36:11 PM ******/
DROP TABLE [dbo].[ActivityScheduleHMC]
GO

/****** Object:  Table [dbo].[ActivityScheduleHMC]    Script Date: 05/19/2022 3:36:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ActivityScheduleHMC](
	[activity_schedule_id] [int] IDENTITY(1,1) NOT NULL,
	[services] [varchar](20) NULL,
	[resident_id] [int] NULL,
	[carestaff_id] [varchar](50) NULL,
	[start_time] [datetime] NULL,
	[end_time] [datetime] NULL,
	[start_date] [datetime] NULL,
	[end_date] [datetime] NULL,
	[recurrence] [varchar](20) NULL,
	[created_by] [varchar](50) NULL,
	[date_created] [datetime] NULL,
	[is_onetime] [bit] NULL,
	[xref] [int] NULL,
	[is_active] [bit] NULL,
	[active_until] [datetime] NULL,
	[acknowledgeBy] [varchar](20) NULL,
	[is_deleted] [bit] NULL,
	[date_deleted] [datetime] NULL,
	[deleted_by] [varchar](20) NULL,
	[is_edited] [bit] NULL,
	[date_edited] [datetime] NULL,
	[edited_by] [varchar](20) NULL,
	[is_rescheduled] [bit] NULL,
	[date_rescheduled] [datetime] NULL,
	[is_reassigned] [bit] NULL,
	[date_reassigned] [datetime] NULL,
	[rescheduled_by] [varchar](50) NULL,
	[reassigned_by] [varchar](50) NULL,
	[reference_id] [int] NULL,
 CONSTRAINT [PK_ActivityScheduleHMC] PRIMARY KEY CLUSTERED 
(
	[activity_schedule_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


