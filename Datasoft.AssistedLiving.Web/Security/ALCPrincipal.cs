﻿using System;
using System.Security.Principal;

namespace Datasoft.AssistedLiving.Security {
    public class ALCPrincipal : IPrincipal {
        private IIdentity identity;
        private string[] roles;

        public ALCPrincipal(IIdentity identity, string[] roles) {
            this.identity = identity;
            this.roles = new string[roles.Length];
            roles.CopyTo(this.roles, 0);
            Array.Sort(this.roles);
        }

        #region IPrincipal Members

        public IIdentity Identity {
            get { return identity; }
        }

        public bool IsInRole(string role) {
            return (Array.BinarySearch(this.roles, role) >= 0);
        }

        #endregion

        public string[] Roles {
            get { return roles; }
        }
    }
}