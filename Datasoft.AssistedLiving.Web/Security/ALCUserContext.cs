﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Datasoft.AssistedLiving.Security {
    public class ALCUserContext {
        public static string UserId {
            get {
                string userInfo = HttpContext.Current.User.Identity.Name;
                return userInfo.Split('|')[0];
            }
        }

        public static string AgencyId {
            get {
                string userInfo = HttpContext.Current.User.Identity.Name;
                return userInfo.Split('|')[1];
            }
        }

        public static string RoleId {
            get {
                string userInfo = HttpContext.Current.User.Identity.Name;
                return userInfo.Split('|')[2];
            }
        }

        private static TimeZoneInfo _agencyTZ;
        public static TimeZoneInfo AgencyTimeZone {
            get {
                if(_agencyTZ == null)
                    _agencyTZ = ALCUserManager.Instance.AgencyTimezone(AgencyId);
                return _agencyTZ;
            }
        }

        public static bool SetAgencyTimeZone(string timezoneId) {
            bool changed = ALCUserManager.Instance.SetAgencyTimeZone(AgencyId, timezoneId);
            if (changed)
                _agencyTZ = null;
            return changed;
        }
        
        public static int AuthTimeout {
            get {
                return ALCUserManager.Instance.AuthTimeout(AgencyId);
            }
        }
        // add code kim 12/18/2021
        public static int? Use_address
        {
            get
            {
                return ALCUserManager.Instance.Use_address(AgencyId);
            }
        }

        public static bool SetAgencyAuthTimeout(int timeout) {
            return ALCUserManager.Instance.SetAgencyAuthTimeout(AgencyId, timeout);
        }
    }
}