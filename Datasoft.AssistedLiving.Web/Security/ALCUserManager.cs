﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datasoft.AssistedLiving.Web.Models;

namespace Datasoft.AssistedLiving.Security {
    public sealed class ALCUserManager {
        private static readonly ALCUserManager instance;
        private IDictionary<string, AgencyInfo> agencyList = new Dictionary<string, AgencyInfo>();

        static ALCUserManager() {
            instance = new ALCUserManager();

            using (var ctx = new ALSMasterDataContext()) {
                var agencies = from a in ctx.AgencyLists
                               select new {
                                   a.agency_id,
                                   a.conn_string,
                                   a.activation_status,
                                   a.activation_date,
                                   a.timezone_id,
                                   a.authentication_timeout,
                                   // kim add code for use_address 12/17/2021
                                   a.use_address
                               };
                if (agencies != null) {
                    foreach (var a in agencies) {
                        // add code for use_adress kim 12/17/2021
                        instance.agencyList.Add(a.agency_id, new AgencyInfo { ConnectionString = a.conn_string, TimezoneID = a.timezone_id, AuthTimeout = a.authentication_timeout, Use_address = a.use_address });
                    }
                }
            }
        }

        public static ALCUserManager Instance {
            get { return instance; }
        }

        public string ConnectionString(string agencyId) {
            if (agencyList.ContainsKey(agencyId))
                return agencyList[agencyId].ConnectionString;

            return null;
        }

        public int AuthTimeout(string agencyId) {
            if (agencyList.ContainsKey(agencyId))
                return agencyList[agencyId].AuthTimeout;
            string defaultTimeout = System.Configuration.ConfigurationManager.AppSettings["DefaultAuthExpirationTimeout"];
            if (!string.IsNullOrEmpty(defaultTimeout) && Convert.ToInt32(defaultTimeout) > 0)
                return Convert.ToInt32(defaultTimeout);
            return 240; //4 hrs default
        }

        public int? Use_address(string agencyId)
        {
            if (agencyList.ContainsKey(agencyId))
                return agencyList[agencyId].Use_address;
            return 0;
        }

        public TimeZoneInfo AgencyTimezone(string agencyId) {
            /*
                var dt = DateTime.UtcNow;
                Console.WriteLine(dt.ToLocalTime());

                var tz = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                var utcOffset = new DateTimeOffset(dt, TimeSpan.Zero);
                Console.WriteLine(utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)));
            */
            if (agencyList.ContainsKey(agencyId)) {
                var tz = TimeZoneInfo.FindSystemTimeZoneById(agencyList[agencyId].TimezoneID);
                if (tz != null)
                    return tz;
            }
            return null;
        }

        public bool SetAgencyTimeZone(string agencyId, string timezoneId) {
            if (agencyList.ContainsKey(agencyId)) {
                var tz = TimeZoneInfo.FindSystemTimeZoneById(agencyList[agencyId].TimezoneID);
                if (tz == null)
                    return false;
                agencyList[agencyId].TimezoneID = timezoneId;
                return true;
            }
            return false;
        }

        public bool SetAgencyAuthTimeout(string agencyId, int timeout) {
            if (agencyList.ContainsKey(agencyId)) {
                agencyList[agencyId].AuthTimeout = timeout;
                return true;
            }
            return false;
        }

        public enum LoggedInStatus {
            AlreadyLoggedIn,
            NotLoggedIn,
            LoggedInOnSameIP,
            AllowableUserMaxedOut,
            AgencyDoesNotExist,
            AgencyInactivated
        }

        class AgencyInfo {
            public string ConnectionString { get; set; }
            public string TimezoneID { get; set; }
            public int AuthTimeout { get; set; }
            // add code kim for use_addres 
            public int? Use_address { get; set; }
        }
    }
}