//to close modal and reset form
function closeModal(id) {
    $("#" + id).modal("hide");
    //resets the inputs inside the <form> tag under the div with id
    $("#" + id + " form").trigger("reset");
    $('#' + id + ' input:checkbox').removeAttr('checked');

}
var SweetAlert2Demo = {
    init: function () {

        //$("#close_referral").click(function (e) {
        //    swal({
        //        title: "Are you sure you want to close without saving your data?",
        //        text: "You won't be able to revert this!",
        //        type: "error",
        //        showCancelButton: !0,
        //        confirmButtonText: "Yes"
        //    }).then(function (e) {
        //        e.value && closeModal("referral_modal")

        //    })
        //});
        //$("#add_call_log_close").click(function (e) {
        //    swal({
        //        title: "Are you sure you want to close without saving your data?",
        //        text: "You won't be able to revert this!",
        //        type: "error",
        //        showCancelButton: !0,
        //        confirmButtonText: "Yes"
        //    }).then(function (e) {
        //        e.value && $('#CallLog').val("");

        //    })
        //});
        //$("#close_assessment").click(function (e) {
        //    swal({
        //        title: "Are you sure you want to close without saving your data?",
        //        text: "You won't be able to revert this!",
        //        type: "error",
        //        showCancelButton: !0,
        //        confirmButtonText: "Yes"
        //    }).then(function (e) {
        //        e.value && closeModal("assessment_modal")

        //    })
        //});
        $(".close1").click(function (e) {
            var id = $(this).attr("modalid");
            //debugger
            if (id == "resident_modal") {
                delete resStateNew['room_facility'];
                delete resStateNew['RMMResidentName'];
                delete resStateNew['txtResidentName'];
                delete resStateNew['DCResidentName'];

                residentState.orig = JSON.stringify($.extend(residentrowState, resStateNew));

                res = $('#residentForm .resForm').extractJson();
                arow1 = $('#admForm #tabs-1a').extractJson('id');
                arow2 = $('#admForm #tabs-2a').extractJson('id');
                arow3 = $('#admForm #tabs-3r').extractJson('id');
                resState2 = $.extend(res, arow1, arow2, arow3);
                delete resState2['room_facility'];
                delete resState2['RMMResidentName'];
                delete resState2['txtResidentName'];
                delete resState2['DCResidentName'];
                residentState.current = JSON.stringify(resState2);

                if (residentState.current == residentState.orig) {
                    closeModal(id);
                    return;
                }
            }

            swal({
                title: "Are you sure you want to close without saving your data?",
                text: "You won't be able to revert this!",
                type: "error",
                showCancelButton: !0,
                confirmButtonText: "Yes"
            }).then(function (e) {
                e.value && closeModal(id)

            })
        })

        //FOR REFERENCE 


        //$("#m_sweetalert_demo_1").click(function (e) {
        //    swal("Good job!")
        //}), $("#m_sweetalert_demo_2").click(function (e) {
        //    swal("Here's the title!", "...and here's the text!")
        //}), $("#m_sweetalert_demo_3_1").click(function (e) {
        //    swal("Good job!", "You clicked the button!", "warning")
        //}), $("#m_sweetalert_demo_3_2").click(function (e) {
        //    swal("Good job!", "You clicked the button!", "error")
        //}), $("#m_sweetalert_demo_3_3").click(function (e) {
        //    swal("Good job!", "You clicked the button!", "success")
        //}), $("#m_sweetalert_demo_3_4").click(function (e) {
        //    swal("Good job!", "You clicked the button!", "info")
        //}), $("#m_sweetalert_demo_3_5").click(function (e) {
        //    swal("Good job!", "You clicked the button!", "question")
        //}), $("#m_sweetalert_demo_4").click(function (e) {
        //    swal({
        //        title: "Good job!",
        //        text: "You clicked the button!",
        //        icon: "success",
        //        confirmButtonText: "Confirm me!",
        //        confirmButtonClass: "btn btn-focus m-btn m-btn--pill m-btn--air"
        //    })
        //}), $("#m_sweetalert_demo_5").click(function (e) {
        //    swal({
        //        title: "Good job!",
        //        text: "You clicked the button!",
        //        icon: "success",
        //        confirmButtonText: "<span><i class='la la-headphones'></i><span>I am game!</span></span>",
        //        confirmButtonClass: "btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon",
        //        showCancelButton: !0,
        //        cancelButtonText: "<span><i class='la la-thumbs-down'></i><span>No, thanks</span></span>",
        //        cancelButtonClass: "btn btn-secondary m-btn m-btn--pill m-btn--icon"
        //    })
        //}), $("#m_sweetalert_demo_6").click(function (e) {
        //    swal({
        //        position: "top-right",
        //        type: "success",
        //        title: "Your work has been saved",
        //        showConfirmButton: !1,
        //        timer: 1500
        //    })
        //}), $("#m_sweetalert_demo_7").click(function (e) {
        //    swal({
        //        title: "jQuery HTML example",
        //        html: $("<div>").addClass("some-class").text("jQuery is everywhere."),
        //        animation: !1,
        //        customClass: "animated tada"
        //    })
        //}), $("#m_sweetalert_demo_8").click(function (e) {
        //    swal({
        //        title: "Are you sure?",
        //        text: "You won't be able to revert this!",
        //        type: "warning",
        //        showCancelButton: !0,
        //        confirmButtonText: "Yes, delete it!"
        //    }).then(function (e) {
        //        e.value && swal("Deleted!", "Your file has been deleted.", "success")
        //    })
        //}), $("#m_sweetalert_demo_9").click(function (e) {
        //    swal({
        //        title: "Are you sure?",
        //        text: "You won't be able to revert this!",
        //        type: "warning",
        //        showCancelButton: !0,
        //        confirmButtonText: "Yes, delete it!",
        //        cancelButtonText: "No, cancel!",
        //        reverseButtons: !0
        //    }).then(function (e) {
        //        e.value ? swal("Deleted!", "Your file has been deleted.", "success") : "cancel" === e.dismiss && swal("Cancelled", "Your imaginary file is safe :)", "error")
        //    })
        //}), $("#m_sweetalert_demo_10").click(function (e) {
        //    swal({
        //        title: "Sweet!",
        //        text: "Modal with a custom image.",
        //        imageUrl: "https://unsplash.it/400/200",
        //        imageWidth: 400,
        //        imageHeight: 200,
        //        imageAlt: "Custom image",
        //        animation: !1
        //    })
        //}), $("#m_sweetalert_demo_11").click(function (e) {
        //    swal({
        //        title: "Auto close alert!",
        //        text: "I will close in 5 seconds.",
        //        timer: 5e3,
        //        onOpen: function () {
        //            swal.showLoading()
        //        }
        //    }).then(function (e) {
        //        "timer" === e.dismiss && console.log("I was closed by the timer")
        //    })
        //})
    }
};


jQuery(document).ready(function () {
    SweetAlert2Demo.init()
});