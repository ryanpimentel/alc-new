USE [rbellinghamnew]
GO
/****** Object:  StoredProcedure [dbo].[getActivityScheduleByStaff]    Script Date: 6/13/2019 1:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[getActivityScheduleByStaff] null, 'dancy', '10/17/2016'
ALTER proc [dbo].[getActivityScheduleByStaff]
	@param int = null,
	@param2 varchar(50) = null,
	@param3 datetime = null
as
begin
	--show all 
	select  b.activity_schedule_id,
			a.activity_id,
			activity_desc,
			a.activity_procedure,
			a.duration,
			resident = case when a.dept_id not in (5,7) and b.resident_id is not null then isnull(c.first_name,'') + ' ' + isnull(c.last_name,'') else null end,
			carestaff = case when b.carestaff_id is not null then isnull(u.first_name,'') + ' ' + isnull(u.last_name,'') else null end,
			rolename = e.[description],
			room = case when a.dept_id in (5,7) and b.room_id is not null then m.room_name else f.room_name end,
			start_time,
			recurrence,
			createdby = isnull(u.first_name,'') + ' ' + isnull(u.last_name,''),
			datecreated = CONVERT(VARCHAR(10),b.date_created,110),
			is_onetime = isnull(is_onetime, 0),
			b.xref,
			b.is_active,
			b.active_until
	from	ActivitySchedule b (nolock)
	join  Activity a (nolock) on a.activity_id = b.activity_id
	left join Resident c (nolock) on b.resident_id = c.resident_id
	left join Admission d (nolock) on c.resident_id = d.resident_id
	left join Room m (nolock) on m.room_id = b.room_id
	left join Room f (nolock) on f.room_id = d.room_id
	left join ALCUser u (nolock) on b.carestaff_id = u.[user_id]
	left join ALCRole e (nolock) on e.role_id = a.dept_id
	where (@param is null or @param = a.dept_id) and (is_onetime is null or is_onetime = 0) and
			(@param2 is null or @param2 = b.carestaff_id)
		  
		  --and (@param2 is null or (',' + RTRIM(recurrence) + ',') LIKE '%,' + @param2 + ',%')
			
	union all

	select  b.activity_schedule_id,
			a.activity_id,
			activity_desc,
			a.activity_procedure,
			a.duration,
			resident = case when a.dept_id not in (5,7) and b.resident_id is not null then isnull(c.first_name,'') + ' ' + isnull(c.last_name,'') else null end,
			carestaff = case when b.carestaff_id is not null then isnull(u.first_name,'') + ' ' + isnull(u.last_name,'') else null end,
			rolename = e.[description],
			room = case when a.dept_id in (5,7) and b.room_id is not null then m.room_name else f.room_name end,
			start_time,
			recurrence,
			createdby = isnull(u.first_name,'') + ' ' + isnull(u.last_name,''),
			datecreated = CONVERT(VARCHAR(10),b.date_created,110),
			is_onetime = isnull(is_onetime, 0),
			b.xref,
			b.is_active,
			b.active_until
	from	ActivitySchedule b (nolock)
	join  Activity a (nolock) on a.activity_id = b.activity_id
	left join Resident c (nolock) on b.resident_id = c.resident_id
	left join Admission d (nolock) on c.resident_id = d.resident_id
	left join Room m (nolock) on m.room_id = b.room_id
	left join Room f (nolock) on f.room_id = d.room_id
	left join ALCUser u (nolock) on b.carestaff_id = u.[user_id]
	left join ALCRole e (nolock) on e.role_id = a.dept_id
	where (@param is null or @param = a.dept_id) and is_onetime = 1 and
			(@param2 is null or @param2 = b.carestaff_id) and
			(@param3 is null or b.start_time >= @param3)
end



