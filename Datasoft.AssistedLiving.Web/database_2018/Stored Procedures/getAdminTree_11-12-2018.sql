USE [AssistedLiving]
GO
/****** Object:  StoredProcedure [dbo].[getAdminTree]    Script Date: 11/12/2018 5:39:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[getAdminTree]
	@role_id int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT id = n.tree_node_id, [name] = n.tree_node_description, parent_id = null, sort_order = n.sort_order, sub_sort_order = null, n.image_url, n.icon_class
	FROM  treenoderole nr
	JOIN  treenode n ON nr.tree_node_id = n.tree_node_id
	WHERE nr.role_id = @role_id AND nr.access_type_id in (1,3) AND 
		n.tree_id = 'ADM' AND (ISNULL(n.[disabled],0) = 0)
	UNION ALL
	SELECT id = s.tree_sub_node_id, [name] = s.tree_sub_node_description, parent_id = n.tree_node_id, sort_order = n.sort_order, sub_sort_order = s.sort_order, s.image_url, s.icon_class
	FROM  treesubnoderole sr
	JOIN  treesubnode s ON sr.tree_sub_node_id = s.tree_sub_node_id
	JOIN treenode n ON s.tree_node_id = n.tree_node_id
	WHERE sr.role_id = @role_id AND sr.access_type_id in (1,3) AND 
		n.tree_id = 'ADM' AND (ISNULL(n.[disabled],0) = 0 AND ISNULL(s.[disabled],0) = 0)
	ORDER BY sort_order, sub_sort_order;
	
END

