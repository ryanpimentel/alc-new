USE [AssistedLiving]
GO
/****** Object:  StoredProcedure [dbo].[getUserList]    Script Date: 2/1/2019 2:46:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[getUserList] 4
ALTER procedure [dbo].[getUserList]
(
	@role_id int = null
)
as
begin
	select		c.[user_id], 
				c.first_name, 
				middle_initial = case when isnull(c.middle_initial,'') = '' or c.middle_initial = ''  then '' else c.middle_initial + '.' end, 
				c.last_name, 
				c.email, 
				roleid = x.role_id, 
				deptid = d.department_id, 
				[role] = y.[description], 
				dept = t.name,
				non_sys_user = isnull(c.non_sys_user,0), 
				c.is_active, 
				c.force_reset_password, 
				c.date_created, 
				c.date_modified,
				shift_start = q.start_time, 
				shift_end = q.end_time, 
				s.dayschedule,
				q.[description],
				created_by = case when q.created_by is null then null else isnull(f.first_name,null) + ' ' + isnull(f.last_name,'') end,
				date_created = s.date_created,
				u.ssn--, c.imagesrc
	from		ALCUser (nolock) c 
	left join	ALCUserRole (nolock) x on c.[user_id] = x.[user_id] and x.is_primary = 1
	left join	ALCRole (nolock) y on y.role_id = x.role_id
	left join	DepartmentUser (nolock) d on d.[user_id] = c.[user_id]
	left join	Department (nolock) t on t.department_id = d.department_id
	left join	Carestaff u on c.[user_id] = u.[user_id]
	left join	CarestaffShift (nolock) s on s.carestaff_id = c.[user_id]
	left join	[Shift] (nolock) q on s.shift_id = q.shift_id
	left join	ALCUser (nolock) f on s.created_by = f.[user_id]
	where	(@role_id is null or @role_id = x.role_id)
	order by c.last_name, c.is_active, c.non_sys_user

end


