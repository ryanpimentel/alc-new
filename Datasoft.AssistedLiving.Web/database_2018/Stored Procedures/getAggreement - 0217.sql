USE [AssistedLiving]
GO
/****** Object:  StoredProcedure [dbo].[getAgreement]    Script Date: 17/02/2019 10:54:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[getAgreement] 9, '2017-07-31'
ALTER procedure [dbo].[getAgreement]
(
	@agreement_id int = null
	--@admission_id int = null
	--@dtc datetime = null
)
as
begin
	SELECT	a.admission_id, 
			a.resident_id,
			a.BSActivity, a.BSBed, a.BSHygiene, a.BSLinens, a.BSMeals, a.BSObservation, a.BSRoom,
			a.BSTransportation, AADOB=b.birth_date,a.OSIniBathing, a.OSIniBathingDef, a.OSIniFood,
			a.OSIniFoodDef, a.OSIniLaundry, a.OSIniLaundryDef, a.OSIniMedication, a.OSIniMedicationDef,
			a.OSIniIncontinent,a.OSIniIncontinentDef, a.OSOtherIncontinent, a.OSPurLaundry, a.OSPurMedication, 
			a.OSPurBathing, a.OSPurFood, a.OSPurIncontinent, resident_name = b.first_name + ' ' + b.last_name,
			a.TotalRate,a.RoomSuite, a.RoomSingle, a.RoomShared, 
						a.AAMonthlyRate,
						a.AAOpServices, 
                        a.AATMRate ,
                        a.AAPeriodRateDate1 ,
                        a.AAPeriodRateDate2 ,
                        a.AAPeriodRate ,
                        a.AAProratedRateDate1 ,
                        a.AAProratedRateDate2 ,
                        a.AAProratedRate ,
                        a.AAOnetimeFee ,
                        a.AAAdvancePay ,
                        a.AATotalDue ,
                        a.AAFSPrivate ,
                        a.AAFSFunds ,
                        a.AAFSRefuse ,
                        a.AAFSInitial 
	FROM	AdmissionAgreement a (NOLOCK)
	LEFT JOIN	Resident b (NOLOCK) on a.resident_id = b.resident_id
	WHERE	@agreement_id is null or a.admission_agreement_id = @agreement_id 
	--and
	--		(@admission_id is null or a.admission_id = @admission_id)
			--a.admission_id = @admission_id and 
			--(@dtc is null or cast(a.date_created as date) = cast(@dtc as date))
end

