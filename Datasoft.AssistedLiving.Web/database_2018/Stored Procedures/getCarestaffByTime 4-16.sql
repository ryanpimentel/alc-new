USE [AssistedLiving]
GO
/****** Object:  StoredProcedure [dbo].[getCarestaffByTime]    Script Date: 4/16/2019 3:27:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [dbo].[getCarestaffByTime] '08:30 AM',18, 'avilla','1,5','05/13/2017',0
-- exec [dbo].[getCarestaffByTime] '10:00 AM',1, 'dancy','2,3'
-- exec [dbo].[getCarestaffByTime] 2, '02:00 PM',4, 'fancy','1,2'
ALTER proc [dbo].[getCarestaffByTime]
(
	@type int = 1,
	@ctime datetime = null,
	@activityid int = null,
	@carestaffid varchar(20) = null,
	@recurrence varchar(20) = null,
	@clientdt datetime = null,
	@isonetime bit = 0
) as
begin
		declare @output table([user_id] varchar(50), carestaff varchar(255))
		--return unfiltered carestaff for force override

		--commented this because when you reassign a task it displays all carestaff including those whose shift are not within the time frame
		--if(@type = 2)
		--begin
		--	insert into @output
		--	select 	distinct a.[user_id], a.first_name + ' ' + a.last_name
		--	from	ALCUser a
		--	left join	ALCUserRole b on a.[user_id] = b.[user_id]
		--	where	is_active = 1 and b.role_id not in (1, 2, 8, 9, 10, 11, 12, 13)
			
		--	select * from @output
		--	return;
		--end

		declare @rt_matches table([user_id] varchar(50), carestaff varchar(255))
		declare @rt_unmatches table([user_id] varchar(50), carestaff varchar(255))
		declare @fn_output table(idx int identity(1,1), [user_id] varchar(50), carestaff varchar(255))
		
		declare @tb table(idx int identity(1,1), val nchar(1))
		declare @cnt int = 1
		declare @totalrows int = 1
		declare @recday nchar(1)

		insert into @tb
		select value from fn_split(@recurrence,',')

		select @totalrows = max(idx) from @tb

		while (@cnt <= @totalrows)
		begin
			select @recday = val from @tb where idx = @cnt
			
			insert into @rt_matches
			select [user_id], carestaff from dbo.fn_GetCarestaffByRecurrence_Matches(@ctime, @activityid, @carestaffid, @recday, @clientdt, @isonetime)
			--select * from @rt_matches
			insert into @rt_unmatches
			select [user_id], carestaff from dbo.fn_GetCarestaffByRecurrence_Unmatches(@ctime, @activityid, @carestaffid, @recday, @clientdt, @isonetime)
			--select * from @rt_unmatches
			set @cnt = @cnt + 1
		end
		
		insert into @fn_output
		select [user_id], carestaff from @rt_unmatches b
		where  not exists(select 1 from @rt_matches x where x.[user_id] = b.[user_id] )
		group by [user_id], carestaff

		set @cnt = 1
		select @totalrows = max(idx) from @fn_output

		declare @uid varchar(50)
		declare @uidd varchar(250)
		declare @days varchar(20)
		while (@cnt <= @totalrows)
		begin
			select @uid = [user_id], @uidd = a.carestaff, @days = b.dayschedule 
			from @fn_output a 
			join CarestaffShift b on a.[user_id] = b.carestaff_id where idx = @cnt;

			if not exists(
				select 1 from @tb a 
				left join (select value from fn_split(@days,',')) b on b.value = a.val
				where b.value is null
			)
			begin
				insert into @output
				select @uid, @uidd
				where not exists (
					select 1 from @output where [user_id] = @uid and carestaff = @uidd
				)
			end

			set @cnt = @cnt + 1
		end

		select * from @output
		-- exec [dbo].[getCarestaffByTime] '10:40 AM',5, 'dancy','1,2,3'
end


