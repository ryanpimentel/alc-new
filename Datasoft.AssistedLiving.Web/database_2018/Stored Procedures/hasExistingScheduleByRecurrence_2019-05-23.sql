USE [BellinghamNew]
GO
/****** Object:  StoredProcedure [dbo].[hasExistingScheduleByRecurrence]    Script Date: 5/23/2019 1:43:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [dbo].[hasExistingScheduleByRecurrence] '10:00 AM', 1,3,1,null,'1,2'
CREATE proc [dbo].[hasExistingScheduleByRecurrence]
(
	@ctime datetime = null,
	@activityid int = null,
	@activityscheduleid int = null,
	@residentid int = null,
	@roomid int = null,
	@recurrence varchar(20) = null,
	@clientdt datetime = null,
	@isonetime bit = 0
) as
begin
		--disable for now. let's allow multiple carestaff can handle a single resident simultaneously at the same time.
		select Conflict = null
		return
		
		declare @rt_matches table(service_time time(7), resident_id int, room_id int, duration int, recurrence varchar(20))
		declare @tb table(idx int identity(1,1), val nchar(1))
		declare @cnt int = 1
		declare @totalrows int = 1
		declare @recday nchar(1)

		insert into @tb
		select value from fn_split(@recurrence,',')

		select @totalrows = max(idx) from @tb

		while (@cnt <= @totalrows)
		begin
			select @recday = val from @tb where idx = @cnt
			
			insert into @rt_matches
			select * from dbo.fn_GetExistingSchedulesByRecurrence(@ctime, @activityid, @activityscheduleid, @residentid, @roomid, @recday, @clientdt, @isonetime) b
			
			set @cnt = @cnt + 1
		end
		declare @recdays varchar(20)
		declare @d table (d char(1))
		declare @x table (idx int identity(1,1), service_time time(7), resident_id int, room_id int, duration int, recurrence varchar(20))
		
		insert into @x
		select service_time, resident_id, room_id, duration, recurrence 
		from @rt_matches
		group by service_time, resident_id, room_id, duration, recurrence 

		set @cnt = 1
		set @totalrows = 1
		select @totalrows = max(idx) from @tb

		while (@cnt <= @totalrows)
		begin
			select @recdays = recurrence from @x where idx = @cnt
			
			insert into @d
			select value from fn_split(@recdays,',')
			set @cnt = @cnt + 1
		end
		declare @conflictdays varchar(500)
		select @conflictdays = COALESCE(@conflictdays + ', ','') + substring(datename(dw, convert(int,d-1) ), 0, 4) from @d
		group by d

		
		select Conflict = @conflictdays
		-- exec [dbo].[hasExistingScheduleByRecurrence]  '10:00 AM', 1,3,1,null,'1,2'
end

