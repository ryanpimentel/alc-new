USE [rbellinghamnew]
GO
/****** Object:  StoredProcedure [dbo].[getActivityScheduleById]    Script Date: 6/21/2019 5:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[getActivityScheduleById] 
(
	@activity_schedule_id int = null
)
as
begin
	select  Id = d.activity_schedule_id,
			ActivityId = a.activity_id,
			Activity = isnull(a.[activity_desc],''),
			ActivityProcedure = a.activity_procedure,
			CarestaffId = d.carestaff_id,
			CarestaffName = c.first_name + ' ' + c.last_name,
			ResidentId = case when a.dept_id not in (5,7) and d.resident_id is not null then d.resident_id else null end,
			ResidentName = case when a.dept_id not in (5,7) and d.resident_id is not null then isnull(b.first_name,'') + ' ' + isnull(b.last_name,'') else null end,
			RoomId = case when a.dept_id in (5,7) and d.room_id is not null then d.room_id else m.room_id end,
			RoomName = case when a.dept_id in (5,7) and d.room_id is not null then e.room_name else f.room_name end,
			ScheduleTime = d.start_time,
			Recurrence = isnull(d.recurrence,''),
			Dept = a.dept_id,
			IsOneTime = isnull(is_onetime,0)
	from ActivitySchedule d
	left join Activity a on a.activity_id = d.activity_id
	left join Resident b on d.resident_id = b.resident_id
	left join Admission m on b.resident_id = m.resident_id
	left join Room e on d.room_id = e.room_id
	left join Room f on m.room_id = f.room_id
	left join ALCUser c on c.[user_id] = d.carestaff_id
	where d.activity_schedule_id = @activity_schedule_id
end
