GO

INSERT INTO [dbo].[TreeNodeRole]
           ([role_id]
           ,[tree_node_id]
           ,[access_type_id]
           ,[date_modified]
           ,[modified_by])
     VALUES (15,'ADMAdministrative', 3, CURRENT_TIMESTAMP, 'system'),
			(15,'ADMAssessment', 3, CURRENT_TIMESTAMP, 'system'),
			(15,'ADMFacilitiesRPT', 3, CURRENT_TIMESTAMP, 'system'),
			(15,'ADMGeneral', 3, CURRENT_TIMESTAMP, 'system'),
			(15,'ADMMedication', 3, CURRENT_TIMESTAMP, 'system'),
			(15,'ADMPOS', 3, CURRENT_TIMESTAMP, 'system'),
			(15,'ADMSecurity', 3, CURRENT_TIMESTAMP, 'system')	
GO




