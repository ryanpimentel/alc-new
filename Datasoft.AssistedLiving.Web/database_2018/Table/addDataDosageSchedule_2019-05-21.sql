/* EXECUTE THIS ONLY IF DosageSchedule table is empty. This is the cause of error in MAR  */

USE [BellinghamNew] /* CHANGE DB NAME */
GO
SET IDENTITY_INSERT [dbo].[DosageSchedule] ON 

GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (1, 1, CAST(N'08:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (2, 2, CAST(N'08:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (3, 2, CAST(N'16:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (4, 3, CAST(N'08:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (5, 3, CAST(N'16:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (6, 3, CAST(N'20:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (7, 4, CAST(N'08:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (8, 4, CAST(N'12:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (9, 4, CAST(N'16:00:00' AS Time))
GO
INSERT [dbo].[DosageSchedule] ([dosage_schedule_id], [dosage_id], [schedule_time]) VALUES (10, 4, CAST(N'20:00:00' AS Time))
GO
SET IDENTITY_INSERT [dbo].[DosageSchedule] OFF
GO
