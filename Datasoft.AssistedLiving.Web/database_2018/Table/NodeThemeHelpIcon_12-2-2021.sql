USE [AssistedLiving]
GO
INSERT [dbo].[TreeNode] ([tree_node_id], [tree_id], [tree_node_name], [tree_node_description], [image_url], [sort_order], [disabled], [icon_class]) 
VALUES (N'ADMHelp', N'ADM', N'Help', N'Help', N'security.jpg', 8, NULL, N'flaticon-questions-circular-button')

INSERT [dbo].[TreeNodeRole] ([role_id], [tree_node_id], [access_type_id], [date_modified], [modified_by]) 
VALUES (1, N'ADMHelp', 3, CAST(N'2017-04-17 21:25:32.100' AS DateTime), N'system')

INSERT [dbo].[TreeSubNode] ([tree_sub_node_id], [tree_node_id], [parent_sub_node_id], [tree_sub_node_name], [tree_sub_node_description], [image_url], [sort_order], [disabled], [icon_class]) 
VALUES (N'ADMHelp', N'ADMHelp', NULL, N'Help', N'Help', N'users.png', 1, NULL, N'flaticon-questions-circular-button')

INSERT [dbo].[TreeSubNodeRole] ([role_id], [tree_sub_node_id], [access_type_id], [date_modified], [modified_by]) 
VALUES (1, N'ADMHelp', 3, CAST(N'2018-03-28 09:29:14.947' AS DateTime), N'system')