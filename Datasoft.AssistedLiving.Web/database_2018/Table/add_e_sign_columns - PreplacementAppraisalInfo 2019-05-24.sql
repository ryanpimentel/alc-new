alter table PreplacementAppraisalInfo
add isSigned bit null, SignedBy varchar(50) null, SignedDate datetime null, ESignature image null
go
