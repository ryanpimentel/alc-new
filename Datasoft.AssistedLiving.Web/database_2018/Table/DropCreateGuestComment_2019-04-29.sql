USE [AssistedLiving]
GO

/****** Object:  Table [dbo].[GuestComment]    Script Date: 4/29/2019 2:33:31 PM ******/
DROP TABLE [dbo].[GuestComment]
GO

/****** Object:  Table [dbo].[GuestComment]    Script Date: 4/29/2019 2:33:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GuestComment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[guest_notification_id] [int] NULL,
	[resident_id] [int] NULL,
	[cid] [varchar](50) NULL,
	[comment] [nvarchar](max) NULL,
	[date_created] [datetime] NOT NULL DEFAULT (getdate()),
	[read_status] [varchar](20) NULL,
	[unread_count] [smallint] NULL DEFAULT ((0)),
	[taskdetail] [varchar](1000) NULL,
	[guest_read_status] [varchar](20) NULL,
 CONSTRAINT [PK_GuestComment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


