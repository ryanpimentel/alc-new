
UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-cogwheel'
 WHERE tree_sub_node_id = 'ADMAssessmentPointSettings'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-settings'
 WHERE tree_sub_node_id = 'ADMCarestaffAdmission'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-folder'
 WHERE tree_sub_node_id = 'ADMCSR'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-edit-1'
 WHERE tree_sub_node_id = 'ADMDailyMeds'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-settings'
 WHERE tree_sub_node_id = 'ADMDailyMedsReport'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-squares-3'
 WHERE tree_sub_node_id = 'ADMDepartments'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-interface-3'
 WHERE tree_sub_node_id = 'ADMFacilityConfigurations'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'fas fa-home'
 WHERE tree_sub_node_id = 'ADMFacilities'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-tabs'
 WHERE tree_sub_node_id = 'ADMHome'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-calendar-1'
 WHERE tree_sub_node_id = 'ADMMAR'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-app'
 WHERE tree_sub_node_id = 'ADMMarketing'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-user-ok'
 WHERE tree_sub_node_id = 'ADMPermissions'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-list-3'
 WHERE tree_sub_node_id = 'ADMReceiveMeds'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-users'
 WHERE tree_sub_node_id = 'ADMResidents'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-profile'
 WHERE tree_sub_node_id = 'ADMRoles'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-squares'
 WHERE tree_sub_node_id = 'ADMRooms'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-clock'
 WHERE tree_sub_node_id = 'ADMServices'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-settings'
 WHERE tree_sub_node_id = 'ADMSettings'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-time'
 WHERE tree_sub_node_id = 'ADMShifts'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-suitcase'
 WHERE tree_sub_node_id = 'ADMSupplies'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-file'
 WHERE tree_sub_node_id = 'ADMTranssched'	
GO

UPDATE [dbo].[TreeSubNode]
   SET icon_class = 'flaticon-users'
 WHERE tree_sub_node_id = 'ADMUsers'	
GO


































