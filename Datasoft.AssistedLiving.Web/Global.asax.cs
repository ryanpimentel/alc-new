﻿using System;

using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Security.Principal;
using System.Web.Security;
using System.Web.SessionState;
using Datasoft.AssistedLiving.Security;
using System.Net.Mail;

namespace Datasoft.AssistedLiving.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e) {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null) {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (authTicket != null && !authTicket.Expired) {
                    if (FormsAuthentication.SlidingExpiration)
                        authTicket = FormsAuthentication.RenewTicketIfOld(authTicket);
                }

                FormsIdentity userIdentity = new FormsIdentity(authTicket);
                string[] roles = authTicket.UserData.Split(new char[] { '|' });

                IPrincipal user = new ALCPrincipal(userIdentity, roles);
                Context.User = user;
            }
        }

        //error logging
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();

            if (exc != null)
            {
                

                MailMessage mail = new MailMessage();

                //TO BE MODIFIED
                mail.To.Add("ryan.pimentel@datasoftlogic.com, mariel.pulmones@datasoftlogic.com, alona.cabrias@datasoftlogic.com, angelie.alasian@datasoftlogic.com, ignacio.taladua@datasoftlogic.com");

                mail.From = new MailAddress("dslalc@datasoftlogic.com");
                mail.Subject = "ERROR FOUND";

                mail.Body = Convert.ToString(DateTime.UtcNow);
                mail.Body += "<br><br>" + exc;

                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Credentials = new System.Net.NetworkCredential
                     ("dslalc@datasoftlogic.com", "dslalciloilo17");
                smtp.Port = 587;

                smtp.EnableSsl = true;
                smtp.Send(mail);

                //Server.Transfer("~/Error", true);
                Context.RewritePath("~/Error");
            }
        }
    }
}
