﻿$(document).ready(function () {   
    var datenow = ALC_Util.CurrentDate;

    //set initial values for loading default datatable (services within the day)
    var ftrs = {
        date1: moment(new Date()).format("MM/DD/YYYY"),
        residentId: 0,
        userId: 'all',
        dept: 'all',
        room: 'all'
    };

    $("#ftrDate1, #ftrDate2").val(datenow);
    console.log(ALC_Util.CurrentDate);
    $('#ftrDate1, #ftrDate2').datepicker({
        dateFormat: 'mm-dd-yyyy',
        orientation: "bottom",
        todayHighlight: true,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        autoclose: true
    });

    var init = function (filters) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "ADmin/GetGridData?func=rendered_services_report&param=" + filters,
            dataType: "json",
            async: false,
            success: function (m) {

                result = m
                //schedule_task = m;
                //console.log(m);
                generateTable(m);
                console.log(m);
            }
        });
    };

    function generateTable(res) {
        var html_table = "";
        if (res.length != 0) {
            $.each(res, function (i, v) {

                html_table += "<tr>";
                html_table += "<td>" + v.completion_date + "</td>";
                html_table += "<td>" + v.start + "</td>";
                html_table += "<td>" + v.resident + "</td>";
                html_table += "<td>" + v.room + "</td>";
                html_table += "<td>" + v.activity_desc + "</td>";
                html_table += "<td>" + v.carestaff2 + "</td>";
                html_table += "<td>" + v.actual_completiondt + "</td>";
                html_table += "</tr>";
            })
        }

        $("#ServicesDataTable").DataTable().destroy();
        $("#ServicesDataTable_body").html('');
        $("#ServicesDataTable_body").html(html_table);
        $("#ServicesDataTable").DataTable({
            stateSave: true,
            //  "scrollX": "true"
            "scrollCollapse": true,
            select: true,
            responsive: !0,
            "ordering": false,
            "searching": false
        });
        $(".loader").hide();
    }

    $(".loader").show();
    $(init(JSON.stringify(ftrs)));

    function unique(array) {
        return $.grep(array, function (el, index) {
            return index == $.inArray(el, array);
        });
    }

    $('#ftrResident,#ftrCarestaff,#ftrDept,#ftrRoom,#ftrDate1, #ftrDate2').change(function () {
        $(".loader").show();
        var dt1 = $('#ftrDate1').val();
        var dt2 = $('#ftrDate2').val();
        var res = $('#ftrResident :selected').val();
        var sta = $('#ftrCarestaff :selected').text();
        var dep = $('#ftrDept :selected').text();
        var room = $('#ftrRoom :selected').text();

        if (dt1 == '' || dt2 == '') {
            swal("Fill in all filter inputs!", "Start and End Date cannot be empty.", "warning");
            $(".loader").hide();
            return false;
        }

        if (res != 0) ftrs.residentId = res;
        else ftrs.residentId = 0;

        if (sta != 'All') ftrs.userId = sta;
        else ftrs.userId = "all";

        if (dep != 'All') ftrs.dept = dep;
        else ftrs.dept = "all";

        if (room != 'All') ftrs.room = room;
        else ftrs.room = "all";
        

        if (dt1 != dt2) {
            dateRangeServices(ftrs, dt1, dt2)
        } else {
            if (dt1 != datenow) ftrs.date1 = dt1;
            else ftrs.date1 = datenow;
            init(JSON.stringify(ftrs));
        }
        
    });

    function dateRangeServices(filters, date1, date2) {
        var results = [];
        dateArray = getDates(date1, date2);
        $.each(dateArray, function (i, item) {           
            filters.date1 = item;
            var fltrs = JSON.stringify(filters);
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ADmin/GetGridData?func=rendered_services_report&param=" + fltrs,
                dataType: "json",
                async: false,
                success: function (m) {
                    if (m.length != 0) {
                        $.each(m, function (i, item) {
                            results.push(item);
                        })    
                    }                   
                }
            });
        });
        console.log(results)
        generateTable(results);
    }

    
    function getDates(startDate, stopDate) {
        var dateArray = [];
        var currentDate = moment(startDate);
        var stopDate = moment(stopDate);
        while (currentDate <= stopDate) {
            dateArray.push(moment(currentDate).format('MM/DD/YYYY'))
            currentDate = moment(currentDate).add(1, 'days');
        }
        return dateArray;
    }

});

