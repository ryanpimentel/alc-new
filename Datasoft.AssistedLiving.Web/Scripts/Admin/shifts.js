﻿var shiftState = {
    orig: '',
    current: ''
};
function setHeight() {
    $('.dataTables_scrollBody').height($(window).height() - 355);
    if ($(window).width() < 1024) {
        //$('#headerRoom').height($("#headerRoom .row").height() + 100);
        $('#shiftsBody').height($("#shiftsBody .m-portlet").height() + 100);
    } else {
        $('#shiftsBody').height($(window).height() - 140);
    }

    if ($(window).height() < 400) {
        $('.dataTables_scrollBody').height(200);
        $('#shiftsBody').height($('#shiftsGrid').height() + 150);
    }

    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('#shiftsBody').height($("#shiftsBody .m-portlet").height() + 100);
        } else {
            $('#shiftsBody').height($(window).height() - 140);
            $('.dataTables_scrollBody').height($(window).height() - 355);
        }

        if ($(window).height() < 400) {
            $('.dataTables_scrollBody').height(200);
            $('#shiftsBody').height($('#shiftsGrid').height() + 150);
        }
        $('#shift_datatable').DataTable().columns.adjust();
    })

    var scroll = new PerfectScrollbar('.dataTables_scrollBody');
    $('#shift_datatable').DataTable().columns.adjust();
}
var load_shift = function () {

    ftr = $("#ftrStatus").val();



    $.ajax({
        url: "Admin/GetGridData?func=shift&param=" + ftr,
        processData: false,
        dataType: "json",
        type: 'POST',
        cache: false,
        success: function (data, textStatus, jqXHR) {


            var html = "";
            $.each(data, function (i, v) {
                update_details = "";
                //kim add status for shifts 06/08/22
                var status = v.IsActive == 1 ? '<span class="m-badge  m-badge--success m-badge--wide">Active</span>' : '<span class="m-badge m-badge--danger m-badge--wide">Inactive</span>';

                html += "<tr id=\"" + v.Id + "\" desc=\"" + v.Description + "\" stime=\"" + v.StartTime + "\" etime=\"" + v.EndTime + "\" >";
                html += "<td>" + v.Description + "</td>";
                html += "<td>" + v.StartTime + "</td>";
                html += "<td>" + v.EndTime + "</td>";
                html += "<td>" + v.CreatedBy + "</td>";
                html += "<td>" + v.DateCreated + "</td>";
                if (v.IsActive == 0) {
                    if (v.UpdateDetails == null) {
                        html += "<td>" + v.DeletedBy + " - " + v.DateDeleted + "&nbsp;&nbsp;&nbsp;" + "<span class=\"m-badge m-badge--danger m-badge--wide\">Deleted</span></td>";

                    }
                    else {
                        html += "<td>" + (v.UpdateDetails == null ? "N/A" : v.UpdatedBy + " - " + v.DateUpdated + "<a is_active=\"" + v.IsActive + "\" deleted_by=\"" + v.DeletedBy + "\"  date_deleted =\"" + v.DateDeleted + "\" edit_details=\"" + v.UpdateDetails + "\"data-target='#viewShiftEditDetails' data-toggle='modal' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill view_edit_details' aria-expanded='true'><i class='la la-external-link-square'></i></a>") + "<span class=\"m-badge m-badge--danger m-badge--wide\">Deleted</span></td>";

                    }


                } else {
                    html += "<td>" + (v.UpdateDetails == null ? "N/A" : v.UpdatedBy + " - " + v.DateUpdated + "<a is_active=\"" + v.IsActive + "\" deleted_by=\"" + v.DeletedBy + "\"  date_deleted =\"" + v.DateDeleted + "\" edit_details=\"" + v.UpdateDetails + "\"data-target='#viewShiftEditDetails' data-toggle='modal' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill view_edit_details' aria-expanded='true'><i class='la la-external-link-square'></i></a>") + "</td>";
                }

                html += "<td  id='" + v.Id + "'>" + status + "</td>";
                html += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a data-target='#viewShift' data-toggle='modal' class='dropdown-item edit_shift' id='" + v.Id + "' href='#'><i class='la la-edit'></i> Edit</a>\n<a class='dropdown-item del delete' href='#' id='" + v.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                html += "</tr>";


            })
            $("#shift_table").html("");
            $("#shift_datatable").DataTable().destroy();
            $("#shift_table").html(html);

            createDatatable();

        }
    });

}

var createDatatable = function () {

    $("#shift_datatable").DataTable({
        responsive: !0,
        pagingType: "full_numbers",
        select: true,
        "scrollX": true,
        "scrollY": "500px",
        "scrollCollapse": true
    });

    setHeight();
    setTimeout(function () { $(".loader").hide(); }, 500);
}
$(document).ready(function () {

    $(".loader").show();
    $('.time').timepicker({
        'timeFormat': 'H:i',
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true
    });
    var mode = 0;

    $(document).on("click", ".edit_shift", function () {
        $(".save").attr("disabled", false);
        mode = 2;
        var id = $(this).closest("tr").attr("id");
        var desc = $(this).closest("tr").attr("desc");
        var stime = $(this).closest("tr").attr("stime");
        var etime = $(this).closest("tr").attr("etime");
        $("#Id").val(id);
        $("#txtDescription").val(desc);
        $("#txtStartTime").val(stime);
        $("#txtEndTime").val(etime);

        $("#viewShift").modal("toggle");
        //$(".save").val(1);
        shiftState.orig = JSON.stringify($('#shiftmodal_form').extractJson('name'));
    });


    $(document).on("click", ".save", function () {
        $(this).attr("disabled", true);
        if ($("#txtDescription").val() == "" || $("#txtStartTime").val() == "" || $("#txtEndTime").val() == "") {
            swal("Enter value for all fields.");
            return;
        }
        var row = $('#shiftmodal_form').extractJson('name');
        shiftState.current = JSON.stringify(row);

        if (shiftState.current == shiftState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }
        // Cheche was here to restrict duplicate shift description 1/5/2022
        doAjax(mode, row, function (m) {
            if (m.result == 0) {
                swal("Shift " + (mode != 2 ? "added " : "updated ") + "successfully!", "", "success");
                // $("#shift_datatable").DataTable().destroy();
            }
            else if (m.result == -1) {
                swal("Shift description already exists!", "", "info");
                return false;
            }
            $(".cancel").click();
            load_shift();
        });
    });

    $(document).on("click", "#addShift", function () {

        $("#viewShift").modal("toggle");
        $(".save").attr("disabled", false);
        // $(".save").val(1);
        mode = 1;
        shiftState.orig = JSON.stringify($('#shiftmodal_form').extractJson('name'));
    });

    $(document).on("click", ".delete", function () {

        var id = $(this).closest("tr").attr("id");
        //Change code for delete function is still in use kim 12/23/2021 
        //doAjax (3, { Id: id }, function (m) {

        //});
        swal({
            title: "Are you sure you want to delete this data?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes"
        }).then(function (e) {
        e.value && doAjax(3, { Id: id }, function (m) {
            if (m.result != -2) {
                swal("Shift deleted successfully!");
                $("#shift_datatable").DataTable().destroy();
                load_shift();
            }
            if (m.result == -2) {
                swal("Shift cannot be deleted because it is currently in use.");
                load_shift();
            }
            load_shift();
            //if (m.result == -3) {
            //    var msg = "Shift deleted successfully!";
            //    $("#shift_datatable").DataTable().destroy();
            //}
            //swal(m);
            // if (m.result == success) {
            //    //inuse
            //    msg = "Shift cannot be deleted because it is currently in use.";
            //}
            //if (m.result != -1 || -3) {
            //    load_shift();
            //}
            //else {
            //    var msg = "Shift deleted successfully!";
            //    $("#shift_datatable").DataTable().destroy();
            //}
            //toastr.success(msg);



            //if (m.result == -3 ) {
            //    var msg = "Shift cannot be deleted because it is currently in use.";
            //}
            //if (m.result != -1 || -3) {
            //    //if (m.result != -1 || -3) {
            //        load_shift();                  
            //        toastr.success("Shift deleted successfully!");
            //        $("#shift_datatable").DataTable().destroy();
            //    }
            //toastr.success(msg);
            //load_shift();  
        });
        });


    });

    $(document).on("click", ".view_edit_details", function () {

        is_active = $(this).attr("is_active");
        deleted_by = $(this).attr("deleted_by");
        date_deleted = $(this).attr("date_deleted");
        details = $(this).attr("edit_details");
        desc = "";
        start = "";
        end = "";

        split_detail = details.split("+");
        var html = "";
        //kim filter null values 07/07/22
        filterNullValues = split_detail.filter(obj => obj);

        for (i = 0; i <= filterNullValues.length - 1; i++) {

            per_detail = filterNullValues[i].split("//");

            desc = per_detail[0];
            start = per_detail[1];
            end = per_detail[2];

            html += "<tr>";
            html += "<td>" + per_detail[0] + "</td>";
            html += "<td>" + per_detail[1] + "</td>";
            html += "<td>" + per_detail[2] + "</td>";
            html += "<td>" + per_detail[3] + "</td>";
            html += "<td>" + per_detail[4] + "</td>";
            html += "</tr>";

        }

        if (is_active == 0) {
            html += "<tr>";
            html += "<td>" + desc + "</td>";
            html += "<td>" + start + "</td>";
            html += "<td>" + end + "</td>";
            html += "<td>" + (deleted_by == "null" ? "" : deleted_by) + "</td>";
            html += "<td>" + (date_deleted == "null" ? "" : date_deleted) + "</td>";
            html += "</tr>";
        }


        $("#shiftEditDetails_table").html("");
        $("#shiftEditDetails_datatable").DataTable().destroy();
        $("#shiftEditDetails_table").html(html);

        $("#shiftEditDetails_datatable").DataTable({
            responsive: !0,
            pagingType: "full_numbers",
            select: true,
            "scrollX": true,
            "scrollY": "500px",
            "scrollCollapse": true
        });

    });

    $('#ftrStatus').change(function () {
        load_shift();
    });


    load_shift();

});

var doAjax = function (mode, row, cb) {
    //get record by id
    if (mode == 0) {
        var data = { func: "shift", id: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
        return;
    }

    var isValid = $('#shiftmodal_form').validate();
    //do other modes
    if (mode == 1 || mode == 2) {

        if (!isValid) {
            // swal("Fill in all required fields.")
            return;
        }

        row = $('#shiftmodal_form').extractJson();
    }
    var data = { func: "shift", mode: mode, data: row };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/PostGridData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);
        }
    });
}






//$("#shiftContent").layout({
//    center: {
//        paneSelector: "#shiftGrid",
//        closable: false,
//        slidable: false,
//        resizable: true,
//        spacing_open: 0
//    },
//    north: {
//        paneSelector: "#wrapToolbar",
//        closable: false,
//        slidable: false,
//        resizable: false,
//        spacing_open: 0
//        , north__childOptions: {
//            paneSelector: "#shiftToolbar",
//            closable: false,
//            slidable: false,
//            resizable: false,
//            spacing_open: 0
//        },
//        center__childOptions: {
//            paneSelector: "#raToolbar",
//            closable: false,
//            slidable: false,
//            resizable: false,
//            spacing_open: 0
//        }
//    },
//    onresize: function () {
//        resizeGrids();
//    }
//});

//var resizeGrids = (function () {
//    var f = function () {
//        $('.ui-jqgrid-bdiv').eq(0).height($("#shiftGrid").height() - 25).width($("#shiftGrid").width());
//        $('#grid').setGridWidth($("#shiftGrid").width(), true);
//    }
//    setTimeout(f, 100);
//    return f;
//})();

//$('#ddlFilter').change(function () {
//    $('#ftrText').val('');
//});
//$('#ftrText').change(function () {
//    var res = $('#ftrText').val();
//    var rm = $('#ddlFilter').val();
//    var rd = $('#ddlFilter :selected').text();
//    //var ia = $('#isActive').is(':checked');
//    var ftrs = {
//        groupOp: "AND",
//        rules: []
//    };
//    var col = 'Description';
//    if (rm == 2)
//        col = "StartTime";
//    else if (rm == 3)
//        col = "EndTime";

//    //if (ia)
//    //    ftrs.rules.push({ field: 'Status', op: 'eq', data: ia });
//    if (res != '')
//        ftrs.rules.push({ field: col, op: 'cn', data: res });


//    $("#grid")[0].p.search = ftrs.rules.length > 0;
//    $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
//    $("#grid").trigger("reloadGrid");
//});

//$('#txtStartTime,#txtEndTime').timepicker({ showPeriod: true, showLeadingZero: true });
//$('#txtStartTime,#txtEndTime').mask('99:99 xy');
//$('#txtStartTime,#txtEndTime').blur(function (e) {
//    var dis = $(this);

//    var sp = dis.val().split(':');
//    if (sp.length == 1) {
//        if (sp[0] > 23) dis.val('00:00');
//    } else if (sp.length > 1) {
//        if (sp[0] > 23) dis.val(('00' + ':' + sp[1]));
//        if (sp[1] > 59) dis.val((sp[0] + ':' + '00'));
//        if (sp[1] > 59 && sp[0] > 23) dis.val('00:00');
//    }

//});


//conflict in js
//$("#diag").dialog({
//    zIndex: 0,
//    autoOpen: false,
//    modal: true,
//    title: "Shift",
//    dialogClass: "calendarVisitDialog",
//    open: function () {
//    }
//});


//conflict in js
//var showdiag = function (mode) {
//    $("#diag").dialog("option", {
//        width: 480,
//        height: 180,
//        position: "center",
//        buttons: {
//            "Save": {
//                click: function () {
//                    var id = $('#grid').jqGrid('getGridParam', 'selrow');
//                    row = $('#grid').jqGrid('getRowData', id);
//                    doAjax(mode, row, function (m) {
//                        if (m.result == 0)
//                            $.growlSuccess({ message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
//                        $("#diag").dialog("close");
//                        if (mode == 1) isReloaded = true;
//                        else {
//                            fromSort = true;
//                            new Grid_Util.Row().saveSelection.call($('#grid')[0]);
//                        }
//                        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
//                    });
//                },
//                class: "primaryBtn",
//                text: "Save"
//            },
//            "Cancel": function () {
//                $("#diag").dialog("close");
//            }
//        }
//    }).dialog("open");
//}

//$('.toolbar').on('click', 'a', function () {
//    var q = $(this);
//    if (q.is('.add')) {
//        $('.form').clearFields();
//        showdiag(1);
//    } else if (q.is('.del')) {
//        var id = $('#grid').jqGrid('getGridParam', 'selrow');
//        if (id) {
//            if (confirm('Are you sure you want to delete the selected row?')) {
//                setTimeout(function () {
//                    var row = $('#grid').jqGrid('getRowData', id);
//                    if (row) {
//                        doAjax(3, { Id: row.Id }, function (m) {
//                            var msg = "Shift deleted successfully!";
//                            if (m.result == -3) //inuse
//                                msg = "Shift cannot be deleted because it is currently in use.";
//                            $.growlSuccess({ message: msg, delay: 6000 });
//                            //if no error or not in use then reload grid
//                            if (m.result != -1 || m.result != -3)
//                                $('#grid').trigger('reloadGrid');
//                        });
//                    }
//                }, 100);
//            }
//        } else {
//            alert("Please select row to delete.")
//        }
//    } else if (q.is('.unsign')) {
//        var id = $('#grid').jqGrid('getGridParam', 'selrow');
//        if (id) {
//            var row = $('#grid').jqGrid('getRowData', id);
//            if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
//                setTimeout(function () {
//                    if (row) {
//                        doAjax(4, { Id: row.Id }, function (m) {
//                            if (m.result == 0)
//                                $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
//                        });
//                    }
//                }, 100);
//            }
//        } else {
//            alert("Please select row to deactivate.")
//        }
//    }

//});

//(function () {
//    //var dataArray = [
//    //    { Id: '1', Name: 'Suite 101', Description: 'Meeting facility', Level: 'First', Rate: '999.20' },
//    //    { Id: '2', Name: 'Fancy 201', Description: 'Lounge', Level: 'Second', Rate: '534.20' },
//    //    { Id: '3', Name: 'Lancy 900', Description: '', Level: 'First', Rate: '923.20' },
//    //    { Id: '4', Name: 'Hancy 123', Description: '', Level: 'Third', Rate: '912.20' }

//    //];
//    //this is in site.layout.js.
//    var GURow = new Grid_Util.Row();

//    $('#grid').jqGrid({
//        url: "Admin/GetGridData?func=shift&param=",
//        datatype: 'json',
//        postData: "",
//        ajaxGridOptions: { contentType: "application/json", cache: false },
//        //datatype: 'local',
//        //data: dataArray,
//        //mtype: 'Get',
//        colNames: ['Id', 'Description', 'Start Time', 'End Time', 'Created By', 'Date Created'],
//        colModel: [
//          { key: true, hidden: true, name: 'Id', index: 'Id' },
//          {
//              key: false, name: 'Description', index: 'Description', formatter: "dynamicLink", formatoptions: {
//                  onClick: function (rowid, irow, icol, celltext, e) {
//                      var G = $('#grid');
//                      G.setSelection(rowid, false);
//                      var row = G.jqGrid('getRowData', rowid);
//                      if (row) {
//                          doAjax(0, row, function (res) {
//                              $(".form").clearFields();
//                              $(".form").mapJson(res);
//                              showdiag(2);
//                          });
//                      }
//                  }
//              }
//          },
//          { key: false, name: 'StartTime', index: 'StartTime' },
//          { key: false, name: 'EndTime', index: 'EndTime' },
//          { key: false, name: 'CreatedBy', index: 'CreatedBy' },
//          { key: false, name: 'DateCreated', index: 'DateCreated' }
//        ],
//        //pager: jQuery('#pager'),
//        rowNum: 1000000,
//        rowList: [10, 20, 30, 40],
//        height: '100%',
//        viewrecords: true,
//        //caption: 'Care Staff',
//        emptyrecords: 'No records to display',
//        jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
//        autowidth: true,
//        multiselect: false,
//        sortname: 'Id',
//        sortorder: 'asc',
//        loadonce: true,
//        onSortCol: function () {
//            fromSort = true;
//            GURow.saveSelection.call(this);
//        },
//        loadComplete: function (data) {
//            if (typeof fromSort != 'undefined') {
//                GURow.restoreSelection.call(this);
//                delete fromSort;
//                return;
//            }

//            var maxId = data && data[0] ? data[0].Id : 0;
//            if (typeof isReloaded != 'undefined') {
//                $.each(data, function (k, d) {
//                    if (d.Id > maxId) maxId = d.Id;
//                });
//            }
//            if (maxId > 0)
//                $("#grid").setSelection(maxId);
//            delete isReloaded;
//        }
//    });
//})();
//$('#selt').chosen({ no_results_text: 'No results found!' })
//$("#grid").jqGrid('bindKeys');

    //Added for Exporting to Grid to Excel (-ABC)
    //function fnExcelReport() {
    //    $("td:hidden,th:hidden", "#grid").remove();
    //    var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Description</td><td bgcolor='#87AFC6'>Start Time</td><td bgcolor='#87AFC6'>End Time</td><td bgcolor='#87AFC6'>Created by</td><td bgcolor='#87AFC6'>Date Created</td> </tr><tr>";
    //    var textRange; var j = 0;
    //    tab = document.getElementById('grid'); // id of table


    //    for (j = 0 ; j < tab.rows.length ; j++) {
    //        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
    //        //tab_text=tab_text+"</tr>";
    //    }

    //    tab_text = tab_text + "</table>";
    //    tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
    //    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    //    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    //    var ua = window.navigator.userAgent;
    //    var msie = ua.indexOf("MSIE ");
    //    var a = document.createElement('a');

    //    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    //    {
    //        txtArea1.document.open("txt/html", "replace");
    //        txtArea1.document.write(tab_text);
    //        txtArea1.document.close();
    //        txtArea1.focus();
    //        sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Shifts.xls");
    //    }
    //    else                 //other browser not tested on IE 11
    //        a.id = 'ExcelDL';
    //        a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
    //        a.download = 'ALC_Shifts' + '.xls';
    //        document.body.appendChild(a);
    //        a.click(); // Downloads the excel document
    //        document.getElementById('ExcelDL').remove();
    //}

    //$('#lnkExportExcel').on('click', function () {
    //    //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
    //    fnExcelReport();
    //    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    //});
    //===========================

