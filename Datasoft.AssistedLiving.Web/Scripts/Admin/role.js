﻿var roleState = {
    orig: '',
    current: ''
};
$(function () {
    $(".loader").show();
})
var id;
var load_roles = function () {

    
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=role&param=",
        dataType: "json",
        async: false,
        success: function (d) {
            doc_html = " ";
            $.each(d, function (i, doc) {
                
                doc_html += "<tr class='tr' id='" + doc.Id + "' name='" + doc.Description + "' ><td><a href='#' sysdefine='" + doc.SysDefined +"' class='rname' id='" + doc.Id + "' data-target='#role_modal' data-toggle='modal' desc='" + doc.Description + "'>" + doc.Description + "</a></td>";
                doc_html += "<td>" + (doc.Responsibility == null ? "" : doc.Responsibility) + "</td>";
                doc_html += "<td>" + (doc.SysDefined == true? "Yes": "No") + "</td>";
                doc_html += "<td>" + doc.DateCreated + "</td>";
                doc_html += "<td>" + doc.DateModified + "</td>";
                doc_html += "<td><a class='dropdown-item del doc_delete' href='#' id='" + doc.Id + "'><i class='la la-trash-o'></i></a></td>";
                doc_html += "</tr>";
                //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";

            })
            $("#roles").DataTable().destroy();
            $("#roles_btable").html(doc_html);
            $("#roles").DataTable({ "pageLength": 5, "scrollX": "true", "scrollCollapse": true, select: true,});



        }


    });
}

var loadRoleDetails = function (id) {
    
    $.ajax({
        url: "Admin/GetGridData?func=user_role&param=" + id,
        datatype: 'json',
        type: "GET",
        success: function (m) {

            var html_table = "";
           // if (m.length != 0) {
                $.each(m, function (i, doc) {
                    var status = doc.IsActive  == 1 ? '<span class="m-badge  m-badge--success m-badge--wide">Active</span>' : '<span class="m-badge m-badge--danger m-badge--wide">Inactive</span>';
                    
                    //if (doc.IsActive == "1") {
                    //    doc.IsActive = "Yes";
                    //} else {
                    //    doc.IsActive = "No";
                    //}
                   
                    html_table += "<tr><td>" + doc.Id + "</a></td>";
                    html_table += "<td>" + doc.Name + "</td>";
                    html_table += "<td>" + (doc.Email == "" ? "N/A" : doc.Email)+ "</td>";
                    html_table += "<td>" + doc.RoleDescription + "</td>";
                 //   html_table += "<td>" + doc.IsActive + "</td>";
                    html_table += "<td>" + status + "</td>"; 
                  
                    html_table += "<td>" + doc.DateCreated + "</td>";
                    html_table += "<td>" + doc.DateModified + "</td>";
                    // doc_html += "<td class='toolbar'><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n\n<a class='dropdown-item del doc_delete' href='#' id='" + doc.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                    html_table += "</tr>";
                    //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";

                })
                
                $("#role_detail_btable").html("");
                $("#role_detail").DataTable().destroy();
                $("#role_detail_btable").html(html_table);
                $("#role_detail").DataTable({ "scrollX": "true", "scrollCollapse": true, select: true});
          //  } 

        }

    })
}
var doAjax = function (mode, row, cb) {
    //get record by id
    if (mode == 0) {
        var data = { func: "role", id: row.Id };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
        return;
    }
    //var isValid = $('.form').validate();
    //do other modes
    if (mode == 1 || mode == 2) {
        // if (!isValid) return;
        row = row;
    }

    var data = { func: "role", mode: mode, data: row };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/PostGridData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);
        }
    });
}
$(document).ready(function () {
    
    $("#role_detail").DataTable();
    $("#roles").DataTable();
    load_roles();
    $(".loader").hide();
    setTimeout(function () {
        $("#roles").find("tbody tr:eq(0)").click();
        
    }, 500);


    $("#roles_btable").on("click", "tr", function () {
        $("#role_detail_lbl").html("<span>" + $(this).attr("name") + "</span>");
        id = $(this).attr("id");
        loadRoleDetails(id);
        //var c = $(this).find('td:first-child').text();
        //$("#prnumber").html("Prescription #" + c);
    })

    $('#roles').on('click', ".rname", function () {

        var dis = $(this);
        var rid = dis.attr('id');
        var name = dis.text();
        sysdefine = dis.attr("sysdefine");

        $('#txtName').val(name);
        $('#responsibility').val(dis.parent("td").next().text());
        $('#Id').val(rid);
        roleState.orig = JSON.stringify($('#role_modal').extractJson());

        if (sysdefine == "true") {
            $("#saveRole, #txtName, #responsibility").prop("disabled", true);
            
        } else {
            $("#saveRole, #txtName, #responsibility").prop("disabled", false);
        }
    });

    $('#roles').on('click', " .doc_delete", function () {
        $(".loader").hide();
        var did = $(this);
        var id = did.attr('id');

        //doAjax(3, { Id: id }, function (m) {
        //    if (m.result == 0) {
        //        toastr.success("Role deleted successfully!");
        //    }

        //    $("#role_modal").modal("hide");
        //    setTimeout(function () {
        //        $("#roles_1 a").trigger("click");
        //    }, 200);

        //});

        setTimeout(function () {
            if (id) {

                swal({
                    title: "Are you sure you want to delete the selected row? If yes, press \"Ok\ to continue.",
                    text: "",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonText: "Ok"
                }).then(function (e) {
                    if (e.value == true) {
                        doAjax(3, {
                            Id: id
                        }, function (m) {
                            $(".loader").show();
                            var msg = "Role deleted successfully!";
                            if (m.result == -3) //inuse
                                msg = "Role cannot be deleted because it is currently in use.";

                            swal(msg, "", "info");

                            $("#role_modal").modal("hide");
                            setTimeout(function () {
                                $("#roles_1 a").trigger("click");
                            }, 100);
                        });
                    }

                });
            }
        }, 300)






    });
    //if (typeof (PHL) !== 'undefined') {
    //    if (PHL.destroy) PHL.destroy();
    //}

    //PHL = $("#roleContent");
    //PHL.layout({
    //    center: {
    //        paneSelector: "#headerRole",
    //        size: '60%',
    //        minSize: '30%',
    //        maxSize: '50%',
    //        north: {
    //            paneSelector: "#wrapToolbar",
    //            closable: false,
    //            slidable: false,
    //            resizable: false,
    //            spacing_open: 0
    //        , north__childOptions: {
    //            paneSelector: "#roleGridToolbar",
    //            closable: false,
    //            slidable: false,
    //            resizable: false,
    //            spacing_open: 0
    //        },
    //            center__childOptions: {
    //                paneSelector: "#raToolbar",
    //                closable: false,
    //                slidable: false,
    //                resizable: false,
    //                spacing_open: 0
    //            }
    //        },
    //    },
    //    south: {
    //        paneSelector: "#middleRole",
    //        size: '40%',
    //        minSize: '20%',
    //        maxSize: '50%'
    //    },
    //    south__autoResize: true,
    //    center__autoResize: true,
    //    resizerDragOpacity: .6,
    //    livePanelResizing: false,
    //    closable: false,
    //    slidable: false,
    //    onresize: function () {
    //        resizeGrids();
    //    }

    //});


    //var resizeGrids = (function () {
    //    var f = function () {
    //        $('.ui-jqgrid-bdiv').eq(0).height($("#headerRole").height() - 90).width($("#headerRole").width());
    //        $('.ui-jqgrid-bdiv').eq(1).height($("#middleRole").height() - 25).width($('#middleRole').width());
    //        $('#grid').setGridWidth($("#roleGrid").width(), true);
    //        $('#urGrid').setGridWidth($("#userRoleGrid").width(), true);
    //    }
    //    setTimeout(f, 100);
    //    return f;
    //})();

    //$('#ddlFilter').change(function () {
    //    $('#ftrText').val('');
    //});
    //$('#ftrText,#isActive').change(function () {
    //    var res = $('#ftrText').val();
    //    //var rm = $('#ddlFilter').val();
    //    //var rd = $('#ddlFilter :selected').text
    //    var ia = $('#isActive').is(':checked');
    //    var ftrs = {
    //        groupOp: "AND",
    //        rules: []
    //    };
    //    var col = 'Description';

    //    if (ia)
    //        ftrs.rules.push({ field: 'SysDefined', op: 'eq', data: ia });

    //    if (res != '')
    //        ftrs.rules.push({ field: col, op: 'cn', data: res });


    //    $("#grid")[0].p.search = ftrs.rules.length > 0;
    //    $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
    //    $("#grid").trigger("reloadGrid");

    //    var grid = jQuery("#grid"),
    //    ids = grid.jqGrid("getDataIDs");
    //    if (ids && ids.length > 0)
    //        grid.jqGrid("setSelection", ids[0]);
    //});

    //$("#diag").dialog({
    //    zIndex: 0,
    //    autoOpen: false,
    //    modal: true,
    //    title: "Role",
    //    dialogClass: "physicianDialog",
    //    open: function () {
    //    }
    //});

   

    //var showdiag = function (mode) {
    //    $("#diag").dialog("option", {
    //        width: 420,
    //        height: 120,
    //        position: "center",
    //        buttons: {
    //            "Save": {
    //                click: function () {
    //                    var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //                    row = $('#grid').jqGrid('getRowData', id);
    //                    doAjax(mode, row, function (m) {
    //                        if (m.result == 0) {
    //                            $.growlSuccess({ message: "Role " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
    //                            $("#diag").dialog("close");
    //                            if (mode == 1) isReloaded = true;
    //                            else {
    //                                fromSort = true;
    //                                new Grid_Util.Row().saveSelection.call($('#grid')[0]);
    //                            }
    //                            $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    //                        } else {
    //                            $.growlError({ message: m.message, delay: 6000 });
    //                        }
    //                    });
    //                },
    //                class: "primaryBtn",
    //                text: "Save"
    //            },
    //            "Cancel": function () {
    //                $("#diag").dialog("close");
    //            }
    //        }
    //    }).dialog("open");
    //}

    //$('.toolbar').on('click', 'a', function () {
    //    var q = $(this);
    //    if (q.is('.add')) {
    //        $('.form').clearFields();
    //        showdiag(1);
    //    } else if (q.is('.del')) {
    //        var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //        if (id) {
    //            if (confirm('Are you sure you want to delete the selected row?')) {
    //                setTimeout(function () {
    //                    var row = $('#grid').jqGrid('getRowData', id);
    //                    if (row) {
    //                        if (row.SysDefined == "true") {
    //                            $.growlWarning({ message: 'Sorry, a system defined role cannot be deleted.', delay: 4000 });
    //                            return;
    //                        }
    //                        doAjax(3, { Id: row.Id }, function (m) {
    //                            var msg = row.Description + " deleted successfully!";
    //                            if (m.result == -3) //inuse
    //                                msg = row.Description + " cannot be deleted because it is currently in use.";
    //                            $.growlSuccess({ message: msg, delay: 6000 });
    //                            //if no error or not in use then reload grid
    //                            if (m.result != -1 || m.result != -3)
    //                                $('#roles_1 a').trigger('click');
    //                        });
    //                    }
    //                }, 100);
    //            }
    //        } else {
    //            alert("Please select row to delete.")
    //        }
    //    } else if (q.is('.unsign')) {
    //        var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //        if (id) {
    //            var row = $('#grid').jqGrid('getRowData', id);
    //            if (confirm('Are you sure you want to de-activate "' + row.Description + '"?')) {
    //                setTimeout(function () {
    //                    if (row) {
    //                        doAjax(4, { Id: row.Id }, function (m) {
    //                            if (m.result == 0)
    //                                $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
    //                        });
    //                    }
    //                }, 100);
    //            }
    //        } else {
    //            alert("Please select row to deactivate.")
    //        }
    //    }
    //});


  //  $(function () {
        //this is in site.layout.js.
        //var GURow = new Grid_Util.Row();



    
   // });

    $('#addRole').on('click', function () {
        $('#role_modal').clearFields();
        $("#saveRole, #txtName, #responsibility").prop("disabled", false);
    });


    $('#saveRole').on('click', function () {
       // debugger;
        $(".loader").show();
        mode = "";
        row = $('#role_modal').extractJson();

        if (row.Id == "") {
            mode = 1;
        } else {
            mode = 2;

            roleState.current = JSON.stringify(row);
            if (roleState.current == roleState.orig) {
                swal("There are no changes to save.", "", "info");
                $(".loader").hide();
                return;
            }
        }

        doAjax(mode, row, function (m) {
            if (m.result == 0) {
                $(".loader").hide();
                swal("Role " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
            }
            $("#role_modal").modal("hide");
            setTimeout(function () {
                $("#roles_1 a").trigger("click");
            }, 200);
        });
    });

     //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        var myGrid = $('#grid'),
        selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
        iValue = myGrid.jqGrid('getInd', selRowId),
        RoleN = myGrid.jqGrid('getCell', selRowId, 'Description');
        selind = iValue;
       
        $("td:hidden,th:hidden", "#urGrid").remove();
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Description</td><td bgcolor='#87AFC6'>Is System Defined</td><td bgcolor='#87AFC6'>Date Created</td><td bgcolor='#87AFC6'>Date Updated</td></tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('urGrid'); // id of table
        tab1 = document.getElementById('grid');
        tab_text = tab_text + tab1.rows[iValue].innerHTML + "</tr><tr></tr><tr><td colspan='7'>Role Users: </td></tr><tr><td bgcolor='#87AFC6'>Id</td><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Email</td><td bgcolor='#87AFC6'>Role</td><td bgcolor='#87AFC6'>Is Active</td><td bgcolor='#87AFC6'>Date Created</td><td bgcolor='#87AFC6'>Date Updated</td></tr><tr>";

        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";

            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_RoleUsers.xls");
        }
        else                 //other browser not tested on IE 11
            a.id = 'ExcelDL';
            a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            a.download = 'ALC_RoleUsers_' + RoleN + '.xls';
            document.body.appendChild(a);
            a.click(); // Downloads the excel document
            document.getElementById('ExcelDL').remove();
    }

    var selind;;
    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        $('#grid').setGridParam({ datatype: 'local' }).trigger('reloadGrid');

        selind = selind - 1;
        var grid = jQuery("#grid"),
        ids = grid.jqGrid("getDataIDs");
        if (ids && ids.length > 0)
            grid.jqGrid("setSelection", ids[selind]);

        setTimeout(function () {
            $("#load_urGrid").hide();
        }, 300
        );
    });
    //============================


});