var optsDpt = [];
var optsRl = [];
var optsTyp = [];
var optsStt = [];
var uniqdpt, uniqrl, uniqtyp, uniqstt;
var age = 0;
var bday;
var svc_mode = "";

var isSaveAndPrint = false;
var isClickedSaveUser = 0;



var c_role = "";
var c_shift = "";
var c_dept = "";
var new_shift = "";
var newShift = [];
var residentID = "";

var isSysUser = 0;
var addUserMode = 0;
var currentActivityId;
var newRecurrence;
var changedrec;
var currentTabIdx = "";
var roleid_before_update = 0;
var agency_ishomecare = "";

var userInfoState = {
    orig: '',
    current: ''
};
var setCheck2 = function (el, str, idx) {

    if (str.indexOf(idx) != -1)
        el.prop('checked', true);
    else el.prop('checked', false);
}

var resizeGrids = (function () {
    var f = function () {
        $('.ui-jqgrid-bdiv').eq(0).height($("#userGrid").height() - 25).width($("#userGrid").width());
        $('#grid').setGridWidth($("#userGrid").width(), true);
    }
    setTimeout(f, 100);
    return f;
})();

var setHeight = function () {
    $('.parent, .card').height(250);
    $('.imgsrc').height(($('.card').height() / 2) - 10);
    $('#userForm #tabsviews').height($(window).height() - 320);
    $('#empForm #tabsformviews').height($(window).height() - 305);

    if ($(window).height() < 400) {
        $('.dataTables_scrollBody').height(200);
        $('#userForm #tabsviews').height(600);
        $('#empForm #tabsformviews').height(500);
    }

    $(window).resize(function () {
        $('.parent, .card').height(250);
        $('.imgsrc').height(($('.card').height() / 2) - 10);
        $('#userForm #tabsviews').height($(window).height() - 320);
        $('#empForm #tabsformviews').height($(window).height() - 305);
        $("#emp_forms").height($('#employee_modal .modal-content').height() - 250);

        if ($(window).height() < 400) {
            $('.dataTables_scrollBody').height(200);
            $('#userForm #tabsviews').height(600);
            $("#emp_forms").height($('#employee_modal .modal-content').height() - 350);
            var emp_scroll = new PerfectScrollbar('#emp_forms');
        }
    })

}

var doAjax = function (mode, row, cb) {

    //get record by id
    var proceed = "";
    if (mode == 0) {
        var data = { func: "user", id: row.Id };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormDataUser",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (cb) cb(m);

                if ($('#HSRothers').is(':checked')) {
                    $("#HSRotherdetails").attr("disabled", false);
                } else {
                    $("#HSRotherdetails").attr("disabled", true);
                    $("#HSRotherdetails").val("");
                }

                $('#HSRothers').click(function () {
                    if (this.checked) {
                        $("#HSRotherdetails").attr("disabled", false);
                    } else {
                        $("#HSRotherdetails").attr("disabled", true);
                        $("#HSRotherdetails").val("");
                    }
                });

                if ($('#HSRtbpositive').is(':checked')) {
                    $("#HSRtbifpositive").attr("disabled", false);
                } else {
                    $("#HSRtbifpositive").attr("disabled", true);
                    $("#HSRtbifpositive").val("");
                }

                $('#HSRtbpositive').click(function () {
                    if (this.checked) {
                        $("#HSRtbifpositive").attr("disabled", false);
                    }
                });
                $('#HSRtbnegative').click(function () {
                    if (this.checked) {
                        $("#HSRtbifpositive").attr("disabled", true);
                        $("#HSRtbifpositive").val("");
                    }
                });


                $("#HSRdate").datepicker({ formatDate: "MM/dd/yyyy" });
                $("#HSRtbdate").datepicker({ formatDate: "MM/dd/yyyy" });
                $("#HSRhealthscreendate").datepicker({ formatDate: "MM/dd/yyyy" });
                $("#PFChiredate, #PFCstartdate, #PFCenddate, #PFCpersonalrecorddate,#PFChealthscreendate,#PFCcriminalrecorddate,#PFClivescandate,#PFCbgclearancedate,#PFCemployeerightdate,#PFCelderabusedate,#PFChepatitisdate,#PFCeligibilitydate,#PFCpayrolldate,#PFCsignedjobdate,#PFCfirstaiddate,#PFCinservicedate,#PFCdriverslicensedate,#PFCsocialsecuritydate,#PFCemphandbookdate,#PFCtbclearancedate,#PFCphotographdate,#PWNempdate").datepicker({ formatDate: "MM/dd/yyyy" });
                $("#PWNallowno,#PWNaddtlamount,#PWN_PAWa,#PWN_PAWb,#PWN_PAWc,#PWN_PAWd,#PWN_PAWe,#PWN_PAWf,#PWN_PAWg,#PWN_PAWh,#PWN_DAA1,#PWN_DAA2,#PWN_DAA3,#PWN_DAA4,#PWN_DAA5,#PWN_DAA6,#PWN_DAA7,#PWN_DAA8,#PWN_DAA9,#PWN_DAA10,#PWN_PAWa,#PWN_TMJ1,#PWN_TMJ2,#PWN_TMJ3,#PWN_TMJ4,#PWN_TMJ5,#PWN_TMJ6,#PWN_TMJ7,#PWN_TMJ8,#PWN_TMJ9,#PRage").forceNumericOnly();

                if (m.BirthDate != "" && $("#HSRuserage").val() == "") {
                    age = _calculateAge(m.BirthDate);
                    $("#HSRuserage").val(age);
                }

                //------------------ personnel record form ------
                $("#PRage").attr("disabled", true);
                $("#PRothernames").attr("disabled", true);
                $("#PRcdlnumber").attr("disabled", true);
                $("#PRexplain").attr("disabled", true);
                $("#PRcompletiondate").attr("disabled", true);

                if ($('#PRage18no').is(':checked')) {
                    $("#PRage").attr("disabled", false);
                }
                if ($('#PRdiffnameyes').is(':checked')) {
                    $("#PRothernames").attr("disabled", false);
                }
                if ($('#PRdlicenseyes').is(':checked')) {
                    $("#PRcdlnumber").attr("disabled", false);
                }
                if ($('#PRrevokedyes').is(':checked')) {
                    $("#PRexplain").attr("disabled", false);
                }
                if ($('#PRenrolledyes').is(':checked')) {
                    $("#PRcompletiondate").attr("disabled", false);
                }

                $('#PRage18yes, #PRage18no, #PRdiffnameyes, #PRdiffnameno, #PRdlicenseyes, #PRdlicenseno, #PRrevokedyes, #PRrevokedno, #PRenrolledyes, #PRenrolledno').click(function () {
                    var ckid = $(this).attr("id");
                    if (this.checked) {
                        if (ckid == "PRage18yes") {
                            $("#PRage").attr("disabled", true);
                            $("#PRage").val("");
                        } else if (ckid == "PRage18no") {
                            $("#PRage").attr("disabled", false);
                        } else if (ckid == "PRdiffnameyes") {
                            $("#PRothernames").attr("disabled", false);
                        } else if (ckid == "PRdiffnameno") {
                            $("#PRothernames").attr("disabled", true);
                            $("#PRothernames").val("");
                        } else if (ckid == "PRdlicenseyes") {
                            $("#PRcdlnumber").attr("disabled", false);
                        } else if (ckid == "PRdlicenseno") {
                            $("#PRcdlnumber").attr("disabled", true);
                            $("#PRcdlnumber").val("");
                        } else if (ckid == "PRrevokedyes") {
                            $("#PRexplain").attr("disabled", false);
                        } else if (ckid == "PRrevokedno") {
                            $("#PRexplain").attr("disabled", true);
                            $("#PRexplain").val("");
                        } else if (ckid == "PRenrolledyes") {
                            $("#PRcompletiondate").attr("disabled", false);
                        } else if (ckid == "PRenrolledno") {
                            $("#PRcompletiondate").attr("disabled", true);
                            $("#PRcompletiondate").val("");
                        }
                    }
                });




                //-----------------------------------------


                c_role = m.Role;
                c_shift = m.ShiftId;
                c_dept = m.Department;

                userInfoState.orig = JSON.stringify($('#tabs-1,#tabs-2').extractJson());

            }
        });
        return;
    }

    if (isSysUser == true) {
        if ($('#txtPassword').val() == '')
            $('#txtConfirm').attr('class', 'form-control m-input');
        else
            $('#txtConfirm').attr('class', 'form-control m-input');

    }

    //VALIDATE HERE
    //var isValid = $('#tabs-1').validate();
    //do other modes


    if (mode == 1 || mode == 2) {



        //if ($('input[name="IsSysUser[]"]:checked').length > 0 ) {
        //    isSysUser = 1;
        //};
        if ($('#chkIsSysUser:checked').length > 0) { /*3-16-2022 : cheche*/
            isSysUser = 1;
        } else {
            isSysUser = 0
        };
        if (isSysUser == 1) {


            // Cheche was here to require Middle Initial to prevent saving empty textfield  1/5/2022 : removed MI requirement : 3-1-2022
            if (mode == 1 || mode == 2) { //added mode 2 for editing nonsysuser to sysuser : Cheche 2-28-2022

                if (currentTabIdx == '#tabs-1') /*3-9-2022*/ {

                    if (agency_ishomecare == "False") {   //check all fields if not homecare                
                        if (($("#txtFirstname").val() == "") || ($("#txtLastname").val() == "") || ($("#txtEmail").val() == "") || ($("#ddlRole").val() == "") || ($("#txtShift").val() == "") || ($("#ddlDepartment").val() == "")) {
                            swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                            $(".loader").hide();
                            proceed = 0;
                            return false;
                        }
                    } else { //if agency is homecare check these fields only
                        if (($("#txtFirstname").val() == "") || ($("#txtLastname").val() == "") || ($("#txtEmail").val() == "")) {
                            swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                            $(".loader").hide();
                            proceed = 0;
                            return false;
                        }
                    }

                    var pass1 = $('#txtPassword').val();
                    var pass2 = $('#txtConfirm').val();

                    if (mode == 1 && currentTabIdx == '#tabs-1') {

                        if (pass1 == "" || pass2 == "") {
                            swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                            $(".loader").hide();
                            return;
                        }

                    }

                    if (pass1 != "" && pass1.length < 8) { // Modified by Cheche : 2/15/2022

                        swal("Password must be at least 8 characters");
                        $(".loader").hide();
                        return;
                    }

                    if (pass1 != pass2) { // Modified by Cheche : 2/15/2022

                        swal("Password and Confirm Password do not match.");
                        $(".loader").hide();
                        return;
                    }

                } else if (currentTabIdx == '#tabs-2') {

                    if (($("#txtAddress1").val() == "") || ($("#txtCity").val() == "") || ($("#txtState").val() == "") || ($("#txtZip").val() == "") || ($("#ddlGender").val() == "") || ($("#txtBirthDate").val() == "") || ($("#txtPhone").val() == "") || ($("#txtSSN").val() == "")) {
                        {
                            swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                            $(".loader").hide();
                            proceed = 0;
                            return false;
                        }
                    }
                }



            }

        } else {  // modified on 3-1-2022 : Cheche

            if (currentTabIdx == '#tabs-1') {
                if (agency_ishomecare == "False") {
                    if (($("#txtFirstname").val() == "") || ($("#txtLastname").val() == "") || ($("#ddlRole").val() == "") || ($("#txtShift").val() == "") || ($("#ddlDepartment").val() == "")) {
                        swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                        $(".loader").hide();
                        proceed = 0;
                        return false;
                    }
                } else {
                    if (($("#txtFirstname").val() == "") || ($("#txtLastname").val() == "")) {
                        swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                        $(".loader").hide();
                        proceed = 0;
                        return false;
                    }
                }
            }
            else if (currentTabIdx == '#tabs-2') {
                if (($("#txtAddress1").val() == "") || ($("#txtCity").val() == "") || ($("#txtState").val() == "") || ($("#txtZip").val() == "") || ($("#ddlGender").val() == "") || ($("#txtBirthDate").val() == "") || ($("#txtPhone").val() == "") || ($("#txtSSN").val() == "")) {
                    {
                        swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                        $(".loader").hide();
                        proceed = 0;
                        return false;
                    }
                }

            }
        }



        var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        var email_address = $("#txtEmail").val();

        if (!email_regex.test(email_address) && isSysUser == true) {  // Modified by Cheche : 3-1-2022
            swal("Invalid email address", "", "warning");
            $(".loader").hide();
            proceed = 0;
            return false;
        }

        //if (!isValid) return;
        row = $('#tabs-1,#tabs-2').extractJson();
        //============== compare changes on savng user info 


        if (currentTabIdx == "#tabs-1" || currentTabIdx == "#tabs-2") {
            userInfoState.current = JSON.stringify(row);

            if (userInfoState.current == userInfoState.orig) {
                swal("There are no changes to save.", "", "info");
                $(".loader").hide();
                return;
            }
        }
        //===============
        var _Roles = [];
        $.each($(".checkbox-grid input:checked"), function () {
            _Roles.push($(this).val());
        });

        row.OtherRoles = "";
        for (i = 0; i < _Roles.length - 1; i++) {
            row.OtherRoles = row.OtherRoles + _Roles[i] + ",";
        }

        //if (row.IsSysUser == false) {
        //    row.IsActive = false;
        //}

        var dow = [];

        $('.dayofweek2').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun': dow.push('0'); break;
                    case 'Mon': dow.push('1'); break;
                    case 'Tue': dow.push('2'); break;
                    case 'Wed': dow.push('3'); break;
                    case 'Thu': dow.push('4'); break;
                    case 'Fri': dow.push('5'); break;
                    case 'Sat': dow.push('6'); break;
                }
            }
        });

        row.DaySchedule = dow.join(',');
        proceed = 1;
    }

    //if (mode == 2) { // for 2nd tab : 3-1-2022 by Cheche
    //    if (($("#txtAddress1").val() == "") || ($("#txtAddress2").val() == "") || ($("#txtCity").val() == "") || ($("#txtState").val() == "") || ($("#txtZip").val() == "") || ($("#ddlGender").val() == "") || ($("#txtBirthDate").val() == "") || ($("#txtPhone").val() == "") || ($("#txtFax").val() == "") || ($("#txtSSN").val() == "")) {
    //        {
    //            swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
    //            $(".loader").hide();
    //            proceed = 0;
    //            return false;
    //        }
    //    }
    //}


    delete row['Mon'];
    delete row['Tue'];
    delete row['Wed'];
    delete row['Thu'];
    delete row['Fri'];
    delete row['Sat'];
    delete row['recurring'];
    delete row['Activity'];
    delete row['Onetimedate'];
    delete row['RoomId'];
    delete row['ResidentId'];
    delete row['ActivityId'];


    //Checks if department, role, or shift is changed
    if (mode == 2) {
        residentID = row.Id;
        proceed = "";

        if (agency_ishomecare == "True") {
            proceed = 1;
        } else {
            if ((row.ShiftId == c_shift) && (row.Department == c_dept) && (row.Role == c_role)) {
                proceed = 1;

            }
            if ((row.Department != c_dept && c_dept != null && c_dept != 0) || (row.Role != c_role && c_role != null && c_role != 0)) {

                getShift(row.ShiftId);
                var counter = viewConflictTasks(row.Id, newShift, row.DaySchedule);

                if (counter == 0) {
                    proceed = 1;

                } else {
                    $(".loader").hide();
                    swal({
                        title: "Changing the Department and Role may cause conflict to the scheduled tasks.",
                        text: "Want to create a new user account instead?",
                        type: "warning",
                        showCancelButton: !0,
                        confirmButtonText: "Yes"
                    }).then(function (e) {

                        if (e.value == true) {
                            //$("#employee_modal #_closemodal").click();
                            closeModal("employee_modal");

                            setTimeout(function () {
                                $("div.col-xl-12 #addnew").click();
                            }, 500);

                        } else {
                            return false;
                        }
                    });

                }

            } else if ((row.ShiftId != c_shift && c_shift != null) || (row.DaySchedule != newRecurrence && row.IsSysUser == true)) {

                proceed = 0;
                getShift(row.ShiftId);

                var counter = viewConflictTasks(row.Id, newShift, row.DaySchedule);

                if (counter == 0) {
                    proceed = 1;

                } else {
                    proceed = 0;
                    $("#employee_conflicttasks_modal").modal("toggle");
                    $(".loader").hide();
                    changedrec = row.DaySchedule;

                    $(document).on("click", "#employee_conflicttasks_modal #proceed", function () {

                        $("#employee_conflicttasks_modal .close").click();
                        var data = { func: "user", mode: mode, data: row };

                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Admin/PostGridData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (cb) cb(m);
                                c_shift = row.ShiftId;
                                newRecurrence = row.DaySchedule;
                            }
                        });

                    })
                }
            } else {
                proceed = 1;
            }
        }


    }

    if (proceed) {

        row.SSN = row.SSN.replace(/[^\w\s]/gi, '');
        var data = { func: "user", mode: mode, data: row };

        if (mode == 1) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify(data),
                dataType: "json",
                async: false,
                success: function (m) {
                    if (cb) cb(m);
                    isClickedSaveUser = 0;
                }
            });
        } else { // 3-16-2022
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify(data),
                dataType: "json",
                async: false,
                success: function (m) {

                    if (cb) cb(m);
                }
            });
        }
        c_dept = row.Department;
        c_role = row.Role;

    }

}

var doAjaxUsr = function (mode, row, cb) {
    //get record by id     
    if (mode == 0) {
        var data = { func: "users", id: row.Id };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (cb) cb(m);
            }
        });
        return;
    }

    //VALIDATE HERE
    //var isValid = $('#empForm').validate();
    //do other modes
    if (mode == 1 || mode == 2) {
        //if (!isValid) {
        //    return;
        //}
        row = $('#empForm .usrTbs').extractJson('id');
        row.userId = $('#userForm #txtId').val();
        //alert(row.userId);
        row.dcd = 1; //dont check duplicates.

        // Added this on 01-05-2022 : ANGELIE
        var pfc_hiredate = $("#PFChiredate").val();
        var pfc_startdate = $("#PFCstartdate").val();
        var pfc_enddate = $("#PFCenddate").val();

        var hasStartDate = (pfc_startdate != null && pfc_startdate != "" ? true : false);
        var hasHireDate = (pfc_hiredate != null && pfc_hiredate != "" ? true : false);
        var hasEndDate = (pfc_enddate != null && pfc_enddate != "" ? true : false);

        if (hasStartDate) { pfc_startdate = new Date(pfc_startdate); };
        if (hasHireDate) { pfc_hiredate = new Date(pfc_hiredate); };
        if (hasEndDate) { pfc_enddate = new Date(pfc_enddate); };

        if (hasStartDate && !hasHireDate) {
            swal("Please fill in the Hire Date.", "", "warning");
            $(".loader").hide();
            return;
        }
        if (hasEndDate && !hasStartDate) {
            swal("Please fill in the Start Date.", "", "warning");
            $(".loader").hide();
            return;
        }

        if (hasStartDate && hasHireDate) {

            if (pfc_hiredate > pfc_startdate) {
                swal("Hired date should be earlier than the Start Date.", "Please check the dates on the Employee Forms - Personal File Checklist tab.", "warning");
                $(".loader").hide();
                return;
            }
        }

        if (hasStartDate && hasEndDate) {
            if (pfc_startdate > pfc_enddate) {
                swal("Start date should be earlier than the End Date.", "", "warning");
                $(".loader").hide();
                return;
            }
        }
    }
    //var tabindex = $("#empForm").tabs('option', 'selected');
    var tabs = $("#tabsForm").find("a");
    var tabindex;

    $.each(tabs, function (i, e) {
        if ($(this).hasClass("active")) {
            tabindex = $(this).parents("li").index();
        }
    })


    if (($.isNumeric(row.PRage) || row.PRage == "") == false) {

        swal("Age input is not a number!", "", "warning");
        return false;
    };

    var data = { func: "user_files", mode: mode, data: row, TabIndex: tabindex };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/PostGridData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);
        }
    });

}
function _calculateAge(birthday) { // modified on 11-18-2022 : Cheche : Reason: existing function returned undefined value
    // birthday is a date
  //  var time = new Date(birthday);
  //  var ageDifMs = Date.now() - time.getTime();
   // var ageDate = new Date(ageDifMs); // miliseconds from epoch

    var today = new Date();
    var bdate = new Date(birthday);
    var age = today.getFullYear() - bdate.getFullYear();
    var m = today.getMonth() - bdate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < bdate.getDate())) {
        age--;
    }
    return age;
}
//Cheche was here : 11-11-2022
var removeEmpData = function () {
    $("#PFCempname, #PFCdepartment, #HSRusername, #HSRuserposition, #CREmployeeName, #CREmployeeAddress,#CRCityField,#CRZipField, #CRSocialSecurityNumber,#CRDateOfBirth,#ERResidentName,#EAusername,#EAuserposition,#EAusername2,#PCFusername,#EHempName,#EHempAdress,#HBDName,#HBDAddress,#PRname,#PRaddress,#PRtelephone,#RLSlastname,#RLSfirstname,#RLSmiddleinitial,#RLSdob,#EEVlastname,#EEVfirstname,#EEVmiddleinitial, #EEVssnumber, #EEVdob, #EEVcityortown, #EEVstate, #EEVzipcode, #EEVemail, #PWNnamemi,#PWNlastname,#PWNssn,#JDAemp_name, #HSRfacilityname, #PCFfacilityname, #PCFNameFaci, #PCFNameFaci2, #PRfacilityname, #PRfacilityaddress, #PRfacilityfilenumber").attr("disabled", false);

}
function ampm(e) {

    var s = e.split(":");
    var t = s[1].split(" ");

    if (t[1] == "AM" || t[1] == "am") {
        if (s[0] == "12") {
            return 0 + "," + t[0]
        } else {
            return s[0] + "," + t[0];
        }

    } else if (t[1] == "PM" || t[1] == "pm") {
        var n = parseInt(s[0]);
        s[0] = n + 12;

        return s[0] + "," + t[0];
    }
}
var viewConflictTasks = function (id, n_shift, recurrence) {


    var stime = ampm(n_shift.starttime);
    var etime = ampm(n_shift.endtime);

    var splitStime = stime.split(",");
    var splitEtime = etime.split(",");
    var dateToday1 = new Date();
    var dateToday2 = new Date();

    dateToday1.setHours(parseInt(splitStime[0]), parseInt(splitStime[1]), 0, 0);
    dateToday2.setHours(parseInt(splitEtime[0]), parseInt(splitEtime[1]), 0, 0);
    var start_time = dateToday1;
    var end_time = dateToday2;

    var counter = 0;

    var recurrence_new = recurrence.split(",");
    var newRecurr = [];
    var weekday = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];

    $(recurrence_new).each(function (i, v) {
        newRecurr.push(weekday[v]);
    });

    $("#proceed").attr("disabled", false);
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=activity_schedule&param3=" + id + "&param9=" + roleid_before_update,
        dataType: "json",
        async: false,
        success: function (m) {
            var html_table = "";
            var svcIdx = "";

            var dtnow = new Date(Date.now());


            if (m.length != 0) {
             
                $.each(m, function (i, v) {

                    //removed this on 11-15-22:Angelie, fixed on # of conflict count
                    //if (v.IsActive == true) {
                    //    //transferred counter here to show active tasks only
                    //    counter = 1;
                    //}

                    $("#empname_lbl").text(v.Carestaff);


                    var date = new Date();
                    var schedTime = ampm(v.Schedule);
                    var t = schedTime.split(",");
                    date.setHours(parseInt(t[0]), parseInt(t[1]), 0, 0);

                    var recurr = v.Recurrence.split(",");

                    var nDate;
                    if (v.ActiveUntil != null) {
                        nDate = new Date(v.ActiveUntil);
                    }


                    if ((v.IsActive == true) || ((v.IsActive == false) && (nDate > dtnow))) {

                        var isConflictInDaysSchedule = 0;

                        for (i = 0; i <= recurr.length - 1; i++) {
                            if ($.inArray(recurr[i], newRecurr) == -1) {
                                isConflictInDaysSchedule = 1;
                            }
                        }


                        var isConflict = 0;

                        if (start_time.getTime() < date.getTime() && end_time.getTime() > date.getTime()) {
                            if (isConflictInDaysSchedule == 0) {
                                html_table += "<tr class='checkError' id='" + v.Id + "'>";
                                html_table += "<td><a href='#' class='svctask' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'>" + v.Activity + "</a></td>";

                            } else {
                                html_table += "<tr style='background:#af0f0f; color:white' class='checkError' id='" + v.Id + "'>";
                                html_table += "<td ><a style='color:white;' href='#' class='svctask' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'>" + v.Activity + "</a></td>";
                                //counter += 1;
                                isConflict = 1;
                            }
                        } else if (start_time.getTime() == date.getTime()) {
                            if (isConflictInDaysSchedule == 0) {
                                html_table += "<tr class='checkError' id='" + v.Id + "'>";
                                html_table += "<td><a href='#' class='svctask' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'>" + v.Activity + "</a></td>";

                            } else {
                                html_table += "<tr style='background:#af0f0f; color:white' class='checkError' id='" + v.Id + "'>";
                                html_table += "<td ><a style='color:white;' href='#' class='svctask' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'>" + v.Activity + "</a></td>";
                                //counter += 1;
                                isConflict = 1;
                            }
                        } else {

                            html_table += "<tr style='background:#af0f0f; color:white' class='checkError' id='" + v.Id + "'>";
                            html_table += "<td ><a style='color:white;' href='#' class='svctask' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'>" + v.Activity + "</a></td>";
                            //counter += 1;
                            isConflict = 1;
                        }

                        html_table += "<td>" + v.Department + "</td>";
                        html_table += "<td>" + (v.Resident == null ? "N/A" : v.Resident) + "</td>";
                        html_table += "<td>" + v.Carestaff + "</td>";
                        html_table += "<td>" + (v.Room == null ? "N/A" : v.Room) + "</td>";
                        html_table += "<td>" + v.Schedule + "</td>";
                        html_table += "<td>" + v.Recurrence + "</td>";
                        html_table += "<td>" + v.Duration + " minutes</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";


                      //  debugger;
                        console.log(counter);
                        if (isConflict == 1) {
                            counter += 1;
                            html_table += "<td><a href='#' class='deactivate' deactivate_id='" + v.Id + "'><span  class='m-badge  m-badge--success m-badge--wide '>Deactivate</span></a> &nbsp;<a href='#' class='reassign' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'><span  class='m-badge  m-badge--success m-badge--wide '>Reassign</span></a></td>";
                            html_table += "</tr>";
                        } else {
                            html_table += "<td></td>";
                            html_table += "</tr>";

                        }
                    }
                })
            }

            if (counter == 0) {
                html_table = "<tr><td colspan='10'> No conflict </td></tr>";
                $("#conflictTasktbl").html(html_table);
                $("#legend").html(" - 0 Conflicts");
                $("#proceed").attr("disabled", false);
            } else {
                $("#conflictTasktbl").html(html_table);
                $("#legend").html(" - " + counter + " " + (counter == 1 ? "Conflict" : "Conflicts"));
                $("#proceed").attr("disabled", true);
            }

        }
    });

    c = counter;
    return c;

}
function getShift(id) {
    var containerNShift = {};
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=activity_schedule&param8=" + id,
        dataType: "json",
        async: false,
        success: function (m) {

            containerNShift = {
                shiftid: m.shift_id,
                starttime: m.StartTime,
                endtime: m.EndTime

            }
            newShift = containerNShift;
        }
    });


}

$(document).ready(function (e) {
    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
        $('.popover').popover('hide');
    }
});
$(document).ready(function () {
    agency_ishomecare = $("#agency_ishomecare").val();

    var scroll1 = new PerfectScrollbar('#userstile');

    //initialization
    var container = {};
    var containerTask = {};
    var employeeDatatable = $("#user_emp_tbl").DataTable();
    // var serviceTask = $("#sGrid").DataTable();
    // var discplinarywu = $("#wGrid").DataTable();
    var currentId;
    $(".numonly").forceNumericOnly();
    $(".confirm_pass").hide();
    //Added this to format the birthdate ANGELIE: 01-04-2022
    $("#txtBirthDate").keyup(function () {

        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var bday = $("#txtBirthDate").val().trim();
        var m = "";
        var d = "";
        var y = "";
        var error = document.getElementById("bday_error_usr");


        var isCorrectFormat = /^(0?[1-9]|1[0-2])\/(0?[1-9]|1\d|2\d|3[01])\/(18|19|20)\d{2}$/.test(bday);
        var isCorrectFormatWithoutSlash = /^(0?[1-9]|1[0-2])(0?[1-9]|1\d|2\d|3[01])(18|19|20)\d{2}$/.test(bday);
        var wordFormat = /^((January|March|May|July|August|October|December)(\s|\S)(0?[1-9]|1\d|2\d|3[0-1])|(April|June|September|November)(\s|\S)(0?[1-9]|1\d|2\d|30)|(February)(\s|\S)(0?[1-9]|1\d|2\d))(\,|\,\s|\,\S|\s|\S)(18|19|20)\d{2}$/.test(bday);
        if (wordFormat) {
            for (var i = 0; i <= months.length; i++) {
                if (bday.includes(months[i])) {
                    m = i + 1;

                    if (m <= 9) {
                        m = "0" + m;
                    }

                    var date_year = bday.replace(months[i], "");
                    date_year = date_year.trim();

                    if (date_year.includes(",")) {
                        date_year = date_year.split(",");
                    } else {
                        date_year = date_year.split(" ");
                    }

                    d = date_year[0].trim();
                    y = date_year[1].trim();
                }
            }

            $("#txtBirthDate").val(m + "/" + d + "/" + y);
            error.textContent = " ";


        } else if (isCorrectFormatWithoutSlash && bday.length == 8) {

            m = bday.slice(0, 2);
            d = bday.slice(2, 4);
            y = bday.slice(4, 8);

            $("#txtBirthDate").val(m + "/" + d + "/" + y)
            error.textContent = " ";

        } else if (isCorrectFormat) {
            $("#txtBirthDate").val(bday);
            error.textContent = " ";

        } else {

            error.textContent = "Please enter a valid date in this format 'MM/DD/YYYY'!  ";
            error.style.color = "red";
        }
    });

    $("#txtPassword").keyup(function () {

        $(".confirm_pass").show();

    })


    $(window).resize(function () {
        if ($(window).width() < 1500) {
            $("#userstile p.user_info").css("font-size", "70%");
            $("#userstile p.user_info .pop-icon").css("font-size", "0.9rem");
        } else {
            $("#userstile p.user_info").css("font-size", "100%");
        }
    })

    $(document).on("click", "#add_task", function () {

        var depart = $("#ddlDepartment").val();
        $("#ddlDept").val(depart);
        svc_mode = "add";
        bindDept(depart, toggleCarestafflist);

        $("#serviceTaskLabel").html("Add Service Task");
        $("#txtResidentId").removeAttr("disabled");
        $("#no_carestaff_available").prop("hidden", true);
    })

    $(document).on("click", ".svctask", function () {

        svc_mode = "edit";


        $("#serviceTaskLabel").html("Edit Service Task");
        $("#txtResidentId").attr("disabled", "disabled");
        $("#txtCarestaff").removeAttr("hidden");
        //   $("#txtCare").attr("hidden", "hidden");
    })


    $(document).on("click", "#conflictTasktbl .deactivate", function () {

        var id = $(this).attr('deactivate_id');

        var dDate = new Date(Date.now());
        var uDate = (dDate.getMonth() + 1) + "/" + dDate.getDate() + "/" + dDate.getFullYear();
        var row = {};

        row.id = id;

        row.active_until = uDate;
        var data = {
            func: "activity_schedule",
            mode: 3,
            data: row
        };

        swal({
            title: "Are you sure you want to deactivate this task?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes"
        }).then(function (e) {
            e.value &&
                $.ajax({

                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify(data),
                    async: false,
                    dataType: "json",
                    success: function (m) {
                        if (m.result == 0) {
                            swal("Task deactivated successfully! Saving shift change of employee ..", "", "success");
                        }
                        var counter = viewConflictTasks(residentID, newShift, newRecurrence);
                        if (counter == 0) {
                            $(".loader").show();
                            setTimeout(function () {
                                $("#employee_conflicttasks_modal #proceed").trigger('click');
                                $(".loader").hide();
                            }, 300)

                        }
                    }
                });

        });

    })

    $(document).on("click", "#conflictTasktbl .reassign", function () {

        //Added this on 11-10-22: Angelie
        $("#carestaff_div").attr("hidden", false);

        $("#serviceTaskLabel").text("Reassign Task");
        $(".forreassigntask").attr("disabled", "disabled");

        currentActivityId = $(this).attr("id");

        var idx = currentId + "_" + $(this).attr("id");

        var rowid = {
            Id: $(this).attr("id")
        };

        if (rowid) {
            doAjax2(0, rowid, function (res) {

                $("#servicetask_modal .form").clearFields();
                bindDept(res.Dept.toString(), function () {

                    if (!res.IsOneTime)
                        res.Onetimedate = '';
                    $("#servicetask_modal .form").mapJson(res);
                    $("#mode").val(1);
                });
                $("#mode").val(1);
                $("#servicetask_modal .form").mapJson(res);
               
                //Modified on 11-11-22: Angelie
                $("#txtResidentId").removeAttr("disabled");
                $("#txtResidentId").val(res.ResidentId);
                $("#txtTimeIn").attr("disabled", true);
                $("#no_carestaff_available").attr("hidden", true);

                //   $("#ddlDept option[value='5']").remove();
                // $("#ddlDept option[value='7']").remove();
                $("#ddlDept").val(res.Dept);
                //if housekeeper or maintenance hide room dropdown
                if (res.Dept == 5 || res.Dept == 7) {
                    $('#liRoom, #liRoom select').show();
                    $('#liResident, #liResident select').hide();
                    $('<option/>').val(res.RoomId).html(res.RoomName).appendTo('#servicetask_modal #txtRoomId');

                } else {
                    $("#txtResidentId").attr("disabled", true);
                    $('#liResident, #liResident select').show();
                    $('#liRoom, #liRoom select').hide();
                    $('<option/>').val(res.ResidentId).html(res.ResidentName).appendTo('#servicetask_modal #txtResidentId');
                }

                //map day of week
                if (!res.IsOneTime) {
                    $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
                    var setCheck = function (el, str, idx) {

                        if (str.indexOf(idx) != -1)
                            el.prop('checked', true);
                        else el.prop('checked', false);
                    }

                    if (res.Recurrence != null && res.Recurrence != '') {
                        var dow = res.Recurrence;
                        $('.dayofweek').find('input[type="checkbox"]').each(function () {
                            var d = $(this);
                            switch (d.attr('name')) {
                                case 'Sun': setCheck(d, dow, '0'); break;
                                case 'Mon': setCheck(d, dow, '1'); break;
                                case 'Tue': setCheck(d, dow, '2'); break;
                                case 'Wed': setCheck(d, dow, '3'); break;
                                case 'Thu': setCheck(d, dow, '4'); break;
                                case 'Fri': setCheck(d, dow, '5'); break;
                                case 'Sat': setCheck(d, dow, '6'); break;
                            }
                        });
                        $('input[name="recurring"]:eq(0)').trigger('click');
                        $('#Onetimedate').val('');
                    }

                } else {
                    $('input[name="recurring"]:eq(1)').trigger('click');
                    res.Recurrence = moment(res.Onetimedate).day().toString();
                }
                $('input[xid="hdCarestaffId"]').val(res.CarestaffId);
                var data = { scheduletime: res.ScheduleTime, activityid: res.ActivityId, carestaffid: res.CarestaffId, recurrence: res.Recurrence };
                data.isonetime = !res.IsOneTime ? "0" : "1";
                //data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
                data.clientdt = !res.IsOneTime ? "Invalid date" : res.Onetimedate;
                data.type = 1;
                //data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#Onetimedate').val()).format("MM/DD/YYYY");
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/GetCarestaffByTime",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {

                        $('#txtCarestaff').empty();

                        if (m.length > 0) {
                            $('<option/>').val('').html('').appendTo('#txtCarestaff');
                            $.each(m, function (i, d) {
                                $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                            });
                        } else {
                            swal("There is no available carestaff for this time schedule!", "", "warning");
                            $("#carestaff_div").attr("hidden", true);
                            $("#no_carestaff_available").attr("hidden", true);
                            return;
                        }
                      
                        //$('#txtCarestaff').val(res.CarestaffId);

                    }
                });
            });
        }

        //$('#txtTimeIn').click(triggerDayofWeekClick);
    });

    $('a.checkall').click(function () {
        var dis = $(this);
        var dd = dis.attr('data-toggle');
        var dow = '0,1,2,3,4,5,6';
        if (dd != 'cc') {
            dow = '';
            dis.attr('data-toggle', 'cc');
            dis.html('Check All Days');
        } else {
            dis.attr('data-toggle', 'dd');
            dis.html('Uncheck All Days');
        }
        $('.dayofweek2').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            switch (d.attr('name')) {
                case 'Sun': setCheck2(d, dow, '0'); break;
                case 'Mon': setCheck2(d, dow, '1'); break;
                case 'Tue': setCheck2(d, dow, '2'); break;
                case 'Wed': setCheck2(d, dow, '3'); break;
                case 'Thu': setCheck2(d, dow, '4'); break;
                case 'Fri': setCheck2(d, dow, '5'); break;
                case 'Sat': setCheck2(d, dow, '6'); break;
            }
        });

    });

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });
    $('#ftrRole,#ftrDepartment,#ftrSysUser,#ftrText,#ftrStatus').change(function () {
        var res = $('#ftrText').val();
        var rm = $('#ddlFilter').val();
        var rd = $('#ddlFilter :selected').text();
        var rl = $('#ftrRole :selected').text();
        var dl = $('#ftrDepartment :selected').text();
        var ia = $('#ftrStatus :selected').val();
        var su = $('#ftrSysUser :selected').val();

        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'Id';
        if (rm == 2)
            col = "Name";
        else if (rm == 3)
            col = "Email";


        if (rl != 'All')
            ftrs.rules.push({ field: 'RoleDescription', op: 'eq', data: rl });
        if (dl != 'All')
            ftrs.rules.push({ field: 'Department', op: 'eq', data: dl });
        if (ia != '-1')
            ftrs.rules.push({ field: 'IsActive', op: 'eq', data: ia });
        if (su != '-1')
            ftrs.rules.push({ field: 'NonSysUser', op: 'eq', data: su });
        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });
    $('#txtBirthDate').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
    }).on("change", function () {
        bday = ($(this).val());
        $("#HSRuserage").val(_calculateAge(bday));

    });
    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
    $('.ndatepicker').datepicker({
        changeMonth: true,
        changeYear: true,
    });

    $('#txtPhone,#txtFax,#PRtelephone, #PRtelnumber1,#PRtelnumber2, #PRtelnumber3, #PRtelnumber4,#PRtelnumber5, #PRtelnumber6,#PRreftel1, #PRreftel2, #PRreftel3, #PRrelnumber, #RLSagencytelno, #EEVtelno, #RLStelnumber').mask("(999)999-9999");
    $("#txtSSN").mask("999-99-9999");



    $('#upload-dwu').on('click', function () {
        var id = currentId;
        // var resname = $('#admForm #DCResidentName').val();
        $('#uid').val(id);
        //$("#modal-title-ud").html("Upload document for " + resname)
        // console.log("DOCUMENT");
        uploadDocument();
    });

    $('#upload-pdoc').on('click', function () {
        var id = currentId;
        // var resname = $('#admForm #DCResidentName').val();
        $('#pd_uid').val(id);
        //$("#modal-title-ud").html("Upload document for " + resname)
        // console.log("DOCUMENT");
        uploadPersonalDocument();
    });

    $('#upload-pit').on('click', function () {
        var id = currentId;
        // var resname = $('#admForm #DCResidentName').val();
        $('#pit_uid').val(id);
        //$("#modal-title-ud").html("Upload document for " + resname)
        // console.log("DOCUMENT");
        uploadPITDocs();
    });

    $('#upload-efr').on('click', function () {
        uploadEmpFormReq();
    });

    $('#loadDWU').on('click', function () {
        initDocuments();
    });

    $('#loadPDOC').on('click', function () {
        initP_Documents();
    });

    $('#loadPIT').on('click', function () {
        initPITDocs();
    });
    $('#loadEFR').on('click', function () {
        initEmpFormReqs();
    });

    //$("#diag").dialog({
    //    zIndex: 0,
    //    autoOpen: false,
    //    modal: true,
    //    title: "Employee",
    //    dialogClass: "physicianDialog",
    //    open: function () {
    //        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
    //                                          .addClass("ui-state-disabled");
    //        if (!$("#userForm").data("layoutContainer")) {
    //            $("#userForm").layout({
    //                center: {
    //                    paneSelector: ".tabPanels",
    //                    closable: false,
    //                    slidable: false,
    //                    resizable: true,
    //                    spacing_open: 0
    //                },
    //                north: {
    //                    paneSelector: ".tabs",
    //                    closable: false,
    //                    slidable: false,
    //                    resizable: false,
    //                    spacing_open: 0,
    //                    size: 55
    //                }
    //            });
    //        }
    //        $("#userForm").layout().resizeAll();
    //        if (!$("#userForm").data("tabs")) {
    //            $("#userForm").tabs({
    //                select: function (event, ui) {
    //                    if (ui.index == 2) {
    //                        setTimeout(initService, 1);
    //                    } else if (ui.index == 3) {
    //                        setTimeout(initRoles, 1);
    //                    } else if (ui.index == 6) {
    //                        setTimeout(initDocuments, 1);
    //                    } else if (ui.index == 7) {
    //                        setTimeout(initP_Documents, 1);
    //                    } else if (ui.index == 8) {
    //                        setTimeout(initPITDocs, 1);
    //                    }

    //                    if (ui.index != 4) {
    //                        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
    //                                          .addClass("ui-state-disabled");
    //                    } else {
    //                        $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
    //                                                                      .removeClass("ui-state-disabled");
    //                    }
    //                }
    //            });
    //        }
    //        $("#userForm").tabs("option", "selected", 0);
    //        $("#empForm").tabs();
    //        //var id = $('#userForm #txtCSId').val();
    //        //$('#userForm #txtCSId').val(id);

    //        var id = $('#userForm #txtId').val();
    //        //$('#userForm #txtCSId').val(id);
    //    }
    //});


    $(document).on("click", ".svctask", function () {

        $(".forreassigntask").removeAttr("disabled");
        $("#serviceTaskLabel").text("Edit Service Task");
        var idx = currentId + "_" + $(this).attr("id");
        var row = containerTask[idx];
        var rowid = {
            Id: $(this).attr("id")
        };

        if (row) {
            doAjax2(0, rowid, function (res) {

                $("#servicetask_modal .form").clearFields();
                bindDept(res.Dept.toString(), function () {

                    if (!res.IsOneTime)
                        res.Onetimedate = '';
                    $("#servicetask_modal .form").mapJson(res);
                    $("#mode").val(2);
                });
                $("#mode").val(2);
                $("#txtResidentId").val(res.ResidentId);
                //   $("#ddlDept option[value='5']").remove();
                // $("#ddlDept option[value='7']").remove();
                $("#ddlDept").val(res.Dept);
                $("#ddlDept").attr("disabled", "disabled");
                //if housekeeper or maintenance hide room dropdown
                if (res.Dept == 5 || res.Dept == 7) {
                    $('#liRoom, #liRoom select').show();
                    $('#liResident, #liResident select').hide();
                    $('<option/>').val(res.RoomId).html(res.RoomName).appendTo('#servicetask_modal #txtRoomId');
                } else {
                    $('#liResident, #liResident select').show();
                    $('#liRoom, #liRoom select').hide();
                    $('<option/>').val(res.ResidentId).html(res.ResidentName).appendTo('#servicetask_modal #txtResidentId');
                }

                //map day of week
                if (!res.IsOneTime) {
                    $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
                    var setCheck = function (el, str, idx) {

                        if (str.indexOf(idx) != -1)
                            el.prop('checked', true);
                        else el.prop('checked', false);
                    }

                    if (res.Recurrence != null && res.Recurrence != '') {
                        var dow = res.Recurrence;
                        $('.dayofweek').find('input[type="checkbox"]').each(function () {
                            var d = $(this);
                            switch (d.attr('name')) {
                                case 'Sun': setCheck(d, dow, '0'); break;
                                case 'Mon': setCheck(d, dow, '1'); break;
                                case 'Tue': setCheck(d, dow, '2'); break;
                                case 'Wed': setCheck(d, dow, '3'); break;
                                case 'Thu': setCheck(d, dow, '4'); break;
                                case 'Fri': setCheck(d, dow, '5'); break;
                                case 'Sat': setCheck(d, dow, '6'); break;
                            }
                        });
                        $('input[name="recurring"]:eq(0)').trigger('click');
                        $('#Onetimedate').val('');
                    }

                } else {
                    $('input[name="recurring"]:eq(1)').trigger('click');
                    res.Recurrence = moment(res.Onetimedate).day().toString();

                }
                $('input[xid="hdCarestaffId"]').val(res.CarestaffId);
                var data = { scheduletime: res.ScheduleTime, activityid: res.ActivityId, carestaffid: res.CarestaffId, recurrence: res.Recurrence };
                data.isonetime = !res.IsOneTime ? "0" : "1";
                //data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
                data.clientdt = !res.IsOneTime ? "Invalid date" : res.Onetimedate;
                data.type = 1;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/GetCarestaffByTime",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {
                        $('#txtCarestaff').empty();
                        $('<option/>').val('').html('').appendTo('#txtCarestaff');
                        $.each(m, function (i, d) {
                            $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                        });
                        $('#txtCarestaff').val(res.CarestaffId);
                        //showdiagTask(2);
                    }
                });
            });
        }
    })
    $(document).on("click", "#userForm #tabs a", function () {
        $("#saveemployeeinfo").show();
        var tab = $(this).attr("href");
        currentTabIdx = tab;

        if (tab == "#tabs-usr") {
            
            var scroll3 = new PerfectScrollbar('#tabsviews');
            scroll3.destroy();
            var scroll2 = new PerfectScrollbar('#tabsformviews');
            
            // To display info upon adding New Employee : 11-11-2022 : Cheche
            row = $('#tabs-1,#tabs-2').extractJson(); 
            var PFCempname = $("#PFCempname").val();
            if(row.Firstname != "" && PFCempname == "")  
            { 
              $("#PFCempname, #JDAemp_name, #PRname, #HBDName, #EHempName, #PCFusername, #EAusername2, #ERResidentName, #EAusername, #CREmployeeName, #HSRusername").val(row.Lastname + ", " + row.Firstname + " " + row.MiddleInitial);
              $("#RLSlastname, #EEVlastname, #PWNlastname").val(row.Lastname);
              $("#RLSfirstname, #EEVfirstname").val(row.Firstname);
              $("#RLSmiddleinitial, #EEVmiddleinitial").val(row.MiddleInitial);
              $("#PWNnamemi").val(row.Firstname + " " + row.MiddleInitial);
              $("#CRCityField, #RLScity, #EEVcityortown").val(row.City);
              $("#CREmployeeAddress").val(row.Address1 + " " + row.Address2);
              $("#CRZipField, #EEVzipcode, #RLSzipcode").val(row.Zip);
              $("#PRssnumber, #EEVssnumber, #CRSocialSecurityNumber, #PWNssn").val(row.SSN);
              $("#CRDateOfBirth, #RLSdob, #EEVdob").val(row.BirthDate);
              $("#PRtelephone, #RLStelnumber, #EEVtelno").val(row.Phone);
              $("#PRaddress, #EHempAdress, #HBDAddress").val(row.Address1 + " " + row.Address2 + " " + row.City + " " + row.StateId + " " + row.Zip);
              $('#EEVemail').val(row.Email);
              $("#RLScitystatezip, #PWNcitystate").val(row.City + " " + row.StateId + " " + row.Zip);
              $("#RLSstate, #EEVstate").val(row.StateId);
              if(row.Gender == "M"){
                $('#RLSmale').attr('checked', 'checked');
              } else if(row.Gender == "F"){
                $('#RLSfemale').attr('checked', 'checked');
              } 
         
              if(row.BirthDate != null){
                  var date = new Date();
                  var ageYear1 = date.getFullYear();
                  var ageMonth = date.getMonth() + 1;
                  var ageDate = date.getDate();
                  var bDate = new Date(row.BirthDate);
                  var ageYear2 = bDate.getFullYear();
                  var ageMonth2 = bDate.getMonth() + 1;
                  var ageDate2 = bDate.getDate();
                  var agediff = ageYear1 - ageYear2;
                  if(ageMonth <= ageMonth2 && ageDate >= ageDate2){
                     $("#HSRuserage").val(parseInt(agediff));
                  }else{
                     $("#HSRuserage").val(parseInt(agediff - 1));
                  }
                 
              }
    
               
            }
            //ends here
        } else {
            var scroll3 = new PerfectScrollbar('#tabsviews');
        }

        if (tab == "#tabs-1") {
            $("#userForm .ps__rail-x").css("display", "none");
        }

        if (tab == "#tabs-3") {
            $("#saveemployeeinfo").hide();
            $(".loader").show();
            setTimeout(function () {
                if (agency_ishomecare == "True") {
                    initServiceHomeCare();
                    $(".refreshTask").prop("hidden", true);
                    $(".refreshTaskHm").prop("hidden", false);
                } else {
                    initService();
                    $(".refreshTask").prop("hidden", false);
                    $(".refreshTaskHm").prop("hidden", true);
                }

                $(".loader").hide();
            }, 100)
        } else if (tab == "#tabs-4") {
            setTimeout(initRoles, 1);
        } else if (tab == "#tabs-dsp") {
            $("#saveemployeeinfo").hide();
            $(".loader").show();
            setTimeout(function () {
                initDocuments();
                $(".loader").hide();
            }, 100)
        } else if (tab == "#tabs-pdocs") {
            $("#saveemployeeinfo").hide();
            $(".loader").show();
            setTimeout(function () {
                initP_Documents();
                $(".loader").hide();
            }, 100)
        } else if (tab == "#tabs-pit") {
            $("#saveemployeeinfo").hide();
            $(".loader").show();
            setTimeout(function () {
                initPITDocs();
                $(".loader").hide();
            }, 100)
        }

        if (tab != "#tabs-usr") {
            $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                .addClass("ui-state-disabled");
        } else {
            $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
                .removeClass("ui-state-disabled");

            var tbs = $("#tabsForm").find("a");
            $.each(tbs, function (i, e) {
                $(this).removeClass("active");
                $(this).removeClass("show");
            })

            var tbs2 = $("#tabsformviews").find(".tab-pane");
            $.each(tbs2, function (i, e) {
                $(this).removeClass("active");
            })

            $.each(tbs, function (i, e) {
                if ($(this).attr("href") == "#tabs-1a") {
                    $(this).click();
                }
            })
        }
    })

    // Cheche was here : 2/16/2022 & on 3-2-2022
    $(document).on("click", "#saveemployeeinfo", function () {
        if (agency_ishomecare == "False") {
            if ($('#txtShift').val().length < 9) {
                swal("Please select shift schedule on the list.", "You can add new shift in the Shift page.", "info");
                return;
            }
        }



        $(".loader").show();
        var id1 = $('#userForm #txtId').val();

        if (addUserMode == 1) {
            id1 = addUserMode;
        }

        var amode = $.isNumeric(id1) ? 1 : 2;
        var row = container[currentId];

        var tabs = $("#tabs").find("a");
        var tabIdx;
        var tabIdxHref;

        $.each(tabs, function (i, e) {
            if ($(this).hasClass("active")) {
                tabIdx = $(this).parents("li").index();
                tabIdxHref = $(this).attr("href");
                //added codes by Cheche 12/13/2021 to remove active class 
                $('.nav-link li.active').removeClass('active');
                $(this).addClass('active');
            }
        })

        setTimeout(function () {

            if (tabIdx != 7 && tabIdx != 4  && tabIdx != 3) {

                doAjax(amode, row, function (m) {

                    $(".loader").hide();

                    //Cheche was here on 2/1/2022 for redirection & editing employee
                    if (m.result == 0) {
                        swal("User information was " + (amode == 1, amode == 2 ? "updated" : "saved") + " successfully!", "", "success");
                        userInfoState.orig = JSON.stringify($('#tabs-1,#tabs-2').extractJson());
                        $("#add_task").removeClass("disabled");//disable for add new without data of the user 04/21/22
                        $("#upload-dwu").removeClass("disabled");
                        $("#upload-pdoc").removeClass("disabled");
                        $("#upload-pit").removeClass("disabled");
	                    $("#search_emp_value").val("");  //clearing text on Search Bar : 11-04-2022 : Cheche


                        addUserMode = 0;

                        //  modified by Cheche 12/15/2021 & modified on 3-9-2022 for continuous redirection 
                        if (amode == 1) {
                            currentId = m.message;
                            if (tabIdx == 0) {

                                if (isSysUser == true) {
                                    var id = m.message;
                                    $("#" + id).click();
                                    $('#' + m.message).trigger('click');
                                    $('#tabs2').trigger('click');

                                    $(this).tabs('active');
                                } else {
                                    var id = m.message;
                                    $("#" + id).click();
                                    $('[href="#tabs-4"]').closest('li').hide();
                                    $('#' + m.message).trigger('click');
                                    $('#tabs2').trigger('click');
                                    $(this).tabs('active');
                                }


                            }
                        }
                        else {
                            if (tabIdx == 0) {

                                if (isSysUser == true) {
                                    var id = m.message;
                                    $("#" + id).click();
                                    $('#' + m.message).trigger('click');
                                    $('#tabs2').trigger('click');
                                    $(this).tabs('active');

                                } else {

                                    $('[href="#tabs-4"]').closest('li').hide();
                                    $('#tabs2').trigger('click');
                                    $(this).tabs('active');
                                }
                            }
                            if (tabIdx == 1) {

                                if (isSysUser == true) {
                                    var id = m.message;
                                    $("#" + id).click();
                                    $('#' + m.message).trigger('click');
                                    $('#tabs3').trigger('click');
                                    $(this).tabs('active');

                                } else {
                                    $('[href="#tabs-4"]').closest('li').hide();
                                    $('#tabs3').trigger('click');
                                    $(this).tabs('active');
                                }
                            }
                            if (tabIdx == 2) {

                                if (isSysUser == true) {
                                    var id = m.message;
                                    $("#" + id).click();
                                    $('#' + m.message).trigger('click');
                                    $('#tabs3').trigger('click');
                                    $(this).tabs('active');
                                } else {
                                    //  $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
                                    $('#tabs3').trigger('click');
                                    $(this).tabs('active');
                                }
                            }

                        }

                        //if (amode == 1 || amode == 2) {

                        //    load_users();
                        //}

                        //modification ends here 
                    }
                    else if (m.result == -2) { // to prevent duplicate record for nonsystem user : Cheche 2/24/2022
                        Swal(m.message, "", "warning");
                        return false;
                    } else if (m.result == -3) {

                        if (m.message == "SSN Duplication") {
                            swal("The person you are associating to this intake has a duplicate Social Security Number", "", "error");
                            //  $.growlWarning({ message: m.message, delay: 6000 });
                            return;
                        }



                    } else if (m.result == -4) {
                        swal(m.message, "", "warning");
                        //  $.growlWarning({ message: m.message, delay: 6000 });
                        return;
                    }

                });

            } else {
                // if tab tabIdx is 4 - for saving employee forms and should stay on same tab when saving every employee form on each tab
                var row = {};
                //var tabIdx2 = $("#empForm").tabs('option', 'selected');

                var tabs = $("#tabsForm").find("a");
                var tabindex;
                var tabIdxHref;

                $.each(tabs, function (i, e) {
                    if ($(this).hasClass("active")) {
                        tabindex = $(this).parents("li").index();
                        tabIdxHref = $(this).attr("href");
                    }
                })
                //alert(tabIdx2);

                doAjaxUsr(amode, row, function (m) {
                    //var tabindex = $("#empForm").tabs('option', 'selected');
                    //if (selectedTabIndex > tabindex) {
                    //    $("#admForm").tabs("option", "selected", selectedTabIndex);
                    //}
                    //else {
                    //selectedTabIndex = tabindex +1;


                    if (isSaveAndPrint == false) {

                        if (tabindex == 0) {
                            swal("Personnel File Checklist  " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 1) {
                            swal("Health Screening Report " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 2) {
                            swal("Criminal Record Statement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 3) {
                            swal("Employee Rights Form " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 4) {
                            swal("Elder Abuse Acknowledgement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 5) {
                            swal("Photography/Video Consent " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 6) {
                            swal("Employee Handbook Receipt " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 7) {
                            swal("Hepatitis B Vaccine Declination Form " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 8) {
                            swal("Personnel Record " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 9) {
                            swal("Request for Live Scan Services " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 10) {
                            swal("Employment Eligibility Verification " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 11) {
                            swal("Payroll Withholding Notice " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                        } else if (tabindex == 12) {
                            swal("Job Description Ackowledgement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");

                        }
                    }
                    $(".loader").hide();
                    //}
                });


            }
            // addUserMode = 0;
        }, 300);
        //debugger
        //load_users();

    });

    //changed this to saveemployeeinfo
    var showdiag = function (mode) {

        var newUserID = $('#userForm #txtId').val(); // cheche 2/18/2022
        var id = $('#userForm #txtId').val();
        var amode = $.isNumeric(id) ? 2 : 1;
        if (mode == 1) {
            $("#diag").dialog("option", 'title', 'Employee');
        }
        $("#diag").dialog("option", {
            width: 1040,
            height: $(window).height() - 200,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        var tabIdx = $("#userForm").tabs('option', 'selected');
                        if (tabIdx != 7 && tabIdx != 4) { // changed from 4 to 7 by Cheche 12/16/2021
                            doAjax(mode, row, function (m) {
                                if (m.result == 0) {
                                    $.growlSuccess({ message: "User " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                    $("#diag").dialog("close");
                                    //$('#grid').trigger('reloadGrid');
                                    if (mode == 1) isReloaded = true;
                                    else {
                                        fromSort = true;
                                        //new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                    }
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (m.result == -4) {
                                    $.growlWarning({ message: m.message, delay: 6000 });
                                    return;
                                }
                            });
                        } else {
                            var row = {};
                            var tabIdx2 = $("#empForm").tabs('option', 'selected');
                            //alert(tabIdx2);

                            doAjaxUsr(2, row, function (m) {
                                var tabindex = $("#empForm").tabs('option', 'selected');
                                //if (selectedTabIndex > tabindex) {
                                //    $("#admForm").tabs("option", "selected", selectedTabIndex);
                                //}
                                //else {
                                //selectedTabIndex = tabindex +1;
                                if (tabindex == 0) {
                                    $.growlSuccess({
                                        message: "Personnel File Checklist  " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 1) {
                                    $.growlSuccess({
                                        message: "Health Screening Report " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 2) {
                                    $.growlSuccess({
                                        message: "Criminal Record Statement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 3) {
                                    $.growlSuccess({
                                        message: "Employee Rights Form " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 4) {
                                    $.growlSuccess({
                                        message: "Elder Abuse Acknowledgement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 5) {
                                    $.growlSuccess({
                                        message: "Photography/Video Consent " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });

                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 6) {
                                    $.growlSuccess({
                                        message: "Employee Handbook Receipt " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 7) {
                                    $.growlSuccess({
                                        message: "Hepatitis B Vaccine Declination Form " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 8) {
                                    $.growlSuccess({
                                        message: "Personnel Record " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 9) {
                                    $.growlSuccess({
                                        message: "Request for Live Scan Services " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 10) {
                                    $.growlSuccess({
                                        message: "Employment Eligibility Verification " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 11) {
                                    $.growlSuccess({
                                        message: "Payroll Withholding Notice " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 12) {
                                    $.growlSuccess({
                                        message: "Job Description Ackowledgement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');

                                }

                                //}
                            });
                        }
                    },
                    class: "primaryBtn",
                    id: "SaveUserFile",
                    text: "Save"
                },
                "Print": {
                    click: function () {
                        var ts = $("#empForm").tabs('option', 'selected');

                        $("#SaveUserFile").click();
                        setTimeout(function () {
                            if (ts == 0) {
                                window.open(ALC_URI.Admin + "/PrintPersonnelFileChecklist?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 1) {
                                window.open(ALC_URI.Admin + "/PrintHealthScreeningReport?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 2) {
                                window.open(ALC_URI.Admin + "/PrintCriminalRecordStatement?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 3) {
                                window.open(ALC_URI.Admin + "/PrintEmployeeRights?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 4) {
                                window.open(ALC_URI.Admin + "/PrintElderAbuseAcknowledgement?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 5) {
                                window.open(ALC_URI.Admin + "/PrintEmpPhotographyConsent?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 6) {
                                window.open(ALC_URI.Admin + "/PrintEmployeeHandbookReceipt?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                            else if (ts == 7) {
                                window.open(ALC_URI.Admin + "/PrintEmpHepatitisForm?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                            else if (ts == 8) {
                                window.open(ALC_URI.Admin + "/PrintPersonnelRecord?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 9) {
                                window.open(ALC_URI.Admin + "/PrintRequestForLiveScanService?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                            else if (ts == 10) {
                                window.open(ALC_URI.Admin + "/PrintEmpEligibilityVerification?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 11) {
                                window.open(ALC_URI.Admin + "/PrintEmployeePayrollWithholding?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                            else if (ts == 12) {
                                window.open(ALC_URI.Admin + "/PrintJobDescAcknowledgement?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                        }, 3000);
                    },
                    id: "EmpFilesPrint",
                    text: "Save & Print"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            },
            resize: function (event, ui) {
                resizeEmpForm();
            }
        }).dialog("open");

        var resizeEmpForm = function () {
            var diag = $('.ui-dialog-content');
            diag = diag.find('#empForm').parents('.ui-dialog-content');
            var height = 0;
            height = diag.eq(0).height() || 0;
            if (height <= 0)
                height = diag.eq(1).height() || 0;
            $('#empForm .tabPanels').height(height - 150);
            //$('#empForm .tabs').height(115).css({ 'overflow': 'hidden' });
            $('#empForm .tabs').height(90).css({ 'overflow': 'hidden' });


        }
        resizeEmpForm();
    }

    $('#txtPass').focus(function () {
        $(this).hide();
        $('#txtPassword').show().focus();
    });
    $('#txtPassword').blur(function () {
        if ($(this).val() == '') {
            //  $(this).hide();
            $("#txtPassword").css("display", "none");/* 3-11-2022*/
            $('#txtPass').show();
        }
    });

    $('#chkIsSysUser').click(function () {

        //added id for pass1 & pass2 to show/hide textbox : 3-22-2022 : Cheche 
        if (this.checked) {
            isSysUser = 1;
            $('#txtPassword').show();
            $('#txtPass').hide();
            $('.non-sys-user').show();
            $('#usersForm .nonsysuser').attr('required', 'required');
            $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
            $("#txtPassword").val("");
        } else {
            isSysUser = 0;
            $('.non-sys-user').hide();
            $('#pw').hide();
            $('#usersForm .nonsysuser').removeAttr('required');
            //  $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
            //  $('[href="#tabs-4"]').closest('li').show();
        }
        $(".confirm_pass").hide();
    });

    $('.toolbar').on('click', 'a', function () {

        var q = $(this);

        if (q.is('.add')) {

            $('#txtId').removeAttr('readonly');
            $('#tabs-1,#tabs-2').clearFields();
            $('#txtPass').hide();
            $('#txtPassword').show();
            $('#chkIsActive').attr('checked', 'checked');
            $('#chkIsSysUser').attr('checked', 'checked');
            $('.non-sys-user').show();
            $('[href="#tabs-4"]').closest('li').show();
            $('[href="#tabs-3"]').closest('li').hide();
            $(".checkbox-grid input").removeAttr('checked').removeAttr('disabled');
            showdiag(1);
        } else if (q.is('.del')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Name + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = row.Name + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3)
                                    $('#users_1 a').trigger('click');
                                //$('#grid').trigger('reloadGrid');
                            });
                        }
                    }, 100);
                }
            } else {
                swal("Please select row to delete.")
            }
        } else if (q.is('.unsign')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');

            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (row.IsActive == true) {
                    if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
                        setTimeout(function () {
                            if (row) {
                                var currId = row.Id;
                                doAjax(4, { Id: row.Id }, function (m) {

                                    if (m.result == 0)
                                        $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                    setTimeout("$('#grid').jqGrid('setSelection','" + currId + "')", 100);

                                    if (m.result != -1 || m.result != -3) {
                                        if (mode == 1) isReloaded = true;
                                        else {
                                            fromSort = true;
                                            //new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                        }
                                        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                    }
                                });
                            }
                        }, 100);
                    }
                } else { $("#lnkdeactivate").attr("disabled", true); }

            } else {
                swal("Please select row to de-activate.");
            }
        } else if (q.is('.copy')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to copy "' + row.Name + '"?')) {

                    $('#txtId').removeAttr('readonly');
                    $('#tabs-1,#tabs-2').clearFields();
                    $('#txtPass').show();
                    //  $('#txtPassword').hide();
                    $("#txtPassword").css("display", "none");
                    $('#chkIsActive').attr('checked', 'checked');
                    if (row.NonSysUser == "0") {
                        $('#chkIsSysUser').attr('checked', 'checked');
                        $('.non-sys-user').show();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
                    } else {
                        $('#chkIsSysUser').removeAttr('checked');
                        $('.non-sys-user').hide();
                        //$('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
                        //  $('[href="#tabs-4"]').closest('li').hide();
                    }
                    if (row) {
                        doAjax(0, row, function (res) {
                            $("#lblSched").html('');
                            $("#lblSched2").html('');
                            //$('.form').mapJson(res);                                              
                            $('#ddlDepartment').val(res.Department);
                            //role permission set primary role
                            $(".checkbox-grid input").filter(function () {
                                return $(this).val() == $('#ddlRole').val()
                            }).attr('checked', true).attr('disabled', true);
                            //set other roles
                            if (res.OtherRoles != null && res.OtherRoles != '') {
                                $.each(res.OtherRoles.split(','), function (x, i) {
                                    $(".checkbox-grid input").filter(function () {
                                        return $(this).val() == i;
                                    }).attr('checked', true);
                                });
                            }
                            //$('#txtCity').val(res.City);
                            //$('#txtState').val(res.State);
                            //$('#txtZip').val(res.Zip);
                            //$('#ddlGender').val(res.Gender);
                            if (res.ShiftStart != null && res.ShiftEnd != null) {
                                $('#txtShiftId').val(res.ShiftId);
                                $('#txtShift').val(res.Shift);
                                $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");
                                $("#lblSched2").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");
                            }
                            showdiag(1);
                        });
                    }
                }
            } else {
                swal("Please select row to copy.")
            }
        } else if (q.is('#lnkUpload')) {
            $("#ImageData").val("");
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            $('#resid').val(id);
            var row = $('#grid').jqGrid('getRowData', id);
            var title = "Upload image of " + row.Name;

            $('#diagupload').dialog({ title: title }).dialog('open');
        }
    });

    $("#upload-resimg").on("click", function () {

        var id = currentId;
        $('#resid').val(id);
        //kim add current image for initial preview 03/23/2022 modified for special char in UserID 05/23/22

        var sample = '#' + id.replace(/(:|\.|\[|\]|,|=|@)/g, '\\$1').trim();
        //   var sample = JSON.stringify(id).replace(/(:|\.|\[|\]|,|=|@)/g, '\\\\$1').trim(); 
        // JSON.stringify(input).replace(/((^")|("$))/g, "").trim();
        // var test1 = $(sample);
        //var test = $("#sample\\.123");
        //   var imagesrc = $('#' + sample);
        var imgSrc = $('#' + id).attr('src');
        if (imgSrc != undefined) {
            $('#image').attr('src', imgSrc);
        }
        else {
            var imgSrc = $(sample).attr('src');
            //  var imgSrc = $("#" + sample).attr('src');
            $('#image').attr('src', imgSrc);
        }

    });


    //add kim for preview image  02/17/22
    $("#ImageData").change(function (e) {

        const file = this.files[0];
        if (file) {
            let reader = new FileReader();
            reader.onload = function (event) {
                $("#image").attr("src", event.target.result);
            };
            reader.readAsDataURL(file);
        }
        else if (file == "" || file == null) {


            //$('#image').attr('src', '');
            //$("#ImageData").val("");
        }

        var val = $(this).val();
        if (val.match(/(?:gif|jpg|png|bmp)$/) || val == "" || val == null) {
            //alert("You are trying to upload an image. Please use the image uploader!");
        }

        else {
            swal('invalid extension!');
            $("#ImageData").val("");
        }

    });

    $("#btnclose").click(function () {
        $('#image').attr('src', '');
        $("#ImageData").val("");
    });


    $("#uploadimg, #btnUploadResImg").click(function () {

        var data = new FormData();
        var files = $("#ImageData").get(0).files;
        if (files.length > 0) {
            //add kim for image only 02/17/22

            data.append("ImageData", files[0]);
            data.append("resid", $("#resid").val());


            $.ajax({
                url: "Admin/UploadImg?func=user",
                type: "POST",
                processData: false,
                contentType: false,
                data: data,
                success: function (response) {
                    $("#uploadresimg_modal").modal("hide");

                    var strhtml = $(".resimg img").attr("src", response.image);
                    var s = $("#" + currentId).attr("src", response.image);

                    //kim modified clear images after saving  02/28/22
                    $('#image').attr('src', '');
                    $("#ImageData").val("");
                },
                error: function (er) {
                    swal(er);
                }

            });

        } else {

            swal("No file chosen");

        }

    })


    //Inserted column SSN on Admin/Employees - by Alona Cabrias June 21 2017
    var load_users = function () {
        //$(function () {
        //this is in site.layout.js.
        //var GURow = new Grid_Util.Row();
        $(".loader").show();
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=user",
            dataType: "json",
            async: false,
            success: function (m) {
                var html_tile = "";
                var gender = "";
                var gender_text;
                if (m) {

                    $.each(m, function (i, v) {
                        if (v.NonSysUser == "1") {
                            v.NonSysUser = "Non-System User";
                        } else if (v.NonSysUser == "0") {
                            v.NonSysUser = "System User";
                        }

                        if (v.IsActive == "1") {
                            v.IsActive = "Active";
                        } else if (v.IsActive == "0") {
                            v.IsActive = "Inactive";
                        }


                        optsDpt.push('<option value="' + v.Department + '">' + v.Department + '</option>');
                        uniqdpt = optsDpt.filter(function (item, pos) {
                            return optsDpt.indexOf(item) == pos;
                        });

                        optsRl.push('<option value="' + v.RoleDescription + '">' + v.RoleDescription + '</option>');
                        uniqrl = optsRl.filter(function (item, pos) {
                            return optsRl.indexOf(item) == pos;
                        });

                        optsTyp.push('<option value="' + v.NonSysUser + '">' + v.NonSysUser + '</option>');
                        uniqtyp = optsTyp.filter(function (item, pos) {
                            return optsTyp.indexOf(item) == pos;
                        });

                        optsStt.push('<option value="' + v.IsActive + '">' + v.IsActive + '</option>');
                        uniqstt = optsStt.filter(function (item, pos) {
                            return optsStt.indexOf(item) == pos;
                        });

                        $('#ftr2').html(uniqrl.join(''));
                        $("#ftr2").html($('#ftr2 option').sort(function (x, y) {
                            return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                        }));
                        $("#ftr2").prepend('<option>All Roles</option>');
                        $("#ftr2").get(0).selectedIndex = 0;
                        $("#ftr2").find('option[value=null]').remove();


                        container[v.Id] = v;

                        gender = "";
                        gender_text = "";

                        if (v.Gender == "M") {
                            gender = '<i class="fas fa-mars pop-icon"></i>';
                            gender_text = 'Male';
                        } else if (v.Gender == "F") {
                            gender = '<i class="fas fa-venus pop-icon"></i>';
                            gender_text = 'Female';
                        } else if (v.Gender == "" || v.Gender == null) {
                            gender = '<i class="fas fa-genderless pop-icon"></i>';
                        }

                        html_tile += '<div class="col-xl-2 col-sm-4 col-md-3 col-xs-12 parent" style="padding-bottom: 10px;padding-top: 10px;padding-right: 0.20% !important;padding-left: 0.20% !important;">';
                        html_tile += '<div class="card m-portlet" style="width: 100%;">';
                        html_tile += '<div style="width:100%;padding:7px;padding-bottom:0px">';
                        
                        if (typeof (btoa) == 'undefined') {
                            

                            html_tile += "<img id='" + v.Id + "' class='imgsrc' src='Resource/EmpImage?id=" + v.Id + "&b=0&r=" + Date.now() + "' alt='" + v.Name + "' width='100%' style='cursor:pointer;height: 11rem;object-fit: contain' data-toggle=\"modal\" data-target=\"#employee_modal\"/>";
                        } else {
                    html_tile += "<img id='" + v.Id + "' class='imgsrc' src='Resource/EmpImage?id=" + btoa(v.Id) + "&b=1&r=" + Date.now() + "' alt='" + v.Name + "' width='100%' style='object-fit: contain' data-toggle=\"modal\" data-target=\"#employee_modal\"/ >";
                }
                        html_tile += '</div><div class="card-body" style="padding-top:5px">';
                        html_tile += '<center><a href="#" style="min-width:80%; text-transform: capitalize;" class="card-title btn btn-sm btn-primary imgname" data-toggle=\"modal\" data-target=\"#employee_modal\" id="' + v.Id + '">' + v.Lastname + " ,<br/>" + v.Firstname + " " + (v.MiddleInitial == "" ? "" : v.MiddleInitial.toUpperCase()) + '</a></center>';
                        html_tile += '<i class="far fa-user pop-icon" style="float:left"></i> ' + '<div style="width:85%;margin-left:5px;float:left">' + v.Id + '</div><br/>';

                        //kim add code here for hover of email address 02/08/22 
                        html_tile += '<p style="font-size:12px; margin-bottom:0" class="card-text topop" data-toggle="popover" data-trigger="hover" title="Email" data-content="' + (v.Email == null || v.Email == '' ? "<span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" : "<span></span>&nbsp;&nbsp;&nbsp;" + v.Email) + '">' + '<i class="fas fa-at pop-icon"></i> <span style="font-size:11px"> Email: ' + (v.Email == null ? 'N/A' : v.Email.substring(0, 8) + '...') + ' </span><br/>';
                        html_tile += '</p>';

                        html_tile += '<i class="fas fa-info pop-icon" style="margin-left:5px"></i><span style="font-size:11px">SSN: ' + (v.SSN != null ? v.SSN : "") + '</span><br/>';

                        html_tile += '<span class="department" style="display:none"> ' + (v.Department != null ? v.Department : " ") + '</span><br/>';
                        html_tile += '<span class="role" style="display:none"> ' + (v.RoleDescription != null ? v.RoleDescription : " ") + '</span><br/>';
                        html_tile += '<span class="shift" style="display:none"> ' + (v.Shift != null ? v.Shift : " ") + '</span><br/>';
                        html_tile += '<span class="type" style="display:none"> ' + (v.NonSysUser != null ? v.NonSysUser : " ") + '</span><br/>';
                        html_tile += '<span class="status" style="display:none"> ' + (v.IsActive != null ? v.IsActive : " ") + '</span><br/>';

                        //html_tile += '<a href="#" class="btn btn-primary">View File</a>';
                        html_tile += '</div>';
                        html_tile += '</div>';
                        html_tile += '</div>';
                    })
                }

                $("#userstile").html(html_tile);
                html_tile =""
                setHeight();
                //kim add code 02/08/22
                $(".topop, .discharge").popover({
                    html: true,
                    trigger: 'hover',
                    delay: { "show": 200, "hide": 100 }
                });
                //$(".card").popover({
                //    html: true
                //});
                
                $(".imgsrc, .imgname").click(function (rowid, iRow, iCol, cellText, e) {
                    
                    // added this to hide the red asterisk for password & confirm password: angelie 03-18-22
                    $(".pass_and_confirm").css("display", "none");

                    var G = $('#grid');
                    // $('#userForm #txtId').val(2);
                    //var this_class = $(this).attr("class");
                    var tbs = $("#tabs").find("a");
                    $(".resdform").show();
                    $.each(tbs, function (i, e) {

                        $(this).removeClass("active");
                        $(this).removeClass("show");
                    });

                    $("#tabsSelect").get(0).selectedIndex = 0;

                    var tbs2 = $("#tabsviews").find(".tab-pane");
                    $.each(tbs2, function (i, e) {
                        if ($(this).attr("id") != "tabs-1a")
                            $(this).removeClass("active");
                    })

                    $.each(tbs, function (i, e) {
                        if ($(this).attr("href") == "#tabs-1") {
                            $(this).click();
                        }

                    })


                    rowid = $(this).attr("id");

                    currentId = rowid; //latest clicked employee


                    //G.setSelection(rowid, false);

                    $('#tabs-1,#tabs-2').clearFields();
                    $('#txtId').attr('readonly', 'readonly');
                    $('#txtPass').show();
                    $("#txtPassword").css("display", "none");
                    // $('#txtPassword').hide();

                    var row = container[currentId];

                    
                    var htmll = "";

                    if (typeof (btoa) == 'undefined')
                        htmll += "<img class='imgsrc' src='Resource/EmpImage?id=" + row.Id + "&b=0&r=" + Date.now() + "' alt='" + row.Name + "' width='100%' style='cursor:pointer;border-radius:5%; height=200px''/>";
                    else
                        htmll += "<img class='imgsrc' src='Resource/EmpImage?id=" + btoa(row.Id) + "&b=1&r=" + Date.now() + "' alt='" + row.Name + "' width='100%' style='cursor:pointer;border-radius:5%; height=200px''/>";

                    $(".resdform .resimg").html(htmll);

                    var dow = row.DaySchedule;
                    newRecurrence = row.DaySchedule;

                    if (dow != null) {

                        $('.dayofweek2').find('input[type="checkbox"]').each(function () {
                            var d = $(this);
                            switch (d.attr('name')) {
                                case 'Sun': setCheck2(d, dow, '0'); break;
                                case 'Mon': setCheck2(d, dow, '1'); break;
                                case 'Tue': setCheck2(d, dow, '2'); break;
                                case 'Wed': setCheck2(d, dow, '3'); break;
                                case 'Thu': setCheck2(d, dow, '4'); break;
                                case 'Fri': setCheck2(d, dow, '5'); break;
                                case 'Sat': setCheck2(d, dow, '6'); break;
                            }
                        });
                    }

                    $('a.checkall').attr('data-toggle', 'cc').html('Check All Days');

                    if (dow == "0,1,2,3,4,5,6") {
                        $('a.checkall').attr('data-toggle', 'dd').html('Uncheck All Days');

                    }

                    if (row.NonSysUser == "System User") {

                        $('#chkIsSysUser').attr('checked', 'checked');
                        $('.non-sys-user').show();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
                        $('[value="#tabs-3"],[value="#tabs-4"]').closest('option').show();
                    } else {
                        $('#chkIsSysUser').removeAttr('checked');
                        $('.non-sys-user').hide();
                        //    $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
                        // $('[href="#tabs-4"]').closest('li').hide();
                        // $('[value="#tabs-3"],[value="#tabs-4"]').closest('option').hide();
                        //  $('[value="#tabs-4"]').closest('option').hide();
                    }
                    $(".checkbox-grid input").removeAttr('checked').removeAttr('disabled');

                    if (row) {

                        $(".loader").show();
                        doAjax(0, row, function (res) {

                            if (res.IsSysUser == true) isSysUser = 1;
                            var zip = res.Zip.split("+++");
                            res.Zip = zip[0];
                            res.Password = zip[1];
                            res.ConfirmPassword = zip[1];

                            $("#txtPassword").val(zip[1]);
                            $("#txtConfirm").val(zip[1]);

                            if (res.State != null) {
                                res.State = res.State.trim();
                            }

                            $("#lblSched").html('');
                            $("#lblSched2").html('');
                            $('#tabs-1,#tabs-2').mapJson(res);
                            $("#empForm").mapJson(res, 'id', "#empForm");
                            $("#empForm #JDtitle").val(res.HSRuserposition);
                            if (res.ShiftStart != null && res.ShiftEnd != null)
                                $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");
                            $("#lblSched2").html("(" + res.ShifDays + " | "+ res.ShiftStart + ' - ' + res.ShiftEnd + ")");

                            if (res.Role == null || $('#carestaffRoles').val().split(",").indexOf(res.Role.toString()) == -1) {
                                //hide service task tab for roles that are heads
                                //comment this to show service task tab for all employees - alona (5-4-22)
                                //$('[href="#tabs-3"]').closest('li').hide();
                            } else {
                                if (res.IsSysUser == false) { /*3-14-2022 : Cheche*/
                                    // $('[href="#tabs-3"]').closest('li').hide();
                                } else {
                                    $('[href="#tabs-3"]').closest('li').show();
                                }
                            }
                            $("#add_task").removeClass("disabled");//disable for add new without data of the user 04/21/22
                            $("#upload-dwu").removeClass("disabled");
                            $("#upload-pdoc").removeClass("disabled");
                            $("#upload-pit").removeClass("disabled");

                            $("#txtEmail").val(res.Email);
                            $("#ddlGender").val(res.Gender);

                            if (agency_ishomecare == "True") {
                                $("#emp_modal_lbl").html("Caregiver - " + row.Name);
                            } else {
                                $("#emp_modal_lbl").html("Employee - " + row.Name);
                            }
                            //$("#diag").dialog("option", 'title', 'Employee - ' + row.Name);
                            $('#ddlRole').val(res.Role);
                            roleid_before_update = res.Role;

                            $('#ddlDepartment').val(res.Department);

                            //role permission set primary role
                            $(".checkbox-grid input").filter(function () {
                                return $(this).val() == $('#ddlRole').val()
                            }).attr('checked', true).attr('disabled', true);

                            //set other roles

                            $.each(res.OtherRoles.split(','), function (x, i) {
                                $(".checkbox-grid input").filter(function () {
                                    return $(this).val() == i;
                                }).attr('checked', true);
                            });

                            $(".loader").hide();
                        });


                    }


                });

                $(".loader").hide();

            }
        });

        setTimeout(function () {
            $(".loader").hide();
        }, 500);




        //$('#grid').jqGrid({
        //    url: "Admin/GetGridData?func=user",
        //    datatype: 'json',
        //    postData: "",
        //    ajaxGridOptions: { contentType: "application/json", cache: false },
        //    //datatype: 'local',
        //    //data: dataArray,
        //    //mtype: 'Get',
        //    colNames: [' ', 'User ID', 'Name', 'Email', 'SSN', 'Role', 'Department', 'Is Active', 'User Type', 'Shift', 'Date Created', 'DaySched', 'DaySchedDisplay', 'Date Updated', 'Employee', 'Department', 'Shift'],
        //    colModel: [
        //        {
        //            key: false,
        //            name: 'Image',
        //            index: 'Image',
        //            width: 150,
        //            fixed: true,
        //            formatter: imageFormatter,
        //            //cellattr: setTitle
        //        },
        //      { hidden: true, key: true, name: 'Id', index: 'Id' },

        //      {
        //          hidden: true, key: false, name: 'Name', index: 'Name', sortable: true, formatter: "dynamicLink", formatoptions: {
        //              //cellValue: function (val, rowid, row, options) {
        //              //    var name = [];
        //              //    if (row.Firstname != "")
        //              //        name.push(row.Firstname + " ");
        //              //    if (row.MiddleInitial != "")
        //              //        name.push(row.MiddleInitial + " ");
        //              //    if (row.Lastname != "")
        //              //        name.push(row.Lastname + " ");
        //              //    return name.join('');
        //              //},
        //              onClick: function (rowid, iRow, iCol, cellText, e) {

        //                  var G = $('#grid');
        //                  G.setSelection(rowid, false);
        //                  $('#tabs-1,#tabs-2').clearFields();
        //                  $('#txtId').attr('readonly', 'readonly');
        //                  $('#txtPass').show();
        //                  $('#txtPassword').hide();
        //                  var row = G.jqGrid('getRowData', rowid);
        //                  if (row.NonSysUser == "0") {
        //                      $('#chkIsSysUser').attr('checked', 'checked');
        //                      $('.non-sys-user').show();
        //                      $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
        //                  } else {
        //                      $('#chkIsSysUser').removeAttr('checked');
        //                      $('.non-sys-user').hide();
        //                      $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
        //                  }
        //                  $(".checkbox-grid li input").removeAttr('checked').removeAttr('disabled');
        //                  if (row) {
        //                      doAjax(0, row, function (res) {
        //                          $("#lblSched").html('');
        //                          $('#tabs-1,#tabs-2').mapJson(res);
        //                          if (res.ShiftStart != null && res.ShiftEnd != null)
        //                              $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");

        //                          if (res.Role == null || $('#carestaffRoles').val().split(",").indexOf(res.Role.toString()) == -1) {
        //                              $('[href="#tabs-3"]').closest('li').hide();
        //                          } else {
        //                              $('[href="#tabs-3"]').closest('li').show();
        //                          }
        //                          $.trim("#txtState");
        //                          showdiag(2);
        //                          var title = $("#diag").dialog("option", 'title');
        //                          $("#diag").dialog("option", 'title', 'Employee - ' + row.Name);
        //                          //role permission set primary role
        //                          $(".checkbox-grid li input").filter(function () {
        //                              return $(this).val() == $('#ddlRole').val()
        //                          }).attr('checked', true).attr('disabled', true);
        //                          //set other roles
        //                          $.each(res.OtherRoles.split(','), function (x, i) {
        //                              $(".checkbox-grid li input").filter(function () {
        //                                  return $(this).val() == i;
        //                              }).attr('checked', true);
        //                          });
        //                      });
        //                  }
        //              }
        //          }
        //      },
        //      { hidden: true, key: false, name: 'Email', index: 'Email', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'SSN', index: 'SSN', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'RoleDescription', index: 'RoleDescription', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'Department', index: 'Department', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'IsActive', index: 'IsActive', sortable: true, edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Active', '0': 'Inactive' } } },
        //      { hidden: true, key: false, name: 'NonSysUser', index: 'NonSysUser', sortable: true, edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Non-System User', '0': 'System User' } } },
        //      { hidden: true, key: false, name: 'Shift', index: 'Shift', editable: false, sortable: false },
        //      { hidden: true, key: false, name: 'DateCreated', index: 'DateCreated', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'DateModified', index: 'DateModified', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'DaySchedule', index: 'DaySchedule', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'DayScheduleDisplay', index: 'DayScheduleDisplay', editable: true, sortable: true },
        //      { key: false, name: 'Info', index: 'Name', formatter: userFormatter },
        //      { key: false, name: 'Department', index: 'Department', editable: true, sortable: true, formatter: dataFormatter },
        //      { key: false, name: 'Shift', index: 'Shift', editable: true, sortable: true, formatter: shiftFormatter },
        //    ],
        //    //pager: jQuery('#pager'),
        //    rowNum: 1000000,
        //    rowList: [10, 20, 30, 40],
        //    height: '100%',
        //    viewrecords: true,
        //    gridview: true,
        //    //caption: 'Care Staff',
        //    mtype: "GET",
        //    emptyrecords: 'No records to display',
        //    //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
        //    autowidth: true,
        //    multiselect: false,
        //    sortname: 'Name',
        //    sortorder: 'asc',
        //    loadonce: true,
        //    onSortCol: function () {
        //        fromSort = true;
        //        GURow.saveSelection.call(this);
        //    },
        //    loadComplete: function (data) {

        //        

        //        if ($('#ftrRole').find('option').size() <= 0) {
        //            var optsRm = [];

        //            $.each(unique($.map(data, function (o) { return o.RoleDescription })), function () {
        //                optsRm.push('<option>' + this + '</option>');
        //            });
        //            $('#ftrRole').html(optsRm.join(''));

        //            $("#ftrRole").html($('#ftrRole option').sort(function (x, y) {
        //                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
        //            }));
        //            $("#ftrRole").prepend('<option>All</option>');
        //            $("#ftrRole").get(0).selectedIndex = 0;
        //        }
        //        if ($('#ftrDepartment').find('option').size() <= 0) {
        //            optsRm = [];

        //            $.each(unique($.map(data, function (o) { return o.Department })), function () {
        //                optsRm.push('<option>' + this + '</option>');
        //            });
        //            $('#ftrDepartment').html(optsRm.join(''));

        //            $("#ftrDepartment").html($('#ftrDepartment option').sort(function (x, y) {
        //                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
        //            }));
        //            $("#ftrDepartment").prepend('<option>All</option>');
        //            $("#ftrDepartment").get(0).selectedIndex = 0;
        //        }

        //        if (typeof fromSort != 'undefined') {
        //            GURow.restoreSelection.call(this);
        //            delete fromSort;
        //            return;
        //        }

        //        var maxId = data && data[0] ? data[0].Id : '';
        //        //if (typeof isReloaded != 'undefined') {
        //        //    $.each(data, function (k, d) {
        //        //        if (d.Id > maxId) maxId = d.Id;
        //        //    });
        //        //}
        //        //if (maxId > 0)
        //        $("#grid").setSelection(maxId);
        //        delete isReloaded;
        //    }
        //});
        //$("#grid").jqGrid('bindKeys');
        //jQuery("#grid").jqGrid('sortableRows');
        //function unique(array) {
        //    return $.grep(array, function (el, index) {
        //        return index == $.inArray(el, array);
        //    });
        //}
        //  });
    }

    function imageFormatter(cellValue, options, rowObject) {
        if (typeof (btoa) == 'undefined')
            return "<img class='imgsrc' src='Resource/EmpImage?id=" + rowObject.Id + "&b=0&r=" + Date.now() + "' alt='" + rowObject.Name + "' width='100%' style='cursor:pointer;'/>";
        else
            return "<img class='imgsrc' src='Resource/EmpImage?id=" + btoa(rowObject.Id) + "&b=1&r=" + Date.now() + "' alt='" + rowObject.Name + "' width='100%' style='cursor:pointer;'/>";
    }

    function userFormatter(cellValue, options, rowObject) {

        var html = "";

        html += "<br><label class='userData' style='font-style:italic;'>" + (rowObject.Id || "") + "</label>";
        html += "<br><br><label class='imgname_p' ><a class='imgname'>" + (rowObject.Name || "") + "</a></label>";
        html += "<br><label class='userData'><strong>" + (rowObject.Email || "") + "</strong></label>";
        html += "<br><label class='userData'>SSN: <strong>" + (rowObject.SSN || "") + "</strong></label>";
        html += "<br><label style='color:red;'class='userData'><strong>" + (rowObject.IsActive == 0 ? "Inactive" : " ") + "</strong></label>";

        return html;

    }

    function dataFormatter(cellValue, options, rowObject) {

        var usertype = rowObject.NonSysUser;

        var html = "";

        html += "<br><label class='imgname_p' ><a class='imgname'>" + (rowObject.Department || "") + "</a></label>";
        html += "<br><br><label class='userData'>Role: <strong>" + (rowObject.RoleDescription || "") + "</strong></label>";
        html += "<br><label class='userData'>Created By: <strong>" + (rowObject.CreatedBy || "") + "</strong></label>";
        html += "<br><label class='userData'>Date Created: <strong>" + (rowObject.ShiftDateCreated || "") + "</strong></label>";

        return html;

    }

    function shiftFormatter(cellValue, options, rowObject) {

        //var usertype = rowObject.NonSysUser;

        var html = "";

        html += "<br><label class='imgname_p' >" + (rowObject.ShiftType || "") + "</a></label>";
        html += "<br><br><label class='userData'>Shift: <strong>" + (rowObject.Shift || "") + "</strong></label>";
        html += "<br><label class='userData'>Shift Days: <strong>" + (rowObject.DayScheduleDisplay || "") + "</strong><br><br></label>";


        return html;

    }

    //END

    var SearchShift = "";

    $("#txtShift").val(SearchShift).blur(function (e) {

        if ($(this).val().length == 0) {

            $(this).val(SearchShift);
            $('#txtShiftId').val('-1');
            $("#lblSched").html("");
            $("#lblSched2").html("");
        }


    }).focus(function (e) {

        if ($(this).val() == SearchShift)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "shift", name:"' + request.term + '"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id,
                            start: item.start,
                            end: item.end
                        }
                    }));
                }
            });
        },

        delay: 200,
        minLength: 0,
        focus: function (event, ui) {
            $("#txtShift").val(ui.item.label);
            $("#txtShiftId").val(ui.item.value);
            $("#lblSched").html("(" + ui.item.start + ' - ' + ui.item.end + ")");
            $("#lblSched2").html("(" + ui.item.start + ' - ' + ui.item.end + ")");
            return false;
        },
        change: function (event, ui) {

        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li>")
            .data("ui-autocomplete-item", item)
            .append("<a><span>" + item.label + ' - (' + item.start + ' - ' + item.end + ")</span></a>")
            .appendTo(ul);


    };


    $('#btn-printsr').click(function (e) {
        var prt = $('#printsr').val();
        if ($('#userForm #txtId').val()) {
            if (prt == "pfc") {
                window.open(ALC_URI.Admin + "/PrintPersonnelFileChecklist?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "hsr") {
                window.open(ALC_URI.Admin + "/PrintHealthScreeningReport?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "pf") {
                window.open(ALC_URI.Admin + "/PrintEmpPhotographyConsent?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "hb") {
                window.open(ALC_URI.Admin + "/PrintEmpHepatitisForm?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "csr") {
                window.open(ALC_URI.Admin + "/PrintCriminalRecordStatement?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "eh") {
                window.open(ALC_URI.Admin + "/PrintEmployeeHandbookReceipt?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "er") {
                window.open(ALC_URI.Admin + "/PrintEmployeeRights?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "eaa") {
                window.open(ALC_URI.Admin + "/PrintElderAbuseAcknowledgement?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "per") {
                window.open(ALC_URI.Admin + "/PrintPersonnelRecord?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "rls") {
                window.open(ALC_URI.Admin + "/PrintRequestForLiveScanService?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "eev") {
                window.open(ALC_URI.Admin + "/PrintEmpEligibilityVerification?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "pw") {
                window.open(ALC_URI.Admin + "/PrintEmployeePayrollWithholding?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "sjd") {
                window.open(ALC_URI.Admin + "/PrintJobDescAcknowledgement?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }

        }
        //else {
        //    alert('Resident has no Admission');
        //    return;
        //}
    });

    $('#AddServiceToolbar').on('click', 'a', function () {

        var q = $(this);
        if (q.is('.add')) {
            AddService();
        } else if (q.is('.del')) {
            var id = $('#sGrid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#sGrid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax2(3, { Id: row.Id }, function (m) {
                                var msg = "Task deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = "Task cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3) {
                                    fromSort = true;
                                    //new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                    $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                }
                            });
                        }
                    }, 100);
                }
            } else {
                swal("Please select row to delete.")
            }
        } else if (q.is('.refresh')) {
            fromSort = true;
            //new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
            //$('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
        }
    });
    $("#diagTask").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Edit Service Task",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });

    $('#txtTimeIn').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#txtCarestaff').empty();

                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#Onetimedate').trigger('change');
        }
    });
    //$('#txtTimeIn').mask('99:99 xy');

    $(document).on("click", "#savesvctask", function () {

        var row = container[currentId];
        var m = $("#mode").val();
        var mode = 1;
        var isReassign = true;
        if (m == "2") mode = 2;
        if (svc_mode == "add") {
            mode = 1;
            isReassign = false;
        }

        doAjax2(mode, row, function (m) {
            if (m.result == 0) {

                if (mode == 1 && isReassign == true) {

                    swal("The task was reassigned successfully!", "", "success");

                    var id = currentActivityId;

                    var dDate = new Date(Date.now());
                    var uDate = (dDate.getMonth() + 1) + "/" + dDate.getDate() + "/" + dDate.getFullYear();
                    var row = {};

                    row.id = id;

                    row.active_until = uDate;
                    var data = {
                        func: "activity_schedule",
                        mode: 3,
                        data: row
                    };

                    $.ajax({

                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify(data),
                        dataType: "json",
                        async: false,
                        success: function (m) {


                            if (m.result == 0) {

                                //swal("Task deactivated successfully!", "", "success");
                                $("#closeTaskModal").click();

                                var counter = viewConflictTasks(residentID, newShift, changedrec);
                                if (counter == 0) {
                                    $(".loader").show();
                                    setTimeout(function () {
                                        $("#employee_conflicttasks_modal #proceed").trigger('click');
                                        $(".loader").hide();
                                    }, 300)
                                }
                            }
                        }
                    });


                } else {

                    if (svc_mode == "add") {
                        swal("The task was added successfully!", "", "success");
                        $("#closeTaskModal").click();
                        $(".refreshTask").click();

                    } else {
                        swal("The task was updated successfully!", "", "success");
                    }



                    loadSvcTask(currentId);
                }


            } else if (m.result == -2) {
                var resVisible = $('#txtResidentId').is(':visible');
                swal("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", "", "error");
                //there is already a schedule for a particular resident or room
            }

            svc_mode = "";
        });


    })

    //delete
    var showdiagTask = function (mode) {
        $("#diagTask").dialog("option", {
            width: 500,
            height: 500,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax2(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diagTask").dialog("close");
                                //initService();
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    //new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                }
                                //$('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');

                            } else if (m.result == -2) {
                                var resVisible = $('#txtResidentId').is(':visible');
                                $.growlWarning({ message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", delay: 6000 });
                                //there is already a schedule for a particular resident or room
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diagTask").dialog("close");
                }
            }
        }).dialog("open");
    }

    var doAjax2 = function (mode, row, cb) {

        //get record by id
        if (mode == 0) {

            var data = {
                func: "activity_schedule",
                id: row.Id
            };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        //var isValid = $('#diagTask .form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            //if (!isValid) {
            //    return;
            //}
            row = $('#servicetask_modal .form').extractJson();
            //validate time

            //var aTask = $('#servicetask_modal #txtTimeIn').val().split(":");
            //var aTime = aTask[0] + ":00 " + aTask[1];
            //if (aTask[0] < 10) {
            //    aTime = "0" + aTask[0] + ":" + aTask[1];
            //    // aTime =  aTask[0] + ":" + aTask[1];

            //} else aTime = $('#servicetask_modal #txtTimeIn').val();



            row.ScheduleTime = $('#servicetask_modal #txtTimeIn').val();

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];
            delete row['recurring'];
            delete row['Activity'];
            delete row['Dept'];
            delete row['Onetimedate'];

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring) {
                row.isonetime = '0';
                $('.dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    if (d.is(':checked')) {
                        switch (d.attr('name')) {
                            case 'Sun': dow.push('0'); break;
                            case 'Mon': dow.push('1'); break;
                            case 'Tue': dow.push('2'); break;
                            case 'Wed': dow.push('3'); break;
                            case 'Thu': dow.push('4'); break;
                            case 'Fri': dow.push('5'); break;
                            case 'Sat': dow.push('6'); break;
                        }
                    }
                });

                if (dow.length == 0) {
                    swal('Please choose day of week schedule.');
                    return;
                }

            } else {

                row.isonetime = '1';
                var onetimeval = $('#Onetimedate').val();
                if (onetimeval == '') {
                    swal("Please choose a one time date schedule.");
                    return;
                }

                var onetimeday = moment(onetimeval).day();
                if ($.isNumeric(onetimeday))
                    dow.push(onetimeday.toString());

                //var aTask = $('#txtTimeIn').val().split(":");
                //var aTime = "";

                //if (aTask[0] < 10 && aTask[0].length == 1) {
                //    aTime = "0" + aTask[0] + ":" + aTask[1];
                //    //aTime = aTask[0] + ":" + aTask[1];
                //} else aTime = $('#txtTimeIn').val();

                row.ScheduleTime = onetimeval + ' ' + $('#txtTimeIn').val();

                var today = new Date(Date.now());
                var new_schedTime = new Date(row.ScheduleTime);

                if (new_schedTime < today) {
                    swal("Selected date and time have passed already. Please select again.", "", "warning");
                    return;
                }
            }
            row.Recurrence = dow.join(',');
            row.addedFromEmployee = 1;


        }

        //kim add code if the carestaff is not the same with the selected employee it will not allow to save 12/29/2021
        if (svc_mode == "add") {

            // kim modified 02/24/22
            if ($('#txtCarestaff').val() == "") {
                //swal("Please select another carestaff")
                swal("Please select a new time/day schedule.");

                return;
            }
        }
        else {
            if ($('#servicetask_modal #txtCarestaff').val() == "" || $('#servicetask_modal #txtCarestaff').val() == null) {
                swal("Please select a new time/day schedule.")
                return;
            }
        }

        var data = { func: "activity_schedule", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            async: false,
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }
    var activity = function () {

    }
    $('#txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#viewActivity').val(title);
        $('#txtCarestaff')[0].options.length = 0;
        toggleCarestafflist();
    });

    var bindDept = function (dptid, cb) {

        var data = { deptid: dptid };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetServicesAndCarestaffByDeptId",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#txtActivity').empty();
                $('#viewActivity').val('');
                var opt = '<option val=""></option>';
                $.each(m.Services, function (i, d) {
                    opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
                });
                $('#txtActivity').append(opt);
                if (typeof (cb) !== 'undefined') cb();
            }
        });
    }

    var toggleCarestafflist = function () {
        var isrecurring = $('input[name=recurring]:checked', '#servceTask').val();
        if (isrecurring == 1)
            triggerDayofWeekClick();
        else
            $('#Onetimedate').trigger('change');

        //if housekeeper or maintenance hide room dropdown
        var val = $('#ddlDept').val();
        if (val == 5 || val == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }
    //when combobox selected

    if ($('#ddlDept').length > 0) {
        $('#ddlDept').change(function () {
            var val = $('#ddlDept').val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }



    $('input[name="recurring"]').click(function () {
        var d = $(this);
        $('input[name="recurring"]').removeAttr("checked");
        $(this).attr("checked", true);
        if (d.val() == 1) {
            $('table.dayofweek input').removeAttr('disabled');
            $('#Onetimedate').attr('disabled', 'disabled').val('');
            $('input[name="recurring"]:eq(0)').attr('checked');
            $('input[name="recurring"]:eq(1)').removeAttr('checked');
        } else {
            $('table.dayofweek input').attr('disabled', 'disabled');
            $('table.dayofweek').clearFields();
            $('#Onetimedate').removeAttr('disabled');
            $('input[name="recurring"]:eq(0)').removeAttr('checked');
        }
    });

    var triggerDayofWeekClick = function () {

        var a = $('#txtTimeIn').val();
        if (a == null || a == '') {
            $('#txtCarestaff').empty();

            //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
            return;
        }

        var dow = [];
        $('.dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun': dow.push('0'); break;
                    case 'Mon': dow.push('1'); break;
                    case 'Tue': dow.push('2'); break;
                    case 'Wed': dow.push('3'); break;
                    case 'Thu': dow.push('4'); break;
                    case 'Fri': dow.push('5'); break;
                    case 'Sat': dow.push('6'); break;
                }
            }
        });
        bindCarestaff(dow);

    }

    $('table.dayofweek input').click(triggerDayofWeekClick);
    $('#txtTimeIn').change(toggleCarestafflist);

    var bindCarestaff = function (dow) {

        if ($('#txtTimeIn').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;

            return;
        }
        if ($('#txtActivity').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;

            return;
        }
        if (dow == null) dow = [];
        var csid = $('input[xid="hdCarestaffId"]').val();
        if (csid == "") csid = null;

        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');

        var data = { scheduletime: $('#txtTimeIn').val(), activityid: $('#txtActivity').val(), carestaffid: csid, recurrence: dow.join(',') };
        data.isonetime = isrecurring ? "0" : "1";
        //data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#Onetimedate').val()).format("MM/DD/YYYY");
        data.clientdt = moment($('#Onetimedate').val()).format("MM/DD/YYYY");
        data.type = 1;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                $('#txtCarestaff').empty();

                //  $('#txtCare').val("No Employee is available for this time and day");
                $('<option/>').val('').html('').appendTo('#txtCarestaff');
                $.each(m, function (i, d) {



                    // kim add code for filter of employee and alert 12/29/2021
                    if (svc_mode == "add") {
                        $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                        //var fn = $("#txtFirstname").val() + (" ") + $("#txtLastname").val();
                        //var check = fn;

                        //var avail = "This employee is unavailable on this time and day";
                        //var cn = $('#txtCarestaff').text();
                        //var cc = $('#txtCarestaff').val(d.Id);

                        //var ch = "dsadaddasdds";
                        //var sa = cn.includes(check);
                        //if (sa == true) {
                        //    $('#txtCare').val(check);
                        //    return false;
                        //}
                        //if (sa == false || cn == "" || cn == null || cc == "" || cc == null || ch == 0) {
                        //    $('#txtCare').val(avail);

                        //    return false;
                        //}
                        //if ($("#txtCarestaff ")[0].selectedIndex <= 0) {
                        //    $('#txtCare').val(avail);
                        //    return false;
                        //}


                    }
                    else {
                        $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                    }
                    // added this to disable selection of carestaff: angelie, 04-11-22
                    if (d.Id == currentId) {
                        $('#txtCarestaff').val(currentId);
                        //$("#txtCarestaff").prop("disabled", true);
                        //$("#carestaff_div").prop("hidden", true);
                        //$("#no_carestaff_available").prop("hidden", true);
                    }

                });

                if ($('#txtCarestaff').val() == "") {
                    $("#no_carestaff_available").prop("hidden", false);
                } else {
                    $("#no_carestaff_available").prop("hidden", true);
                }

            }
        });
    }


    $('#Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff(dow);
    });


    var AddService = function () {

        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#txtId').val();
            if (!id) return;
        }
        $("#ddlDept option[value='5']").remove();
        $("#ddlDept option[value='7']").remove();
        var dept = $('#ddlDept').val();
        $('#diagTask').clearFields();
        $('#liResident select').val(id);
        $('input[xid="hdCarestaffId"]').val('');
        $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
        $('input[name="recurring"]:eq(1)').removeAttr('checked');
        $('#Onetimedate').attr('disabled', 'disabled');
        $('.dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
        showdiagTask(1);
        //if housekeeper or maintenance hide room dropdown
        $('#ddlDept').val(dept);
        if (dept == 5 || dept == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    var initRoles = function () {

    }

    var loadSvcTask = function (id) {    
        roleId = $('#ddlRole').val();
        containerTask = {};


        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=activity_schedule&param3=" + id + "&param9=" + roleId,
            //url: "Admin/GetGridData?func=activity_schedule&param3=" + id ,
            dataType: "json",
            success: function (m) {
                var html_table = "";
                var svcIdx = "";


                var dtnow = new Date(Date.now());

                if (m.length != 0) {

                    $.each(m, function (i, v) {
                        svcIdx = currentId + "_" + v.Id;
                        containerTask[svcIdx] = v;

                        var nDate;
                        if (v.ActiveUntil != null) {
                            nDate = new Date(v.ActiveUntil);
                        }

                        if ((v.IsActive == true) || ((v.IsActive == false) && (nDate > dtnow))) {

                            html_table += "<tr id='" + v.Id + "'>";
                            html_table += "<td><a href='#' class='svctask' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'>" + v.Activity + "</a></td>";
                            html_table += "<td>" + v.Department + "</td>";
                            html_table += "<td>" + (v.Resident == null ? "N/A" : v.Resident) + "</td>";
                            html_table += "<td>" + v.Carestaff + "</td>";
                            html_table += "<td>" + (v.Room == null ? "N/A" : v.Room) + "</td>";
                            html_table += "<td>" + v.Schedule + "</td>";
                            html_table += "<td>" + v.Recurrence + "</td>";
                            html_table += "<td>" + v.Duration + " minutes</td>";
                            html_table += "<td>" + v.DateCreated + "</td>";
                            html_table += "</tr>";
                        }
                    })
                }

                $("#sGrid").DataTable().destroy();
                $("#sGridtbl").html("");               
                $("#sGridtbl").html(html_table);

                serviceTask = $("#sGrid").DataTable({
                    responsive: !0,
                    pagingType: "full_numbers",
                    dom: 'lBfrtip',
                    //"scrollX": true,
                    //"scrollCollapse": true,
                    select: true,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            text: '<i class="la la-download"></i> Export to Excel',
                            title: 'ALC_Employees_ServiceTask_' + id,
                            className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',

                            init: function (api, node, config) {
                                $(node).removeClass('dt-button')
                            }
                        }, {
                            text: 'Refresh',
                            action: function (e, dt, node, config) {
                                loadSvcTask(currentId);
                            },
                            className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air refreshTask',
                            init: function (api, node, config) {
                                $(node).removeClass('dt-button')
                            }
                        }


                    ]
                });

                $(".loadSvcTaskHm").prop("hidden", true);
            }
        });
    }

    var initService = function () {

        //if ($('#sGrid')[0] && $('#sGrid')[0].grid)
        //    $.jgrid.gridUnload('sGrid');
        //var SGrid = $('#sGrid');

        //var GURow = new Grid_Util.Row();
        //var pageWidth = $("#sGrid").parent().width() - 100;

        var id = currentId;
        if (!id) {
            id = $('#txtId').val();
            if (!id) return;
        }

        loadSvcTask(id);


        //SGrid.jqGrid({
        //    url: "Admin/GetGridData?func=activity_schedule&param3=" + id,// + $('#ftrRecurrence').val(),
        //    datatype: 'json',
        //    postData: '',
        //    ajaxGridOptions: { contentType: "application/json", cache: false },
        //    //datatype: 'local',
        //    //data: dataArray,
        //    //mtype: 'Get',
        //    colNames: ['Id', 'Activity', 'Department', 'Resident', 'Staff', 'Room', 'Schedule', 'Recurrence', 'Duration (Minutes)', 'Date Created',  /*'Completed', 'Remarks'*/],
        //    colModel: [
        //      { key: true, hidden: true, name: 'Id', index: 'Id' },
        //      {
        //          key: false, name: 'Activity', index: 'Activity', sortable: true, width: (pageWidth * (8 / 100)), formatter: "dynamicLink", formatoptions: {
        //              onClick: function (rowid, irow, icol, celltext, e) {
        //                  
        //              }
        //          }
        //      },
        //      { key: false, name: 'Department', index: 'Department', sortable: true, width: (pageWidth * (8 / 100)) },
        //      { key: false, name: 'Resident', index: 'Resident', sortable: true, width: (pageWidth * (6 / 100)) },
        //      { key: false, name: 'Carestaff', index: 'Carestaff', sortable: true, width: (pageWidth * (6 / 100)) },
        //      { key: false, name: 'Room', index: 'Room', sortable: true, width: (pageWidth * (4 / 100)) },
        //      //{ key: false, name: 'ScheduleDate', index: 'ScheduleDate' },
        //      { key: false, name: 'Schedule', index: 'Schedule', sortable: true, width: (pageWidth * (8 / 100)) },
        //       { key: false, name: 'Recurrence', index: 'Recurrence', sortable: true, width: (pageWidth * (8 / 100)) },
        //      { key: false, name: 'Duration', index: 'Duration', sortable: true, width: (pageWidth * (5 / 100)) },
        //      {
        //          key: false, name: 'DateCreated', index: 'DateCreated', sortable: true, width: (pageWidth * (8 / 100)),
        //          formatter: function (cellvalue, options, row) {
        //              return (cellvalue || '').replace(/-/g, '/');
        //          }
        //      }
        //      //{ key: false, name: 'Completed', index: 'Completed', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Yes', '0': 'No' } } },
        //      //{ key: false, name: 'Remarks', index: 'Remarks' }
        //    ],
        //    //pager: jQuery('#pager'),
        //    rowNum: 1000000,
        //    rowList: [10, 20, 30, 40],
        //    height: '100%',
        //    viewrecords: true,
        //    //caption: 'Care Staff',
        //    emptyrecords: 'No records to display',
        //    jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
        //    autowidth: true,
        //    multiselect: false,
        //    sortname: 'Id',
        //    sortorder: 'asc',
        //    loadonce: true,
        //    onSortCol: function () {
        //        fromSort = true;
        //        GURow.saveSelection.call(this);
        //    },
        //    loadComplete: function (data) {
        //        //if ($('#ftrResident').find('option').size() > 0) return;
        //        //var optsRes = ['<option>All</option>'], optsCS = ['<option>All</option>'], optsDP = ['<option>All</option>'];
        //        //$.each(unique($.map(data, function (o) { return o.Resident })), function () {
        //        //    optsRes.push('<option>' + this + '</option>');
        //        //});
        //        //$.each(unique($.map(data, function (o) { return o.Carestaff })), function () {
        //        //    optsCS.push('<option>' + this + '</option>');
        //        //});
        //        //$.each(unique($.map(data, function (o) { return o.Department })), function () {
        //        //    optsDP.push('<option>' + this + '</option>');
        //        //});
        //        //$('#ftrResident').html(optsRes.join(''));
        //        //$('#ftrCarestaff').html(optsCS.join(''));
        //        //$('#ftrDept').html(optsDP.join(''));

        //        if (typeof fromSort != 'undefined') {
        //            GURow.restoreSelection.call(this);
        //            delete fromSort;
        //            return;
        //        }

        //        var maxId = data && data[0] ? data[0].Id : 0;
        //        if (typeof isReloaded != 'undefined') {
        //            $.each(data, function (k, d) {
        //                if (d.Id > maxId) maxId = d.Id;
        //            });
        //        }
        //        if (maxId > 0)
        //            $("#sGrid").setSelection(maxId);
        //        delete isReloaded;
        //    }
        //});
        //SGrid.jqGrid('bindKeys');
        //$('.tabPanels').layout({
        //    center: {
        //        paneSelector: "#serviceGrid",
        //        closable: false,
        //        slidable: false,
        //        resizable: true,
        //        spacing_open: 0
        //    },
        //    north: {
        //        paneSelector: "#AddServiceToolbar",
        //        closable: false,
        //        slidable: false,
        //        resizable: false,
        //        spacing_open: 0
        //    },
        //    onresize: function () {
        //        resizeGridsS();
        //        //resizeGridHS();
        //    }
        //});
        //resizeGridsS();


    }
    //end initservice

    var initServiceHomeCare = function () {

        var id = currentId;
        if (!id) {
            id = $('#txtId').val();
            if (!id) return;
        }

        containerTask = {};
        $("#sGridtbl").html("");

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=activity_schedule_svc&param3=" + id,
            //url: "Admin/GetGridData?func=activity_schedule&param3=" + id ,
            dataType: "json",
            success: function (m) {

                var srvcs_html = "";
                var svcIdx = "";

                if (m.length != 0) {
                    $.each(m, function (i, s) {

                        srvcs_html += "<tr><td><a>" + s.PlannedDate + "</a></td>";
                        srvcs_html += "<td>" + (s.PostedDate == null ? "-" : s.PostedDate) + "</td > ";
                        srvcs_html += "<td>" + s.Resident + "</td>";
                        srvcs_html += "<td>" + s.Caregiver + "</td>";
                        srvcs_html += "<td>" + s.PlannedServices + "</td>";
                        srvcs_html += "<td>" + (s.PostedServices == "" ? "-" : s.PostedServices) + "</td>";
                        srvcs_html += "<td>" + s.StartTime + " - " + s.EndTime + "</td>";
                        srvcs_html += "<td>" + (s.PostedDate == null ? "-" : s.ActualStartTime + " - " + s.ActualEndTime) + "</td>";
                        srvcs_html += "</tr>";
                    })

                    $("#sGrid").DataTable().destroy();
                    $("#sGridtbl").html(srvcs_html);
                    serviceTask = $("#sGrid").DataTable({
                        responsive: !0,
                        pagingType: "full_numbers",
                        dom: 'lBfrtip',
                        //"scrollX": true,
                        //"scrollCollapse": true,
                        select: true,
                        buttons: [
                            {
                                extend: 'excelHtml5',
                                text: '<i class="la la-download"></i> Export to Excel',
                                title: 'ALC_Employees_ServiceTask_' + id,
                                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',

                                init: function (api, node, config) {
                                    $(node).removeClass('dt-button')
                                }
                            }, {
                                text: 'Refresh',
                                action: function (e, dt, node, config) {
                                    initServiceHomeCare(currentId);
                                },
                                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air refreshTaskHm',
                                init: function (api, node, config) {
                                    $(node).removeClass('dt-button')
                                }
                            }


                        ]
                    });
                }

                $(".refreshTask").prop("hidden", true);

                //var dtnow = new Date(Date.now());

                //if (m.length != 0) {

                //    $.each(m, function (i, v) {
                //        svcIdx = currentId + "_" + v.Id;
                //        containerTask[svcIdx] = v;

                //        var nDate;
                //        if (v.ActiveUntil != null) {
                //            nDate = new Date(v.ActiveUntil);
                //        }

                //        if ((v.IsActive == true) || ((v.IsActive == false) && (nDate > dtnow))) {

                //            html_table += "<tr id='" + v.Id + "'>";
                //            html_table += "<td><a href='#' class='svctask' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'>" + v.Activity + "</a></td>";
                //            html_table += "<td>" + v.Department + "</td>";
                //            html_table += "<td>" + (v.Resident == null ? "N/A" : v.Resident) + "</td>";
                //            html_table += "<td>" + v.Carestaff + "</td>";
                //            html_table += "<td>" + (v.Room == null ? "N/A" : v.Room) + "</td>";
                //            html_table += "<td>" + v.Schedule + "</td>";
                //            html_table += "<td>" + v.Recurrence + "</td>";
                //            html_table += "<td>" + v.Duration + " minutes</td>";
                //            html_table += "<td>" + v.DateCreated + "</td>";
                //            html_table += "</tr>";
                //        }
                //    })
                //}

                //$("#sGrid").DataTable().destroy();

                //$("#sGridtbl").html(html_table);

                //serviceTask = $("#sGrid").DataTable({
                //    responsive: !0,
                //    pagingType: "full_numbers",
                //    dom: 'lBfrtip',
                //    //"scrollX": true,
                //    //"scrollCollapse": true,
                //    select: true,
                //    buttons: [
                //        {
                //            extend: 'excelHtml5',
                //            text: '<i class="la la-download"></i> Export to Excel',
                //            title: 'ALC_Employees_ServiceTask_' + id,
                //            className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',

                //            init: function (api, node, config) {
                //                $(node).removeClass('dt-button')
                //            }
                //        }, {
                //            text: 'Refresh',
                //            action: function (e, dt, node, config) {
                //                loadSvcTask(currentId);
                //            },
                //            className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air refreshTask',
                //            init: function (api, node, config) {
                //                $(node).removeClass('dt-button')
                //            }
                //        }


                //    ]
                //});
            }
        });

    }
    //end initservice_hmc

    //=======initDocumentsDisciplinaryWrUp=======================
    var initDocuments = function () {
        var id = currentId;
        if (!id) return;
        $.ajax({
            url: "Admin/GetGridData?func=disciplinary_writeups&param=" + id,
            datatype: 'json',
            postData: '',
            success: function (m) {

                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {

                        html_table += "<tr>";
                        html_table += "<td><a href='#' id='" + v.UUID + "' class='dnld'>" + v.Description + "</a></td>";
                        html_table += "<td>" + v.Discip_reason + "</td>";
                        html_table += "<td>" + v.CreatedBy + "</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";
                        html_table += "<td style='max-width:500px;word-break:break-all'><a href='#' id='" + v.UUID + "' class='dnld'>" + v.Filename + "</a></td>";
                        html_table += "<td>" + v.Size + "KB</td>";
                        html_table += "<td>" + v.Filetype + "</td>";
                        // add below code for action delete button Document Displinary kim 11/24/2021
                        html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del doc_delete' href='#' id='" + v.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                        html_table += "</tr>";
                    })

                }

                $("#dwuGrid").DataTable().destroy();
                $("#dwubody").html('');
                $("#dwubody").html(html_table);
                $("#dwuGrid").DataTable({
                    stateSave: true,
                    "scrollX": "true",
                    "scrollCollapse": true,
                    select: true,
                    responsive: !0,
                }
                );
            }
        });
        // add code kim 11/24/2021
        $(document).on("click", ".doc_delete", function () {
            var did = $(this);
            var doc_uuid = did.attr('id');
            if (!doc_uuid) return;
            //  if (!confirm("Are you sure you want to delete the selected row?")) return;
            swal({
                title: "Are you sure you want to delete this document?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify({
                            func: 'disciplinary_writeups',
                            mode: 3,
                            data: {
                                docId: doc_uuid
                            }
                        }),
                        dataType: "json",
                        success: function (m) {
                            if (m.result == 0) {
                                //$.growlSuccess({
                                //    message: "Document has beend deleted successfully!",
                                //    delay: 3000
                                //});
                                toastr.success("Document has been deleted successfully!");
                                //refresh table after delete kim 11/25/2021
                                initDocuments();
                            } else
                                $.growlError({
                                    message: m.message,
                                    delay: 6000
                                });
                        }
                    });
            });
        });
    }

    $(document).on("click", ".dnld", function (i, v) {
        window.location.href = "Admin/DownloadDisciplinaryWriteUp/?DOCID=" + $(this).attr("id");

    })







    //==========End initDocumentsDisciplinaryWrUp==============

    //=======initPersonalDocuments=======================
    var initP_Documents = function () {
        var id = currentId;
        if (!id) return;
        $.ajax({
            url: "Admin/GetGridData?func=personal_document&param=" + id,
            datatype: 'json',
            postData: '',
            success: function (m) {

                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {
                        html_table += "<tr>";
                        html_table += "<td><a href='#' id='" + v.UUID + "' class='dnld_pd'>" + v.Description + "</a></td>";
                        html_table += "<td>" + v.CreatedBy + "</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";
                        html_table += "<td style='max-width:500px;word-break:break-all'><a href='#' id='" + v.UUID + "' class='dnld_pd'>" + v.Filename + "</a></td>";
                        html_table += "<td>" + v.Size + "KB</td>";
                        html_table += "<td>" + v.Filetype + "</td>";
                        // add code below for action delete button in every column *****Personal Documents kim 11/24/2021
                        html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del doc_delete' href='#' id='" + v.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                        html_table += "</tr>";
                    })

                }

                $("#pdocGrid").DataTable().destroy();
                $("#pdocbody").html('');
                $("#pdocbody").html(html_table);
                $("#pdocGrid").DataTable({
                    stateSave: true,
                    "scrollX": "true",
                    "scrollCollapse": true,
                    select: true,
                    responsive: !0,
                }
                );
            }
        });
        // add code below for function of delete button *****Personal Documents kim 11/24/2021

        $(document).on("click", ".doc_delete", function () {
            var did = $(this);
            var doc_uuid = did.attr('id');
            if (!doc_uuid) return;
            swal({
                title: "Are you sure you want to delete this document?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify({
                            func: 'personal_document',
                            mode: 3,
                            data: {
                                docId: doc_uuid
                            }
                        }),
                        dataType: "json",
                        success: function (m) {
                            if (m.result == 0) {
                                toastr.success("Document has been deleted successfully!");
                                //refresh table after delete button has been click kim 11/25/2021
                                initP_Documents();
                            } else
                                $.growlError({
                                    message: m.message,
                                    delay: 6000
                                });
                        }
                    });
            });
        });
    }




    $(document).on("click", ".dnld_pd", function (i, v) {
        window.location.href = "Admin/DownloadPersonalDocument/?DOCID=" + $(this).attr("id");
    })
    $(document).on("click", "#refresh-pdoc", function () {
        initP_Documents();
    })


    //==========End initPersonalDocuments==============

    //=======initProof In Service Training=======================
    var initPITDocs = function () {
        var id = currentId;
        if (!id) return;
        $.ajax({
            url: "Admin/GetGridData?func=proof_inservice_training&param=" + id,
            datatype: 'json',
            postData: '',
            success: function (m) {

                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {

                        html_table += "<tr>";
                        html_table += "<td><a href='#' id='" + v.UUID + "' class='dnld_pit'>" + v.pit_title + "</a></td>";
                        html_table += "<td>" + v.CreatedBy + "</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";
                        html_table += "<td style='max-width:500px;word-break:break-all'><a href='#' id='" + v.UUID + "' class='dnld_pit'>" + v.Filename + "</a></td>";
                        html_table += "<td>" + v.Size + "KB</td>";
                        html_table += "<td>" + v.Filetype + "</td>";
                        // add code below for action delete button in every column *****Proof In Service Training kim 11/24/2021
                        html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del doc_delete' href='#' id='" + v.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                        html_table += "</tr>";
                    })

                }

                $("#pitGrid").DataTable().destroy();
                $("#pitbody").html('');
                $("#pitbody").html(html_table);
                $("#pitGrid").DataTable({
                    stateSave: true,
                    "scrollX": "true",
                    "scrollCollapse": true,
                    select: true,
                    responsive: !0,
                }
                );
            }
        });

        // add code below for function of delete button *****Proof In Service Training kim 11/24/2021
        $(document).on("click", ".doc_delete", function () {
            var did = $(this);
            var doc_uuid = did.attr('id');
            if (!doc_uuid) return;
            swal({
                title: "Are you sure you want to delete this document?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify({
                            func: 'proof_inservice_training',
                            mode: 3,
                            data: {
                                docId: doc_uuid
                            }
                        }),
                        dataType: "json",
                        success: function (m) {
                            if (m.result == 0) {
                                toastr.success("Document has been deleted successfully!");
                                //refresh table after delete kim 11/25/2021
                                initPITDocs();
                            } else
                                $.growlError({
                                    message: m.message,
                                    delay: 6000
                                });
                        }
                    });
            });
        });
    }

    $(document).on("click", ".dnld_pit", function (i, v) {
        window.location.href = "Admin/DownloadPITDocs/?DOCID=" + $(this).attr("id");
    })

    //==========End initProof In Service Training==============

    //=======init Employee form request datatable=======================
    var initEmpFormReqs = function () {
        var id = currentId;
        if (!id) return;
        $.ajax({
            url: "Admin/GetGridData?func=empform_request",
            datatype: 'json',
            postData: '',
            success: function (m) {

                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {
                        console.log(v);

                        html_table += "<tr>";
                        html_table += "<td><a href='#' id='" + v.UUID + "' class='dnld_efr'>" + v.pit_title + "</a></td>";
                        html_table += "<td>" + v.CreatedBy + "</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";
                        html_table += "<td style='max-width:500px;word-break:break-all'><a href='#' id='" + v.UUID + "' class='dnld_efr'>" + v.Filename + "</a></td>";
                        html_table += "<td>" + v.Size + "KB</td>";
                        html_table += "<td>" + v.Filetype + "</td>";
                        // add code below for action delete button in every column *****Proof In Service Training kim 11/24/2021
                        html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del efr_delete' href='#' id='" + v.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                        html_table += "</tr>";
                    })

                }

                $("#efrGrid").DataTable().destroy();
                $("#efrbody").html('');
                $("#efrbody").html(html_table);
                $("#efrGrid").DataTable({
                    stateSave: true,
                    "scrollX": "true",
                    "scrollCollapse": true,
                    select: true,
                    responsive: !0,
                }
                );
            }
        });

        // add code below for function of delete button *****Proof In Service Training kim 11/24/2021
        $(document).on("click", ".efr_delete", function () {
            var did = $(this);
            var doc_uuid = did.attr('id');
            if (!doc_uuid) return;
            swal({
                title: "Are you sure you want to delete this document?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify({
                            func: 'empform_request',
                            mode: 3,
                            data: {
                                docId: doc_uuid
                            }
                        }),
                        dataType: "json",
                        success: function (m) {
                            if (m.result == 0) {
                                toastr.success("Document has been deleted successfully!");
                                //refresh table after delete kim 11/25/2021
                                initEmpFormReqs();
                            } else
                                $.growlError({
                                    message: m.message,
                                    delay: 6000
                                });
                        }
                    });
            });
        });
    }

    $(document).on("click", ".dnld_efr", function (i, v) {
        window.location.href = "Admin/DownloadEmpFormRequest/?DOCID=" + $(this).attr("id");
    })

    //==========End initProof In Service Training==============

    //var resizeGridsS = (function () {
    //    //debugger;
    //    var f = function () {
    //        var tabH = $('.tabs').height() + 26;
    //        var tBH = $('#DocumentToolbar').height();
    //        $("#dGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
    //        $("#dGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));

    //        var tBH1 = $('#DocumentToolbar1').height();
    //        $("#pdocGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
    //        $("#pdocGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH1));
    //        //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
    //        //console.log($("#sGrid").height())
    //    }
    //    setTimeout(f, 100);
    //    return f;
    //});

    $('#DocumentToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.upload')) {
            uploadDocument();
        } else if (q.is('.deldsp')) {
            var docid = $('#dGrid').jqGrid('getGridParam', 'selrow');
            if (!docid) return;
            if (!confirm("Are you sure you want to delete the selected row?")) return;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify({
                    func: 'disciplinary_writeups',
                    mode: 3,
                    data: {
                        docId: docid
                    }
                }),
                dataType: "json",
                success: function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({
                            message: "Document has been deleted successfully!",
                            delay: 3000
                        });
                        initDocuments();
                    } else
                        $.growlError({
                            message: m.message,
                            delay: 6000
                        });
                }
            });
        } else if (q.is('.refresh')) {
            initDocuments();
        }
    });

    function truncate(n) {
        var len = 40;
        var ext = n.substring(n.lastIndexOf(".") + 1, n.length).toLowerCase();
        var filename = n.replace('.' + ext, '');
        if (filename.length <= len) {
            return n;
        }
        var flen = filename.length;
        var f1 = filename.substr(0, (len / 2));
        var f2 = filename.substr((flen - (len / 2)), flen);
        filename = f1 + '...' + f2;
        return filename + '.' + ext;
    }

    //$(document).on("click", "#dwu_upld", function () {


    //})

    function uploadDocument() {
        var array = [];
        var done = [];
        //added upload files button here: angelie
        var dialog_html = "<div class='upload-container'><div class='upload-header'>" +
            "<button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Clear List</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +
            "<div class='memoform'><table style='width:100%'><tr><td>Memo Title</td><td><input type='text' id='memotitle' class='form-control m-input w200px'></td></tr>" +
            "<tr><td>Reason of Disciplinary Action</td><td><input type='text' id='memoreason' class='form-control m-input w200px'></td></tr></table></div>" +
            "<div id='fileList' class='upload-list'></div><div class='upload-footer'><div id='divLegend'>" +
            //kim modified upload button will not disappear with clear list 03/01/22
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded </div>" +
            "<button id='btnUploadfiles' class='btn btn-outline' style='margin-left: 350px;background-color: #5bfda3 !important;border-color: #5bfda3 !important;'>Upload Files</button></div></div ></div > ";

        $(".dwu_div").html(dialog_html);

        var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a  style='display:none'>&nbsp;</a></td>" +
            //"<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" + //remove upload.remove here and added attribute fname: angelie
            "<td style='width:2%'><a class='status-done' id='filename' fname='fname'>&nbsp;</a></td></tr>" +

            // "<td style='width:2%'><a class='status-done' id='filename' fname='fname'>&nbsp;</a></td></tr>" +

            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        $(".dwu_div #fileUpload").on('change', function () {
            
            var itms = $('.upload-container .totalfiles span:eq(1)');
            if (parseInt(itms.text()) > 0)
                itms.text(parseInt(itms.text()) + this.files.length);
            else
                itms.text(this.files.length);
        });
        $('.dwu_div #btnAddfiles').on('click', function () {
            if ($('#memotitle').val() == '') {
                swal("Please enter memo title.");
                return;
            } else if ($('#memoreason').val() == '') {
                swal("Please state reason of disciplary action.");
                return;
            } else {
                $(".dwu_div #fileUpload").trigger('click');
            }
        });
        $('.dwu_div #btnClearList').on('click', function () {
            $('.dwu_div #fileList').html('');
            $('.dwu_div .totalfiles').html("<span>0</span> of <span>0</span> Files Uploaded");
        });

        //$('#fileUpload').on('click', function () {
        $("#dwu_modal").find("#fileUpload").fileupload({
            url: "Admin/UploadDisciplinaryWriteUp?",
            dataType: "json",
            done: function (e, data) {
              
                
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');

                        if (dat.message == "Duplicate employee document") {
                            swal("Duplicate employee file uploaded.", "", "warning")
                        }
                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');
                        var itms = $('.upload-container .totalfiles span:eq(0)');
                        // revised this part: angelie
                        if (parseInt(dat.message) > 0) {
                         
                            itms.text(parseInt(itms.text()) + 1);

                            swal({
                                title: "Document uploaded successfully.",
                                text: "",
                                type: "success",
                                confirmButtonText: "Ok"
                            }).then(function (e) {
                                $('#dwu_modal').modal('hide');
                                initDocuments();
                            });
                        }
                    }

                    return false;
                }
       
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
                $('#memotitle, #memoreason').val("");
            },
            add: function (e, data) {
             
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                    'csv', 'txt', 'rtf', 'zip', 'mp3', 'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif'
                ];

                $.each(files, function (x, i) {
                    var err = "";
                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    //kim change size 11/17/22
                    if (this.size <= 1000)
                        err = "The file size is to small, please select a file greater than 1KB";
                    
                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('.dwu_div #fileList');
                    var mname = $('#memotitle').val();
                    
               

                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('a#filename').attr('fname', name);
                    item.find('span.size').text(size);
                    item.find('span.memoname').text(mname);

                    array.push(name);
                    

                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        swal("Error.", err, "warning");
                        return true;
                    } else {
                        if (array.length >= 2) {
                            for (let i = 0; i <= array.length - 1; i++) {
                                if (array[i + 1] == array[i]) {
                                    item.find('.innerprog').width('100%').css('background', 'red');
                                    item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                                        //revised this: angelie
                                        if (data.files != null) {
                                            for (i = 0; i < data.files.length; i++) {
                                                const index1 = array.indexOf($(this).attr('fname'));
                                                array = [...array.slice(0, index1), ...array.slice(index1 + 1)];

                                                if (data.files[i].name == $(this).attr('fname')) {
                                                    data.files[i] = {};
                                                }
                                            }
                                        };
                                      
                                        item.remove();
                                        var itms = $('.upload-container .totalfiles span:eq(1)');
                                        itms.text(parseInt(itms.text()) - 1);


                                    });
                                    swal("Duplicate employee file document uploaded. Please remove the document", "", "warning");
                                    return true;
                                }
                                else {

                                    item.find('a:eq(0)').show();
                                    item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                                        //revised this: angelie
                                        if (data.files != null) {
                                            for (i = 0; i < data.files.length; i++) {

                                                if (data.files[i].name == $(this).attr('fname')) {
                                                    data.files[i] = {};

                                                }
                                            }
                                        };

                                        item.remove();
                                        var itms = $('.upload-container .totalfiles span:eq(1)');
                                        itms.text(parseInt(itms.text()) - 1);



                                    });
                                }
                            }
                        }
                        else {
                           
                        item.find('a:eq(0)').show();
                        item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {
                      
                            //revised this: angelie
                            if (data.files != null) {
                                for (i = 0; i < data.files.length; i++) {

                                    if (data.files[i].name == $(this).attr('fname')) {
                                        data.files[i] = {};

                                    }
                                }
                            };

                            item.remove();
                            var itms = $('.upload-container .totalfiles span:eq(1)');
                            itms.text(parseInt(itms.text()) - 1);



                            });
                        }
                    };

                    data.context = item;
                    data.urlparam = {
                        name: this.name,
                        size: this.size,
                        type: this.type,
                        file: this.file
                    };
                    //added this: angelie
                    //data.submit();
                    $('.dwu_div #btnClearList').on('click', function () {
                        data.urlparam = {};
                        data.files = [''];
                        array = [];
                        done = [];

                        $('#memotitle, #memoreason').val("");
                        $('.dwu_div #filelist').empty();
                        $('.dwu_div .totalfiles').html("<span>0</span> of <span>0</span> files uploaded");
                    });

                    $('.dwu_div #btnUploadfiles').on('click', function () {
                                              
                        var dl = done.length;
                        if (dl >= 1) {
                            a = 0;                            
                            for (let i = 0; i <= array.length-1; i++) {
                                if (done[i] != array[a]) {
                                    data.submit();                                    
                                }
                                else {
                                    a++;
                                }
                                }
                            }                        
                        else {
                            data.submit();
                        }
                       
                    });
                });
            },
            submit: function (e, data) {           
                // revised this part: angelie
                                
                    var rid = $('#uid').val();
                    var memt = $('.memoform #memotitle').val();
                    var memr = $('.memoform #memoreason').val();
                    data.formData = {
                        paramsid: "documents",
                        fileparams: JSON.stringify(data.urlparam),
                        rid: rid,
                        mt: memt,
                        mr: memr
                    };
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({
                    message: "Error: " + jqXHR.responseText,
                    delay: 6000
                });
            }
        });
    }
    //-------------------------------------------------------
    function uploadPersonalDocument() {
        var array = [];
        var done = [];
        var dialog_html =
            "<div class='upload-container'><div class='upload-header'>" +

            "<button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Clear List</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +

            "<div class='memoform'><table style='width:100%'><tr><td>Name of File</td><td><input type='text' id='nameoffile' class='w200px form-control m-input'></td></tr>" +
            "</table></div>" +
            "<div id='fileList' class='upload-list'></div><div class='upload-footer'><div id='divLegend'>" +
            //kim modified upload button will not disappear with clear list 03/01/22
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded </div>" +
            "<button id='btnUploadfiles' class='btn btn-outline' style='margin-left: 350px;background-color: #5bfda3 !important;border-color: #5bfda3 !important;'>Upload Files</button></div></div ></div > ";


        $(".pdoc_div").html(dialog_html);

        var uploadItem =
            "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a  style='display:none'>&nbsp;</a></td>" +
            "<td style='width:2%'><a class='status-done' id='filename' fname='fname'>&nbsp;</a></td></tr>" +
            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        $(".pdoc_div #fileUpload").on('change', function () {
            var itms = $('.upload-container .totalfiles span:eq(1)');
            if (parseInt(itms.text()) > 0)
                itms.text(parseInt(itms.text()) + this.files.length);
            else
                itms.text(this.files.length);
        });
        $('.pdoc_div #btnAddfiles').on('click', function () {
            if ($('#nameoffile').val() == '') {
                swal("Please enter name of file.");
                return;
            } else {
                $(".pdoc_div #fileUpload").trigger('click');
            }
        });
        $('.pdoc_div #btnClearList').on('click', function () {
            var mode = "clear";


            $('.pdoc_div #fileList').html('');
            $('.pdoc_div .totalfiles').html("<span>0</span> of <span>0</span> Files Uploaded");

        });

        //$('#fileUpload').on('click', function () {
        $("#pdoc_modal").find("#fileUpload").fileupload({
            url: "Admin/UploadPersonalDocument",
            dataType: "json",
            done: function (e, data) {
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');
                        if (dat.message == "Duplicate employee document") {
                            swal("Duplicate employee file uploaded.", "", "warning")
                        }

                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');
                        var itms = $('.upload-container .totalfiles span:eq(0)');
                        if (parseInt(dat.message) > 0) {
                            itms.text(parseInt(itms.text()) + 1);

                            swal({
                                title: "Document uploaded successfully.",
                                text: "",
                                type: "success",
                                confirmButtonText: "Ok"
                            }).then(function (e) {
                                $('#pdoc_modal').modal('hide');
                                initP_Documents();
                            });
                        }
                    }
                    return false;
                }
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
                $('#nameoffile, #description').val("");
            },
            add: function (e, data) {
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                    'csv', 'txt', 'rtf', 'zip', 'mp3', 'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif'
                ];

                $.each(files, function (x, i) {
                    var err = "";
                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    //kim change size 11/17/22
                    if (this.size <= 1000)
                        err = "The file size is to small, please select a file greater than 1KB";
               
                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('.pdoc_div #fileList');
                    var mname = $('#nameoffile').val();

                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('a#filename').attr('fname', name);
                    item.find('span.size').text(size);
                    item.find('span.memoname').text(mname);
                    array.push(name);
                    //
                    
                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        swal("Error.", err, "warning");
                        return true;
                    } else {
                        if (array.length >= 2) {
                            for (let i = 0; i <= array.length - 1; i++) {
                                if (array[i + 1] == array[i]) {
                                    item.find('.innerprog').width('100%').css('background', 'red');
                                    item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                                        //revised this: angelie
                                        if (data.files != null) {
                                            for (i = 0; i < data.files.length; i++) {
                                                const index1 = array.indexOf($(this).attr('fname'));
                                                array = [...array.slice(0, index1), ...array.slice(index1 + 1)];

                                                if (data.files[i].name == $(this).attr('fname')) {
                                                    data.files[i] = {};
                                                }
                                            }
                                        };

                                        item.remove();
                                        var itms = $('.upload-container .totalfiles span:eq(1)');
                                        itms.text(parseInt(itms.text()) - 1);


                                    });
                                    swal("Duplicate employee file document uploaded. Please remove the document", "", "warning");
                                    return true;
                                }
                                else {

                                    item.find('a:eq(0)').show();
                                    item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                                        //revised this: angelie
                                        if (data.files != null) {
                                            for (i = 0; i < data.files.length; i++) {

                                                if (data.files[i].name == $(this).attr('fname')) {
                                                    data.files[i] = {};

                                                }
                                            }
                                        };

                                        item.remove();
                                        var itms = $('.upload-container .totalfiles span:eq(1)');
                                        itms.text(parseInt(itms.text()) - 1);



                                    });
                                }
                               
                            }
                        }
                        else {

                            item.find('a:eq(0)').show();
                            item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                                //revised this: angelie
                                if (data.files != null) {
                                    for (i = 0; i < data.files.length; i++) {

                                        if (data.files[i].name == $(this).attr('fname')) {
                                            data.files[i] = {};

                                        }
                                    }
                                };

                                item.remove();
                                var itms = $('.upload-container .totalfiles span:eq(1)');
                                itms.text(parseInt(itms.text()) - 1);



                            });
                        }
                    }


                    //
                   
                    data.context = item;
                    data.urlparam = {
                        name: this.name,
                        size: this.size,
                        type: this.type,
                        file: this.file
                    };
                    $('.pdoc_div #btnClearList').on('click', function () {


                        data.urlparam = {};
                        data.files = [''];
                        array = [];
                        done = [];

                        $('#nameoffile, #description').val("");
                        $('.pdoc_div #filelist').empty();
                        $('.pdoc_div .totalfiles').html("<span>0</span> of <span>0</span> files uploaded");
                    });

                        $('.pdoc_div #btnUploadfiles').on('click', function () {
                            var dl = done.length;
                            if (dl >= 1) {
                                a = 0;
                                for (let i = 0; i <= array.length - 1; i++) {
                                    if (done[i] != array[a]) {
                                        data.submit();
                                    }
                                    else {
                                        a++;
                                    }
                                }
                            }
                            else {
                                data.submit();
                            }
                            
                    });


                });
            },
            submit: function (e, data) {
                var rid = $('#pd_uid').val();
                var memt = $('.memoform #nameoffile').val();
                var memr = $('.memoform #description').val();
                data.formData = {
                    paramsid: "documents",
                    fileparams: JSON.stringify(data.urlparam),
                    rid: rid,
                    mt: memt,
                    mr: memr
                };
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({
                    message: "Error: " + jqXHR.responseText,
                    delay: 6000
                });
            }
        });
    }
    //-------------------------------------------------------
    function uploadPITDocs() {
        var array = [];
        var done = [];
        var dialog_html = "<div class='upload-container'><div class='upload-header'>" +
            "<button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Clear List</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +
            "<div class='memoform'><table><tr><td>In-Service Training Title</td><td><input type='text' id='pit_title' class='w200px'></td></tr>" +
            "</table></div>" +
            "<div id='fileList' class='upload-list'></div><div class='upload-footer'><div id='divLegend'>" +
            //kim modified upload button will not disappear with clear list 03/01/22
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded </div>" +
            "<button id='btnUploadfiles' class='btn btn-outline' style='margin-left: 350px;background-color: #5bfda3 !important;border-color: #5bfda3 !important;'>Upload Files</button></div></div ></div > ";

        $(".pit_div").html(dialog_html);

        var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a  style='display:none'>&nbsp;</a></td>" +
            //"<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" + //remove upload.remove here and added attribute fname: angelie
            "<td style='width:2%'><a class='status-done' id='filename' fname='fname'>&nbsp;</a></td></tr>" +
            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        $(".pit_div #fileUpload").on('change', function () {
            var itms = $('.upload-container .totalfiles span:eq(1)');
            if (parseInt(itms.text()) > 0)
                itms.text(parseInt(itms.text()) + this.files.length);
            else
                itms.text(this.files.length);
        });
        $('.pit_div #btnAddfiles').on('click', function () {
            if ($('#pit_title').val() == '') {
                swal("Please enter In-Service Training Title.");
                return;
            } else {
                $(".pit_div #fileUpload").trigger('click');
            }
        });
        $('.pit_div #btnClearList').on('click', function () {
            $('.pit_div #fileList').html('');
            $('.pit_div .totalfiles').html("<span>0</span> of <span>0</span> Files Uploaded");
        });

        var fpnames = [];
        $("#pit_modal").find("#fileUpload").fileupload({
            url: "Admin/UploadPITDocs",
            dataType: "json",
            done: function (e, data) {
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');
                        
                        if (dat.message == "Duplicate employee document") {
                            swal("Duplicate employee file uploaded.", "", "warning")
                        }
                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');
                        var itms = $('.upload-container .totalfiles span:eq(0)');

                        if (parseInt(dat.message) > 0) {
                            itms.text(parseInt(itms.text()) + 1);

                            swal({
                                title: "Document uploaded successfully.",
                                text: "",
                                type: "success",
                                confirmButtonText: "Ok"
                            }).then(function (e) {
                                $('#pit_modal').modal('hide');
                                initPITDocs();
                            });
                        }
                    }
                    return false;
                }

            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
                $('.pit_title').val("");
            },
            add: function (e, data) {
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                    'csv', 'txt', 'rtf', 'zip', 'mp3', 'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif'
                ];


                $.each(files, function (x, i) {
                    var err = "";

                    if (($.inArray(this.name, fpnames) != -1)) {
                        err = "Duplicate files uploaded."
                    } else if (($.inArray(this.name, fpnames) == -1)) {
                        fpnames.push(this.name);
                    }

                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    //kim change size 11/17/22
                    if (this.size <= 1000)
                        err = "The file size is to small, please select a file greater than 1KB";

                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('.pit_div #fileList');
                    var pname = $('#pit_title').val();
                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('a#filename').attr('fname', name);
                    item.find('span.size').text(size);
                    item.find('span.pitname').text(pname);
                    array.push(name);
                    //
                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        swal("Error.", err, "warning");
                        return true;
                    } else {
                        if (array.length >= 2) {
                            for (let i = 0; i <= array.length - 1; i++) {
                                if (array[i + 1] == array[i]) {
                                    item.find('.innerprog').width('100%').css('background', 'red');
                                    item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                                        //revised this: angelie
                                        if (data.files != null) {
                                            for (i = 0; i < data.files.length; i++) {
                                                const index1 = array.indexOf($(this).attr('fname'));
                                                array = [...array.slice(0, index1), ...array.slice(index1 + 1)];

                                                if (data.files[i].name == $(this).attr('fname')) {
                                                    data.files[i] = {};
                                                }
                                            }
                                        };

                                        item.remove();
                                        var itms = $('.upload-container .totalfiles span:eq(1)');
                                        itms.text(parseInt(itms.text()) - 1);


                                    });
                                    swal("Duplicate employee file document uploaded. Please remove the document", "", "warning");
                                    return true;
                                }
                                else {

                                    item.find('a:eq(0)').show();
                                    item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                                        //revised this: angelie
                                        if (data.files != null) {
                                            for (i = 0; i < data.files.length; i++) {

                                                if (data.files[i].name == $(this).attr('fname')) {
                                                    data.files[i] = {};

                                                }
                                            }
                                        };

                                        item.remove();
                                        var itms = $('.upload-container .totalfiles span:eq(1)');
                                        itms.text(parseInt(itms.text()) - 1);



                                    });
                                }
                            }
                        }
                        else {

                            item.find('a:eq(0)').show();
                            item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                                //revised this: angelie
                                if (data.files != null) {
                                    for (i = 0; i < data.files.length; i++) {

                                        if (data.files[i].name == $(this).attr('fname')) {
                                            data.files[i] = {};

                                        }
                                    }
                                };

                                item.remove();
                                var itms = $('.upload-container .totalfiles span:eq(1)');
                                itms.text(parseInt(itms.text()) - 1);



                            });
                        }
                    }

                    data.context = item;
                    data.urlparam = {
                        name: this.name,
                        size: this.size,
                        type: this.type,
                        file: this.file
                    };
                    $('.pit_div #btnClearList').on('click', function () {
                        data.urlparam = {};
                        data.files = [''];
                        array = [];
                        done = [];

                        $('#pit_title').val("");
                        $('.pit_div #filelist').empty();
                        $('.pit_div .totalfiles').html("<span>0</span> of <span>0</span> files uploaded");
                    });

                    $('.pit_div #btnUploadfiles').on('click', function () {
                        var dl = done.length;
                        if (dl >= 1) {
                            a = 0;
                            for (let i = 0; i <= array.length - 1; i++) {
                                if (done[i] != array[a]) {
                                    data.submit();
                                }
                                else {
                                    a++;
                                }
                            }
                        }
                        else {
                            data.submit();
                        }
                    });

                });
            },
            submit: function (e, data) {
                var rid = $("#pit_uid").val();
                var pitt = $('.memoform #pit_title').val();
                data.formData = {
                    paramsid: "documents",
                    fileparams: JSON.stringify(data.urlparam),
                    rid: rid,
                    pt: pitt
                };
                //console.log(data.formData)
                fpnames = [];
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({
                    message: "Error: " + jqXHR.responseText,
                    delay: 6000
                });
            }
        });
    }
    //-------------------------------------------------

    $('#DocumentToolbar1').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.upload')) {
            //uploadP_Document();
        } else if (q.is('.deldsp')) {
            var docid = $('#pdocGrid').jqGrid('getGridParam', 'selrow');
            if (!docid) return;
            if (!confirm("Are you sure you want to delete the selected row?")) return;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify({
                    func: 'personal_document',
                    mode: 3,
                    data: {
                        docId: docid
                    }
                }),
                dataType: "json",
                success: function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({
                            message: "Document has been deleted successfully!",
                            delay: 3000
                        });
                        initP_Documents();
                    } else
                        $.growlError({
                            message: m.message,
                            delay: 6000
                        });
                }
            });
        } else if (q.is('.refresh')) {
            initP_Documents();
        }
    });




    //========================================================

    //var initPITDocs = function () {
    //    if ($('#pGrid')[0] && $('#pGrid')[0].grid)
    //        $.jgrid.gridUnload('pGrid');
    //    var DGrid = $('#pGrid');

    //    var GURow = new Grid_Util.Row();
    //    var pageWidth = $("#pGrid").parent().width() - 100;

    //    var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //    if (!id) return;

    //    DGrid.jqGrid({
    //        url: "Admin/GetGridData?func=proof_inservice_training&param=" + id,
    //        datatype: 'json',
    //        postData: '',
    //        ajaxGridOptions: {
    //            contentType: "application/json",
    //            cache: false
    //        },
    //        //datatype: 'local',
    //        //data: dataArray,
    //        //mtype: 'Get',
    //        colNames: ['Id', 'Title', 'Uploaded By', 'Date Uploaded', 'Filename', 'Size', 'UUID', 'Type'],
    //        colModel: [{
    //            key: true,
    //            hidden: true,
    //            name: 'Id',
    //            index: 'Id'
    //        },
    //            {
    //                key: false,
    //                name: 'pit_title',
    //                index: 'pit_title',
    //                sortable: true,
    //                width: (pageWidth * (10 / 100)),
    //                formatter: "dynamicLink",
    //                formatoptions: {
    //                    onClick: function (rowid, irow, icol, celltext, e) {
    //                        var G = $('#pGrid');
    //                        G.setSelection(rowid, false);
    //                        var row = G.jqGrid('getRowData', rowid);
    //                        if (row) {
    //                            window.location.href = "Admin/DownloadPITDocs/?DOCID=" + row.UUID;
    //                        }
    //                    }
    //                }
    //            },
    //            {
    //                key: false,
    //                name: 'CreatedBy',
    //                index: 'CreatedBy',
    //                sortable: true,
    //                width: (pageWidth * (6 / 100))
    //            },
    //            {
    //                key: false,
    //                name: 'DateCreated',
    //                index: 'DateCreated',
    //                sortable: true,
    //                width: (pageWidth * (4 / 100))
    //            },
    //            {
    //                key: false,
    //                name: 'Filename',
    //                index: 'Filename',
    //                sortable: true,
    //                width: (pageWidth * (12 / 100)),
    //                formatter: "dynamicLink",
    //                formatoptions: {
    //                    onClick: function (rowid, irow, icol, celltext, e) {
    //                        var G = $('#pGrid');
    //                        G.setSelection(rowid, false);
    //                        var row = G.jqGrid('getRowData', rowid);
    //                        if (row) {
    //                            window.location.href = "Admin/DownloadPITDocs/?DOCID=" + row.UUID;
    //                        }
    //                    }
    //                }
    //            },
    //            {
    //                key: false,
    //                name: 'Size',
    //                index: 'Size',
    //                sortable: true,
    //                width: (pageWidth * (4 / 100)),
    //                formatter: function (cellvalue, options, row) {
    //                    return (cellvalue / 1000) + "KB";
    //                }
    //            },
    //            {
    //                key: false,
    //                hidden: true,
    //                name: 'UUID',
    //                index: 'UUID'
    //            },
    //            {
    //                key: false,
    //                name: 'Filetype',
    //                index: 'Filetype',
    //                sortable: true,
    //                width: (pageWidth * (10 / 100))
    //            }
    //        ],
    //        //pager: jQuery('#pager'),
    //        rowNum: 1000000,
    //        rowList: [10, 20, 30, 40],
    //        height: '100%',
    //        viewrecords: true,
    //        //caption: 'Care Staff',
    //        emptyrecords: 'No records to display',
    //        jsonReader: {
    //            root: 'rows',
    //            page: 'page',
    //            total: 'total',
    //            records: 'records',
    //            repeatitems: false,
    //            id: '0'
    //        },
    //        autowidth: true,
    //        multiselect: false,
    //        sortname: 'Id',
    //        sortorder: 'asc',
    //        loadonce: true,
    //        onSortCol: function () {
    //            fromSort = true;
    //            GURow.saveSelection.call(this);
    //        },
    //        loadComplete: function (data) {
    //            if (typeof fromSort != 'undefined') {
    //                GURow.restoreSelection.call(this);
    //                delete fromSort;
    //                return;
    //            }

    //            var maxId = data && data[0] ? data[0].Id : 0;
    //            if (typeof isReloaded != 'undefined') {
    //                $.each(data, function (k, d) {
    //                    if (d.Id > maxId) maxId = d.Id;
    //                });
    //            }
    //            if (maxId > 0)
    //                $("#pGrid").setSelection(maxId);
    //            delete isReloaded;
    //        }
    //    });
    //    DGrid.jqGrid('bindKeys');
    //    $('#userForm div.tabPanels:eq(0)').layout({
    //        center: {
    //            paneSelector: "#pit_Grid",
    //            closable: false,
    //            slidable: false,
    //            resizable: true,
    //            spacing_open: 0
    //        },
    //        north: {
    //            paneSelector: "#PIT_Toolbar",
    //            closable: false,
    //            slidable: false,
    //            resizable: false,
    //            spacing_open: 0
    //        },
    //        onresize: function () {
    //            resizeGridsP();
    //            //resizeGridHS();
    //        }
    //    });
    //    resizeGridsP();
    //}

    var resizeGridsP = (function () {
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#PIT_Toolbar').height();
            $("#pGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#pGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
            //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
            //console.log($("#sGrid").height())
        }
        setTimeout(f, 100);
        return f;
    });

    //$('#PIT_Toolbar').on('click', 'a', function () {
    //    var q = $(this);
    //    if (q.is('.upload')) {
    //        uploadPITDocs();
    //    } else if (q.is('.deldsp')) {
    //        var docid = $('#dGrid').jqGrid('getGridParam', 'selrow');
    //        if (!docid) return;
    //        if (!confirm("Are you sure you want to delete the selected row?")) return;
    //        $.ajax({
    //            type: "POST",
    //            contentType: "application/json; charset=utf-8",
    //            url: "Admin/PostGridData",
    //            data: JSON.stringify({
    //                func: 'proof_inservice_training',
    //                mode: 3,
    //                data: {
    //                    docId: docid
    //                }
    //            }),
    //            dataType: "json",
    //            success: function (m) {
    //                if (m.result == 0) {
    //                    $.growlSuccess({
    //                        message: "Document has been deleted successfully!",
    //                        delay: 3000
    //                    });
    //                    initDocuments();
    //                } else
    //                    $.growlError({
    //                        message: m.message,
    //                        delay: 6000
    //                    });
    //            }
    //        });
    //    } else if (q.is('.refresh')) {
    //        initPITDocs();
    //    }
    //});



    $("#ddlRole").change(function () {
        $(".checkbox-grid input")
            .removeAttr('checked')
            .removeAttr('disabled')
            .filter(function () {
                return $(this).val() == $('#ddlRole').val();
            }).attr('checked', true).attr('disabled', true);

        if ($('#ddlRole').val() == 1) {
            $("#dept").css("display", "none");
        } else { $("#dept").css("display", "block"); };
    });


    $("#search_emp_value").keyup(function () {
        var string = $(this).val().toLowerCase();

        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });

        $('#userstile').scrollTop(1);
    })

    $("#search_emp_value").keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    })

    $("#showall").on("click", function () {
        $("#search_emp_value").val("");
        $(".parent").each(function (i, e) {
            $(this).show();
        });
    });

    $('#ftr1').change(function () {
        // var string = $(this :selected).text();
        var string1 = $('#ftr1 :selected').text();
        var string2, all;
        $('#ftr2').html("");
        if (string1 == "Role") {
            $('#ftr2').html(uniqrl.join(''));
            all = "Roles";
        } else if (string1 == "Department") {
            $('#ftr2').html(uniqdpt.join(''));
            all = "Departments";
        } else if (string1 == "Type") {
            $('#ftr2').html(uniqtyp.join(''));
            all = "Types";
        } else if (string1 == "Status") {
            $('#ftr2').html(uniqstt.join(''));
            all = "Status";
        }

        $("#ftr2").html($('#ftr2 option').sort(function (x, y) {
            return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
        }));
        $("#ftr2").prepend('<option>All ' + all + '</option>');
        $("#ftr2").get(0).selectedIndex = 0;
        $("#ftr2").find('option[value=null]').remove();

        $(".parent").each(function (i, e) {
            $(this).show();
        });
    });

    $('#ftr2').change(function () {
        // var string = $(this :selected).text();
        var string = $('#ftr2 :selected').text();
        var s = string.indexOf("All");
        if (s == 0) {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        } else {
            $(".parent").each(function (i, e) {
                var a = $(this).text();

                if (a.indexOf(string) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            })
        }
    })

    $(document).on("click", "#addnew", function () {
    
        var id = currentId;
        if (id != null) {
            currentId = "";
        }
        loadSvcTask(currentId);
        // added this to show the red asterisk for password & confirm password: angelie 03-18-22
        $(".pass_and_confirm").css("display", "block");
        $(".confirm_pass").hide(); /*this will hide the textbox for pw and confirm pw upon opening the modal : 3-22-2022 : Cheche*/
        $("#add_task").addClass("disabled");/*disable this button if no data has been save in the basic information file 04/21/2022*/
        $("#upload-dwu").addClass("disabled");
        $("#upload-pdoc").addClass("disabled");
        $("#upload-pit").addClass("disabled");
        //$(".form").clearFields();
        //$("#empForm").clearFields();
        $("#lblSched").text("");//clear text label of shift
        $("#lblSched2").text("");//clear text label of shift in add services     
        $(".resdform").hide();  // when adding new employee, upload profile photo is disabled. Upload photo is only available for existing employees
        $("#userForm").clearFields();
        $("#txtPassword").css("display", "none");
        var tbs = $("#userForm .nav-tabs").find("a");
        $.each(tbs, function (i, e) {
            $(this).removeClass("active");
            $(this).removeClass("show");
        })

        $.each(tbs, function (i, e) {
            if ($(this).attr("href") == "#tabs-1") {
                $(this).click();
            }
        })
        // comment out line 4768 & 4771 to avoid conflict when saving employee : cheche 2-23-2022 modified
        var scrolll = new PerfectScrollbar('#tabsviews');
        removeEmpData();
        // 9-28-2022 : Cheche
        if (agency_ishomecare == "False") 
            $("#emp_modal_lbl").text("Add New Employee");
        else
            $("#emp_modal_lbl").text("Add New Caregiver");

        $("#txtId").removeAttr("readonly");
        //  $('#chkIsSysUser').attr('checked', 'checked'); 
        $("#employee_modal").modal("toggle");
        addUserMode = 1;
        // $('.non-sys-user').show();
        $('.non-sys-user').hide(); // modified : 2-23-2022 : cheche  
        //  $('[href="#tabs-4"]').closest('li').hide(); /*hide tabs3 & 4 upon opening modal: 3 - 9 - 2022*/
        //  $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide(); 
        $(".resimg").html("");

    });

    //Added for Exporting to Grid to Excel (-ABC)

    function fnExcelReport() {
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr><td  bgcolor='#87AFC6'></td><td  bgcolor='#87AFC6'>Employee</td><td  bgcolor='#87AFC6'>Department</td> </tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0; j < tab.rows.length; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Employees.xls");
        }
        else                 //other browser not tested on IE 11
            a.id = 'ExcelDL';
        a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
        a.download = 'ALC_Employees' + '.xls';
        document.body.appendChild(a);
        a.click(); // Downloads the excel document
        document.getElementById('ExcelDL').remove();
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    });
    //============================

    load_users();

    var showPrintButton = function () {
 
        $("#printEmployeeFile").show();
    }

    $("#tabsFormselect").change(function () {
        $('[href="' + $(this).find("option:selected").val() + '"]').tab('show');
    });

    $("#tabsSelect").change(function () {
        var tab = $(this).find("option:selected").val();
        $('[href="' + tab + '"]').tab('show');

        if (tab == "#tabs-3") {
            loadSvcTask(currentId);
        }

        if (tab == "#tabs-usr") {
            $("#tabsFormselect").get(0).selectedIndex = 0;
            $('[href="#tabs-1a"]').tab('show');
        }

        if (tab == "#tabs-usr") {
            var scroll3 = new PerfectScrollbar('#tabsviews');
            scroll3.destroy();
            var scroll2 = new PerfectScrollbar('#tabsformviews');
        } else {
            var scroll3 = new PerfectScrollbar('#tabsviews');
        }

        if (tab == "#tabs-1") {
            $("#userForm .ps__rail-x").css("display", "none");
        }

    });

    $(document).on("click", ".emptab", function () {
        var href = $(this).find("a").attr("href");

        if (href == "#tabs-usr") {
            $("#printEmployeeFile").show();
        } else {
            $("#printEmployeeFile").hide();
        }

    });

    $(document).on("click", "#printEmployeeFile", function () {
        isSaveAndPrint = true;
      //  $("#saveemployeeinfo").trigger("click"); // comment out on 9-29-2022 : Because it prevents the file from printing : Cheche

        var id = $('#userForm #txtId').val();
        var file = "";

        let timerInterval
        Swal.fire({
            title: 'File has been updated successfully.',
            html: 'Your file will be ready to print in <strong></strong> seconds.',
            timer: 4000,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                        .textContent = Swal.getTimerLeft()
                }, 100)
            },
            onClose: () => {
                isSaveAndPrint = false;
                clearInterval(timerInterval)

            }
        }).then((result) => {
            if (
                result.dismiss === Swal.DismissReason.timer
            ) {
                $("#tabsForm").find("a").each(function () {

                    if ($(this).hasClass('active')) {
                        file = $(this).attr("href");
                    }

                })
                if (file == "#tabs-1a") {
                    window.open(ALC_URI.Admin + "/PrintPersonnelFileChecklist?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-2a") {
                    window.open(ALC_URI.Admin + "/PrintHealthScreeningReport?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-6a") {
                    window.open(ALC_URI.Admin + "/PrintEmpPhotographyConsent?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-8a") {
                    window.open(ALC_URI.Admin + "/PrintEmpHepatitisForm?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-3a") {
                    window.open(ALC_URI.Admin + "/PrintCriminalRecordStatement?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-7a") {
                    window.open(ALC_URI.Admin + "/PrintEmployeeHandbookReceipt?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-4a") {
                    window.open(ALC_URI.Admin + "/PrintEmployeeRights?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-5a") {
                    window.open(ALC_URI.Admin + "/PrintElderAbuseAcknowledgement?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-9a") {
                    window.open(ALC_URI.Admin + "/PrintPersonnelRecord?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-10a") {
                    window.open(ALC_URI.Admin + "/PrintRequestForLiveScanService?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-13a") {
                    window.open(ALC_URI.Admin + "/PrintEmpEligibilityVerification?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-14a") {
                    window.open(ALC_URI.Admin + "/PrintEmployeePayrollWithholding?id=" + id, '_blank');
                    return;
                }
                else if (file == "#tabs-11a") {
                    window.open(ALC_URI.Admin + "/PrintJobDescAcknowledgement?id=" + id, '_blank');
                    return;
                }


            }

        })

    })


    $(document).on("click", "#empForm #tabsForm a", function () {
        $('#tabsformviews').scrollTop(1);

        var tab = $(this).attr("href");
        currentTabIdx = tab;

        if (tab == "#tabs-15a") {
            $("#saveemployeeinfo").hide();
            initEmpFormReqs();
        } else {
            $("#saveemployeeinfo").show();
        }
    })


    function uploadEmpFormReq() {

        var dialog_html = "<div class='upload-container'>" +
            "<div class='memoform'><table><tr><td style='padding-right:10px'>Employee Form Title </td><td><input type='text' id='efr_title' class='form-control m-input w200px'></td></tr>" +
            "</table></div>" +
            "<div class='upload-header' style='border-bottom:none !important'><button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Reset</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +

            "<div id='fileList' class='upload-list' style='height: 120px'></div><div class='upload-footer'><div id='divLegend'>" +
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded </div>" +
            "<button id='btnUploadfiles' class='btn btn-outline' style='margin-left: 350px;background-color: #5bfda3 !important;border-color: #5bfda3 !important;'>Submit Request</button></div></div ></div > ";

        $(".empformreq_div").html(dialog_html);

        var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a class='status-loading' style='display:none'>&nbsp;</a></td>" +
            //"<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" + //remove upload.remove here and added attribute fname: angelie
            "<td style='width:2%'><a class='status-done' id='filename' fname='fname'>&nbsp;</a></td></tr>" +
            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        $(".empformreq_div #fileUpload").on('change', function () {
            //var itms = $('.upload-container .totalfiles span:eq(1)');
            //if (parseInt(itms.text()) > 0)
            //    itms.text(parseInt(itms.text()) + this.files.length);
            //else
            //    itms.text(this.files.length);
            $(".empformreq_div .totalfiles").text("File has been attached.");
        });
        $('.empformreq_div #btnAddfiles').on('click', function () {
            if ($('#efr_title').val() == '') {
                swal("Please enter title of form.");
                return;
            } else {
                $(".empformreq_div #fileUpload").trigger('click');
            }
        });
        $('.empformreq_div #btnClearList').on('click', function () {
            $('.empformreq_div #fileList').html('');
            $('.empformreq_div .totalfiles').html("No file attached.");
            $(".empformreq_div #btnAddfiles").attr('disabled', false);
            $(".empformreq_div #btnUploadfiles").attr('disabled', false);
        });
        $(".empformreq_div").find("#fileUpload").fileupload({
            url: "Admin/UploadEmpFormRequest",
            dataType: "json",
            done: function (e, data) {
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    console.log(dat);
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');

                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');


                        $(".empformreq_div .totalfiles").text("File successfully uploaded.");
                        $(".empformreq_div .status-loading").hide();

                        // to not allow multiple uploading of files
                        $('.upload-header').hide();
                        $('.empformreq_div #btnUploadfiles').attr('disabled', true);
                        swal("Request sent!", "Your request have been submitted successfully. An email with all your request details and file attachment has been sent to the ALC Team.", "success");
                    }
                    return false;
                }

            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
                $(".empformreq_div .status-loading").show();
            },
            add: function (e, data) {
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                    'csv', 'txt', 'rtf', 'zip', 'jpg', 'jpeg', 'png'
                ];

                $.each(files, function (x, i) {
                    var err = "";
                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    if (this.size == 0)
                        err = "The file size is to small, please select a file greater than 2mb";
                    //if (this.type.length == 0 || ext)
                    //    err = "The file type is not supported.";
                    //else if (this.type.length && !acceptFileTypes.test(this.type)) {
                    //    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    //    err = "The file type of '." + ext + "' is not supported.";
                    //}
                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('.empformreq_div #fileList');
                    var pname = $('#efr_title').val();
                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('a#filename').attr('fname', name);
                    item.find('span.size').text(size);
                    item.find('span.pitname').text(pname);
                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        $(".empformreq_div #btnAddfiles").attr("disabled", true);
                        $(".empformreq_div #btnUploadfiles").attr("disabled", true);
                        $(".empformreq_div .totalfiles").text(err);
                        return true;
                    } else {
                        //item.find('a:eq(0)').show();
                        item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                            if (data.files != null) {
                                for (i = 0; i < data.files.length; i++) {
                                    if (data.files[i].name == $(this).attr('fname')) {
                                        data.files[i] = {};
                                    }
                                }
                            }
                            item.remove();
                            //var itms = $('.upload-container .totalfiles span:eq(1)');
                            $(".empformreq_div .totalfiles").text("File has been attached.");
                        });
                    }


                    data.context = item;
                    data.urlparam = {
                        name: this.name,
                        size: this.size,
                        type: this.type,
                        file: this.file
                    };
                    $('.empformreq_div #btnClearList').on('click', function () {


                        data.urlparam = {};
                        data.files = [''];

                        $('#efr_title').val("");
                        $('.empformreq_div #filelist').empty();
                        $('.empformreq_div .totalfiles').html("<span>0</span> of <span>0</span> files uploaded");
                        $('.empformreq_div #btnUploadfiles').attr('disabled', false);
                    });

                    $('.empformreq_div #btnUploadfiles').on('click', function () {
                        data.submit();
                    });

                });
            },
            submit: function (e, data) {
                var pitt = $('.memoform #efr_title').val();
                data.formData = {
                    paramsid: "documents",
                    fileparams: JSON.stringify(data.urlparam),
                    rid: 0,
                    pt: pitt
                };
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({
                    message: "Error: " + jqXHR.responseText,
                    delay: 6000
                });
            }
        });
    }

    function closeModal(id) {

        $("#" + id).modal("hide");
        //resets the inputs inside the <form> tag under the div with id
        $("#" + id + " form").trigger("reset");
        $('#' + id + ' input:checkbox').removeAttr('checked');

    }
    // Cheche was here to autoclose modal w/o asking questions if fields were empty 2-28-2022 && load users after closing the modal 3-4-2022
    $(document).on("click", "#close_user, #_closemodal", function () {
        load_users();
        var id = $(this).attr("modalid");

        newState = $('#tabs-1,#tabs-2').extractJson();
        userInfoState.current = JSON.stringify(newState);

        //modified to prevent tabs from closing if required field were not filled out : 3-10-2022 Cheche
        if (currentTabIdx == '#tabs-1') {
            if (isSysUser == true) {
                if (newState.Id != "" && newState.Firstname != "" && newState.MiddleInitial != "" && newState.Lastname != "" && newState.Password != "" && newState.ConfirmPassword != "" && newState.Email != "" && newState.Role != "" && newState.Department != "" && newState.Shift != "") {
                    if (userInfoState.current == userInfoState.orig) {
                        //      load_users(); comment out 04/22/22 kim no need to load users everytime closing of modal
                        closeModal(id);
                        return;
                    }
                }
            } else {
                if (newState.Firstname != "" && newState.MiddleInitial != "" && newState.Lastname != "" && newState.Role != "" && newState.Department != "") {
                    if (userInfoState.current == userInfoState.orig) {
                        //      load_users();
                        closeModal(id);
                        return;
                    }
                }
            }
            //  load_users();
        //clearing text on Search Bar : 11-04-2022 : Cheche
        $("#search_emp_value").val("");
        }
        //else if (currentTabIdx == '#tabs-2') {

        //    if (newState.BirthDate == "" && newState.Role == null) {
        //        closeModal(id);
        //        return;
        //    }

        //    if (($("#txtAddress1").val() == "") || ($("#txtAddress2").val() == "") || ($("#txtCity").val() == "") || ($("#txtState").val() == "") || ($("#txtZip").val() == "") || ($("#ddlGender").val() == "") || ($("#txtBirthDate").val() == "") || ($("#txtPhone").val() == "") || ($("#txtFax").val() == "") || ($("#txtSSN").val() == "")) {
        //        swal("Please fill in all the required information!", "", "info");
        //        return false;
        //    } else {
        //        if (userInfoState.current != userInfoState.orig) {
        //            swal("Ooops, you can't exit this without saving the required information!", "", "warning");
        //            return false;
        //        } else {
        //            load_users();
        //            closeModal(id);
        //            return;
        //        }
        //    }
        //}
        if (currentTabIdx == '#tabs-2' || currentTabIdx == '#tabs-3' || currentTabIdx == '#tabs-usr' || currentTabIdx == '#tabs-dsp' || currentTabIdx == '#tabs-pdocs' || currentTabIdx == '#tabs-pit') {
            //   load_users();
            closeModal(id);
            return;
        }

        if (userInfoState.current == userInfoState.orig || newState.Role == null || newState.Firstname == "") {
            closeModal(id);
            return
        } else {
            swal({
                title: "Are you sure you want to close without saving your data?",
                text: "You won't be able to revert this!",
                type: "error",
                showCancelButton: !0,
                confirmButtonText: "Yes"
            }).then(function (e) {
                e.value && closeModal(id)
            })
        }


    });

    $(document).on("click", "#serviceGrid .refreshTaskHm", function () {
        initServiceHomeCare();
    });

    $(document).on("click", "#serviceGrid .refreshTask", function () {
        initService();
    });

    var SearchTextCity = "City";
    $("#txtCity").val(SearchTextCity).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextCity);
    }).focus(function (e) {
        if ($(this).val() == SearchTextCity)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "city"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtCity").val(ui.item.city);
            $("#txtState").val(ui.item.state);
            $("#txtStateId").val(ui.item.state_id);
            $("#txtZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search City...") {
                $("#txtState").val("");
                $("#txtStateId").val("");
                $("#txtZip").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.city + "</a>")
            .appendTo(ul);
    };


    var SearchTextState = "State";
    $("#txtState").val(SearchTextState).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextState);
    }).focus(function (e) {
        if ($(this).val() == SearchTextState)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "state"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtCity").val(ui.item.city);
            $("#txtState").val(ui.item.state);
            $("#txtStateId").val(ui.item.state_id);
            $("#txtZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search State...") {
                $("#txtState").val("");
                $("#txtStateId").val("");
                $("#txtCity").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.state + "</a>")
            .appendTo(ul);
    };


    var SearchTextZip = "Zip";
    $("#txtZip").val(SearchTextZip).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextZip);
    }).focus(function (e) {
        if ($(this).val() == SearchTextZip)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "zip"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtCity").val(ui.item.city);
            $("#txtState").val(ui.item.state);
            $("#txtStateId").val(ui.item.state_id);
            $("#txtZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search Zipcode...") {
                $("#txtState").val("");
                $("#txtStateId").val("");
                $("#txtCity").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.zipcode + "</a>")
            .appendTo(ul);
    };

    //$("#employee_modal .close1").click(function (e) {
    //    var id = $(this).attr("modalid");
    //    swal({
    //        title: "Are you sure you want to close without saving your data?",
    //        text: "You won't be able to revert this!",
    //        type: "error",
    //        showCancelButton: !0,
    //        confirmButtonText: "Yes"
    //    }).then(function (e) {
    //        e.value && closeModal(id)

    //    })
    //})



});


