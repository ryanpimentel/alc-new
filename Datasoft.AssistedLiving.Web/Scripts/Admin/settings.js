﻿var AgencySettingState = {
    orig: '',
    current: ''
};
var ftrs = {
    date1: moment(new Date()).format("MM/DD/YYYY"),
    date2: moment(new Date()).format("MM/DD/YYYY"),
    userId: 'all',
    action: 'all',
};
var table_usract = "";
var datenow = ALC_Util.CurrentDate;
var ishomeCareValue = false;
var agencyNameOrig = "";
var optUId = [];
var filterBy = "NA";
var filterValue = "0";
var load_agency_info = function () {
    $.ajax({
        url: "Admin/GetGridData?func=settings&param=",
        datatype: 'json',
        type: "POST",
        contentType: "application/json",
        success: function (info) {
            a = info;
            ishomeCareValue = a[0].IsHomeCare;
            $("#a_form").mapJson(a[0]);

            agencyNameOrig = a[0].FacilityName;

            $("#agencyGrid .form-control").prop("disabled", true);

            setTimeout(function () { $(".loader").hide(); }, 500)
            AgencySettingState.orig = JSON.stringify($('#a_form').extractJson());

           
            if (ishomeCareValue == true) {
                $("#facilityName").val("Home Care Centre Demo");
            }
        }
    });
}

function validateEmail(e) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(e)) {
        return true;
    } else {
        return false;
    }
}

var initEmpFormReqs = function () {
    //var id = currentId;
    //if (!id) return;
    $.ajax({
        url: "Admin/GetGridData?func=empform_request",
        datatype: 'json',
        postData: '',
        success: function (m) {

            if (m.length != 0) {
                var html_table = "";

                $.each(m, function (i, v) {
                    console.log(v);

                    if (v.IsApproved == true) {
                        status = '<span class="m-badge m-badge--success m-badge--wide">Done</span>';
                    } else {
                        status = '<span class="m-badge m-badge--warning m-badge--wide">Pending</span>';
                    }

                    html_table += "<tr>";
                    html_table += "<td><a href='#' id='" + v.UUID + "' class='dnld_efr'>" + v.pit_title + "</a></td>";
                    html_table += "<td>" + status + "</td>";
                    html_table += "<td>" + v.CreatedBy + "</td>";
                    html_table += "<td>" + v.DateCreated + "</td>";
                    html_table += "<td style='max-width:500px;word-break:break-all'><a href='#' id='" + v.UUID + "' class='dnld_efr'>" + v.Filename + "</a></td>";
                    html_table += "<td>" + v.Size + "KB</td>";
                    html_table += "<td>" + v.Filetype + "</td>";
                    if (v.IsApproved == true) {
                        html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill disabled' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a>\n</span></td>";
                    } else {
                        html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del efr_delete' href='#' id='" + v.Id + "'><i class='la la-trash-o'></i> Delete</a><a class='dropdown-item del efr_done' href='#' id='" + v.Id + "'><i class='la la-trash-o' ></i> Mark as Done</a></div>\n</span></td>";
                    }
                    html_table += "</tr>";
                })

            }

            $("#efrGrid").DataTable().destroy();
            $("#efrbody").html('');
            $("#efrbody").html(html_table);
            $("#efrGrid").DataTable({
                stateSave: true,
              //  "scrollX": "true", kim comment for action button able to see 05/05/22
                "scrollCollapse": true,
                select: true,
                responsive: !0,
            }
            );
        }
    });

    $(document).on("click", ".efr_delete", function () {
        var did = $(this);
        var doc_uuid = did.attr('id');
        if (!doc_uuid) return;
        swal({
            title: "Are you sure you want to delete this request?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!"
        }).then(function (e) {
            e.value &&
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify({
                        func: 'empform_request',
                        mode: 3,
                        data: {
                            docId: doc_uuid
                        }
                    }),
                    dataType: "json",
                    success: function (m) {
                        if (m.result == 0) {
                            toastr.success("Request has been deleted successfully!");
                            //refresh table after delete kim 11/25/2021
                            initEmpFormReqs();
                        } else
                            $.growlError({
                                message: m.message,
                                delay: 6000
                            });
                    }
                });
        });
    });

    $(document).on("click", ".efr_done", function () {
        var did = $(this);
        var doc_uuid = did.attr('id');
        if (!doc_uuid) return;
        swal({
            title: "Mark this request as done?",
            text: "Is this form available and up already on your agency forms?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            e.value &&
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify({
                        func: 'empform_request',
                        mode: 2,
                        data: {
                            docId: doc_uuid
                        }
                    }),
                    dataType: "json",
                    success: function (m) {
                        if (m.result == 0) {
                            toastr.success("Request has been mark approved!");
                            //refresh table after delete kim 11/25/2021
                            initEmpFormReqs();
                        } else
                            $.growlError({
                                message: m.message,
                                delay: 6000
                            });
                    }
                });
        });
    });
}


var generateTable = function () {
    var html_table = "";
    //table_usract.destroy();
    //if (res.length != 0) {

    //    $.each(res, function (i, v) {
    //         changing label of Facility to Amenities for ALC : Nov-01-2022 : Cheche
    //        if (v.Module == "facility") {
    //            var am = "amenities";
    //        } else {
    //            var am = v.Module;
    //        }

    //        html_table += "<tr>";
    //        html_table += "<td>" + v.Timestamp + "</td>";
    //        html_table += "<td>" + v.UserId + "</td>";
    //        html_table += "<td>" + v.UserName + "</td>";
    //          html_table += "<td>" + v.Module + "</td>";
    //        html_table += "<td>" + am + "</td>";
    //        html_table += "<td>" + v.Action + "</td>";
    //        html_table += "<td>" + v.RecordId + "</td>";
    //        html_table += "<td>" + v.Record + "</td>";
    //        html_table += "</tr>";

    //    })

    //}

    $("#usractGrid").DataTable().destroy();
            //$("#usractbody").html('');
            //$("#usractbody").html(html_table);
            //$("#usractGrid").DataTable({});
    $("#usractGrid").DataTable({
                //stateSave: true,
                //  "scrollX": "true"
                //"scrollCollapse": true,
                //select: true,
                //"ordering": false,
                //"searching": false,
                //"processing": true,
                //"serverSide": true,
                //"orderMulti": false,
                //"bFilter": false,
                //"responsive": true,
                "scrollX": true,
                "processing": true,
                "scrollY": "true",
                "scrollCollapse": true,
                "serverSide": true,
                "orderMulti": false,
                "bFilter": false,
                "responsive": true,
                select: true,
                "ajax": {
                    "url": "Admin/GetGridData?func=user_activity&param=" + JSON.stringify(ftrs),
                    "type": "POST",
                    "dataType": "json"
                },
                "columns": [
                    { "data": "Timestamp", "name": "Timestamp" },
                    { "data": "UserId", "name": "UserId" },
                    { "data": "UserName", "name": "UserName" },
                    { "data": "Module", "name": "Module" },
                    { "data": "Action", "name": "Action" },
                    { "data": "RecordId", "name": "RecordId" },
                    { "data": "Record", "name": "Record" },
                ],
            });
            $(".loader").hide();
}

var initUserActivity = function (filters) {
    
    $('#ftrDate, #ftrDate2').datepicker({
        dateFormat: 'mm-dd-yyyy',
        orientation: "bottom",
        todayHighlight: true,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        autoclose: true
    });

    //$.ajax({
    //    type: "POST",
    //    url: "Admin/GetGridData?func=user_activity&param=" + JSON.stringify(filters),
    //    datatype: 'json',
    //    postData: '',
    //    success: function (m) {           
    //        generateTable(m);                     
    //    }
    //});

    generateTable();    

    $('#ftrDate,#ftrDate2,#ftrUserId,#ftrAction').change(function () {
        $(".loader").show();        
        var dt1 = $('#ftrDate').val();
        var dt2 = $('#ftrDate2').val();
        var sta = $('#ftrUserId :selected').val();
        var act = $('#ftrAction :selected').val();

        if (dt1 == '' || dt2 == '') {
            swal("Fill in all filter inputs!", "Start and End Date cannot be empty.", "warning");
            $(".loader").hide(); 
            return false;                 
        }

        if (sta != 'all') ftrs.userId = sta;
        else ftrs.userId = "all";

        if (act != 'all') ftrs.action = act;
        else ftrs.action = "all";

        if (dt1 != datenow) ftrs.date1 = dt1;
        else ftrs.date1 = datenow;

        if (dt2 != datenow) ftrs.date2 = dt2;
        else ftrs.date2 = datenow;


        initUserActivity(ftrs);


    });
}



$(document).ready(function () {
    
    $(".loader").show();
    $("#agencyGrid").height($(window).height() - 275);
    load_agency_info();     
    
    $(document).on("click", ".cancelagency", function () {
        load_agency_info();
        $("#agencyGrid .form-control").prop("disabled", true);
        $(".saveagency").prop("disabled", true);
        $("input.error").css("border", "#ddd 1px solid");
        $("span.error").css("display", "none"); 
    });

    $(document).on("click", "#edit_agency", function () {
        $("#agencyGrid .form-control").prop("disabled", false);

        $("#agencyName").prop("disabled", true);
        $("#facilityName").prop("disabled", true);

        $(".btn").prop("disabled", false);

        $("#zip").forceNumericOnly();
        $("#phone").forceNumericOnly();
        $("#fax").forceNumericOnly();
        $("#lnumber").forceNumericOnly();
        $("#lphone").forceNumericOnly();

    });

    $("#showalllib").on("click", function () {
        library_payer();
    });

    $("#showallallergies").on("click", function () {
        library_allergy();
    });
    

    $("#showallphysician").on("click", function () {
        library_physician();
    });
    $("#showallpharmacy").on("click", function () {
        library_pharmacy();
    });

    $(document).on("click", ".saveagency", function () {
        
        var mode = 2;
        // added agency to display in textbox by Cheche 12/22/2021
        //agency = $("agencyName").val(); 
        email = $("#agencyEmail").val();
        var isValid = $('#a_form').validate();
        var isValidEmail;
        // delete lname and lphone kim 03/24/22
        if (($.trim($('#address1').val()) == "") || ($.trim($('#address2').val()) == "") || ($.trim($('#city').val()) == "") || ($.trim($('#state').val()) == "") || ($.trim($('#zip').val()) == "") || ($.trim($('#phone').val()) == "") || ($.trim($('#fax').val()) == "") || ($.trim($('#agencyEmail').val()) == "") || ($.trim($('#lnumber').val()) == "")) {

            swal('Please fill in everything!', "", "warning");
    return;
        }

        if (isValid) {
            isValidEmail = validateEmail(email);
            if (isValidEmail) {
                row = $('#a_form').extractJson();
            } else {
                swal("Please provide a valid email.")
                $("#agencyGrid .form-control").prop("disabled", false);
                $("#agencyName").prop("disabled", true);
            }
        } else return;


        row.FacilityName = agencyNameOrig;

        AgencySettingState.current = JSON.stringify(row);
        if (AgencySettingState.current == AgencySettingState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }

        var data = { func: "settings", mode: mode, data: row }
     
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.result == 0) {
                    setTimeout(function () {
                        if (ishomeCareValue != row.IsHomeCare) {
                            Swal.fire({
                                title: 'Success! Please wait.',
                                html: 'System will now auto logout for agency settings to be updated.',// add html attribute if you want or remove
                                allowOutsideClick: false,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                },
                            });
                        }                      

                    }, 300);

                    $("#agencyGrid .form-control").prop("disabled", true);
                    $(".btns").prop("disabled", true);
                    AgencySettingState.orig = JSON.stringify($('#a_form').extractJson());

                    if (ishomeCareValue != row.IsHomeCare) {
                        setTimeout(function () {
                            $.ajax({
                                type: "POST",
                                async: false,
                                contentType: "application/json; charset=utf-8",
                                url: ALC_URI.Login + "/LogOff",
                                success: function (m) {
                                    if (m.Result == 1) {
                                        location.href = m.RedirectUrl;
                                    }
                                }
                            });
                        }, 3000);
                    }else { // transferred here to always trigger successful alert wether ALC|HOMECARE : Cheche : 11-22-2022
                            //toastr.success("Agency Information updated successfully!");
                        swal('Agency Information updated successfully!', "", "success");
                        $(".saveagency").prop("disabled", true);
                        $(".cancelagency").prop("disabled", true);
                        } 
                    
                   

                }
            }
        });
    });

    function truncate(n) {
        var len = 40;
        var ext = n.substring(n.lastIndexOf(".") + 1, n.length).toLowerCase();
        var filename = n.replace('.' + ext, '');
        if (filename.length <= len) {
            return n;
        }
        var flen = filename.length;
        var f1 = filename.substr(0, (len / 2));
        var f2 = filename.substr((flen - (len / 2)), flen);
        filename = f1 + '...' + f2;
        return filename + '.' + ext;
    }

    $('#upload-efr').on('click', function () {
        uploadEmpFormReq();
    });


    
    function library_payer() {

        $.ajax({
            url: "Admin/GetGridData?func=payers",
            datatype: 'json',
            postData: '',
            success: function (m) {
                if (m.length != 0) {
                    var html_table = "";
                    $.each(m, function (i, v) {
                       
                        if (v.Name != "" && v.Name != null) {
                            html_table += "<tr>";
                            html_table += "<td>" + v.Name + "</td>";
                            html_table += "<td>" + v.Create_date + "</td>";
                            html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a data-dismiss='modal'  class='dropdown-item edit payer_edit' id='" + v.Id + "' name='" + v.Name + "' href='#'><i class='la la-edit'></i> Edit</a>\n<a class='dropdown-item del payer_delete' href='#' id='" + v.Id + "' name='" + v.Name + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                            html_table += "</tr>";
                        }


                    })


                }

                $("#servicesLibraryDetails_table").html("");
                $("#servicesLibrary_datatable").DataTable().destroy();
                $("#servicesLibraryDetails_table").html(html_table);
                $("#servicesLibrary_datatable").DataTable({
                    stateSave: true,
                    "scrollCollapse": true,
                    select: true,
                    responsive: !0,
                }
                );
            }
        });



    
    $(document).on('click', " .payer_delete", function () {
       
        var dis = $(this);
        var payer = dis.attr('name');
        var payerid = dis.attr('id');
        mode = 3;
        var data = {
            func: "payers", mode: mode, data: {
                payer: payer, payerid: payerid
            }
        }
        swal({
            title: "Are you sure you want to delete this Payer?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!"
        }).then(function (e) {
            e.value &&
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {

                        if (m.result == 0) {
                            setTimeout(function () {
                                $(".loader").hide();

                                swal("Successfully Deleted Payer!", "", "success");

                                return
                            }, 300);
                        }
                        if (m.result == -1) {
                            $(".loader").hide();
                            swal("Please fill up payer name", "", "error");
                            return
                        }
                        if (m.result == -2) {
                            $(".loader").hide();
                            swal("Duplicate Payer Found!", "", "error");
                            return
                        }
                        library_payer();

                    }
                });
        });

    });
    $(document).on("click", ".payer_edit", function () {
        $("#edit_payer").modal("show");
        var dis = $(this);
        var rid = dis.attr('name');
        var ris = dis.attr('id');
        $('#txtNamepayer').val(rid);
        $('#txtIdpayer').val(ris);

    });

        $(document).on("click", "#saveeditpayer", function () {
        
        mode = 2;
        var payer = $('#txtNamepayer').val();
        var payerid = $('#txtIdpayer').val();
        var data = {
            func: "payers", mode: mode, data: {
                payer: payer, payerid: payerid
            }
        }
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (m.result == 0) {
                    
                        $(".loader").hide();

                        swal("Successfully updated Payer!", "", "success");
                    $("#edit_payer").modal("hide");
                        return
                   
                }
                if (m.result == -1) {
                    $(".loader").hide();
                    swal("Please fill up payer name", "", "error");
                    return
                }
                if (m.result == -2) {
                    $(".loader").hide();
                    swal("Duplicate payer Found!", "", "error");
                    return
                }
                $("#edit_payer").modal("hide");
            }
        });

        });
    }
    $('#addnewlib').on('click', function () {
        
        var payer = $("#search_payer_value").val();
        if (payer == "" || payer == null) {
            swal("Please fill up payer name", "", "error");
            return;
        }
        mode = 1;
        var data = {
            func: "payers", mode: mode, data: {
                payer: payer
            }
        }
        swal({
            title: "Are you sure you want to Add this Payer?",
            text: "You won't be able to revert this!",
            type: "question",
            showCancelButton: !0,
            confirmButtonText: "Yes, Add it!"
        }).then(function (e) {
            e.value &&
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {

                        if (m.result == 0) {
                            setTimeout(function () {
                                $(".loader").hide();

                                swal("Successfully add new payer!", "", "success");

                                return;;
                            }, 300);
                        }
                        if (m.result == -1) {
                            $(".loader").hide();
                            swal("Please fill up payer name", "", "error");
                            return;
                        }
                        if (m.result == -2) {
                            $(".loader").hide();
                            swal("Duplicate Payer Found!", "", "error");
                            return;;
                        }

                        $('#search_payer_value').val("");

                    }
                });
        });
    });
    $('#addnewaller').on('click', function () {

        var allergy = $("#search_allergy_value").val();
        if (allergy == "" || allergy == null) {
            swal("Please fill up allergy name", "", "error");
            return;
        }
        mode = 1;
        var data = {
            func: "allergy", mode: mode, data: {
                allergy: allergy
            }
        }
        swal({
            title: "Are you sure you want to Add this Allergy?",
            text: "You won't be able to revert this!",
            type: "question",
            showCancelButton: !0,
            confirmButtonText: "Yes, Add it!"
        }).then(function (e) {
            e.value &&   
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (m.result == 0) {
                    setTimeout(function () {
                        $(".loader").hide();

                        swal("Successfully add new Allergy!", "", "success");

                        return
                    }, 300);
                }
                if (m.result == -1) {
                    $(".loader").hide();
                    swal("Please fill up allergy name", "", "error");
                    return
                }
                if (m.result == -2) {
                $(".loader").hide();
                    swal("Duplicate Allergy Found!", "", "error");
                    return
                }

                $('#search_allergy_value').val("");

            }
                    });
            });
    });
    //add button for physician kim 11/16/22
    $('#addnewphysician').on('click', function () {

        var physician = $("#search_physician_value").val();
        if (physician == "" || physician == null) {
            swal("Please fill up Physician name", "", "error");
            return;
        }
        mode = 1;
        var data = {
            func: "physician", mode: mode, data: {
                physician: physician
            }
        }
        swal({
            title: "Are you sure you want to Add this Physician?",
            text: "You won't be able to revert this!",
            type: "question",
            showCancelButton: !0,
            confirmButtonText: "Yes, Add it!"
        }).then(function (e) {
            e.value &&
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {

                        if (m.result == 0) {
                            setTimeout(function () {
                                $(".loader").hide();

                                swal("Successfully add new Physician!", "", "success");

                                return
                            }, 300);
                        }
                        if (m.result == -1) {
                            $(".loader").hide();
                            swal("Please fill up Physician name", "", "error");
                            return
                        }
                        if (m.result == -2) {
                            $(".loader").hide();
                            swal("Duplicate Physician Found!", "", "error");
                            return
                        }

                        $('#search_physician_value').val("");

                    }
                });
        });
    });
    //add button for pharmacy kim 11/25/22
    $('#addnewpharmacy').on('click', function () {

        var pharmacy = $("#search_pharmacy_value").val();
        if (pharmacy == "" || pharmacy == null) {
            swal("Please fill up Pharmacy name", "", "error");
            return;
        }
        mode = 1;
        var data = {
            func: "pharmacy", mode: mode, data: {
                pharmacy: pharmacy
            }
        }
        swal({
            title: "Are you sure you want to Add this Pharmacy?",
            text: "You won't be able to revert this!",
            type: "question",
            showCancelButton: !0,
            confirmButtonText: "Yes, Add it!"
        }).then(function (e) {
            e.value &&
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {

                        if (m.result == 0) {
                            setTimeout(function () {
                                $(".loader").hide();

                                swal("Successfully add new Pharmacy!", "", "success");

                                return
                            }, 300);
                        }
                        if (m.result == -1) {
                            $(".loader").hide();
                            swal("Please fill up Pharmacy name", "", "error");
                            return
                        }
                        if (m.result == -2) {
                            $(".loader").hide();
                            swal("Duplicate Pharmacy Found!", "", "error");
                            return
                        }

                        $('#search_pharmacy_value').val("");

                    }
                });
        });
    });
    function library_allergy() {

        $.ajax({
            url: "Admin/GetGridData?func=allergy",
            datatype: 'json',
            postData: '',
            success: function (m) {
                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {
                        if (v.Name != null) {
                           
                            html_table += "<tr>";
                            html_table += "<td>" + v.Name + "</td>";
                            html_table += "<td>" + v.Create_date + "</td>";
                            html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a data-dismiss='modal'  class='dropdown-item edit allergy_edit' id='" + v.Id + "' name='" + v.Name + "' href='#'><i class='la la-edit'></i> Edit</a>\n<a class='dropdown-item del allergy_delete' href='#' id='" + v.Id + "' name='" + v.Name + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";

                            html_table += "</tr>";
                        }

                    })
                }
               
                $("#servicesAllergyDetails_table").html("");
                $("#servicesAllergy_datatable").DataTable().destroy();
                $("#servicesAllergyDetails_table").html(html_table);
                $("#servicesAllergy_datatable").DataTable({
                    stateSave: true,
                    "scrollCollapse": true,
                    select: true,
                    responsive: !0,
                }
                );
            }
        });
    
        $(document).on('click', " .allergy_delete", function () {
            
            var dis = $(this);
            var allergy = dis.attr('name');
            var allergyid = dis.attr('id');
            mode = 3;
            var data = {
                func: "allergy", mode: mode, data: {
                    allergy: allergy, allergyid: allergyid
                }
            }
            swal({
                title: "Are you sure you want to delete this Allergy?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function (e) {
            e.value &&
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {

                    if (m.result == 0) {
                        setTimeout(function () {
                            $(".loader").hide();

                            swal("Successfully Deleted Allergy!", "", "success");

                            return
                        }, 300);
                    }
                    if (m.result == -1) {
                        $(".loader").hide();
                        swal("Please fill up allergy name", "", "error");
                        return
                    }
                    if (m.result == -2) {
                        $(".loader").hide();
                        swal("Duplicate Allergy Found!", "", "error");
                        return
                    }
                            
                    library_allergy();

                }
                    });
                });

        });
        $(document).on("click", ".allergy_edit", function () {
            $("#edit_allergy").modal("show");
            var dis = $(this);
            var rid = dis.attr('name');
            var ris = dis.attr('id');
            $('#txtNameallergy').val(rid);
            $('#txtIdallergy').val(ris);      
           
        });

        $(document).on("click", "#saveeditallergy", function () {
            
            mode = 2;
            
            var allergy = $('#txtNameallergy').val();
            var allergyid = $('#txtIdallergy').val();
                var data = {
                    func: "allergy", mode: mode, data: {
                        allergy: allergy, allergyid: allergyid
                    }
                }
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {
                        
                        if (m.result == 0) {                           
                            $(".loader").hide();
                            swal("Successfully updated Allergy!", "", "success");
                            $("#edit_allergy").modal("hide");
                            return                    
                        }
                        if (m.result == -1) {
                            $(".loader").hide();
                            swal("Please fill up allergy name", "", "error");
                            return
                        }
                        if (m.result == -2) {
                            $(".loader").hide();
                            swal("Duplicate Allergy Found!", "", "error");
                            return
                        }
                        $("#edit_allergy").modal("hide");
                    }
                });

            });
    }
    //add function for physician kim 11/16/22
    function library_physician() {
        
        $.ajax({
            url: "Admin/GetGridData?func=physician",
            datatype: 'json',
            postData: '',
            success: function (m) {
                
                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {
                        
                        if (v.Name != null) {

                            html_table += "<tr>";
                            html_table += "<td>" + v.Name + "</td>";
                            html_table += "<td>" + v.Create_date + "</td>";
                            html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a data-dismiss='modal'  class='dropdown-item edit physician_edit' id='" + v.Id + "' name='" + v.Name + "' href='#'><i class='la la-edit'></i> Edit</a>\n<a class='dropdown-item del physician_delete' href='#' id='" + v.Id + "' name='" + v.Name + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";

                            html_table += "</tr>";
                        }

                    })
                }

                $("#servicesPhysicianDetails_table").html("");
                $("#servicesPhysician_datatable").DataTable().destroy();
                $("#servicesPhysicianDetails_table").html(html_table);
                $("#servicesPhysician_datatable").DataTable({
                    stateSave: true,
                    "scrollCollapse": true,
                    select: true,
                    responsive: !0,
                }
                );
            }
        });

        $(document).on('click', " .physician_delete", function () {

            var dis = $(this);
            var physician = dis.attr('name');
            var physicianid = dis.attr('id');
            mode = 3;
            var data = {
                func: "physician", mode: mode, data: {
                    physician: physician, physicianid: physicianid
                }
            }
            swal({
                title: "Are you sure you want to delete this Physician?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (m) {

                            if (m.result == 0) {
                                setTimeout(function () {
                                    $(".loader").hide();

                                    swal("Successfully Deleted Physician!", "", "success");

                                    return
                                }, 300);
                            }
                            if (m.result == -1) {
                                $(".loader").hide();
                                swal("Please fill up Physician name", "", "error");
                                return
                            }
                            if (m.result == -2) {
                                $(".loader").hide();
                                swal("Duplicate Physician Found!", "", "error");
                                return
                            }
                            library_physician();

                        }
                    });
            });

        });
        $(document).on("click", ".physician_edit", function () {
            $("#edit_physician").modal("show");
            var dis = $(this);
            var rid = dis.attr('name');
            var ris = dis.attr('id');
            $('#txtNamephysician').val(rid);
            $('#txtIdphysician').val(ris);

        });

        $(document).on("click", "#saveeditphysician", function () {

            mode = 2;
            var physician = $('#txtNamephysician').val();
            var physicianid = $('#txtIdphysician').val();
            var data = {
                func: "physician", mode: mode, data: {
                    physician: physician, physicianid: physicianid
                }
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {

                    if (m.result == 0) {

                        $(".loader").hide();
                        swal("Successfully updated Physician!", "", "success");
                        $("#edit_physician").modal("hide");
                        return

                    }
                    if (m.result == -1) {
                        $(".loader").hide();
                        swal("Please fill up Physician name", "", "error");
                        return
                    }
                    if (m.result == -2) {
                        $(".loader").hide();
                        swal("Duplicate Physician Found!", "", "error");
                        return
                    }
                    $("#edit_physician").modal("hide");
                }
            });

        });
    }
    // add function for pharmacy kim 11/25/22
    function library_pharmacy() {

        $.ajax({
            url: "Admin/GetGridData?func=pharmacy",
            datatype: 'json',
            postData: '',
            success: function (m) {

                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {

                        if (v.Name != null) {

                            html_table += "<tr>";
                            html_table += "<td style='word-wrap: break-word'>" + v.Name + "</td>";
                            html_table += "<td>" + v.Create_date + "</td>";
                            html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a data-dismiss='modal'  class='dropdown-item edit pharmacy_edit' id='" + v.Id + "' name='" + v.Name + "' href='#'><i class='la la-edit'></i> Edit</a>\n<a class='dropdown-item del pharmacy_delete' href='#' id='" + v.Id + "' name='" + v.Name + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";

                            html_table += "</tr>";
                        }

                    })
                }

                $("#servicesPharmacyDetails_table").html("");
                $("#servicesPharmacy_datatable").DataTable().destroy();
                $("#servicesPharmacyDetails_table").html(html_table);
                $("#servicesPharmacy_datatable").DataTable({
                    stateSave: true,
                    "scrollCollapse": true,
                    select: true,
                    responsive: !0,
                }
                );
            }
        });

        $(document).on('click', " .pharmacy_delete", function () {

            var dis = $(this);
            var pharmacy = dis.attr('name');
            var pharmacyid = dis.attr('id');
            mode = 3;
            var data = {
                func: "pharmacy", mode: mode, data: {
                    pharmacy: pharmacy, pharmacyid: pharmacyid
                }
            }
            swal({
                title: "Are you sure you want to delete this Pharmacy?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (m) {

                            if (m.result == 0) {
                                setTimeout(function () {
                                    $(".loader").hide();

                                    swal("Successfully Deleted Pharmacy!", "", "success");

                                    return
                                }, 300);
                            }
                            if (m.result == -1) {
                                $(".loader").hide();
                                swal("Please fill up Pharmacy name", "", "error");
                                return
                            }
                            if (m.result == -2) {
                                $(".loader").hide();
                                swal("Duplicate Physician Found!", "", "error");
                                return
                            }
                            library_pharmacy();

                        }
                    });
            });

        });
        $(document).on("click", ".pharmacy_edit", function () {
            $("#edit_pharmacy").modal("show");
            var dis = $(this);
            var rid = dis.attr('name');
            var ris = dis.attr('id');
            $('#txtNamepharmacy').val(rid);
            $('#txtIdpharmacy').val(ris);

        });

        $(document).on("click", "#saveeditpharmacy", function () {

            mode = 2;
            var pharmacy = $('#txtNamepharmacy').val();
            var pharmacyid = $('#txtIdpharmacy').val();
            var data = {
                func: "pharmacy", mode: mode, data: {
                    pharmacy: pharmacy, pharmacyid: pharmacyid
                }
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {

                    if (m.result == 0) {

                        $(".loader").hide();
                        swal("Successfully updated Pharmacy!", "", "success");
                        $("#edit_pharmacy").modal("hide");
                        return

                    }
                    if (m.result == -1) {
                        $(".loader").hide();
                        swal("Please fill up Pharmacy name", "", "error");
                        return
                    }
                    if (m.result == -2) {
                        $(".loader").hide();
                        swal("Duplicate Pharmacy Found!", "", "error");
                        return
                    }
                    $("#edit_pharmacy").modal("hide");
                }
            });

        });
    }
           
    


    function uploadEmpFormReq() {

        var dialog_html = "<div class='upload-container'>" +
            "<div class='memoform'><table><tr><td style='padding-right:10px'>Employee Form Title </td><td><input type='text' id='efr_title' class='form-control m-input w200px'></td></tr>" +
            "</table></div>" +
            "<div class='upload-header' style='border-bottom:none !important'><button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Reset</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +

            "<div id='fileList' class='upload-list' style='height: 120px'></div><div class='upload-footer'><div id='divLegend'>" +
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded </div>" +
            "<button id='btnUploadfiles' class='btn btn-outline' style='margin-left: 350px;background-color: #5bfda3 !important;border-color: #5bfda3 !important;'>Submit Request</button></div></div ></div > ";

        $(".empformreq_div").html(dialog_html);

        var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a class='status-loading' style='display:none'>&nbsp;</a></td>" +
            //"<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" + //remove upload.remove here and added attribute fname: angelie
            "<td style='width:2%'><a class='status-done' id='filename' fname='fname'>&nbsp;</a></td></tr>" +
            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        $(".empformreq_div #fileUpload").on('change', function () {
            //var itms = $('.upload-container .totalfiles span:eq(1)');
            //if (parseInt(itms.text()) > 0)
            //    itms.text(parseInt(itms.text()) + this.files.length);
            //else
            //    itms.text(this.files.length);
            $(".empformreq_div .totalfiles").text("File has been attached.");
        });
        $('.empformreq_div #btnAddfiles').on('click', function () {
            if ($('#efr_title').val() == '') {
                swal("Please enter title of form.");
                return;
            } else {
                $(".empformreq_div #fileUpload").trigger('click');
            }
        });
        $('.empformreq_div #btnClearList').on('click', function () {
            $('.empformreq_div #fileList').html('');
            $('.empformreq_div .totalfiles').html("No file attached.");
            $(".empformreq_div #btnAddfiles").attr('disabled', false);
            $(".empformreq_div #btnUploadfiles").attr('disabled', false);
        });
        $(".empformreq_div").find("#fileUpload").fileupload({
            url: "Admin/UploadEmpFormRequest",
            dataType: "json",
            done: function (e, data) {
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    console.log(dat);
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');

                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');


                        $(".empformreq_div .totalfiles").text("File successfully uploaded.");
                        $(".empformreq_div .status-loading").hide();

                        // to not allow multiple uploading of files
                        $('.upload-header').hide();
                        $('.empformreq_div #btnUploadfiles').attr('disabled', true);
                        swal("Request sent!", "Your request have been submitted successfully. An email with all your request details and file attachment has been sent to the ALC Team.", "success");
                    }
                    return false;
                }

            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
                $(".empformreq_div .status-loading").show();
            },
            add: function (e, data) {
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xslx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                    'csv', 'txt', 'rtf', 'zip', 'jpg', 'jpeg', 'png'
                ];

                $.each(files, function (x, i) {
                    var err = "";
                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    //if (this.type.length == 0 || ext)
                    //    err = "The file type is not supported.";
                    //else if (this.type.length && !acceptFileTypes.test(this.type)) {
                    //    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    //    err = "The file type of '." + ext + "' is not supported.";
                    //}
                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('.empformreq_div #fileList');
                    var pname = $('#efr_title').val();
                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('a#filename').attr('fname', name);
                    item.find('span.size').text(size);
                    item.find('span.pitname').text(pname);
                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        $(".empformreq_div #btnAddfiles").attr("disabled", true);
                        $(".empformreq_div #btnUploadfiles").attr("disabled", true);
                        $(".empformreq_div .totalfiles").text(err);
                        return true;
                    } else {
                        //item.find('a:eq(0)').show();
                        item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                            if (data.files != null) {
                                for (i = 0; i < data.files.length; i++) {
                                    if (data.files[i].name == $(this).attr('fname')) {
                                        data.files[i] = {};
                                    }
                                }
                            }
                            item.remove();
                            //var itms = $('.upload-container .totalfiles span:eq(1)');
                            $(".empformreq_div .totalfiles").text("File has been attached.");
                        });
                    }


                    data.context = item;
                    data.urlparam = {
                        name: this.name,
                        size: this.size,
                        type: this.type,
                        file: this.file
                    };
                    $('.empformreq_div #btnClearList').on('click', function () {


                        data.urlparam = {};
                        data.files = [''];

                        $('#efr_title').val("");
                        $('.empformreq_div #filelist').empty();
                        $('.empformreq_div .totalfiles').html("<span>0</span> of <span>0</span> files uploaded");
                        $('.empformreq_div #btnUploadfiles').attr('disabled', false);
                    });

                    $('.empformreq_div #btnUploadfiles').on('click', function () {
                        data.submit();
                    });

                });
            },
            submit: function (e, data) {
                var pitt = $('.memoform #efr_title').val();
                data.formData = {
                    paramsid: "documents",
                    fileparams: JSON.stringify(data.urlparam),
                    rid: 0,
                    pt: pitt
                };
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({
                    message: "Error: " + jqXHR.responseText,
                    delay: 6000
                });
            }
        });
    }

    $('#loadEFR').on('click', function () {
        initEmpFormReqs();
    });

    $(document).on("click", ".dnld_efr", function (i, v) {
        window.location.href = "Admin/DownloadEmpFormRequest/?DOCID=" + $(this).attr("id");
    })

    $(document).on("click", "#settingsTab a", function () {
        var tab = $(this).attr("href");

        if (tab == "#tabs-as") {
            $("#upload-efr").hide();
            $("#edit_agency").show();
            $(".cancelagency").show();
            $(".saveagency").show();
        } else if (tab == "#tabs-cefr") {
            $("#upload-efr").show();
            $("#edit_agency").hide();
           
            initEmpFormReqs();
        } else if (tab == "#tabs-usract") {
            $(".loader").show();
            $("#upload-efr").hide();
            $("#edit_agency").hide();         
           // $(".filterdiv .form-control").prop("disabled", false);
            initUserActivity(ftrs);            
            $("#ftrDate, #ftrDate2").val(datenow);
            
        }
        //kim hide upload-eft and edit_agency
        else if (tab == "#tabs-lib") {
            $("#upload-efr").hide();
            $(".saveagency").hide();
            $(".cancelagency").hide();
            $("#edit_agency").hide();
        }
    });


    var SearchTextCity = "City";
    $("#city").val(SearchTextCity).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextCity);
    }).focus(function (e) {
        if ($(this).val() == SearchTextCity)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "city"}',
                success: function (data) {
                   
                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#city").val(ui.item.city);
            $("#state").val(ui.item.state);
            $("#state_id").val(ui.item.state_id);
            $("#zip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search City...") {
                $("#state").val("");
                $("#state_id").val("");
                $("#zip").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.city + "</a>")
            .appendTo(ul);
    };


    var SearchTextState = "State";
    $("#state").val(SearchTextState).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextState);
    }).focus(function (e) {
        if ($(this).val() == SearchTextState)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "state"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#city").val(ui.item.city);
            $("state").val(ui.item.state);
            $("#state_id").val(ui.item.state_id);
            $("#zip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search State...") {
                $("#state").val("");
                $("#state_id").val("");
                $("#city").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.state + "</a>")
            .appendTo(ul);
    };


    var SearchTextZip = "Zip";
    $("#zip").val(SearchTextZip).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextZip);
    }).focus(function (e) {
        if ($(this).val() == SearchTextZip)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "zip"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#city").val(ui.item.city);
            $("#state").val(ui.item.state);
            $("#state_id").val(ui.item.state_id);
            $("#zip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search Zipcode...") {
                $("#state").val("");
                $("#state_id").val("");
                $("#city").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.zipcode + "</a>")
            .appendTo(ul);
    };


});

