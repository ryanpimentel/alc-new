﻿$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }

    PHL = $("#csContent");
    PHL.layout({
        center: {
            paneSelector: "#headerCS",
            size: '40%',
            minSize: '30%',
            maxSize: '40%',
            north: {
                paneSelector: "#wrapToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
                , north__childOptions: {
                    size: 0,
                    paneSelector: "#csGridToolbar",
                    closable: false,
                    slidable: false,
                    resizable: false,
                    spacing_open: 0
                }
                ,center__childOptions: {
                    paneSelector: "#raToolbar",
                    closable: false,
                    slidable: false,
                    resizable: false,
                    spacing_open: 0
                }
            },
        },
        south: {
            paneSelector: "#middleCS",
            size: '60%',
            minSize: '20%',
            maxSize: '60%'
        },
        south__autoResize: true,
        center__autoResize: true,
        resizerDragOpacity: .6,
        livePanelResizing: false,
        closable: false,
        slidable: false,
        onresize: function () {
            resizeGrids();
        }

    });


    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#headerCS").height() - 56).width($("#headerCS").width());
            $('.ui-jqgrid-bdiv').eq(1).height($("#middleCS").height() - 56).width($('#middleCS').width());
            $('#grid').setGridWidth($("#csGrid").width(), true);
            $('#urGrid').setGridWidth($("#userCSGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });
    $('#ftrRole,#ftrDepartment,#ftrSysUser,#ftrText,#ftrStatus').change(function () {
        var res = $('#ftrText').val();
        var rm = $('#ddlFilter').val();
        var rd = $('#ddlFilter :selected').text();
        var rl = $('#ftrRole :selected').text();
        var dl = $('#ftrDepartment :selected').text();
        var ia = $('#ftrStatus :selected').val();
        var su = $('#ftrSysUser :selected').val();

        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'Id';
        if (rm == 2)
            col = "Name";
        else if (rm == 3)
            col = "Email";


        if (rl != 'All')
            ftrs.rules.push({ field: 'RoleDescription', op: 'eq', data: rl });
        if (dl != 'All')
            ftrs.rules.push({ field: 'Department', op: 'eq', data: dl });
        if (ia != '-1')
            ftrs.rules.push({ field: 'IsActive', op: 'eq', data: ia });
        if (su != '-1')
            ftrs.rules.push({ field: 'NonSysUser', op: 'eq', data: su });
        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });

    var SearchShift = "";
    $("#txtShift").val(SearchShift).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchShift);
    }).focus(function (e) {
        if ($(this).val() == SearchShift)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            var csid = $('#grid').jqGrid('getGridParam', 'selrow') || '';
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "shift_v2", name:"' + request.term + '",csid:"' + csid + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id,
                            start: item.start,
                            end: item.end
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {
            $("#txtShift").val(ui.item.label);
            $("#txtShiftId").val(ui.item.value);
            $("#lblSched").html("(" + ui.item.start + ' - ' + ui.item.end + ")");
            return false;
        },
        change: function (event, ui) {

        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).on('blur', function () {
        if($(this).val() == '')
            $("#lblSched").html('');
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a><span>" + item.label + ' - (' + item.start + ' - ' + item.end + ")</span></a>")
                .appendTo(ul);
    };

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Shift",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "carestaff_shift", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }

            row = $('.form').extractJson();

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];

            $('.dayofweek').find('input[type="checkbox"]').each(function () {
                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun': dow.push('0'); break;
                        case 'Mon': dow.push('1'); break;
                        case 'Tue': dow.push('2'); break;
                        case 'Wed': dow.push('3'); break;
                        case 'Thu': dow.push('4'); break;
                        case 'Fri': dow.push('5'); break;
                        case 'Sat': dow.push('6'); break;
                    }
                }
            });

            if (dow.length == 0) {
                alert('Please choose day of week schedule.');
                return;
            }
            row.DaySchedule = dow.join(',');
        }
        row.CarestaffId = $('#grid').jqGrid('getGridParam', 'selrow');
        
        var data = { func: "carestaff_shift_v2", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: 480,
            height: 240,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({ message: "Shift " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                            else if (m.result == -2) {
                                var oMsg = " there are schedule tasks that are assigned to it.";
                                if(mode == 1)
                                    oMsg = " it overlaps with other existing schedules."
                                $.growlWarning({ message: "Cannot " + (mode == 1 ? 'save' : 'update') + " carestaff shift because " + oMsg, delay: 6000 });
                                return;
                            }
                            $("#diag").dialog("close");
                            if (mode == 1) isReloaded = true;
                            else {
                                fromSort = true;
                                new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                            }
                            $('#urGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            var G = $('#urGrid');
            var rowid = $('#urGrid').jqGrid('getGridParam', 'selrow');
            G.setSelection(rowid, false);

            var row = G.jqGrid('getRowData', rowid);
            if (row) {
                $(".form").clearFields();
                $("#lblSched").html('');
                showdiag(1);

                var rowid = $('#grid').jqGrid('getGridParam', 'selrow');
                var row = $('#grid').jqGrid('getRowData', rowid);
                if (row)
                    $("#diag").dialog("option", 'title', 'Shift - ' + row.Name);
            }
        } else if (q.is('.del')) {
            var G = $('#urGrid');
            var id = $('#urGrid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#urGrid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = "Shift deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = "Shift cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3) {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#urGrid')[0]);
                                    $('#urGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                }
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        }
    });
    var setCheck = function (el, str, idx) {
        if (str.indexOf(idx) != -1)
            el.prop('checked', true);
        else el.prop('checked', false);
    }
    $('a.checkall').click(function () {
        var dis = $(this);
        var dd = dis.attr('data-toggle');
        var dow = '0,1,2,3,4,5,6';
        if (dd != 'cc') {
            dow = '';
            dis.attr('data-toggle', 'cc');
            dis.html('Check All Days');
        } else {
            dis.attr('data-toggle', 'dd');
            dis.html('Uncheck All Days');
        }
        $('.dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            switch (d.attr('name')) {
                case 'Sun': setCheck(d, dow, '0'); break;
                case 'Mon': setCheck(d, dow, '1'); break;
                case 'Tue': setCheck(d, dow, '2'); break;
                case 'Wed': setCheck(d, dow, '3'); break;
                case 'Thu': setCheck(d, dow, '4'); break;
                case 'Fri': setCheck(d, dow, '5'); break;
                case 'Sat': setCheck(d, dow, '6'); break;
            }
        });

    });


    $(function () {
        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();
        var removeByAttr = function (arr, attr, value) {
            var i = arr.length;
            while (i--) {
                if (arr[i]
                    && arr[i].hasOwnProperty(attr)
                    && (arguments.length > 2 && arr[i][attr] === value)) {

                    arr.splice(i, 1);

                }
            }
            return arr;
        }
        function beforeProcessing(data, status, xhr) {
            var delList = [];
            if (data && data.length > 0) {
                var delList = $.grep(data, function (a) { return a.NonSysUser == 1 });
                $.each(delList, function (i, a) {
                    removeByAttr(data, 'Id', a.Id);
                });
            }
        }

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=userv2",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['User ID', 'Name', 'Email', 'SSN', 'Role', 'Department', 'Is Active', 'User Type','Date Created', 'Date Updated'],
            colModel: [
              { hidden: false, key: true, name: 'Id', index: 'Id' },
              { hidden: false, key: false, name: 'Name', index: 'Name', sortable: true},
              { hidden: false, key: false, name: 'Email', index: 'Email', editable: true, sortable: true },
              { hidden: false, key: false, name: 'SSN', index: 'SSN', editable: true, sortable: true },
              { hidden: false, key: false, name: 'RoleDescription', index: 'RoleDescription', editable: true, sortable: true },
              { hidden: false, key: false, name: 'Department', index: 'Department', editable: true, sortable: true },
              { hidden: false, key: false, name: 'IsActive', index: 'IsActive', sortable: true, edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Active', '0': 'Inactive' } } },
              { hidden: false, key: false, name: 'NonSysUser', index: 'NonSysUser', sortable: true, edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Non-System User', '0': 'System User' } } },
              { hidden: false, key: false, name: 'DateCreated', index: 'DateCreated', editable: true, sortable: true },
              { hidden: false, key: false, name: 'DateModified', index: 'DateModified', editable: true, sortable: true }
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            gridview: true,
            //caption: 'Care Staff',
            mtype: "GET",
            emptyrecords: 'No records to display',
            //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSelectRow: function (id) {
                onFocusUserGrid(id);
            },
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            beforeProcessing: function (data, status, xhr) { beforeProcessing(data, status, xhr) },
            loadComplete: function (data) {
                if ($('#ftrRole').find('option').size() <= 0) {
                    var optsRm = [];

                    $.each(unique($.map(data, function (o) { return o.RoleDescription })), function () {
                        optsRm.push('<option>' + this + '</option>');
                    });
                    $('#ftrRole').html(optsRm.join(''));

                    $("#ftrRole").html($('#ftrRole option').sort(function (x, y) {
                        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                    }));
                    $("#ftrRole").prepend('<option>All</option>');
                    $("#ftrRole").get(0).selectedIndex = 0;
                }
                if ($('#ftrDepartment').find('option').size() <= 0) {
                    optsRm = [];

                    $.each(unique($.map(data, function (o) { return o.Department })), function () {
                        optsRm.push('<option>' + this + '</option>');
                    });
                    $('#ftrDepartment').html(optsRm.join(''));

                    $("#ftrDepartment").html($('#ftrDepartment option').sort(function (x, y) {
                        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                    }));
                    $("#ftrDepartment").prepend('<option>All</option>');
                    $("#ftrDepartment").get(0).selectedIndex = 0;
                }

                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }
                var maxId = data && data[0] ? data[0].Id : '';
                $("#grid").setSelection(maxId);
                delete isReloaded;
            }
        });

        $("#grid").jqGrid('bindKeys');
        jQuery("#grid").jqGrid('sortableRows');
        function unique(array) {
            return $.grep(array, function (el, index) {
                return index == $.inArray(el, array);
            });
        }
        function onFocusUserGrid(userid) {
            if ($('#urGrid')[0] && $('#urGrid')[0].grid)
                $.jgrid.gridUnload('urGrid');

            var GURow2 = new Grid_Util.Row();

            $('#urGrid').jqGrid({
                url: "Admin/GetGridData?func=carestaff_shift_v2&param=" + userid,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                colNames: ['CSId', 'ShiftId', 'CarestaffId', 'Shift', 'Start Time', 'End Time', 'Day Schedule', 'DaySchedule', 'Created By', 'Date Created'],
                colModel: [
                  { key: true, hidden: true, name: 'CSId', index: 'CSId' },
                  { key: false, hidden: true, name: 'ShiftId', index: 'ShiftId' },
                  { key: false, hidden: true, name: 'CarestaffId', index: 'CarestaffId' },
                  { key: false, name: 'Shift', index: 'Shift' , formatter: "dynamicLink", formatoptions: {
                          onClick: function (rowid, irow, icol, celltext, e) {
                              $("#lblSched").html('');
                              var G = $('#urGrid');
                              G.setSelection(rowid, false);
                              var row = G.jqGrid('getRowData', rowid);
                              if (row) {
                                  $(".form").clearFields();
                                  $('#txtCarestaffName').val(row.CarestaffName);
                                  $('#txtCarestaffId').val(row.CarestaffId);
                                  $('#txtCSId').val(row.CSId);
                                  $('#txtShift').val(row.Shift);
                                  $('#txtShiftId').val(row.ShiftId);
                                  if (row.StartTime != "" && row.EndTime != "")
                                      $("#lblSched").html("(" + row.StartTime + ' - ' + row.EndTime + ")");
                                  var setCheck = function (el, str, idx) {
                                      if (str.indexOf(idx) != -1)
                                          el.prop('checked', true);
                                  }
                                  var dow = row.DaySchedule;
                                  $('.dayofweek').find('input[type="checkbox"]').each(function () {
                                      var d = $(this);
                                      switch (d.attr('name')) {
                                          case 'Sun': setCheck(d, dow, '0'); break;
                                          case 'Mon': setCheck(d, dow, '1'); break;
                                          case 'Tue': setCheck(d, dow, '2'); break;
                                          case 'Wed': setCheck(d, dow, '3'); break;
                                          case 'Thu': setCheck(d, dow, '4'); break;
                                          case 'Fri': setCheck(d, dow, '5'); break;
                                          case 'Sat': setCheck(d, dow, '6'); break;
                                      }
                                  });
                                  $('a.checkall').attr('data-toggle', 'cc').html('Check All Days');
                                  showdiag(2);

                                  var rowid = $('#grid').jqGrid('getGridParam', 'selrow');
                                  var row = $('#grid').jqGrid('getRowData', rowid);
                                  if (row)
                                      $("#diag").dialog("option", 'title', 'Shift - ' + row.Name);
                              }
                          }
                      }
                  },
                  { key: false, name: 'StartTime', index: 'StartTime' },
                  { key: false, name: 'EndTime', index: 'EndTime' },
                  { key: false, name: 'DayScheduleDisplay', index: 'DayScheduleDisplay' },
                  { key: false, hidden: true, name: 'DaySchedule', index: 'DaySchedule' },
                  { key: false, name: 'CreatedBy', index: 'CreatedBy' },
                  { key: false, name: 'DateCreated', index: 'DateCreated' }
                ],
                //pager: jQuery('#pager'),
                onSortCol: function () {
                    fromSort2 = true;
                    GURow2.saveSelection.call(this);
                },
                loadComplete: function (data) {
                    if ($("#urGrid").getGridParam("records") == 0)
                        $('#urPager').html('No results found.');
                    else
                        $('#urPager').html('');

                    if (typeof fromSort2 != 'undefined') {
                        GURow2.restoreSelection.call(this);
                        delete fromSort2;
                        return;
                    }

                    var maxId = data && data[0] ? data[0].Id : 0;
                    if (typeof isReloaded != 'undefined') {
                        $.each(data, function (k, d) {
                            if (d.Id > maxId) maxId = d.Id;
                        });
                    }
                    if (maxId > 0)
                        $("#urGrid").setSelection(maxId);
                    delete isReloaded;
                    resizeGrids();
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false,
                sortname: 'Id',
                sortorder: 'asc',
                loadonce: true
            });
            $("#urGrid").jqGrid('bindKeys');
        }
    });  

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        var myGrid = $('#grid'),
        selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
        iValue = myGrid.jqGrid('getInd', selRowId),
        DeptN = myGrid.jqGrid('getCell', selRowId, 'Description');

        $("td:hidden,th:hidden", "#urGrid").remove();
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Is System Defined</td><td bgcolor='#87AFC6'> Date Created</td><td bgcolor='#87AFC6'>Date Updated</td></tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('urGrid'); // id of table
        tab1 = document.getElementById('grid');
        tab_text = tab_text + tab1.rows[iValue].innerHTML + "</tr><tr></tr><tr><td colspan='7'>Department Users: </td></tr><tr><td bgcolor='#87AFC6'>Id</td><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Email</td><td bgcolor='#87AFC6'>Department</td><td bgcolor='#87AFC6'>Is Active</td><td bgcolor='#87AFC6'>Date Created</td><td bgcolor='#87AFC6'>Date Updated</td></tr><tr>";

        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";

            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_DepartmentUsers.xls");
        }
        else                 //other browser not tested on IE 11
            a.id = 'ExcelDL';
        a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
        a.download = 'ALC_DepartmentUsers_' + DeptN + '.xls';
        document.body.appendChild(a);
        a.click(); // Downloads the excel document
        document.getElementById('ExcelDL').remove();
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    });
    //============================

});