﻿// height and width to adjust the last row "Action" and the scroll where place horizontally kim 11/26/2021
function setHeight() {
       //Edited this on 01-20-22: Angelie here
    $('.dataTables_scrollBody').width($(window).width() - 600);
 
    $('.dataTables_scrollBody').height($(window).height() - 400);
    $(window).resize(function () {

        $('.dataTables_scrollBody').width($(window).width() - 600);
        $('.dataTables_scrollBody').height($(window).height() - 400);

        if ($(window).width() > 400 || $(window).height() > 400 || $(window).height() > 100) {


            $('.dataTables_scrollBody').height($(window).height() - 325);
        } else {

            $('.dataTables_scrollBody').height($(window).height());
        }


        $('#supplies_tbl').DataTable().columns.adjust();
    })


    var scroll = new PerfectScrollbar('.dataTables_scrollBody');
    $('#supplies_tbl').DataTable().columns.adjust();
}

$(document).ready(function () {

	//Added this on 01-20-22: Angelie here
    $.fn.dataTable.render.ellipsis = function () {
        return function (data, type, row) {
            if (data) {
                return type === 'display' && data.length > 10 ?
                    '<span title="' + data + '">' + data.substr(0, 10) + '…' + '&#8230;</span>' :
                    data;
            }


        };
    };

    //------------------------------
    //FUNCTIONS
    var table_supplies = "";
    var load_supplies = function () {
    
            
        $("#supplies_tbl").DataTable().destroy();
        table_supplies = $("#supplies_tbl").DataTable({
            "scrollX": true,
            "processing": true,
            "scrollY": "true",
            "scrollCollapse": true,
            "serverSide": true,
            "orderMulti": false,
            "bFilter": false,
            "responsive": true,
            select: true,
            //"pageLength": 25,
            "ajax": {
                "url": "Admin/GetGridData?func=supply&param=",
                "type": "POST",
                "dataType": "json"
            },
            "columns": [
                { "data": "Description", "name": "Description" },
                { "data": "Form", "name": "Form" },
                { "data": "Strength", "name": "Strength" },
                { "data": "Generic", "name": "Generic" },
                { "data": "ReferenceDrug", "name": "ReferenceDrug" },
                { "data": "ActiveIngredient", "name": "ActiveIngredient" },
                //{ "data": "SupplyCode", "name": "SupplyCode" },
                { "data": "Status", "name": "Status" },
                //{ "data": "ProductCode", "name": "ProductCode" },
                { "data": "ProductType", "name": "ProductType" },
                { "data": "VolumeSize", "name": "VolumeSize" },
                { "data": "Count", "name": "Count" },
                { "data": null, "name": "Id" }

            ],
            "columnDefs": [{
                targets: -1,
                "data": null,
               // "defaultContent": '\n <span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-edit"></i>\n</a><div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item edit" href="#"><i class="la la-edit"></i> Edit Details</a>\n<a class="dropdown-item del" href="#"><i class="la la-trash-o"></i> Delete</a>\n<a class="dropdown-item add_supCount" href="#"><i class="la la-plus"></i> Add Supply</a>\n</div>\n</span>\n                        '
                "defaultContent": '\n <span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-edit"></i>\n</a><div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item edit" href="#"><i class="la la-edit"></i> Edit Details</a>\n<a class="dropdown-item add_supCount" href="#"><i class="la la-plus"></i> Add Supply</a>\n</div>\n</span>\n                        '  //removed delete option : 10-10-2022: Cheche
                //title: "Actions",
                //orderable: !1,
                //render: function (a, e, n, t) {
                //    return '\n <span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-edit"></i>\n</a><div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item edit" href="#"><i class="la la-edit"></i> Edit Details</a>\n<a class="dropdown-item del" href="#"><i class="la la-trash-o"></i> Delete</a>\n<a class="dropdown-item add_supCount" href="#"><i class="la la-plus"></i> Add Supply</a>\n</div>\n</span>\n                        '
                //}
            }, {
                    //Added this on 01-20-22: Angelie here
                //targets: [0, 1, 2, 3, 5, 6, 8],
                targets: [0, 1, 2, 3, 5],
                render: $.fn.dataTable.render.ellipsis()

            }, {

                //targets: 7,
                targets: 6,
                render: function (a, e, n, t) {
                    var s = {
                        "true": {
                            title: "Active",
                            class: "m-badge--success"
                        },

                        "false": {
                            title: "Inactive",
                            class: " m-badge--danger"
                        }
                    };
                    return void 0 === s[a] ? a : '<span class="m-badge ' + s[a].class + ' m-badge--wide">' + s[a].title + "</span>"
                }

            }, {
                //targets: 9,
                targets: 7,
                render: function (a, e, n, t) {
                    var s = {
                        "1": {
                            title: "Medicine"
                        },
                        "2": {
                            title: "Supply"
                        }
                       
                    };
                    return void 0 === s[a] ? a : s[a].title
                }
            }, {
                //targets: 10,
                targets: 8,
                render: function (a, e, n, t) {
                    var s = {
                        "1": {
                            title: "1 -tab/cap"
                        }
                    };
                    return void 0 === s[a] ? a + " -ml/bottle" : s[a].title
                }
            }],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="la la-download"></i> Export to Excel',
                    title: 'ALC_Medicines_and_Supplies',
                    className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                    init: function (api, node, config) {
                        $(node).removeClass('dt-button')
                    }
                }

            ]

        });
        setHeight();
       
    }

    var load_filtered_supplies = function (ftr, data, isActive, supplyType) {
        supplyType = "&param10=" + supplyType || "";

        $("#supplies_tbl").DataTable().destroy();

        table_supplies = $("#supplies_tbl").DataTable({
            "scrollX": true,
            "scrollY": "true",
            "scrollCollapse": true,
            "processing": true,
            "serverSide": true,
            "orderMulti": false,
            "responsive": true,
            "bFilter": false,
            select: true,
            //"pageLength": 25,
            "ajax": {
                "url": "Admin/GetGridData?func=supply_v2&param=" + ftr + "&param2=" + data + "&param3=" + isActive + supplyType,
                "type": "POST",
                "dataType": "json"
            },
            "columns": [
                { "data": "Description", "name": "Description" },
                { "data": "Form", "name": "Form" },
                { "data": "Strength", "name": "Strength" },
                { "data": "Generic", "name": "Generic" },
                { "data": "ReferenceDrug", "name": "ReferenceDrug" },
                { "data": "ActiveIngredient", "name": "ActiveIngredient" },
                //{ "data": "SupplyCode", "name": "SupplyCode" },
                { "data": "Status", "name": "Status" },
                //{ "data": "ProductCode", "name": "ProductCode" },
                { "data": "ProductType", "name": "ProductType" },
                { "data": "VolumeSize", "name": "VolumeSize" },
                { "data": "Count", "name": "Count" },
                { "data": null, "name": "Id" }

            ],
            "columnDefs": [{
                targets: -1,
                "data": null,
             //   "defaultContent": '\n <span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-edit"></i>\n</a><div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item edit" href="#"><i class="la la-edit"></i> Edit Details</a>\n<a class="dropdown-item del" href="#"><i class="la la-trash-o"></i> Delete</a>\n<a class="dropdown-item add_supCount" href="#"><i class="la la-plus"></i> Add Supply</a>\n</div>\n</span>\n                        '
                "defaultContent": '\n <span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-edit"></i>\n</a><div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item edit" href="#"><i class="la la-edit"></i> Edit Details</a>\n<a class="dropdown-item add_supCount" href="#"><i class="la la-plus"></i> Add Supply</a>\n</div>\n</span>\n                        '  //removed delete option : 10-10-2022: Cheche
            }, {
                    //Added this on 01-20-22: Angelie here
                //targets: [0, 1, 2, 3, 5, 6, 8],
                targets: [0, 1, 2, 3, 5],
                render: $.fn.dataTable.render.ellipsis()


            }, {
                //targets: 7,
                targets: 6,
                render: function (a, e, n, t) {
                    var s = {
                        "true": {
                            title: "Active",
                            class: "m-badge--success"
                        },
                        "false": {
                            title: "Inactive",
                            class: " m-badge--danger"
                        }
                    };
                    return void 0 === s[a] ? a : '<span class="m-badge ' + s[a].class + ' m-badge--wide">' + s[a].title + "</span>"
                }

            }, {
                //targets: 9,
                targets: 7,
                render: function (a, e, n, t) {
                    var s = {
                        "1": {
                            title: "Medicine"
                        },
                        "2": {
                            title: "Supply"
                        },
                    
                    };
                    return void 0 === s[a] ? a : s[a].title
                }
            }, {
                //targets: 10,
                targets: 8,
                render: function (a, e, n, t) {
                    var s = {
                        "1": {
                            title: "1 tab/cap"
                        }
                    };
                    return void 0 === s[a] ? a + " -ml/bottle" : s[a].title
                }
            }],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="la la-download"></i> Export to Excel',
                    title: 'ALC_Medicines_and_Supplies',
                    className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                    init: function (api, node, config) {
                        $(node).removeClass('dt-button')
                    }
                }

            ]

        });

        setHeight();

    }
    // kim add this function for all active 03/16/22
    var load_allactive_supplies = function () {


        $("#supplies_tbl").DataTable().destroy();
        table_supplies = $("#supplies_tbl").DataTable({
            "scrollX": true,
            "processing": true,
            "scrollY": "true",
            "scrollCollapse": true,
            "serverSide": true,
            "orderMulti": false,
            "bFilter": false,
            "responsive": true,
            select: true,
            //"pageLength": 25,
            "ajax": {
                "url": "Admin/GetGridData?func=supply_all&param=",
                "type": "POST",
                "dataType": "json"
            },
            "columns": [
                { "data": "Description", "name": "Description" },
                { "data": "Form", "name": "Form" },
                { "data": "Strength", "name": "Strength" },
                { "data": "Generic", "name": "Generic" },
                { "data": "ReferenceDrug", "name": "ReferenceDrug" },
                { "data": "ActiveIngredient", "name": "ActiveIngredient" },
                //{ "data": "SupplyCode", "name": "SupplyCode" },
                { "data": "Status", "name": "Status" },
                //{ "data": "ProductCode", "name": "ProductCode" },
                { "data": "ProductType", "name": "ProductType" },
                { "data": "VolumeSize", "name": "VolumeSize" },
                { "data": "Count", "name": "Count" },
                { "data": null, "name": "Id" }

            ],
            "columnDefs": [{
                targets: -1,
                "data": null, 
               // "defaultContent": '\n <span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-edit"></i>\n</a><div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item edit" href="#"><i class="la la-edit"></i> Edit Details</a>\n<a class="dropdown-item del" href="#"><i class="la la-trash-o"></i> Delete</a>\n<a class="dropdown-item add_supCount" href="#"><i class="la la-plus"></i> Add Supply</a>\n</div>\n</span>\n                        '   
                "defaultContent": '\n <span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-edit"></i>\n</a><div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item edit" href="#"><i class="la la-edit"></i> Edit Details</a>\n<a class="dropdown-item add_supCount" href="#"><i class="la la-plus"></i> Add Supply</a>\n</div>\n</span>\n                        ' //removed delete option : 10-10-2022: Cheche
                //title: "Actions",
                //orderable: !1,
                //render: function (a, e, n, t) {
                //    return '\n <span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="la la-edit"></i>\n</a><div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item edit" href="#"><i class="la la-edit"></i> Edit Details</a>\n<a class="dropdown-item del" href="#"><i class="la la-trash-o"></i> Delete</a>\n<a class="dropdown-item add_supCount" href="#"><i class="la la-plus"></i> Add Supply</a>\n</div>\n</span>\n                        '
                //}
            }, {
                //Added this on 01-20-22: Angelie here
                //targets: [0, 1, 2, 3, 5, 6, 8],
                targets: [0, 1, 2, 3, 5],
                render: $.fn.dataTable.render.ellipsis()

            }, {

                //targets: 7,
                targets: 6,
                render: function (a, e, n, t) {
                    var s = {
                        "true": {
                            title: "Active",
                            class: "m-badge--success"
                        },

                        "false": {
                            title: "Inactive",
                            class: " m-badge--danger"
                        }
                    };
                    return void 0 === s[a] ? a : '<span class="m-badge ' + s[a].class + ' m-badge--wide">' + s[a].title + "</span>"
                }

            }, {
                //targets: 9,
                targets: 7,
                render: function (a, e, n, t) {
                    var s = {
                        "1": {
                            title: "Medicine"
                        },
                        "2": {
                            title: "Supply"
                        }

                    };
                    return void 0 === s[a] ? a : s[a].title
                }
            }, {
                //targets: 10,
                targets: 8,
                render: function (a, e, n, t) {
                    var s = {
                        "1": {
                            title: "1 -tab/cap"
                        }
                    };
                    return void 0 === s[a] ? a + " -ml/bottle" : s[a].title
                }
            }],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="la la-download"></i> Export to Excel',
                    title: 'ALC_Medicines_and_Supplies',
                    className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                    init: function (api, node, config) {
                        $(node).removeClass('dt-button')
                    }
                }

            ]

        });
        setHeight();

    }










    //doAjax function on previous version
    var updateSupplies = function (mode, row, func) {

        func = func || "";
        if (mode == 0) {
            var data = { func: "supply", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    m.Status = m.Status.toString();

                    if (m.Status == "false") {
                        m.Status = "0";
                    } else if (m.Status == "true") {
                        m.Status = "1";
                    }

                    $("#update_supply_form").mapJson(m, 'name');
                    $("#update_supply_form #SupplyCount").val(m.SupplyCount).attr("disabled", true).text(m.SupplyCount);

                    $("#update_supply_form #ddlStatus").val(m.Status).trigger("change");

                    if (m.ProductType == "2") {
                        $("#update_supply_form #ddlType").val("2").trigger("change");
                    } else {
                        $("#update_supply_form #ddlType").val("1").trigger("change");
                    }


                    $("#updatemode").val(2);
                }
            });
            setHeight();
            return;
        }

        //do other modes
        if (mode == 1 || mode == 2) {
            if (func == "") {
                var form = document.getElementById('update_supply_form');
                for (var i = 0; i < form.elements.length; i++) {
                    if (form.elements[i].value === '' && form.elements[i].hasAttribute('required')) {
                        swal("<span>Fill in all required (<span style=\"color: red;\">*</span>) fields. </span>")
                        return false;
                    }
                }
            }

        }

        if (mode == 2 && func != "addcount") { row.SupplyCount = "0"; };

        var data = { func: "supply", mode: mode, data: row };

       // var option = ["", "added", "updated", "deleted"];
       var option = ["", "added", "updated"];  //removed delete option : 10-10-2022 : Cheche
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (m.result == -3) {//inuse
                    swal("This data is currently in use.", "", "warning");

                } else {
                    swal("Supply has been " + option[mode] + " successfully!", "", "success");
                    //toastr.success("Supply has been " + option[mode] + " successfully!");
                    var pageinfo = $("#supplies_tbl").DataTable().page.info();
                    $("#supplies_tbl").DataTable().destroy();
                    $("#supplies_tbl tbody tr").remove();
                    //load_supplies();
              
                    var ftr = $("#ftrDescGen").val();
                    var data = $("#ftrText").val().toLowerCase();
                    var isActive = false;
                    var selected = $("#ftrStatus").val();
                    var supplyType = $("input[name='radio_supplyType']:checked").val();
                   // var supplyStatus = $("input[name='radio_supplyStatus']:checked").val();                   
                   // var supplyType = $("input[name='radio_supplyType']:checked").val();
                   
                    if (selected == "1") {
                        isActive = true;
                    }
                    else if (selected == "0") {
                        isActive = false;
                    }
                    else if (selected == "2") {
                        isActive = "neither";
                    }                                   
                   //kim 05/16/22
                    //if (supplyType == undefined) {
                    //     supplyType = "neither";
                    // }
                    //table_supplies.destroy();
                    //$("#supplies_tbl tbody tr").remove();
                    //kim 05/11/22 
                    load_filtered_supplies(ftr, data, isActive, supplyType);
                  //  load_allactive_supplies();
                    setHeight();
                }

                $(".modal").modal("hide");
                $("#update_supply_diag form").trigger("reset");


            }
        });
    }

    //ON CLICKS
    $("#update_supply").on("click", function () {
        var row = $('#update_supply_diag').find('form').extractJson();
        var mode = parseInt($("#updatemode").val());
        //   row.SupplyCount = "0"; //to prevent the doubling of supply count
        updateSupplies(mode, row);
    })


    $(".add").on("click", function () {
        $("#updatemode").val(1);
        $("#update_supply_form #SupplyCount").removeAttr("disabled");
    })

//commented out on 10-10-2022 : Cheche : Redundancy
  //  $(document).on("click", ".del", function () {

  //      //var id = $(this).closest("tr").attr("id");
  //      var data = table_supplies.row($(this).parents('tr')).data();
  //      var id = data.Id.toString();
 //       swal({
 //           title: "Are you sure you want to delete this data?",
//            text: "You won't be able to revert this!",
 //           type: "warning",
//            showCancelButton: !0,
  //          confirmButtonText: "Yes"
 //       }).then(function (e) {
   //         e.value && updateSupplies(3, { Id: id }) //mode 3 is for delete

 //       })

 //   })

    $(document).on("click", ".edit", function () {
        // var id = $(this).closest("tr").attr("id");
        var data = table_supplies.row($(this).parents('tr')).data();
        var id = data.Id.toString();
        $("#update_supply_diag").modal("toggle");
        updateSupplies(0, { Id: id })
    })

    $(document).on("click", ".add_supCount", function () {
        $("#add_supply_count_dialog").modal("toggle");

        //var id = $(this).closest("tr").attr("id");
        var data = table_supplies.row($(this).parents('tr')).data();
        var id = data.Id.toString();
        $("#SupplyCount").forceNumericOnly();

        var data = { func: "supply", id: id };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (m.Status == true) {
                    m.Status = 1;
                } else if (m.Status == false) {
                    m.Status = 0;
                }

                $("#ddlStatus").val(m.Status);
                $("#update_supply_count_form").mapJson(m, 'name');
                $("#update_supply_count_form #SupplyCount").val(0);

            }
        });


    })

    $("#add_supply_count").on("click", function () {
        var row = $('#add_supply_count_dialog').find('form').extractJson();
        updateSupplies(2, row, 'addcount');
    })

    //INITIALIZATION
   // load_supplies();
    load_allactive_supplies();
    $('.loader').hide();
   // load_ftrStatus();
    var isChecked = $('#supply_head #isActive').is(':checked');

    //Added this on 01-20-22: Angelie here
    $("#ftrText").keypress(
        function (event) {
            if (event.which == '13') {
                event.preventDefault();
            }
        });

    $("#ftrText").keyup(function (e) {
        
        var supplyStatus = $("#ftrStatus").val();
        var ftr = $("#ftrDescGen").val();
        var data = $(this).val().toLowerCase();
        var isActive = false;

      //  var supplyStatus = $("input[name='radio_supplyStatus']:checked").val();
        var supplyType = $("input[name='radio_supplyType']:checked").val();

        if (supplyStatus == "1") {
            isActive = true;
        }
        if ((supplyType != undefined && supplyStatus == undefined) || (supplyType == undefined && supplyStatus == undefined)) {
            isActive = "neither";
        }

        if (e.keyCode == 8) {
            data = $(this).val().toLowerCase();
            if (data == "" && isActive == false) {
                table_supplies.destroy();
                $("#supplies_tbl tbody tr").remove();
                load_supplies();
                //setHeight();
            } else {
                table_supplies.destroy();
                $("#supplies_tbl tbody tr").remove();
                load_filtered_supplies(ftr, data, isActive, supplyType);
                //setHeight();
            }
        } else {
            table_supplies.destroy();
            $("#supplies_tbl tbody tr").remove();
            load_filtered_supplies(ftr, data, isActive, supplyType);
            //setHeight();

        }
    });

    

    //cheche was here to modify : 3-7-2022
    $('#ftrStatus, .supplyType').click(function () {
     
        var ftr = $("#ftrDescGen").val();
        var data = $("#ftrText").val().toLowerCase();
        var selected = $("#ftrStatus").val();
        var isActive = false;

        // var supplyStatus = $("input[name='radio_supplyStatus']:checked").val();
        var supplyStatus = $('#ftrStatus option:selected').text();
        var supplyType = $("input[name='radio_supplyType']:checked").val();

        if (supplyStatus == "2") { // added this block of code for all status
            load_supplies();
        }

        if (selected == "1") { // changed from 1 to Active
            isActive = true;
        }
        if (supplyType != undefined && supplyStatus == undefined) {
            isActive = "neither";

        }
        if (supplyStatus != "All") {
            table_supplies.destroy();
            $("#supplies_tbl tbody tr").remove();

            load_filtered_supplies(ftr, data, isActive, supplyType);
        }
        setHeight();
      
    });
});