﻿
function PrescriptionCalendar(elem) {
    var REPLACESTR = '{0}';
    var $e = $('#' + elem);

    var dowRow = '<tr class="dowRow">{0}</tr>';
    var dayRow = '<tr class="dayRow"><td class="ml ba bt2 bl2" style="background-color:#ccc;"></td>'
        + '<td class="mc ba bt2 br2" style="background-color:#ccc;"></td>{0}</tr>';

    var getDaysArray = function (year, month) {
        var names = ['SU', 'M', 'T', 'W', 'TH', 'F', 'S'];
        var date = new Date(year, month - 1, 1);
        var result = [];
        while (date.getMonth() == month - 1) {
            result.push(date.getDate() + "-" + names[date.getDay()]);
            date.setDate(date.getDate() + 1);
        }
        return result;
    }


    var createHeaderDayRow = function (month, year) {
        var mdrRow = dayRow;
        var mdoRow = dowRow;

        var da = getDaysArray(year, month);
        var strDow = '<td class="dd bt2">{0}</td>';
        var strDay = '<td class="dn bt2">{0}</td>';
        var sDay = '';
        var sDow = '';
        var mR = [];
        var mO = [];
        for (var i = 0; i < 31; ++i) {
            if (i < da.length) {
                var val = da[i].split('-');
                var day = val[0];
                var dow = val[1];
                var sDay = strDay.replace(REPLACESTR, day);
                var sDow = strDow.replace(REPLACESTR, dow);
                if (i == 30) {
                    sDay = $('<div/>').append($(sDay).addClass('br2')).html();
                    sDow = $('<div/>').append($(sDow).addClass('br2')).html();
                }
            } else {
                sDay = strDay.replace(REPLACESTR, '&nbsp;');
                sDow = strDow.replace(REPLACESTR, '&nbsp;');
                if (i == 30) {
                    sDay = $('<div/>').append($(sDay).addClass('br2')).html();
                    sDow = $('<div/>').append($(sDow).addClass('br2')).html();
                }
            }
            mR.push(sDay);
            mO.push(sDow);
        }
        mdrRow = mdrRow.replace(REPLACESTR, mR.join(''));
        mdoRow = mdoRow.replace(REPLACESTR, mO.join(''));
        return { dayRow: mdrRow, dowRow: mdoRow };
    }

    var createNewRow = function (month, year, rdid) {
        var html = [];
        var timeCell = '<td rdid="' + rdid + '" class="dl mc  br2" editableCell="true">{0}</td>';
        var spacerCell = '<tr class="residentMedicationScheduleRowSpacer newRow">{0}</tr>';

        //create days
        var chkDays = [];
        var da = getDaysArray(year, month);
        var strDay = '<td class="dl">{0}</td>';
        var mday = '';
        var eday = '';

        for (var i = 0; i < 31; ++i) {
            mday = '';
            eday = '';
            if (i < da.length) {
                var val = da[i].split('-');
                var day = val[0];
                mday = '';//'<input type="checkbox" value="0"/>';
                eday = strDay.replace(REPLACESTR, mday);
                if (i == 30)
                    eday = $('<div/>').append($(strDay.replace(REPLACESTR, mday)).addClass('br2')).html();
            } else {
                eday = strDay.replace(REPLACESTR, '');
                if (i == 30)
                    eday = $('<div/>').append($(strDay.replace(REPLACESTR, '')).addClass('br2')).html();
            }
            chkDays.push(eday);
        }

        var tc = timeCell.replace(REPLACESTR, '&nbsp;');
        spacerCell = spacerCell.replace(REPLACESTR, tc + chkDays.join(''));
        html.push(spacerCell);

        return html.join('');
    }

    var createDayRow = function (month, year) {
        var drElem = dayRow;
        return createHeaderDayRow(month, year).dayRow;
    }

    var createMedRow = function (meds, month, year) {
        var html = [];
        var chunk = [];
        var tRowspan = 2;
        for (var k = 0; k < meds.length; ++k) {
            /*
			medicine: 'VENLAFAXINE XR 75MG(EFFEXOR)',
			dosage_indication_description: 'Apply one patch to affected area on left thigh daily as directed',
			administration_times: ['8:00AM'],
			days_administered: [1,2,3,4,5,6]
			*/
            if (meds[k]["rd_id"] != null) {
                html = [];
                var medCell = '<tr class="residentMedicationScheduleRow">{0}</tr>';
                var medNameCell = '<td class="ml ba bl2" rowspan="2"><strong>{0}<strong></td>';
                var timeCell = '<td dmtid="{1}" rdid="{2}" class="dl mc  br2" editableCell="true">{0}</td>';

                var spacerCell = '<tr class="residentMedicationScheduleRowSpacer">{0}</tr>';
                var dosageCell = '<tr class="residentMedicationDosageRow">{0}</tr>';
                var medDescCell = '<td  rdid="{1}" rowspan="RS" class="ml ba bl2" editableCell="true">{0}</td>';

                var AT = meds[k].administration_times;
                var AT_len = AT.length <= 3 ? 4 : AT.length;
                for (var y = 0; y < AT_len; ++y) {
                    //create days
                    var chkDays = [];
                    var da = getDaysArray(year, month);
                    var strDay = '<td class="dl">{0}</td>';
                    var mday = '';
                    var eday = '';

                    for (var i = 0; i < 31; ++i) {
                        mday = '';
                        eday = '';
                        if (i < da.length) {
                            var val = da[i].split('-');
                            var day = val[0];
                            if (y < AT.length) {
                                var DA = AT[y].days_administered;
                                if (DA && DA.length > 0) {
                                    var r = $.grep(DA, function (o) { return moment(o.adm_date, 'YYYY-MM-DD').format('D') == day });
                                    if (r.length > 0) {
                                        //kim 06/144/22 modified this code for hovering text and able to see who administered by and comment in every complete/warning
                                        //mday = '<input type="checkbox" disabled="disabled" title="Administered by: ' + r[0].adm_by + '" checked="checked" value="' + (i + 1) + '"/>';
                                        var tip = [];
                                        var admin = "Administered By: " + r[0].adm_by + "&#013;";
                                        var comment = "Comment: N/A";
                                        tip.push('<span style="color:blue;font-weight:bold" title="Administered By:"> </span>');

                                        if (!r[0].is_given) {
                                            tip.push('<div  class="col-lg-12" style="width: 100%; left: 10%;  top: 90%; padding: 0; position:fixed; font-weight: bold;"><span style="color:blue;font-weight:bold">Comment:&nbsp;' + r[0].comment + '</span></div>');
                                            comment = "Comment: " + r[0].comment + "&#013;";
                                        }
                                        if (r[0].is_given) {
                                            mday = $('<div>').append($('<span class="complete" title="' + admin + comment + '" data-toggle="tooltip"></span></div>')).html();
                                            // mday = $('<div>').append($('<span class="complete" ></span></div>').attr('tps', tip.join(''))).html();

                                        }
                                        else {
                                            mday = $('<div>').append($('<span class="warning" title="' + admin + comment + '" data-toggle="tooltip"></span></div>')).html();
                                        }
                                    } else {
                                        if (((i + 2) < parseInt(moment().format('DD'))) || ((i + 1) > parseInt(moment().format('DD'))))
                                            mday = '<input type="checkbox" disabled="disabled" value="' + (i + 1) + '"/>';
                                        //mday = '<span class="warning"></span>';
                                        else
                                            mday = '<input type="checkbox" disabled="disabled" value="' + (i + 1) + '"/>';
                                        //mday = '<input dmtid="' + AT[y].dmt_id + '" rdid="' + meds[k].rd_id + '" class="cbactive" type="checkbox" value="' + (i + 1) + '"/>';
                                    }
                                }
                                else {
                                    if (AT[0].time != null) {
                                        if (((i + 2) < parseInt(moment().format('DD'))) || ((i + 1) > parseInt(moment().format('DD'))))
                                            mday = '<input type="checkbox" disabled="disabled" value="' + (i + 1) + '"/>';
                                        else
                                            mday = '<input type="checkbox" disabled="disabled" value="' + (i + 1) + '"/>';
                                        //mday = '<input dmtid="' + AT[y].dmt_id + '" rdid="' + meds[k].rd_id + '" class="cbactive" type="checkbox" value="' + (i + 1) + '"/>';
                                    }
                                }
                            }
                            eday = strDay.replace(REPLACESTR, mday);
                            if (i == 30)
                                eday = $('<div/>').append($(strDay.replace(REPLACESTR, mday)).addClass('br2')).html();
                        } else {
                            eday = strDay.replace(REPLACESTR, '');
                            if (i == 30)
                                eday = $('<div/>').append($(strDay.replace(REPLACESTR, '')).addClass('br2')).html();
                        }
                        chkDays.push(eday);
                    }

                    var tc = timeCell.replace(REPLACESTR, '&nbsp;').replace("{1}", '').replace("{2}", meds[k].rd_id);
                    //create sched rows
                    if (y == 0) {
                        medNameCell = medNameCell.replace(REPLACESTR, meds[k].medicine);
                        if (AT && AT.length > 0) {
                            if (AT[0].time != null)
                                tc = timeCell.replace(REPLACESTR, moment(AT[0].time, 'hh:mm:ss').format('h:mm A')).replace("{1}", AT[0].dmt_id).replace("{2}", AT[0].rd_id);
                        }
                        medCell = medCell.replace(REPLACESTR, medNameCell + tc + chkDays.join(''));
                        html.push(medCell);
                    } else if (y == 1) {
                        if (AT && AT.length == 2) {
                            if (AT[1].time != null)
                                tc = timeCell.replace(REPLACESTR, moment(AT[1].time, 'hh:mm:ss').format('h:mm A')).replace("{1}", AT[1].dmt_id).replace("{2}", meds[k].rd_id);
                        }
                        else if (AT && AT.length >= 3)
                            if (AT[1].time != null)
                                tc = timeCell.replace(REPLACESTR, moment(AT[1].time, 'hh:mm:ss').format('h:mm A')).replace("{1}", AT[1].dmt_id).replace("{2}", meds[k].rd_id);
                        html.push(spacerCell.replace(REPLACESTR, tc + chkDays.join('')));
                    } else if (y == 2) {
                        medDescCell = medDescCell.replace(REPLACESTR, meds[k].dosage).replace("{1}", meds[k].rd_id);
                        if (AT && AT.length > 2)
                            if (AT[y].time != null)
                                tc = timeCell.replace(REPLACESTR, moment(AT[y].time, 'hh:mm:ss').format('h:mm A')).replace("{1}", AT[y].dmt_id).replace("{2}", meds[k].rd_id);
                        dosageCell = dosageCell.replace(REPLACESTR, medDescCell + tc + chkDays.join(''));
                        html.push(dosageCell);
                    } else if (y == 3) {
                        if (AT && AT.length >= 4)
                            if (AT[3].time != null)
                                tc = timeCell.replace(REPLACESTR, moment(AT[3].time, 'hh:mm:ss').format('h:mm A')).replace("{1}", AT[3].dmt_id).replace("{2}", meds[k].rd_id);
                        html.push(spacerCell.replace(REPLACESTR, tc + chkDays.join('')));
                    } else if (y > 3) {
                        if (AT && AT.length > 4)
                            if (AT[y].time != null)
                                tc = timeCell.replace(REPLACESTR, moment(AT[y].time, 'hh:mm:ss').format('h:mm A')).replace("{1}", AT[y].dmt_id).replace("{2}", meds[k].rd_id);
                        html.push(spacerCell.replace(REPLACESTR, tc + chkDays.join('')));
                        tRowspan++;
                    }
                }

                var part = html.join('');
                part = part.replace('rowspan="RS"', 'rowspan="' + tRowspan + '"');
                tRowspan = 2;
                if (k < meds.length - 1)
                    part += createDayRow(month, year);
                chunk.push(part);
            }
        }

        return chunk.join('');
    }

    var doSave = function (m, s, o, d) {
        var D = { Func: "resident_medication", Mode: m, Data: d, Section: s };
        if (s == 'time') {
            if (d && typeof (d.dmtid) != 'undefined' && d.dmtid != '' && d.rdid != '') {
                D.Mode = d.val != '' ? 2 : 3;
            } else {
                if (d && d.rdid != '' && d.val.trim() != '') {
                    D.Mode = 1;
                } else
                    return;

            }
            //o = string val
        } else if (s == 'dosage') {
            if (d && d.rdid != null) {
                D.Mode = d.val != '' ? 2 : 3;
            } else D.Mode = 1;
        } else if (s == 'day') {
            if (d && d.dmtid != null && d.dmid != null) {
                D.Mode = o.attr('checked') == "checked" ? 2 : 3;
            } else {
                //Data.Checked = (o.attr('checked') == "checked" ? 1 : 0);
                D.Mode = 1;
            }

        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(D),
            dataType: "json",
            success: function (m) {
                //if mode = add then add attribute to target element
                if (s == 'day' && m.return != "0") {
                    o.attr('dmid', m.return);
                } else if (s == "time" && m.return != "0") {
                    d.o.attr('dmtid', m.return);
                    if (d.cb) d.cb();
                }

                if (m.result == 0) {
                    $.growlSuccess({ message: (D.Mode == 1 ? 'Saved' : 'Updated') + " successfully!", delay: 1000 });
                }
            }
        });

    }
    /*
    var bindEditor = function (el) {
        $(el).off('click');
        $(el).bind('click', function (e) {
            var target = e.srcElement || e.target;
            if (target) {
                var $t = $(target);
                if (!$t) return true;
                if (!$t.is(".popupMenu")[0]) {
                    $(".popupMenu").slideUp(300);
                    targetElement = null;
                    if ($t.is('td') && $t.attr('editableCell') != undefined) {
                        //make dosage field editable
                        if ($t.hasClass('ml')) {
                            var text = $t.text();
                            $t.text('');
                            $('<textarea class="editor"/>').appendTo($t).val(text)
							.select().blur(function () {
							    var nText = $(this).val();
							    $(this).parent().text(nText).find('textarea').remove();
							    if (text != nText)
							        doSave(2, 'dosage', $(this), { rdid: $t.attr('rdid'), val: nText });
							});
                        }
                            //make hour time editable
                        else if ($t.hasClass('dl')) {
                            var text = $t.text();
                            if (text == '&nbsp;')
                                text = '';
                            $t.text('');
                            var inpt = $('<input type="text" class="editor"/>');
                            inpt.appendTo($t).val(text)

                            inpt.timepicker({
                                showPeriod: true,
                                showLeadingZero: false,
                                showOn: 'focus',
                                onClose: function () {
                                    var $d = $(this);
                                    var nText = $d.val().trim();
                                    var callback = function () {
                                        if (nText == '') nText = "&nbsp;";
                                        else nText = moment($d.val().trim(), 'h:mm A').format('h:mm A');

                                        var td = $d.parent();
                                        td.html(nText).find('input[type="text"]').remove();

                                        var sibs = td.siblings('.dl');

                                        if ($d.val().trim() == '' || nText == "&nbsp;") {
                                            td.attr('dmtid', '');
                                            sibs.html('');
                                            return;
                                        }

                                        if (sibs.length > 0) {
                                            $.each(sibs, function (i) {
                                                if ($(this).find('input[type="checkbox"]').length == 0) {
                                                    if (((i + 2) < parseInt(moment().format('DD'))) || ((i + 1) > parseInt(moment().format('DD'))))
                                                        $(this).append('<input type="checkbox" disabled="disabled" value="' + (i + 1) + '"/>');
                                                    else $(this).append('<input dmtid="' + td.attr('dmtid') + '" rdid="' + td.attr('rdid') + '" class="cbactive" type="checkbox" value="' + (i + 1) + '"/>');
                                                }
                                            })
                                        }
                                    }
                                    if (text.trim() != nText)
                                        doSave(1, 'time', $d, { o: $t, dmtid: $t.attr('dmtid'), rdid: $t.attr('rdid'), val: nText, cb: callback });
                                    else
                                        callback();


                                }
                            });
                            inpt.focus()
                        }
                    } else if ($t.is('input.cbactive')) {
                        var month = $('#monthfilter').val();
                        var year = $('#yrfilter').val();
                        doSave(1, 'day', $t, {
                            dmid: $t.attr('dmid'),
                            dmtid: $t.attr('dmtid'),
                            val: moment($('#monthfilter').val() + '/' + $t.val() + '/' + $('#yrfilter').val(), 'MM/DD/YYYY').format('MM/DD/YYYY')
                        });
                    }

                    return true;
                }
            }
        });
    }
    */
    this.build = function (o, month, year) {

        var dateText = moment(year + '-' + month).format('MMMM YYYY').toUpperCase();
        var html = [];
        var dob = (o.dateofbirth == null ? "" : new Date(o.dateofbirth));
        dob = (dob == "" ? "" : (dob.getMonth() + 1) + "-" + dob.getDate() + "-" + dob.getFullYear());
        var header = '<tr><td colspan="2" class="br2">&nbsp;</td>'
            + '<td colspan="12" class="bt2">&nbsp;</td>'
            + '<td colspan="7" class="ba2">&nbsp;</td>'
            + '<td colspan="12" class="ba2">&nbsp;</td></tr>';

        var headerLabel = '<tr><td id="yrDate" rowspan="2" class="ml f14 ba bc fc_fff tc bt2 bl2">' + dateText + '</td>'
            + '<td class="mc tc f10"  rowspan="2"><strong>HOUR</strong></td>'
            + '<td colspan="12" class="bl2">&nbsp;</td>'
            + '<td colspan="7" class="ba2"><strong>RESIDENT\'S NAME</strong></td>'
            + '<td colspan="12" class="ba2"><strong>' + (o.resident_name || '') + '</strong></td></tr>'

        var footer1 = '<tr class="dayRow" style="font-weight:bold"><td class="ml ba2">RESIDENT\'S NAME</td>'
            + '<td class="mc ba2">SEX</td><td colspan="5" class="mc ba2">ROOM#</td>'
            + '<td colspan="5" class="mc ba2">DOB</td><td colspan="8" rowspan="2" class="mc ba2 tc">' + (o.pharmacy || '') + '</td>'
            + '<td colspan="10" class="mc ba2">TELEPHONE #</td><td colspan="3" class="mc ba2">PAGE</td></tr>';

        var footer2 = '<tr class="dayRow" style="font-weight:bold"><td class="ml ba2">' + (o.resident_name || '') + '</td>'
            + '<td class="mc ba2">' + (o.sex || '') + '</td><td colspan="5" class="mc ba2">' + (o.room_number || '') + '</td>'
            + '<td colspan="5" class="mc ba2">' + (dob || '') + '</td>'
            + '<td colspan="10" class="mc ba2">' + (o.telephone || '') + '</td><td colspan="3" class="mc ba2">#</td></tr>';

        html.push('<div style="padding:5px;"><table class="mcal">');
        html.push(header);
        html.push(headerLabel);

        var headerRow = createHeaderDayRow(month, year);
        html.push(headerRow.dowRow);
        html.push(headerRow.dayRow);

        //create medication rows
        var meds = o.daily_medication;

        if (meds && meds.length > 0) {
            html.push(createMedRow(meds, month, year));
        }

        html.push(footer1);
        html.push(footer2);
        html.push('</table></div>');
        $e.html(html.join(''));

        //kim comment out this below code since it not needed for hovering text 06/14/2022
        //$('[tps != ""]').on('mouseover', function (event) {
        //    $(this).qtip({
        //        prerender: false,
        //        overwrite: true,
        //        position: {
        //            target: 'mouse',

        //        },
        //        show: {                   
        //            event: event.type,
        //            ready: true,                    
        //        },
        //        hide: {                                    
        //            event: 'mouseout',              
        //        },
        //        content: {
        //          attr: 'tps' // Tell qTip2 to look inside this attr for its content
        //        },     
        //        events: {
        //            hidden: function (event, api) {
        //                // Destroy it immediately
        //                api.destroy(true);
        //            }                  
        //        }
        //    }, event);
        //});
        //$('[data-tooltip!=""]').qtip({ // Grab all elements with a non-blank data-tooltip attr.


        //    show: {
        //        event: 'click',
        //        solo: true
        //    },
        //    hide: {
        //        delay: 200,
        //        fixed: true, // <--- add this
        //        effect: function () { $(this).fadeOut(250); }
        //    },
        //    content: {

        //        attr: 'data-tooltip' // Tell qTip2 to look inside this attr for its content
        //    },

        //    position: {
        //        viewport: $('#PCAL')
        //    }
        //$('[data-tooltip!=""]').qtip({ // Grab all elements with a non-blank data-tooltip attr.


        //    content: {
        //        attr: 'data-tooltip' // Tell qTip2 to look inside this attr for its content
        //    }
        //    style: {
        //        classes: "qtip-blue",
        //        tip: {
        //            corner: 'left center'
        //        }
        //    },
        //    position: {
        //        viewport: $('#PCAL')
        //    }
        //      })

        //kim end comment out
        /*
        var targetElement = null;
        var targetNewRow = [];

        var createPopupMenus = function () {
            if ($("body:not(:has(#presCalMenu1))")[0]) {
                var menuOptionsHtml = "<li><a href=\"#\" class=\"add_time\">Add new time</a></li>" +
				"<li class=\"separator\"></li>" +
				"<li><a href=\"#\" class=\"undo\">Undo new time</a></li>";
                $("body").append("<div id=\"presCalMenu1\" class=\"popupMenu\"><ul>" + menuOptionsHtml + "</ul></div>");
            }
            $(".popupMenu").off();
            $(".popupMenu").on("click", "a", function (e) {
                var $a = $(this);
                if ($a.is(".add_time")) {
                    var target = targetElement;
                    if (target) {
                        var t = $(target);
                        var prt = t.parent();
                        if (prt.is('tr')) {
                            if (prt.attr('class') == undefined || (prt.hasClass('dayRow') || prt.hasClass('dowRow'))) return true;
                        } else return true;

                        if (prt.is('td')) {
                            target = prt[0];
                            t = $(target);
                        }
                        if (target && target.tagName.toUpperCase() == 'TD') {
                            var trp = t.parent('tr');
                            var tr = trp;
                            var tclass = '.residentMedicationDosageRow';
                            if (!tr.is(tclass)) {
                                tr = trp.prevUntil('.dayRow').filter(tclass + ':eq(0)');
                                if (tr.length == 0)
                                    tr = trp.nextUntil('.dayRow').filter(tclass + ':eq(0)');
                            }
                            if (tr.length > 0) {
                                var td = tr.find('td.ml.ba');
                                if (td.length <= 0) {
                                    var dr = tr.nextAll(tclass);
                                    if (dr.length > 0) {
                                        td = dr.find('td.ml.ba');
                                        tr = dr;
                                    } else {
                                        dr = tr.prevAll(tclass);
                                        if (dr.length > 0) {
                                            td = dr.find('td.ml.ba');
                                            tr = dr;
                                        }
                                    }
                                }

                                if (td.length <= 0) return true;

                                var rowspan = td.attr('rowspan');
                                td.attr('rowspan', parseInt(rowspan) + 1);
                                var newRow = $(createNewRow(month, year, td.attr('rdid')));
                                targetNewRow.push(newRow);
                                newRow.insertAfter(tr.nextUntil('.dayRow').last());
                                bindEditor(newRow);
                                $(".popupMenu").hide();
                                targetElement = null;
                                return false;
                            }
                        }
                    }
                } else if ($a.is('.undo')) {
                    if (targetNewRow.length > 0) {
                        var trp = targetNewRow.pop();
                        var tr = trp;
                        var tclass = '.residentMedicationDosageRow';
                        if (!tr.is(tclass)) {
                            tr = trp.prevUntil('.dayRow').filter(tclass + ':eq(0)');
                            if (tr.length == 0)
                                tr = trp.nextUntil('.dayRow').filter(tclass + ':eq(0)');
                        }
                        if (tr.length > 0) {
                            var td = tr.find('td.ml.ba');
                            if (td.length <= 0) {
                                var dr = tr.nextAll(tclass);
                                if (dr.length > 0) {
                                    td = dr.find('td.ml.ba');
                                    tr = dr;
                                } else {
                                    dr = tr.prevAll(tclass);
                                    if (dr.length > 0) {
                                        td = dr.find('td.ml.ba');
                                        tr = dr;
                                    }
                                }
                            }

                            if (td.length <= 0) return true;

                            var rowspan = td.attr('rowspan');
                            td.attr('rowspan', parseInt(rowspan) - 1);
                            trp.remove();
                            $(".popupMenu").hide();
                            targetElement = null;
                            return false;
                        }
                    }
                }
            });
        }
        createPopupMenus();
        $e.off('contextmenu');
        $e.bind("contextmenu", function (e) {
            var target = e.srcElement || e.target;
            if (target) {
                var t = $(target);
                var prt = t.parent();
                if (prt.is('tr')) {
                    if (prt.attr('class') == undefined || (prt.hasClass('dayRow') || prt.hasClass('dowRow'))) return true;
                } else return true;
                targetElement = target;
                var menu = $("#presCalMenu1");
                if (targetNewRow.length > 0)
                    menu.find('a.undo, li.separator').show();
                else
                    menu.find('a.undo, li.separator').hide();
                menu.css({ top: e.pageY, left: e.pageX }).slideDown(300);
                return false;
            }
            return true;
        });

        bindEditor($e);
        */


    }
}

$(document).ready(function () {
    $('#PCAL').height($('#mainContent').height() - 55);
    if (typeof (DML) == 'undefined') {
        DML = {
            resize: function () {
                setTimeout(function () {
                    $('#PCAL').height($('#mainContent').height() - 55);
                }, 500);
            }
        }
    }

    var pc = new PrescriptionCalendar('PCAL');

    $('#btnReload').click(function () {
        var month = $('#monthfilter').val();
        var year = $('#yrfilter').val();
        var dateSpan = moment(year + '-' + month + '-' + 1).format('YYYY-MM-DD');
        pullData(dateSpan, function (data) {
            pc.build(data, month, year);
        });
        //pc.build(data, month, year);
    });

    function pullData(datespan, cb) {
        //debugger

        //var selId = $('#grid').jqGrid('getGridParam', 'selrow');

        var selId = $("#current_selid").val();

        var data = { func: "resident_medication", param: selId, param2: datespan };
        console.log("DATA");
        console.log(data);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                console.log(m);
                if (cb) cb(m);
            }
        });
    }

    var minDate = new Date();
    var uptoYr = parseInt(moment(new Date()).format('YYYY'));
    for (var i = 2000; i <= uptoYr; i++) {
        $('#yrfilter').append($("<option/>", {
            value: i,
            text: i
        }));
    };

    $('#monthfilter').val(minDate.getMonth() + 1);
    $('#yrfilter').val(minDate.getFullYear());
    var month = $('#monthfilter').val();
    var year = $('#yrfilter').val();
    var dateSpan = moment(year + '-' + month + '-' + 1).format('YYYY-MM-DD');
    pullData(dateSpan, function (data) {
        pc.build(data, month, year);
    });
    //pc.build(data, month, year);

});