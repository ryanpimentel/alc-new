﻿var fLastRow;
$(document).ready(function () {

    $("#txtExpirationDate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    $("#txtDateFilled").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    $("#txtDateStarted").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");

    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }
    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Add / Modify Record",
        dialogClass: "companiesDialog",
        open: function () {
            if (!$("#residentForm").data("layoutContainer")) {
                $("#residentForm").layout({
                    center: {
                        paneSelector: ".tabPanels",
                        closable: false,
                        slidable: false,
                        resizable: true,
                        spacing_open: 0
                    },
                    north: {
                        paneSelector: ".tabs",
                        closable: false,
                        slidable: false,
                        resizable: false,
                        spacing_open: 0,
                        size: 27
                    }
                });
            }
            $("#residentForm").layout().resizeAll();
            if (!$("#residentForm").data("tabs")) {
                $("#residentForm").tabs();
            }
            $("#residentForm").tabs("option", "selected", 0);
        }
    });

    $(function () {
        $("#txtReligiousAdvisorTelephone,#txtResponsiblePersonTelephone,#txtNearestRelativeTelephone").mask("(999)999-9999");
        $("#residentForm").tabs();
        $("#txtBirthdate, #txtDateLeft,#txtDateAdmitted").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0"
        });//("option", "dateFormat", "mm/dd/yy");
    });

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "resident", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('.form').extractJson();
        }
        //validate Tab Grids
        var fGridData = $.grep($('#fGrid').jqGrid('getGridParam', 'data'), function (n, i) {
            return (n.E == true) || (n.D == 'true');//post only marked with E=edited and D=deleted rows
        });
        var oGridData = $.grep($('#oGrid').jqGrid('getGridParam', 'data'), function (n, i) {
            return (n.E == true) || (n.D == 'true');
        });

        var reqObj = null;
        $.each(fGridData, function (i) {
            if (this.Name == '' && this.D != 'true') {
                isValid = false;
                reqObj = this;
                return;
            }
        });
        if (!isValid) {
            $("#residentForm").tabs("option", "selected", 1);
            alert('Name field is required.')
            $('#fGrid').jqGrid('editRow', reqObj.RId);

            return;
        }

        $.each(oGridData, function (i) {
            if (this.Name == '' && this.D != 'true') {
                isValid = false;
                reqObj = this;
                return;
            }
        });
        if (!isValid) {
            $("#residentForm").tabs("option", "selected", 2);
            alert('Name field is required.')
            $('#oGrid').jqGrid('editRow', reqObj.RId);
            return;
        }

        var data = { func: "resident", mode: mode, data: row };
        data.data.fGrid = fGridData;
        data.data.oGrid = oGridData;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var createTabGrids = function (mode) {
        var lastRow = null;
        if ($('#fGrid')[0] && $('#fGrid')[0].grid)
            $.jgrid.gridUnload('fGrid');
        var FGrid = $('#fGrid');

        FGrid.jqGrid({
            datatype: 'local',
            //data: [{ RId: '1', Name: 'Harold Brunner', Address: '123 AB St. 2nd Ave Los Amigos Mexico', Telephone: '(234)123233', E: false }],
            colNames: ['RId', 'Name', 'Address', 'Telephone', 'E', 'D'],
            colModel: [
              { key: true, hidden: true, name: 'RId', index: 'RId' },
              { key: false, name: 'Name', index: 'Name', width: 200, editable: true, edittype: 'text' },
              { key: false, name: 'Address', index: 'Address', width: 300, editable: true, edittype: 'text' },
              {
                  key: false, name: 'Telephone', index: 'Telephone', width: 100, editable: true, edittype: 'custom',
                  editoptions: {
                      custom_element: function (value, options) {
                          var el = document.createElement("input");
                          el.type = "text";
                          el.value = value;
                          $(el).mask("(999)999-9999");
                          return el;
                      },
                      custom_value: function (elem, operation, value) {
                          if (operation === 'get') {
                              return $(elem).val();
                          } else if (operation === 'set') {
                              $('input', elem).val(value);
                          }
                      }
                  }
              },
              { key: false, hidden: true, name: 'E', index: 'E', edittype: 'text' },
              { key: false, hidden: true, name: 'D', index: 'D', edittype: 'text' }],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [],
            pgbuttons: false,
            pgtext: null,
            height: '100%',
            //cellEdit: true,
            viewrecords: false,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            editurl: 'clientArray',
            cellsubmit: 'clientArray',
            beforeSelectRow: function (rowid, e) {
                if (FGrid.attr('lastRow') != 'undefined') {
                    lastRow = FGrid.attr('lastRow');
                }

                if (lastRow != rowid) {
                    FGrid.jqGrid('saveRow', lastRow);
                    var row = FGrid.jqGrid('getRowData', lastRow);
                    row.E = true;
                    FGrid.jqGrid('setRowData', lastRow, row);
                }

                var attr = $('#fGrid tr[id=' + rowid + ']').attr('editable');
                if (typeof attr === typeof undefined || attr == 0)
                    FGrid.jqGrid('editRow', rowid);

                FGrid.attr('lastRow', rowid);
            }
        });

        if ($('#oGrid')[0] && $('#oGrid')[0].grid)
            $.jgrid.gridUnload('oGrid');
        var oGrid = $('#oGrid');

        oGrid.jqGrid({
            datatype: 'local',
            //data: [{ RId: '1', Name: 'Shanon Stoney', Address: '422 John St. 2nd Ave. West Harbor', Telephone: '(234)856867', E:false }],
            colNames: ['RId', 'Name', 'Address', 'Telephone', 'E', 'D'],
            colModel: [
              { key: true, hidden: true, name: 'RId', index: 'RId' },
              { key: false, name: 'Name', index: 'Name', width: 200, editable: true, edittype: 'text' },
              { key: false, name: 'Address', index: 'Address', width: 300, editable: true, edittype: 'text' },
              {
                  key: false, name: 'Telephone', index: 'Telephone', width: 100, editable: true, edittype: 'custom',
                  editoptions: {
                      custom_element: function (value, options) {
                          var el = document.createElement("input");
                          el.type = "text";
                          el.value = value;
                          $(el).mask("(999)999-9999");
                          return el;
                      },
                      custom_value: function (elem, operation, value) {
                          if (operation === 'get') {
                              return $(elem).val();
                          } else if (operation === 'set') {
                              $('input', elem).val(value);
                          }
                      }
                  }
              },
              { key: false, hidden: true, name: 'E', index: 'E' },
              { key: false, hidden: true, name: 'D', index: 'D' }],
            pager: jQuery('#pager'),
            rowList: [],
            pgbuttons: false,
            pgtext: null,
            rowNum: 1000000,
            //rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: false,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            editurl: 'clientArray',
            cellsubmit: 'clientArray',
            beforeSelectRow: function (rowid, e) {
                if (oGrid.attr('lastRow') != 'undefined') {
                    lastRow = oGrid.attr('lastRow');
                }
                if (lastRow != rowid) {
                    oGrid.jqGrid('saveRow', lastRow);
                    var row = $('#oGrid').jqGrid('getRowData', lastRow);
                    row.E = true;
                    oGrid.jqGrid('setRowData', lastRow, row);
                }
                var attr = $('#oGrid tr[id=' + rowid + ']').attr('editable');
                if (typeof attr === typeof undefined || attr == 0)
                    oGrid.jqGrid('editRow', rowid);

                oGrid.attr('lastRow', rowid);
            }
        });

    }



    //var SearchTextMedicine = "Search Medication...";
    //$("#txtSupplyName").val(SearchTextMedicine).blur(function (e) {
    //    if ($(this).val().length == 0)
    //        $(this).val(SearchTextMedicine);
    //}).focus(function (e) {
    //    if ($(this).val() == SearchTextMedicine)
    //        $(this).val("");
    //}).autocomplete({
    //    source: function (request, response) {
    //        $.ajax({
    //            url: "Admin/Search",
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "json",
    //            type: "POST",
    //            data: '{func: "search_medicine", name:"' + request.term + '"}',
    //            success: function (data) {
    //                response($.map(data, function (item) {
    //                    return {
    //                        label: item.name,
    //                        value: item.id,
    //                        generic: item.generic
    //                    }
    //                }));
    //            }
    //        });
    //    },
    //    delay: 200,
    //    minLength: 2,
    //    focus: function (event, ui) {
    //        $("#txtSupplyName").val(ui.item.label);
    //        $("#txtGeneric").val(ui.item.generic);
    //        $('#txtSupplyId').val(ui.item.value);
    //        return false;
    //    },
    //    change: function (event, ui) {
    //        var dis = $(this);
    //        if (dis.val() == '' || dis.val() == "Search Medicine name...") {
    //            $("#addResident").hide();
    //            return false;
    //        }
    //        $("#addResident").toggle(!ui.item);
    //    },
    //    select: function (event, ui) {
    //        return false;
    //    }
    //}).data("ui-autocomplete")._renderItem = function (ul, item) {
    //    return $("<li></li>")
    //            .data("item.autocomplete", item)
    //            .append("<a>" + item.label + "</a>")
    //            .appendTo(ul);
    //};



    //var SearchTextResident = "Search resident name...";
    //$("#txtResidentName").val(SearchTextResident).blur(function (e) {
    //    if ($(this).val().length == 0)
    //        $(this).val(SearchTextResident);
    //}).focus(function (e) {
    //    if ($(this).val() == SearchTextResident)
    //        $(this).val("");
    //}).autocomplete({
    //    source: function (request, response) {
    //        $.ajax({
    //            url: "Admin/Search",
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "json",
    //            type: "POST",
    //            data: '{func: "search_admission", name:"' + request.term + '"}',
    //            success: function (data) {
    //                response($.map(data, function (item) {
    //                    return {
    //                        label: item.name,
    //                        value: item.admissionid,
    //                        admissiondate: item.admissiondate,
    //                        residentid: item.residentid,
    //                        physician: item.physician
    //                    }
    //                }));
    //            }
    //        });
    //    },
    //    delay: 200,
    //    minLength: 2,
    //    focus: function (event, ui) {
    //        $("#txtResidentName").val(ui.item.label);
    //        $('#txtAdmissionId').val(ui.item.value);
    //        $('#txtAdmissionDate').val(ui.item.admissiondate);
    //        $('#txtResidentId').val(ui.item.residentid);
    //        $('#txtAttendingPhysician').val(ui.item.physician);
    //        refreshGrid();
    //        return false;
    //    },
    //    change: function (event, ui) {
    //        var dis = $(this);

    //        if (dis.val() == '' || dis.val() == "Search resident name...") {
    //            $("#addResident").hide();
    //            $('#txtAdmissionId').val('');
    //            $('#txtResidentId').val('');
    //            $('#txtAdmissionDate').val('');
    //            $('#txtAttendingPhysician').val('');
    //            return false;
    //        }

    //        $("#addResident").toggle(!ui.item);
    //    },
    //    select: function (event, ui) {
    //        return false;
    //    }
    //}).data("ui-autocomplete")._renderItem = function (ul, item) {
    //    return $("<li></li>")
    //            .data("item.autocomplete", item)
    //            .append("<a>" + item.label + "<br/>" + "Admission Date:" + item.admissiondate + "</a>")
    //            .appendTo(ul);
    //};


    var showdiag = function (mode) {
        createTabGrids();
        $("#diag").dialog("option", {
            width: 500,
            height: $(window).height() - 200,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var isValid = $('.form').validate();
                        //do other modes
                        var a = parseFloat($("#txtQty").val());

                        if (isNaN(a) == true) {
                            alert('Please Enter Proper Qty...');
                            return;
                        }

                        row = $('.form').extractJson();


                        var data = { func: "destruction", mode: mode, data: row };
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Admin/PostGridData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (cb) cb(m);
                            }
                        });
                        alert('Saved!');
                        $("#diag").dialog("close");
                        refreshGrid();
                        refreshGrid2();
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }


    $('#tabs-2,#tabs-3').click(function (e) {
        var G = '#fGrid';
        if (this.id == 'tabs-3') G = '#oGrid';
        var trgt = $(e.target);
        //debugger;
        if (trgt.attr('role') == 'gridcell' || trgt.is('.add') || trgt[0].id.indexOf('jqg') == 0 || trgt.hasClass('editable') || trgt.hasClass('customelement')) return true;
        var editRow = $(G + ' tr[editable=1]');
        if (editRow.length > 0) {
            editRow.each(function (i) {
                $(G).jqGrid('saveRow', this.id);
                var row = $(G).jqGrid('getRowData', this.id);
                row.E = true;
                $(G).jqGrid('setRowData', this.id, row);
            });
        }
    });


    $('#receivemedsGridToolbar').on('click', 'a', function () {
        if ($("#txtAdmissionId").val() == '') {
            alert('Please Select an Admitted Resident..');
            return;
        }
        $("#txtAdmissionIdAdd").val($("#txtAdmissionId").val());
        $("#txtResidentIdAdd").val($("#txtResidentId").val());
        $("#txtResidentNameAdd").val($("#txtResidentName").val());
        showdiag(1);
    });


    $('#financialGridToolbar,#otherGridToolbar').on('click', 'a', function () {
        var g = $(this).parents('div.toolbar')[0].id == 'financialGridToolbar' ? '#fGrid' : '#oGrid';
        var q = $(this);
        var G = $(g);
        if (q.is('.add')) {
            var editRow = $(g + ' tr[editable=1]');
            if (editRow.length > 0) {
                editRow.each(function (i) {
                    G.jqGrid('saveRow', this.id);
                    var row = $(G).jqGrid('getRowData', this.id);
                    row.E = true;
                    $(G).jqGrid('setRowData', this.id, row);
                });
            }
            G.jqGrid('addRow', "new");
            G.attr('lastRow', G.jqGrid('getGridParam', 'selrow'));
        } else if (q.is('.del')) {
            var id = G.jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = G.jqGrid('getRowData', id);
                        if (row) {
                            row.D = 'true';
                            $(G).jqGrid('setRowData', id, row);
                            $(g + ' tr[id=' + id + ']').addClass("not-editable-row").hide();

                        }
                    }, 100);
                }

            } else {
                alert("Please select row to delete.")
            }
        }
    });


    $('#btnGenerate').on('click', function () {
        refreshGrid();
    });


    var refreshGrid = function () {
        var url = "Admin/GetGridData?func=destruction&param=" + $("#txtAdmissionId").val();
        jQuery("#grid").jqGrid('setGridParam',
       {
           url: url
       }).trigger("reloadGrid");
    }

    var refreshGrid2 = function () {
        var url = "Admin/GetGridData?func=destructionhistory";
        jQuery("#grid2").jqGrid('setGridParam',
       {
           url: url
       }).trigger("reloadGrid");
    }

    $(function () {

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=destruction&param=" + $("#txtAdmissionId").val(),
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'SupplyId', 'Generic', 'DestructionDetailId', 'Resident', 'Medication Name', 'Qty', 'Date Filled', 'Disposal Date', 'Prescription Number', 'Pharmacy'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
               { key: false, hidden: true, name: 'SupplyId', index: 'SupplyId' },
               { key: false, hidden: true, name: 'Generic', index: 'Generic' },
                { key: false, hidden: true, name: 'DestructionDetailId', index: 'DestructionDetailId' },
              { key: false, name: 'Resident', index: 'Resident' },
              {
                  key: false, name: 'Supply', index: 'Supply', formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('.form').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax(0, row, function (res) {
                                  $(".form").mapJson(res);
                                  $('#txtDestructionDetailId').val(row.DestructionDetailId);
                                  $('#txtSupplyName').val(row.Supply);
                                  $('#txtResidentNameAdd').val(row.Resident);
                                  $('#txtSupplyId').val(row.SupplyId);
                                  $('#txtGeneric').val(row.Generic);
                                  $('#txtQty').val(row.Qty);

                                  $('#txtDateFilled').val(row.DateFilled);
                                  $('#txtDisposalDate').val(row.DisposalDate);

                                  $('#txtPrescriptionNumber').val(row.PrescriptionNumber);

                                  $('#txtPharmacy').val(row.Pharmacy);

                                  showdiag(2);
                                  $('#fGrid').jqGrid('setGridParam', { data: res.FGrid }).trigger('reloadGrid');
                                  $('#oGrid').jqGrid('setGridParam', { data: res.OGrid }).trigger('reloadGrid');

                              });
                          }
                      }
                  }
              },
              { key: false, name: 'Qty', index: 'Qty' },

              {
                  key: false, name: 'DateFilled', index: 'DateFilled', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' }

              },
              {
                  key: false, name: 'DisposalDate', index: 'DisposalDate', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' }

              },

              { key: false, name: 'PrescriptionNumber', index: 'PrescriptionNumber', },

              { key: false, name: 'Pharmacy', index: 'Pharmacy', },
            ],
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false
        });
    });


    $(function () {
        $('#grid2').jqGrid({
            url: "Admin/GetGridData?func=destructionhistory",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Resident', 'ResidentId', 'AdmissionDate', 'AttendingPhysician', 'SupplyId', 'Generic', 'DestructionDetailId', 'Medication Name', 'Qty', 'Date Filled', 'Disposal Date', 'Prescription Number', 'Pharmacy'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              { key: false, name: 'Resident', index: 'Resident' },
              { key: false, hidden: true, name: 'ResidentId', index: 'ResidentId' },
               { key: false, hidden: true, name: 'AdmissionDate', index: 'AdmissionDate', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' } },
                { key: false, hidden: true, name: 'AttendingPhysician', index: 'AttendingPhysician' },
               { key: false, hidden: true, name: 'SupplyId', index: 'SupplyId' },
               { key: false, hidden: true, name: 'Generic', index: 'Generic' },
                { key: false, hidden: true, name: 'DestructionDetailId', index: 'DestructionDetailId' },
              {
                  key: false, name: 'Supply', index: 'Supply'
                  //, formatter: "dynamicLink", formatoptions: {
                  //    onClick: function (rowid, iRow, iCol, cellText, e) {
                  //        var G = $('#grid2');
                  //        G.setSelection(rowid, false);
                  //        $('.form').clearFields();
                  //        var row = G.jqGrid('getRowData', rowid);
                  //        if (row) {
                  //            doAjax(0, row, function (res) {
                  //                $('#txtAdmissionId').val(row.Id);
                  //                $('#txtResidentName').val(row.Resident);
                  //                $('#txtResidentId').val(row.ResidentId);
                  //                $('#txtAdmissionDate').val(row.AdmissionDate);
                  //                $('#txtAttendingPhysician').val(row.AttendingPhysician);


                  //                $('#fGrid').jqGrid('setGridParam', { data: res.FGrid }).trigger('reloadGrid');
                  //                $('#oGrid').jqGrid('setGridParam', { data: res.OGrid }).trigger('reloadGrid');
                  //                setTimeout(function () { refreshGrid(); }, 200);
                  //                $("#residentForm").tabs("option", "selected", 0);
                  //            });
                  //        }
                  //    }
                  // }
              },
              { key: false, name: 'Qty', index: 'Qty' },

              {
                  key: false, name: 'DateFilled', index: 'DateFilled', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' }

              },
              {
                  key: false, name: 'DisposalDate', index: 'DisposalDate', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' }

              },

              { key: false, name: 'PrescriptionNumber', index: 'PrescriptionNumber', },

              { key: false, name: 'Pharmacy', index: 'Pharmacy', },
            ],
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false
        });

    });
});