﻿var _prescriptionId;
var _admissionId;
var _residentId;
$(document).ready(function () {
    $("#txtPrescriptionDate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    $("#txtPRNDate").datepicker({
        minDate: 0,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    PHL = $("#residentMedsContent");
    PHL.layout({
        north: {
            paneSelector: "#headerResidentMeds",
            size: '40%',
            minSize: '20%'
        },
        center: {
            paneSelector: "#middleResidentMeds",
            size: '30%',
            minSize: '20%',
            maxSize: '40%'
        },
        south: {
            paneSelector: "#bottomResidentMeds",
            size: '30%',
            minSize: '20%',
            maxSize: '40%'
        },
        autoResize: true,
        resizerDragOpacity: .6,
        livePanelResizing: false,
        closable: false,
        slidable: false,
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#headerResidentMeds").height() - 25).width($("#headerResidentMeds").width());
            $('.ui-jqgrid-bdiv').eq(1).height($("#middleResidentMeds").height() - 55).width($('#middleResidentMeds').width());
            $('.ui-jqgrid-bdiv').eq(2).height($("#bottomResidentMeds").height() - 55).width($('#bottomResidentMeds').width());
        }
        setTimeout(f, 100);
        return f;
    })();


    //COMMENT RYAN
    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Receive Medication",
        dialogClass: "physicianDialog",
        open: function () {
        }
    });


    $("#diag2").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Prescription",
        dialogClass: "physicianDialog",
        open: function () {
        }
    });

    //$('#txtHourIntake').timepicker();
    //$('#txtHourIntake').focus(function () { $(this).select(); });
    //$('#txtHourIntake').mask('99:99');
    //$('#txtHourIntake').blur(function (e) {
    //    var dis = $(this);
    //    var sp = dis.val().split(':');
    //    if (sp.length == 1) {
    //        if (sp[0] > 23) dis.val('00:00');
    //    } else if (sp.length > 1) {
    //        if (sp[0] > 23) dis.val(('00' + ':' + sp[1]));
    //        if (sp[1] > 59) dis.val((sp[0] + ':' + '00'));
    //        if (sp[1] > 59 && sp[0] > 23) dis.val('00:00');
    //    }

    //});
    //comment RYAN


    var SearchTextMedicine = "Search Medicine name...";
    $("#txtSupplyName").val(SearchTextMedicine).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextMedicine);
    }).focus(function (e) {
        if ($(this).val() == SearchTextMedicine)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_medicine", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id,
                            generic: item.generic
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {
            $("#txtSupplyName").val(ui.item.label);
            $("#txtGeneric").val(ui.item.generic);
            $('#txtSupplyId').val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search Medicine name...") {
                $("#addResident").hide();
                return false;
            }
            $("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "resident", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        var isValid = $('.formMeds').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('.formMeds').extractJson();
        }
        //validate Tab Grids
        var fGridData = $.grep($('#fGrid').jqGrid('getGridParam', 'data'), function (n, i) {
            return (n.E == true) || (n.D == 'true');//post only marked with E=edited and D=deleted rows
        });
        var oGridData = $.grep($('#oGrid').jqGrid('getGridParam', 'data'), function (n, i) {
            return (n.E == true) || (n.D == 'true');
        });

        var reqObj = null;
        $.each(fGridData, function (i) {
            if (this.Name == '' && this.D != 'true') {
                isValid = false;
                reqObj = this;
                return;
            }
        });
        if (!isValid) {
            $("#residentForm").tabs("option", "selected", 1);
            alert('Name field is required.')
            $('#fGrid').jqGrid('editRow', reqObj.RId);

            return;
        }

        $.each(oGridData, function (i) {
            if (this.Name == '' && this.D != 'true') {
                isValid = false;
                reqObj = this;
                return;
            }
        });
        if (!isValid) {
            $("#residentForm").tabs("option", "selected", 2);
            alert('Name field is required.')
            $('#oGrid').jqGrid('editRow', reqObj.RId);
            return;
        }

        var data = { func: "resident", mode: mode, data: row };
        data.data.fGrid = fGridData;
        data.data.oGrid = oGridData;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiagMeds = function (mode) {
        $("#diag").dialog("option", {
            width: 800,
            height: 290,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {

                        //alert('kkk');
                        //var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        //row = $('#grid').jqGrid('getRowData', id);

                        //doAjax(mode, row, function (m) {
                        //if (m.result == 0) {
                        //alert('Saving');
                        var stat = $('#cbIsPRN').is(':checked') ? '1' : null;
                        var prndate = $('#txtPRNDate').val();
                        if (stat == 1 && prndate == '') {
                            $.growlWarning({ message: "Please enter PRN Date if this medication type is PRN.", delay: 6000 });
                            return;
                        }
                        if ($('#txtSupplyId').val() == '') {
                            $.growlWarning({ message: "Please enter valid supply", delay: 6000 });
                            return;
                        }
                        if (prndate == '') prndate = null;
                        var data = {
                            func: "receivemeds", mode: mode, Id: (mode == 1 ? 0 : $('#txtId').val()),
                            prescriptionid: _prescriptionId,
                            supplyid: $('#txtSupplyId').val(),
                            qty: $('#txtQty').val(),
                            dosage: $('#txtDosage').val(),
                            isprn: stat,
                            prndate: prndate
                        };
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Admin/PostGridData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (m.result == 0) {
                                    $.growlSuccess({ message: "Received Med saved successfully", delay: 6000 });
                                }
                                setTimeout(function () { $('#mGrid').trigger('reloadGrid'); }, 200);
                                $("#diag").dialog("close");
                            }
                        });

                        ////$.growlSuccess({ message: "Resident Medication " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                        //$("#diag").dialog("close");

                        //debugger;
                        //$.growlSuccess({ message: "Received Med saved successfully", delay: 6000 });

                        //} else {
                        //    $.growlError({ message: m.message, delay: 6000 });
                        //}
                        //});
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }


    var showdiagPres = function (mode) {
        $("#diag2").dialog("option", {
            width: 500,
            height: 290,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        //debugger;
                        var data = {
                            func: "receivemedsprescription", mode: ($('#txtId').val() == "") ? 1 : 2, PrescriptionNo: $('#txtPrescriptionNumber').val(), PrescriptionDate: $('#txtPrescriptionDate').val(),
                            Physician: $('#txtPhysician').val(), Pharmacy: $('#txtPharmacy').val(), AdmissionId: _admissionId,
                            ResidentId: _residentId, Id: $('#txtId').val()
                        };
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Admin/PostGridData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (m.result == 0) {
                                    $.growlSuccess({ message: "Prescription saved successfully", delay: 6000 });
                                }
                                setTimeout(function () { $('#pGrid').trigger('reloadGrid'); }, 200);
                                $("#diag2").dialog("close");
                            }
                        });

                        //$("#diag2").dialog("close");
                        //setTimeout(function () { $('#pGrid').trigger('reloadGrid'); }, 200);
                        //var msg = "Prescription saved successfully".
                        //$.growlSuccess({ message: msg, delay: 6000 });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag2").dialog("close");
                }
            }
        }).dialog("open");
    }



    $('#medsGridToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            //COMMENT RYAN
            $('.formMeds').clearFields();
            if (_prescriptionId != null) {
                showdiagMeds(1);
            }
        } else if (q.is('.del')) {
            //var id = $('#grid').jqGrid('getGridParam', 'selrow');
            //if (id) {
            //    if (confirm('Are you sure you want to delete the selected row?')) {
            //        setTimeout(function () {
            //            var row = $('#grid').jqGrid('getRowData', id);
            //            if (row) {
            //                doAjax(3, { Id: row.Id }, function (m) {
            //                    var msg = row.Medicine + " deleted successfully!";
            //                    if (m.result == -3) //inuse
            //                        msg = row.Medicine + " cannot be deleted because it is currently in use.";
            //                    $.growlSuccess({ message: msg, delay: 6000 });
            //                    //if no error or not in use then reload grid
            //                    if (m.result != -1 || m.result != -3)
            //                        $('#grid').trigger('reloadGrid');
            //                });
            //            }
            //        }, 100);
            //    }
            //} else {
            //    alert("Please select row to delete.")
            //}
        }
        //else if (q.is('.unsign')) {
        //    var id = $('#grid').jqGrid('getGridParam', 'selrow');
        //    if (id) {
        //        var row = $('#grid').jqGrid('getRowData', id);
        //        if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
        //            setTimeout(function () {
        //                if (row) {
        //                    doAjax(4, { Id: row.Id }, function (m) {
        //                        if (m.result == 0)
        //                            $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
        //                    });
        //                }
        //            }, 100);
        //        }
        //    } else {
        //        alert("Please select row to deactivate.")
        //    }
        //}
    });


    $('#prescGridToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            //COMMENT RYAN
            if (_residentId) {
                $('.formPresc').clearFields();
                showdiagPres(1);
            }
        }
        if (q.is('.edit')) {
            //COMMENT RYAN
            $('.formPresc').clearFields();
            var id = $('#pGrid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#pGrid').jqGrid('getRowData', id);
                $('#txtId').val(row.Id);
                $('#txtPrescriptionNumber').val(row.PrescriptionNumber);
                $('#txtPrescriptionDate').val(row.PrescriptionDate);
                $('#txtPhysician').val(row.AttendingPhysician);
                $('#txtPharmacy').val(row.Pharmacy);
                showdiagPres(1);
            }
        } else if (q.is('.del')) {
            //var id = $('#grid').jqGrid('getGridParam', 'selrow');
            //if (id) {
            //    if (confirm('Are you sure you want to delete the selected row?')) {
            //        setTimeout(function () {
            //            var row = $('#grid').jqGrid('getRowData', id);
            //            if (row) {
            //                doAjax(3, { Id: row.Id }, function (m) {
            //                    var msg = row.Medicine + " deleted successfully!";
            //                    if (m.result == -3) //inuse
            //                        msg = row.Medicine + " cannot be deleted because it is currently in use.";
            //                    $.growlSuccess({ message: msg, delay: 6000 });
            //                    //if no error or not in use then reload grid
            //                    if (m.result != -1 || m.result != -3)
            //                        $('#grid').trigger('reloadGrid');
            //                });
            //            }
            //        }, 100);
            //    }
            //} else {
            //    alert("Please select row to delete.")
            //}
        }
        //else if (q.is('.unsign')) {
        //    var id = $('#grid').jqGrid('getGridParam', 'selrow');
        //    if (id) {
        //        var row = $('#grid').jqGrid('getRowData', id);
        //        if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
        //            setTimeout(function () {
        //                if (row) {
        //                    doAjax(4, { Id: row.Id }, function (m) {
        //                        if (m.result == 0)
        //                            $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
        //                    });
        //                }
        //            }, 100);
        //        }
        //    } else {
        //        alert("Please select row to deactivate.")
        //    }
        //}
    });



    $(function () {
        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=resident_admission_list&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['AdmissionId', 'ResidentId', 'Firstname', 'MiddleInitial', 'Lastname', 'Gender', 'SSN', 'Birthdate'],
            colModel: [
              { key: true, name: 'AdmissionId', index: 'AdmissionId', hidden: true },
              { key: false, name: 'ResidentId', index: 'ResidentId', hidden: true },
              { key: false, name: 'Firstname', index: 'Firstname' },
              { key: false, name: 'MiddleInitial', index: 'MiddleInitial' },
              { key: false, name: 'Lastname', index: 'Lastname' },
              { key: false, name: 'Gender', index: 'Gender', },
              { key: false, name: 'SSN', index: 'SSN', },
              { key: false, name: 'Birthdate', index: 'Birthdate', }],
            onSelectRow: function (id) {
                setTimeout(function () { _prescriptionId = null; }, 100);
                setTimeout(function () { _admissionId = id; }, 100);
                var rowData = jQuery(this).getRowData(id);
                setTimeout(function () {
                    _residentId = rowData['ResidentId'];
                    onFocusResidentPrescriptionGrid(id);
                }, 100);

                //onFocusResidentMedicationGrid(null);
            },
            gridComplete: function (data) {
                var id = $("tr:eq(1)", "tbody:first", "#grid").attr('id');
                $("#grid").setSelection(id);
            },
            //pager: jQuery('#pager'),
            scroll: true,
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'AdmissionId',
            sortorder: 'asc',
            loadonce: true
        });

        function onFocusResidentPrescriptionGrid(admissionid) {
            if ($('#pGrid')[0] && $('#pGrid')[0].grid)
                $.jgrid.gridUnload('pGrid');

            $('#pGrid').jqGrid({
                url: "Admin/GetGridData?func=receivemedsprescription&param=" + admissionid,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['Id', 'Resident', 'Prescription Number', 'Prescription Date', 'Pharmacy', 'Physician'],
                colModel: [
                  { key: true, hidden: true, name: 'Id', index: 'Id' },
                  {
                      key: false, name: 'Resident', index: 'Resident', editable: true
                  },
                  {
                      key: false, name: 'PrescriptionNumber', index: 'PrescriptionNumber', editable: true
                  },
                  { key: false, name: 'PrescriptionDate', index: 'PrescriptionDate', editable: true, formatter: 'date', formatoptions: { srcformat: 'm/d/Y', newformat: 'm/d/Y' } }
                  ,
                  { key: false, name: 'Pharmacy', index: 'Pharmacy', editable: true }
                  ,
                { key: false, name: 'AttendingPhysician', index: 'AttendingPhysician', editable: true }
                ],
                onSelectRow: function (id) {
                    setTimeout(function () { _prescriptionId = id; }, 100);
                    onFocusResidentMedicationGrid(id);
                },
                //pager: jQuery('#pager'),
                gridComplete: function (data) {
                    var id = $("#pGrid tr:eq(1)").attr('id');
                    if (!id) {
                        if ($("#pGrid").getGridParam("records") == 0)
                            $('#pPager').html('No results found.');
                        else
                            $('#pPager').html('');
                        return;
                    } else
                        $('#pPager').html('');
                    $("#pGrid").setSelection(id);
                },
                loadComplete: function (data) {

                    //if ($("#pGrid").getGridParam("records") == 0)
                    //    $('#pPager').html('No results found.');
                    //else
                    //    $('#pPager').html('');
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false,
                sortname: 'Id',
                sortorder: 'asc',
                loadonce: true
            });
        }

        //$(function() {
        //    if ($('#mGrid')[0] && $('#mGrid')[0].grid)
        //        $.jgrid.gridUnload('mGrid');

        //    $('#mGrid').jqGrid({
        //        url: "Admin/GetGridData?func=receivemeddetail&param=" + _prescriptionId,
        //        datatype: 'json',
        //        postData: "",
        //        ajaxGridOptions: { contentType: "application/json", cache: false },
        //        //datatype: 'local',
        //        //data: dataArray,
        //        //mtype: 'Get',
        //        colNames: ['Id', 'Description', 'Generic', 'UOM', 'Qty', 'Dosage'],
        //        colModel: [
        //          { key: true, hidden: true, name: 'Id', index: 'Id' },
        //          {
        //              key: false, name: 'Description', index: 'Name', formatter: "dynamicLink", formatoptions: {
        //                  onClick: function (rowid, iRow, iCol, cellText, e) {
        //                      var G = $('#mGrid');
        //                      G.setSelection(rowid, false);
        //                      //COMMENT RYAN
        //                      $('.formMeds').clearFields();
        //                      var row = G.jqGrid('getRowData', rowid);
        //                      if (row) {
        //                          //COMMENT RYAN
        //                          $('.formMeds').mapJson(row);
        //                          showdiagMeds(2);
        //                      }
        //                  }
        //              }
        //          },
        //          { key: false, name: 'Generic', index: 'Generic' },
        //          { key: false, name: 'UOM', index: 'UOM' },
        //          { key: false, name: 'Qty', index: 'Qty', formatter: 'currency', formatoptions: { decimalSeparator: ".", defaultValue: '0.00' } },
        //          { key: false, name: 'Dosage', index: 'Dosage' }],
        //        //pager: jQuery('#pager'),
        //        rowNum: 1000000,
        //        rowList: [10, 20, 30, 40],
        //        height: '100%',
        //        viewrecords: true,
        //        //caption: 'Care Staff',
        //        emptyrecords: 'No records to display',
        //        jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
        //        autowidth: true,
        //        multiselect: false
        //    });

        //}
        //)

        function onFocusResidentMedicationGrid(prescriptionid) {
            if ($('#mGrid')[0] && $('#mGrid')[0].grid)
                $.jgrid.gridUnload('mGrid');

            $('#mGrid').jqGrid({
                url: "Admin/GetGridData?func=receivemeddetail&param=" + prescriptionid,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['Id', 'SupplyId', 'Description', 'Generic', 'Qty', 'Frequency', 'Dosage', 'Receive Date', 'Is PRN', 'PRN Date'],
                colModel: [
                  { key: true, hidden: true, name: 'Id', index: 'Id' },
                  { key: false, hidden: true, name: 'SupplyId', index: 'SupplyId' },
                  {
                      key: false, name: 'SupplyName', index: 'Name', formatter: "dynamicLink", formatoptions: {
                          onClick: function (rowid, iRow, iCol, cellText, e) {
                              var G = $('#mGrid');
                              G.setSelection(rowid, false);
                              //COMMENT RYAN
                              $('.formMeds').clearFields();
                              var row = G.jqGrid('getRowData', rowid);
                              if (row) {
                                  //COMMENT RYAN
                                  $('.formMeds').mapJson(row);

                                  if ($('#cbIsPRN').is(':checked'))
                                      $('#txtPRNDate').removeAttr('disabled');
                                  else
                                      $('#txtPRNDate').val('').attr('disabled', 'disabled');
                                  showdiagMeds(2);

                              }
                          }
                      }, editable: true
                  },
                  { key: false, name: 'Generic', index: 'Generic', editable: true },
                  { key: false, name: 'Qty', index: 'Qty', formatter: 'currency', formatoptions: { decimalSeparator: ".", defaultValue: '0.00' }, editable: true },
                  { key: false, name: 'Freq', index: 'Freq', editable: true },
                  { key: false, hidden: true, name: 'Dosage', index: 'Dosage', editable: true },
                  { key: false, name: 'ReceiveDate', index: 'ReceiveDate', editable: true },
                  { key: false, hidden: true, name: 'IsPRN', index: 'IsPRN', editable: true },
                  { key: false, name: 'PRNDate', index: 'PRNDate', editable: true },
                ],
                //pager: jQuery('#pager'),
                gridComplete: function (data) {
                    var id = $("#mGrid tr:eq(1)").attr('id');
                    if (!id) {
                        if ($("#mGrid").getGridParam("records") == 0)
                            $('#mPager').html('No results found.');
                        else
                            $('#mPager').html('');
                        return;
                    } else
                        $('#mPager').html('');
                },
                loadComplete: function (data) {

                    //if ($("#mGrid").getGridParam("records") == 0)
                    //    $('#mPager').html('No results found.');
                    //else
                    //    $('#pPager').html('');
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                scroll: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false,
                sortname: 'Id',
                sortorder: 'asc',
                loadonce: true
            });
        }

    });

    $("#cbIsPRN").click(function (x) {
        if ($(this).is(':checked'))
            $('#txtPRNDate').removeAttr('disabled');
        else
            $('#txtPRNDate').val('').attr('disabled', 'disabled');
    });

});