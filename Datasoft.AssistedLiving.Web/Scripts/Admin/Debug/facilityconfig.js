﻿var _FacilityId = 0;
$(document).ready(function () {

    PHL = $("#facilityConfigContent");
    PHL.layout({
        center: {
            paneSelector: "#headerfacilityConfig",
            size: '50%',
            minSize: '30%',
            maxSize: '50%'
        },
        south: {
            paneSelector: "#middlefacilityConfig",
            size: '50%',
            minSize: '50%',
            maxSize: '50%'
        },
        south__autoResize: true,
        center__autoResize: true,
        resizerDragOpacity: .6,
        livePanelResizing: false,
        closable: false,
        slidable: false,
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#headerfacilityConfig").height() - 25).width($("#headerfacilityConfig").width());
            $('.ui-jqgrid-bdiv').eq(1).height($("#middlefacilityConfig").height() - 25).width($('#middlefacilityConfig').width());
            $('#grid').setGridWidth($("#headerfacilityConfig").width(), true);
            $('#cGrid').setGridWidth($("#middlefacilityConfig").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();


    var doAjax = function (rows, func, cb) {
        var data = { func: func, data: rows, id: _FacilityId, stype: $('#roleSelect').val() };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }
    var midGridEditable = false;

    $('#configGridToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.edit')) {
            midGridEditable = true;
            q.hide().parents('ul').find('a.save,a.cancel').show();
            $('#headerfacilityConfig').block({ message: null });
            $('#roleSelect').prop('disabled', true);
        } else if (q.is('.cancel') || q.is('.save')) {
            midGridEditable = false;
            q.parents('ul').find('a.save,a.cancel').hide().parents('ul').find('a.edit').show();
            $('#headerfacilityConfig').unblock();
            $('#roleSelect').prop('disabled', false);

            if (q.is('.save')) {
                //do save
                var changes = [];
                var rows = jQuery("#cGrid").getDataIDs();
                for (a = 0; a < rows.length; a++) {
                    var data = $("#cGrid").jqGrid('getRowData', rows[a]);
                    if (!data) return;
                    var x = $(data.CheckVal).find('input[type=checkbox]')[0].id;
                    var y = $('#' + x);
                    if (y[0].checked)
                        changes.push({ source_type: $('#roleSelect').val(), source_id: data['Id'], facilityId: _FacilityId });
                }
                doAjax(changes, "facilityconfig", function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({ message: "Configurations saved successfully!", delay: 6000 });
                        //reload grid
                        var selRowId = $("#grid").jqGrid('getGridParam', 'selrow');
                        $("#grid").setSelection(selRowId);
                    }
                });
            }
            else {
                var selRowId = $("#grid").jqGrid('getGridParam', 'selrow');
                $("#grid").setSelection(selRowId);
            }
        }
    });

    $('#roleSelect').change(function () {
        onFocusfacilityGrid(_FacilityId);
    });
    if ($('#grid')[0] && $('#grid')[0].grid)
        $.jgrid.gridUnload('grid');

    var GURow = new Grid_Util.Row();

    $('#grid').jqGrid({
        url: "Admin/GetGridData?func=facility&param=",
        datatype: 'json',
        postData: "",
        ajaxGridOptions: { contentType: "application/json", cache: false },
        //datatype: 'local',
        //data: dataArray,
        //mtype: 'Get',
        colNames: ['Id', 'Facility', 'Description', 'Level', 'Rate'],
        colModel: [
          { key: true, hidden: true, name: 'Id', index: 'Id' },
          { key: false, name: 'Name', index: 'Name' },
          { key: false, name: 'Description', index: 'Description' },
          { key: false, name: 'Level', index: 'Level' },
          { key: false, name: 'Rate', index: 'Rate', formatter: 'currency', formatoptions: { decimalSeparator: ".", prefix: "$", defaultValue: '0.00' } }],
        //pager: jQuery('#pager'),
        rowNum: 1000000,
        sortname: 'Id',
        sortorder: 'asc',
        loadonce: true,
        onSortCol: function () {
            fromSort = true;
            GURow.saveSelection.call(this);
        },
        onSelectRow: function (id) {
            _FacilityId = id;
            //var rowData = jQuery(this).getRowData(id);
            onFocusfacilityGrid(_FacilityId);
        },
        loadComplete: function (data) {
            if (typeof fromSort != 'undefined') {
                GURow.restoreSelection.call(this);
                delete fromSort;
                return;
            }

            var maxId = data && data[0] ? data[0].Id : 0;
            if (typeof isReloaded != 'undefined') {
                $.each(data, function (k, d) {
                    if (d.Id > maxId) maxId = d.Id;
                });
            }
            if (maxId > 0)
                $("#grid").setSelection(maxId);
            delete isReloaded;
        },
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        emptyrecords: 'No records to display',
        jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
        autowidth: true,
        multiselect: false
    });
    $("#grid").jqGrid('bindKeys');
    function onFocusfacilityGrid(facilityid) {
        if ($('#cGrid')[0] && $('#cGrid')[0].grid)
            $.jgrid.gridUnload('cGrid');
        var roleid = $('#roleSelect').val();
        var _url = "Admin/GetGridData?func=facilityconfig&param=" + facilityid + "&param2=" + roleid;
        if (roleid == "rooms") {
            $('#cGrid').jqGrid({
                url: _url,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['Id', 'Room', 'Level', 'Room Type', 'Room Rate', 'Is Catered By Facility'],
                colModel: [
                    { key: true, hidden: true, name: 'Id', index: 'Id' },
                    { key: false, name: 'Name', index: 'Name' },
                    { key: false, name: 'RoomType', index: 'RoomType', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Suite', '2': 'Residence' } } },
                    { key: false, name: 'Level', index: 'Level' },
                    { key: false, name: 'Rate', index: 'Rate', formatter: 'currency', formatoptions: { decimalSeparator: ".", prefix: "$", defaultValue: '0.00' } },
                    {
                        key: false, name: 'CheckVal', index: 'CheckVal', align: 'left', formatter: function checkbox(cellValue, option) {
                            var cv = cellValue;
                            var sel = ' checked="checked"';
                            return '<center><input type="checkbox" value="1" id="checkbox_' + option.rowId + '" ' + (cv == 1 ? sel : '') + '  /></center>';
                        }
                    }],
                //pager: jQuery('#pager'),
                loadComplete: function (data) {
                    $('#configGrid input[type=checkbox]').click(function (e) {
                        if (!midGridEditable) return false;
                        var tr = $(e.target).closest('tr');
                        if (tr[0]) {
                            var rowId = tr[0].id;
                            var $grid = $("#cGrid");
                            $grid.setSelection(rowId);

                            $("#" + $.jgrid.jqID(rowId)).addClass("edited");
                            $grid.jqGrid("setCell", rowId, "CheckVal", "", "dirty-cell");
                        }
                    });
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false
            });
        }
        else if (roleid == "services") {
            $('#cGrid').jqGrid({
                url: _url,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['Id', 'Activity', 'Procedure', 'Department', 'Created By', 'Date Created', 'Is Offered By Facility'],
                colModel: [
                  { key: true, hidden: true, name: 'Id', index: 'Id' },
                  { key: false, name: 'Activity', index: 'Activity' },
                  { key: false, name: 'Proc', index: 'Proc' },
                  { key: false, name: 'Department', index: 'Department' },
                  { key: false, name: 'CreatedBy', index: 'CreatedBy' },
                  { key: false, name: 'DateCreated', index: 'DateCreated' },
                   {
                       key: false, name: 'CheckVal', index: 'CheckVal', align: 'left', formatter: function checkbox(cellValue, option) {
                           var cv = cellValue;
                           var sel = ' checked="checked"';
                           return '<center><input type="checkbox" value="1" id="checkbox_' + option.rowId + '" ' + (cv == 1 ? sel : '') + '  /></center>';
                       }
                   }
                ],
                //pager: jQuery('#pager'),
                loadComplete: function (data) {

                    $('#configGrid input[type=checkbox]').click(function (e) {

                        if (!midGridEditable) return false;
                        var tr = $(e.target).closest('tr');
                        if (tr[0]) {
                            var rowId = tr[0].id;
                            var $grid = $("#cGrid");
                            $grid.setSelection(rowId);

                            $("#" + $.jgrid.jqID(rowId)).addClass("edited");
                            $grid.jqGrid("setCell", rowId, "CheckVal", "", "dirty-cell");
                        }
                    });
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false
            });
        }
        else if (roleid == "shifts") {

            $('#cGrid').jqGrid({
                url: _url,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['Id', 'Description', 'Start Time', 'End Time', 'Created By', 'Date Created', 'Is Observed By Facility'],
                colModel: [
                  { key: true, hidden: true, name: 'Id', index: 'Id' },
                  { key: false, name: 'Description', index: 'Description' },
                  { key: false, name: 'StartTime', index: 'StartTime' },
                  { key: false, name: 'EndTime', index: 'EndTime' },
                  { key: false, name: 'CreatedBy', index: 'CreatedBy' },
                  { key: false, name: 'DateCreated', index: 'DateCreated' },
                   {
                       key: false, name: 'CheckVal', index: 'CheckVal', align: 'left', formatter: function checkbox(cellValue, option) {
                           var cv = cellValue;
                           var sel = ' checked="checked"';
                           return '<center><input type="checkbox" value="1" id="checkbox_' + option.rowId + '" ' + (cv == 1 ? sel : '') + '  /></center>';
                       }
                   }
                ],
                //pager: jQuery('#pager'),
                loadComplete: function (data) {

                    $('#configGrid input[type=checkbox]').click(function (e) {

                        if (!midGridEditable) return false;
                        var tr = $(e.target).closest('tr');
                        if (tr[0]) {
                            var rowId = tr[0].id;
                            var $grid = $("#cGrid");
                            $grid.setSelection(rowId);

                            $("#" + $.jgrid.jqID(rowId)).addClass("edited");
                            $grid.jqGrid("setCell", rowId, "CheckVal", "", "dirty-cell");
                        }
                    });
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false
            });
        }
    }

});