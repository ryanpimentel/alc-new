﻿$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }

    PHL = $("#roleContent");
    PHL.layout({
        center: {
            paneSelector: "#headerRole",
            size: '60%',
            minSize: '30%',
            maxSize: '50%',
            north: {
                paneSelector: "#wrapToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            , north__childOptions: {
                paneSelector: "#roleGridToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
                center__childOptions: {
                    paneSelector: "#raToolbar",
                    closable: false,
                    slidable: false,
                    resizable: false,
                    spacing_open: 0
                }
            },
        },
        south: {
            paneSelector: "#middleRole",
            size: '40%',
            minSize: '20%',
            maxSize: '50%'
        },
        south__autoResize: true,
        center__autoResize: true,
        resizerDragOpacity: .6,
        livePanelResizing: false,
        closable: false,
        slidable: false,
        onresize: function () {
            resizeGrids();
        }

    });


    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#headerRole").height() - 90).width($("#headerRole").width());
            $('.ui-jqgrid-bdiv').eq(1).height($("#middleRole").height() - 25).width($('#middleRole').width());
            $('#grid').setGridWidth($("#roleGrid").width(), true);
            $('#urGrid').setGridWidth($("#userRoleGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });
    $('#ftrText,#isActive').change(function () {
        var res = $('#ftrText').val();
        //var rm = $('#ddlFilter').val();
        //var rd = $('#ddlFilter :selected').text
        var ia = $('#isActive').is(':checked');
        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'Description';

        if (ia)
            ftrs.rules.push({ field: 'SysDefined', op: 'eq', data: ia });

        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Role",
        dialogClass: "physicianDialog",
        open: function () {
        }
    });

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "department", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('.form').extractJson();
        }

        var data = { func: "department", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: 420,
            height: 120,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "Department " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diag").dialog("close");
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                }
                                $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                            } else {
                                $.growlError({ message: m.message, delay: 6000 });
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('.form').clearFields();
            showdiag(1);
        } else if (q.is('.del')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            if (row.SysDefined) {
                                $.growlWarning({ message: 'Sorry, a system defined department cannot be deleted.', delay: 4000 });
                                return;
                            }
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Description + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = row.Description + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });

                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3)
                                    $('#departments_1 a').trigger('click');
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.unsign')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to de-activate "' + row.Description + '"?')) {
                    setTimeout(function () {
                        if (row) {
                            doAjax(4, { Id: row.Id }, function (m) {
                                if (m.result == 0)
                                    $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to deactivate.")
            }
        }
    });


    $(function () {
        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=department&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Name', 'Is System Defined', 'Date Created', 'Date Updated'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Description', index: 'Description', formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('.form').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax(0, row, function (res) {
                                  $('.form').mapJson(res);
                                  showdiag(2);
                              });
                          }
                      }
                  }
              },
              { key: false, name: 'SysDefined', index: 'SysDefined', editable: true, edittype: "select", formatter: 'select', editoptions: { value: { true: 'True', false: 'False' } } },
              { key: false, name: 'DateCreated', index: 'DateCreated', editable: true },
              { key: false, name: 'DateModified', index: 'DateModified', editable: true }],
            onSelectRow: function (id) {
                onFocusRoleGrid(id);
            },
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;
            },
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true
        });
        $("#grid").jqGrid('bindKeys');

        function onFocusRoleGrid(roleid) {
            if ($('#urGrid')[0] && $('#urGrid')[0].grid)
                $.jgrid.gridUnload('urGrid');

            var GURow2 = new Grid_Util.Row();

            $('#urGrid').jqGrid({
                url: "Admin/GetGridData?func=department_user&param=" + roleid,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['Id', 'Name', 'Email', 'Department', 'Is Active', 'Date Created', 'Date Updated'],
                colModel: [
                  { key: true, name: 'Id', index: 'Id' },
                  {
                      key: false, name: 'Name', index: 'Name', formatoptions: {
                          cellValue: function (val, rowid, row, options) {
                              var name = [];
                              if (row.Firstname != "")
                                  name.push(row.Firstname + " ");
                              if (row.MiddleInitial != "")
                                  name.push(row.MiddleInitial + " ");
                              if (row.Lastname != "")
                                  name.push(row.Lastname + " ");
                              return name.join('');
                          }
                      }
                  },
                  { key: false, name: 'Email', index: 'Email', editable: true },
                  { key: false, name: 'RoleDescription', index: 'RoleDescription', editable: true },
                  { key: false, name: 'IsActive', index: 'IsActive', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Active', '0': 'Inactive' } } },
                  { key: false, name: 'DateCreated', index: 'DateCreated', editable: true },
                  { key: false, name: 'DateModified', index: 'DateModified', editable: true }],
                //pager: jQuery('#pager'),
                onSortCol: function () {
                    fromSort2 = true;
                    GURow2.saveSelection.call(this);
                },
                loadComplete: function (data) {
                    if ($("#urGrid").getGridParam("records") == 0)
                        $('#urPager').html('No results found.');
                    else
                        $('#urPager').html('');

                    if (typeof fromSort2 != 'undefined') {
                        GURow2.restoreSelection.call(this);
                        delete fromSort2;
                        return;
                    }

                    var maxId = data && data[0] ? data[0].Id : 0;
                    if (typeof isReloaded != 'undefined') {
                        $.each(data, function (k, d) {
                            if (d.Id > maxId) maxId = d.Id;
                        });
                    }
                    if (maxId > 0)
                        $("#urGrid").setSelection(maxId);
                    delete isReloaded;
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false,
                sortname: 'Id',
                sortorder: 'asc',
                loadonce: true
            });
            $("#urGrid").jqGrid('bindKeys');
        }
    });

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        var myGrid = $('#grid'),
         selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
         iValue = myGrid.jqGrid('getInd', selRowId);

        $("td:hidden,th:hidden", "#urGrid").remove();
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Is System Defined</td><td bgcolor='#87AFC6'> Date Created</td><td bgcolor='#87AFC6'>Date Updated</td></tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('urGrid'); // id of table
        tab1 = document.getElementById('grid');
        tab_text = tab_text + tab1.rows[iValue].innerHTML + "</tr><tr></tr><tr><td colspan='7'>Department Users: </td></tr><tr><td bgcolor='#87AFC6'>Id</td><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Email</td><td bgcolor='#87AFC6'>Department</td><td bgcolor='#87AFC6'>Is Active</td><td bgcolor='#87AFC6'>Date Created</td><td bgcolor='#87AFC6'>Date Updated</td></tr><tr>";

        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";

            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Departments.xls");
        }
        else                 //other browser not tested on IE 11
            //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));            
            var data_type = 'data:application/vnd.ms-excel';
        a.href = data_type + ', ' + encodeURIComponent(tab_text);
        a.download = 'ALC_Departments' + '.xls';
        a.click();

        //return (sa);
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();

    });
    //============================

});