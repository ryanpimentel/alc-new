﻿var Calendar = (function () {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var cellClasses = { 1: "typeCont", 2: "typeGen", 3: "typeInp", 4: "typeRout" };
    var icons = { "551": "snp.png", "421": "ptp.png", "431": "otp.png", "441": "stp.png", "561": "swp.png", "571": "hhap.png" };
    var settings = { startDate: null, endDate: null, admissions: [] };
    var selectedDate = "";

    var init = function (options) {
        if (options.startDate) settings.startDate = new Date(options.startDate);
        if (options.endDate) settings.endDate = new Date(options.endDate);
        if (options.admissions) {
            settings.admissions = [];
            $.each(options.admissions, function (i, item) {
                var loc = {
                    carestaff_admission_id: item.carestaff_admission_id, carestaff_id: item.carestaff_id, name: item.name, admission_date: new Date(item.admission_date), dow: item.dow
                };
                settings.admissions.push(loc);
            });
        }
    }

    var limit = function (val, max, step) {
        return val < max ? val - step : max;
    };

    var renderHeaderHtml = function () {
        var html = '';//"<td>Week</td>";
        for (var i = 0; i < days.length; i++) {
            html += "<td>" + days[i] + "</td>";
        }

        return "<thead><tr>" + html + "</tr><thead>";
    };

    var getCellDesc = function (d) {
        var html = "";
        $.each(settings.admissions, function (i, item) {
            if (d >= item.admission_date && item.dow.indexOf(d.getDay()) != -1) {
                html += "<a admissionid=\"" + item.carestaff_admission_id + "\" id=\"" + item.carestaff_id + "\" href=\"#\" class=\"visit_icon\">" + item.name + "</a>";
            }
        });

        return html;
    }

    var renderCellHtml = function (w, d) {
        var html = "<td><table";
        if (d != null) html += " id=\"" + (d.getMonth() + 1) + "_" + d.getDate() + "_" + d.getFullYear() + "\"";
        html += " class=\"planBlock";
        if (d != null) html += " day";
        html += "\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td class=\"date\">";
        //if (w != null) html += w;
        if (d != null) html += months[d.getMonth()] + " " + d.getDate();
        html += "</td></tr><tr><td class=\"planned\"><div>";
        if (d != null) html += getCellDesc(d);
        if (w != null) html += "";
        html += "</div></table></td>";//</td></tr><tr><td class=\"actual\"><div>";
        //html += "</div></td></tr><tr><td class=\"actual\"><div>";
        //if (d != null) html += getCellIcons(d, true);
        //if (w != null) html += "Actual";
        //html += "</div></td></tr></table></td>";

        return html;
    }

    var renderBodyHtml = function () {
        var html = "<tr>";
        for (var i = 0; i < settings.startDate.getDay() ; i++) {
            //if (i == 0) html += renderCellHtml(1);
            html += renderCellHtml();
        }

        var day = settings.startDate;
        var week = 1;
        for (; day <= settings.endDate;) {
            //if (day.getDay() == 0) html += renderCellHtml(week);
            html += renderCellHtml(null, day);
            if (day.getDay() == 6) {
                html += "</tr><tr>";
                week += 1;
            }
            day.setDate(day.getDate() + 1);
        }

        for (var i = settings.endDate.getDay() ; i < 6; i++) {
            html += renderCellHtml();
        }
        html += "</tr>"

        return "<tbody>" + html + "</tbody>";
    }

    var renderTableHtml = function () {
        var html = "<table class=\"clearFloats03\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
        html += renderHeaderHtml();
        html += renderBodyHtml();
        html += "</table>";

        return html;
    }

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "carestaff_admission", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }


        var resName = $('#txtResidentName');
        if (resName.val() == "Search resident name...")
            resName.val('');

        resName = $('#txtCarestaff');
        if (resName.val() == "Search carestaff name...")
            resName.val('');

        var isValid = $('.form').validate();

        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                if ($('#txtResidentId').val().length == 0) {
                    $('#txtResidentName').val("Search resident name...");
                }
                if ($('#txtCarestaffId').val().length == 0) {
                    $('#txtCarestaff').val("Search carestaff name...");
                }
                return;
            }

            //validate time
            var ti = moment($('#txtTimeIn').val(), 'HH:mm');
            var to = moment($('#txtTimeOut').val(), 'HH:mm');

            if (ti > to) {
                alert('Time In should be less than the Time Out period');
                $('#txtTimeIn').focus();
                return;
            }


            var dow = [];
            $('.dayofweek').find('input[type="checkbox"]').each(function () {
                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun': dow.push('0'); break;
                        case 'Mon': dow.push('1'); break;
                        case 'Tue': dow.push('2'); break;
                        case 'Wed': dow.push('3'); break;
                        case 'Thu': dow.push('4'); break;
                        case 'Fri': dow.push('5'); break;
                        case 'Sat': dow.push('6'); break;
                    }
                }
            });
            if (dow.length == 0) {
                alert('Please choose day of week schedule.');
                return;
            }

            row = $('.form').extractJson();

            row.DayOfWeek = dow.join(',');
            row.TimeIn = ti.format('MM/DD/YY HH:mm');
            row.TimeOut = to.format('MM/DD/YY HH:mm');
        }
        var data = { func: "carestaff_admission", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $('#room_facility').val($('#filterExp').val());
        $("#diag").dialog("option", {
            width: 550,
            height: 470,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var row = {};
                        doAjax(mode, row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({ message: "Admission " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                            $("#diag").dialog("close");
                            $('#grid').trigger('reloadGrid');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
        $('#txtCarestaff').focus();
    }

    var createPopupMenus = function () {
        if ($("body:not(:has(#calendarMenu1))")[0]) {
            var menuOptionsHtml = "<li><a href=\"#\" class=\"visit\">Post Carestaff Visit</a></li>";// +
            //"<li class=\"separator\"></li>" +
            //"<li><a href=\"#\" class=\"template\">Menu 1</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 2</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 3</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 4</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 5</a>";
            $("body").append("<div id=\"calendarMenu1\" class=\"popupMenu\"><ul>" + menuOptionsHtml + "</ul></div>");
        }
        //if ($("body:not(:has(#calendarMenu2))")[0]) {
        //    var menuOptionsHtml = "<li><a href=\"#\" class=\"supply\">Supply</a></li>" +
        //        "<li><a href=\"#\" class=\"wound\">Wound Note</a></li>" +
        //        "<li class=\"separator\"></li>" +
        //        "<li><a href=\"#\" class=\"levelOfCare\">Level of Care</a></li>" +
        //        "<li><a href=\"#\" class=\"order\">Physician Order</a></li>" +
        //        "<li><a href=\"#\" class=\"communicationNote\">Communication Note</a></li>";
        //    $("body").append("<div id=\"calendarMenu2\" class=\"popupMenu\"><ul>" + menuOptionsHtml + "</ul></div>");
        //}
        $(".popupMenu").on("click", "a", function (e) {
            var $window = $(window);
            var $a = $(this);
            if ($a.is(".visit")) {
                $('.form').clearFields();
                $('#txtPlanDate').val(selectedDate.replace(/_/g, '/'));
                showdiag(1);
            }
            else {
                var $dialog = $("<div><iframe frameborder=\"0\" class=\"frame\"></iframe></div>").dialog({
                    modal: true,
                    title: $a.text(),
                    width: $window.width() - 100,
                    height: $window.height() - 100,
                    close: function () {
                        $dialog.dialog("destroy").remove();
                    }
                });
                //$dialog.find(".frame").attr("src", url);
            }
            $(".popupMenu").hide();

            return false;
        });
    }

    return {
        Build: function (options) {
            init(options);
            if (settings.startDate == null && settings.endDate == null) return;
            $("#calendar").html(renderTableHtml());
            $("#calendar").on("click", ".planned, .actual", function (e) {
                var $a = $(this);
                var $d = $a.closest(".day");
                if ($d[0]) {
                    selectedDate = $d.attr("id");
                    if ($a.is(".planned")) {
                        $("#calendarMenu1").css({ top: e.pageY, left: e.pageX }).slideDown(300);
                        $("#calendarMenu2").hide();
                    }
                    else if ($a.is(".actual")) {
                        $("#calendarMenu2").css({ top: e.pageY, left: e.pageX }).slideDown(300);
                        $("#calendarMenu1").hide();
                    }
                }
            });
            $("#calendar").on("click", ".visit_icon", function (e) {
                var row = { Id: $(this).attr('admissionid') };
                doAjax(0, row, function (res) {
                    $(".form").clearFields();
                    $(".form").mapJson(res);
                    //map day of week
                    if (res.DayOfWeek != null && res.DayOfWeek != '') {
                        var setCheck = function (el, str, idx) {
                            if (str.indexOf(idx) != -1)
                                el.prop('checked', true);
                        }
                        var dow = res.DayOfWeek;
                        $('.dayofweek').find('input[type="checkbox"]').each(function () {
                            var d = $(this);
                            switch (d.attr('name')) {
                                case 'Sun': setCheck(d, dow, '0'); break;
                                case 'Mon': setCheck(d, dow, '1'); break;
                                case 'Tue': setCheck(d, dow, '2'); break;
                                case 'Wed': setCheck(d, dow, '3'); break;
                                case 'Thu': setCheck(d, dow, '4'); break;
                                case 'Fri': setCheck(d, dow, '5'); break;
                                case 'Sat': setCheck(d, dow, '6'); break;
                            }
                        });
                    }
                    showdiag(2);
                });
                e.preventDefault();
                return false;
            });
            $("a.visit_icon").each(function () {
                $(this).qtip({
                    content: {
                        text: 'Loading...',
                        ajax: {
                            url: "Admin/GetFormData",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            type: "POST",
                            data: JSON.stringify({ func: "carestaff_admission", id: $(this).attr('admissionid') }),
                            success: function (data, status) {
                                if (data) {
                                    var dw = data.DayOfWeek.split(',');
                                    var dow = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                                    var sow = [];
                                    for (var i = 0; i < dw.length; ++i) {
                                        sow.push(dow[dw[i]]);
                                    }
                                    var htm = [];
                                    htm.push('<span class="labeltip">Assigned Resident:</span><span class="valuetip">' + data.ResidentName + '</span><br/>');
                                    htm.push('<span class="labeltip">Facility/Room:</span><span class="valuetip">' + data.RoomFacility + '</span><br/>');
                                    htm.push('<span class="labeltip">Day of week:</span><span class="valuetip">' + sow.join(',') + '</span><br/>');
                                    this.set('content.text', htm.join(''));
                                }
                            }
                        }
                    },
                    style: {
                        classes: "qtip-blue",
                        tip: {
                            corner: 'left center'
                        }
                    },
                    position: {
                        viewport: $('#calendar')
                    }
                });
            });
            createPopupMenus();
        }
    }
})();

$(document).ready(function () {
    $('#txtTimeIn,#txtTimeOut').timepicker();
    $("#txtPlanDate,#txtEndDate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    var minDate = new Date();

    for (var i = 2000; i < 2050; i++) {
        $('#yrfilter').append($("<option/>", {
            value: i,
            text: i
        }));
    };

    $('#monthfilter').val(minDate.getMonth() + 1);
    $('#yrfilter').val(minDate.getFullYear());

    $("#calendarLayoutContainer").layout({
        center: {
            paneSelector: "#calendar",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#raToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
        }
    });
    function buildCal(date) {
        var carestaffId = $('#filterExp').val();
        var data = { startDate: date.start, endDate: date.end, carestaffId: carestaffId };
        $.ajax({
            url: "Admin/GetCarestaffCalendar",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            success: function (response) {
                if (response) {
                    Calendar.Build({
                        startDate: data.startDate,
                        endDate: data.endDate,
                        admissions: response.Admissions
                    });
                }
            }
        });

    }

    function getMonthDateRange(year, month) {
        // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
        // array is 'year', 'month', 'day', etc
        var startDate = moment([year, month - 1]);

        // Clone the value before .endOf()
        var endDate = moment(startDate).endOf('month');

        // just for demonstration:
        //console.log(startDate.toDate());
        //console.log(endDate.toDate());

        // make sure to call toDate() for plain JavaScript date type
        return { start: startDate, end: endDate };
    }

    var buildCalendar = function () {

        var month = $('#monthfilter').val();
        var year = $('#yrfilter').val();
        minDate = getMonthDateRange(year, month);
        buildCal(minDate);
    }

    buildCalendar();
    $('#txtTimeIn').focus(function () { $(this).select(); });
    $('#btnReload').click(function () {
        buildCalendar();
    })

    $('#filterExp,#monthfilter,#yrfilter').change(function () {
        buildCalendar();
    });

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Post Carestaff Visit",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });

    $('#txtTimeIn,#txtTimeOut').mask('99:99');
    $('#txtTimeIn,#txtTimeOut').blur(function (e) {
        var dis = $(this);
        var sp = dis.val().split(':');
        if (sp.length == 1) {
            if (sp[0] > 23) dis.val('00:00');
        } else if (sp.length > 1) {
            if (sp[0] > 23) dis.val(('00' + ':' + sp[1]));
            if (sp[1] > 59) dis.val((sp[0] + ':' + '00'));
            if (sp[1] > 59 && sp[0] > 23) dis.val('00:00');
        }

    });

    var SearchText = "Search resident name...";
    $("#txtResidentName").val(SearchText).blur(function (e) {
        if ($('#txtResidentId').val().length == 0 || $(this).val().length == 0)
            $(this).val(SearchText);
    }).focus(function (e) {
        if ($(this).val() == SearchText)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_resident", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 2,
        focus: function (event, ui) {
            $("#txtResidentName").val(ui.item.label);
            $('#txtResidentId').val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item)
                $('#txtResidentId').val('');
        },
        select: function (event, ui) {
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };

    var SearchCarestaffText = "Search carestaff name...";
    $("#txtCarestaff").val(SearchCarestaffText).blur(function (e) {
        if ($('#txtCarestaffId').val().length == 0 || $(this).val().length == 0)
            $(this).val(SearchCarestaffText);
    }).focus(function (e) {
        if ($(this).val() == SearchCarestaffText)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_carestaff", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 2,
        focus: function (event, ui) {
            $("#txtCarestaff").val(ui.item.label);
            $('#txtCarestaffId').val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item)
                $('#txtCarestaffId').val('');
        },
        select: function (event, ui) {
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };


});
$(document).click(function (e) {
    if (!$(e.target).closest(".day")[0] && !$(e.target).is(".popupMenu")[0])
        $(".popupMenu").slideUp(300);
});