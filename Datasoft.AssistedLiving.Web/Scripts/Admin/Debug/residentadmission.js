﻿var Calendar = (function () {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var cellClasses = { 1: "typeCont", 2: "typeGen", 3: "typeInp", 4: "typeRout" };
    var icons = { "551": "snp.png", "421": "ptp.png", "431": "otp.png", "441": "stp.png", "561": "swp.png", "571": "hhap.png" };
    var settings = { startDate: null, endDate: null, admissions: [] };
    var selectedDate = "";

    var init = function (options) {
        if (options.startDate) settings.startDate = new Date(options.startDate);
        if (options.endDate) settings.endDate = new Date(options.endDate);
        if (options.admissions) {
            settings.admissions = [];
            $.each(options.admissions, function (i, item) {
                var loc = {
                    admission_id: item.admission_id, resident_id: item.resident_id, name: item.name, admission_date: new Date(item.admission_date)
                };
                settings.admissions.push(loc);
            });
        }
    }

    var limit = function (val, max, step) {
        return val < max ? val - step : max;
    };

    var renderHeaderHtml = function () {
        var html = '';//"<td>Week</td>";
        for (var i = 0; i < days.length; i++) {
            html += "<td>" + days[i] + "</td>";
        }

        return "<thead><tr>" + html + "</tr><thead>";
    };

    var getCellDesc = function (d) {
        var html = "";
        $.each(settings.admissions, function (i, item) {
            if (d >= item.admission_date) {
                html += "<a admissionid=\"" + item.admission_id + "\" id=\"" + item.resident_id + "\" href=\"#\" class=\"visit_icon\">" + item.name + "</a>";
            }
        });

        return html;
    }

    var renderCellHtml = function (w, d) {
        var html = "<td><table";
        if (d != null) html += " id=\"" + (d.getMonth() + 1) + "_" + d.getDate() + "_" + d.getFullYear() + "\"";
        html += " class=\"planBlock";
        if (d != null) html += " day";
        html += "\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td class=\"date\">";
        //if (w != null) html += w;
        if (d != null) html += months[d.getMonth()] + " " + d.getDate();
        html += "</td></tr><tr><td class=\"planned\"><div>";
        if (d != null) html += getCellDesc(d);
        if (w != null) html += "";
        html += "</div></table></td>";//</td></tr><tr><td class=\"actual\"><div>";
        //html += "</div></td></tr><tr><td class=\"actual\"><div>";
        //if (d != null) html += getCellIcons(d, true);
        //if (w != null) html += "Actual";
        //html += "</div></td></tr></table></td>";

        return html;
    }

    var renderBodyHtml = function () {
        var html = "<tr>";
        for (var i = 0; i < settings.startDate.getDay() ; i++) {
            //if (i == 0) html += renderCellHtml(1);
            html += renderCellHtml();
        }

        var day = settings.startDate;
        var week = 1;
        for (; day <= settings.endDate;) {
            //if (day.getDay() == 0) html += renderCellHtml(week);
            html += renderCellHtml(null, day);
            if (day.getDay() == 6) {
                html += "</tr><tr>";
                week += 1;
            }
            day.setDate(day.getDate() + 1);
        }

        for (var i = settings.endDate.getDay() ; i < 6; i++) {
            html += renderCellHtml();
        }
        html += "</tr>"

        return "<tbody>" + html + "</tbody>";
    }

    var renderTableHtml = function () {
        var html = "<table class=\"clearFloats03\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
        html += renderHeaderHtml();
        html += renderBodyHtml();
        html += "</table>";

        return html;
    }
    var selectedTabIndex = 0;
    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {

            var data = { func: "resident_admission", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        var resName = $('#txtResidentName');
        if (resName.val() == "Search resident name...")
            resName.val('');

        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                $('#txtResidentName').val("Search resident name...");
                return;
            }
            row = $('.form').extractJson('id');
        }
        var tabindex = $("#residentForm").tabs('option', 'selected');
        var data = { func: "resident_admission", mode: mode, data: row, TabIndex: tabindex };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });

    }
    var showdiag = function (en) {
        var id = $('#txtId').val();
        var mode = $.isNumeric(id) ? 2 : 1;
        selectedTabIndex = 0;
        $('#room_facility').val($('#filterExp').val());
        $("#diag").dialog("option", {
            width: $(window).width() - 150,
            height: $(window).height() - 150,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var row = {};

                        doAjax(mode, row, function (m) {
                            var tibx = $("#residentForm").tabs('option', 'selected');
                            if (m.message == "Duplicate") {
                                alert('Admission for resident already exists!');
                                return;
                            }
                            if (mode == 1) {
                                $('#txtId').val(m.result);
                                //    $.growlSuccess({ message: "Admission " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                //    $("#diag").dialog("close");
                            }

                            var tabindex = $("#residentForm").tabs('option', 'selected');
                            if (tabindex == 0) {
                                $('#DCResidentName').val($('#txtResidentName').val());
                                $('#ANSResidentName').val($('#txtResidentName').val());
                                $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                                $('#AAResidentName').val($('#txtResidentName').val());
                                $('#RMMResidentName').val($('#txtResidentName').val());
                                $('#DCRoomNumber').val($('#room_facility option:selected').text());
                                $('#AARoomNo').val($('#room_facility option:selected').text());
                                $('#RMMRoomNo').val($('#room_facility option:selected').text());
                            }
                            else if (tabindex == 2) {
                                $('#AADOB').val($('#ANSDOB').val());
                            }
                            if (selectedTabIndex > tabindex) {
                                $("#residentForm").tabs("option", "selected", selectedTabIndex);
                            }
                            else {
                                selectedTabIndex = tabindex + 1;
                                if (tabindex == 0) {

                                    $("#residentForm").tabs('option', 'disabled', [2, 3, 4]);
                                    $("#residentForm").tabs("option", "selected", 1);
                                }
                                else if (tabindex == 1) {

                                    $("#residentForm").tabs('option', 'disabled', [3, 4]);
                                    $("#residentForm").tabs("option", "selected", 2);
                                }
                                else if (tabindex == 2) {

                                    $("#residentForm").tabs('option', 'disabled', [4]);
                                    $("#residentForm").tabs("option", "selected", 3);
                                }
                                else if (tabindex == 3) {

                                    $("#residentForm").tabs('option', 'disabled', []);
                                    $("#residentForm").tabs("option", "selected", 4);
                                }
                                else if (tabindex == 4) {

                                    $.growlSuccess({ message: "Admission " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                    $("#diag").dialog("close");
                                }
                            }
                        });

                    },
                    class: "primaryBtn",
                    text: "Save"
                },

                "Print": {
                    click: function () {
                        var ts = $("#residentForm").tabs('option', 'selected');
                        //debugger;
                        if (ts == 1)
                            window.open(ALC_URI.Admin + "/PrintDocumentChecklist?id=" + $('#txtId').val(), '_blank');
                        if (ts == 2)
                            window.open(ALC_URI.Admin + "/PrintAppraisalNeedAndService?id=" + $('#txtId').val(), '_blank');
                        if (ts == 3)
                            window.open(ALC_URI.Admin + "/PrintAdmissionAgreement?id=" + $('#txtId').val(), '_blank');
                        if (ts == 4)
                            window.open(ALC_URI.Admin + "/PrintMovein?id=" + $('#txtId').val(), '_blank');
                    },
                    id: "AdmissionPrint",
                    text: "Print"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            },
            resize: function (event, ui) {
                var diag = $('.ui-dialog-content');
                diag = diag.find('#residentForm').parents('.ui-dialog-content');
                var height = 0;
                height = diag.eq(0).height() || 0;
                if (height <= 0)
                    height = diag.eq(1).height() || 0;
                $("#AdmissionContainer,#residentForm").height(height);
                $('#AdmissionContent').height(height - 20);
                $('#tabs-1,#tabs-2,#tabs-3,#tabs-4,#tabs-5').height(height - 30);
            }
        }).dialog("open");


        if (en == 0)
            $("#residentForm").tabs('option', 'disabled', []);
        $('#txtResidentName').focus();
    }

    var createPopupMenus = function () {
        if ($("body:not(:has(#calendarMenu1))")[0]) {
            var menuOptionsHtml = "<li><a href=\"#\" class=\"visit\">Plot New Admission</a></li>";// +
            //"<li class=\"separator\"></li>" +
            //"<li><a href=\"#\" class=\"template\">Menu 1</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 2</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 3</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 4</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 5</a>";
            $("body").append("<div id=\"calendarMenu1\" class=\"popupMenu\"><ul>" + menuOptionsHtml + "</ul></div>");
        }
        //if ($("body:not(:has(#calendarMenu2))")[0]) {
        //    var menuOptionsHtml = "<li><a href=\"#\" class=\"supply\">Supply</a></li>" +
        //        "<li><a href=\"#\" class=\"wound\">Wound Note</a></li>" +
        //        "<li class=\"separator\"></li>" +
        //        "<li><a href=\"#\" class=\"levelOfCare\">Level of Care</a></li>" +
        //        "<li><a href=\"#\" class=\"order\">Physician Order</a></li>" +
        //        "<li><a href=\"#\" class=\"communicationNote\">Communication Note</a></li>";
        //    $("body").append("<div id=\"calendarMenu2\" class=\"popupMenu\"><ul>" + menuOptionsHtml + "</ul></div>");
        //}
        $(".popupMenu").on("click", "a", function (e) {
            var $window = $(window);
            var $a = $(this);
            if ($a.is(".visit")) {
                $('.form').clearFields();
                $('#txtAdmissionDate').val(selectedDate.replace(/_/g, '/'));
                showdiag(1);
            }
            else {
                var $dialog = $("<div><iframe frameborder=\"0\" class=\"frame\"></iframe></div>").dialog({
                    modal: true,
                    title: $a.text(),
                    width: $window.width() - 100,
                    height: $window.height() - 100,
                    close: function () {
                        $dialog.dialog("destroy").remove();
                    }
                });
                //$dialog.find(".frame").attr("src", url);
            }
            $(".popupMenu").hide();

            return false;
        });
    }

    return {
        Build: function (options) {
            init(options);
            if (settings.startDate == null && settings.endDate == null) return;
            $("#calendar").html(renderTableHtml());
            $("#calendar").on("click", ".planned, .actual", function (e) {
                var $a = $(this);
                var $d = $a.closest(".day");
                if ($d[0]) {
                    selectedDate = $d.attr("id");
                    if ($a.is(".planned")) {
                        $("#calendarMenu1").css({ top: e.pageY, left: e.pageX }).slideDown(300);
                        $("#calendarMenu2").hide();
                    }
                    else if ($a.is(".actual")) {
                        $("#calendarMenu2").css({ top: e.pageY, left: e.pageX }).slideDown(300);
                        $("#calendarMenu1").hide();
                    }
                }
            });
            $("#calendar").on("click", ".visit_icon", function (e) {
                var row = { Id: $(this).attr('id') };
                doAjax(0, row, function (res) {
                   // debugger;
                    $(".form").clearFields();
                    //$(".form").mapJson(res);
                    $(".form").mapJson(res, 'id');
                    $("#addResident").hide();
                    showdiag(0);
                });
                e.preventDefault();
                return false;
            });
            createPopupMenus();
        }
    }
})();

$(document).ready(function () {

    var minDate = new Date();

    for (var i = 2000; i < 2050; i++) {
        $('#yrfilter').append($("<option/>", {
            value: i,
            text: i
        }));
    };

    $('#monthfilter').val(minDate.getMonth() + 1);
    $('#yrfilter').val(minDate.getFullYear());

    $("#calendarLayoutContainer").layout({
        center: {
            paneSelector: "#calendar",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#raToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
        }
    });
    function buildCal(date) {
        var facilityId = null, roomId = null;
        var fr = $('#filterExp').val();
        var frArr = fr.split('_');
        if (frArr != null && frArr.length > 0) {
            if (frArr[0].indexOf('facility') != -1)
                facilityId = frArr[1];
            else
                roomId = frArr[1];
        }
        var data = { startDate: date.start, endDate: date.end, facilityId: facilityId, roomId: roomId };
        $.ajax({
            url: "Admin/GetAdmissionCalendar",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            success: function (response) {
                if (response) {
                    Calendar.Build({
                        startDate: data.startDate,
                        endDate: data.endDate,
                        admissions: response.Admissions
                    });
                }
            }
        });

    }

    function getMonthDateRange(year, month) {
        // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
        // array is 'year', 'month', 'day', etc
        var startDate = moment([year, month - 1]);

        // Clone the value before .endOf()
        var endDate = moment(startDate).endOf('month');

        // just for demonstration:
        //console.log(startDate.toDate());
        //console.log(endDate.toDate());

        // make sure to call toDate() for plain JavaScript date type
        return { start: startDate, end: endDate };
    }

    var buildCalendar = function () {

        var month = $('#monthfilter').val();
        var year = $('#yrfilter').val();
        minDate = getMonthDateRange(year, month);
        buildCal(minDate);
    }

    buildCalendar();

    $('#btnReload').click(function () {
        buildCalendar();
    })

    $('#filterExp,#monthfilter,#yrfilter').change(function () {
        buildCalendar();
    });

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Residents",
        dialogClass: "companiesDialog",
        open: function () {

            //if (!$("#residentForm").data("layoutContainer")) {
            //    $("#residentForm").layout({
            //        center: {
            //            paneSelector: ".tabPanels",
            //            closable: false,
            //            slidable: false,
            //            resizable: true,
            //            spacing_open: 0
            //        },
            //        north: {
            //            paneSelector: ".tabs",
            //            closable: false,
            //            slidable: false,
            //            resizable: false,
            //            spacing_open: 0,
            //            size: 27
            //        }
            //    });
            //}
            //if (!$("#residentForm").data("tabs")) {
            //    $("#residentForm").tabs();
            //}

            $("#residentForm").tabs();
            $("#residentForm").tabs("option", "selected", 0);
            $("#residentForm").tabs('option', 'disabled', [1, 2, 3, 4]);
            var diag = $('.ui-dialog-content');
            diag = diag.find('#residentForm').parents('.ui-dialog-content');
            var height = 0;
            height = diag.eq(0).height() || 0;
            if (height <= 0)
                height = diag.eq(1).height() || 0;
            $("#AdmissionContainer,#residentForm").height(height);
            $('#AdmissionContent').height(height - 20);
            $('#tabs-1,#tabs-2,#tabs-3,#tabs-4,#tabs-5').height(height - 30);
        }
    });

    $.ajax({
        url: "Admin/GetNodeData",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        type: "POST",
        data: '{id:"Admission_0"}',
        success: function (d) {
            $("#diag").empty();
            $("#diag").html(d);
            $("input:checkbox").on('click', function () {
                // in the handler, 'this' refers to the box clicked on
                var $box = $(this);
                if ($box.is(":checked")) {
                    // the name of the box is retrieved using the .attr() method
                    // as it is assumed and expected to be immutable
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    // the checked state of the group/box on the other hand will change
                    // and the current value is retrieved using .prop() method
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                } else {
                    $box.prop("checked", false);
                }
            });
            $('#txtHospitalPhone,#txtPhysicianPhone,#txtPhysicianFax,#txtPharmacyPhone').mask("(999)999-9999");
            $(".content-datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });
            $("#txtDischargeDate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });//("option", "dateFormat", "mm/dd/yy");
            var SearchText = "Search resident name...";
            $("#txtResidentName").val(SearchText).blur(function (e) {
                if ($(this).val().length == 0)
                    $(this).val(SearchText);
            }).focus(function (e) {
                if ($(this).val() == SearchText)
                    $(this).val("");
            }).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Admin/Search",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        type: "POST",
                        data: '{func: "search_resident", name:"' + request.term + '"}',
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.id
                                }
                            }));
                        }
                    });
                },
                delay: 200,
                minLength: 0,
                focus: function (event, ui) {
                    $("#txtResidentName").val(ui.item.label);
                    $('#txtResidentId').val(ui.item.value);
                    return false;
                },
                change: function (event, ui) {
                    var dis = $(this);
                    if (dis.val() == '' || dis.val() == "Search resident name...") {
                        $("#addResident").hide();
                        return false;
                    }
                    $("#addResident").toggle(!ui.item);
                },
                select: function (event, ui) {
                    return false;
                }
            }).on('focus', function () {
                $(this).autocomplete('search', $(this).val());
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + item.label + "</a>")
                        .appendTo(ul);
            };
            $('#addResident').click(function () {
                var row = { name: $('#txtResidentName').val() };
                var data = { func: "smart_add_resident", mode: 1, data: row };
                $.ajax({
                    url: "Admin/PostGridData",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (data) {
                        if (data.result == 0) {
                            $('#txtResidentId').val(data.message);
                            $.growlSuccess({ message: "Resident saved successfully!", delay: 6000 });
                            $(this).hide();
                        }
                        //else if (m.result == -1) {
                        //    $.growlError({ message: "Resident saved successfully!", delay: 6000 });
                        //}
                    }
                });
            })
            $.each(_FR.Rooms, function (index, value) {
                $('#room_facility').append($('<option>', { value: 'room_' + value.Id.toString(), text: value.Name }));
            });


        }
    });






});
$(document).click(function (e) {
    if (!$(e.target).closest(".day")[0] && !$(e.target).is(".popupMenu")[0])
        $(".popupMenu").slideUp(300);
});