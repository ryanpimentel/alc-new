﻿ /// <reference path="C:\ALC 1.5 2\trunk\Datasoft.AssistedLiving.Web\Views/Shared/Lockout.cshtml" />
/// <reference path="C:\ALC 1.5 2\trunk\Datasoft.AssistedLiving.Web\Views/Shared/Lockout.cshtml" />
$(function () {
    $(".loader").show();
})
var fLastRow;
var rowsToColor = [];
html = "";
html_tile = "";
resmode = "";
var prev_tab = "";
var tabindex_adm = "";
var currentId = "";
var SaveDocCheckForms = true;
var isSaveAndPrint = false;
var resStateNew = "";
var admFormState = "";
var residentrowState = "";
var isClickedSaveResident = 0;
var confirmProceedAddDuplicate = 0;
var residentrowStateFirstName = "";
var residentrowStateLastName = "";
var residentrowStateMI = "";
var curresidentrowStateFirstName = "";
var curresidentrowStateLastName = "";
var curresidentrowStateMI = "";
var hassavededit = false;
var tags = [];
var array = [];
var currentadmissiondate = "";


var scroll1 = new PerfectScrollbar('#residentsTile');
var scroll2 = new PerfectScrollbar('#tabs-1rf');
var scroll3 = new PerfectScrollbar('#divfile1');
var residentallergy = 0;
var is_admitted = 0;
var agency_ishomecare = "";

$(document).click(function (e) {
    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
        $('.popover').popover('hide');
    }
});
//added to auto resize comments text field : Cheche : 10-25-2022
var txComments = document.getElementsByTagName('textarea');
for (var i = 0; i < txComments.length; i++) {
  txComments[i].setAttribute('style', 'height:5px' + (txComments[i].scrollHeight) + 'px;overflow-y:hidden;');
  txComments[i].addEventListener("input", OnInput, false);
}

function OnInput(e) {
  this.style.height = 'auto';
  this.style.height = (this.scrollHeight) + 'px';
}

// ends here
function getallergy() {
    $.ajax({
        url: "Admin/Search",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        data: '{func: "search_allergy"}',
        success: function (d) {
            document.getElementById('Allergies').innerHTML = "";
            //   var tagInputEle = $('#txtAllergy');
            //   tagInputEle.tagsinput();
            for (let i = 0; i < d.length; i++) {
                if (d[i].allergy_name != null) {
                    var option = "<option  id='" + d[i].allergy_id + "' value='" + d[i].allergy_name + "'></option>"
                    document.getElementById('Allergies').innerHTML += option;


                }


            }

        }
    });
} function getalle(array) {


    const input = document.querySelector('.tag-container input');

    function createTag(label) {


        const divs = document.createElement('div');
        divs.setAttribute('class', 'tag');
        const span = document.createElement('span');
        span.innerHTML = label;
        const closeBtn = document.createElement('c');
        closeBtn.setAttribute('class', label);
        closeBtn.innerHTML = 'x';
        divs.appendChild(span);
        divs.appendChild(closeBtn);
        return divs;



    }

    function reset() {

        document.querySelectorAll('.tag').forEach(function (tag) {
            tag.parentElement.removeChild(tag);
        })
    }

    function addTag() {
        const tagContainer = document.querySelector('.tag-container');
        reset();
        tags.slice().reverse().forEach(function (tag) {

            if (tag != " ") {
                const input = createTag(tag);
                tagContainer.prepend(input);
            }
        })
    }
    if (array != undefined) {
        tags.push(array);
        addTag();
        input.value = '';
    }

    input.addEventListener('keyup', function (e) {

        if (e.key == ' ') {
            if
            (input.value.length != 0) {
                tags.push(input.value);
                addTag();
                input.value = '';
            }

        }
    })
    document.addEventListener('click', function (e) {
        if (e.target.tagName == 'C') {

            const value = e.target.getAttribute('class');
            const index = tags.indexOf(value);
            if (index >= 0) {
                tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
                addTag();
            }

        }
    })
    return
}


var load_residents = function (status) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=resident&param=" + status,
        dataType: "json",
        async: false,
        success: function (d) {       
            
            $.each(d, function (i, v) {
              
                button_class = "";
                icon_class = "";
                dis_title = "";

                if (v.isAdmitted == true) {
                    button_class = "discharge btn m-btn m-btn--icon btn-lg m-btn--icon-only btn-outline-primary";
                    icon_class = "fa fa-sign-out-alt";
                    if (agency_ishomecare == "True") {
                        dis_title = "End Contract";
                    } else {
                        dis_title = "Discharge resident";
                    }
                    
                } else {
                    button_class = "cancel_discharge btn m-btn m-btn--icon btn-lg m-btn--icon-only btn-danger";
                    icon_class = "fa fa-arrow-circle-left";
                    if (agency_ishomecare == "True") {
                        dis_title = "Cancel End of Contract";
                    } else {
                        dis_title = "Cancel discharge of resident";
                    }
                    
                } 

                gender = "";
                gender_text = "";
                check_text = "";
                used = "";
                hover = "";
               
                //add checker if agency is homecare, default address is home address
                if (agency_ishomecare == "True") {
                    hover = '<p style="font-size:10px; margin-bottom:0"  class="card-text topop" data-toggle="popover" data-trigger="hover" title="Address" data-content="' + (v.AddressPriorAdmission == null || v.AddressPriorAdmission == '' ? "<span><i class='fas fa-home'></i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;—<br>" : "<span><i class='fas fa-home'></i></span>&nbsp;&nbsp;&nbsp;" + v.AddressPriorAdmission + '<br>') + '">';
                    if (v.AddressPriorAdmission == null || v.AddressPriorAdmission == "") {

                        used = '<i class="fas fa-home pop-icon"></i> ' + '' + '<br/>';
                    }
                    else {
                        used = '<i class="fas fa-home pop-icon"></i> ' + v.AddressPriorAdmission.substring(0, 10) + '...' + '<br/>';
                    }
                } else {
                    //add code kim 12/03/2021
                    if (v.Check == 0) {
                        used = '<i class="fas fa-bed pop-icon"></i> ' + v.RoomName + '<br/>';
                        hover = '<p style="font-size:10px; margin-bottom:0"  class="card-text topop" data-toggle="popover" data-trigger="hover" title="Room" data-content="' + (v.RoomName == null || v.RoomName == '' ? "<span><i class='fas fa-bed'></i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;—<br>" : "<span><i class='fas fa-bed'></i></span>&nbsp;&nbsp;&nbsp;" + v.RoomName + '<br>') + '">';
                    }
                    else if (v.Check == 1) {
                        // kim 12/31/2021 revise add code truncate  for address
                        hover = '<p style="font-size:10px; margin-bottom:0"  class="card-text topop" data-toggle="popover" data-trigger="hover" title="Address" data-content="' + (v.AddressPriorAdmission == null || v.AddressPriorAdmission == '' ? "<span><i class='fas fa-home'></i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;—<br>" : "<span><i class='fas fa-home'></i></span>&nbsp;&nbsp;&nbsp;" + v.AddressPriorAdmission + '<br>') + '">';
                        if (v.AddressPriorAdmission == null || v.AddressPriorAdmission == "") {

                            used = '<i class="fas fa-home pop-icon"></i> ' + '' + '<br/>';
                        }
                        else {
                            used = '<i class="fas fa-home pop-icon"></i> ' + v.AddressPriorAdmission.substring(0, 10) + '...' + '<br/>';
                        }
                    }
                }
                

                if (v.Gender == "M") {
                    gender = '<i class="fas fa-mars pop-icon"></i>';
                    gender_text = 'Male';
                } else if (v.Gender == "F") {
                    gender = '<i class="fas fa-venus pop-icon"></i>';
                    gender_text = 'Female';
                } else if (v.Gender == "" || v.Gender == null) {
                    gender = '<i class="fas fa-genderless pop-icon"></i>';
                }
            
               
                html_tile += '<div class="col-xl-2 col-sm-4 col-md-3 col-xs-12 parent" style="padding-bottom: 15px;padding-top: 10px;padding-right: 0.20% !important;padding-left: 0.20% !important;">';
                html_tile += '<div class="card m-portlet" style=" width: 100%;">';
                html_tile += '<div class="topop imgdiv" style="width:100%;padding:7px;padding-bottom:0px" data-toggle="popover" data-trigger="hover" title="Emergency Contact Info" data-content="' + (v.ResponsiblePerson == null || v.ResponsiblePerson == '' ? "<span><i class='fas fa-user pop-icon'></i></span>N/A <br>" : "<span><i class='fas fa-user pop-icon'></i></span>" + v.ResponsiblePerson + '<br>') + (v.ResponsiblePersonTelephone == null || v.ResponsiblePersonTelephone == '' ? "<span><i class='fas fa-phone pop-icon'></i></span>N/A <br>" : "<span><i class='fas fa-phone pop-icon'></i></span>" + v.ResponsiblePersonTelephone + '<br>') + (v.ResponsiblePersonAddress == null || v.ResponsiblePersonAddress == '' ? "<span><i class='fas fa-home pop-icon'></i></span>N/A <br>" : "<span><i class='fas fa-home pop-icon'></i></span>" + v.ResponsiblePersonAddress + '<br>') + '">';
              //  html_tile += '<div class="topop imgdiv" style="width:100%;padding:7px;padding-bottom:0px" data-toggle="popover" data-trigger="hover" title="Prior Address"' + (v.AddressPriorAdmission == null || v.AddressPriorAdmission == '' ? "<span><i class='fas fa-home pop-icon'></i></span>N/A <br>" : "<span><i class='fas fa-home pop-icon'></i></span>" + v.AddressPriorAdmission + '<br>') + '">';
               
                html_tile += '<img style="object-fit: contain" class="card-img-top resident_name" data-toggle=\"modal\" data-target=\"#resident_modal\" id="' + v.Id + '" src="Resource/ResidentImage?id=' + v.Id + '&r=' + Date.now() + '" alt="Card image cap">';

                html_tile += '</div><div class="card-body" style="padding-top:5px">';
              // kim add code capitalize below for every first letter 01 / 14 / 2022 
                html_tile += '<div class="btn-group" style="width:100%" role="group"><a href="#" style=" text-transform: capitalize; min-width:80%;min-hepadding-bottomight:38px" class="card-title btn btn-sm btn-primary resident_name" data-toggle=\"modal\" data-target=\"#resident_modal\" id="' + v.Id + '">' + v.Lastname + " ,<br/>" + v.Firstname + " " + (v.MiddleInitial == "" ? "" : v.MiddleInitial.toUpperCase() + ".") + '</a>';
                html_tile += '<a style="height:38px;" data-toggle="popover" data-trigger="hover" data-content="' + dis_title + '" href="#" resname=" ' + v.Name + '" class="' + button_class + '" id="' + v.Id + '"><i class="' + icon_class + '"></i>' + '</a></div>';

                html_tile +=  gender + ' ' + gender_text + '' + (v.IsDNR == true ? '<span class="dnr"><span>DNR</span></span>' : "") + '<br/>';
                // delete other info such as responsible person, address and contact number kim 12/03/2021
              //  + gender + ' ' + gender_text + '' + (v.IsDNR == true ? '<span class="dnr"><span>DNR</span></span>' : "") + '<br/>';
                html_tile += hover;
                html_tile += used;
                               
                
                html_tile += '<i class="fas fa-birthday-cake pop-icon"></i> ' + v.Birthdate + '<br/>';
               
                html_tile += '<p style="font-size:10px" class="card-text topop" data-toggle="popover" data-trigger="hover" title="SSN" data-content="' + (v.SSN == null || v.SSN == '' ? "<span><i class='fas fa-info'></i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;—<br>" : "<span><i class='fas fa-info'></i></span>&nbsp;&nbsp;&nbsp;" + v.SSN) + '">'+ '<i class="fas fa-info pop-icon"></i> <span style="font-size:9.5px"> SSN: ' + (v.SSN == null ? '' : v.SSN.substring(0, 8) + '...') + ' </span><br/>';
                html_tile += '</p>';
                html_tile += '</p></div>';

                html_tile += '</div></div>';

            });
            $("#residentsTile").html(html_tile);
            html_tile = ""
            $(".topop, .discharge, .cancel_discharge").popover({
                html: true,
                trigger: 'hover',
                delay: { "show": 200, "hide": 100 }
            });

            $(".loader").hide();
            setHeight();
        }

    });
    
    


    $('.resident_name').on('click', function () {
        is_admitted = 1;
        tags = [];
        array = [];

        getalle();
        getallergy();

      //  inputallergy();
        //added this block to bind changing of all admissiondate inputs       
        // add minimun date for admission kim 10/21/2022   
        $('#txtAdmissionDate,#txtDateAdmitted,#AAAdmissionDate').off('change').on('change', function () {
            var id = this.id;
            if (id == 'txtDateAdmitted') {
                $('#AAAdmissionDate, #txtAdmissionDate').val(this.value);
            } else if (id == 'AAAdmissionDate') {
                $('#txtDateAdmitted, #txtAdmissionDate').val(this.value);
            } else if (id == 'txtAdmissionDate') {
                $('#txtDateAdmitted, #AAAdmissionDate').val(this.value);
            }
        });

        $("#txtMedicalPlanNumber, #txtMedicalPlanNumber2, #txtMedicalPlanNumber3, #txtDentalPlanNumber").forceNumericOnly();
        $(".resimg").html('');
        $("#residentForm").find("li a").not(":eq(0)").show();
        var tbs = $("#residentForm .nav-tabs").find("a");
        $.each(tbs, function (i, e) {
            $(this).removeClass("active");
            $(this).removeClass("show");
        })

        $.each(tbs, function (i, e) {
            if ($(this).attr("href") == "#tabs-1rf") {
                $(this).click();
            }
          
            if ($(this).attr("href") == "#tabs-1a") {
                
               $(this).click();
             }
        })

      
        resmode = 2;
        currentId = $(this).attr("id");
        //createTabGrids();
        $(".resdform").show();

        //if (!$("#residentForm").data("tabs")) {
        $("#residentForm").tabs({
            
            activate: function (event, ui) {
                var index = $("#residentForm").tabs("option", "active");
                if (is_admitted == 1) {
                    $("#btnSaveResident").attr("disabled", false);

                } else {
                    $("#btnSaveResident").attr("disabled", true);
                    $("#addNewAssessment").attr("disabled", true);
                   
                    
                }
                
                if (index != 5) {
                    $("#btn-printsr").attr("disabled", true);
                }
            
                if (index == 1) {             
                   
                    $("#room_facility").val("room_" + $('#DRoomId option:selected').val());
                    setHeight();
                    $("#residentForm .tabPanels").css("overflow-y", "hidden");
                } else {
                    setHeight();
                    $("#residentForm .tabPanels").css("overflow-y", "auto");
                }

                if (index == 4) {
                    $("#residentForm .tabPanels").css("overflow-x", "auto");
                } else {
                    $("#residentForm .tabPanels").css("overflow-x", "hidden");
                }

                if (index == 2) {
                    $(".loader").show();
                    $("#docs_table").html('');
                    $("#btnSaveResident").attr("disabled", true);
                    setTimeout(function () {
                        initDocuments();
                        $(".loader").hide();
                    }, 100)
                } else if (index == 3) {
                 
                    $(".loader").show();
                    $("#services_table").html('');
                    $("#btnSaveResident").attr("disabled", true);
                    setTimeout(function () {
                
                        if (agency_ishomecare == "True") {
                            initServiceHomeCare();
                        } else {
                            initService();
                        }                       
                        
                        $(".loader").hide();
                    }, 100)
                } else if (index == 4) {
               
                     $(".loader").show();
                    // $("#fromDate,#toDate, #hfromDate, #htoDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));
                    $("#history_table").html('');

                    var completion_date = $("#fromDate").val();
                    var completion_date_end = $("#toDate").val();
            
           

                    if (completion_date == "") {
                        var date = new Date();
                        completion = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
                        $("#fromDate").val((date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear());
                    } else {
                        completion = completion_date + " 12:00:00 AM";
                    }

                    if (completion_date_end == "") {
                        var date = new Date();
                        completion_end = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
                        $("#toDate").val((date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear());
                    } else {
                        completion_end = completion_date_end + " 12:00:00 AM";
                    }
               
                    //$("#hGrid").DataTable().destroy();
                   // $(".loader").show(); //10-4-2022
                    setTimeout(function () {
                 //  debugger;
                        $(".loader").show();
                        generateWeeklySchedule(completion, completion_end, "All");
                        $(".loader").hide();
                    }, 100)
                    //$("#hGrid").DataTable({
                    //    "scrollY": "400px",
                    //    "scrollCollapse": true,
                    //    stateSave: true,
                    //    responsive: !0,
                    //    pagingType: "full_numbers",
                    //    dom: 'lBfrtip'
                    //});
                    // setTimeout(initHistory, 1);
                    $("#btnSaveResident").attr("disabled", true);
                } else if (index == 1) {
                    //} else if (ui.index == 3) {
                    //    if ($("#admForm").tabs('option', 'selected') != 0)
                    //        $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
                    //        .removeClass("ui-state-disabled");
                } else if (index == 5) {
                    var data = {
                        Id: currentId
                    };
                    loadAssessment(data);
                    $("#btn-printsr").attr("disabled", false);
                    $("#btnSaveResident").attr("disabled", true);
                    $('#printappraisaldate').html('');
                    $.get(ALC_URI.Admin + "/GetAppraisalNSDates?id=" + $('#txtAdmissionId').val(), function (data) {
                        $.each(data, function () {
                            $('#printappraisaldate').append($('<option>', {
                                value: this.id,
                                text: this.date
                            }));
                        });
                    });
                    $('#printaggreementdate').html('');
                    $.get(ALC_URI.Admin + "/GetAgreementDates?id=" + $('#txtAdmissionId').val(), function (data) {
                        $.each(data, function () {
                            $('#printaggreementdate').append($('<option>', {
                                value: this.id,
                                text: this.date
                            }));
                        });
                    });
                    $('#printassessmentdate').html('');
                    $.get(ALC_URI.Admin + "/GetAssessmentDates?id=" + $('#txtResidentId').val(), function (data) {
                        $.each(data, function () {
                            $('#printassessmentdate').append($('<option>', {
                                value: this.id,
                                text: this.date
                            }));
                        });
                    });

                }
            }

        });

        $("#residentForm").tabs("option", "active", 0);
        //$("#residentForm").tabs("option", "active", 0);
        // }
        // $("#residentForm").tabs("option", "selected", 0);

        var dis = $(this);
        var css = dis.attr('class');
        var rid = dis.attr('id');

        doAjax(0, rid, function (res) {
            
            $(".loader").show();
            $(".resident-modal-name").html(res.Firstname + " " + (res.MiddleInitial != null ? res.MiddleInitial + "." : "") + " " + res.Lastname);
            //console.log(res);
            //$(".resimg").html(row.Image);
            //$(".resimg > .resbtn").hide();
            $('#residentForm .resForm').mapJson(res);
            htmll = "<img class='imgsrc' src='Resource/ResidentImage?id=" + rid + "&r=" + Date.now() + "' alt='" + "" + "' width='100%' style='cursor:pointer;border-radius:5%'/>";
            $(".resdform .resimg").html(htmll);
            //$('#txtDateLeft, #txtBirthdate').datepicker();
            //showdiag(2);
            var title = $("#diag").dialog("option", 'title');           
            $("#isAdmitted_status").val(res.IsAdmitted);
            //loadAssessment(rid);


            
            if (res.IsAdmitted == true) {
            
                //$(".resForm :input").removeAttr("disabled");
                //Cheche was here : 2/14/2022 to enabled & disabled some fields to avoid confusion & disabled SSN for edit              
                $('#tabs-1rf, #txtSSN').attr('disabled', true); 
                //9-23-2022 : Cheche for EOC date : for Homecare
                if (agency_ishomecare == "True") {
                    if (res.DateLeft == "") {
                        $("#txtDateLeft").attr('disabled', false);
                        $("#txtDateLeft").css({ "pointer-events": "unset", "background-color": "#ffffff", "opacity": "1" });
                    } else {
                        $("#txtDateLeft").attr('readonly', true);
                        $("#txtDateLeft").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                    }
                } else { //for ALC discharge/date left date input
                    //if(res.DateLeft == ""){
                         $("#txtDateLeft").attr('enabled', true);
                    //}else{
                          $("#txtDateLeft").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                    //}
                     $("#txtDischargeDate").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                        //$("#txtDischargeDate").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" }); // for ALC
                        $("#txtReasonsLeavingFacility").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                        $('#txtForwardingAddress').removeAttr('readonly');
                  
                }
                                               
                $('#txtDateAdmitted, #txtAdmissionDate').removeAttr('readonly');
                $("#txtDateAdmitted, #txtAdmissionDate").datepicker("enable");               
                $("#btnSaveResident").attr("disabled", false);
                $("#add_task").removeClass("disabled");// add task
                $("#upload-resimg").removeClass("disabled");//upload image
                $("#upload-document").removeClass("disabled");// upload document
                $("#btnSaveAdmission").removeClass("disabled");// save button in admission 
                $("#admForm #tabs-ass #addNewAssessment").removeAttr('style');//remove css in add new assessment button
                $("#admForm #tabs-ass #addNewAssessment").removeAttr("disabled", false);
                $("#tabs-2a input[type=text]").removeAttr("disabled", false);
                $("#divfile2").removeAttr('style');
                $("#tabs-1rf input[type=text], input[type=checkbox]").removeAttr("disabled");
                $("#admForm #tabs-1a input[type=text] ").removeAttr("disabled", false);
                $("#admForm #tabs-1a #btnSaveResident").removeAttr("disabled", false)
                $("#admForm #tabs-ass input[type=text] ").removeAttr("disabled", false);
              
               
             //   $("#tabs-adm #tabs-2a input[type=text], input[type=checkbox] ").removeAttr("disabled", false);     
                $("#ddlGender, #DRoomId, #ResidentStatus").removeAttr("disabled", false);
                $("#admForm #tabs-3r input[type=text]").removeAttr("disabled", false);

                //added this to disable end of care or discharge date, angelie: 09-14-2022
               //$("#txtDateLeft").attr('disabled', true);
            } else { 
                is_admitted = 0;  
                $("#divfile2").css({ "pointer-events": "none" });
                $("#ddlGender, #DRoomId, #ResidentStatus").attr("disabled", true);
             //   $('#txtDateLeft, #txtReasonsLeavingFacility').attr('readonly', 'readonly');             
                $('#txtDateAdmitted, #txtAdmissionDate').attr('readonly', 'readonly');
                $("#txtDateAdmitted, #txtAdmissionDate").datepicker("destroy");
             //   $('#txtDateLeft, #txtReasonsLeavingFacility').attr('disabled', 'false');
             //   $('#txtDischargeDate').attr('disabled', 'false');
             //   $("#txtDischargeDate").datepicker("destroy");
                $("#upload-resimg").addClass("disabled", true); // disable upload button   
                $("#tabs-2a input[type=text]").attr("disabled", true); 
                $("#tabs-1rf input[type=text], input[type=checkbox]").attr("disabled", true); 
                $("#btnSaveResident").attr("disabled", true); //disable saving info when resident is discharged
                $("#add_task").addClass("disabled"); //cannot add service task when resident is discharged
                $("#admForm #tabs-1a #btnSaveResident").attr("disabled", true);// kim add disable save button for discharge 03/09/22
                $("#admForm #tabs-1a input[type=text] ").attr("disabled", true);         
                $("#admForm #tabs-ass input[type=text] ").attr("disabled", true);
             //   $("#tabs-adm #tabs-2a input[type=text], input[type=checkbox] ").attr("disabled", true);
                $("#admForm #tabs-ass #addNewAssessment").css({ "pointer-events": "none" });//add css in add new assessment button
             
                
             
                $("#btnSaveAdmission").addClass("disabled", true);
                $("#btnSaveAdmission").css({ "pointer-events": "none" });
                $("#tabs-doc .doc_delete").css({ "pointer-events": "none" });

                $("#admForm #tabs-3r input[type=text]").attr("disabled", true);// kim add new disable 03/16/22
                $("#upload-document").addClass("disabled", true);

            }
            residentState.orig = JSON.stringify($('#residentForm .resForm').extractJson());
            residentrowState = $('#residentForm .resForm').extractJson();
            residentrowStateFirstName = $("#txtFirstname").val();
            residentrowStateLastName = $("#txtLastname").val();
            residentrowStateMI = $("#txtMiddleInitial").val();
          
            $("#txtRStateId").val(res.RState);
            $("#txtResStateId").val(res.ResState);
            $("#txtRState").val(res.RState);
            $("#txtResState").val(res.ResState);
        });

        // $("#admForm").tabs("option", "selected", 0);
        doAjaxAdm(0, rid, function (res) {           
            //console.log(res)
          
            $(".adm-content").clearFields();
            $(".adm-content").mapJson(res, 'id');
            $(".loader").hide();

            room_id = res['room_facility'].split("_");
            $("#room_facility").val(parseInt(room_id[1]));

            //kim 11/11/2022 add this in general information under admission
            var al = $("#txtAllergy").val();
            if (al != null && al != "") {

                var aname = al.split(',');
                array = aname.filter(item => item);
                var len = array.length - 1;
                for (var i = 0; i <= len; i++) {
                    getalle(array[i]);
                }
            }
            else {
                tags = [];
                array = [];
                $('.tag-container').find('.tag').remove()
            }
           
            $("#DCRoomNumber, #RMMRoomNo").val($('#DRoomId option:selected').text());
            arow1 = $('#admForm #tabs-1a').extractJson('id');
            arow2 = $('#admForm #tabs-2a').extractJson('id');
            arow3 = $('#admForm #tabs-3r').extractJson('id');
            resStateNew = $.extend(arow1, arow2, arow3);

        });


    });
   
 
    //added on 4-18-2022 : Cheche
    $("#txtSSN").on('click', function () {

        if ($('#txtSSN').click == 1) {
            if ($('#txtSSN').val() != "") {
                swal("Double check residents SSN number before saving!", "Editing SSN number after saving is not allowed!", "info");
            }
        }
       
    });
    // add minimun date for admission kim 10/21/2022
    var minDate = new Date();
    $('#txtDateAdmitted').datepicker('setStartDate', minDate);     
    $('#txtAdmissionDate').datepicker('setStartDate', minDate);     
  


    $('#btnSaveResident, #btnSaveAdmission').on('click', function () {
        if ($(this).attr('id') == "btnSaveResident") {
            isClickedSaveResident = 1;
        }
        $(".loader").show();
        var form = form = document.getElementById('tabs-1rf');
        var f = form.getElementsByClassName('required');
        for (var i = 0; i < f.length; i++) {
            if (f[i].value == '') {

                $(".loader").hide();
                swal("Fill in all required fields on Resident Information section")
                // $(".req").show();

                return false;
            } else {
                //  $(".req").hide();
            }
        }

        // Added this on 01-06-2022 : ANGELIE
        var adDate = $("#txtAdmissionDate").val();
        var discDate = $("#txtDischargeDate").val();
        var dateToday = new Date();
       
        var hasDiscDate = (discDate != null && discDate != "" ? true : false);
        var hasAdDate = (adDate != null && adDate != "" ? true : false);
        var hascad = (currentadmissiondate != null && currentadmissiondate != "" ? true : false);

        if (hasDiscDate) { discDate = new Date(discDate); };
        if (hasAdDate) { adDate = new Date(adDate); };
        if (hascad) { currentadmissiondate = new date(currentadmissiondate); };


        if ($('#checkSendEmail').is(":checked")) {
            if ($("#txtResponsiblePersonEmail").val() == '') {
                swal("Send email notification is turned on. Please fill out 'Responsible Person Email' field to activate this feature.", "", "warning")
                $(".loader").hide();
                return false;
            }
        }
        var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        var email_address = $("#txtResponsiblePersonEmail").val();

        if (email_address != "") {
            if (!email_regex.test(email_address)) {
                swal("Invalid email address", "", "warning");
                $(".loader").hide();
                return false;
            }
        }
     
       //Modified code here to auto discharge residents if date left has already passed : 11-15-2022: Cheche
         var id = $('#admForm #txtResidentId').val();
         var dleft = $("#txtDateLeft").val();
          var pastdate = new Date();
          pastdate = (pastdate.getMonth() + 1) + "/" + pastdate.getDate() + "/" + pastdate.getFullYear();
      
        if ($(this).attr('id') == "btnSaveResident" ) {
            if (dleft > pastdate && dleft != "") {
                SaveDocCheckForms = false;
                showdiag(resmode);
            } else if (dleft < pastdate && dleft != "") {
                $(".loader").hide();
                var resname = $('#admForm #txtResidentName').val();
                if (agency_ishomecare == "False") {
                    var txtTitle = "Discharge Date has already passed!"
                }
                swal({
                    title: txtTitle,
                    text: resname + "'s will be discharge!",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonText: "Yes, Proceed."
                }).then(function (e) {
                    e.value &&
                        doAjax(6, {
                            ResidentId: id
                        }, function (m) {
                            if (m.result == 0) {
                                if (agency_ishomecare == "True") {
                                    swal(resname + "'s contract has been ended!", "", "success")
                                } else {
                                    swal(resname + " has been discharged successfully!", "", "success")
                                }
                                $(".loader").show();
                            }
                            $("#resident_modal").modal('hide');
                            $(".loader").show();
                            load_residents('active_only');
                        });
                });
            }
            //add this for savng in general information kim 11/18/22
            else {
                SaveDocCheckForms = false;
                showdiag(resmode);
            }
        } else {
            $('.loader').show();
            SaveDocCheckForms = true;
            showdiag(resmode);
            //$('#files_modal').modal('hide');
        
       
            }
    });

    //added by Cheche to auto-reflect status DNR : 3-22-2022
    $('.cancelbtn').on('click', function () {
        load_residents('active_only');
    });
      

     //clearing text om Search Box : 11-04-2022 : Cheche
     $("#close_resident, #resClose").on('click', function(){
       $("#showall").trigger('click');
     });

    $('#btnConfirmAddDuplicate').on('click', function () {
        confirmProceedAddDuplicate = 1;
        $('#btnSaveResident').trigger('click');
    });

    


    $("#headerRoom #addResident").on("click", function () {

        $(".resident-modal-name").html("");
        $("#txtSSN").removeAttr('disabled', 'false'); //4-18-2022 : Cheche
     //   $('#txtDateLeft, #txtReasonsLeavingFacility').attr('disabled', 'false');
        $("#tabs-1rf input[type=text], input[type=checkbox]").removeAttr('disabled', false);
        $("#ddlGender, #DRoomId, #ResidentStatus").removeAttr("disabled", false);
        $('#txtDateAdmitted, #txtAdmissionDate').removeAttr('readonly');
        $("#txtDateAdmitted, #txtAdmissionDate").datepicker("enable");
        $("#btnSaveResident").attr("disabled", false);
        $('#residentForm').clearFields();
        $(".tag").remove();
        $("#ResidentStatus").val(1);
        $("#residentForm").tabs();
        $("#residentForm").find("li a").not(":eq(0)").hide();
        resmode = 1;
        $(".resdform").hide();
       
        var tbs = $("#residentForm .nav-tabs").find("a");
        $.each(tbs, function (i, e) {
            $(this).removeClass("active");
            $(this).removeClass("show");
        })

        $.each(tbs, function (i, e) {
            if ($(this).attr("href") == "#tabs-1rf") {
                $(this).click();
            }
        })

        if (agency_ishomecare == "True") {
            $("#txtDateAdmitted").css({ "pointer-events": "all", "background-color": "#ffffff", "opacity": "1" });
            $("#txtDateLeft").css({ "pointer-events": "all", "background-color": "#ffffff", "opacity": "1" });
            $("#txtReasonsLeavingFacility").css({ "pointer-events": "all", "background-color": "#ffffff", "opacity": "1" });
            $("#txtForwardingAddress").removeAttr("readonly");
            $("#txtDateLeft").attr('disabled', false);
            $("#txtReasonsLeavingFacility").attr("disabled", true);
        } else {
            //$("#txtDateLeft").attr('enabled', false);
            $("#txtDateLeft").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
            $("#txtDischargeDate").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
            $("#txtReasonsLeavingFacility").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
            $('#txtForwardingAddress').removeAttr('readonly');

        }
    });
    //inputallergy();

    $("#upload-resimg").on("click", function () {
        
        var id = $('#admForm #txtResidentId').val();
        var resname = $('#admForm #txtResidentName').val();
        $('#resid').val(id);
        $(".modal-residentName").html("Upload photo for " + resname);
        //kim add current image for initial preview 03/23/2022 modified for special char in UserID 05/23/22

        var sample = '#' + id.replace(/(:|\.|\[|\]|,|=|@)/g, '\\$1').trim();
        //   var sample = JSON.stringify(id).replace(/(:|\.|\[|\]|,|=|@)/g, '\\\\$1').trim(); 
        // JSON.stringify(input).replace(/((^")|("$))/g, "").trim();
        // var test1 = $(sample);
        //var test = $("#sample\\.123");
        //   var imagesrc = $('#' + sample);
        var imgSrc = $('#' + id).attr('src');
        if (imgSrc != undefined) {
            $('#image').attr('src', imgSrc);
        }
        else {
            var imgSrc = $(sample).attr('src');
            //  var imgSrc = $("#" + sample).attr('src');
            $('#image').attr('src', imgSrc);
        }

    });


    $('.discharge').on('click', function () {
        var dis = $(this);
        var rid = dis.attr('id');
        if (!rid) return;
        var rname = dis.attr('resname');

        if (agency_ishomecare == "True") {
            var txtTitle = "Are you sure you want to end contract with "
        } else {
            var txtTitle = "Are you sure you want to discharge ";
        }
        
        swal({
            title: txtTitle + rname +"?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, Proceed."
        }).then(function (e) {
            e.value &&
                    doAjax(6, {
                        ResidentId: rid
                    }, function (m) {
                        setTimeout(function () { 
                            if (agency_ishomecare == "True") {
                                swal(rname + "'s contact has been ended!", "", "success")
                            } else {
                                swal(rname + " has been discharged successfully!", "", "success")
                            }                            
                        }, 300);
                        //if (m.result == -3) //inuse
                        //    msg = row.Name + " cannot be discharged because it is currently in use.";
                        $("#residents_1 a").trigger("click");
                    });
        });
    });

    $('.cancel_discharge').on('click', function () {
        var dis = $(this);
        var rid = dis.attr('id');
        if (!rid) return;
        var rname = dis.attr('resname');
        swal({
            title: "Are you sure you want to cancel discharge of resident " + rname + "?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, cancel discharge."
        }).then(function (e) {
            e.value &&
                    doAjax(7, {
                        ResidentId: rid
                    }, function (m) {
                        setTimeout(function () {
                            swal(rname + "'s discharged status has been changed!", "", "success")
                        }, 300);
                        //if (m.result == -3) //inuse
                        //    msg = row.Name + " cannot be discharged because it is currently in use.";
                        $("#residents_1 a").trigger("click");
                    });
        });
    });

    $('#upload-document').on('click', function () {
        var id = $('#admForm #txtResidentId').val();
        var resname = $('#admForm #DCResidentName').val();
        $('#dresid').val(id);
        $("#modal-title-ud").html("Upload document for " + resname)
        //console.log("DOCUMENT");
        uploadDocument();

    });


    $('#btnUploadDocument').on('click', function () {
        initDocuments();
    });


    $("#financial-res").on("click", function () {
        var id = $('#admForm #txtResidentId').val();
        var resname = $('#admForm #txtResidentName').val();
        $('#fresid').val(id);
        $(".modal-residentName").html("Financial Responsible - " + resname);

    });

    $('#btnAddFinancialRes').on('click', function () {
        parsefinancialres(1);
    });




    $('#add_task').on('click', function () {
        if ($("#resident_modal #isAdmitted_status").val() == "false") {
            swal("Adding schedule for a discharged resident is not allowed.", "", "warning")
            return false;
        } else {
            // kim add code here to show modal of add service 02/24/22
            $('#addtask_modal').modal('show'); // 2-28-2022 : kimberly
            AddService();
        }

        //setTimeout(function(){

        //}, 700)
    });

    $('#btnAddTask').on('click', function () {
        
        $(this).prop("disabled", true);
        var form = document.getElementById('addtask_modal');
        var f = form.getElementsByClassName('required');
        for (var i = 0; i < f.length; i++) {
            if (f[i].value == '') {
              
                swal("Fill in all required fields.")
                $(".req").show();
                $("#btnAddTask").prop("disabled", false);
                return false;
            } else {
                $(".req").show();
            }
        }
        showdiagTask(1);        
    });

    $("#search_dam_value").keyup(function () {
        var string = $(this).val().toLowerCase();
        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
        $('#residentsTile').scrollTop(1);
    });

    $("#search_dam_value").keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    })

    $("#showall").on("click", function () {
        $("#search_dam_value").val("");
        $(".parent").each(function (i, e) {
            $(this).show();
        });

    });

    var parsefinancialres = function (mode, id) {
        if (mode == 1) {
            var id = $('#admForm #txtResidentId').val();
            row = $('.financialdiv').extractJson('id');
            row['Id'] = id;
            var data = {
                func: "financial_and_other",
                mode: 1,
                data: row
            };

        } else {
            data = {
                Id: id
            }

            var data = {
                func: "financial_and_other",
                mode: 3,
                data: data
            };
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //if (cb) cb(m);

                if (m.result == 0) {
                    //$.growlSuccess({
                    //    message: "Document has beend deleted successfully!",
                    //    delay: 3000
                    //});
                    if (mode == 1) {
                        toastr.success("Financial responsible added successfully!");

                    } else if (mode == 3) {
                        toastr.success("Financial responsible deleted successfully!");
                    }
                }

                initFinancialResponsible();
            }
        });
    }

    //$("#diag").dialog({
    //    zIndex: 0,
    //    autoOpen: false,
    //    modal: true,
    //    title: "Residents",
    //    dialogClass: "companiesDialog",
    //    open: function () {
    //        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
    //            .addClass("ui-state-disabled");
    //        if (!$("#residentForm").data("layoutContainer")) {
    //            $("#residentForm").layout({
    //                center: {
    //                    paneSelector: ".tabPanels",
    //                    closable: false,
    //                    slidable: false,
    //                    resizable: true,
    //                    spacing_open: 0
    //                },
    //                north: {
    //                    paneSelector: ".tabs",
    //                    closable: false,
    //                    slidable: false,
    //                    resizable: false,
    //                    spacing_open: 0,
    //                    size: 27
    //                }
    //            });
    //        }
    //        $("#residentForm").layout().resizeAll();
    //        if (!$("#residentForm").data("tabs")) {
    //            $("#residentForm").tabs({
    //                select: function (event, ui) {
    //                    $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
    //                        .addClass("ui-state-disabled");
    //                    $('#assPager').hide();
    //                    if (ui.index == 6) {
    //                        setTimeout(initHistory, 1);
    //                    } else if (ui.index == 5) {
    //                        setTimeout(initService, 1);
    //                    } else if (ui.index == 4) {
    //                        setTimeout(initDocuments, 1);
    //                    } else if (ui.index == 3) {
    //                        if ($("#admForm").tabs('option', 'selected') != 0)
    //                            $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
    //                            .removeClass("ui-state-disabled");
    //                    } else if (ui.index == 7) {
    //                        $('#printappraisaldate').html('');
    //                        $.get(ALC_URI.Admin + "/GetAppraisalNSDates?id=" + $('#txtAdmissionId').val(), function (data) {
    //                            $.each(data, function () {
    //                                $('#printappraisaldate').append($('<option>', {
    //                                    value: this.id,
    //                                    text: this.date
    //                                }));
    //                            });
    //                        });
    //                        $('#printaggreementdate').html('');
    //                        $.get(ALC_URI.Admin + "/GetAgreementDates?id=" + $('#txtAdmissionId').val(), function (data) {
    //                            $.each(data, function () {
    //                                $('#printaggreementdate').append($('<option>', {
    //                                    value: this.id,
    //                                    text: this.date
    //                                }));
    //                            });
    //                        });
    //                        $('#printassessmentdate').html('');
    //                        $.get(ALC_URI.Admin + "/GetAssessmentDates?id=" + $('#txtResidentId').val(), function (data) {
    //                            $.each(data, function () {
    //                                $('#printassessmentdate').append($('<option>', {
    //                                    value: this.id,
    //                                    text: this.date
    //                                }));
    //                            });
    //                        });

    //                    }
    //                }
    //            });
    //        }
    //        $("#residentForm").tabs("option", "selected", 0);
    //    }
    //});







    //added by mariel
    $("#diagupload").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Upload Picture",
        // dialogClass: "companiesDialog",
        open: function () {

        }
    });

    $("#diagHistory").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        width: 1040,
        title: "Service History",
        // dialogClass: "companiesDialog",
        open: function () {

        }
    });



    //load admission tab data

    $.ajax({
        url: "Admin/GetNodeData",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        type: "POST",
        data: '{id:"Admission_0"}',
        success: function (d) {
            
            //console.log("GETNODEDATA " + d);
            $("#tabs-adm").html(d);
            $('#admForm .tabs').css('background', 'none');
            $('#AdmissionContainer').css({
                'padding': '10px 10px'
            });
            $('#admForm .tabPanels').css({
                'border': 'solid 1px #ccc',
                'padding': '5px 5px',
                'overflow': 'auto !important'
            });
            //$("#admForm #liRoom,#admForm #liResident").hide();

            $("input:checkbox").on('click', function () {
                // in the handler, 'this' refers to the box clicked on
                var $box = $(this);
                if ($box.is(":checked")) {
                    // the name of the box is retrieved using the .attr() method
                    // as it is assumed and expected to be immutable
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    // the checked state of the group/box on the other hand will change
                    // and the current value is retrieved using .prop() method
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                } else {
                    $box.prop("checked", false);
                }
            });
            $('#txtHospitalPhone,#txtPhysicianPhone,#txtPhysicianFax,#txtPharmacyPhone').mask("(999)999-9999");

            $("#txtResidentName, #room_facility").attr("disabled", true);

            
            var minDate = new Date();
            $('#txtDateAdmitted').datepicker('setStartDate', minDate);      

            $(".content-datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            }).on('changeDate', function () {
                $(this).datepicker('hide');
            });
            $("#txtDischargeDate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0",
                autoclose: true,
            }).on('changeDate', function () {
                $(this).datepicker('hide');
            }); //("option", "dateFormat", "mm/dd/yy");

            $('#room_facility').append('<option></option>');
            $.each(_FR.Rooms, function (index, value) {
                $('#room_facility').append($('<option>', {
                    value: 'room_' + value.Id.toString(),
                    text: value.Name
                }));
            });

            $('#room_facility').change(function () {
                $('#DCRoomNumber,#AARoomNo,#AARoomNo,#RMMRoomNo').val($('#room_facility option:selected').text());
            });

            $('#DRoomId').change(function () {
                $('#DCRoomNumber,#AARoomNo,#AARoomNo,#RMMRoomNo').val($('#DRoomId option:selected').text());
                $("#room_facility").val("room_" + $('#DRoomId option:selected').val());
            });

            //$("#residentForm").tabs({
            //    activate: function (event, ui) {
            //        var index = $("#residentForm").tabs("option", "active");


            $("#admForm").tabs({
                activate: function (event, ui) {
                    var index = $("#admForm").tabs("option", "active");

                    // kim modified code for disable button 03/17/22
                        if (is_admitted == 1) {
                        
                            $("#btnSaveResident").attr("disabled", false);
                        } else {
                            $("#btnSaveResident").attr("disabled", true);
                        }                  


                    //$("#btnSaveResident").attr("disabled", false);
                    $('#RMMResidentName,#RMMRoomNo').attr('disabled', 'disabled');

                    if (index != 15 || ui.index != 5) {
                        $(".ui-dialog-buttonpane button:contains('Save')").attr("disabled", false)
                            .removeClass("ui-state-disabled");
                    }

                    if (index == 0) {
                        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                            .addClass("ui-state-disabled");
                    } else {
                        $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
                            .removeClass("ui-state-disabled");
                    }
                    if (index == 2) {       
                        var id = $('#admForm #txtResidentId').val();
                        var data = {
                            Id: id
                        };

                        loadAssessment(data);
                        $("#update-status-a").hide();
                        $("#btnSaveResident").attr("disabled", true);
                        $("#content-assessment input").attr("disabled", true);
                        $("#content-assessment #CompletedDate").css("background-color", "#f4f5f8");
                        $("#content-assessment #load").removeAttr("hidden");                      
                     
                       

                        $("#addNewAssessment").on("click", function () {
                           //kim for clearing previous data on add new assessment button 04/26/22
                            setTimeout(function () {
                                $("input[name='AssessmentReason']:checkbox").prop('checked', false);                                
                                $("#TotalScore").val("");   
                                $('#content-assessment #add input[type=checkbox]:checked').removeAttr('checked');  
                                $('textarea').val('');                              
                                $("#label-TotalScore").html(0);    
                                $("#content-assessment #m_assessment_1 #CompletedDate").val("");
                                $("#content-assessment #m_assessment_1 #CompletedDate").css("background-color", "#FFFFCC");
                            }, 5000)
                        
                            $("#content-assessment #add").removeAttr("hidden");  
                            $("#content-assessment #load").attr("hidden", true);  
                            var atbs = $("#content-assessment .nav-tabs").find("a");
                            $.each(atbs, function (i, e) {
                                $(this).removeClass("active");
                                $(this).removeClass("show");
                            })
                            $.each(atbs, function (i, e) {
                                if ($(this).attr("href") == "#m_assessment") {
                                    $(this).click();
                                }
                                if ($(this).attr("href") == "#m_assessment") {
                                    $(this).click();
                                }
                            })
                            loadAssessment(data);
                            $("#content-assessment input").removeAttr("disabled");
                            $("#content-assessment #FirstName, #content-assessment #MiddleInitial, #content-assessment #LastName, #content-assessment #CompletedBy, #content-assessment #TotalScore").attr("disabled", true);
                            $("#btnSaveResident").removeAttr("disabled");                          
                                $('#assPager').show();                        
                        })
                        $("#printAssessment").on("click", function () {
                            var assessment_id = $("#assessment_list").children("option:selected").val();
                            window.open(ALC_URI.Marketer + "/PrintAssessment?id=" + assessment_id, '_blank');
                        })

                        $("#assessment_list").on("change", function () {                         
                            var id = $("#assessment_list option:selected").val();
                            $("#content-assessment #load").removeAttr("hidden");  
                         
                            var data = {
                                Id: id,
                                isAssessment: true
                            };
                            loadAssessment(data);
                            $("#btnSaveResident").attr("disabled", true);
                            $("#content-assessment #CompletedDate").css("background-color", "#f4f5f8");
                            $("#content-assessment input").attr("disabled", true);
                            $('#assessment_list').val(id);                         

                        })
                    }

                    if (index == 1) {

                      
                        $("#DCRoomNumber, #RMMRoomNo").val($('#DRoomId option:selected').text());
                        $('#DCResidentName,#DCRoomNumber').attr('disabled', 'disabled');

                        $(document).on("click", ".form_id", function () {


                            setHeight();

                            setTimeout(function () {
                                //scroll to top of document
                                //debugger
                                $('#divfile1').scrollTop(0);
                            }, 300);

                            //-------- Autopopulate data on document checklist forms ----------
                            // add #AATotalRate to disable in resident adimission agreement kim 11/22/2021

                            $('#RMMResidentName,#AAAdmissionDate,#AARoomNo,#AAResidentName,#AASSNo,#AADOB, #AATotalRate').attr('disabled', 'disabled');
                            $('#ANSResidentName,#ANSFacilityName,#ANSDOB,#ANSFacilityLicenseNo,#ANSAge,#ANSTelNo,#ANSDate').attr('disabled', 'disabled');

                            $('#AAResidentName,#UIRResidentInvolved1,#ANSResidentName,#RMMRoomNo').val($("#txtResidentName").val());
                            $('#AADOB,#ANSDOB').val($('#txtBirthdate').val());
                            $('#AARoomNo, #AARoomNo2').val($('#DRoomId option:selected').text());
                            $('#AASSNo,#AASSNo2,#IESocialSecurityNumberField').val($('#txtSSN').val());
                            $('#AAAdmissionDate,#UIRDateAdmission1,#AAAdmissionDate2').val($('#txtAdmissionDate').val());

                            var age = getAge($('#txtBirthdate').val());
                            $('#ANSAge,#ANSAge2,#IEAgeField,#residentAgeField,#applicantage,#RESAPPapplicantage,#UIRAge1').val(age);

                            
                            $('#AATotalRate2').val($('#AATotalRate').val());
                            $('#AATotalRate').off('change').on('change', function () {
                                $('#AATotalRate2').val(this.value);
                            });

                            //added this 11-09-22: Angelie
                            if ($('#AATMRate').val() == "") {
                                $('#AATMRate').val(0);
                                $('#AAOpServices').val(0);
                                $('#AAMonthlyRate').val(0);
                            }


                            if ($('#ddlGender').val() == 'M') {
                                $('#ANSMale').prop('checked', true);
                                $('#ANSFemale').prop('checked', false);
                                $('#IEGenderField,#diagSexField,#UIRSex1').val('M');
                            } else if ($('#ddlGender').val() == 'F') {
                                $('#ANSMale').prop('checked', false);
                                $('#ANSFemale').prop('checked', true);
                                $('#IEGenderField,#diagSexField,#UIRSex1').val('F');
                            }

                            if ($("#physicianNameField").val() == '') {
                                $("#physicianNameField").val($('#txtPCPhysician').val());
                            }
                            if ($("#physicianPhoneField").val() == '') {
                                $("#physicianPhoneField").val($('#txtRPhysicianPhone').val());
                            }

                            $(".PCFacilityName").text($("#TDNfacilityName").val());
                            $(".PCFacilityAddress").text($("#TDNfacilityAddress").val());
                            $(".PCResName").text($("#txtResidentName").val());

                            //------ EO autopopulate
                            var tab_id;
                            var fid = $(this);
                            var form_id = fid.attr('docname');
                            $("#printAdmissionFile").attr("docname", form_id);
                           
                            //if (form_id == "AAform") {
                            //    if (agency_ishomecare == "False") {
                            //        $(".fmodal-title").html("Admission Agreement");
                            //    }
                            //    else {
                            //        $(".fmodal-title").html("Contract Agreement");
                            //    }
                            //        tab_id = "#tabs-4a";
                            //        tabindex_adm = "2a";
                            // }      
                            if (form_id == "AAform") {
                                $(".fmodal-title").html("Admission Agreement");
                                tab_id = "#tabs-4a";
                                tabindex_adm = "2a";
                            }
                            else if (form_id == "EIform") {
                          
                                $(".fmodal-title").html("Identification and Emergency Information");
                                tab_id = "#tabs-11a";
                                tabindex_adm = "2b";
                            } else if (form_id == "PRform") {
                                $(".fmodal-title").html("Physician's Report for Residential Care Facilities for the Elderly (RCFE)");
                                tab_id = "#tabs-7a";
                                tabindex_adm = "2c";
                            } else if (form_id == "RMIform") {
                                $(".fmodal-title").html("Release of Client/Resident Medical Information");
                                tab_id = "#tabs-9a";
                                tabindex_adm = "2d";
                            } else if (form_id == "CMEform") {
                                $(".fmodal-title").html("Consent to Medical Examination");
                                tab_id = "#tabs-13a";
                                tabindex_adm = "2e";
                            } else if (form_id == "CETform") {
                                $(".fmodal-title").html("Consent for Emergency Medical Treatment");
                                tab_id = "#tabs-14a";
                                tabindex_adm = "2f";
                            } else if (form_id == "PAform") {
                                $(".fmodal-title").html("Preplacement Appraisal Information <br> Admission - Residential Care Facilities");
                                tab_id = "#tabs-8a";
                                tabindex_adm = "2g";
                            } else if (form_id == "RAform") {
                                $(".fmodal-title").html("Resident Appraisal <br> Residential Care Facilities For The Elderly");
                                tab_id = "#tabs-12a";
                                tabindex_adm = "2h";
                            } else if (form_id == "NSPform") {
                                $(".fmodal-title").html("Appraisal/Needs and Service Plan");
                                tab_id = "#tabs-3a";
                                tabindex_adm = "2i";
                            } else if (form_id == "PRSform") {
                                $(".fmodal-title").html("Personal Rights <br> Residential Care Facilities for the Elderly");
                                tab_id = "#tabs-15a";
                                tabindex_adm = "2j";
                            } else if (form_id == "TDNform") {
                                $(".fmodal-title").html("Telecomunication Device Notification");
                                tab_id = "#tabs-10a";
                                tabindex_adm = "2k";
                            } else if (form_id == "PPRform") {
                                $(".fmodal-title").html("Personal Property Record");
                                tab_id = "#tabs-17a";
                                tabindex_adm = "2l";
                            } else if (form_id == "RBTLform") {
                                $(".fmodal-title").html("RB Theft and Loss Policy");
                                tab_id = "#tabs-17a";
                                tabindex_adm = "2m";
                            } else if (form_id == "RMDTform") {
                                $(".fmodal-title").html("Right Re: Med Decision/ Treatment");
                                tab_id = "#tabs-19a";
                                tabindex_adm = "2n";
                            } else if (form_id == "UIRform") {
                                $(".fmodal-title").html("Unusual Incident/Injury Report");
                                tab_id = "#tabs-16a";
                                tabindex_adm = "2o";
                            } else if (form_id == "RWRform") {
                                $(".fmodal-title").html("Resident Weight Record (Binder)");
                                tab_id = "#tabs-17a";
                                tabindex_adm = "2p";
                            } else if (form_id == "CSMform") {
                                $(".fmodal-title").html("Centrally Stored Medication (Book)");
                                tab_id = "#tabs-17a";
                                tabindex_adm = "2q";
                            } else if (form_id == "PCform") {
                                $(".fmodal-title").html("Photographic Consent");
                                tab_id = "#tabs-18a";
                                tabindex_adm = "2r";
                            }
                            $("#files_modal").modal("toggle");
                            $(prev_tab).hide();
                            $(tab_id).show();
                            prev_tab = tab_id;
                            if (form_id == "PCform") {
                                $("#btnSaveAdmission").hide();
                            } else {
                                $("#btnSaveAdmission").show();
                            }
                            //--------------------------------
                            if (tab_id != "#tabs-16a") {
                                $("#btnSaveAdmission").attr("disabled", false);
                            }
                            if (tab_id == "#tabs-3a") {
                                //appraisal needs and service instance
                                if ($('#ANSAppraisalID').val() == '') {
                                    $('#ANSAppraisalDate').val(ALC_Util.CurrentDate).show();
                                    $("#ANSDate").val(ALC_Util.CurrentDate); //replicate date from appraisal date
                                    $('#SelectAppraisalDate').hide();
                                    $('#btnNewAppraisal').hide();

                                    $('#ANSAppraisalDate').change(function () {
                                        $("#ANSDate").val($("#ANSAppraisalDate").val());
                                    });
                                } else {
                                    //load selection appraisal dates    
                                    $('#ANSAppraisalDate').hide();
                                    $('#btnNewAppraisal').show();
                                    $('#SelectAppraisalDate').html('');
                                    if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                        return;
                                    getappraisaldates();
                                }
                            }
                            if (tab_id == "#tabs-4a") {
                                //admission agreement instance
                                $('#AAResidentName').val($("#txtResidentName").val());
                                $('#AADOB').val($('#txtBirthdate').val());
                                $('#AARoomNo,#AARoomNo2').val($('#DRoomId option:selected').text());
                                $('#AASSNo').val($('#txtSSN').val());
                                $('#AAAdmissionDate').val($('#txtAdmissionDate').val());

                                if ($('#AAAgreementID').val() == '') {
                                    $('#AAAgreementDate').val(ALC_Util.CurrentDate).show();
                                    $('#SelectAgreementDate').hide();
                                    $('#btnNewAgr').hide();
                                } else {
                                    //load selection appraisal dates    
                                    $('#AAAgreementDate').hide();
                                    $('#btnNewAgr').show();
                                    $('#SelectAgreementDate').html('');
                                    if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                        return;
                                    getaggreementdates();
                                }
                            }

                            if (tab_id == "#tabs-16a") {
                                //angelie here
                                $("#dtlabel").hide();
                                $("#selectDateOccured").hide();
                                $("#printUIR").hide();
                                $("#addNewUIR").show();
                                $("#btnSaveAdmission").attr("disabled", true);
                                // $("#uirForm textarea, input").attr("disabled", true);
                                // $("#uirForm input").prop("disabled", true);
                                $('#uirForm').find('input, textarea').attr('disabled', 'disabled');
                                $("#resident_id").val(currentId);
                                var resid = $("#resident_id").val();
                                var uirid = "";
                                var fac = $("#UIRNameOfFacility").val();
                                var facnum = $("#UIRFacilityFileNumber").val();
                                var addr = $("#UIRAddress").val();
                                var telnum = $("#UIRTelephoneNumber").val();
                                var csz = $("#UIRCityStateZip").val();
                                var resemail = $("#UIRResponsiblePerson").val();
                                var res = $("#UIRResidentInvolved1").val();
                                var age = $("#UIRAge1").val();
                                var sex = $("#UIRSex1").val();
                                var dtadmission = $("#UIRDateAdmission1").val();

                                $('#selectDateOccured').html('');
                                if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                    return;

                                getDateOccured($('#txtAdmissionId').val());

                                $('#addNewUIR').on('click', function () {
                                    // uirid = $(this).val();
                                    $("#printUIR").hide();
                                    //$("#tabs-16a input, textarea").removeAttr("disabled");
                                    $('#uirForm').find('input, textarea').removeAttr('disabled');
                                    // $(".ui-dialog-buttonpane button:contains('Save')").attr("disabled", false).removeClass("ui-state-disabled");
                                    $("#btnSaveAdmission").attr("disabled", false);
                                    $("#selectDateOccured").attr("disabled", true);

                                    $('#tabs-16a').clearFields();
                                    $("#UIRNameOfFacility").val(fac);
                                    $("#UIRFacilityFileNumber").val(facnum);
                                    $("#UIRAddress").val(addr);
                                    $("#UIRTelephoneNumber").val(telnum);
                                    $("#UIRCityStateZip").val(csz);
                                    $("#UIRResponsiblePerson").val(resemail);
                                    $("#UIRResidentInvolved1").val(res);
                                    $("#UIRAge1").val(age);
                                    $("#UIRSex1").val(sex);
                                    $("#UIRDateAdmission1").val(dtadmission);
                                    $("#UIRNameOfFacility").attr("disabled", "disabled");
                                    $("#UIRFacilityFileNumber").attr("disabled", "disabled");
                                    $("#UIRTelephoneNumber").attr("disabled", "disabled");
                                    $("#UIRCityStateZip").attr("disabled", "disabled");
                                    $("#UIRResponsiblePerson").attr("disabled", "disabled");
                                    $("#UIRResidentInvolved1").attr("disabled", "disabled");
                                    $("#UIRAge1").attr("disabled", "disabled");
                                    $("#UIRSex1").attr("disabled", "disabled");
                                    $("#UIRDateAdmission1").attr("disabled", "disabled");
                                    // $('#tabs-16a').click();

                                });
                                $('#printUIR').on('click', function () {
                                    temp_id = $("#selectDateOccured").val();
                                    //  window.open(alc_uri.admin + "/printincidentreport?id=" + temp_id, '_blank');
                                    window.open(ALC_URI.Admin + "/PrintIncidentReport?id=" + temp_id, '_blank');
                                });
                                $('#selectDateOccured').on('change', function () {
                                    var _uirid = $(this).val();
                                    // $('#UIRDateOccuredId').val($(this).val());
                                    $('#tabs-16a #datatable').clearFields();
                                    $.get(ALC_URI.GuestNotification + "/GetGridData?func=incident_report&param=" + _uirid + "&param2=" + resid + "&param3=" + 1, function (dt) {
                                        //console.log(dt);
                                        $('#tabs-16a #content-admission #datatable').mapJson(dt[0], 'id');

                                        $("#UIRDateOccuredId").val(_uirid);
                                        $("#addNewUIR").val($("#UIRDateOccuredId").val());
                                        $("#printUIR").val(_uirid);
                                        //$('#selectDateOccured option:selected').text($("#UIRDateOccured1").val());
                                        // $("#tabs-16a input").attr("disabled", "disabled");
                                        $('#uirForm').find('input, textarea').attr('disabled', 'disabled');
                                    });
                                });


                            };
                            //---------------------------------
                            admFormState = $('#files_modal .filesdiv').extractJson('id');
                        });

                    }
                    if (index == 3) {
                        var mov = $('#RMMMoveInDate').val();
                        var assD = $('#AssMoveInDate').val();

                        if (mov == '') {
                            $('#RMMMoveInDate').val(assD);
                        }
                    }

                    if (index == 5) {
                        $('#assPager').show();
                        var mov = $('#RMMMoveInDate').val();
                        if (mov != '') {
                            $('#AssMoveInDate').val(mov);
                        }
                        //if ($('#AssessmentId').val() == '')
                        $(".ui-dialog-buttonpane button:contains('Save')").attr("disabled", true)
                        .addClass("ui-state-disabled");
                        //else
                        //    $(".ui-dialog-buttonpane button:contains('Save')").removeAttr("disabled", true)
                        //    .removeClass("ui-state-disabled");
                    } else {
                        $('#assPager').hide();
                    }

                    if (index == 2) {
                        setHeight();
                        $("#admForm .adm-content").css("overflow-y", "unset");
                        $('#tabs-ass form').css("overflow", "hidden auto");
                    } else {
                        setHeight();
                        $("#admForm .adm-content").css("overflow-y", "unset");
                    }
                }

            });
            $("#admForm").tabs("option", "selected", 0);
            //$("#admForm").tabs('option', 'disabled', [1, 2, 3, 4]);
            //var diag = $('.ui-dialog-content');
            //diag = diag.find('#admForm').parents('.ui-dialog-content');
            //var height = 0;
            //height = diag.eq(0).height() || 0;
            //if (height <= 0)
            //    height = diag.eq(1).height() || 0;
            //$("#AdmissionContainer,#admForm").height(height);
            //$('#AdmissionContent').height(height - 20);
            //$('#admForm').find('#tabs-1,#tabs-2,#tabs-3,#tabs-4,#tabs-5').height(height - 30);

            //load assessment tab        
           
            $.ajax({                
                url: "Admin/GetNodeData",
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                type: "POST",
                data: '{id:"assessmentwizard_0"}',
                success: function (d) {
                    $("#tabs-6a").empty();
                    $("#tabs-6a").html(d);
                    $('#update-status').hide();
                    $.ajax({
                        url: ALC_URI.Admin + "/GetNodeData",
                        contentType: "application/json; charset=utf-8",
                        dataType: "html",
                        type: "POST",
                        data: '{id:"morsefallscale_0"}',
                        success: function (d) {
                            $("#mfs-container").empty();
                            $("#mfs-container").html(d);
                        },
                        complete: function () { },
                        error: function () { }
                    });
                },
                complete: function () { },
                error: function () { }
            });
        }
    });

    $(function () {
        $("#txtSSN,#AASSNo,#AASSNo2").mask("999-99-9999");
        $("#txtReligiousAdvisorTelephone,#txtResponsiblePersonTelephone,#txtNearestRelativeTelephone,#txtRPharmacyPhone,#txtRPhysicianPhone,#frPhone").mask("(999)999-9999");
        //$("#residentForm").tabs();
        $("#txtBirthdate,#txtDateAdmitted").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
            onClose: function () {
                if (this.id == 'txtDateAdmitted') {
                    $('#AAAdmissionDate,#txtAdmissionDate').val(this.value);
                } else if (this.id == 'txtDateLeft') {
                    $('#txtDischargeDate').val(this.value);
                }
            }
        }).on('changeDate', function () {
            $(this).datepicker('hide');
        }); //("option", "dateFormat", "mm/dd/yy");

    });

    //var createTabGrids = function (mode) {
    //    debugger;
    //    var lastRow = null;
    //    if ($('#fGrid')[0] && $('#fGrid')[0].grid)
    //        $.jgrid.gridUnload('fGrid');
    //    var FGrid = $('#fGrid');

    //    FGrid.jqGrid({
    //        datatype: 'local',
    //        //data: [{ RId: '1', Name: 'Harold Brunner', Address: '123 AB St. 2nd Ave Los Amigos Mexico', Telephone: '(234)123233', E: false }],
    //        colNames: ['RId', 'Name', 'Address', 'Telephone', 'E', 'D'],
    //        colModel: [{
    //            key: true,
    //            hidden: true,
    //            name: 'RId',
    //            index: 'RId'
    //        },
    //            {
    //                key: false,
    //                name: 'Name',
    //                index: 'Name',
    //                width: 200,
    //                editable: true,
    //                edittype: 'text'
    //            },
    //            {
    //                key: false,
    //                name: 'Address',
    //                index: 'Address',
    //                width: 300,
    //                editable: true,
    //                edittype: 'text'
    //            },
    //            {
    //                key: false,
    //                name: 'Telephone',
    //                index: 'Telephone',
    //                width: 100,
    //                editable: true,
    //                edittype: 'custom',
    //                editoptions: {
    //                    custom_element: function (value, options) {
    //                        var el = document.createElement("input");
    //                        el.type = "text";
    //                        el.value = value;
    //                        $(el).mask("(999)999-9999");
    //                        return el;
    //                    },
    //                    custom_value: function (elem, operation, value) {
    //                        if (operation === 'get') {
    //                            return $(elem).val();
    //                        } else if (operation === 'set') {
    //                            $('input', elem).val(value);
    //                        }
    //                    }
    //                }
    //            },
    //            {
    //                key: false,
    //                hidden: true,
    //                name: 'E',
    //                index: 'E',
    //                edittype: 'text'
    //            },
    //            {
    //                key: false,
    //                hidden: true,
    //                name: 'D',
    //                index: 'D',
    //                edittype: 'text'
    //            }
    //        ],
    //        //pager: jQuery('#pager'),
    //        rowNum: 1000000,
    //        rowList: [],
    //        pgbuttons: false,
    //        pgtext: null,
    //        height: '100%',
    //        //cellEdit: true,
    //        viewrecords: false,
    //        //caption: 'Care Staff',
    //        emptyrecords: 'No records to display',
    //        //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
    //        autowidth: true,
    //        multiselect: false,
    //        editurl: 'clientArray',
    //        cellsubmit: 'clientArray',
    //        beforeSelectRow: function (rowid, e) {
    //            if (FGrid.attr('lastRow') != 'undefined') {
    //                lastRow = FGrid.attr('lastRow');
    //            }

    //            if (lastRow != rowid) {
    //                FGrid.jqGrid('saveRow', lastRow);
    //                var row = FGrid.jqGrid('getRowData', lastRow);
    //                row.E = true;
    //                FGrid.jqGrid('setRowData', lastRow, row);
    //            }

    //            var attr = $('#fGrid tr[id=' + rowid + ']').attr('editable');
    //            if (typeof attr === typeof undefined || attr == 0)
    //                FGrid.jqGrid('editRow', rowid);

    //            FGrid.attr('lastRow', rowid);
    //        }
    //    });

    //    if ($('#oGrid')[0] && $('#oGrid')[0].grid)
    //        $.jgrid.gridUnload('oGrid');
    //    var oGrid = $('#oGrid');

    //    oGrid.jqGrid({
    //        datatype: 'local',
    //        //data: [{ RId: '1', Name: 'Shanon Stoney', Address: '422 John St. 2nd Ave. West Harbor', Telephone: '(234)856867', E:false }],
    //        colNames: ['RId', 'Name', 'Address', 'Telephone', 'E', 'D'],
    //        colModel: [{
    //            key: true,
    //            hidden: true,
    //            name: 'RId',
    //            index: 'RId'
    //        },
    //            {
    //                key: false,
    //                name: 'Name',
    //                index: 'Name',
    //                width: 200,
    //                editable: true,
    //                edittype: 'text'
    //            },
    //            {
    //                key: false,
    //                name: 'Address',
    //                index: 'Address',
    //                width: 300,
    //                editable: true,
    //                edittype: 'text'
    //            },
    //            {
    //                key: false,
    //                name: 'Telephone',
    //                index: 'Telephone',
    //                width: 100,
    //                editable: true,
    //                edittype: 'custom',
    //                editoptions: {
    //                    custom_element: function (value, options) {
    //                        var el = document.createElement("input");
    //                        el.type = "text";
    //                        el.value = value;
    //                        $(el).mask("(999)999-9999");
    //                        return el;
    //                    },
    //                    custom_value: function (elem, operation, value) {
    //                        if (operation === 'get') {
    //                            return $(elem).val();
    //                        } else if (operation === 'set') {
    //                            $('input', elem).val(value);
    //                        }
    //                    }
    //                }
    //            },
    //            {
    //                key: false,
    //                hidden: true,
    //                name: 'E',
    //                index: 'E'
    //            },
    //            {
    //                key: false,
    //                hidden: true,
    //                name: 'D',
    //                index: 'D'
    //            }
    //        ],
    //        pager: jQuery('#pager'),
    //        rowList: [],
    //        pgbuttons: false,
    //        pgtext: null,
    //        rowNum: 1000000,
    //        //rowList: [10, 20, 30, 40],
    //        height: '100%',
    //        viewrecords: false,
    //        //caption: 'Care Staff',
    //        emptyrecords: 'No records to display',
    //        //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
    //        autowidth: true,
    //        multiselect: false,
    //        editurl: 'clientArray',
    //        cellsubmit: 'clientArray',
    //        beforeSelectRow: function (rowid, e) {
    //            if (oGrid.attr('lastRow') != 'undefined') {
    //                lastRow = oGrid.attr('lastRow');
    //            }
    //            if (lastRow != rowid) {
    //                oGrid.jqGrid('saveRow', lastRow);
    //                var row = $('#oGrid').jqGrid('getRowData', lastRow);
    //                row.E = true;
    //                oGrid.jqGrid('setRowData', lastRow, row);
    //            }
    //            var attr = $('#oGrid tr[id=' + rowid + ']').attr('editable');
    //            if (typeof attr === typeof undefined || attr == 0)
    //                oGrid.jqGrid('editRow', rowid);

    //            oGrid.attr('lastRow', rowid);
    //        }
    //    });

    //    $("#fGrid").jqGrid('bindKeys');
    //    $("#oGrid").jqGrid('bindKeys');
    //}


    $('#printsr').change(function () {
        var dis = $(this);
        if (dis.val() == 'ans') {
            $('#trAppraisaldate').show();
        } else {
            $('#trAppraisaldate').hide();
        }

        if (dis.val() == 'aa') {
            $('#trAgreementdate').show();
        } else {
            $('#trAgreementdate').hide();
        }
        if (dis.val() == 'ass') {
            $('#trAssessmentdate').show();
        } else {
            $('#trAssessmentdate').hide();
        }


    })


    $("#diagResForms").dialog({

        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Resident Files"

    });


    $(".printResForm").click(function () {

        var func = $(this).attr("id");

        window.open(ALC_URI.Admin + "/" + func + "?id=" + $('#printResId').val(), '_blank');

    });


    $('#btn-printsr').click(function (e) {
        var prt = $('#printsr').val();
        if (prt == null || prt == '') {
            swal('Please select print out first and try again.');
            return false;
        }
        if ($('#admForm #txtAdmissionId').val()) {
            if (prt == "dc") {
                window.open(ALC_URI.Admin + "/PrintDocumentChecklist?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "ans") {
                var apprid = $('#printappraisaldate').val();
                if (apprid == null || apprid == '') {
                    //var create = confirm("Sorry, we are unable to print the document. There are no \"Appraisal needs and services plan worksheet\" created yet for this resident.Would you like to create one? If yes, press \"Ok\ to continue.");
                    //if (create) {
                    //    $("#residentForm").tabs("option", "selected", 3);
                    //    $("#admForm").tabs("option", "selected", 2);
                    //}
                    //return false;
                    setTimeout(function () {
                        swal({
                            title: "Sorry, we are unable to print the document. There are no \"Appraisal needs and services plan worksheet\" created yet for this resident. Would you like to create one? If yes, press \"Ok\ to continue.",
                            text: "",
                            type: "warning",
                            showCancelButton: !0,
                            confirmButtonText: "Ok"
                        }).then(function (e) {
                            if (e.value == true) {
                                $("#residentForm").find("ul li #ui-id-6").click();
                                $("#admForm").find("#tabs ul li #ui-id-2").click();
                                $("#tabs-2a").find("#NSPform").click();
                            }

                        });
                    }, 100)
                    return false;
                } else {
                    window.open(ALC_URI.Admin + "/PrintAppraisalNeedAndService?id=" + $('#admForm #txtAdmissionId').val() + '&id2=' + apprid, '_blank');
                    return false;
                }

            } else if (prt == "aa") {
                var admaggrid = $('#printaggreementdate').val();
                if (admaggrid == null || admaggrid == '' || admaggrid == "undefined") {
                    // var create = confirm("Sorry, we are unable to print the document. There are no \"Admission agreement\" created yet for this resident. Would you like to create one? If yes, press \"Ok\ to continue.");
                    //if (create)
                    // $("#residentForm,#admForm").tabs("option", "selected", 3);
                    setTimeout(function () {
                        swal({
                            title: "Sorry, we are unable to print the document. There are no \"Admission agreement\" created yet for this resident. Would you like to create one? If yes, press \"Ok\ to continue.",
                            text: "",
                            type: "warning",
                            showCancelButton: !0,
                            confirmButtonText: "Ok"
                        }).then(function (e) {
                            if (e.value == true) {
                                $("#residentForm").find("ul li #ui-id-6").click();
                                $("#admForm").find("#tabs ul li #ui-id-2").click();
                                $("#tabs-2a").find("#AAform").click();
                            }

                        });
                    }, 100)
                    return false;

                } else {
                    window.open(ALC_URI.Admin + "/PrintAdmissionAgreement?id=" + $('#admForm #txtAdmissionId').val() + '&id2=' + admaggrid, '_blank');
                    return false;
                }

            } else if (prt == "rmi") {
                window.open(ALC_URI.Admin + "/PrintMovein?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "pai") {
                window.open(ALC_URI.Admin + "/PrintPreplacement?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "pr") {
                window.open(ALC_URI.Admin + "/PrintPhysicianReport?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "romi") {
                window.open(ALC_URI.Admin + "/PrintReleaseMedicalInfo?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "tdn") {
                window.open(ALC_URI.Admin + "/PrintTelecommunicationNotif?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "ra") {
                window.open(ALC_URI.Admin + "/PrintResidentAppraisal?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "cme") {
                window.open(ALC_URI.Admin + "/PrintConsentMedExam?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "cmt") {
                window.open(ALC_URI.Admin + "/PrintConsentMedicalTreatment?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "iei") {
                window.open(ALC_URI.Admin + "/PrintResidentIdentificationEmergency?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "per") {
                window.open(ALC_URI.Admin + "/PrintPersonalRights?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "dr") {
                window.open(ALC_URI.Admin + "/PrintDecisionRights?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            } else if (prt == "pc") {
                window.open(ALC_URI.Admin + "/PrintPhotographyConsent?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return false;
            }

        } else {
            swal('Resident has no Admission');
            return false;
        }
        if ($("#AssessmentId").val()) {
            if (prt == "mfs") {
                window.open(ALC_URI.Marketer + "/PrintMorseFallScale?id=" + $("#AssessmentId").val(), '_blank');
                return false;
            } else if (prt == "ass") {
                var assgrid = $('#printassessmentdate').val();
                window.open(ALC_URI.Marketer + "/PrintAssessment?id=" + assgrid, '_blank');
                return false;
            }
        } else {
            swal('Resident has no Assessment');
            return false;
        }
    });

    $('#tabs-2,#tabs-3').click(function (e) {
        var G = '#fGrid';
        if (this.id == 'tabs-3') G = '#oGrid';
        var trgt = $(e.target);
        if (trgt.attr('role') == 'gridcell' || trgt.is('.add') || trgt[0].id.indexOf('jqg') == 0 || trgt.hasClass('editable') || trgt.hasClass('customelement')) return true;
        var editRow = $(G + ' tr[editable=1]');
        if (editRow.length > 0) {
            editRow.each(function (i) {
                $(G).jqGrid('saveRow', this.id);
                var row = $(G).jqGrid('getRowData', this.id);
                row.E = true;
                $(G).jqGrid('setRowData', this.id, row);
            });
        }
    });

    $('#residentGridToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('#residentForm').clearFields();
            showdiag(1);
            $('.lnkUpload,.resimg').hide();
        } else if (q.is('.del')) {

            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to discharge the selected resident?')) {
                    var row = $('#grid').jqGrid('getRowData', id);

                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);

                        if (row) {
                            doAjax(6, {
                                Id: row.Id
                            }, function (m) {
                                var msg = row.Name + " has been discharged!";
                                fromSort = true;
                                new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                $('#grid').setGridParam({
                                    datatype: 'json'
                                }).trigger('reloadGrid');
                                if (m.result == -3) //inuse
                                    msg = row.Name + " cannot be discharged because it is currently in use.";
                                $.growlSuccess({
                                    message: msg,
                                    delay: 6000
                                });
                                $('#grid').trigger('reloadGrid');

                            });
                        }
                    }, 100);
                }

            } else {
                swal("Please select resident to discharge.")
            }
        } else if (q.is('.admres')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to Admit selected Resident?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(5, {
                                Id: row.Id
                            }, function (m) {
                                var msg = row.Firstname + ' ' + row.Lastname + " admitted Successfully!";
                                $('#grid').trigger('reloadGrid');

                                if (m.result == -2) //inuse
                                    msg = "ERROR! " + row.Firstname + ' ' + row.Lastname + " is already Admitted.";
                                $.growlSuccess({
                                    message: msg,
                                    delay: 6000
                                });
                            });
                        }
                    }, 300);
                }

            } else {
                swal("Please select row to delete.")
            }
        } else if (q.is('.upload')) {
            $("#ImageData").val("");
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            $('#resid').val(id);
            var row = $('#grid').jqGrid('getRowData', id);
            var title = "Upload image of " + row.Name;

            $('#diagupload').dialog({
                title: title
            }).dialog('open');
        }
    });

    $('#financialGridToolbar,#otherGridToolbar').on('click', 'a', function () {
        var g = $(this).parents('div.toolbar')[0].id == 'financialGridToolbar' ? '#fGrid' : '#oGrid';
        var q = $(this);
        var G = $(g);
        if (q.is('.add')) {
            var editRow = $(g + ' tr[editable=1]');
            if (editRow.length > 0) {
                editRow.each(function (i) {
                    G.jqGrid('saveRow', this.id);
                    var row = $(G).jqGrid('getRowData', this.id);
                    row.E = true;
                    $(G).jqGrid('setRowData', this.id, row);
                });
            }
            G.jqGrid('addRow', "new");
            G.attr('lastRow', G.jqGrid('getGridParam', 'selrow'));
        } else if (q.is('.del')) {
           
            var id = G.jqGrid('getGridParam', 'selrow');
            if (id) {

                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = G.jqGrid('getRowData', id);
                        if (row) {
                       
                            row.D = 'true';
                            $(G).jqGrid('setRowData', id, row);
                            $(g + ' tr[id=' + id + ']').addClass("not-editable-row").hide();
                            //G.jqGrid('delGridRow', id, {
                            //    afterShowForm: function ($form) {
                            //        $("#dData", $form.parent()).click();
                            //    }
                            //});
                        }
                    }, 100);
                }

            } else {
                swal("Please select row to delete.")
            }
        }
    });


    $(function () {
        //var dataArray = [
        //    { Id: '1', Description: 'Supply 101', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFFO09', Status: '1', Cost: '655.20', Type: '1' },
        //    { Id: '2', Description: 'Supply 111', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFDO03', Status: '0', Cost: '231.20', Type: '2' },
        //    { Id: '3', Description: 'Supply 201', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFFO02', Status: '1', Cost: '112.20', Type: '1' },
        //    { Id: '4', Description: 'Supply 301', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EF4G01', Status: '0', Cost: '432.20', Type: '2' },
        //    { Id: '5', Description: 'Supply 401', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFRS55', Status: '1', Cost: '211.20', Type: '1' },

        //];

        //this is in site.layout.js.
        //var GURow = new Grid_Util.Row();

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=resident&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: {
                contentType: "application/json",
                cache: false
            },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',

            //'Admission Date' column added by Mariel P. on 6/20/17
            colNames: ['Id', '', 'Resident', 'Resident', 'MiddleInitial', 'Lastname', 'Service Schedule of the Day', 'RoomId', 'FullName', 'SSN', 'Birthdate', 'Admission Date', 'Resident Status', 'isAdmitted'],
            colModel: [{
                key: true,
                hidden: true,
                name: 'Id',
                index: 'Id'
            },
                {
                    key: false,
                    name: 'Image',
                    index: 'Image',
                    fixed: true,
                    formatter: imageFormatter,
                    cellattr: setTitle,
                    width: 150
                },
                {
                    key: false,
                    name: 'Firstname',
                    index: 'Firstname',
                    formatter: "dynamicLink",
                    formatter: dataFormatter,
                    cellattr: setTitle,
                    formatoptions: {
                        cellValue: function (cellValue, rowId, rowData, options) {
                            var name = [];
                            if (cellValue != "") name.push(cellValue);
                            if (rowData != null) {
                                if (rowData.MiddleInitial && rowData.MiddleInitial != '') {
                                    name.push(" " + rowData.MiddleInitial.toUpperCase() + '.');
                                }
                                if (rowData.Lastname && rowData.Lastname != '') name.push(" " + rowData.Lastname);
                            }
                            return name.join('');
                        }
                        //,
                        //onClick: function (rowid, iRow, iCol, cellText, e) {
                        //    var G = $('#grid');
                        //    G.setSelection(rowid, false);
                        //    $('#residentForm .resForm').clearFields();
                        //    var row = G.jqGrid('getRowData', rowid);
                        //    if (row) {
                        //        doAjax(0, row, function (res) {
                        //            $('#residentForm .resForm').mapJson(res);
                        //            showdiag(2);
                        //            var title = $("#diag").dialog("option", 'title');
                        //            $("#diag").dialog("option", 'title', 'Resident - ' + cellText);
                        //            $('#fGrid').jqGrid('setGridParam', { data: res.FGrid }).trigger('reloadGrid');
                        //            $('#oGrid').jqGrid('setGridParam', { data: res.OGrid }).trigger('reloadGrid');
                        //            loadAssessment(row);

                        //        });
                        //        $("#admForm").tabs("option", "selected", 0);
                        //        doAjaxAdm(0, row, function (res) {
                        //            $("#admForm").clearFields();
                        //            $("#admForm").mapJson(res, 'id', "#admForm");
                        //            $('#txtResidentName,#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val(res.txtResidentName);
                        //            $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                        //            setTimeout(function () {
                        //                $('#txtDateAdmitted').val($('#txtAdmissionDate').val());
                        //                $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#DRoomId option:selected').text());
                        //                $('#AADOB,#ANSDOB').val($('#txtBirthdate').val());
                        //                $('#AASSNo').val($('#txtSSN').val());
                        //                var mov = $('#RMMMoveInDate').val();
                        //                if (mov != '') {
                        //                    $('#AssMoveInDate').val(mov);
                        //                }

                        //                if ($('#ANSAppraisalID').val() == '') {
                        //                    $('#ANSAppraisalDate').val(ALC_Util.CurrentDate).show();
                        //                    $('#SelectAppraisalDate').hide();
                        //                    $('#btnNew').hide();
                        //                } else {
                        //                    //load selection appraisal dates    
                        //                    $('#ANSAppraisalDate').hide();
                        //                    $('#btnNew').show();
                        //                    $('#SelectAppraisalDate').html('');
                        //                    if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                        //                        return;
                        //                    $.get(ALC_URI.Admin + "/GetAppraisalNSDates?id=" + $('#txtAdmissionId').val(), function (data) {
                        //                        $.each(data, function () {
                        //                            $('#SelectAppraisalDate').append($('<option>', { value: this.id, text: this.date }));
                        //                        });
                        //                        $('#SelectAppraisalDate').val($('#ANSAppraisalID').val()).show();
                        //                        $('#SelectAppraisalDate').off('change').on('change', function () {
                        //                            $('#ANSAppraisalID').val($(this).val());
                        //                            $.get(ALC_URI.Admin + "/GetAppraisalNS?id=" + $(this).val(), function (dt) {
                        //                                $('#tabs-3a section').clearFields();
                        //                                $('#ANSResidentName').val(res.txtResidentName);
                        //                                $('#tabs-3a section').mapJson(dt, 'id');
                        //                            });
                        //                        });
                        //                        $('#btnNew').off('click').on('click', function() {
                        //                            showdiagNewAppraisal();
                        //                        });
                        //                    });

                        //                }
                        //            }, 100);
                        //        });
                        //    }
                        //}
                    }
                },
                {
                    key: false,
                    hidden: true,
                    name: 'Resident2',
                    index: 'Firstname',
                    formatter: "dynamicLink",
                    formatter: dataFormatter2
                },
                {
                    key: false,
                    hidden: true,
                    name: 'MiddleInitial',
                    index: 'MiddleInitial'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'Lastname',
                    index: 'Lastname'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'RoomName',
                    index: 'RoomName',
                    formatter: admissionFormatter,
                    cellattr: setTitle
                },
                {
                    key: false,
                    hidden: true,
                    name: 'RoomId',
                    index: 'RoomId'
                },
                //  {
                //    key: false, name: 'Gender', index: 'Gender', formatter: function (cellValue, rowId, rowData, options) {
                //        if (cellValue == "M") return "Male";
                //        return "Female";
                //  }
                //},
                {
                    key: false,
                    hidden: true,
                    name: 'Name',
                    index: 'Name',
                },
                {
                    key: false,
                    name: 'SSN',
                    index: 'SSN',
                    hidden: true,
                },
                {
                    key: false,
                    name: 'Birthdate',
                    index: 'Birthdate',
                    hidden: true,
                },
                {
                    key: false,
                    name: 'AdmissionDate',
                    index: 'AdmissionDate',
                    hidden: true,
                },
                {
                    key: false,
                    name: 'ResidentStatus',
                    index: 'ResidentStatus',
                    hidden: true,
                },
                {
                    key: false,
                    name: 'isAdmitted',
                    index: 'isAdmitted',
                    hidden: true,
                }
            ],
            //pager: jQuery('#pager'),
            gridComplete: function () {
                //for (var i = 0; i < rowsToColor.length; i++) {
                //    var AdmissionStatus = $("#" + rowsToColor[i]).find("td").eq(-1).html();
                //    if (AdmissionStatus == "Move In For Follow Up") {
                //        $("#" + rowsToColor[i]).find("td").css("background-color", "#ff9933");
                //    }
                //}

                var data = {};
                var grid = $('#grid');
                var rows = grid.jqGrid('getDataIDs');
                var rowData;




            },
            scroll: true,
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: {
                root: 'rows',
                page: 'page',
                total: 'total',
                records: 'records',
                repeatitems: false,
                id: '0'
            },
            autowidth: true,
            multiselect: false,
            sortname: 'Name',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                //added by Mariel 

                $(".residentFilebtn, .getHistory").click(function () {
                    //alert("sdfsf");
                    var this_class = $(this).attr("class");

                    var id = $(this).attr("id").split("_");
                    $(".rf_" + id[1]).click();

                    $("#printsr").val("");

                    setTimeout(function () {

                        $("#residentForm").tabs("option", "selected", id[0]);

                    }, 1000);

                });

                $(".imgsrc, .imgname").off('click').click(function (rowid, iRow, iCol, cellText, e) {
                    $(".loader").show();
                    var G = $('#grid');
                    var this_class = $(this).attr("class").split(" ");


                    if (this_class[0] == "imgsrc") {
                        rowid = $(this).parent().parent().attr("id");
                    } else if (this_class[0] == "getHistory" || this_class[0] == "imgname") {
                        rowid = $(this).attr("id");
                    } else {
                        rowid = $(this).parent().parent().parent().attr("id");
                    }

                    G.setSelection(rowid, false);
                    $('#residentForm .resForm').clearFields();
                    var row = G.jqGrid('getRowData', rowid);

                    if (row) {
                        $('.lnkUpload,.resimg').show();
                        doAjax(0, row, function (res) {
                            $(".resdform .resimg").html(row.Image);
                            $(".resimg > .resbtn").hide();
                            $('#residentForm .resForm').mapJson(res);
                            showdiag(2);
                            var title = $("#diag").dialog("option", 'title');
                            $("#diag").dialog("option", 'title', 'Resident - ' + row.Name);
                            $('#fGrid').jqGrid('setGridParam', {
                                data: res.FGrid
                            }).trigger('reloadGrid');
                            $('#oGrid').jqGrid('setGridParam', {
                                data: res.OGrid
                            }).trigger('reloadGrid');
                            //loadAssessment(row);
                        });

                        $("#diag").on("click", ".lnkUpload", function () {
                            $("#lnkUpload").click();
                        })

                        $("#admForm").tabs("option", "selected", 0);
                        doAjaxAdm(0, row, function (res) {
                            $("#admForm").clearFields();
                            $("#admForm").mapJson(res, 'id', "#admForm");
                     
                            //$('#txtResidentName,#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val(res.txtResidentName);
                            loadAssessment(row);
                            $("#content-assessment #CompletedDate").css("background-color", "#f4f5f8");
                            $("#addNewAssessment").on("click", function () {
                               
                                $("#content-assessment #load").attr("hidden", true);
                                $('.m-checkbox-list').find('input[type=checkbox]:checked').remove();
                                $('#content-assessment textarea').val('');
                                $("#content-assessment# label-TotalScore").html(0);
                                //var id = $('#grid').jqGrid('getGridParam', 'selrow');
                                //var row = $('#grid').jqGrid('getRowData', id);
                                //loadAssessment(row);

                                //$(".ui-dialog-buttonpane button:contains('Save')").attr("disabled", false)
                                // .removeClass("ui-state-disabled");
                                $("#content-assessment input").removeAttr("disabled");


                                $('#assPager').show();
                                $("#content-assessment-tab #CompletedDate").val("");
                                $("#content-assessment-tab #CompletedDate").css("background-color", "#FFFFCC");

                            })

                            $("#printAssessment").on("click", function () {

                                alert($("#assessment_list").children("option:selected").val());
                            })

                            //$("#assessment_list").on("change", function () {
                            //    debugger
                            //    var id = $("#assessment_list option:selected").val();

                            //    var data = {
                            //        Id: id,
                            //        isAssessment: true
                            //    };
                            //    loadAssessment(data);
                            //    $(".ui-dialog-buttonpane button:contains('Save')").attr("disabled", true).addClass("ui-state-disabled");
                            //    $("#content-assessment-tab #CompletedDate").css("background-color", "white");
                            //    $("#content-assessment input").attr("disabled", "disabled");
                            //    $('#assessment_list').val(id);

                            //    setTimeout(function () {
                            //        $('#assPager').show();
                            //    }, 1000);

                            //})

                            setTimeout(function () {
                                var fname = $('#txtFirstname').val();
                               // fname.substr(0, 1).toUpperCase() + fname.substr(1);
                                var mname = $('#txtMiddleInitial').val();
                                var lname = $('#txtLastname').val();
                            
                                //var name = fname.trim();
                                if (mname != '' || mname != null) {
                                    var name = lname + ", " + fname + " " + mname;
                                   
                                }
                                if (mname == '' || mname == null) {
                                 var   name = lname + ", " + fname;
                                }
                            
                                //if (lname != '')
                                //name += ' ' + lname.trim();
                               
                                $('#txtResidentName,#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val(name);
                                var g = $('#ddlGender').val()
                                if (g == "M") {
                                    $('#ANSMale').prop('checked', true);
                                }
                                $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                                $('#txtDateAdmitted').val($('#txtAdmissionDate').val());
                                $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#DRoomId option:selected').text());
                                $('#AADOB,#ANSDOB,#residentBirthDateField,#IEDateOfBirthField').val($('#txtBirthdate').val());
                                $('#AASSNo').val($('#txtSSN').val());

                                //$('#AADOB,#ANSDOB').attr('readonly', 'readonly');
                                $('#AADOB,#ANSDOB,#txtBirthdate').off('change').on('change', function () {
                                    var id = this.id;
                                    if (id == 'AADOB') {
                                        $('#ANSDOB,#txtBirthdate,#residentBirthDateField,#IEDateOfBirthField').val(this.value);
                                    } else if (id == 'ANSDOB') {
                                        $('#AADOB,#txtBirthdate,#residentBirthDateField,#IEDateOfBirthField').val(this.value);
                                    } else if (id == 'txtBirthdate') {
                                        $('#AADOB,#ANSDOB,#residentBirthDateField,#IEDateOfBirthField').val(this.value);
                                    }
                                    var age = getAge(this.value);
                                    //$('#ANSAge').val(age);
                                    $('#ANSAge,#ANSAge2,#residentAgeField,#applicantage,#IEAgeField,#RESAPPapplicantage').val(age);
                                });

                                $('#ddlGender').off('change').on('change', function () {
                                    if ($('#ddlGender').val() == 'M') {
                                        $('#ANSMale').prop('checked', true);
                                        $('#ANSFemale').prop('checked', false);
                                        $('#IEGenderField').val('M');
                                    } else if ($('#ddlGender').val() == 'F') {
                                        $('#ANSMale').prop('checked', false);
                                        $('#ANSFemale').prop('checked', true);
                                        $('#IEGenderField').val('F');
                                    }

                                    $('#').val(age);
                                });

                                $('#ANSMale').change(function () {
                                    if (this.checked) {
                                        $(this).prop("checked");
                                        $('#ddlGender').val("M");
                                        $('#IEGenderField').val('M');
                                    }
                                });
                                $('#ANSFemale').change(function () {
                                    if (this.checked) {
                                        $(this).prop("checked");
                                        $('#ddlGender').val("F");
                                        $('#IEGenderField').val('F');
                                    }
                                });

                                $('#txtAdmissionDate,#txtDateAdmitted,#AAAdmissionDate').off('change').on('change', function () {
                                    var id = this.id;
                                    if (id == 'txtDateAdmitted') {
                                        $('#AAAdmissionDate, #txtAdmissionDate').val(this.value);
                                    } else if (id == 'AAAdmissionDate') {
                                        $('#txtDateAdmitted, #txtAdmissionDate').val(this.value);
                                    } else if (id == 'txtAdmissionDate') {
                                        $('#txtDateAdmitted, #AAAdmissionDate').val(this.value);
                                    }
                                });

                                $('#txtPharmacy,#txtPharmacyPhone,#txtPhysicianPhone,#txtPhysician,#txtAllergies,#txtHospital,#txtHospitalAddress').off('change').on('change', function () {
                                    var id = this.id;
                                    if (id == 'txtPharmacy') {
                                        $('#txtRPharmacy').val(this.value);
                                    } else if (id == 'txtPharmacyPhone')
                                        $('#txtRPharmacyPhone').val(this.value);
                                    else if (id == 'txtPhysicianPhone')
                                        $('#txtRPhysicianPhone').val(this.value);
                                    else if (id == 'txtPhysician')
                                        $('#txtPCPhysician').val(this.value);
                                    else if (id == 'txtAllergies')
                                        $('#txtAllergy').val(this.value);
                                    else if (id == 'txtHospital')
                                        $('#txtHospitalEmergency').val(this.value);
                                    else if (id == 'txtHospitalAddress')
                                        $('#txAddressEmergency').val(this.value);
                                });

                                $('#AASSNo,#txtSSN').off('change').on('change', function () {
                                    var id = this.id;
                                    if (id == 'AASSNo') {
                                        $('#txtSSN').val(this.value);
                                    } else if (id == 'txtSSN') {
                                        $('#AASSNo').val(this.value);
                                    }
                                });

                                $('#txtDischargeDate,#txtDateLeft').off('change').on('change', function () {
                                    var id = this.id;
                                    if (id == 'txtDischargeDate') {
                                        $('#txtDateLeft').val(this.value);
                                    } else if (id == 'txtDateLeft') {
                                        $('#txtDischargeDate').val(this.value);
                                    }
                                });

                                $('#DCRoomNumber,#AARoomNo,#RMMRoomNo,#ANSAge2,#ANSAge').attr('readonly', 'readonly');
                                $('#txtResidentName,#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').attr('readonly', 'readonly');

                                if ($('#ANSAge').val() == "") {
                                    var age = getAge($('#txtBirthdate').val());
                                    $('#ANSAge,#ANSAge2').val(age);
                                }

                                $('#ANSAge2').val($('#ANSAge').val());
                               

                                //var mov = $('#RMMMoveInDate').val();
                                //if (mov != '') {
                                //    $('#AssMoveInDate').val(mov);
                                //}
                                ////appraisal needs and service instance
                                //if ($('#ANSAppraisalID').val() == '') {
                                //    $('#ANSAppraisalDate').val(ALC_Util.CurrentDate).show();
                                //    $('#SelectAppraisalDate').hide();
                                //    $('#btnNew').hide();
                                //} else {
                                //    //load selection appraisal dates    
                                //    $('#ANSAppraisalDate').hide();
                                //    $('#btnNew').show();
                                //    $('#SelectAppraisalDate').html('');
                                //    if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                //        return;
                                //    $.get(ALC_URI.Admin + "/GetAppraisalNSDates?id=" + $('#txtAdmissionId').val(), function (data) {
                                //        $.each(data, function () {
                                //            $('#SelectAppraisalDate').append($('<option>', {
                                //                value: this.id,
                                //                text: this.date
                                //            }));
                                //        });
                                //        $('#SelectAppraisalDate').val($('#ANSAppraisalID').val()).show();
                                //        $('#SelectAppraisalDate').off('change').on('change', function () {
                                //            $('#ANSAppraisalID').val($(this).val());
                                //            $.get(ALC_URI.Admin + "/GetAppraisalNS?id=" + $(this).val(), function (dt) {
                                //                $('#tabs-3a section').clearFields();
                                //                $('#ANSResidentName').val(name);
                                //                $('#ANSDOB').val($('#txtBirthdate').val());
                                //                if ($('#ANSAge').val() == "") {
                                //                    var age = getAge($('#txtBirthdate').val());
                                //                    $('#ANSAge,#ANSAge2').val(age);
                                //                }

                                //                $('#ANSAge2').val($('#ANSAge').val());
                                //                $('#tabs-3a section').mapJson(dt, 'id');
                                //            });
                                //        });
                                //        $('#btnNew').off('click').on('click', function () {
                                //            //showdiagNewAppraisal();
                                //        });
                                //    });
                                //}

                                ////admission agreement instance
                                //if ($('#AAAgreementID').val() == '') {
                                //    $('#AAAgreementDate').val(ALC_Util.CurrentDate).show();
                                //    $('#SelectAgreementDate').hide();
                                //    $('#btnNewAgr').hide();
                                //} else {
                                //    //load selection appraisal dates    
                                //    debugger
                                //    $('#AAAgreementDate').hide();
                                //    $('#btnNewAgr').show();
                                //    $('#SelectAgreementDate').html('');
                                //    if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                //        return;
                                //    $.get(ALC_URI.Admin + "/GetAgreementDates?id=" + $('#txtAdmissionId').val(), function (data) {
                                //        $.each(data, function () {
                                //            $('#SelectAgreementDate').append($('<option>', {
                                //                value: this.id,
                                //                text: this.date
                                //            }));
                                //        });
                                //        $('#SelectAgreementDate').val($('#AAAgreementID').val()).show();
                                //        $('#SelectAgreementDate').off('change').on('change', function () {
                                //            $('#AAAgreementID').val($(this).val());
                                //            $.get(ALC_URI.Admin + "/GetAgreement?id=" + $(this).val(), function (dt) {
                                //                debugger
                                //                $('#tabs-4a').clearFields();
                                //                $('#AAResidentName').val(name);
                                //                $('#AADOB').val($('#txtBirthdate').val());
                                //                $('#AARoomNo').val($('#DRoomId option:selected').text());
                                //                $('#AASSNo').val($('#txtSSN').val());
                                //                $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                                //                $('#tabs-4a').mapJson(dt, 'id');
                                //            });
                                //        });
                                //        $('#btnNewAgr').off('click').on('click', function () {
                                //            showdiagNewAgreement();
                                //        });
                                //    });
                                //}
                                //residentState.orig = JSON.stringify($('#residentForm .resForm').extractJson());
                                //initHistory();
                            }, 0);
                        });
                    }

                    $(".loader").hide();
                });

                //if ($('#ftrRoom').find('option').size() <= 0) {
                //    var optsRm = [];

                //    $.each(unique($.map(data, function (o) {
                //        return o.RoomName
                //    })), function () {
                //        optsRm.push('<option>' + this + '</option>');
                //    });
                //    $('#ftrRoom').html(optsRm.join(''));

                //    $("#ftrRoom").html($('#ftrRoom option').sort(function (x, y) {
                //        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                //    }));
                //    $("#ftrRoom").prepend('<option>All</option>');
                //    $("#ftrRoom").get(0).selectedIndex = 0;
                //}
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;
                //Grid.Settings.init();


            }
        });
        $("#grid").jqGrid('bindKeys');
    });


    function getTodayTask() {

        var result;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = mm + '/' + dd + '/' + yyyy;

        $.ajax({
            url: "Staff/GetGridData?func=activity_schedule&param=" + today,
            type: "GET",
            datatype: 'json',
            postData: "",
            async: false,
            success: function (data) {
                var group_to_values = data.reduce(function (obj, item) {
                    obj[item.resident_id] = obj[item.resident_id] || [];
                    obj[item.resident_id].push(item);
                    return obj;
                }, {});


                result = group_to_values;

            }
        });


        return result;
    }

    //var todayTask = getTodayTask();

    //$('#txtResidentName,#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').attr('readonly', 'readonly');
    $('#txtFirstname,#txtMiddleInitial,#txtLastname').change(function () {
        var fname = $('#txtFirstname').val();
        // fname.substr(0, 1).toUpperCase() + fname.substr(1);
        var mname = $('#txtMiddleInitial').val();
        var lname = $('#txtLastname').val();
      
        //var name = fname.trim();
        if (mname != '' || mname != null) {
       var     name = lname +", "+ fname +" "+ mname+".";

        }
        if (mname == '' || mname == null) {
            var name = lname + ", " + fname + " " ;
        }

        $('#txtResidentName,#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val(name);
    });

    //$('#DCRoomNumber,#AARoomNo,#RMMRoomNo').attr('readonly', 'readonly');
    $('#DRoomId').change(function () {
        $('#DCRoomNumber,#AARoomNo,#AARoomNo2, #RMMRoomNo').val($('#DRoomId option:selected').text());
    });

    $('#txtRPharmacy,#txtRPharmacyPhone,#txtRPhysicianPhone,#txtPCPhysician,#txtAllergy,#txtHospitalEmergency,#txAddressEmergency').change(function () {
        var id = this.id;
        if (id == 'txtRPharmacy') {
            $('#txtPharmacy').val(this.value);
        } else if (id == 'txtRPharmacyPhone')
            $('#txtPharmacyPhone').val(this.value);
        else if (id == 'txtRPhysicianPhone')
            $('#txtPhysicianPhone').val(this.value);
        else if (id == 'txtPCPhysician')
            $('#txtPhysician').val(this.value);
        else if (id == 'txtAllergy')
            $('#txtAllergies').val(this.value);
        else if (id == 'txtHospitalEmergency')
            $('#txtHospital').val(this.value);
        else if (id == 'txAddressEmergency')
            $('#txtHospitalAddress').val(this.value);

    });

    $('#ftrRoom,#ftrResident,#ftrSortResident').change(function () {
        var res = $('#ftrResident').val();
        var rm = $('#ftrRoom :selected').text();
        var rsort = $('#ftrSortResident :selected').text();
        var ftrs = {
            groupOp: "AND",
            rules: []
        };

        if (rsort == 'First Name') {
            $('#grid').jqGrid('setGridParam', {
                sortname: 'Firstname',
                sortorder: 'asc'
            }).trigger('reloadGrid');
            $("#grid").showCol("Resident2").setGridWidth($("#residentGrid").width(), true);
            $("#grid").hideCol("Firstname").setGridWidth($("#residentGrid").width(), true);
        }
        if (rsort == 'Last Name') {
            $('#grid').jqGrid('setGridParam', {
                sortname: 'Lastname',
                sortorder: 'asc'
            }).trigger('reloadGrid');
            $("#grid").showCol("Firstname").setGridWidth($("#residentGrid").width(), true);
            $("#grid").hideCol("Resident2").setGridWidth($("#residentGrid").width(), true);
        }

        if (rm != 'All')
            ftrs.rules.push({
                field: 'RoomName',
                op: 'eq',
                data: rm
            });
        if (res != '')
            ftrs.rules.push({
                field: 'Name',
                op: 'cn',
                data: res
            });

        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, {
            filters: JSON.stringify(ftrs)
        });
        $("#grid").trigger("reloadGrid");
    });

    //added by Mariel ---START---
    function imageFormatter(cellValue, options, rowObject) {

        var html = "";

        html += "<img class='imgsrc' src='Resource/ResidentImage?id=" + rowObject.Id + "&r=" + Date.now() + "' alt='" + rowObject.Name + "' width='100%' style='cursor:pointer;'/>";

        return html;

    }

    function dataFormatter(cellValue, options, rowObject) {

        var html = "";
        var html1 = "";
        var mainTable = "";
        var scheduleTable = "";

        var statue = "";
        var dnr = "<div class='dnr'><span>DNR</span></div>";

        html += "<table style='table-layout: fixed;'>";
        html += "<tr><td>Contact: </td>";
        html += "<td>" + (rowObject.ResponsiblePerson != null ? (rowObject.ResponsiblePerson != "" ? rowObject.ResponsiblePerson : "N/A") : "N/A") + "</td></tr>";

        html += "<tr><td>Telephone: </td>";
        html += "<td>" + (rowObject.ResponsiblePersonTelephone != "" ? (rowObject.ResponsiblePersonTelephone != null ? rowObject.ResponsiblePersonTelephone : "N/A") : "N/A") + "</td></tr>";
        html += "<tr><td>Email: </td>";
        html += "<td>" + (rowObject.ResponsiblePersonEmail != "" ? (rowObject.ResponsiblePersonEmail != null ? rowObject.ResponsiblePersonEmail : "N/A") : "N/A") + "</td></tr>";
        html += "</table>";


        html += "<br/>"
        html += "<table style='table-layout: fixed;'>";
        html += "<tr><td>Doctor: </td>";

        html += "<td>" + (rowObject.Physician != null ? (rowObject.Physician != "" ? rowObject.Physician + " " : " ") : " ") + "</td></tr>";
        html += "<tr><td>Phone: </td>";
        html += "<td>" + (rowObject.PhysicianPhone != "" ? (rowObject.PhysicianPhone != null ? rowObject.PhysicianPhone : "N/A") : "N/A") + "   Fax: " + (rowObject.PhysicianFax != null ? (rowObject.PhysicianFax != "" ? rowObject.PhysicianFax : "N/A") : "N/A") + "</td></tr>";
        html += "<tr><td>Email: </td>";
        html += "<td>" + (rowObject.PhysicianEmail != null ? (rowObject.PhysicianEmail != "" ? rowObject.PhysicianEmail : "N/A") : "N/A") + "</td></tr>";
        html += "</table>";

        var todayTask = getTodayTask();
        var services = todayTask[rowObject.Id];

        scheduleTable += "<table class='resTable'>"
        scheduleTable += "<tr class='tblhead'><td style='width: 10%;'>Time</td>";
        scheduleTable += "<td style='width: 30%;'>Service</td>";
        scheduleTable += "<td style='width: 15%;'>Status</td><td style='width: 15%;'>Time Completed</td>";
        scheduleTable += "<td style='width: 30%;'>Carestaff</td></tr>";

        if (services != undefined) {
            $.each(services, function (i, item) {

                if (item.activity_status == 0) {

                    if (item.timelapsed == true) {
                        status = "<span class='missed'>Missed</span>";
                    } else {
                        status = "Not yet completed";
                    }

                } else {

                    status = "<div class='taskCompleted'></div>";

                }

                scheduleTable += "<tr title='" + item.activity_desc + "'>";
                scheduleTable += "<td>" + item.start_time + "</td><td>" + item.activity_desc + "</td><td>" + status + "</td>";
                scheduleTable += "<td>" + (item.actual_completion_date != null ? item.actual_completion_date : "N/A") + "</td><td>" + item.carestaff_name + "</td>";
                scheduleTable += "</tr>";

            });
        }

        scheduleTable += "</table>";

        mainTable += "<table class='resTable'>";
        //mainTable += "<tr><td><label class='imgname_p' >" + (rowObject.IsDNR ? dnr : "") + "<a class='imgname rf_" + rowObject.Id + "' id='" + rowObject.Id + "'>" + (rowObject.Name || "") + "</a></label></td></tr>";
        mainTable += "<tr><td>";
        mainTable += "<table class='resTable' style='word-wrap: break-word'><tr><td style='width:1%'></td><td style='width:17%' align='center'>";
        mainTable += "<label class='imgname_p' >" + (rowObject.IsDNR ? dnr : "") + "<a class='imgname rf_" + rowObject.Id + "' id='" + rowObject.Id + "'>" + (rowObject.Name || "") + "</a></label><br/><br/><br/>";
        mainTable += "<center><button class='resbtn residentFilebtn' title='" + rowObject.Name + "' id='7_" + rowObject.Id + "'>Files</button><br/>";
        mainTable += "<button class='resbtn residentFilebtn getHistory' title='" + rowObject.Name + "' id='6_" + rowObject.Id + "'>Get History</button>";

        if (rowObject.isAdmitted == false) {
            mainTable += "<p style=\"color: red;font-weight: bold; margin-top: 10px;\">DISCHARGED</p>";
        }

        mainTable += "</center></td><td style='width:1%'></td>";
        mainTable += "<td style='width:23%;' valign='top'>" + html + "</td>";
        mainTable += "<td style='width:57%;' valign='top'>" + scheduleTable + "</td></tr></table>";
        mainTable += "</td></tr>";
        //mainTable += "<button class='resbtn residentFilebtn' title='" + rowObject.Name + "' id='7_" + rowObject.Id + "'>Files</button>";
        //mainTable += "<button class='resbtn residentFilebtn getHistory' title='" + rowObject.Name + "' id='6_" + rowObject.Id + "'>Get History</button>";
        mainTable += "</table>";

        return mainTable;


    }

    function dataFormatter2(cellValue, options, rowObject) {

        var html = "";
        var html1 = "";
        var mainTable = "";
        var scheduleTable = "";

        var statue = "";
        var dnr = "<div class='dnr'><span>DNR</span></div>";

        html += "<table style='table-layout: fixed;'>";
        html += "<tr><td>Contact: </td>";
        html += "<td>" + (rowObject.ResponsiblePerson != null ? (rowObject.ResponsiblePerson != "" ? rowObject.ResponsiblePerson : "N/A") : "N/A") + "</td></tr>";

        html += "<tr><td>Telephone: </td>";
        html += "<td>" + (rowObject.ResponsiblePersonTelephone != "" ? (rowObject.ResponsiblePersonTelephone != null ? rowObject.ResponsiblePersonTelephone : "N/A") : "N/A") + "</td></tr>";
        html += "<tr><td>Email: </td>";
        html += "<td>" + (rowObject.ResponsiblePersonEmail != "" ? (rowObject.ResponsiblePersonEmail != null ? rowObject.ResponsiblePersonEmail : "N/A") : "N/A") + "</td></tr>";
        html += "</table>";


        html += "<br/>"
        html += "<table style='table-layout: fixed;'>";
        html += "<tr><td>Doctor: </td>";

        html += "<td>" + (rowObject.Physician != null ? (rowObject.Physician != "" ? rowObject.Physician + " " : " ") : " ") + "</td></tr>";
        html += "<tr><td>Phone: </td>";
        html += "<td>" + (rowObject.PhysicianPhone != "" ? (rowObject.PhysicianPhone != null ? rowObject.PhysicianPhone : "N/A") : "N/A") + "   Fax: " + (rowObject.PhysicianFax != null ? (rowObject.PhysicianFax != "" ? rowObject.PhysicianFax : "N/A") : "N/A") + "</td></tr>";
        html += "<tr><td>Email: </td>";
        html += "<td>" + (rowObject.PhysicianEmail != null ? (rowObject.PhysicianEmail != "" ? rowObject.PhysicianEmail : "N/A") : "N/A") + "</td></tr>";
        html += "</table>";

        var todayTask = getTodayTask();
        var services = todayTask[rowObject.Id];

        scheduleTable += "<table class='resTable'>"
        scheduleTable += "<tr class='tblhead'><td style='width: 10%;'>Time</td>";
        scheduleTable += "<td style='width: 30%;'>Service</td>";
        scheduleTable += "<td style='width: 15%;'>Status</td><td style='width: 15%;'>Time Completed</td>";
        scheduleTable += "<td style='width: 30%;'>Carestaff</td></tr>";

        if (services != undefined) {
            $.each(services, function (i, item) {

                if (item.activity_status == 0) {

                    if (item.timelapsed == true) {
                        status = "<span class='missed'>Missed</span>";
                    } else {
                        status = "Not yet completed";
                    }

                } else {

                    status = "<div class='taskCompleted'></div>";

                }

                scheduleTable += "<tr title='" + item.activity_desc + "'>";
                scheduleTable += "<td>" + item.start_time + "</td><td>" + item.activity_desc + "</td><td>" + status + "</td>";
                scheduleTable += "<td>" + (item.actual_completion_date != null ? item.actual_completion_date : "N/A") + "</td><td>" + item.carestaff_name + "</td>";
                scheduleTable += "</tr>";

            })
        }

        scheduleTable += "</table>";

        mainTable += "<table class='resTable'>";
        //mainTable += "<tr><td><label class='imgname_p' >" + (rowObject.IsDNR ? dnr : "") + "<a class='imgname rf_" + rowObject.Id + "' id='" + rowObject.Id + "'>" + (rowObject.Name || "") + "</a></label></td></tr>";
        mainTable += "<tr><td>";
        mainTable += "<table class='resTable' style='word-wrap: break-word'><tr><td style='width:1%'></td><td style='width:17%' align='center'>";
        mainTable += "<label class='imgname_p' >" + (rowObject.IsDNR ? dnr : "") + "<a class='imgname rf_" + rowObject.Id + "' id='" + rowObject.Id + "'>" + rowObject.Firstname + " " + (rowObject.MiddleInitial ? rowObject.MiddleInitial + ". " : "") + rowObject.Lastname + "</a></label><br/><br/><br/>";
        mainTable += "<center><button class='resbtn residentFilebtn' title='" + rowObject.Firstname + " " + rowObject.MiddleInitial + " " + rowObject.Lastname + "' id='7_" + rowObject.Id + "'>Files</button><br/>";
        mainTable += "<button class='resbtn residentFilebtn getHistory' title='" + rowObject.Firstname + " " + rowObject.MiddleInitial + " " + rowObject.Lastname + "' id='6_" + rowObject.Id + "'>Get History</button></center>";
        mainTable += "</td><td style='width:1%'></td>";
        mainTable += "<td style='width:23%;' valign='top'>" + html + "</td>";
        mainTable += "<td style='width:57%;' valign='top'>" + scheduleTable + "</td></tr></table>";
        mainTable += "</td></tr>";
        //mainTable += "<button class='resbtn residentFilebtn' title='" + rowObject.Name + "' id='7_" + rowObject.Id + "'>Files</button>";
        //mainTable += "<button class='resbtn residentFilebtn getHistory' title='" + rowObject.Name + "' id='6_" + rowObject.Id + "'>Get History</button>";
        mainTable += "</table>";

        return mainTable;


    }

    function admissionFormatter(cellValue, options, rowObject) {

        var html = "";
        var innerTable = "";

        //html += "<br><label class='imgname_p' ><a class='imgname'>" + (rowObject.RoomName || "") + "</a></label>";
        //html += "<br><br><label class='resData'>Admission Date: <strong>" + (rowObject.AdmissionDate || "") + "</strong></label>";
        //html += "<br><label class='resData'>Status: <strong>" + (rowObject.ResidentStatus || "") + "</strong></label>";
        var todayTask = getTodayTask();
        var services = todayTask[rowObject.Id];

        if (services != undefined) {


            innerTable += "<table class='resTable'>"
            innerTable += "<tr class='tblhead'><td style='width: 10%;'>Time</td>";
            innerTable += "<td style='width: 30%;'>Service</td>";
            innerTable += "<td style='width: 15%;'>Status</td><td style='width: 15%;'>Time Completed</td>";
            innerTable += "<td style='width: 30%;'>Carestaff</td></tr>";

            $.each(services, function (i, item) {
                if (item.activity_status == 0) {

                    if (item.timelapsed == true) {
                        status = "<span class='missed'>Missed</span>";
                    } else {
                        status = "Not yet completed";
                    }

                } else {

                    status = "<div class='taskCompleted'></div>";

                }

                innerTable += "<tr title='" + item.activity_desc + "'>";
                innerTable += "<td>" + item.start_time + "</td><td>" + item.activity_desc + "</td><td>" + status + "</td>";
                innerTable += "<td>" + (item.actual_completion_date != null ? item.actual_completion_date : "") + "</td><td>" + item.carestaff_name + "</td>";
                innerTable += "</tr>";

            })

            innerTable += "</table>";
        } else {
            innerTable += "<table style='width:100px;'></table>";
        }

        html += "<table class='resTable'>";
        html += "<tr><td style='width:85%;'>" + innerTable + "</td><td><a class='getHistory' id='6_" + rowObject.Id + "' ><div class='gtHistory'></div></a></td></tr>";
        html += "</table>";


        return html;

    }

    function setTitle(id, cellval, row, colModel) {
        var col = colModel.name;
        var title = '';
        if (col == 'Image')
            title = "Resident: " + (row.Name || '') + (row.IsDNR ? " (Do not resuscitate)" : "") + "\nBirthdate: " + (row.Birthdate || '') + "\nSSN: " + (row.SSN || '');
        else if (col == 'RoomName')
            title = "Room: " + (row.RoomName || '') + "\nAdmission Date: " + (row.AdmissionDate || '') + "\nStatus: " + (row.ResidentStatus || '');

        return 'title="' + title + '"';
    }

    $("#btnclose").click(function () {
        $('#image').attr('src', '');
        $("#ImageData").val("");
    });
    //add kim for preview image  02/22/22
    $("#ImageData").change(function (e) {

        const file = this.files[0];
        if (file) {
            let reader = new FileReader();
            reader.onload = function (event) {
                $("#image").attr("src", event.target.result);
            };
            reader.readAsDataURL(file);
        }
        else if (file == "" || file == null) {
            $('#image').attr('src', '');
            $("#ImageData").val("");
        }
        var val = $(this).val();
        if (val.match(/(?:gif|jpg|png|bmp)$/) || val == "" || val == null) {
            //alert("You are trying to upload an image. Please use the image uploader!");
        }
        else {
            swal('invalid extension!');
            $("#ImageData").val("");
        }

    });


    $("#uploadimg, #btnUploadResImg").click(function () {
        var data = new FormData();
        var files = $("#uploadresimg_modal #ImageData").get(0).files;

        if (files.length > 0) {
            data.append("ImageData", files[0]);
            data.append("resid", $("#resid").val());

            $.ajax({
                url: "Admin/UploadImg?func=resident",
                type: "POST",
                processData: false,
                contentType: false,
                data: data,
                success: function (response) {
                    $("#uploadresimg_modal").modal("hide");
                    //if ($('#diag').dialog('isOpen')) {
                    //angelie here
                    var strhtml = $(".resimg img").attr("src", response.image);
                    var s = $("img#" + currentId).attr("src", response.image);
                    //$(".card-img-top").attr("src", response.image);

                    //}

                    //$("#diagupload").dialog("close");
                    //$('#grid').setGridParam({
                    //    datatype: 'json'
                    //}).trigger('reloadGrid');
                    //kim modified clear images after saving  02/28/22
                    $('#image').attr('src', '');
                    $("#ImageData").val("");
                },
                error: function (er) {
                    swal(er);
                }

            });

        } else {

            swal("No file chosen");

        }



    });

    //---END---

    function rowColorFormatter(cellValue, options, rowObject) {
        if (cellValue == "Move In For Follow Up") {
            rowsToColor[rowsToColor.length] = options.rowId;
        }
        return cellValue;
    }



    $("#diagTask").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Add Service Task",
        dialogClass: "calendarVisitDialog",
        open: function () { }
    });

    $('#txtTimeIn').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#txtCarestaff').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#Onetimedate').trigger('change');
        }
    });
    // $('#txtTimeIn').mask('99:99 xy');

    $('#txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#viewActivity').val(title);


        toggleCarestafflist();
    });

    $('#diagTask input[name=recurring]').change(function () {
        $('#diagTask #txtCarestaff')[0].options.length = 0;
    });

    if ($('#ddlDept').length > 0) {
        $('#ddlDept').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('table.dayofweek input').removeAttr('disabled');
            $('#Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('table.dayofweek input').attr('disabled', 'disabled');
            $('table.dayofweek').clearFields();
            $('#Onetimedate').removeAttr('disabled');
        }
    });


    $('table.dayofweek input').click(triggerDayofWeekClick);

    $('#Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,

    }).on('changeDate', function () {
        $(this).datepicker('hide');
    });

    $('#Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff(dow);
    });

    //$('#AddServiceToolbar').on('click', 'a', function () {
    //    var q = $(this);
    //    if (q.is('.add')) {
    //        AddService();
    //    } else if (q.is('.del')) {
    //        var id = $('#sGrid').jqGrid('getGridParam', 'selrow');
    //        if (id) {
    //            if (confirm('Are you sure you want to delete the selected row?')) {
    //                setTimeout(function () {
    //                    var row = $('#sGrid').jqGrid('getRowData', id);
    //                    if (row) {
    //                        doAjax2(3, {
    //                            Id: row.Id
    //                        }, function (m) {
    //                            var msg = "Task deleted successfully!";
    //                            if (m.result == -3) //inuse
    //                                msg = "Task cannot be deleted because it is currently in use.";
    //                            $.growlSuccess({
    //                                message: msg,
    //                                delay: 6000
    //                            });
    //                            //if no error or not in use then reload grid
    //                            if (m.result != -1 || m.result != -3) {
    //                                fromSort = true;
    //                                new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
    //                                $('#sGrid').setGridParam({
    //                                    datatype: 'json'
    //                                }).trigger('reloadGrid');
    //                            }
    //                        });
    //                    }
    //                }, 100);
    //            }
    //        } else {
    //            alert("Please select row to delete.")
    //        }
    //    } else if (q.is('.refresh')) {
    //        //fromSort = true;
    //        //new Grid_Util.Row().saveSelection.call( $( '#sGrid' )[0] );
    //        $('#sGrid').setGridParam({
    //            datatype: 'json'
    //        }).trigger('reloadGrid');
    //    }
    //});



    //$('#historyServiceToolbar').on('click', 'a', function () {
    //    var q = $(this);
    //    if (q.is('.resetSend')) {
    //        //to do
    //    } else if (q.is('.print')) {
    //        //do print
    //    } else if (q.is('.refresh')) {
    //        //  initHistory();
    //        var completion_date = $("#fromDate").val();
    //        var completion_date_end = $("#toDate").val();

    //        if (completion_date == "") {
    //            var date = new Date();
    //            completion = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
    //            $("#fromDate").val((date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear());
    //        } else {
    //            completion = completion_date + " 12:00:00 AM";
    //        }

    //        if (completion_date_end == "") {
    //            var date = new Date();
    //            completion_end = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
    //            $("#toDate").val((date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear());
    //        } else {
    //            completion_end = completion_date_end + " 12:00:00 AM";
    //        }
    //        $("#hGrid").DataTable().destroy();
    //        generateWeeklySchedule(completion, completion_end, "All");
    //        $("#hGrid").DataTable({
    //            "scrollY": "400px",
    //            "scrollCollapse": true,
    //            stateSave: true,
    //            responsive: !0,
    //            pagingType: "full_numbers",
    //            dom: 'lBfrtip'
    //        });
    //    }
    //});



    var resizeGridsS = (function () {
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#AddServiceToolbar').height();
            $("#sGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#sGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
            //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
            //console.log($("#sGrid").height())
        }
        setTimeout(f, 100);
        return f;
    });


    //var resizeGridHS = (function () {
    //    var f = function () {
    //        var tabH = $('.tabs').height() + 26;
    //        var tBH = $('#historyServiceToolbar').height();
    //        $("#hGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
    //        $("#hGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
    //        //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
    //        //console.log($("#sGrid").height())
    //    }
    //    setTimeout(f, 100);
    //    return f;
    //});

    var initFinancialResponsible = function () {
        var id = $('#admForm #txtResidentId').val();

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=financial_and_other&param=" + id,
            dataType: "json",
            async: false,
            success: function (d) {
                doc_html = " ";
                $.each(d, function (i, fr) {
                    //console.log(fr);
                    doc_html += "<tr><td><a>" + fr.FRname + "</a></td>";
                    doc_html += "<td>" + fr.FRaddress + "</td>";
                    doc_html += "<td>" + fr.FRphone + "</td>";
                    doc_html += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item edit fr_edit' id='" + fr.Id + "' href='#'><i class='la la-edit'></i> Edit</a>\n<a class='dropdown-item del fr_delete' href='#' id='" + fr.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                    doc_html += "</tr>";
                    //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";

                })
                // setTimeout(function () {
                $("#fr_table").html(doc_html);
                //    $("#dGrid").DataTable();
                // }, 50);

            }

        });


        //$('.doc_name').on('click', function () {
        //    debugger;
        //    var did = $(this);
        //    var doc_uuid = did.attr('id');
        //    //$('#document_id').val(id);
        //    window.location.href = "Admin/DownloadDocumentFile/?DOCID=" + doc_uuid;
        //}); 

        $('.fr_delete').on('click', function () {
            var did = $(this);
            var doc_uuid = did.attr('id');
            parsefinancialres(3, doc_uuid);
        });

    }

    //var resizeGridsS = (function () {
    //    //debugger;
    //    var f = function () {
    //        var tabH = $('.tabs').height() + 26;
    //        var tBH = $('#DocumentToolbar').height();
    //        $("#dGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
    //        $("#dGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
    //    }
    //    setTimeout(f, 100);
    //    return f;
    //});

    //var repopulateResident = function () {
    //    $.ajax({
    //        type: "GET",
    //        contentType: "application/json; charset=utf-8",
    //        url: "Admin/GetResident",
    //        dataType: "json",
    //        success: function (m) {
    //            //$('#txtResidentId').empty();
    //            //$('<option/>').val('').html('').appendTo('#txtResidentId');
    //            //$.each(m, function (i, d) {
    //            //    $('<option/>').val(d.Id).html(d.Name).appendTo('#txtResidentId');
    //            //});
    //            //$('#txtResidentId').val(res.CarestaffId);
    //            showdiagTask(2);
    //        }
    //    });

    //}

    $('#DocumentToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.upload')) {
            uploadDocument();
        } else if (q.is('.del')) {
            var docid = $('#dGrid').jqGrid('getGridParam', 'selrow');
            if (!docid) return;
            if (!confirm("Are you sure you want to delete the selected row?")) return;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify({
                    func: 'documents',
                    mode: 3,
                    data: {
                        docId: docid
                    }
                }),
                dataType: "json",
                success: function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({
                            message: "Document has beend deleted successfully!",
                            delay: 3000
                        });
                        initDocuments();
                    } else
                        $.growlError({
                            message: m.message,
                            delay: 6000
                        });
                }
            });
        } else if (q.is('.refresh')) {
            initDocuments();
        }
    });

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        $('#grid').setGridParam({
            datatype: 'json'
        }).trigger('reloadGrid');
    });
    //==========================

}
var tab = 1;
var pagecount = 1;

var loadPager = function () {
    $("#pager-no").val(pagecount);
}

var loadAssessment = function (row) {
    //load assessment
   
    if (row.isAssessment == undefined) {

        var params = {
            func: "assessment_wizard",
            id: row.Id
        };
    } else {

        var params = {
            func: "assessment_wizard_list",
            id: row.Id
        };
    }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ALC_URI.Marketer + "/GetFormData",
        data: JSON.stringify(params),
        dataType: "json",
        success: function (m) {
            //console.log(m);
            var assessment_id = m.Data.AssessmentId;

            $("#AssessmentForm").clearFields("input,select,textarea");

            if (m.Assessments.length > 0) {
                $("#assessmentlist_container").show();
                var string = "";
               
                $.each(m.Assessments, function (i, e) {

                    var milli = e.CompletedDate.replace(/\/Date\((-?\d+)\)\//, '$1');
                    var d = new Date(parseInt(milli));

                    string += "<option value=\"" + e.AssessmentId + "\">" + (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear() + "</option>";
                });

                $("#assessment_list").html(string);

                //$('#assessment_list option[value=' + assessment_id + ']').attr('selected', true);
                $('#assessment_list').val(assessment_id);
                // $("#content-assessment").show();
                $("#printAssessment").removeAttr("disabled").removeAttr("title");
                $("#printAssessment").addClass("btn-outline-accent");

            } else {
                $("#assessmentlist_container").hide();
                //$("#printAssessment").attr("disabled", true).css("pointer-events", "none").attr("title", "HELLO");
                $("#printAssessment").attr("disabled", true).attr("title", "There is no available assessment to print.");
                $("#printAssessment").removeClass("btn-outline-accent");
                
            }


            //if (m.Result == 1) {
            //    $("#AssessmentForm #FirstName").val(m.Data.FirstName);
            //    $("#AssessmentForm #LastName").val(m.Data.LastName);
            //    $("#AssessmentForm #MiddleInitial").val(m.Data.MiddleInitial);
            //    var info = $.cookie('ALCInfo');
            //    if (info != '') {
            //        var ins = info.split('|');
            //        var name = '';
            //        if (ins.length > 1) {
            //            name = ins[1];
            //        }
            //        $("#CompletedBy").val(capsFirstLetter(name));
            //    }
            //} else {
            //    //$(".form").serializeArray().map(function (x) { m[x.name] = x.value; },'id');
            //    $("#AssessmentForm").mapJson(m.Data, 'id');
            //}
            //
            if (m.Result == 1) {

                $("#FirstName").val(m.Data.FirstName);
                $("#LastName").val(m.Data.LastName);
                $("#MiddleInitial").val(m.Data.MiddleInitial);
                $("#label-TotalScore").html("0");
                var info = $.cookie('ALCInfo');
                if (info != '') {
                    var ins = info.split('|');
                    var name = '';
                    if (ins.length > 1) {
                        name = ins[1];
                    }
                    $("#CompletedBy").val(capsFirstLetter(name));
                }
            } else {
                //$(".form").serializeArray().map(function (x) { m[x.name] = x.value; },'id');
                $(".assTab").mapJson(m.Data, 'id');
                $("#label-TotalScore").html(m.Data.TotalScore);
                $('#assessment_list').val(assessment_id);
            }


            $("#AssResidentId").val(row.Id);
            //$("#AssReferralId").val(referralId);
            for (var ps in m.Points) {
                $("#" + ps).val(m.Points[ps]);
                $($("#" + ps).parents().children()[1]).html(m.Points[ps])
            }
            //tab = 1;
            //pagecount = 1;
            $('#assessmentTreeView a:eq(0)').trigger('click');
            if ($('#assPager').length > 0) {
                $('#assPager').remove();
            }
            var $foot = $('<div id="assPager"><div style="padding-top:5px;padding-left:5px;float:left;"> <label><strong>Total Score: <span id="label-TotalScore">0</span></strong> </label></div> ' +
                '<div style="padding:0;"> <center> <input class="navi-button" id="ass-wiz-prev" type="button" value="<<" /> ' +
                ' <span><label id="lpager" style="float:none;">Page  <input type="number" id="pager-no" min ="1" max="34"  /> of 34</label></span> <input class="navi-button" id="ass-wiz-next" type="button" value=">>" /> </center> </div></div>');

            $('.ui-dialog-buttonpane :eq(0)').append($foot);
            $('#ass-wiz-next').off().on('click', function (e) {
                var $current = $("#tabs-" + tab.toString() + 'b .container-display');
                var $next = $("#tabs-" + tab.toString() + 'b .container-display').next('.container');
                if ($next.length == 0) {
                    if (tab == 7)
                        return;
                    tab++;
                    $next = $("#tabs-" + tab.toString() + 'b .container').first('.container');
                }
                $("#AssessmentForm").tabs("option", "selected", tab - 1);
                $next.addClass('container-display');
                $current.removeClass('container-display');
                pagecount++;
                loadPager();
            });
            $('#ass-wiz-prev').off().on('click', function (e) {
                var $current = $("#tabs-" + tab.toString() + 'b .container-display');
                var $prev = $("#tabs-" + tab.toString() + 'b .container-display').prev('.container');
                if ($prev.length == 0) {
                    if (tab == 1)
                        return;
                    tab--;
                    $prev = $("#tabs-" + tab.toString() + 'b .container').last('.container');
                }
                $("#AssessmentForm").tabs("option", "selected", tab - 1);
                $prev.addClass('container-display');
                $current.removeClass('container-display');
                pagecount--;
                loadPager();
            });
            $('#assessmentTreeView').off().on('click', 'a', function (e) {
                var i = $(this).attr('page');
                assessmentWizardGoToPage(i);

            });
            $('#pager-no').off().on('keypress', function (e) {
                if (e.which == 13) {
                    assessmentWizardGoToPage($("#pager-no").val());
                }
            });


            $('#assPager').hide();
            $("#label-TotalScore").text(m.Data.TotalScore);
            var mov = $('#RMMMoveInDate').val();
            if (mov != '') {
                $('#content-assessment #AssMoveInDate').val(mov);
            }
            loadPager();

        }
    });
}

function assessmentWizardGoToPage(page) {
    var $current = $("#tabs-" + tab.toString() + 'b .container-display');
    var gotopage = page;
    if (gotopage == pagecount)
        return;
    if (gotopage > 34 || gotopage < 1) {
        $("#pager-no").val(pagecount);
        return;
    } else
        $("#pager-no").val(gotopage);
    var x = 0;
    var tab_col = {
        1: x += $('#tabs-1b .container').length,
        2: x += $('#tabs-2b .container').length,
        3: x += $('#tabs-3b .container').length,
        4: x += $('#tabs-4b .container').length,
        5: x += $('#tabs-5b .container').length,
        6: x += $('#tabs-6b .container').length,
        7: x += $('#tabs-7b .container').length,
    };
    for (var i = 1; i <= 7; i++) {
        if (gotopage <= tab_col[i]) {
            tab = i;
            pagecount = gotopage;
            var $go = $("#tabs-" + tab.toString() + 'b .container').slice(((tab_col[i] - ((i == 1) ? 0 : tab_col[i - 1])) - (tab_col[i] - pagecount)) - 1).slice(0, 1);

            $("#AssessmentForm").tabs("option", "selected", tab - 1);
            $go.addClass('container-display');
            $current.removeClass('container-display');
            return;
        }
    }
}

var getaggreementdates = function () {
    $.get(ALC_URI.Admin + "/GetAgreementDates?id=" + $('#txtAdmissionId').val(), function (data) {
        $('#SelectAgreementDate').html('');
        $.each(data, function () {
            $('#SelectAgreementDate').append($('<option>', {
                value: this.id,
                text: this.date
            }));
        });
        $('#SelectAgreementDate').val($('#AAAgreementID').val()).show();
    });
}
var getappraisaldates = function () {
    $.get(ALC_URI.Admin + "/GetAppraisalNSDates?id=" + $('#txtAdmissionId').val(), function (data) {
        $('#SelectAppraisalDate').html('');
        $.each(data, function () {
            $('#SelectAppraisalDate').append($('<option>', {
                value: this.id,
                text: this.date
            }));
        });
        $('#SelectAppraisalDate').val($('#ANSAppraisalID').val()).show();
    });
}

var residentState = {
    orig: '',
    current: ''
};


var doAjax = function (mode, row, cb) {
    
    //get record by id
    if (mode == 0) {
        var data = {
            func: "resident",
            id: row
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);               
            }
        });
        return;
    }
    //var isValid = $('#residentForm .resForm').validate();
    //do other modes
    if (mode == 1 || mode == 2) { 
        
        //if (!isValid) return;
        var id = $('#admForm #txtResidentId').val();
        row = $('#residentForm .resForm').extractJson();
        residentState.current = JSON.stringify(row);
        row['ResidentId'] = id;
        row['isAdmittedStatus'] = $('#isAdmitted_status').val();      
       
                   

        //Remove whitespaces in name to avoid name duplication: angelie 04-05-22
        row['Firstname'] = row['Firstname'].trim();
        row['Lastname'] = row['Lastname'].trim();
        row['MiddleInitial'] = row['MiddleInitial'].trim();

        //var tabIdx = $("#residentForm").tabs('option', 'active');
        //debugger;
        //console.log($("#txtFirstname").val());
        //$("#txtFirstname").val($("#txtFirstname").val().trim());
        //console.log($("#txtFirstname").val());

        //if (tabIdx == 0) {
        if (residentState.current == residentState.orig) {
            if (two >= 0 && one == all.length) {
                swal("There are no changes to save.", "", "info");
                $(".loader").hide();
                return;
            }
        }
        //}

        if (confirmProceedAddDuplicate == 1) {
            row.proceedDuplicate = 1
        }

        if (mode == 2) {
           
            if (confirmProceedAddDuplicate == 1) {
                row.proceedDuplicate = 1
            } else {
                curresidentrowStateFirstName = $("#txtFirstname").val();
                curresidentrowStateLastName = $("#txtLastname").val();
                curresidentrowStateMI = $("#txtMiddleInitial").val();

                if ((residentrowStateFirstName != curresidentrowStateFirstName) ||
                    (residentrowStateLastName != curresidentrowStateLastName) ||
                    (residentrowStateMI != curresidentrowStateMI)){
                    row.proceedDuplicate = 0;
                } else {
                    row.proceedDuplicate = 1;
                }
            }         
        }
    }

    //if (mode == 1 || mode == 2) {
    //    //validate Tab Grids
    //    var fGridData = $.grep($('#fGrid').jqGrid('getGridParam', 'data'), function (n, i) {
    //        return (n.E == true) || (n.D == 'true'); //post only marked with E=edited and D=deleted rows
    //    });
    //    var oGridData = $.grep($('#oGrid').jqGrid('getGridParam', 'data'), function (n, i) {
    //        return (n.E == true) || (n.D == 'true');
    //    });

    //    var reqObj = null;
    //    $.each(fGridData, function (i) {
    //        if (this.Name == '' && this.D != 'true') {
    //            isValid = false;
    //            reqObj = this;
    //            return;
    //        }
    //    });
    //    if (!isValid) {
    //        $("#residentForm").tabs("option", "selected", 1);
    //        alert('Name field is required.')
    //        $('#fGrid').jqGrid('editRow', reqObj.RId);

    //        return;
    //    }

    //    $.each(oGridData, function (i) {
    //        if (this.Name == '' && this.D != 'true') {
    //            isValid = false;
    //            reqObj = this;
    //            return;
    //        }
    //    });
    //    if (!isValid) {
    //        $("#residentForm").tabs("option", "selected", 2);
    //        alert('Name field is required.')
    //        $('#oGrid').jqGrid('editRow', reqObj.RId);
    //        return;
    //    }
    //}
    
    var data = {
        func: "resident",
        mode: mode,
        data: row
    };
    
    if (mode == 1 || mode == 2) {
   
        //added this to try to avoid duplication of added resident
        if (isClickedSaveResident == 1) {
            
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                
                    isClickedSaveResident = 0;
                    confirmProceedAddDuplicate = 0;
                    hassavededit = true;
                }
            });
        }
    } else {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }
}

var selectedTabIndex = 0;
var doAjaxAdm = function (mode, row, cb) {
 
    //get record by id
    if (mode == 0) {
        var data = {
            func: "resident_admission",
            id: row
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                
                //if (cb) cb(m);
                var result = $.extend(m.obj1, m.obj2, m.obj3);
                if (cb) cb(result);   
                $(".AAagencyName").html(result.ANSFacilityName); //autopopulate agency name in Admission Agreement Form

                $("#PhysicianSignatureDate, #CEMTDateForSignField, #IEDateOfBirthField, #IEDateAdmittedToFacility, #IEDateLeft, #IEDateField, #releaseDate, #authExpiration, #residentBirthDateField, #authorizationDateCompletedField, #diagExamDateField, #diagDateTBTestGivenField,#diagDateTBTestReadField, #bedriddenEstimatedDateIllnessField, #AAPeriodRateDate1,#AAPeriodRateDate2, #AAProratedRateDate1, #AAProratedRateDate2").datepicker({
                    formatDate: "MM/dd/yyyy"
                }).on('changeDate', function () {
                    $(this).datepicker('hide');
                });

                $("#IEPlacementAgencyTelephone, #IETelephoneOfNearestRelative, #IEPersonResponsibleForFinancialAffairsOneTelephone, #IEPersonResponsibleForFinancialAffairsTwoTelephone, #IEPersonResponsibleForFinancialAffairsThreeTelephone, #facilityTelephoneField,#physicianPhoneField,#CEMTWorkPhoneField,#CEMTHomePhoneField,#PERLAareatel,#UIRTelephoneNumber").mask("(999)999-9999");
                $("#IEOtherPersonToBeNotifiedInEmergencyPhysicianTelephone, #IEOtherPersonToBeNotifiedInEmergencyMentalHealthProviderTelephone, #IEOtherPersonToBeNotifiedInEmergencyDentistTelephone, #IEOtherPersonToBeNotifiedInEmergencyRelativeTelephone, #IEOtherPersonToBeNotifiedInEmergencyFriendTelephone, #IEReligiousAdvisorTelephone, #ANSTelNo").mask("(999)999-9999");
                $("#appDateComp, #ARdateComp, #LdateComp, #TBdate, #TDNclientSignatureDate, #TDNauthrepSignatureDate, #TDNfacilityAuthRepSignatureDate, #RESAPPTBdate, #ANSDate, #ANSRecentHospWhen, #ANSMedHistYesWhen, #ANSStrokeYesDate, #ANSFunctionObjectiveH").datepicker({
                    formatDate: "MM/dd/yyyy"
                }).on('changeDate', function () {
                    $(this).datepicker('hide');
                });
                $("#UIRDateOccured1, #UIRDateAdmission1, #UIRDateOccured2, #UIRDateAdmission2, #UIRDateOccured3, #UIRDateAdmission3, #UIRDateOfSubmission, #UIRDateReviewed , #CompletedDate, #AssMoveInDate, #AssesMoveInDate, #ReviewerDate, #FamilyDate").datepicker({ formatDate: "MM/dd/yyyy" }).on('changeDate', function () {
                    $(this).datepicker('hide');
                }).on('changeDate', function () {
                    $(this).datepicker('hide');
                });

                $("#MFSHistoryofScoring, #MFSSecondaryDiagnosis, #MFSAmbulatoryAid, #MFSHeparinLock, #MFSGait, #MFSMentalStatus").forceNumericOnly();
                $("#MFSHistoryofScoring, #MFSSecondaryDiagnosis, #MFSAmbulatoryAid, #MFSHeparinLock, #MFSGait, #MFSMentalStatus").attr('maxlength', '2');

                $("#MFSHistoryofScoring, #MFSSecondaryDiagnosis, #MFSAmbulatoryAid, #MFSHeparinLock, #MFSGait, #MFSMentalStatus").keyup(function () {
                    var val = $(this).val()
                    var id = $(this).attr('id');
                    len = val.length;

                    if (id == "MFSHistoryofScoring") {
                        if (len == 2) {
                            if (val > 25) {
                                $('#MFSHistoryofScoring').val("")
                                swal("Score out of scale!");
                            }
                        }
                    } else if (id == "MFSSecondaryDiagnosis") {
                        if (len == 2) {
                            if (val > 15) {
                                $('#MFSSecondaryDiagnosis').val("")
                                swal("Score out of scale!");
                            }
                        }
                    } else if (id == "MFSAmbulatoryAid") {
                        if (len == 2) {
                            if (val > 30) {
                                $('#MFSAmbulatoryAid').val("")
                                swal("Score out of scale!");
                            }
                        }
                    } else if (id == "MFSHeparinLock") {
                        if (len == 2) {
                            if (val > 20) {
                                $('#MFSHeparinLock').val("")
                                swal("Score out of scale!");
                            }
                        }
                    } else if (id == "MFSGait") {
                        if (len == 2) {
                            if (val > 20) {
                                $('#MFSGait').val("")
                                swal("Score out of scale!");
                            }
                        }
                    } else if (id == "MFSMentalStatus") {
                        if (len == 2) {
                            if (val > 15) {
                                $('#MFSMentalStatus').val("")
                                swal("Score out of scale!");
                            }
                        }
                    }
                });
            }
        });
        return;
    }

    //var tabindex = $("#admForm").tabs('option', 'active');

    var tabindex = tabindex_adm;
    if (SaveDocCheckForms == false) {
        tabindex = $("#admForm").tabs('option', 'active');
    }
    var url = "";
    //var isValid = $('#admForm').validate();
    //do other modes
    //kim check if allergy changes 11/15/22
    var alle = tags.toString();
    var all = tags;

    let a = 0;
    let one = 0;
    let two = 0;
    for (let i = 0; i < all.length; i++) {
        if (array[a] == all[i]) {
            a++;
            one++;
        }
        else if (array[a] != all[i]) {
            two++;
        }
    }

    if (mode == 1 || mode == 2) {

        //if (!isValid) {
        //    return;
        //}
        
        if (tabindex == 0 || tabindex == 1 || tabindex == 3) { //Saving Admin tabs except forms from doc checklist
            row1 = $('#admForm #tabs-1a').extractJson('id');
            row2 = $('#admForm #tabs-2a').extractJson('id');
            row3 = $('#admForm #tabs-3r').extractJson('id');
            row = $.extend(row1, row2, row3);
            row['txtAllergy'] = alle;
        } else {                                                //Saving Document Checklist Forms
            row1 = $('#admForm #tabs-1a').extractJson('id');
            row2 = $('#files_modal .admTbs').extractJson('id');
            row = $.extend(row1, row2);
            rowadmForms = $('#files_modal .filesdiv').extractJson('id');
        }
        // allergy conflict below codes
        if (SaveDocCheckForms == true) {
            //rowadmForms['ANSResidentName'] = admFormState.val();
            residentState.current = JSON.stringify(rowadmForms);
            residentState.orig = JSON.stringify(admFormState);

        } else {
            delete resStateNew['room_facility'];
            delete row['room_facility'];
            residentState.current = JSON.stringify(row);
            residentState.orig = JSON.stringify(resStateNew);
        }


        
        //if (tabIdx == 0) {
        //add this for no changes in allergy kim 11/18/22
        if (isSaveAndPrint == false) {
            if (residentState.current == residentState.orig && (two >= 0 && one == all.length)) {
                swal("There are no changes to save.", "", "info");
                $(".loader").hide();
                return;
            }
        }

        row.isAdmittedStatus = $('#isAdmitted_status').val();
        row.residentId = $('#residentForm #txtId').val();
        row.dcd = 1; //dont check duplicates.



    }


    if (tabindex != "2i" && tabindex != "2o") { //if not admission aggreement and unusual incident report tab
        url = "Admin/PostGridData";
    } else {
        url = "Admin/PostGridDataV2";
    }

    var data = {
        func: "resident_admission",
        mode: mode,
        data: row,
        TabIndex: tabindex.toString()
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);
           
            $('#files_modal').modal('hide');
            $('.loader').hide();
        }
    });

}

//kim start here!
var showdiag = function (mode) {
    var id = $('#admForm #txtAdmissionId').val(); 
    var amode = $.isNumeric(id) ? 2 : 1;
    var id_ = 0;
    var tabIdx = $("#residentForm").tabs('option', 'active');
    if (tabIdx != 1) {

        doAjax(mode, id, function (m) {

         
          //  console.log(m);
            if (m.result == -1) {

           
                $(".loader").hide();
                //swal("Error! Identical record found!", "Error adding new resident. Kindly verify with agency to avoid admission of duplicate resident.", "error");

                //added for SSN duplication: angelie 04-01-22
             
                if (m.message == "SSN Duplication") {
                    swal("The person you are associating to this intake has a duplicate Social Security Number", "", "error");

                   
                    return;
                }
            };
            if (m.result == -2) {
                $(".loader").hide();
                swal("Error! Identical record found!<br> (" + m.message + ")", "Error saving resident. Kindly verify with agency to avoid admission of duplicate resident.", "error");
                return false;
            };
            if (m.result == 0) {
             
                $(".loader").hide();
                
                //swal("Resident " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
             
                Swal({
                    type: 'success',
                    title: "Resident " + (mode == 1 ? 'saved' : 'updated') + " successfully!",
                    showConfirmButton: false,
                    timer: 2000
                });
                $("#confirmAddDuplicateResident_modal").modal('hide');
                residentState.orig = JSON.stringify($('#residentForm .resForm').extractJson());
                residentrowState = $('#residentForm .resForm').extractJson();
                residentrowStateFirstName = curresidentrowStateFirstName;
                residentrowStateLastName = curresidentrowStateLastName;
                residentrowStateMI = curresidentrowStateMI;
                //clearing text on Search box : 11-04-2022 : Cheche
                $("#search_dam_value").val("");
       
                           
             

                //load_residents('active_only'); //commented out again for saving twice does not work if this is on -abc                 
            }
         
            if (mode == 1) {
              
                //$(".cancelbtn").trigger("click");
                $("#resident_modal").modal('hide');
                setTimeout(function () {
                    $("#residents_1 a").trigger("click");
                }, 100);
            } else {   //added this to prevent this modal displaying again after successful saved :  4-19-2022 : Cheche
                $("#confirmAddDuplicateResident_modal").modal('hide');
            }
         
        });
    } else {
     
        var row = {};
        var tabIdx2 = $("#admForm").tabs('option', 'active');
        if (tabIdx2 != 2) {

            doAjaxAdm(2, id, function (m) {
                arow1 = $('#admForm #tabs-1a').extractJson('id');
                arow2 = $('#admForm #tabs-2a').extractJson('id');
                arow3 = $('#admForm #tabs-3r').extractJson('id');
                resStateNew = $.extend(arow1, arow2, arow3);
                admFormState = $('#files_modal .filesdiv').extractJson('id');


                // console.log(m)
                $(".loader").hide();
                if (m.message == "Duplicate") {

                    swal('Admission for resident already exists!');
                    return;
                }
                if (m.message == "Ok") {

                }
                var tabindex = $("#admForm").tabs('option', 'active');

                if (tabindex == 0) {
                    var fname = $('#txtFirstname').val();
                    // fname.substr(0, 1).toUpperCase() + fname.substr(1);
                    var mname = $('#txtMiddleInitial').val();
                    var lname = $('#txtLastname').val();
                    //var name = fname.trim();
                    if (mname != '' || mname != null) {
                        name = lname + mname + fname;

                    }
                    if (mname == '' || mname == null) {
                        name += ' ' + mname + '.';
                    }

                                //if (lname != '')
                                //name += ' ' + lname.trim();

                    $('#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val(name);
                    $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                    $('#txtDateAdmitted').val($('#txtAdmissionDate').val());                    
                    $('#DCRoomNumber,#AARoomNo,#AARoomNo, #RMMRoomNo').val($('#DRoomId option:selected').text());                   
                    $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                        .addClass("ui-state-disabled");
                    if (m) {
                        $(".loader").hide();
                        swal("General Information " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                    }
                } else if (tabindex == 1) { //document checklist tab

                    if (m) {

                        selectedTabIndex = tabindex + 1;


                        if (isSaveAndPrint == false) {
                            if (SaveDocCheckForms) {
                                if (tabindex_adm == "2a") {
                                    $('#AADOB').val($('#ANSDOB').val());
                                    var ret = JSON.parse(m.return);
                                    if (ret && ret.AgreementID) {
                                        $('#AAAgreementDate').hide();
                                        $('#btnNewAgr').show();
                                        $('#AAAgreementID').val(ret.AgreementID);
                                        if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                            return;
                                        getaggreementdates();

                                    }
                                    swal("Admission Aggreement " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2b") {
                                    swal("Identification and Emergency Information  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2c") {
                                    swal("Physician Report  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2d") {
                                    swal("Release of Medical Information  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2e") {
                                    swal("Consent to Medical Exam " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2f") {
                                    swal("Consent for Emergency Treatment  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2g") {
                                    swal("Pre-placement Appraisal  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2h") {
                                    swal("Resident Appraisal  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2i") {
                                    var ret2 = JSON.parse(m.return);
                                    if (ret2 && ret2.AppraisalID) {
                                        $('#ANSAppraisalDate').hide();
                                        $('#btnNewAppraisal').show();
                                        $('#ANSAppraisalID').val(ret2.AppraisalID);
                                        if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                            return;
                                        getappraisaldates();

                                    }
                                    swal("Appraisal Needs and Services Plan  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2j") {
                                    swal("Personal Rights  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2k") {
                                    swal("Telecommunication Device Notification  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2l") {
                                    swal("Personal Property Record  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2m") {
                                    swal("RB Theft and Loss Policy  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2n") {
                                    swal("Right Re: Med Decision/ Treatment  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2o") {
                                    //var isUir = m.split("_");
                                    //if (isUir.length > 1) {
                                    //-------------------UIR starts here-----------------------
                                    $('#selectDateOccured').html('');
                                    getDateOccured($('#txtAdmissionId').val());
                                    $("#printUIR").show();
                                    $('#uirForm').find('input, textarea').attr('disabled', 'disabled');
                                    //$("#btnSaveAdmission").attr("disabled", true);
                                    //  setTimeout(function () {
                                    //$("#printUIR").val(isUir[0]);
                                    //$("#uirdate").val(isUir[0]);
                                    //$("#selectDateOccured").val(isUir[0]);
                                    $("#printUIR").val(m);
                                    $("#uirdate").val(m);
                                    //$("#selectDateOccured").val(m);
                                    swal("Unusual Insident Report  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2p") {
                                    swal("Resident Weight Record  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2q") {
                                    swal("Centrally Stored Medication  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                } else if (tabindex_adm == "2r") {
                                    swal("Photographic Consent  " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                                }
                            } else {
                                swal("Document Checklist " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                            }
                        } else {
                            if (SaveDocCheckForms) {
                                if (tabindex_adm == "2a") {
                                    $('#AADOB').val($('#ANSDOB').val());
                                    var ret = JSON.parse(m.return);
                                    if (ret && ret.AgreementID) {
                                        $('#AAAgreementDate').hide();
                                        $('#btnNewAgr').show();
                                        $('#AAAgreementID').val(ret.AgreementID);
                                        if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                            return;
                                        getaggreementdates();

                                    }
                                } else if (tabindex_adm == "2i") {
                                    var ret2 = JSON.parse(m.return);
                                    if (ret2 && ret2.AppraisalID) {
                                        $('#ANSAppraisalDate').hide();
                                        $('#btnNewAppraisal').show();
                                        $('#ANSAppraisalID').val(ret2.AppraisalID);
                                        if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                                            return;
                                        getappraisaldates();

                                    }
                                }
                            }
                        }


                    }


                } else if (tabindex == 3) {
                    swal("New Resident Move-In Memo " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                    //toastr.success();
                }

                //if (selectedTabIndex > tabindex) {
                //    //$("#admForm").tabs("option", "selected", selectedTabIndex);
                //}
            });
        } else {           

            $(".loader").hide();
            $("#btnSaveResident").attr("disabled", true);
            //save assessment
            //debugger;
            row = {
                Func: 'assessment_wizard'
            };
            //row.Mode = ($("#AssessmentId").val() == "") ? 1 : 2;
            row.Mode = 1;
            //var isValid = true;
            //isValid = $('#tabs-ass').validate();
            //if (!isValid) {
            //    return;
            //}

            // kim 05/11/22 for adding new assessment completed date and reason for assessment
            var cdate = $("#CompletedDate").val();
            var movein = $('input[name=AssessmentReason]:checked');
            //removed by Cheche : 11-25-2022
            //  if (cdate == null || cdate == "") {
            //    swal('"Please enter Completed Date"', "", "warning");
            //     return
            //   }
            if (movein.length == 0) {
                swal("Select One Reason for Assessment!", "Click Reason for Assessment to select.", "warning");
                return
            }



            row.Data = $('#tabs-ass').extractJson('id');
            var assessment_reason = $("input[name='AssessmentReason']:checked").attr('id');
            swal({
                title: "Save assessment now?",
                text: "You cannot edit an assessment once it is already saved. Please make sure you are done with the form before saving it.",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes"
            }).then(function (e) {
                if (e.value == true) {
                    $(".loader").show();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: ALC_URI.Marketer + "/PostGridData",
                        data: JSON.stringify(row),
                        dataType: "json",
                        success: function (m) {
             
                            swal("Assessment has been saved.", "", "success");

                            var data = {
                                Id: row.Data.AssResidentId,
                            };
                            loadAssessment(data);
                            //$('#assessment_list').val();admission
                            $("#content-assessment #CompletedDate").css("background-color", "#f4f5f8");
                            $("#btnSaveResident").attr("disabled", true);
                            $("#content-assessment input").attr("disabled", "disabled");
                            $("#assessmentlist_container").show();
                            setTimeout(function () {
                                $('#assPager').show();
                            }, 1000);
                            $(".loader").hide();
                        }
                    });
                } else {
                    $("#btnSaveResident").attr("disabled", false);
                }
            });            
        }
    }


    //            if (ts == 1)
    //                window.open(ALC_URI.Admin + "/PrintDocumentChecklist?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 2)
    //                window.open(ALC_URI.Admin + "/PrintAppraisalNeedAndService?id=" + $('#admForm #txtAdmissionId').val() + '&id2=' + $('#ANSAppraisalID').val(), '_blank');
    //            else if (ts == 3)
    //                window.open(ALC_URI.Admin + "/PrintAdmissionAgreement?id=" + $('#admForm #txtAdmissionId').val() + '&id2=' + $('#AAAgreementID').val(), '_blank');
    //            else if (ts == 4)
    //                window.open(ALC_URI.Admin + "/PrintMovein?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 6) {

    //                window.open(ALC_URI.Admin + "/PrintPhysicianReport?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            } else if (ts == 7)
    //                window.open(ALC_URI.Admin + "/PrintPreplacement?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 8)
    //                window.open(ALC_URI.Admin + "/PrintReleaseMedicalInfo?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 9)
    //                window.open(ALC_URI.Admin + "/PrintTelecommunicationNotif?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 10)
    //                window.open(ALC_URI.Admin + "/PrintResidentIdentificationEmergency?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 11)
    //                window.open(ALC_URI.Admin + "/PrintResidentAppraisal?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 12)
    //                window.open(ALC_URI.Admin + "/PrintConsentMedExam?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 13)
    //                window.open(ALC_URI.Admin + "/PrintConsentMedicalTreatment?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 14)
    //                window.open(ALC_URI.Admin + "/PrintPersonalRights?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
    //            else if (ts == 5) {
    //                var ta = $("#AssessmentForm").tabs('option', 'selected');
    //                if (ta == 6)
    //                    window.open(ALC_URI.Marketer + "/PrintMorseFallScale?id=" + $("#AssessmentId").val(), '_blank');
    //                else
    //                    window.open(ALC_URI.Marketer + "/PrintAssessment?id=" + $("#AssessmentId").val(), '_blank');
    //            } else if (ts == 15) {
    //                setTimeout(function () {
    //                    var pid = $("#uirdate").val();
    //                window.open(ALC_URI.Admin + "/PrintIncidentReport?id=" + pid, '_blank');
    //                },2000);

    //            }
    //        }, 4000)

    if (mode == 2) {

        $("#admForm").tabs('option', 'disabled', []);
        $("#residentForm").tabs('option', 'disabled', []);

    }
    //else if (mode == 1)
    //    $("#residentForm").tabs('option', 'disabled', [3, 4, 5, 6]);

}

function getAge(birthday) {
    if (birthday == '') return '';
    var b = moment(birthday)._d;
    var ageDifMs = Date.now() - b.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function unique(array) {
    return $.grep(array, function (el, index) {
        return index == $.inArray(el, array);
    });
}

function capsFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var showdiagTask = function (mode) {
    //$("#diagTask").dialog("option", {
    //    width: 500,
    //    height: 500,
    //    position: "center",
    //    buttons: {
    //        "Save": {
    //            click: function () {
    //var id = $('#grid').jqGrid('getGridParam', 'selrow');
    row = $('#admForm #txtResidentId').val();
    //row = $('#grid').jqGrid('getRowData', id);

    doAjax2(mode, row, function (m) {
        if (m.result == 0) {
            swal("Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
            $("#btnAddTask").prop("disabled", false);
            $('#addtask_modal').modal('hide');
            initService();
        } else if (m.result == -2) {
            var resVisible = $('#txtResidentId').is(':visible');
            $.growlWarning({
                message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".",
                delay: 6000
            });
            $("#btnAddTask").prop("disabled", false);
            //there is already a schedule for a particular resident or room
        }
    });
    //            },
    //            class: "primaryBtn",
    //            text: "Save"
    //        },
    //        "Cancel": function () {
    //            $("#diagTask").dialog("close");
    //        }
    //    }
    //}).dialog("open");
}

var doAjax2 = function (mode, row, cb) {
    
    //get record by id
    if (mode == 0) {

        var data = {
            func: "activity_schedule",
            id: row.Id
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
      
                if (cb) cb(m);
            }
        });
        return;
    }

    //var isValid = $('#diagTask .form').validate();
    //do other modes
    if (mode == 1 || mode == 2) {

        row = $('#addtask_modal').extractJson();

        //validate time

        ////  $("#txtResidentId").val(r);
        //var taskTimeVal = row.ScheduleTime.split(":");
        //var taskTime;
        ////var aTime = aTask[0] + ":00 " + aTask[1];
        //if (taskTimeVal[0] < 10) {
        //    taskTime = "0" + taskTimeVal[0] + ":" + taskTimeVal[1];
        //} else taskTime = row.ScheduleTime;
        row['ScheduleTime'] = $("#txtTimeIn").val();

        var dow = [];

        delete row['Sun'];
        delete row['Mon'];
        delete row['Tue'];
        delete row['Wed'];
        delete row['Thu'];
        delete row['Fri'];
        delete row['Sat'];
        delete row['recurring'];
        delete row['Activity'];
        delete row['Dept'];
        delete row['Onetimedate'];

        var isrecurring = $('#reccur').is(':checked');
      
        if (isrecurring ) {
            row.isonetime = '0';
            $('.dayofweek').find('input[type="checkbox"]').each(function () {
             
                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun':
                            dow.push('0');
                            break;
                        case 'Mon':
                            dow.push('1');
                            break;
                        case 'Tue':
                            dow.push('2');
                            break;
                        case 'Wed':
                            dow.push('3');
                            break;
                        case 'Thu':
                            dow.push('4');
                            break;
                        case 'Fri':
                            dow.push('5');
                            break;
                        case 'Sat':
                            dow.push('6');
                            break;
                    }
                }
            });

            if (dow.length == 0) {
                swal('Please choose day of week schedule.');
                $("#btnAddTask").prop("disabled", false);
                return;
            }
            if (($('#txtCarestaff').val()) == "") {
                swal("Please select a carestaff.")
                $("#btnAddTask").prop("disabled", false);
                return;
            }

        }       
        else {
            row.isonetime = '1';
            var onetimeval = $('#Onetimedate').val();
            if (onetimeval == '') {
                swal("Please tick Schedule.");
                $("#btnAddTask").prop("disabled", false);
                return;
            }

            var onetimeday = moment(onetimeval).day();
            if ($.isNumeric(onetimeday))
                dow.push(onetimeday.toString());

            row.ScheduleTime = onetimeval + ' ' + $("#txtTimeIn").val();
        }
        row.Recurrence = dow.join(',');
        row.addedFromResident = 1;
    }
    //console.log(row);
    var data = {
        func: "activity_schedule",
        mode: mode,
        data: row
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/PostGridData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {

            if (cb) cb(m);
        }
    });
}

var doAjax2_hc = function (mode, row, cb) {

    /* commented this for future use - user can add services via client modal
    //get record by id
    if (mode == 0) {
        var data = {
            func: "activity_schedule",
            id: row.Id
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
        return;
    }

    //var isValid = $('#diagTask .form').validate();
    //do other modes
    if (mode == 1 || mode == 2) {

        row = $('#addtask_modal').extractJson();

        //validate time

        ////  $("#txtResidentId").val(r);
        //var taskTimeVal = row.ScheduleTime.split(":");
        //var taskTime;
        ////var aTime = aTask[0] + ":00 " + aTask[1];
        //if (taskTimeVal[0] < 10) {
        //    taskTime = "0" + taskTimeVal[0] + ":" + taskTimeVal[1];
        //} else taskTime = row.ScheduleTime;
        row['ScheduleTime'] = $("#txtTimeIn").val();

        var dow = [];

        delete row['Sun'];
        delete row['Mon'];
        delete row['Tue'];
        delete row['Wed'];
        delete row['Thu'];
        delete row['Fri'];
        delete row['Sat'];
        delete row['recurring'];
        delete row['Activity'];
        delete row['Dept'];
        delete row['Onetimedate'];

        var isrecurring = $('#reccur').is(':checked');

        if (isrecurring) {
            row.isonetime = '0';
            $('.dayofweek').find('input[type="checkbox"]').each(function () {

                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun':
                            dow.push('0');
                            break;
                        case 'Mon':
                            dow.push('1');
                            break;
                        case 'Tue':
                            dow.push('2');
                            break;
                        case 'Wed':
                            dow.push('3');
                            break;
                        case 'Thu':
                            dow.push('4');
                            break;
                        case 'Fri':
                            dow.push('5');
                            break;
                        case 'Sat':
                            dow.push('6');
                            break;
                    }
                }
            });

            if (dow.length == 0) {
                swal('Please choose day of week schedule.');
                $("#btnAddTask").prop("disabled", false);
                return;
            }
            if (($('#txtCarestaff').val()) == "") {
                swal("Please select a carestaff.")
                $("#btnAddTask").prop("disabled", false);
                return;
            }

        }
        else {
            row.isonetime = '1';
            var onetimeval = $('#Onetimedate').val();
            if (onetimeval == '') {
                swal("Please tick Schedule.");
                $("#btnAddTask").prop("disabled", false);
                return;
            }

            var onetimeday = moment(onetimeval).day();
            if ($.isNumeric(onetimeday))
                dow.push(onetimeday.toString());

            row.ScheduleTime = onetimeval + ' ' + $("#txtTimeIn").val();
        }
        row.Recurrence = dow.join(',');
    }
    //console.log(row);
    
    */

    var data = {
        func: "activity_schedule_svc",
        mode: mode,
        data: row
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/PostGridData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);
        }
    });
}

var bindDept = function (dptid, cb) {

    var data = {
        deptid: dptid
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetServicesAndCarestaffByDeptId",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            //$('#txtCarestaff').empty();
            //$('<option/>').val('').html('').appendTo('#txtCarestaff');
            //$.each(m.Carestaffs, function (i, d) {
            //    $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaff');
            //});

            $('#txtActivity').empty();
            $('#viewActivity').val('');
            var opt = '<option val=""></option>';
            $.each(m.Services, function (i, d) {
                opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
            });
            $('#txtActivity').append(opt);
            if (typeof (cb) !== 'undefined') cb();
        }
    });
}

var toggleCarestafflist = function () {
    var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
    if (isrecurring)
        triggerDayofWeekClick();
    else
        $('#Onetimedate').trigger('change');

    //if housekeeper or maintenance hide room dropdown
    var val = $('#ddlDept').val();
    if (val == 5 || val == 7) {
        $('#liRoom, #liRoom select').show();
        $('#liResident, #liResident select').hide();
    } else {
        $('#liResident, #liResident select').show();
        $('#liRoom, #liRoom select').hide();
    }
}

var triggerDayofWeekClick = function () {
    var a = $('#txtTimeIn').val();
    if (a == null || a == '') {
        $('#txtCarestaff').empty();
        //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
        return;
    }

    var dow = [];
    $('.dayofweek').find('input[type="checkbox"]').each(function () {
        var d = $(this);
        if (d.is(':checked')) {
            switch (d.attr('name')) {
                case 'Sun':
                    dow.push('0');
                    break;
                case 'Mon':
                    dow.push('1');
                    break;
                case 'Tue':
                    dow.push('2');
                    break;
                case 'Wed':
                    dow.push('3');
                    break;
                case 'Thu':
                    dow.push('4');
                    break;
                case 'Fri':
                    dow.push('5');
                    break;
                case 'Sat':
                    dow.push('6');
                    break;
            }
        }
    });
    bindCarestaff(dow);

}

var bindCarestaff = function (dow) {
    if ($('#txtTimeIn').val() == '') {
        $('#txtCarestaff')[0].options.length = 0;
        return;
    }
    if ($('#txtActivity').val() == '') {
        $('#txtCarestaff')[0].options.length = 0;
        return;
    }
    if (dow == null) dow = [];
    var csid = $('input[xid="hdCarestaffId"]').val();
    if (csid == "") csid = null;

    var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');

    var data = {
        scheduletime: $('#txtTimeIn').val(),
        activityid: $('#txtActivity').val(),
        carestaffid: csid,
        recurrence: dow.join(',')
    };
    data.isonetime = isrecurring ? "0" : "1";
    data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#Onetimedate').val()).format("MM/DD/YYYY");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetCarestaffByTime",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            $('#txtCarestaff').empty();
            $('<option/>').val('').html('').appendTo('#txtCarestaff');
            $.each(m, function (i, d) {
                $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
            });
        }
    });
}

var AddService = function () {
    //var id = $('#grid').jqGrid('getGridParam', 'selrow');
    $('#addtask_modal').clearFields();
    var id = $('#admForm #txtResidentId').val();
    var roomNo = $('#DRoomId').val();


    if (!id) {
        id = $('#residentForm #txtId').val();
        if (!id) return;
    }

    $("#addtask_modal #ddlDept option[value='5']").remove();
    $("#addtask_modal #ddlDept option[value='7']").remove();
    var dept = $('#ddlDept').val();
    //$('#addtask_modal').clearFields();
    //$('#txtResidentIdtask select').val(id);
    // $("#addtask_modal #txtResidentId option[value=" + id + "]").attr('selected', 'selected');
    $('#addtask_modal #txtResidentId').val(id);
    $("#addtask_modal #txtRoomId option[value=" + roomNo + "]").attr('selected', 'selected');

    $('#addtask_modal input[xid="hdCarestaffId"]').val('');
    $('#addtask_modal input[name="recurring"]:eq(0)').attr('checked', 'checked');
    $('#addtask_modal input[name="recurring"]:eq(1)').removeAttr('checked');   
    $('#addtask_modal #Onetimedate').attr('disabled', 'disabled');
    $('#addtask_modal .dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
    //showdiagTask(1);
    // back here
    //if housekeeper or maintenance hide room dropdown
    $('#addtask_modal #ddlDept').val(dept);
    if (dept == 5 || dept == 7) {
        $('#addtask_modal #liRoom, #addtask_modal #liRoom select').show();
        $('#addtask_modal #liResident, #addtask_modal #liResident select').hide();
    } else {
        $('#addtask_modal #liResident, #addtask_modal #liResident select').show();
        $('#addtask_modal #liRoom, #addtask_modal #liRoom select').hide();
    }
}

var initService = function () {

    var id = $('#admForm #txtResidentId').val();
    $("#services_table").html('');
    $("#sGrid").DataTable().destroy();
    
    document.getElementById("add_task").removeAttribute("data-toggle");
    //$(".loader").show(); kim comment out for loading after saving a services 03/01/2022
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=activity_schedule&param=-1&param2=" + id, // + $('#ftrRecurrence').val(),
        dataType: "json",
        async: false,
        success: function (d) {
            srvcs_html = " ";

            $.each(d, function (i, s) {
               
                if (s.is_active == true) {

                    var sroom = s.Room;
                    //kim add here 03/08/22 
                    if (s.Room == "" || s.Room == null || s.Room == "undefined") {
                        sroom = "N/A";
                    }

                    srvcs_html += "<tr><td><a>" + s.Activity + "</a></td>";
                    srvcs_html += "<td>" + s.Department + "</td>";
                    srvcs_html += "<td>" + s.Resident + "</td>";
                    srvcs_html += "<td>" + s.Carestaff + "</td>";
                    srvcs_html += "<td>" + sroom  + "</td>";
                    srvcs_html += "<td>" + s.Schedule + "</td>";
                    srvcs_html += "<td>" + s.Recurrence + "</td>";
                    srvcs_html += "<td>" + s.Duration + "</td>";
                    srvcs_html += "<td>" + s.DateCreated + "</td>";
                    //srvcs_html += "<td>" + doc.DateCreated + "</td>";

                    if ($("#resident_modal #isAdmitted_status").val() == "false") {
                        srvcs_html += "<td  hidden><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item srvc_delete' id='" + s.Id + "' href='#'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                        
                    } else {
                        srvcs_html += "<td ><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item srvc_delete' id='" + s.Id + "' href='#'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                    }
                    
                    srvcs_html += "</tr>";
                    //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";
                }
            })


            $("#services_table").html(srvcs_html);
            $("#sGrid").DataTable({
                // kim comment out display of table after saving a services 03/01/22
                //stateSave: true,
                //responsive: !0,
                //pagingType: "full_numbers",
                //dom: 'lBfrtip',
                //select: true,
                //"scrollX": "true",
                //"scrollCollapse": true,
                //"initComplete": function (settings, json) {
                //   // debugger

                //    document.getElementById("add_task").setAttribute("data-toggle", "modal");
                //    $(".loader").hide();
                //}

            });


            if ($("#isAdmitted_status").val() == false) {
                $(".for_active_only").prop("hidden", false);
            } else {
                $(".for_active_only").prop("hidden", true);
            }


        }
    });

    //SGrid.jqGrid({
    //    url: "Admin/GetGridData?func=activity_schedule&param=-1&param2=" + id, // + $('#ftrRecurrence').val(),
    //    datatype: 'json',
    //    postData: '',
    //    ajaxGridOptions: {
    //        contentType: "application/json",
    //        cache: false
    //    },
    //    //datatype: 'local',
    //    //data: dataArray,
    //    //mtype: 'Get',
    //    colNames: ['Id', 'Activity', 'Department', 'Resident', 'Staff', 'Room', 'Schedule', 'Recurrence', 'Duration (Minutes)', 'Date Created', /* 'Completed', 'Remarks'*/],
    //    colModel: [{
    //        key: true,
    //        hidden: true,
    //        name: 'Id',
    //        index: 'Id'
    //    },
    //        {
    //            key: false,
    //            name: 'Activity',
    //            index: 'Activity',
    //            sortable: true,
    //            width: (pageWidth * (8 / 100)),
    //            formatter: "dynamicLink",
    //            formatoptions: {
    //                onClick: function (rowid, irow, icol, celltext, e) {
    //                    var G = $('#sGrid');
    //                    G.setSelection(rowid, false);
    //                    var row = G.jqGrid('getRowData', rowid);
    //                    if (row) {
    //                        doAjax2(0, row, function (res) {
    //                            $("#diagTask .form").clearFields();
    //                            bindDept(res.Dept.toString(), function () {
    //                                if (!res.IsOneTime)
    //                                    res.Onetimedate = '';
    //                                $("#diagTask .form").mapJson(res);
    //                            });
    //                            $("#ddlDept option[value='5']").remove();
    //                            $("#ddlDept option[value='7']").remove();
    //                            //if housekeeper or maintenance hide room dropdown
    //                            if (res.Dept == 5 || res.Dept == 7) {
    //                                $('#liRoom, #liRoom select').show();
    //                                $('#liResident, #liResident select').hide();
    //                            } else {
    //                                $('#liResident, #liResident select').show();
    //                                $('#liRoom, #liRoom select').hide();
    //                            }
    //                            //map day of week
    //                            if (!res.IsOneTime) {
    //                                $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
    //                                if (res.Recurrence != null && res.Recurrence != '') {
    //                                    var setCheck = function (el, str, idx) {
    //                                        if (str.indexOf(idx) != -1)
    //                                            el.prop('checked', true);
    //                                    }
    //                                    var dow = res.Recurrence;
    //                                    $('.dayofweek').find('input[type="checkbox"]').each(function () {
    //                                        var d = $(this);
    //                                        switch (d.attr('name')) {
    //                                            case 'Sun':
    //                                                setCheck(d, dow, '0');
    //                                                break;
    //                                            case 'Mon':
    //                                                setCheck(d, dow, '1');
    //                                                break;
    //                                            case 'Tue':
    //                                                setCheck(d, dow, '2');
    //                                                break;
    //                                            case 'Wed':
    //                                                setCheck(d, dow, '3');
    //                                                break;
    //                                            case 'Thu':
    //                                                setCheck(d, dow, '4');
    //                                                break;
    //                                            case 'Fri':
    //                                                setCheck(d, dow, '5');
    //                                                break;
    //                                            case 'Sat':
    //                                                setCheck(d, dow, '6');
    //                                                break;
    //                                        }
    //                                    });
    //                                    //debugger;
    //                                    $('input[name="recurring"]:eq(0)').trigger('click');
    //                                    $('#Onetimedate').val('');
    //                                }
    //                            } else {
    //                                $('input[name="recurring"]:eq(1)').trigger('click');
    //                                res.Recurrence = moment(res.Onetimedate).day().toString();
    //                            }
    //                            $('input[xid="hdCarestaffId"]').val(res.CarestaffId);
    //                            var data = {
    //                                scheduletime: res.ScheduleTime,
    //                                activityid: res.ActivityId,
    //                                carestaffid: res.CarestaffId,
    //                                recurrence: res.Recurrence
    //                            };
    //                            data.isonetime = !res.IsOneTime ? "0" : "1";
    //                            data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
    //                            $.ajax({
    //                                type: "POST",
    //                                contentType: "application/json; charset=utf-8",
    //                                url: "Admin/GetCarestaffByTime",
    //                                data: JSON.stringify(data),
    //                                dataType: "json",
    //                                success: function (m) {
    //                                    $('#txtCarestaff').empty();
    //                                    $('<option/>').val('').html('').appendTo('#txtCarestaff');
    //                                    $.each(m, function (i, d) {
    //                                        $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
    //                                    });
    //                                    $('#txtCarestaff').val(res.CarestaffId);
    //                                    showdiagTask(2);
    //                                }
    //                            });
    //                        });
    //                    }
    //                }
    //            }
    //        },
    //        {
    //            key: false,
    //            name: 'Department',
    //            index: 'Department',
    //            sortable: true,
    //            width: (pageWidth * (8 / 100))
    //        },
    //        {
    //            key: false,
    //            name: 'Resident',
    //            index: 'Resident',
    //            sortable: true,
    //            width: (pageWidth * (6 / 100))
    //        },
    //        {
    //            key: false,
    //            name: 'Carestaff',
    //            index: 'Carestaff',
    //            sortable: true,
    //            width: (pageWidth * (6 / 100))
    //        },
    //        {
    //            key: false,
    //            name: 'Room',
    //            index: 'Room',
    //            sortable: true,
    //            width: (pageWidth * (4 / 100))
    //        },
    //        //{ key: false, name: 'ScheduleDate', index: 'ScheduleDate' },
    //        {
    //            key: false,
    //            name: 'Schedule',
    //            index: 'Schedule',
    //            sortable: true,
    //            width: (pageWidth * (8 / 100))
    //        },
    //        {
    //            key: false,
    //            name: 'Recurrence',
    //            index: 'Recurrence',
    //            sortable: true,
    //            width: (pageWidth * (8 / 100))
    //        },
    //        {
    //            key: false,
    //            name: 'Duration',
    //            index: 'Duration',
    //            sortable: true,
    //            width: (pageWidth * (5 / 100))
    //        },
    //        {
    //            key: false,
    //            name: 'DateCreated',
    //            index: 'DateCreated',
    //            sortable: true,
    //            width: (pageWidth * (8 / 100)),
    //            formatter: function (cellvalue, options, row) {
    //                return (cellvalue || '').replace(/-/g, '/');
    //            }
    //        }
    //        //{ key: false, name: 'Completed', index: 'Completed', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Yes', '0': 'No' } } },
    //        //{ key: false, name: 'Remarks', index: 'Remarks' }
    //    ],
    //    //pager: jQuery('#pager'),
    //    rowNum: 1000000,
    //    rowList: [10, 20, 30, 40],
    //    height: '100%',
    //    viewrecords: true,
    //    //caption: 'Care Staff',
    //    emptyrecords: 'No records to display',
    //    jsonReader: {
    //        root: 'rows',
    //        page: 'page',
    //        total: 'total',
    //        records: 'records',
    //        repeatitems: false,
    //        id: '0'
    //    },
    //    autowidth: true,
    //    multiselect: false,
    //    sortname: 'Id',
    //    sortorder: 'asc',
    //    loadonce: true,
    //    onSortCol: function () {
    //        fromSort = true;
    //        GURow.saveSelection.call(this);
    //    },
    //    loadComplete: function (data) {
    //        //if ($('#ftrResident').find('option').size() > 0) return;
    //        //var optsRes = ['<option>All</option>'], optsCS = ['<option>All</option>'], optsDP = ['<option>All</option>'];
    //        //$.each(unique($.map(data, function (o) { return o.Resident })), function () {
    //        //    optsRes.push('<option>' + this + '</option>');
    //        //});
    //        //$.each(unique($.map(data, function (o) { return o.Carestaff })), function () {
    //        //    optsCS.push('<option>' + this + '</option>');
    //        //});
    //        //$.each(unique($.map(data, function (o) { return o.Department })), function () {
    //        //    optsDP.push('<option>' + this + '</option>');
    //        //});
    //        //$('#ftrResident').html(optsRes.join(''));
    //        //$('#ftrCarestaff').html(optsCS.join(''));
    //        //$('#ftrDept').html(optsDP.join(''));

    //        if (typeof fromSort != 'undefined') {
    //            GURow.restoreSelection.call(this);
    //            delete fromSort;
    //            return;
    //        }

    //        var maxId = data && data[0] ? data[0].Id : 0;
    //        if (typeof isReloaded != 'undefined') {
    //            $.each(data, function (k, d) {
    //                if (d.Id > maxId) maxId = d.Id;
    //            });
    //        }
    //        if (maxId > 0)
    //            $("#sGrid").setSelection(maxId);
    //        delete isReloaded;
    //    }
    //});
    //SGrid.jqGrid('bindKeys');
    //$('#residentForm div.tabPanels:eq(0)').layout({
    //    center: {
    //        paneSelector: "#serviceGrid",
    //        closable: false,
    //        slidable: false,
    //        resizable: true,
    //        spacing_open: 0
    //    },
    //    north: {
    //        paneSelector: "#AddServiceToolbar",
    //        closable: false,
    //        slidable: false,
    //        resizable: false,
    //        spacing_open: 0
    //    },
    //    onresize: function () {
    //        resizeGridsS();
    //        //resizeGridHS();
    //    }
    //});
    //resizeGridsS();
}

var initServiceHomeCare = function () {

    var id = $('#admForm #txtResidentId').val();
    $("#services_table").html('');
    $("#sGrid").DataTable().destroy();

    document.getElementById("add_task").removeAttribute("data-toggle");
    //$(".loader").show(); kim comment out for loading after saving a services 03/01/2022
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=activity_schedule_svc&param=-1&param2=" + id, // + $('#ftrRecurrence').val(),
        dataType: "json",
        async: false,
        success: function (d) {
         
            srvcs_html = " ";

            $.each(d, function (i, s) {

                srvcs_html += "<tr><td><a>" + s.StartDate + " - " + s.EndDate + "</a></td>";
                srvcs_html += "<td>" + s.StartTime + " - " + s.EndTime + "</td>";
                srvcs_html += "<td>" + s.Resident + "</td>";
                srvcs_html += "<td>" + s.Caregiver + "</td>";
                srvcs_html += "<td>" + s.Services + "</td>";
                srvcs_html += "<td>" + s.Recurrence + "</td>";
                //srvcs_html += "<td>" + " " + "</td>";
                srvcs_html += "<td ><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item srvc_delete_hc' id='" + s.Id + "' href='#'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                srvcs_html += "</tr>";
            })

            $("#services_table").html(srvcs_html);
            $("#sGrid").DataTable({});

            //$.each(d, function (i, s) {

            //    if (s.is_active == true) {

            //        var sroom = s.Room;
            //        //kim add here 03/08/22 
            //        if (s.Room == "" || s.Room == null || s.Room == "undefined") {
            //            sroom = "N/A";
            //        }

            //        srvcs_html += "<tr><td><a>" + s.Activity + "</a></td>";
            //        srvcs_html += "<td>" + s.Department + "</td>";
            //        srvcs_html += "<td>" + s.Resident + "</td>";
            //        srvcs_html += "<td>" + s.Carestaff + "</td>";
            //        srvcs_html += "<td>" + sroom + "</td>";
            //        srvcs_html += "<td>" + s.Schedule + "</td>";
            //        srvcs_html += "<td>" + s.Recurrence + "</td>";
            //        srvcs_html += "<td>" + s.Duration + "</td>";
            //        srvcs_html += "<td>" + s.DateCreated + "</td>";
            //        //srvcs_html += "<td>" + doc.DateCreated + "</td>";

            //        if ($("#resident_modal #isAdmitted_status").val() == "false") {
            //            srvcs_html += "<td  hidden><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item srvc_delete' id='" + s.Id + "' href='#'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";

            //        } else {
            //            srvcs_html += "<td ><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item srvc_delete' id='" + s.Id + "' href='#'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
            //        }

            //        srvcs_html += "</tr>";
            //        //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";
            //    }
            //})


            //$("#services_table").html(srvcs_html);
            //$("#sGrid").DataTable({
            //    // kim comment out display of table after saving a services 03/01/22
            //    //stateSave: true,
            //    //responsive: !0,
            //    //pagingType: "full_numbers",
            //    //dom: 'lBfrtip',
            //    //select: true,
            //    //"scrollX": "true",
            //    //"scrollCollapse": true,
            //    //"initComplete": function (settings, json) {
            //    //   // debugger

            //    //    document.getElementById("add_task").setAttribute("data-toggle", "modal");
            //    //    $(".loader").hide();
            //    //}

            //});


            //if ($("#isAdmitted_status").val() == false) {
            //    $(".for_active_only").prop("hidden", false);
            //} else {
            //    $(".for_active_only").prop("hidden", true);
            //}


        }
    });
}

//var initHistory = function () {
//    $("#hGrid").DataTable().destroy();
//    $("#history_table").html('');

//    var removeByAttr = function (arr, attr, value) {
//        var i = arr.length;
//        while (i--) {
//            if (arr[i] &&
//                arr[i].hasOwnProperty(attr) &&
//                (arguments.length > 2 && arr[i][attr] === value)) {

//                arr.splice(i, 1);

//            }
//        }
//        return arr;
//    }

//    function beforeProcessing(data, status, xhr) {
//        var delList = [];
//        if (data && data.length > 0) {
//            //search for items with xref and check if it has correspoding acivity_schedule_id in the list
//            //if exists then remove item with xref and preserve the record with activity_schedule_id = xref
//            var occ = $.grep(data, function (a) {
//                return a.xref > 0
//            });
//            $.each(occ, function (x, y) {
//                var matchObj = $.grep(data, function (b) {
//                    return b.activity_schedule_id == y.xref;
//                });
//                if (matchObj.length > 0) {
//                    delList.push(y);
//                }
//            });

//            $.each(delList, function (i, a) {
//                removeByAttr(data, 'activity_schedule_id', a.activity_schedule_id);
//            });
//        }
//    }

//    //if ($('#hGrid')[0] && $('#hGrid')[0].grid)
//    //    $.jgrid.gridUnload('hGrid');
//    //var HGrid = $('#hGrid');

//    //var GURow = new Grid_Util.Row();
//    //var pageWidth = $("#hGrid").parent().width() - 100;

//    //var id = $('#grid').jqGrid('getGridParam', 'selrow');
//    var id = $('#admForm #txtResidentId').val();
//    //if (!id) {
//    //    id = $('#diagTask #txtId').val();
//    //    if (!id) return;
//    //}

//    $.ajax({
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        url: "Staff/GetGridData?func=activity_schedule&param=" + $('#fromDate').val() + "&param2=" + $('#toDate').val() + "&param3=" + id,
//        dataType: "json",
//        async: false,
//        success: function (d) {
//            console.log(d);
//            doc_html = " ";
//            $.each(d, function (i, doc) {

//                if (doc.carestaff_activity_id == null && doc.activity_status == 0)
//                    doc.activity_status = '<span class="m-badge  m-badge--metal m-badge--wide">N/A</span';
//                if (doc.carestaff_activity_id == null && doc.timelapsed) {
//                    doc.activity_status = '<span class="m-badge m-badge--danger m-badge--wide">Missed</span>';
//                }

//                doc_html += "<tr><td><a>" + doc.activity_desc + "</a></td>";
//                doc_html += "<td>" + doc.activity_proc + "</td>";
//                doc_html += "<td>" + doc.department + "</td>";
//                doc_html += "<td>" + doc.resident + "</td>";
//                doc_html += "<td>" + doc.room + "</td>";
//                doc_html += "<td>" + doc.carestaff_name + "</td>";
//                doc_html += "<td>" + doc.start_time + "</td>";
//                doc_html += "<td>" + doc.service_duration + "</td>";
//                doc_html += "<td>" + doc.activity_status + "</td>";
//                doc_html += "<td>" + doc.completion_date + "</td>";
//                doc_html += "<td>" + doc.acknowledged_by + "</td>";
//                doc_html += "<td>" + doc.remarks + "</td>";
//                //doc_html += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item doc_name' id='" + doc.UUID + "' href='#'><i class='la la-edit'></i> Download</a>\n<a class='dropdown-item del doc_delete' href='#' id='" + doc.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
//                doc_html += "</tr>";
//                //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";

//            })

//            $("#history_table").html(doc_html);
//            $("#hGrid").DataTable({
//                "scrollY": "400px",
//                "scrollCollapse": true,
//                stateSave: true,
//                responsive: !0,
//                pagingType: "full_numbers",
//                dom: 'lBfrtip'
//            });
//        }

//    });


//    //HGrid.jqGrid({
//    //    url: "Staff/GetGridData?func=activity_schedule&param=" + $('#fromDate').val() + "&param2=" + $('#toDate').val() + "&param3=" + id,
//    //    beforeProcessing: function (data, status, xhr) {
//    //        beforeProcessing(data, status, xhr)
//    //    },
//    //    datatype: 'json',
//    //    postData: "",
//    //    ajaxGridOptions: {
//    //        contentType: "application/json",
//    //        cache: false
//    //    },
//    //    //datatype: 'local',
//    //    //data: dataArray,
//    //    //mtype: 'Get',
//    //    colNames: ['ActivityId', 'ActivityScheduleId', 'CarestaffActivityId', 'Task', 'Procedure', 'Department', 'Resident', 'Room', 'Staff', 'Schedule', 'Duration (Minutes)', 'Completed', 'oStat', 'Acknowledge Date', 'Acknowledge By', 'Remarks', 'Reschedule', 'XRef', 'TimeLapsed'],
//    //    colModel: [{
//    //        key: false,
//    //        hidden: true,
//    //        name: 'activity_id',
//    //        index: 'activity_id'
//    //    },
//    //        {
//    //            key: true,
//    //            hidden: true,
//    //            name: 'activity_schedule_id',
//    //            index: 'activity_schedule_id'
//    //        },
//    //        {
//    //            key: false,
//    //            hidden: true,
//    //            name: 'carestaff_activity_id',
//    //            index: 'carestaff_activity_id'
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'activity_desc',
//    //            index: 'activity_desc',
//    //            width: 200
//    //, formatter: "dynamicLink", formatoptions: {
//    //onClick: function (rowid, irow, icol, celltext, e) {
//    //    var G = $('#grid');
//    //    G.setSelection(rowid, false);
//    //    var row = G.jqGrid('getRowData', rowid);
//    //    if (row) {
//    //        $('#diag').clearFields();
//    //        $('#diag #CarestaffActivityId').val(row.carestaff_activity_id);
//    //        $('#diag #ActivityScheduleId').val(row.activity_schedule_id);
//    //        //$('#diag #AcknowledgedBy').val(row.acknowledged_by);
//    //        //$('#diag #Id').val(row.carestaff_activity_id);
//    //        //$('#diag #Remarks').val(row.remarks);
//    //        if (row.reschedule_dt && row.reschedule_dt != '')
//    //            $('#diag #Remarks').val("Rescheduled - " + row.reschedule_dt);
//    //        else
//    //            $('#diag #Remarks').val(row.remarks);

//    //        if (row.oactivity_status == false)
//    //            $('#diag #No').attr('checked', 'checked');
//    //        if (row.oactivity_status == true)
//    //            $('#diag #Yes').attr('checked', 'checked');

//    //        $('#diag #staffInfo').html('Is the selected task completed?');
//    //        showdiag();
//    //        if ((moment($('#curDate').val()).format("MM/DD/YYYY") != moment(ALC_Util.CurrentDate).format("MM/DD/YYYY")) || (row.reschedule_dt && row.reschedule_dt != '')) {
//    //            $('#diag').find('input, textarea, button, select').attr('disabled', 'disabled');
//    //            $('.ui-dialog-buttonset').hide();
//    //        } else {
//    //            $('#diag').find('input, textarea, button, select').removeAttr('disabled');
//    //            $('.ui-dialog-buttonset').show();
//    //        }

//    //    }
//    //}
//    //}
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'activity_proc',
//    //            width: 200,
//    //            index: 'activity_proc'
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'department',
//    //            width: 100,
//    //            index: 'department'
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'resident',
//    //            index: 'resident',
//    //            width: (pageWidth * (12 / 100))
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'room',
//    //            index: 'room',
//    //            width: (pageWidth * (8 / 100))
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'carestaff_name',
//    //            index: 'carestaff_name',
//    //            width: (pageWidth * (12 / 100))
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'start_time',
//    //            index: 'start_time',
//    //            width: (pageWidth * (8 / 100))
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'service_duration',
//    //            index: 'service_duration',
//    //            width: (pageWidth * (12 / 100))
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'activity_status',
//    //            index: 'activity_status',
//    //            width: (pageWidth * (8 / 100)),
//    //            edittype: "select",
//    //            formatter: function (cellvalue, options, row) {
//    //                //do custom formatting here
//    //                if (row.carestaff_activity_id == null && row.timelapsed) {
//    //                    return '<span style="background:#d61b1b;color:white;font-size: 9px;letter-spacing: 1px;padding: 2px 4px;border-radius: 5px 5px;">Missed</span>';
//    //                }
//    //                var selected = cellvalue == 1 ? ' checked="checked"' : '';

//    //                var htm = "<input disabled type='checkbox'" + selected + "></input>";
//    //                return htm;
//    //            }
//    //        },
//    //        {
//    //            key: false,
//    //            hidden: true,
//    //            name: 'oactivity_status',
//    //            index: 'oactivity_status'
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'completion_date',
//    //            index: 'completion_date',
//    //            formatter: function (cellvalue, options, row) {
//    //                if (row.completion_date)
//    //                    return moment(row.completion_date).format('MM/DD/YYYY');
//    //                return '';
//    //            }
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'acknowledged_by',
//    //            index: 'acknowledged_by'
//    //        },
//    //        {
//    //            key: false,
//    //            name: 'remarks',
//    //            index: 'remarks',
//    //            formatter: function (cellvalue, options, row) {
//    //                if (row.reschedule_dt && row.reschedule_dt != '') {
//    //                    var htm = "<a xref_id='" + row.xref + "'>Rescheduled - " + row.reschedule_dt + "</a>";
//    //                    return htm;
//    //                } else {
//    //                    return cellvalue;
//    //                }

//    //            }
//    //        },
//    //        {
//    //            key: false,
//    //            hidden: true,
//    //            name: 'reschedule_dt',
//    //            index: 'reschedule_dt'
//    //        },
//    //        {
//    //            key: false,
//    //            hidden: true,
//    //            name: 'xref',
//    //            index: 'xref'
//    //        },
//    //        {
//    //            key: false,
//    //            hidden: true,
//    //            name: 'timelapsed',
//    //            index: 'timelapsed'
//    //        }
//    //    ],
//    //    //pager: jQuery('#pager'),
//    //    rowNum: 1000000,
//    //    rowList: [10, 20, 30, 40],
//    //    height: '100%',
//    //    viewrecords: true,
//    //    //caption: 'Care Staff',
//    //    emptyrecords: 'No records to display',
//    //    jsonReader: {
//    //        root: 'rows',
//    //        page: 'page',
//    //        total: 'total',
//    //        records: 'records',
//    //        repeatitems: false,
//    //        id: '0'
//    //    },
//    //    shrinkToFit: false,
//    //    forceFit: true,
//    //    multiselect: false,
//    //    sortname: 'Id',
//    //    sortorder: 'asc',
//    //    loadonce: true,
//    //    onSortCol: function () {
//    //        fromSort = true;
//    //        GURow.saveSelection.call(this);
//    //    },
//    //    loadComplete: function (data) {
//    //        if (typeof fromSort != 'undefined') {
//    //            GURow.restoreSelection.call(this);
//    //            delete fromSort;
//    //            return;
//    //        }

//    //        var maxId = data && data[0] ? data[0].Id : 0;
//    //        if (typeof isReloaded != 'undefined') {
//    //            $.each(data, function (k, d) {
//    //                if (d.Id > maxId) maxId = d.Id;
//    //            });
//    //        }
//    //        if (maxId > 0)
//    //            $("#hGrid").setSelection(maxId);
//    //        delete isReloaded;
//    //    }
//    //});
//    //HGrid.jqGrid('bindKeys');
//    //$('#tabs-5').layout({
//    //    center: {
//    //        paneSelector: "#historyGrid",
//    //        closable: false,
//    //        slidable: false,
//    //        resizable: true,
//    //        spacing_open: 0
//    //    },
//    //    north: {
//    //        paneSelector: "#historyServiceToolbar",
//    //        closable: false,
//    //        slidable: false,
//    //        resizable: false,
//    //        spacing_open: 0
//    //    },
//    //    onresize: function () {
//    //        resizeGridHS();
//    //        resizeGridsS();
//    //    }
//    //});
//    //resizeGridHS();
//}

//added on 9-21-2022 : Cheche 
  $(document).on("click", ".dl_doc", function (i, v) {

        window.location.href = "Admin/DownloadDocumentFile/?DOCID=" + $(this).attr("id");
  })

var initDocuments = function () {
    //if ($('#dGrid')[0] && $('#dGrid')[0].grid)
    //    $.jgrid.gridUnload('dGrid');
    //var DGrid = $('#dGrid');

    //var GURow = new Grid_Util.Row();
    //var pageWidth = $("#dGrid").parent().width() - 100;

    //var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //if (!id) return;
    var id = $('#admForm #txtResidentId').val();
    $("#dGrid").DataTable().destroy();
    $("#docs_table").html('');
    var s = $("#isAdmitted_status").val();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=documents&param=" + id,
        dataType: "json",
        async: false,
        success: function (d) {
            //console.log(d);
            doc_html = " ";
            $.each(d, function (i, doc) {
                // console.log(doc);
                doc_html += "<tr><td style='max-width:700px;word-break:break-all'><a href='#' id='" + doc.UUID + "' class='dl_doc'>" + doc.Filename + "</a></td>"; //added on 9-21-2022 : Cheche 
               // doc_html += "<tr><td style='max-width:700px;word-break:break-all'><a>" + doc.Filename + "</a></td>";
                doc_html += "<td style='max-width:700px;word-break:break-all'>" + doc.Description + "</td>";
                doc_html += "<td>" + doc.Size + "</td>";
                doc_html += "<td>" + doc.Filetype + "</td>";
                doc_html += "<td>" + doc.CreatedBy + "</td>";
                doc_html += "<td>" + doc.DateCreated + "</td>";
                if (s == "false") {
                    doc_html += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item doc_name' id='" + doc.UUID + "' href='#'><i class='la la-edit'></i> Download</a>\n</div>\n</span></td>";
                }
                else {
                    doc_html += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item doc_name' id='" + doc.UUID + "' href='#'><i class='la la-edit'></i> Download</a>\n<a class='dropdown-item del doc_delete' href='#' id='" + doc.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";

                }
              //  doc_html += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item doc_name' id='" + doc.UUID + "' href='#'><i class='la la-edit'></i> Download</a>\n<a class='dropdown-item del doc_delete' href='#' id='" + doc.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                doc_html += "</tr>";
                //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";

            })

            $("#docs_table").html(doc_html);
            $("#dGrid").DataTable({});
            //$("#dGrid").DataTable({
            //    responsive: !0,
            //    //"scrollX": true,
            //    "scrollY": true,
            //    "scrollCollapse": true,
            //    //select: true
            //});
        }

    });



    $('.doc_name').on('click', function () {
        var did = $(this);
        var doc_uuid = did.attr('id');
        //$('#document_id').val(id);
        window.location.href = "Admin/DownloadDocumentFile/?DOCID=" + doc_uuid;
    });


    $('.doc_delete').on('click', function () {
        var did = $(this);
        var doc_uuid = did.attr('id');
        if (!doc_uuid) return;
        //if (!confirm("Are you sure you want to delete the selected row?")) return;
        swal({
            title: "Are you sure you want to delete this document?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!"
        }).then(function (e) {
            e.value &&
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify({
                    func: 'documents',
                    mode: 3,
                    data: {
                        docId: doc_uuid
                    }
                }),
                dataType: "json",
                success: function (m) {
                    if (m.result == 0) {
                        swal("Document has been deleted successfully!", "", "success");
                        initDocuments();
                    } else
                        $.growlError({
                            message: m.message,
                            delay: 6000
                        });
                }
            });

        });



    });
}

function truncate(n) {
  //change the value of len to prevent overlapping text for a filename when uploading multiple documents : 10-25-2022 : Cheche
   // var len = 40;
    var len = 30;
    var ext = n.substring(n.lastIndexOf(".") + 1, n.length).toLowerCase();
    var filename = n.replace('.' + ext, '');
    if (filename.length <= len) {
        return n;
    }
    var flen = filename.length;
    var f1 = filename.substr(0, (len / 2));
    var f2 = filename.substr((flen - (len / 2)), flen);
    filename = f1 + '...' + f2;
    return filename + '.' + ext;
}

function uploadDocument() {
    //var dialog_html = "<div class=\"form\"><ul class=\"w100\">" +
    //    "<li><label for=\"fileName\">Name:</label><input type=\"text\" id=\"fileName\" /></li>" +
    //    "<li><label for=\"fileDescription\">Description:</label><textarea id=\"fileDescription\"></textarea></li>" +
    //    "<li><label for=\"fileUpload\">File:</label><input id=\"fileUpload\" type=\"file\" name=\"files[]\" class=\"fileUpload\">" + 
    //    "<input id=\"fileText\" class=\"fileText\" type=\"text\" disabled=\"disabled\"/></li>" +
    //    "</ul></div><div id=\"uploadProgress\" class=\"progressPanel\"><div class=\"progressBar\" style=\"width: 0%;\"></div></div>";

    var dialog_html = "<div class='upload-container'><div class='upload-header'>" +
        "<button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Clear List</button>" +
        "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +
        "<div id='fileList' class='upload-list'></div><div class='upload-footer'><div id='divLegend'>" +        
        //kim modified upload button will not disappear with clear list 03/01/22
        "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded </div>" +
        "<button id='btnUploadfiles' class='btn btn-outline' style='margin-left: 350px;background-color: #5bfda3 !important;border-color: #5bfda3 !important;'>Upload Files</button></div></div ></div > ";

    $(".documentdiv").html(dialog_html);

    var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
        "<tr><td style='width:73%'><span class='filename'></span></td>" +
        "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
        "<td style='width:2%'><a class='status-loading' style='display:none'>&nbsp;</a></td>" +
        //"<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" + //remove upload.remove here and added attribute fname: angelie
        "<td style='width:2%'><a class='status-done' id='filename' fname='fname'>&nbsp;</a></td></tr>" +
        "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

    //var $dialog = $("<div><div class=\"dialogContent\">" + dialog_html + "</div></div>").dialog({
    //    modal: true,
    //    title: "Upload File",
    //    width: 500,
    //    close: function () {
    //        $dialog.find("#fileupload").fileupload("destroy");
    //        initDocuments();
    //        $dialog.dialog("destroy").remove();
    //    },
    //    buttons: {
    //        "Close": function () {
    //            $dialog.dialog("close");
    //        }
    //    }
    //});
    //$("#btnUpload").attr('disabled', 'disabled').removeClass('primaryBtn').addClass('primaryBtn-disabled');
    $("#fileUpload").on('change', function () {
        var itms = $('.upload-container .totalfiles span:eq(1)');
        if (parseInt(itms.text()) > 0)
            itms.text(parseInt(itms.text()) + this.files.length);
        else
            itms.text(this.files.length);
    });
    $('#btnAddfiles').on('click', function () {
        $("#fileUpload").trigger('click');
    });
    $('#btnClearList').on('click', function () {
        $('#fileList').html('');
        $('.totalfiles').html("<span>0</span> of <span>0</span> Files Uploaded");
    });

    //$('#fileUpload').on('click', function () {
    $("#uploaddocument_modal").find("#fileUpload").fileupload({
        url: "Admin/UploadFile",
        dataType: "json",
        done: function (e, data) {
            data.context.find('a:eq(0)').hide();
            if (data.files.length > 0) {
                var dat = data.result;
                if (dat.result != 0) {
                    data.context.find('.innerprog').css('background', 'red');
                    var tt = $('<div/>').html(dat.message).text();
                    data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');

                    if (dat.message == "Duplicate resident document") {
                        swal("Duplicate resident file uploaded.", "", "warning")
                    } 
                  
                } else {
                    data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');
                    var itms = $('.upload-container .totalfiles span:eq(0)');

                    if (parseInt(dat.message) > 0) {
                        itms.text(parseInt(itms.text()) + 1);
                        //toastr.success("Document uploaded successfully.");
                        //swal("File successfully uploaded.", "", "success")

                        swal({
                            title: "Document uploaded successfully.",
                            text: "",
                            type: "success",
                            confirmButtonText: "Ok"
                        }).then(function (e) {
                            $('#uploaddocument_modal').modal('hide');
                            initDocuments();
                        });
                    }
                }
                return false;
            }
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            var item = data.context;
            if (item != null) {
                var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                item.find('span.size').text(size);
            }
            data.context.find('.innerprog').css("width", progress + "%");
        },
        add: function (e, data) {
            var files = data.files;
            if (data.items == null)
                data.items = [];

            //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
            var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                'csv', 'txt', 'rtf', 'zip', 'mp3', 'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif'
            ];

            $.each(files, function (x, i) {
                var err = "";
                //validation
                var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                if ($.inArray(ext, acceptedExts) == -1)
                    err = "The file type of '." + ext + "' is not supported.";
                if (this.size > 20000000)                   
                    err = "The file size limit exceeded the maximum of 20MB.";
             //kim change size 11/17/22
                if (this.size <= 1000)
                    err = "The file size is to small, please select a file greater than 1KB";

                var name = truncate(this.name);
                var size = (this.size / 1000) + 'KB';
                var item = $(uploadItem).appendTo('#fileList');
                item.find('span.filename').text(name).attr('title', this.name);
                item.find('a#filename').attr('fname', name);
                item.find('span.size').text(size);
                if (err != '') {
                    item.find('.innerprog').width('100%').css('background', 'red');
                    item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                    swal("Error.", err, "warning");
                    return true;
                } else {
                    item.find('a:eq(0)').show();
                    item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {

                        if (data.files != null) {
                            for (i = 0; i < data.files.length; i++) {
                                if (data.files[i].name == $(this).attr('fname')) {
                                    data.files[i] = {};
                                }
                            }
                        }
                        item.remove();
                        var itms = $('.upload-container .totalfiles span:eq(1)');
                        itms.text(parseInt(itms.text()) - 1);

                    });
                }
                data.context = item;
                data.urlparam = {
                    name: this.name,
                    size: this.size,
                    type: this.type,
                    file: this.file
                };
                $('#btnClearList').on('click', function () {
                   
                    data.urlparam = {};
                    data.files = [''];

                  
                    $('#filelist').empty();
                    $('.totalfiles').html("<span>0</span> of <span>0</span> files uploaded");
                });

                $('#btnUploadfiles').on('click', function () {
                    data.submit();
                });

            });
        },
        submit: function (e, data) {
            var rid = $('#dresid').val();
            data.formData = {
                paramsid: "documents",
                fileparams: JSON.stringify(data.urlparam),
                rid: rid
            };
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.growlError({
                message: "Error: " + jqXHR.responseText,
                delay: 6000
            });
        }
    });
}

var showdiagNewAgreement = function (mode) {
    //$("#diagNewAgreement").dialog("option", {
    //    width: 420,
    //    height: 120,
    //    position: "center",
    //    buttons: {
    //        "Save": {
    //            click: function () {
    //var isValid = $('#diagNewAgreement').validate();
    //if (!isValid) return;

    if (!moment($('#NewAgreementDate').val(), 'MM/DD/YYYY', true).isValid()) {
        swal('Please input a valid date.');
        $('#NewAgreementDate').val('').focus();
        return;
    }
    if ($('#txtAdmissionId').val() == '')
        return;
    var data = {
        AgreementDate: $('#NewAgreementDate').val(),
        AdmissionId: $('#txtAdmissionId').val(),
        ResidentId: $('#txtResidentId').val(),
        TMR: $('#AATotalRate').val()
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/CreateNewAgreement",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {

            if (m.result > 0) {
                //$.growlSuccess({
                //    message: "New Admission Agreement for '" + $('#NewAgreementDate').val() + "' created successfully!",
                //    delay: 6000
                //});

                toastr.success("New Admission Agreement for '" + $('#NewAgreementDate').val() + "' created successfully!");

                $('#SelectAgreementDate').append($('<option>', {
                    value: m.result,
                    text: $('#NewAgreementDate').val()
                }));
                //$('#ansBody').clearFields();
                $('#SelectAgreementDate').val(m.result).trigger('change');
                $("#diagNewAgreement").dialog("close");
            }
        }
    });
    //            },
    //            class: "primaryBtn",
    //            text: "Save"
    //        },
    //        "Cancel": function () {
    //            $("#diagNewAgreement").dialog("close");
    //        }
    //    }
    //}).dialog("open");
}

var showdiagNewAppraisal = function (mode) {

    if (!moment($('#NewAppraisalDate').val(), 'MM/DD/YYYY', true).isValid()) {
        swal('Please input a valid date.');
        $('#NewAppraisalDate').val('').focus();
        return;
    }
    if ($('#txtAdmissionId').val() == '')
        return;
    var data = {
        AppraisalDate: $('#NewAppraisalDate').val(),
        AdmissionId: $('#txtAdmissionId').val(),
        ResidentId: $('#txtResidentId').val(),
        Age: $('#ANSAge').val(),
        DOB: $('#ANSDOB').val()
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/CreateNewAppraisal",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (m.result > 0) {
                //$.growlSuccess({
                //    message: "New Appraisal for '" + $('#NewAppraisalDate').val() + "' created successfully!",
                //    delay: 6000
                //});
                toastr.success("New Appraisal for '" + $('#NewAppraisalDate').val() + "' created successfully!");
                $('#SelectAppraisalDate').append($('<option>', {
                    value: m.result,
                    text: $('#NewAppraisalDate').val()
                }));
                              
                //$('#ansBody').clearFields();
                $('#SelectAppraisalDate').val(m.result).trigger('change');
                $("#diagNewAppraisal").dialog("close");
            }
        }
    });

    //                //var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //                //row = $('#grid').jqGrid('getRowData', id);
    //                //doAjax(mode, row, function (m) {
    //                //    if (m.result == 0) {
    //                //        $.growlSuccess({ message: "Role " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
    //                //        $("#diag").dialog("close");
    //                //        if (mode == 1) isReloaded = true;
    //                //        else {
    //                //            fromSort = true;
    //                //            new Grid_Util.Row().saveSelection.call($('#grid')[0]);
    //                //        }
    //                //        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    //                //    } else {
    //                //        $.growlError({ message: m.message, delay: 6000 });
    //                //    }
    //                //});
    //            },
    //            class: "primaryBtn",
    //            text: "Save"
    //        },
    //        "Cancel": function () {
    //            $("#diagNewAppraisal").dialog("close");
    //        }
    //    }
    //}).dialog("open");
}

var createExcelFromGrid = function (gridID, filename) {
    var grid = $('#' + gridID);
    var rowIDList = grid.getDataIDs();
    var row = grid.getRowData(rowIDList[0]);
    var colNames = [];
    var i = 0;
    for (var cName in row) {
        colNames[i++] = cName; // Capture Column Names
    }
    var html = "";
    for (var j = 0; j < rowIDList.length; j++) {
        row = grid.getRowData(rowIDList[j]); // Get Each Row
        for (var i = 0; i < colNames.length; i++) {
            html += row[colNames[i]] + ';'; // Create a CSV delimited with ;
        }
        html += '\n';
    }
    html += '\n';

    var a = document.createElement('a');
    a.id = 'ExcelDL';
    a.href = 'data:application/vnd.ms-excel,' + html;
    a.download = filename ? filename + ".xls" : 'DataList.xls';
    document.body.appendChild(a);
    a.click(); // Downloads the excel document
    document.getElementById('ExcelDL').remove();
}

//Added for Exporting to Grid to Excel (-ABC)
function fnExcelReport() {
    $("td:hidden,th:hidden", "#grid").remove();
    var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'></td><td bgcolor='#87AFC6'>Resident</td></tr><tr>";
    var textRange;
    var j = 0;
    tab = document.getElementById('grid'); // id of table


    for (j = 0; j < tab.rows.length; j++) {
        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, ""); //remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");


    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
    {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Residents.xls");
    } else //other browser not tested on IE 11
        var a = document.createElement('a');
    a.id = 'ExcelDL';
    a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
    a.download = 'ALC_Residents' + '.xls';
    document.body.appendChild(a);
    a.click(); // Downloads the excel document
    document.getElementById('ExcelDL').remove();
}

function getDateOccured(txtAdmissionId) {
    $.get(ALC_URI.GuestNotification + "/GetDateOccured?id=" + txtAdmissionId, function (data) {
        $('#selectDateOccured').html('');
        $.each(data, function (i, v) {
            //console.log(data);
            if (data != "" || data != null) {
                $("#dtlabel").show();
                $("#selectDateOccured").show();
                $("#printUIR").show();
                $('#selectDateOccured').append($('<option>', {
                    value: v.UIR_id,
                    text: v.UIRDateOccured1
                }));
            }
        });


        uirid = $('#selectDateOccured').val();
        $("#addNewUIR").val(uirid);
        $("#UIRDateOccuredId").val(uirid);
        $("#printUIR").val(uirid);

        $("#selectDateOccured").attr("disabled", false);

    });
}

function setHeight() {
    $('.parent, .card').height(250);
    $('.card-img-top').height(($('.card').height() / 2));
    $('#residentForm .tab-content ').height($(window).height() - 300);
    $('#admForm .adm-content').height($(window).height() - 370);
    $('#files_modal .filesdiv ').height($(window).height() - 270);
    $('#tabs-ass form').height($(window).height() - 480);
    $("#divfile2").height($("#divfile1").height());

    if ($(window).height() < 400) {
        $('#residentForm .tab-content ').height(600);
        $('#tabs-ass form').height(500);
    }

    $(window).resize(function () {
        $('.parent, .card').height(250);
        $('.card-img-top').height(($('.card').height() / 2));
        $('#residentForm .tab-content ').height($(window).height() - 300);
        $('#admForm .adm-content').height($(window).height() - 370);
        $('#files_modal .filesdiv ').height($(window).height() - 270);
        $('#tabs-ass form').height($(window).height() - 480);
        $("#divfile2").height($("#divfile1").height());
        if ($(window).height() < 400) {
            $('#residentForm .tab-content ').height(600);
            $('#tabs-ass form').height(500);
        }
    })
}


$(document).ready(function () {


    //Added on 3-31-2022 : Alona 
    //function getpayer() {

    //    $.ajax({
    //        url: "Admin/Search",
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        type: "POST",
    //        data: '{func: "search_payer"}',
    //        success: function (d) {
    //            document.getElementById('Payers').innerHTML = "";
    //            for (let i = 0; i < d.length; i++) {
    //                if (d[i].payer_name != null) {
    //                    var option = "<option value='" + d[i].payer_name + "'></option>"
    //                    document.getElementById('Payers').innerHTML += option;
    //                }

    //            }
    //            //response($.map(d, function (item) {// not needed : 4-4-2022 changed data to d
    //            //    debugger
    //            //        return {
    //            //            label: item.reference,
    //            //            value: item.id
    //            //        }
    //            //}));
    //        }
    //    });
    //}
  


    //ends here

    //ADDED BY RDP 2-26-25
    $("#btn-printsr").attr("disabled", true);

    $(".loader").show();
    $(document).click();
    agency_ishomecare = $("#agency_ishomecare").val();

    setTimeout(function () {
        load_residents('active_only');
        setHeight();
        $(".loader").hide();
    }, 300)

    //Added this to format the birthdate ANGELIE: 01-04-2022
    $("#txtBirthdate").keyup(function () {

        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var bday = $("#txtBirthdate").val().trim();
        var m = "";
        var d = "";
        var y = "";
        var error = document.getElementById("bday_error_res");

        var isCorrectFormat = /^(0?[1-9]|1[0-2])\/(0?[1-9]|1\d|2\d|3[01])\/(18|19|20)\d{2}$/.test(bday);
        var isCorrectFormatWithoutSlash = /^(0?[1-9]|1[0-2])(0?[1-9]|1\d|2\d|3[01])(18|19|20)\d{2}$/.test(bday);
        var wordFormat = /^((January|March|May|July|August|October|December)(\s|\S)(0?[1-9]|1\d|2\d|3[0-1])|(April|June|September|November)(\s|\S)(0?[1-9]|1\d|2\d|30)|(February)(\s|\S)(0?[1-9]|1\d|2\d))(\,|\,\s|\,\S|\s|\S)(18|19|20)\d{2}$/.test(bday);
        if (wordFormat) {
            for (var i = 0; i <= months.length; i++) {
                if (bday.includes(months[i])) {
                    m = i + 1;

                    if (m <= 9) {
                        m = "0" + m;
                    }

                    var date_year = bday.replace(months[i], "");
                    date_year = date_year.trim();

                    if (date_year.includes(",")) {
                        date_year = date_year.split(",");
                    } else {
                        date_year = date_year.split(" ");
                    }

                    d = date_year[0].trim();
                    y = date_year[1].trim();
                }
            }

            $("#txtBirthdate").val(m + "/" + d + "/" + y);
            error.textContent = " ";
        } else if (isCorrectFormatWithoutSlash && bday.length == 8) {

            m = bday.slice(0, 2);
            d = bday.slice(2, 4);
            y = bday.slice(4, 8);

            $("#txtBirthdate").val(m + "/" + d + "/" + y)
            error.textContent = " ";
        } else if (isCorrectFormat) {
            $("#txtBirthdate").val(bday);
            error.textContent = " ";
        } else {
            error.textContent = "Please enter a valid date in this format 'MM/DD/YYYY'!  ";
            error.style.color = "red";
        }

    })

    $("#txtDateLeft").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        autoclose: true,
    }).on('changeDate', function () {
        $(this).datepicker('hide');
    }); //("option", "dateFormat", "mm/dd/yy");


    //$('.parent , .card').width($(window).width() - '90%');
    //$(window).resize(function () {
    //    $('.parent , .card').width($(window).width() - '90%');
    //})

    var rd = 0;
    //if (typeof (PHL) !== 'undefined') {
    //    if (PHL.destroy) PHL.destroy();
    //}



    $("#statusSelect").on("change", function () {

        var status = $("#statusSelect option:selected").text();
        if (status == 'All') {

            $(".loader").show();
            setTimeout(function () {
                load_residents('');
                setHeight();
            }, 300)
        }
        else if (status == 'Active') {

            $(".loader").show();
            setTimeout(function () {
                load_residents('active_only');
                setHeight();
            }, 300)
        }
        else if (status == 'Discharged') {

            $(".loader").show();
            setTimeout(function () {
                load_residents('inactive_only');
                setHeight();
            }, 300)
        }
    });

    $(document).on("click", ".srvc_delete", function () {
        //  debugger
        //$("#.srvc_delete").on("click", function () {
        var id = $(this);
        var s_id = id.attr('id');
        setTimeout(function () {
            if (s_id) {
                //       debugger

                swal({
                    title: "Are you sure you want to delete the selected row? If yes, press \"Ok\ to continue.",
                    text: "",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonText: "Ok"
                }).then(function (e) {
                    if (e.value == true) {
                        doAjax2(3, {
                            Id: s_id
                        }, function (m) {
                            //  debugger
                            var msg = "Task deleted successfully!";
                            if (m.result == -3) //inuse
                                msg = "Task cannot be deleted because it is currently in use.";

                            toastr.success(msg);
                            initService();
                        });
                    }

                });
            }
        }, 300)
    });

    $(document).on("click", ".srvc_delete_hc", function () {
        //  debugger
        //$("#.srvc_delete").on("click", function () {
        var id = $(this);
        var s_id = id.attr('id');
        setTimeout(function () {
            if (s_id) {
                //       debugger

                swal({
                    title: "Are you sure you want to delete the selected row? If yes, press \"Ok\ to continue.",
                    text: "",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonText: "Ok"
                }).then(function (e) {
                    if (e.value == true) {

                        doAjax2_hc(3, {
                            Id: s_id
                        }, function (m) {
                            //  debugger
                            var msg = "Service deleted successfully!";
                            if (m.result == -3) //inuse
                                msg = "Task cannot be deleted because it is currently in use.";

                            toastr.success(msg);
                            initService();
                        });
                    }

                });
            }
        }, 300)
    });

    $('#SelectAgreementDate').on('change', function () {
        var agg_id = $(this).val();
        $.get(ALC_URI.Admin + "/GetAgreement?id=" + $(this).val(), function (dt) {
            $('#tabs-4a').clearFields();
            $('#AAResidentName').val($("#txtResidentName").val());
            $('#AAResidentName2').val($("#txtResidentName").val());
            $('#AADOB').val($('#txtBirthdate').val());
            $('#AADOB2').val($('#txtBirthdate').val());
            $('#AARoomNo').val($('#DRoomId option:selected').text());
            $('#AARoomNo2').val($('#DRoomId option:selected').text());
            $('#AASSNo').val($('#txtSSN').val());
            $('#AASSNo2').val($('#txtSSN').val());
            $('#AAAdmissionDate, #AAAdmissionDate2').val($('#txtAdmissionDate').val());
            $("#AATotalRate2").val($("#AATotalRate").val());
            $('#tabs-4a').mapJson(dt, 'id');
            $('#SelectAgreementDate').val(agg_id);
            $('#AAAgreementID').val(agg_id);
        });
    });

    //$('#btnNewAgr').off('click').on('click', function () {
    //   });

    $("#NewAgreementDate").datepicker().on('changeDate', function () {
        $(this).datepicker('hide');
    });

    $("#btnSaveAggreement").on("click", function () {
        showdiagNewAgreement();
    });



    $('#SelectAppraisalDate').off('change').on('change', function () {
        $('#ANSAppraisalID').val($(this).val());
        var ans_id = $(this).val();
        $.get(ALC_URI.Admin + "/GetAppraisalNS?id=" + $(this).val(), function (dt) {
            $('#tabs-3a').clearFields();
            //$('#tabs-3a input:checkbox').prop('checked', false);


            $('#ANSResidentName').val($("#txtResidentName").val());
            $('#ANSDOB').val($('#txtBirthdate').val());
            if ($('#ddlGender').val() == 'M') {
                $('#ANSMale').prop('checked', true);
                $('#ANSFemale').prop('checked', false);
            } else if ($('#ddlGender').val() == 'F') {
                $('#ANSMale').prop('checked', false);
                $('#ANSFemale').prop('checked', true);
            }
            $('#tabs-3a').mapJson(dt, 'id');
            $('#SelectAppraisalDate').val(ans_id);
            $('#ANSAppraisalID').val(ans_id);

            if ($('#ANSAge').val() == "") {
                var age = getAge($('#txtBirthdate').val());
                $('#ANSAge,#ANSAge2').val(age);
            }

            $('#ANSAge2').val($('#ANSAge').val());
            // $('#tabs-3a section').mapJson(dt, 'id');
            $("#ANSDate").val($('#NewAppraisalDate').val()); //autopopulate date field from appraisal date field
        });
    });

    $("#btnSaveAppraisal").on("click", function () {
        showdiagNewAppraisal();
    });


    $("#fromDate, #hfromDate").datepicker({
        formatDate: "MM/dd/yyyy",
        onClose: function () {
        }
    }).on('changeDate', function (selected) {
         $(this).datepicker('hide');
        // kim add code here to limit selection of the date 02/10/22
        if($("#hfromDate").val() != null){
            var minDate = new Date(selected.date.valueOf()); 
            $('#toDate').datepicker('setStartDate', minDate);
            $(this).datepicker('hide');
            var dis = $(this);
            if (moment($("#toDate").val()) < moment($("#fromDate").val())) {
                swal('"To Date" should be greather or equal to "From Date"');
                return;
            }
            if (moment($("#htoDate").val()) < moment($("#hfromDate").val())) {
                swal('"To Date" should be greather or equal to "From Date"');
               return;
            }
        }
        
        var completion_date = $("#fromDate").val();
        var completion_date_end = $("#toDate").val();
        
        //modified codes to limit to 1 month to avoid overloading : Cheche : 11-28-2022
         if(completion_date == ""){
            swal('"Please select "From Date"');
        }else if(completion_date_end == "")
        {
            swal('"Please select "To Date"');
        }else{
            var ndate = new Date(completion_date);
            var ndateEnd = new Date(completion_date_end);
            var limitMonth = ndate.getMonth() + 1;
            var limitDate = ndate.getDate();
            var limitYear = ndate.getFullYear();
            var limitMonth1 = ndateEnd.getMonth() + 1;
            var limitDate1 = ndateEnd.getDate();
            var limitYear1 = ndateEnd.getFullYear();
            var diffMonth = limitMonth1 - limitMonth;
        
       
            if(limitMonth < limitMonth1 && limitDate < limitDate1 && limitYear == limitYear1 || diffMonth > 1 || limitMonth < limitMonth1 && limitDate < limitDate1 && limitYear == limitYear1)
            {
                swal('"To Date" should be equal or less than one month to "From Date"');
                return;
            }else{
            if (completion_date == "") {
                var date = new Date();
                completion = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
            } else {
                completion = completion_date + " 12:00:00 AM";
            }
            if (completion_date_end == "") {
                var date = new Date();
                completion_end = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
                $("#toDate").text((date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear());
            } else {
                completion_end = completion_date_end + " 12:00:00 AM";
            }
        
             generateWeeklySchedule(completion, completion_end, "All");
        
            }
        }
        
        
    });
    $("#toDate, #htoDate").datepicker({
        formatDate: "MM/d//yyyy",
        onClose: function () {
        }
    }).on('changeDate', function (selected) {
        $(this).datepicker('hide');
        // kim add code here to limit selection of the date 02/10/22
        if($("#htoDate").val() != null){
            var minDate = new Date(selected.date.valueOf());
            $('#fromDate').datepicker('setEndDate', minDate);
            $(this).datepicker('hide');
            var dis = $(this);
            //if (dis.val() == "")
            //    dis.val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));

            if (moment($("#toDate").val()) < moment($("#fromDate").val())) {
                swal('"To Date" should be greather or equal to "From Date"');
                //kim comment out return date last selected 01/28/2022
                //   $(this).val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));
                //  $(this).datepicker('update', new Date);           
                return;
            }
            if (moment($("#htoDate").val()) < moment($("#hfromDate").val())) {
                swal('"To Date" should be greather or equal to "From Date"');
                //kim comment out return date last selected 01/28/2022
                //   $(this).val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));          
                //  $(this).datepicker('update',new Date);
                return;
            }
        }

        //initHistory();
        var completion_date = $("#fromDate").val();
        var completion_date_end = $("#toDate").val();

        //modified codes to limit to 1 month to avoid overloading : Cheche : 11-28-2022
        if(completion_date == ""){
            swal('"Please select "From Date"');
        }else if(completion_date_end == "")
        {
            swal('"Please select "To Date"');
        }else{
      
            var ndate = new Date(completion_date);
            var ndateEnd = new Date(completion_date_end);
            var limitMonth = ndate.getMonth() + 1;
            var limitDate = ndate.getDate();
            var limitYear = ndate.getFullYear();
            var limitMonth1 = ndateEnd.getMonth() + 1;
            var limitDate1 = ndateEnd.getDate();
            var limitYear1 = ndateEnd.getFullYear();
            var diffMonth = limitMonth1 - limitMonth;

            if(limitMonth < limitMonth1 && limitDate < limitDate1 && limitYear == limitYear1 || diffMonth > 1)
            {
                swal('"From Date" should be equal or less than one month to "To Date"');
                return;
            }else{
            if (completion_date == "") {
                var date = new Date();
                completion = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
            } else {
                completion = completion_date + " 12:00:00 AM";
            }
            if (completion_date_end == "") {
                var date = new Date();
                completion_end = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
                $("#toDate").text((date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear());
            } else {
                completion_end = completion_date_end + " 12:00:00 AM";
            }
        
           //$("#hGrid").DataTable().destroy();
            generateWeeklySchedule(completion, completion_end, "All");
            //$("#hGrid").DataTable({
            //    "scrollY": "400px",
            //    "scrollCollapse": true,
            //    stateSave: true,
            //    responsive: !0,
            //    pagingType: "full_numbers",
            //    dom: 'lBfrtip'
            //});
            }
        }
        
        
    });

    $(".admCloseBtn").on("click", function () {
        $('#files_modal').modal('hide');
    });

    var buildCalendar = function () {
        var date = new Date(),
            n = date.getMonth(),
            y = date.getFullYear();

        var month = n + 1;
        var year = y;
        minDate = getMonthDateRange(year, month);
        buildCal(minDate);
    }
    $("#printAdmissionFile").on("click", function () {

        isSaveAndPrint = true;

        $("#btnSaveAdmission").trigger("click");


        let timerInterval
        Swal.fire({
            title: 'Your file is being processed.',
            html: '<strong></strong>',
            timer: 4000,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                        .textContent = Swal.getTimerLeft()
                }, 100)
            },
            onClose: () => {
                isSaveAndPrint = false;
                clearInterval(timerInterval)

            }
        }).then((result) => {
            if (
                result.dismiss === Swal.DismissReason.timer
            ) {
                var admissionId = $("#txtAdmissionId").val();
                var form_id = $(this).attr("docname");
                if (form_id == "AAform") {
                    var agreementDate = $("#SelectAgreementDate option:selected").val();

                    if (agreementDate == null) {
                        swal("This resident has no Signed Admission Agreement. Please create one by filling in the form and clicking the 'Save' button that can be found on the lower portion of the form.", "", "warning");

                    } else {
                        window.open(ALC_URI.Admin + "/PrintAdmissionAgreement?id=" + admissionId + '&id2=' + agreementDate, '_blank');
                    }

                    return false;

                } else if (form_id == "EIform") {
                    window.open(ALC_URI.Admin + "/PrintResidentIdentificationEmergency?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "PRform") {
                    window.open(ALC_URI.Admin + "/PrintPhysicianReport?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "RMIform") {
                    window.open(ALC_URI.Admin + "/PrintReleaseMedicalInfo?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "CMEform") {
                    window.open(ALC_URI.Admin + "/PrintConsentMedExam?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "CETform") {
                    window.open(ALC_URI.Admin + "/PrintConsentMedicalTreatment?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "PAform") {
                    window.open(ALC_URI.Admin + "/PrintPreplacement?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "RAform") {
                    window.open(ALC_URI.Admin + "/PrintResidentAppraisal?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "NSPform") {

                    var appraisalDate = $("#SelectAppraisalDate option:selected").val();

                    if (appraisalDate == null) {
                        swal("This resident has no Appraisal/Needs and Service Plan. Please create one by filling in the form and clicking the 'Save' button that can be found on the lower portion of the form.", "", "warning");
                    } else {
                        window.open(ALC_URI.Admin + "/PrintAppraisalNeedAndService?id=" + admissionId + '&id2=' + appraisalDate, '_blank');
                    }

                    return false;

                } else if (form_id == "PRSform") {
                    window.open(ALC_URI.Admin + "/PrintPersonalRights?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "TDNform") {
                    window.open(ALC_URI.Admin + "/PrintTelecommunicationNotif?id=" + admissionId, '_blank');
                    return false;

                } else if (form_id == "PPRform") {
                    swal("This form is unavailable.", "", "danger")
                    return false;
                } else if (form_id == "RBTLform") {
                    swal("This form is unavailable.", "", "danger")
                    return false;
                } else if (form_id == "RMDTform") {
                    window.open(ALC_URI.Admin + "/PrintDecisionRights?id=" + admissionId, '_blank');
                    return false;
                } else if (form_id == "UIRform") {

                    var incidentDate = $("#selectDateOccured option:selected").val();


                    if (incidentDate == null) {
                        swal("This resident has no Unusual Incident/Injury Report. To create one, fill in the form and click the 'Save' button that can be found on the lower portion of the form.", "", "warning");
                    } else {
                        window.open(ALC_URI.Admin + "/PrintIncidentReport?id=" + incidentDate, '_blank');
                    }

                    return false;
                } else if (form_id == "RWRform") {
                    swal("This form is unavailable.", "", "danger")
                    return false;
                } else if (form_id == "CSMform") {
                    swal("This form is unavailable.", "", "danger")
                    return false;
                } else if (form_id == "PCform") {
                    window.open(ALC_URI.Admin + "/PrintPhotographyConsent?id=" + admissionId, '_blank');
                    return false;

                }
            }

        })


    })

    $('#AAPeriodRate,#AAProratedRate,#AAOnetimeFee, #AAAdvancePay').off('change').on('change', function () {

        period_rate = $("#AAPeriodRate").val();
        prorated_rate = $("#AAProratedRate").val();
        onetime_fee = $("#AAOnetimeFee").val();
        advance_pay = $("#AAAdvancePay").val();

        period_rate = (period_rate == "" ? 0 : parseInt(period_rate));
        prorated_rate = (prorated_rate == "" ? 0 : parseInt(prorated_rate));
        onetime_fee = (onetime_fee == "" ? 0 : parseInt(onetime_fee));
        advance_pay = (advance_pay == "" ? 0 : parseInt(advance_pay));

        total_due = (period_rate + prorated_rate + onetime_fee) - advance_pay;
        total_due = (total_due >= 0 ? total_due : 0)
        $('#AATotalDue').val(total_due);

    });

    // Auto population of address starts here: 07-12-22
    var SearchTextCity = "City";
    $("#txtCity").val(SearchTextCity).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextCity);
    }).focus(function (e) {
        if ($(this).val() == SearchTextCity)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "city"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtCity").val(ui.item.city);
            $("#txtRState").val(ui.item.state);
            $("#txtRStateId").val(ui.item.state_id);
            $("#txtZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search City...") {
                $("#txtRState").val("");
                $("#txtRStateId").val("");
                $("#txtZip").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.city + "</a>")
            .appendTo(ul);
    };


    var SearchTextState = "State";
    $("#txtRState").val(SearchTextState).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextState);
    }).focus(function (e) {
        if ($(this).val() == SearchTextState)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "state"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtCity").val(ui.item.city);
            $("#txtRState").val(ui.item.state);
            $("#txtRStateId").val(ui.item.state_id);
            $("#txtZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search State...") {
                //$("#txtRState").val("");
                //$("#txtRStateId").val("");
                $("#txtCity").val("");
                $("#txtZip").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.state + "</a>")
            .appendTo(ul);
    };


    var SearchTextZip = "";
    $("#txtZip").val(SearchTextZip).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextZip);
    }).focus(function (e) {
        if ($(this).val() == SearchTextZip)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "zip"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtCity").val(ui.item.city);
            $("#txtRState").val(ui.item.state);
            $("#txtRStateId").val(ui.item.state_id);
            $("#txtZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search Zipcode...") {
                $("#txtRState").val("");
                $("#txtRStateId").val("");
                $("#txtCity").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.zipcode + " - " + item.city + "</a>")
            .appendTo(ul);
    };

    //responsible person
    var SearchTextCity = "";
    $("#txtResCity").val(SearchTextCity).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextCity);
    }).focus(function (e) {
        if ($(this).val() == SearchTextCity)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "city"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtResCity").val(ui.item.city);
            $("#txtResState").val(ui.item.state);
            $("#txtResStateId").val(ui.item.state_id);
            $("#txtResZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search City...") {
                $("#txtResState").val("");
                $("#txtResStateId").val("");
                $("#txtResZip").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.city + "</a>")
            .appendTo(ul);
    };


    var SearchTextState = "";
    $("#txtResState").val(SearchTextState).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextState);
    }).focus(function (e) {
        if ($(this).val() == SearchTextState)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "state"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtResCity").val(ui.item.city);
            $("#txtResState").val(ui.item.state);
            $("#txtResStateId").val(ui.item.state_id);
            $("#txtResZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search State...") {
                //$("#txtResState").val("");
                //$("#txtResStateId").val("");
                $("#txtResCity").val("");
                $("#txtResZip").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.state + "</a>")
            .appendTo(ul);
    };


    var SearchTextZip = "";
    $("#txtResZip").val(SearchTextZip).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextZip);
    }).focus(function (e) {
        if ($(this).val() == SearchTextZip)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_city", name:"' + request.term + '", param: "zip"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            zipcode: item.zipcode,
                            city: item.city,
                            state: item.state_name,
                            state_id: item.state_id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {

            $("#txtResCity").val(ui.item.city);
            $("#txtResState").val(ui.item.state);
            $("#txtResStateId").val(ui.item.state_id);
            $("#txtResZip").val(ui.item.zipcode);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search Zipcode...") {
                $("#txtResState").val("");
                $("#txtResStateId").val("");
                $("#txtResCity").val("");
                return false;
            }
            //$("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.zipcode + "</a>")
            .appendTo(ul);
    };


    // Auto population ends here 


    $("#AAMonthlyRate, #AAOpServices").on('change', function () {

        mr = $("#AAMonthlyRate").val();
        add_mr = $("#AAOpServices").val();

        if (mr == "") {
            mr = 0;
            $("#AAMonthlyRate").val(0);
        }

        if (add_mr == "") {
            add_mr = 0;
            $("#AAOpServices").val(0);
        } 

        t_mr = parseInt(mr) + parseInt(add_mr);
        $("#AATMRate").val(t_mr);
        $("#AATotalRate").val(t_mr);
        $("#AATotalRate2").val(t_mr);
            

       
        
    });

  
});