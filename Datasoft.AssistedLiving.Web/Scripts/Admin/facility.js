﻿var facilityState = {
    orig: '',
    current: ''
};
function setHeight() {

    $('.dataTables_scrollBody').height($(window).height() - 450);
    if ($(window).width() < 1024) {
        $('#headerRoom').height($("#headerRoom .row").height() + 100);
    } else {
        $('#headerRoom').height($(window).height() - 140);
    }

    if ($(window).height() < 400) {
        $('.dataTables_scrollBody').height(200);
        $('#headerRoom').height($('#facilityGrid').height() + 380);
    }

    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('#headerRoom').height($("#headerRoom .row").height() + 100);
        } else {
            $('#headerRoom').height($(window).height() - 140);
            $('.dataTables_scrollBody').height($(window).height() - 450);
        }

        if ($(window).height() < 400) {
            $('.dataTables_scrollBody').height(200);
            $('#headerRoom').height($('#facilityGrid').height() + 380);
        }

        $('#facigrid').DataTable().columns.adjust();
    })
    $('#facigrid').DataTable().columns.adjust();

    var scroll = new PerfectScrollbar('.dataTables_scrollBody');
    $('#facigrid').DataTable().columns.adjust();
}
var load_facilities = (function () {
    //var dataArray = [
    //    { Id: '1', Name: 'Suite 101', Description: 'Meeting facility', Level: 'First', Rate: '999.20' },
    //    { Id: '2', Name: 'Fancy 201', Description: 'Lounge', Level: 'Second', Rate: '534.20' },
    //    { Id: '3', Name: 'Lancy 900', Description: '', Level: 'First', Rate: '923.20' },
    //    { Id: '4', Name: 'Hancy 123', Description: '', Level: 'Third', Rate: '912.20' }

    //];

    //this is in site.layout.js.
    //var GURow = new Grid_Util.Row();


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=facility&param=",
        dataType: "json",
        async: false,
        success: function (d) {


            doc_html = " ";
            var faci = [];
            var desc = [];
            $.each(d, function (i, doc) {

                if (doc.Name != "") {
                    faci.push('<option>' + doc.Name + '</option>');
                }
                if (doc.Description != "") {
                    desc.push('<option>' + doc.Description + '</option>');
                }


                doc_html += "<tr><td><a href='#' class='fname' id='" + doc.Id + "' data-target='#facility_modal' data-toggle='modal' desc='" + doc.Description + "' level='" + doc.Level + "' rate='" + doc.Rate + "' name='" + doc.Name + "'>" + doc.Name + "</a></td>";
                doc_html += "<td>" + doc.Description + "</td>";
                doc_html += "<td>" + doc.Level + "</td>";
                doc_html += "<td>$" + doc.Rate + "</td>";
                doc_html += "<td class='toolbar'><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n\n<a href='#' class='dropdown-item fname' id='" + doc.Id + "' data-target='#facility_modal' data-toggle='modal'  name='" + doc.Name + "' desc='" + doc.Description + "' level='" + doc.Level + "' rate='" + doc.Rate + "'><i class='la la-edit'></i> Edit</a> <a class='dropdown-item del faci_delete' href='#' id='" + doc.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                doc_html += "</tr>";
                //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";

            })

            $("#faci_btable").html(doc_html);
            $("#facigrid").DataTable().destroy();

            table_facilities = $("#facigrid").DataTable({
                responsive: !0,
                pagingType: "full_numbers",
                select: true,
                "scrollX": true,
                "scrollY": true,
                "scrollCollapse": true,
                dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="la la-download"></i> Export to Excel',
                        title: 'ALC_Services',
                        className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }

                ],
            });

            //  var faci = [];

            ////Facility FILTER
            //$.each(d, function (o) { return o.Name }), function () {
            //    console.log(faci);
            //        faci.push('<option>' + this + '</option>');
            //};

            //faci.push('<option value="' + d.Name + '">' + d.Name + '</option>');
            var uniqfaci = faci.filter(function (item, pos) {
                return faci.indexOf(item) == pos;
            });
            $('#ftrFacility').html(faci.join(''));
            $("#ftrFacility").html($('#ftrFacility option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $("#ftrFacility").prepend('<option>All</option>');
            $("#ftrFacility").get(0).selectedIndex = 0;
            //Facility FILTER : ENDS HERE

            //DESCRIPTION FILTER
            var uniqdesc = desc.filter(function (item, pos) {
                return desc.indexOf(item) == pos;
            });
            $('#ftrDescription').html(desc.join(''));
            $("#ftrDescription").html($('#ftrDescription option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $("#ftrDescription").prepend('<option>All</option>');
            $("#ftrDescription").get(0).selectedIndex = 0;
            ////DESCRIPTION FILTER : ENDS HERE
            setTimeout(function () { $(".loader").hide(); }, 500)

        }


    });
    var dropdown_ftr = function (i, data) {

        table_facilities.column(i)
             .search(data ? '^' + data + '$' : data, true, false)
             .draw();
    };

    $('#ftrFacility').change(function () {
        var act = $('#ftrFacility :selected').text();
        var index_a = $('#ftrFacility').prop("selectedIndex");

        if (act != "All") {
            col_num = 0;
            column_value = $('#ftrFacility').val();
            $("#ftrDescription").get(0).selectedIndex = 0;
            load_facilities();
            dropdown_ftr(col_num, column_value);
            $('#ftrFacility').get(0).selectedIndex = index_a;
        } else {
            load_facilities();
        }
        setTimeout(function () { setHeight(); }, 200)
    });
    $('#ftrDescription').change(function () {
        var act = $('#ftrDescription :selected').text();
        var index_a = $('#ftrDescription').prop("selectedIndex");

        if (act != "All") {
            col_num = 1;
            column_value = $('#ftrDescription').val();
            $("#ftrFacility").get(0).selectedIndex = 0;
            load_facilities();
            dropdown_ftr(col_num, column_value);
            $('#ftrDescription').get(0).selectedIndex = index_a;
            setTimeout(function () { setHeight(); }, 200)
        } else {
            load_facilities();
        }
        setTimeout(function () { setHeight(); }, 200)
    });

    $('#facigrid').on('click', ".fname", function () {
        var dis = $(this);
        var rid = dis.attr('id');
        var name = dis.attr('name');
        var desc = dis.attr('desc');
        var level = dis.attr('level');
        var rate = dis.attr('rate');

        $('#Id').val(rid);
        $('#txtName').val(name);
        $('#txtDescription').val(desc);
        $('#txtLevel').val(level);
        $('#txtRate').val(rate);

        facilityState.orig = JSON.stringify($('#facility_modal').extractJson());

    });
    // modified by Cheche on 12/20/2021 Reason: TO DELETE FACILITY
    $('#facigrid').on('click', ".faci_delete", function () {
        var did = $(this);
        var del_id = did.attr('id');

        Swal({
            title: 'Are you sure you want to delete this Amenity?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function (e) 
        {
            e.value &&
                $.ajax ({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify({
                        func: 'facility',
                        mode: 3,
                        data: {Id: del_id}
                    }),
                    dataType: "json",
                    success: function (m) {
                   
                          if (m.result == 0) {
                              swal("Amenity deleted successfully!", "", "success");
                              
                              setTimeout(function () {
                                  $("#amenities_1 a").trigger("click");
                              }, 200); 
                          } else {
                              $.growlError({
                                      message: m.message,
                                      delay: 6000
                              });
                          }
                       
                    }
                
                });
        });
    });
   
    // code modification ends here

    //$('#grid').jqGrid({
    //    url: "Admin/GetGridData?func=facility&param=",
    //    datatype: 'json',
    //    postData: "",
    //    ajaxGridOptions: { contentType: "application/json", cache: false },
    //    datatype: 'local',
    //    data: dataArray,
    //    mtype: 'Get',
    //    colNames: ['Id', 'Facility', 'Description', 'Level', 'Rate'],
    //    colModel: [
    //      { key: true, hidden: true, name: 'Id', index: 'Id' },
    //      {
    //          key: false, name: 'Name', index: 'Name', formatter: "dynamicLink", formatoptions: {
    //              onClick: function (rowid, iRow, iCol, cellText, e) {
    //                  var G = $('#grid');
    //                  G.setSelection(rowid, false);
    //                  $('.form').clearFields();
    //                  var row = G.jqGrid('getRowData', rowid);
    //                  if (row) {
    //                      $('.form').mapJson(row);
    //                      showdiag(2);
    //                  }
    //              }
    //          }
    //      },
    //      { key: false, name: 'Description', index: 'Description' },
    //      { key: false, name: 'Level', index: 'Level' },
    //      { key: false, name: 'Rate', index: 'Rate', formatter: 'currency', formatoptions: { decimalSeparator: ".", prefix: "$", defaultValue: '0.00' } }],
    //    pager: jQuery('#pager'),
    //    rowNum: 1000000,
    //    rowList: [10, 20, 30, 40],
    //    height: '100%',
    //    viewrecords: true,
    //    caption: 'Care Staff',
    //    emptyrecords: 'No records to display',
    //    jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
    //    autowidth: true,
    //    multiselect: false,
    //    sortname: 'Id',
    //    sortorder: 'asc',
    //    loadonce: true,
    //    onSortCol: function () {
    //        fromSort = true;
    //        GURow.saveSelection.call(this);
    //    },
    //    loadComplete: function (data) {
    //        if (typeof fromSort != 'undefined') {
    //            GURow.restoreSelection.call(this);
    //            delete fromSort;
    //            return;
    //        }

    //        var maxId = data && data[0] ? data[0].Id : 0;
    //        if (typeof isReloaded != 'undefined') {
    //            $.each(data, function (k, d) {
    //                if (d.Id > maxId) maxId = d.Id;
    //            });
    //        }
    //        if (maxId > 0)
    //            $("#grid").setSelection(maxId);
    //        delete isReloaded;
    //    }
    //});
    //$("#grid").jqGrid('bindKeys');
});

$(document).ready(function () {

    $(".loader").show();

    var col_num = 0;
    var column_value = "";
    var table_facilities = "";
    $("#txtRate").forceNumericOnly();
    //if (typeof (PHL) !== 'undefined') {
    //    if (PHL.destroy) PHL.destroy();
    //}


    //$("#facilityContent").layout({
    //    center: {
    //        paneSelector: "#facilityGrid",
    //        closable: false,
    //        slidable: false,
    //        resizable: true,
    //        spacing_open: 0
    //    },
    //    north: {
    //        paneSelector: "#wrapToolbar",
    //        closable: false,
    //        slidable: false,
    //        resizable: false,
    //        spacing_open: 0
    //        , north__childOptions: {
    //            paneSelector: "#gridToolbar",
    //            closable: false,
    //            slidable: false,
    //            resizable: false,
    //            spacing_open: 0
    //        },
    //        center__childOptions: {
    //            paneSelector: "#raToolbar",
    //            closable: false,
    //            slidable: false,
    //            resizable: false,
    //            spacing_open: 0
    //        }
    //    },
    //    onresize: function () {
    //        resizeGrids();
    //    }
    //});

    //var resizeGrids = (function () {
    //    var f = function () {
    //        $('.ui-jqgrid-bdiv').eq(0).height($("#facilityGrid").height() - 25).width($("#facilityGrid").width());
    //        $('#grid').setGridWidth($("#facilityGrid").width(), true);
    //    }
    //    setTimeout(f, 100);
    //    return f;
    //})();

    //$('#ddlFilter').change(function () {
    //    $('#ftrText').val('');
    //});
    //$('#ftrText').change(function () {
    //    var res = $('#ftrText').val();
    //    var rm = $('#ddlFilter').val();
    //    var rd = $('#ddlFilter :selected').text();
    //    var ftrs = {
    //        groupOp: "AND",
    //        rules: []
    //    };
    //    var col = 'Name';
    //    if (rm == 2)
    //        col = "Description";

    //    if (res != '')
    //        ftrs.rules.push({ field: col, op: 'cn', data: res });


    //    $("#grid")[0].p.search = ftrs.rules.length > 0;
    //    $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
    //    $("#grid").trigger("reloadGrid");
    //});


    //$("#txtRate").forceNumericOnly();
    //$("#diag").dialog({
    //    zIndex: 0,
    //    autoOpen: false,
    //    modal: true,
    //    title: "Facility",
    //    dialogClass: "companiesDialog",
    //    open: function () {
    //    }
    //});

    var doAjax = function (mode, row, cb) {
        //var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            //if (!isValid) return;
            //row = $('.form').extractJson();
            var form = document.getElementById('facility_modal_form');
            for (var i = 0; i < form.elements.length; i++) {
                if (form.elements[i].value === '' && form.elements[i].hasAttribute("required")) {
                    swal("<span>Fill in all required (<span style=\"color: red;\">*</span>) fields. </span>")
                    return false;
                }
            }
        }
        if (mode == 2) {
            facilityState.current = JSON.stringify(row);

            if (facilityState.current == facilityState.orig) {
          
                swal("There are no changes to save.", "", "info");
                $(".loader").hide();
                return;
            }
        }

        var data = { func: "facility", mode: mode, data: row };
        $.ajax({
          
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
            
                if (cb) cb(m);
            }
        });
    }

    //var showdiag = function (mode) {
    //    $("#diag").dialog("option", {
    //        width: $(window).width() - 750,
    //        height: 340,
    //        position: "center",
    //        buttons: {
    //            "Save": {
    //                click: function () {
    //                    var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //                    row = $('#grid').jqGrid('getRowData', id);
    //                    doAjax(mode, row, function (m) {
    //                        if (m.result == 0)
    //                            $.growlSuccess({ message: "Facility " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
    //                        $("#diag").dialog("close");
    //                        if (mode == 1) isReloaded = true;
    //                        else {
    //                            fromSort = true;
    //                            new Grid_Util.Row().saveSelection.call($('#grid')[0]);
    //                        }
    //                        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    //                    });
    //                },
    //                class: "primaryBtn",
    //                text: "Save"
    //            },
    //            "Cancel": function () {
    //                $("#diag").dialog("close");
    //            }
    //        }
    //    }).dialog("open");
    //}

    //$('.toolbar').on('click', 'a', function () {
    //    var q = $(this);
    //    if (q.is('.add')) {
    //        $('.form').clearFields();
    //        showdiag(1);
    //    } else if (q.is('.del')) {
    //        var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //        if (id) {
    //            if (confirm('Are you sure you want to delete the selected row?')) {
    //                setTimeout(function () {
    //                    var row = $('#grid').jqGrid('getRowData', id);
    //                    if (row) {
    //                        doAjax(3, { Id: row.Id }, function (m) {
    //                            var msg = row.Name + " deleted successfully!";
    //                            if (m.result == -3) //inuse
    //                                msg = row.Name + " cannot be deleted because it is currently in use.";
    //                            $.growlSuccess({ message: msg, delay: 6000 });
    //                        });
    //                    }
    //                }, 100);
    //            }
    //        } else {
    //            alert("Please select row to delete.")
    //        }
    //    } else if (q.is('.unsign')) {
    //        var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //        if (id) {
    //            var row = $('#grid').jqGrid('getRowData', id);
    //            if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
    //                setTimeout(function () {
    //                    if (row) {
    //                        doAjax(4, { Id: row.Id }, function (m) {
    //                            if (m.result == 0)
    //                                $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
    //                        });
    //                    }
    //                }, 100);
    //            }
    //        } else {
    //            alert("Please select row to deactivate.")
    //        }
    //    }
    //});




    $('#addFacility').on('click', function () {
        $('#facility_modal').clearFields();
    });


    $('#saveFacility').on('click', function () {
    
        var rate = $("#txtRate").val();
        var isValid = /^[0-9,.]*$/.test(rate);

        if (isValid == false) {
          
            Swal({
                type: 'error',
                title: 'Oops...',
                text: 'Invalid "Rate" value. Please input digits only.'
            })
            return false;
        }
       
        mode = "";
        row = $('#facility_modal').extractJson();
        if (row.Id == "") {
           mode = 1;
        } else {
            mode = 2;
        }

        if (mode == 2) {

        }

        doAjax(mode, row, function (m) {
            if (m.result == 0) {
                swal("Amenity " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                facilityState.orig = JSON.stringify($('#facility_modal').extractJson());
            }// Cheche was here to add restrictions for existing facility 01/05/2022
            else if (m.result == -1) {
                swal("Amenity name already exists!", "", "info");
                return false;
            }

            $("#facility_modal").modal("hide");
            setTimeout(function () {
 
                $("#amenities_1 a").trigger("click");
            }, 200);

        });
    });

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        //$("td:hidden,th:hidden", "#grid").remove();
        //var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Facility</td><td bgcolor='#87AFC6'>Description</td><td bgcolor='#87AFC6'>Level</td><td bgcolor='#87AFC6'>Rate</td> </tr><tr>";
        var textRange; var j = 0;
        var tab_text = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML;
            //tab_text=tab_text+"</tr>";
        }

        //tab_text = tab_text + "</table>";
        //tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        //tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        //tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Facilities.xls");
        }
        else                 //other browser not tested on IE 11
            a.id = 'ExcelDL';
        a.href = 'data:application/pdf,' + encodeURIComponent(tab_text);
        a.download = 'ALC_Facilities' + '.pdf';
        document.body.appendChild(a);
        a.click(); // Downloads the excel document
        document.getElementById('ExcelDL').remove();
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    });
    //========================



    load_facilities();
    setHeight();
});