﻿var fLastRow;
var rowsToColor = [];
$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }
    $("#roomContent").layout({
        center: {
            paneSelector: "#residentGrid",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#wrapToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
            , north__childOptions: {
                paneSelector: "#residentGridToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            center__childOptions: {
                paneSelector: "#raToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            }
        },
        onresize: function () {
            resizeGrids();
            $('#grid').setGridWidth($("#residentGrid").width(), true);
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#residentGrid").height() - 25).width($("#residentGrid").width());
        }
        setTimeout(f, 100);
        return f;
    })();


    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Residents",
        dialogClass: "companiesDialog",
        open: function () {
            $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                                              .addClass("ui-state-disabled");
            if (!$("#residentForm").data("layoutContainer")) {
                $("#residentForm").layout({
                    center: {
                        paneSelector: ".tabPanels",
                        closable: false,
                        slidable: false,
                        resizable: true,
                        spacing_open: 0
                    },
                    north: {
                        paneSelector: ".tabs",
                        closable: false,
                        slidable: false,
                        resizable: false,
                        spacing_open: 0,
                        size: 27
                    }
                });
            }
            $("#residentForm").layout().resizeAll();
            if (!$("#residentForm").data("tabs")) {
                $("#residentForm").tabs({
                    select: function (event, ui) {
                        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                                              .addClass("ui-state-disabled");
                        $('#assPager').hide();
                        if (ui.index == 6) {
                            setTimeout(initHistory, 1);
                        } else if (ui.index == 5) {
                            setTimeout(initService, 1);
                        } else if (ui.index == 4) {
                            setTimeout(initDocuments, 1);
                        } else if (ui.index == 3) {
                            if ($("#admForm").tabs('option', 'selected') != 0)
                                $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
                                              .removeClass("ui-state-disabled");
                        }
                    }
                });
            }
            $("#residentForm").tabs("option", "selected", 0);
        }
    });

    //load admission tab data
    $.ajax({
        url: "Admin/GetNodeData",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        type: "POST",
        data: '{id:"Admission_0"}',
        success: function (d) {
            $("#tabs-adm").html(d);
            $('#admForm .tabs').css('background', 'none');
            $('#AdmissionContainer').css({ 'padding': '10px 10px' });
            $('#admForm .tabPanels').css({ 'border': 'solid 1px #ccc', 'padding': '5px 5px', 'overflow': 'auto !important' });
            $("#admForm #liRoom,#admForm #liResident").hide();

            $("input:checkbox").on('click', function () {
                // in the handler, 'this' refers to the box clicked on
                var $box = $(this);
                if ($box.is(":checked")) {
                    // the name of the box is retrieved using the .attr() method
                    // as it is assumed and expected to be immutable
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    // the checked state of the group/box on the other hand will change
                    // and the current value is retrieved using .prop() method
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                } else {
                    $box.prop("checked", false);
                }
            });
            $('#txtHospitalPhone,#txtPhysicianPhone,#txtPhysicianFax,#txtPharmacyPhone').mask("(999)999-9999");
            $(".content-datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });
            $("#txtDischargeDate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });//("option", "dateFormat", "mm/dd/yy");

            $("#admForm").tabs({
                select: function (event, ui) {
                    if (ui.index == 0) {
                        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                                        .addClass("ui-state-disabled");
                    } else {
                        $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
                                    .removeClass("ui-state-disabled");
                    }
                    if (ui.index == 5) {
                        $('#assPager').show();
                    } else {
                        $('#assPager').hide();
                    }
                }

            });
            $("#admForm").tabs("option", "selected", 0);
            $("#admForm").tabs('option', 'disabled', [1, 2, 3, 4]);
            //var diag = $('.ui-dialog-content');
            //diag = diag.find('#admForm').parents('.ui-dialog-content');
            //var height = 0;
            //height = diag.eq(0).height() || 0;
            //if (height <= 0)
            //    height = diag.eq(1).height() || 0;
            //$("#AdmissionContainer,#admForm").height(height);
            //$('#AdmissionContent').height(height - 20);
            //$('#admForm').find('#tabs-1,#tabs-2,#tabs-3,#tabs-4,#tabs-5').height(height - 30);

        }
    });

    //load assessment tab
    $.ajax({
        url: "Admin/GetNodeData",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        type: "POST",
        data: '{id:"assessmentwizard_0"}',
        success: function (d) {
            $("#tabs-6a").empty();
            $("#tabs-6a").html(d);
            $('#update-status').hide();
            $.ajax({
                url: ALC_URI.Admin + "/GetNodeData",
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                type: "POST",
                data: '{id:"morsefallscale_0"}',
                success: function (d) {
                    $("#mfs-container").empty();
                    $("#mfs-container").html(d);
                },
                complete: function () { },
                error: function () { }
            });
        },
        complete: function () { },
        error: function () { }
    });

    var tab = 1;
    var pagecount = 1;

    var loadPager = function () {
        $("#pager-no").val(pagecount);
    }

    var loadAssessment = function (row) {
        //load assessment
        // debugger;
        var params = { func: "assessment_wizard", id: row.Id };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Marketer + "/GetFormData",
            data: JSON.stringify(params),
            dataType: "json",
            success: function (m) {
                if (m.Result == 1) {
                    $("#AssessmentForm #FirstName").val(m.Data.FirstName);
                    $("#AssessmentForm #LastName").val(m.Data.LastName);
                    $("#AssessmentForm #MiddleInitial").val(m.Data.MiddleInitial);
                    var info = $.cookie('ALCInfo');
                    if (info != '') {
                        var ins = info.split('|');
                        var name = '';
                        if (ins.length > 1) {
                            name = ins[1];
                        }
                        $("#CompletedBy").val(capsFirstLetter(name));
                    }
                } else {
                    //$(".form").serializeArray().map(function (x) { m[x.name] = x.value; },'id');
                    $("#AssessmentForm").mapJson(m.Data, 'id');
                }

                $("#AssResidentId").val(row.Id);
                //$("#AssReferralId").val(referralId);
                for (var ps in m.Points) {
                    $("#" + ps).val(m.Points[ps]);
                    $($("#" + ps).parents().children()[1]).html(m.Points[ps])
                }
                //tab = 1;
                //pagecount = 1;

                $('#assessmentTreeView a:eq(0)').trigger('click');
                if ($('#assPager').length > 0) {
                    $('#assPager').remove();
                }
                var $foot = $('<div id="assPager"><div style="padding-top:5px;padding-left:5px;float:left;"> <label><strong>Total Score: <span id="label-TotalScore">0</span></strong> </label></div> ' +
                        '<div style="padding:0;"> <center> <input class="navi-button" id="ass-wiz-prev" type="button" value="<<" /> ' +
                        ' <span><label id="lpager" style="float:none;">Page  <input type="number" id="pager-no" min ="1" max="34"  /> of 34</label></span> <input class="navi-button" id="ass-wiz-next" type="button" value=">>" /> </center> </div></div>');

                $('.ui-dialog-buttonpane').append($foot);
                $('#ass-wiz-next').off().on('click', function (e) {
                    var $current = $("#tabs-" + tab.toString() + 'b .container-display');
                    var $next = $("#tabs-" + tab.toString() + 'b .container-display').next('.container');
                    if ($next.length == 0) {
                        if (tab == 7)
                            return;
                        tab++;
                        $next = $("#tabs-" + tab.toString() + 'b .container').first('.container');
                    }
                    $("#AssessmentForm").tabs("option", "selected", tab - 1);
                    $next.addClass('container-display');
                    $current.removeClass('container-display');
                    pagecount++;
                    loadPager();
                });
                $('#ass-wiz-prev').off().on('click', function (e) {
                    var $current = $("#tabs-" + tab.toString() + 'b .container-display');
                    var $prev = $("#tabs-" + tab.toString() + 'b .container-display').prev('.container');
                    if ($prev.length == 0) {
                        if (tab == 1)
                            return;
                        tab--;
                        $prev = $("#tabs-" + tab.toString() + 'b .container').last('.container');
                    }
                    $("#AssessmentForm").tabs("option", "selected", tab - 1);
                    $prev.addClass('container-display');
                    $current.removeClass('container-display');
                    pagecount--;
                    loadPager();
                });
                $('#assessmentTreeView').off().on('click', 'a', function (e) {
                    var i = $(this).attr('page');
                    assessmentWizardGoToPage(i);

                });
                $('#pager-no').off().on('keypress', function (e) {
                    if (e.which == 13) {
                        assessmentWizardGoToPage($("#pager-no").val());
                    }
                });

                $('#assPager').hide();
                $("#label-TotalScore").text(m.Data.TotalScore);
                //debugger;
                loadPager();

            }
        });
    }
    function assessmentWizardGoToPage(page) {
        var $current = $("#tabs-" + tab.toString() + 'b .container-display');
        var gotopage = page;
        if (gotopage == pagecount)
            return;
        if (gotopage > 34 || gotopage < 1) {
            $("#pager-no").val(pagecount);
            return;
        } else
            $("#pager-no").val(gotopage);
        var x = 0;
        var tab_col = {
            1: x += $('#tabs-1b .container').length,
            2: x += $('#tabs-2b .container').length,
            3: x += $('#tabs-3b .container').length,
            4: x += $('#tabs-4b .container').length,
            5: x += $('#tabs-5b .container').length,
            6: x += $('#tabs-6b .container').length,
            7: x += $('#tabs-7b .container').length,
        };
        for (var i = 1; i <= 7; i++) {
            if (gotopage <= tab_col[i]) {
                tab = i;
                pagecount = gotopage;
                var $go = $("#tabs-" + tab.toString() + 'b .container').slice(((tab_col[i] - ((i == 1) ? 0 : tab_col[i - 1])) - (tab_col[i] - pagecount)) - 1).slice(0, 1);

                $("#AssessmentForm").tabs("option", "selected", tab - 1);
                $go.addClass('container-display');
                $current.removeClass('container-display');
                return;
            }
        }
    }

    $(function () {
        $("#txtReligiousAdvisorTelephone,#txtResponsiblePersonTelephone,#txtNearestRelativeTelephone").mask("(999)999-9999");
        //$("#residentForm").tabs();
        $("#txtBirthdate, #txtDateLeft,#txtDateAdmitted").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
            onClose: function () {
                if (this.id == 'txtDateAdmitted') {
                    $('#AAAdmissionDate,#txtAdmissionDate').val(this.value);
                } else if (this.id == 'txtDateLeft') {
                    $('#txtDischargeDate').val(this.value);
                }
            }
        });//("option", "dateFormat", "mm/dd/yy");
    });

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "resident", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        var isValid = $('#residentForm .resForm').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('#residentForm .resForm').extractJson();
        }

        if (mode == 1 || mode == 2) {
            //validate Tab Grids
            var fGridData = $.grep($('#fGrid').jqGrid('getGridParam', 'data'), function (n, i) {
                return (n.E == true) || (n.D == 'true');//post only marked with E=edited and D=deleted rows
            });
            var oGridData = $.grep($('#oGrid').jqGrid('getGridParam', 'data'), function (n, i) {
                return (n.E == true) || (n.D == 'true');
            });

            var reqObj = null;
            $.each(fGridData, function (i) {
                if (this.Name == '' && this.D != 'true') {
                    isValid = false;
                    reqObj = this;
                    return;
                }
            });
            if (!isValid) {
                $("#residentForm").tabs("option", "selected", 1);
                alert('Name field is required.')
                $('#fGrid').jqGrid('editRow', reqObj.RId);

                return;
            }

            $.each(oGridData, function (i) {
                if (this.Name == '' && this.D != 'true') {
                    isValid = false;
                    reqObj = this;
                    return;
                }
            });
            if (!isValid) {
                $("#residentForm").tabs("option", "selected", 2);
                alert('Name field is required.')
                $('#oGrid').jqGrid('editRow', reqObj.RId);
                return;
            }
        }

        var data = { func: "resident_movein", mode: mode, data: row };

        if (mode == 1 || mode == 2) {
            data.data.fGrid = fGridData;
            data.data.oGrid = oGridData;
        }
        console.log(data);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var selectedTabIndex = 0;
    var doAjaxAdm = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {

            var data = { func: "resident_admission", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }


        var isValid = $('#admForm').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }
            row = $('#admForm .admTbs').extractJson('id');
            row.residentId = $('#residentForm #txtId').val();
            row.dcd = 1; //dont check duplicates.
        }
        var tabindex = $("#admForm").tabs('option', 'selected');
        var data = { func: "resident_admission", mode: mode, data: row, TabIndex: tabindex };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });

    }

    var createTabGrids = function (mode) {
        var lastRow = null;
        if ($('#fGrid')[0] && $('#fGrid')[0].grid)
            $.jgrid.gridUnload('fGrid');
        var FGrid = $('#fGrid');

        FGrid.jqGrid({
            datatype: 'local',
            //data: [{ RId: '1', Name: 'Harold Brunner', Address: '123 AB St. 2nd Ave Los Amigos Mexico', Telephone: '(234)123233', E: false }],
            colNames: ['RId', 'Name', 'Address', 'Telephone', 'E', 'D'],
            colModel: [
              { key: true, hidden: true, name: 'RId', index: 'RId' },
              { key: false, name: 'Name', index: 'Name', width: 200, editable: true, edittype: 'text' },
              { key: false, name: 'Address', index: 'Address', width: 300, editable: true, edittype: 'text' },
              {
                  key: false, name: 'Telephone', index: 'Telephone', width: 100, editable: true, edittype: 'custom',
                  editoptions: {
                      custom_element: function (value, options) {
                          var el = document.createElement("input");
                          el.type = "text";
                          el.value = value;
                          $(el).mask("(999)999-9999");
                          return el;
                      },
                      custom_value: function (elem, operation, value) {
                          if (operation === 'get') {
                              return $(elem).val();
                          } else if (operation === 'set') {
                              $('input', elem).val(value);
                          }
                      }
                  }
              },
              { key: false, hidden: true, name: 'E', index: 'E', edittype: 'text' },
              { key: false, hidden: true, name: 'D', index: 'D', edittype: 'text' }],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [],
            pgbuttons: false,
            pgtext: null,
            height: '100%',
            //cellEdit: true,
            viewrecords: false,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            editurl: 'clientArray',
            cellsubmit: 'clientArray',
            beforeSelectRow: function (rowid, e) {
                if (FGrid.attr('lastRow') != 'undefined') {
                    lastRow = FGrid.attr('lastRow');
                }

                if (lastRow != rowid) {
                    FGrid.jqGrid('saveRow', lastRow);
                    var row = FGrid.jqGrid('getRowData', lastRow);
                    row.E = true;
                    FGrid.jqGrid('setRowData', lastRow, row);
                }

                var attr = $('#fGrid tr[id=' + rowid + ']').attr('editable');
                if (typeof attr === typeof undefined || attr == 0)
                    FGrid.jqGrid('editRow', rowid);

                FGrid.attr('lastRow', rowid);
            }
        });

        if ($('#oGrid')[0] && $('#oGrid')[0].grid)
            $.jgrid.gridUnload('oGrid');
        var oGrid = $('#oGrid');

        oGrid.jqGrid({
            datatype: 'local',
            //data: [{ RId: '1', Name: 'Shanon Stoney', Address: '422 John St. 2nd Ave. West Harbor', Telephone: '(234)856867', E:false }],
            colNames: ['RId', 'Name', 'Address', 'Telephone', 'E', 'D'],
            colModel: [
              { key: true, hidden: true, name: 'RId', index: 'RId' },
              { key: false, name: 'Name', index: 'Name', width: 200, editable: true, edittype: 'text' },
              { key: false, name: 'Address', index: 'Address', width: 300, editable: true, edittype: 'text' },
              {
                  key: false, name: 'Telephone', index: 'Telephone', width: 100, editable: true, edittype: 'custom',
                  editoptions: {
                      custom_element: function (value, options) {
                          var el = document.createElement("input");
                          el.type = "text";
                          el.value = value;
                          $(el).mask("(999)999-9999");
                          return el;
                      },
                      custom_value: function (elem, operation, value) {
                          if (operation === 'get') {
                              return $(elem).val();
                          } else if (operation === 'set') {
                              $('input', elem).val(value);
                          }
                      }
                  }
              },
              { key: false, hidden: true, name: 'E', index: 'E' },
              { key: false, hidden: true, name: 'D', index: 'D' }],
            pager: jQuery('#pager'),
            rowList: [],
            pgbuttons: false,
            pgtext: null,
            rowNum: 1000000,
            //rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: false,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            editurl: 'clientArray',
            cellsubmit: 'clientArray',
            beforeSelectRow: function (rowid, e) {
                if (oGrid.attr('lastRow') != 'undefined') {
                    lastRow = oGrid.attr('lastRow');
                }
                if (lastRow != rowid) {
                    oGrid.jqGrid('saveRow', lastRow);
                    var row = $('#oGrid').jqGrid('getRowData', lastRow);
                    row.E = true;
                    oGrid.jqGrid('setRowData', lastRow, row);
                }
                var attr = $('#oGrid tr[id=' + rowid + ']').attr('editable');
                if (typeof attr === typeof undefined || attr == 0)
                    oGrid.jqGrid('editRow', rowid);

                oGrid.attr('lastRow', rowid);
            }
        });

        $("#fGrid").jqGrid('bindKeys');
        $("#oGrid").jqGrid('bindKeys');
    }
    var showdiag = function (mode) {
        createTabGrids();
        var id = $('#admForm #txtAdmissionId').val();
        var amode = $.isNumeric(id) ? 2 : 1;
        $("#diag").dialog("option", {
            width: $(window).width() - 400,
            height: $(window).height() - 200,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var tabIdx = $("#residentForm").tabs('option', 'selected');
                        if (tabIdx != 3) {
                            //close grid editors before save
                            var editRow = $('#fGrid tr[editable=1]');
                            if (editRow.length > 0) {
                                editRow.each(function (i) {
                                    $('#fGrid').jqGrid('saveRow', this.id);
                                    var row = $('#fGrid').jqGrid('getRowData', this.id);
                                    row.E = true;
                                    $('#fGrid').jqGrid('setRowData', this.id, row);
                                });
                            }
                            //close grid editors before save
                            editRow = $('#oGrid tr[editable=1]');
                            if (editRow.length > 0) {
                                editRow.each(function (i) {
                                    $('#oGrid').jqGrid('saveRow', this.id);
                                    var row = $('#oGrid').jqGrid('getRowData', this.id);
                                    row.E = true;
                                    $('#oGrid').jqGrid('setRowData', this.id, row);
                                });
                            }

                            var id = $('#grid').jqGrid('getGridParam', 'selrow');
                            row = $('#grid').jqGrid('getRowData', id);
                         //   debugger;
                            doAjax(mode, row, function (m) {
                                if (m.result == 0)
                                    $.growlSuccess({ message: "Resident " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                //enable
                                if (mode == 1) {
                                    $("#residentForm").tabs('option', 'disabled', []);
                                    $("#residentForm").tabs("option", "selected", 3);
                                    $("#admForm").tabs("option", "selected", 0);
                                    var retVal = eval('(' + m.return + ')');
                                    $('#residentForm #txtId,#admForm #txtResidentId').val(retVal.rid);
                                    $('#admForm #txtAdmissionId').val(retVal.aid);
                                    var fname = $('#txtFirstname').val();
                                    var middle = $('#txtMiddleInitial').val();
                                    var lname = $('#txtLastname').val();
                                    if (middle != "")
                                        fname += middle + ". " + lname;
                                    $('#txtResidentName').val(fname);
                                }
                                //repopulateResident();
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                }
                                $("#diag").dialog("close");
                                $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                            });
                        } else {
                            var row = {};
                            var tabIdx2 = $("#admForm").tabs('option', 'selected');
                            if (tabIdx2 != 5) {
                                doAjaxAdm(2, row, function (m) {
                                    if (m.message == "Duplicate") {
                                        alert('Admission for resident already exists!');
                                        return;
                                    }
                                    if (amode == 1) {
                                        $('#admForm #txtAdmissionId').val(m.result);
                                        //    $.growlSuccess({ message: "Admission " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                        //    $("#diag").dialog("close");
                                    }
                                    $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
                                                  .removeClass("ui-state-disabled");

                                    var tabindex = $("#admForm").tabs('option', 'selected');
                                    if (tabindex == 0) {
                                        $('#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val($('#txtResidentName').val());
                                        $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                                        $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#DRoomId option:selected').text());
                                        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                                                  .addClass("ui-state-disabled");
                                    }
                                    else if (tabindex == 2) {
                                        $('#AADOB').val($('#ANSDOB').val());
                                    }
                                    if (selectedTabIndex > tabindex) {
                                        $("#admForm").tabs("option", "selected", selectedTabIndex);
                                    }
                                    else {
                                        selectedTabIndex = tabindex + 1;
                                        if (tabindex == 0) {
                                            $("#admForm").tabs('option', 'disabled', [2, 3, 4]);
                                            $("#admForm").tabs("option", "selected", 1);
                                        }
                                        else if (tabindex == 1) {

                                            $("#admForm").tabs('option', 'disabled', [3, 4]);
                                            $("#admForm").tabs("option", "selected", 2);
                                        }
                                        else if (tabindex == 2) {

                                            $("#admForm").tabs('option', 'disabled', [4]);
                                            $("#admForm").tabs("option", "selected", 3);
                                        }
                                        else if (tabindex == 3) {

                                            $("#admForm").tabs('option', 'disabled', []);
                                            $("#admForm").tabs("option", "selected", 4);
                                        }
                                        else if (tabindex == 4) {
                                            $.growlSuccess({ message: "Admission " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                            $("#diag").dialog("close");
                                        }
                                    }
                                });
                            } else {
                                //save assessment
                                row = { Func: 'assessment_wizard' };
                                row.Mode = ($("#AssessmentId").val() == "") ? 1 : 2;

                                var isValid = true;
                                isValid = $('#AssessmentForm').validate();
                                if (!isValid) {
                                    return;
                                }
                                row.Data = $('#AssessmentForm').extractJson('id');
                                $.ajax({
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    url: ALC_URI.Marketer + "/PostGridData",
                                    data: JSON.stringify(row),
                                    dataType: "json",
                                    success: function (m) {
                                        $.growlSuccess({ message: "Assessment has been saved.", delay: 6000 });
                                        $("#diag").dialog("close");
                                    }
                                });
                            }
                        }
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Print": {
                    click: function () {
                        var ts = $("#admForm").tabs('option', 'selected');
                        if (ts == 1)
                            window.open(ALC_URI.Admin + "/PrintDocumentChecklist?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                        else if (ts == 2)
                            window.open(ALC_URI.Admin + "/PrintAppraisalNeedAndService?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                        else if (ts == 3)
                            window.open(ALC_URI.Admin + "/PrintAdmissionAgreement?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                        else if (ts == 4)
                            window.open(ALC_URI.Admin + "/PrintMovein?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                        else if (ts == 5) {
                            var ta = $("#AssessmentForm").tabs('option', 'selected');
                            if (ta == 6)
                                window.open(ALC_URI.Marketer + "/PrintMorseFallScale?id=" + $("#AssessmentId").val(), '_blank');
                            else
                                window.open(ALC_URI.Marketer + "/PrintAssessment?id=" + $("#AssessmentId").val(), '_blank');
                        }
                    },
                    id: "AdmissionPrint",
                    text: "Print"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            },
            resize: function (event, ui) {
                resizeAdmForm();
                resizeAsmtForm();
            }
        }).dialog("open");
        if (mode == 2) {
            $("#admForm").tabs('option', 'disabled', []);
            $("#residentForm").tabs('option', 'disabled', []);

        } else if (mode == 1)
            $("#residentForm").tabs('option', 'disabled', [3, 4, 5, 6]);

        var resizeAdmForm = function () {
            var diag = $('.ui-dialog-content');
            diag = diag.find('#admForm').parents('.ui-dialog-content');
            var height = 0;
            height = diag.eq(0).height() || 0;
            if (height <= 0)
                height = diag.eq(1).height() || 0;
            $('#admForm .tabPanels').height(height - 90);
        }
        resizeAdmForm();

        var resizeAsmtForm = function () {
            var diag = $('.ui-dialog-content');
            diag = diag.find('#admForm').parents('.ui-dialog-content');
            var height = 0;
            height = diag.eq(0).height() || 0;
            if (height <= 0)
                height = diag.eq(1).height() || 0;

            $('#assessmentContainer').height(height - 90);
            $('#assessmentContent,#assessmentTreeContainer').height(height - 90);
            $("#assessmentTreeView").height(height - 110);
            $("#AssessmentForm").height(height - 92);
            $('#assessmentTreeView > ol.tree').css({ 'min-width': 'initial', 'height': 'auto' });
            $('#tabs-1b,#tabs-2b,#tabs-3b,#tabs-4b,#tabs-5b,#tabs-6b,#tabs-7b').height(height - 94);
        }
        resizeAsmtForm();
    }

    $('#btn-printsr').click(function (e) {
        var prt = $('#printsr').val();
        if ($('#admForm #txtAdmissionId').val()) {
            if (prt == "dc") {
                window.open(ALC_URI.Admin + "/PrintDocumentChecklist?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return;
            }
            else if (prt == "ans") {
                window.open(ALC_URI.Admin + "/PrintAppraisalNeedAndService?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return;
            }
            else if (prt == "aa") {
                window.open(ALC_URI.Admin + "/PrintAdmissionAgreement?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return;
            }
            else if (prt == "rmi") {
                window.open(ALC_URI.Admin + "/PrintMovein?id=" + $('#admForm #txtAdmissionId').val(), '_blank');
                return;
            }

        }
        else {
            alert('Resident has no Admission');
            return;
        }
        if ($("#AssessmentId").val()) {
            if (prt == "mfs") {
                window.open(ALC_URI.Marketer + "/PrintMorseFallScale?id=" + $("#AssessmentId").val(), '_blank');
                return;
            }
            else if (prt == "ass") {
                window.open(ALC_URI.Marketer + "/PrintAssessment?id=" + $("#AssessmentId").val(), '_blank');
                return;
            }
        }
        else {
            alert('Resident has no Assessment');
            return;
        }
    });

    $('#tabs-2,#tabs-3').click(function (e) {
        var G = '#fGrid';
        if (this.id == 'tabs-3') G = '#oGrid';
        var trgt = $(e.target);
        //debugger;
        if (trgt.attr('role') == 'gridcell' || trgt.is('.add') || trgt[0].id.indexOf('jqg') == 0 || trgt.hasClass('editable') || trgt.hasClass('customelement')) return true;
        var editRow = $(G + ' tr[editable=1]');
        if (editRow.length > 0) {
            editRow.each(function (i) {
                $(G).jqGrid('saveRow', this.id);
                var row = $(G).jqGrid('getRowData', this.id);
                row.E = true;
                $(G).jqGrid('setRowData', this.id, row);
            });
        }
    });

    $('#residentGridToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('#residentForm').clearFields();
            showdiag(1);
        } else if (q.is('.del')) {
            //debugger;
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Firstname + ' ' + row.Lastname + " deleted successfully!";
                                $('#grid').trigger('reloadGrid');
                                if (m.result == -3) //inuse
                                    msg = row.Firstname + ' ' + row.Lastname + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                            });
                        }
                    }, 100);
                }

            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.admres')) {
            //debugger;
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to Admit selected Resident?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(5, { Id: row.Id }, function (m) {
                                var msg = row.Firstname + ' ' + row.Lastname + " Admitted Successfully!";
                                $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                var countResVal = parseInt($('#countRId').text()) - 1;
                                $('#countRId').text(countResVal);
                                if (m.result == -2) //inuse
                                    msg = "ERROR! " + row.Firstname + ' ' + row.Lastname + " is already Admitted.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                            });
                        }
                    }, 300);
                }

            } else {
                alert("No resident to admit.")
            }
        }
    });

    $('#financialGridToolbar,#otherGridToolbar').on('click', 'a', function () {
        var g = $(this).parents('div.toolbar')[0].id == 'financialGridToolbar' ? '#fGrid' : '#oGrid';
        var q = $(this);
        var G = $(g);
        if (q.is('.add')) {
            var editRow = $(g + ' tr[editable=1]');
            if (editRow.length > 0) {
                editRow.each(function (i) {
                    G.jqGrid('saveRow', this.id);
                    var row = $(G).jqGrid('getRowData', this.id);
                    row.E = true;
                    $(G).jqGrid('setRowData', this.id, row);
                });
            }
            G.jqGrid('addRow', "new");
            G.attr('lastRow', G.jqGrid('getGridParam', 'selrow'));
        } else if (q.is('.del')) {
            var id = G.jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = G.jqGrid('getRowData', id);
                        if (row) {
                            row.D = 'true';
                            $(G).jqGrid('setRowData', id, row);
                            $(g + ' tr[id=' + id + ']').addClass("not-editable-row").hide();
                            //G.jqGrid('delGridRow', id, {
                            //    afterShowForm: function ($form) {
                            //        $("#dData", $form.parent()).click();
                            //    }
                            //});
                        }
                    }, 100);
                }

            } else {
                alert("Please select row to delete.")
            }
        }
    });


    $(function () {
        //var dataArray = [
        //    { Id: '1', Description: 'Supply 101', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFFO09', Status: '1', Cost: '655.20', Type: '1' },
        //    { Id: '2', Description: 'Supply 111', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFDO03', Status: '0', Cost: '231.20', Type: '2' },
        //    { Id: '3', Description: 'Supply 201', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFFO02', Status: '1', Cost: '112.20', Type: '1' },
        //    { Id: '4', Description: 'Supply 301', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EF4G01', Status: '0', Cost: '432.20', Type: '2' },
        //    { Id: '5', Description: 'Supply 401', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFRS55', Status: '1', Cost: '211.20', Type: '1' },

        //];

        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=resident_movein&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',

            //'Admission Date' column added by Mariel P. on 6/20/17
            colNames: ['Id', 'Name', 'MiddleInitial', 'Lastname', 'Room', 'RoomId', 'Gender', 'FullName', 'SSN', 'Birthdate', 'Admission Date', 'Admit Status'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Firstname', index: 'Firstname', formatter: "dynamicLink", formatoptions: {
                      cellValue: function (cellValue, rowId, rowData, options) {
                          var name = [];
                          if (cellValue != "") name.push(cellValue);
                          if (rowData != null) {
                              if (rowData.MiddleInitial && rowData.MiddleInitial != '') {
                                  name.push(" " + rowData.MiddleInitial.toUpperCase());
                              }
                              if (rowData.Lastname && rowData.Lastname != '') name.push(" " + rowData.Lastname);
                          }
                          return name.join('');
                      },
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('#residentForm .resForm').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax(0, row, function (res) {
                                  $('#residentForm .resForm').mapJson(res);
                                  showdiag(2);
                                  var title = $("#diag").dialog("option", 'title');
                                  $("#diag").dialog("option", 'title', 'Resident - ' + cellText);
                                  $('#fGrid').jqGrid('setGridParam', { data: res.FGrid }).trigger('reloadGrid');
                                  $('#oGrid').jqGrid('setGridParam', { data: res.OGrid }).trigger('reloadGrid');
                                  loadAssessment(row);

                              });
                              $("#admForm").tabs("option", "selected", 0);
                              doAjaxAdm(0, row, function (res) {
                                  $("#admForm").clearFields();
                                  $("#admForm").mapJson(res, 'id', "#admForm");
                                  $('#txtResidentName,#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val(res.txtResidentName);
                                  $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                                  $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#DRoomId option:selected').text());
                                  $('#AADOB').val($('#ANSDOB').val());
                              });
                          }
                      }
                  }
              },
              { key: false, hidden: true, name: 'MiddleInitial', index: 'MiddleInitial' },
              { key: false, hidden: true, name: 'Lastname', index: 'Lastname' },
              { key: false, name: 'RoomName', index: 'RoomName' },
              { key: false, hidden: true, name: 'RoomId', index: 'RoomId' },
              {
                  key: false, name: 'Gender', index: 'Gender', formatter: function (cellValue, rowId, rowData, options) {
                      if (cellValue == "M") return "Male";
                      return "Female";
                  }
              },
              { key: false, hidden: true, name: 'Name', index: 'Name', },
              { key: false, name: 'SSN', index: 'SSN', },
              { key: false, name: 'Birthdate', index: 'Birthdate', },
              { key: false, name: 'AdmissionDate', index: 'AdmissionDate', },
              { key: false, name: 'AdmissionStatus', index: 'AdmissionStatus', formatter: rowColorFormatter }
            ],
            //pager: jQuery('#pager'),
            gridComplete: function () {
                for (var i = 0; i < rowsToColor.length; i++) {
                    var AdmissionStatus = $("#" + rowsToColor[i]).find("td").eq(-1).html();
                    if (AdmissionStatus == "Move In For Follow Up") {
                        //      $("#" + rowsToColor[i]).find("td").css("background-color", "#ff9933");
                    }
                }
            },
            scroll: true,
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if ($('#ftrRoom').find('option').size() <= 0) {
                    var optsRm = [];

                    $.each(unique($.map(data, function (o) { return o.RoomName })), function () {
                        optsRm.push('<option>' + this + '</option>');
                    });
                    $('#ftrRoom').html(optsRm.join(''));

                    $("#ftrRoom").html($('#ftrRoom option').sort(function (x, y) {
                        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                    }));
                    $("#ftrRoom").prepend('<option>All</option>');
                    $("#ftrRoom").get(0).selectedIndex = 0;
                }
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;
                //Grid.Settings.init();
            }
        });
        $("#grid").jqGrid('bindKeys');
    });

    $('#ftrRoom,#ftrResident').change(function () {
        var res = $('#ftrResident').val();
        var rm = $('#ftrRoom :selected').text();
        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        if (rm != 'All')
            ftrs.rules.push({ field: 'RoomName', op: 'eq', data: rm });
        if (res != '')
            ftrs.rules.push({ field: 'Name', op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });
    function rowColorFormatter(cellValue, options, rowObject) {
        if (cellValue == "Move In For Follow Up") {
            rowsToColor[rowsToColor.length] = options.rowId;
        }
        return cellValue;
    }
    function unique(array) {
        return $.grep(array, function (el, index) {
            return index == $.inArray(el, array);
        });
    }

    function capsFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    $("#diagTask").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Add Service Task",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });

    $('#txtTimeIn').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#txtCarestaff').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#Onetimedate').trigger('change');
        }
    });
    $('#txtTimeIn').mask('99:99 xy');

    var showdiagTask = function (mode) {
        $("#diagTask").dialog("option", {
            width: 500,
            height: 500,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax2(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diagTask").dialog("close");
                                //initService();
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                }
                                $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');

                            } else if (m.result == -2) {
                                var resVisible = $('#txtResidentId').is(':visible');
                                $.growlWarning({ message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", delay: 6000 });
                                //there is already a schedule for a particular resident or room
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diagTask").dialog("close");
                }
            }
        }).dialog("open");
    }

    var doAjax2 = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "activity_schedule", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        var isValid = $('#diagTask .form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }
            row = $('#diagTask .form').extractJson();
            //validate time

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];
            delete row['recurring'];
            delete row['Activity'];
            delete row['Dept'];
            delete row['Onetimedate'];

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring) {
                row.isonetime = '0';
                $('.dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    if (d.is(':checked')) {
                        switch (d.attr('name')) {
                            case 'Sun': dow.push('0'); break;
                            case 'Mon': dow.push('1'); break;
                            case 'Tue': dow.push('2'); break;
                            case 'Wed': dow.push('3'); break;
                            case 'Thu': dow.push('4'); break;
                            case 'Fri': dow.push('5'); break;
                            case 'Sat': dow.push('6'); break;
                        }
                    }
                });

                if (dow.length == 0) {
                    alert('Please choose day of week schedule.');
                    return;
                }

            } else {
                row.isonetime = '1';
                var onetimeval = $('#Onetimedate').val();
                if (onetimeval == '') {
                    alert("Please choose a one time date schedule.");
                    return;
                }

                var onetimeday = moment(onetimeval).day();
                if ($.isNumeric(onetimeday))
                    dow.push(onetimeday.toString());

                row.ScheduleTime = onetimeval + ' ' + $('#txtTimeIn').val();
            }
            row.Recurrence = dow.join(',');
        }

        var data = { func: "activity_schedule", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    $('#txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#viewActivity').val(title);
        $('#txtCarestaff')[0].options.length = 0;
        toggleCarestafflist();
    });

    var bindDept = function (dptid, cb) {
        var data = { deptid: dptid };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetServicesAndCarestaffByDeptId",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //$('#txtCarestaff').empty();
                //$('<option/>').val('').html('').appendTo('#txtCarestaff');
                //$.each(m.Carestaffs, function (i, d) {
                //    $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaff');
                //});

                $('#txtActivity').empty();
                $('#viewActivity').val('');
                var opt = '<option val=""></option>';
                $.each(m.Services, function (i, d) {
                    opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
                });
                $('#txtActivity').append(opt);
                if (typeof (cb) !== 'undefined') cb();
            }
        });
    }

    var toggleCarestafflist = function () {
        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
        if (isrecurring)
            triggerDayofWeekClick();
        else
            $('#Onetimedate').trigger('change');

        //if housekeeper or maintenance hide room dropdown
        var val = $('#ddlDept').val();
        if (val == 5 || val == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    if ($('#ddlDept').length > 0) {
        $('#ddlDept').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('table.dayofweek input').removeAttr('disabled');
            $('#Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('table.dayofweek input').attr('disabled', 'disabled');
            $('table.dayofweek').clearFields();
            $('#Onetimedate').removeAttr('disabled');
        }
    });

    var triggerDayofWeekClick = function () {
        var a = $('#txtTimeIn').val();
        if (a == null || a == '') {
            $('#txtCarestaff').empty();
            //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
            return;
        }

        var dow = [];
        $('.dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun': dow.push('0'); break;
                    case 'Mon': dow.push('1'); break;
                    case 'Tue': dow.push('2'); break;
                    case 'Wed': dow.push('3'); break;
                    case 'Thu': dow.push('4'); break;
                    case 'Fri': dow.push('5'); break;
                    case 'Sat': dow.push('6'); break;
                }
            }
        });
        bindCarestaff(dow);

    }

    $('table.dayofweek input').click(triggerDayofWeekClick);

    var bindCarestaff = function (dow) {
        if ($('#txtTimeIn').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if ($('#txtActivity').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if (dow == null) dow = [];
        var csid = $('input[xid="hdCarestaffId"]').val();
        if (csid == "") csid = null;

        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');

        var data = { scheduletime: $('#txtTimeIn').val(), activityid: $('#txtActivity').val(), carestaffid: csid, recurrence: dow.join(',') };
        data.isonetime = isrecurring ? "0" : "1";
        data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#Onetimedate').val()).format("MM/DD/YYYY");
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#txtCarestaff').empty();
                $('<option/>').val('').html('').appendTo('#txtCarestaff');
                $.each(m, function (i, d) {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                });
            }
        });
    }


    $('#Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff(dow);
    });

    $('#AddServiceToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            AddService();
        } else if (q.is('.del')) {
            var id = $('#sGrid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#sGrid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax2(3, { Id: row.Id }, function (m) {
                                var msg = "Task deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = "Task cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3) {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                    $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                }
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.refresh')) {
            //fromSort = true;
            //new Grid_Util.Row().saveSelection.call( $( '#sGrid' )[0] );
            $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
        }
    });

    var AddService = function () {
        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#residentForm #txtId').val();
            if (!id) return;
        }

        $("#ddlDept option[value='5']").remove();
        $("#ddlDept option[value='7']").remove();
        var dept = $('#ddlDept').val();
        $('#diagTask').clearFields();
        $('#liResident select').val(id);
        $('input[xid="hdCarestaffId"]').val('');
        $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
        $('input[name="recurring"]:eq(1)').removeAttr('checked');
        $('#Onetimedate').attr('disabled', 'disabled');
        $('.dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
        showdiagTask(1);
        //if housekeeper or maintenance hide room dropdown
        $('#ddlDept').val(dept);
        if (dept == 5 || dept == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    $('#historyServiceToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.resetSend')) {
            //to do
        } else if (q.is('.print')) {
            //do print
        } else if (q.is('.refresh')) {
            initHistory();
        }
    });

    var initService = function () {
        if ($('#sGrid')[0] && $('#sGrid')[0].grid)
            $.jgrid.gridUnload('sGrid');
        var SGrid = $('#sGrid');

        var GURow = new Grid_Util.Row();
        var pageWidth = $("#sGrid").parent().width() - 100;

        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#diagTask #txtId').val();
            if (!id) return;
        }

        SGrid.jqGrid({
            url: "Admin/GetGridData?func=activity_schedule&param=-1&param2=" + id,// + $('#ftrRecurrence').val(),
            datatype: 'json',
            postData: '',
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Activity', 'Department', 'Resident', 'Staff', 'Room', 'Schedule', 'Recurrence', 'Duration (Minutes)'/*,'Date',  'Completed', 'Remarks'*/],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Activity', index: 'Activity', sortable: true, width: (pageWidth * (8 / 100)), formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, irow, icol, celltext, e) {
                          var G = $('#sGrid');
                          G.setSelection(rowid, false);
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax2(0, row, function (res) {
                                  $("#diagTask .form").clearFields();
                                  bindDept(res.Dept.toString(), function () {
                                      if (!res.IsOneTime)
                                          res.Onetimedate = '';
                                      $("#diagTask .form").mapJson(res);
                                  });
                                  $("#ddlDept option[value='5']").remove();
                                  $("#ddlDept option[value='7']").remove();
                                  //if housekeeper or maintenance hide room dropdown
                                  if (res.Dept == 5 || res.Dept == 7) {
                                      $('#liRoom, #liRoom select').show();
                                      $('#liResident, #liResident select').hide();
                                  } else {
                                      $('#liResident, #liResident select').show();
                                      $('#liRoom, #liRoom select').hide();
                                  }
                                  //map day of week
                                  if (!res.IsOneTime) {
                                      $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
                                      if (res.Recurrence != null && res.Recurrence != '') {
                                          var setCheck = function (el, str, idx) {
                                              if (str.indexOf(idx) != -1)
                                                  el.prop('checked', true);
                                          }
                                          var dow = res.Recurrence;
                                          $('.dayofweek').find('input[type="checkbox"]').each(function () {
                                              var d = $(this);
                                              switch (d.attr('name')) {
                                                  case 'Sun': setCheck(d, dow, '0'); break;
                                                  case 'Mon': setCheck(d, dow, '1'); break;
                                                  case 'Tue': setCheck(d, dow, '2'); break;
                                                  case 'Wed': setCheck(d, dow, '3'); break;
                                                  case 'Thu': setCheck(d, dow, '4'); break;
                                                  case 'Fri': setCheck(d, dow, '5'); break;
                                                  case 'Sat': setCheck(d, dow, '6'); break;
                                              }
                                          });
                                          //debugger;
                                          $('input[name="recurring"]:eq(0)').trigger('click');
                                          $('#Onetimedate').val('');
                                      }
                                  } else {
                                      $('input[name="recurring"]:eq(1)').trigger('click');
                                      res.Recurrence = moment(res.Onetimedate).day().toString();
                                  }
                                  $('input[xid="hdCarestaffId"]').val(res.CarestaffId);
                                  var data = { scheduletime: res.ScheduleTime, activityid: res.ActivityId, carestaffid: res.CarestaffId, recurrence: res.Recurrence };
                                  data.isonetime = !res.IsOneTime ? "0" : "1";
                                  data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
                                  $.ajax({
                                      type: "POST",
                                      contentType: "application/json; charset=utf-8",
                                      url: "Admin/GetCarestaffByTime",
                                      data: JSON.stringify(data),
                                      dataType: "json",
                                      success: function (m) {
                                          $('#txtCarestaff').empty();
                                          $('<option/>').val('').html('').appendTo('#txtCarestaff');
                                          $.each(m, function (i, d) {
                                              $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                                          });
                                          $('#txtCarestaff').val(res.CarestaffId);
                                          showdiagTask(2);
                                      }
                                  });
                              });
                          }
                      }
                  }
              },
              { key: false, name: 'Department', index: 'Department', sortable: true, width: (pageWidth * (8 / 100)) },
              { key: false, name: 'Resident', index: 'Resident', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'Carestaff', index: 'Carestaff', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'Room', index: 'Room', sortable: true, width: (pageWidth * (4 / 100)) },
              //{ key: false, name: 'ScheduleDate', index: 'ScheduleDate' },
              { key: false, name: 'Schedule', index: 'Schedule', sortable: true, width: (pageWidth * (8 / 100)) },
               { key: false, name: 'Recurrence', index: 'Recurrence', sortable: true, width: (pageWidth * (8 / 100)) },
              { key: false, name: 'Duration', index: 'Duration', sortable: true, width: (pageWidth * (5 / 100)) },
              //{ key: false, name: 'Completed', index: 'Completed', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Yes', '0': 'No' } } },
              //{ key: false, name: 'Remarks', index: 'Remarks' }
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                //if ($('#ftrResident').find('option').size() > 0) return;
                //var optsRes = ['<option>All</option>'], optsCS = ['<option>All</option>'], optsDP = ['<option>All</option>'];
                //$.each(unique($.map(data, function (o) { return o.Resident })), function () {
                //    optsRes.push('<option>' + this + '</option>');
                //});
                //$.each(unique($.map(data, function (o) { return o.Carestaff })), function () {
                //    optsCS.push('<option>' + this + '</option>');
                //});
                //$.each(unique($.map(data, function (o) { return o.Department })), function () {
                //    optsDP.push('<option>' + this + '</option>');
                //});
                //$('#ftrResident').html(optsRes.join(''));
                //$('#ftrCarestaff').html(optsCS.join(''));
                //$('#ftrDept').html(optsDP.join(''));

                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#sGrid").setSelection(maxId);
                delete isReloaded;
            }
        });
        SGrid.jqGrid('bindKeys');
        $('#residentForm div.tabPanels:eq(0)').layout({
            center: {
                paneSelector: "#serviceGrid",
                closable: false,
                slidable: false,
                resizable: true,
                spacing_open: 0
            },
            north: {
                paneSelector: "#AddServiceToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            onresize: function () {
                resizeGridsS();
                //resizeGridHS();
            }
        });
        resizeGridsS();
    }

    var resizeGridsS = (function () {
        //debugger;
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#AddServiceToolbar').height();
            $("#sGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#sGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
            //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
            //console.log($("#sGrid").height())
        }
        setTimeout(f, 100);
        return f;
    });


    $("#fromDate,#toDate").datepicker({
        formatDate: "MM/dd/yyyy",
        onClose: function () {
            var dis = $(this);
            if (dis.val() == "")
                dis.val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));

            if (moment($("#toDate").val()) < moment($("#fromDate").val())) {
                alert('"To Date" should be greather or equal to "From Date"');
                $("#toDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));
                return;
            }

            initHistory();
        }
    });
    $("#fromDate,#toDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));

    var initHistory = function () {
        var removeByAttr = function (arr, attr, value) {
            var i = arr.length;
            while (i--) {
                if (arr[i]
                    && arr[i].hasOwnProperty(attr)
                    && (arguments.length > 2 && arr[i][attr] === value)) {

                    arr.splice(i, 1);

                }
            }
            return arr;
        }

        function beforeProcessing(data, status, xhr) {
            var delList = [];
            if (data && data.length > 0) {
                //search for items with xref and check if it has correspoding acivity_schedule_id in the list
                //if exists then remove item with xref and preserve the record with activity_schedule_id = xref
                var occ = $.grep(data, function (a) { return a.xref > 0 });
                $.each(occ, function (x, y) {
                    var matchObj = $.grep(data, function (b) {
                        return b.activity_schedule_id == y.xref;
                    });
                    if (matchObj.length > 0) {
                        delList.push(y);
                    }
                });

                $.each(delList, function (i, a) {
                    removeByAttr(data, 'activity_schedule_id', a.activity_schedule_id);
                });
            }
        }

        if ($('#hGrid')[0] && $('#hGrid')[0].grid)
            $.jgrid.gridUnload('hGrid');
        var HGrid = $('#hGrid');

        var GURow = new Grid_Util.Row();
        var pageWidth = $("#hGrid").parent().width() - 100;

        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#diagTask #txtId').val();
            if (!id) return;
        }

        HGrid.jqGrid({
            url: "Staff/GetGridData?func=activity_schedule&param=" + $('#fromDate').val() + "&param2=" + $('#toDate').val() + "&param3=" + id,
            beforeProcessing: function (data, status, xhr) { beforeProcessing(data, status, xhr) },
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['ActivityId', 'ActivityScheduleId', 'CarestaffActivityId', 'Task', 'Procedure', 'Department', 'Resident', 'Room', 'Staff', 'Schedule', 'Duration (Minutes)', 'Completed', 'oStat', 'Acknowledge Date', 'Acknowledge By', 'Remarks', 'Reschedule', 'XRef', 'TimeLapsed'],
            colModel: [
              { key: false, hidden: true, name: 'activity_id', index: 'activity_id' },
              { key: true, hidden: true, name: 'activity_schedule_id', index: 'activity_schedule_id' },
              { key: false, hidden: true, name: 'carestaff_activity_id', index: 'carestaff_activity_id' },
              {
                  key: false, name: 'activity_desc', index: 'activity_desc', width: 200
                  //, formatter: "dynamicLink", formatoptions: {
                  //onClick: function (rowid, irow, icol, celltext, e) {
                  //    var G = $('#grid');
                  //    G.setSelection(rowid, false);
                  //    var row = G.jqGrid('getRowData', rowid);
                  //    if (row) {
                  //        $('#diag').clearFields();
                  //        $('#diag #CarestaffActivityId').val(row.carestaff_activity_id);
                  //        $('#diag #ActivityScheduleId').val(row.activity_schedule_id);
                  //        //$('#diag #AcknowledgedBy').val(row.acknowledged_by);
                  //        //$('#diag #Id').val(row.carestaff_activity_id);
                  //        //$('#diag #Remarks').val(row.remarks);
                  //        if (row.reschedule_dt && row.reschedule_dt != '')
                  //            $('#diag #Remarks').val("Rescheduled - " + row.reschedule_dt);
                  //        else
                  //            $('#diag #Remarks').val(row.remarks);

                  //        if (row.oactivity_status == false)
                  //            $('#diag #No').attr('checked', 'checked');
                  //        if (row.oactivity_status == true)
                  //            $('#diag #Yes').attr('checked', 'checked');

                  //        $('#diag #staffInfo').html('Is the selected task completed?');
                  //        showdiag();
                  //        if ((moment($('#curDate').val()).format("MM/DD/YYYY") != moment(ALC_Util.CurrentDate).format("MM/DD/YYYY")) || (row.reschedule_dt && row.reschedule_dt != '')) {
                  //            $('#diag').find('input, textarea, button, select').attr('disabled', 'disabled');
                  //            $('.ui-dialog-buttonset').hide();
                  //        } else {
                  //            $('#diag').find('input, textarea, button, select').removeAttr('disabled');
                  //            $('.ui-dialog-buttonset').show();
                  //        }

                  //    }
                  //}
                  //}
              },
              { key: false, name: 'activity_proc', width: 200, index: 'activity_proc' },
              { key: false, name: 'department', width: 100, index: 'department' },
              { key: false, name: 'resident', index: 'resident', width: (pageWidth * (12 / 100)) },
              { key: false, name: 'room', index: 'room', width: (pageWidth * (8 / 100)) },
              { key: false, name: 'carestaff_name', index: 'carestaff_name', width: (pageWidth * (12 / 100)) },
              { key: false, name: 'start_time', index: 'start_time', width: (pageWidth * (8 / 100)) },
              { key: false, name: 'service_duration', index: 'service_duration', width: (pageWidth * (12 / 100)) },
              {
                  key: false, name: 'activity_status', index: 'activity_status', width: (pageWidth * (8 / 100)), edittype: "select",
                  formatter: function (cellvalue, options, row) {
                      //do custom formatting here
                      if (row.carestaff_activity_id == null && row.timelapsed) {
                          return '<span style="background:#d61b1b;color:white;font-size: 9px;letter-spacing: 1px;padding: 2px 4px;border-radius: 5px 5px;">Missed</span>';
                      }
                      var selected = cellvalue == 1 ? ' checked="checked"' : '';

                      var htm = "<input disabled type='checkbox'" + selected + "></input>";
                      return htm;
                  }
              },
              { key: false, hidden: true, name: 'oactivity_status', index: 'oactivity_status' },
              {
                  key: false, name: 'completion_date', index: 'completion_date', formatter: function (cellvalue, options, row) {
                      if (row.completion_date)
                          return moment(row.completion_date).format('MM/DD/YYYY');
                      return '';
                  }
              },
              { key: false, name: 'acknowledged_by', index: 'acknowledged_by' },
              {
                  key: false, name: 'remarks', index: 'remarks', formatter: function (cellvalue, options, row) {
                      if (row.reschedule_dt && row.reschedule_dt != '') {
                          var htm = "<a xref_id='" + row.xref + "'>Rescheduled - " + row.reschedule_dt + "</a>";
                          return htm;
                      } else {
                          return cellvalue;
                      }

                  }
              },
              { key: false, hidden: true, name: 'reschedule_dt', index: 'reschedule_dt' },
              { key: false, hidden: true, name: 'xref', index: 'xref' },
              { key: false, hidden: true, name: 'timelapsed', index: 'timelapsed' }
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            shrinkToFit: false,
            forceFit: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#hGrid").setSelection(maxId);
                delete isReloaded;
            }
        });
        HGrid.jqGrid('bindKeys');
        $('#tabs-5').layout({
            center: {
                paneSelector: "#historyGrid",
                closable: false,
                slidable: false,
                resizable: true,
                spacing_open: 0
            },
            north: {
                paneSelector: "#historyServiceToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            onresize: function () {
                resizeGridHS();
                resizeGridsS();
            }
        });
        resizeGridHS();
    }

    var resizeGridHS = (function () {
        //debugger;
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#historyServiceToolbar').height();
            $("#hGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#hGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
            //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
            //console.log($("#sGrid").height())
        }
        setTimeout(f, 100);
        return f;
    });

    var initDocuments = function () {
        if ($('#dGrid')[0] && $('#dGrid')[0].grid)
            $.jgrid.gridUnload('dGrid');
        var DGrid = $('#dGrid');

        var GURow = new Grid_Util.Row();
        var pageWidth = $("#dGrid").parent().width() - 100;

        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) return;

        DGrid.jqGrid({
            url: "Admin/GetGridData?func=documents&param=" + id,
            datatype: 'json',
            postData: '',
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Filename', 'Description', 'Size', 'UUID', 'Type', 'Uploaded By', 'Date Uploaded'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Filename', index: 'Filename', sortable: true, width: (pageWidth * (12 / 100)), formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, irow, icol, celltext, e) {
                          var G = $('#dGrid');
                          G.setSelection(rowid, false);
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              window.location.href = "Admin/DownloadDocumentFile/?DOCID=" + row.UUID;
                          }
                      }
                  }
              },
              { key: false, name: 'Description', index: 'Description', sortable: true, width: (pageWidth * (10 / 100)) },
              {
                  key: false, name: 'Size', index: 'Size', sortable: true, width: (pageWidth * (4 / 100)), formatter: function (cellvalue, options, row) {
                      return (cellvalue / 1000) + "KB";
                  }
              },
              { key: false, hidden: true, name: 'UUID', index: 'UUID' },
              { key: false, name: 'Filetype', index: 'Filetype', sortable: true, width: (pageWidth * (10 / 100)) },
              { key: false, name: 'CreatedBy', index: 'CreatedBy', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'DateCreated', index: 'DateCreated', sortable: true, width: (pageWidth * (4 / 100)) }
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#dGrid").setSelection(maxId);
                delete isReloaded;
            }
        });
        DGrid.jqGrid('bindKeys');
        $('#residentForm div.tabPanels:eq(0)').layout({
            center: {
                paneSelector: "#documentGrid",
                closable: false,
                slidable: false,
                resizable: true,
                spacing_open: 0
            },
            north: {
                paneSelector: "#DocumentToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            onresize: function () {
                resizeGridsS();
                //resizeGridHS();
            }
        });
        resizeGridsS();
    }

    var resizeGridsS = (function () {
        //debugger;
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#DocumentToolbar').height();
            $("#dGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#dGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
        }
        setTimeout(f, 100);
        return f;
    });

    //var repopulateResident = function () {
    //    $.ajax({
    //        type: "GET",
    //        contentType: "application/json; charset=utf-8",
    //        url: "Admin/GetResident",
    //        dataType: "json",
    //        success: function (m) {
    //            //$('#txtResidentId').empty();
    //            //$('<option/>').val('').html('').appendTo('#txtResidentId');
    //            //$.each(m, function (i, d) {
    //            //    $('<option/>').val(d.Id).html(d.Name).appendTo('#txtResidentId');
    //            //});
    //            //$('#txtResidentId').val(res.CarestaffId);
    //            showdiagTask(2);
    //        }
    //    });

    //}

    $('#DocumentToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.upload')) {
            uploadDocument();
        }
        else if (q.is('.del')) {
            var docid = $('#dGrid').jqGrid('getGridParam', 'selrow');
            if (!docid) return;
            if (!confirm("Are you sure you want to delete the selected row?")) return;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify({ func: 'documents', mode: 3, data: { docId: docid } }),
                dataType: "json",
                success: function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({ message: "Document has beend deleted successfully!", delay: 3000 });
                        initDocuments();
                    } else
                        $.growlError({ message: m.message, delay: 6000 });
                }
            });
        }
        else if (q.is('.refresh')) {
            initDocuments();
        }
    });

    function truncate(n) {
        var len = 40;
        var ext = n.substring(n.lastIndexOf(".") + 1, n.length).toLowerCase();
        var filename = n.replace('.' + ext, '');
        if (filename.length <= len) {
            return n;
        }
        var flen = filename.length;
        var f1 = filename.substr(0, (len / 2));
        var f2 = filename.substr((flen - (len / 2)), flen);
        filename = f1 + '...' + f2;
        return filename + '.' + ext;
    }

    function uploadDocument() {
        //var dialog_html = "<div class=\"form\"><ul class=\"w100\">" +
        //    "<li><label for=\"fileName\">Name:</label><input type=\"text\" id=\"fileName\" /></li>" +
        //    "<li><label for=\"fileDescription\">Description:</label><textarea id=\"fileDescription\"></textarea></li>" +
        //    "<li><label for=\"fileUpload\">File:</label><input id=\"fileUpload\" type=\"file\" name=\"files[]\" class=\"fileUpload\">" + 
        //    "<input id=\"fileText\" class=\"fileText\" type=\"text\" disabled=\"disabled\"/></li>" +
        //    "</ul></div><div id=\"uploadProgress\" class=\"progressPanel\"><div class=\"progressBar\" style=\"width: 0%;\"></div></div>";

        var dialog_html = "<div class='upload-container'><div class='upload-header'>" +
            "<button id='btnAddfiles'>Select Files</button><button id='btnClearList'>Clear List</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +
            "<div id='fileList' class='upload-list'></div><div class='upload-footer'><div id='divLegend'>" +
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded</div></div></div></div>";

        var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a class='status-loading' style='display:none'>&nbsp;</a></td>" +
            "<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" +
            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        var $dialog = $("<div><div class=\"dialogContent\">" + dialog_html + "</div></div>").dialog({
            modal: true,
            title: "Upload File",
            width: 500,
            close: function () {
                $dialog.find("#fileupload").fileupload("destroy");
                initDocuments();
                $dialog.dialog("destroy").remove();
            },
            buttons: {
                "Close": function () {
                    $dialog.dialog("close");
                }
            }
        });
        //$("#btnUpload").attr('disabled', 'disabled').removeClass('primaryBtn').addClass('primaryBtn-disabled');
        $("#fileUpload").on('change', function () {
            var itms = $('.upload-container .totalfiles span:eq(1)');
            if (parseInt(itms.text()) > 0)
                itms.text(parseInt(itms.text()) + this.files.length);
            else
                itms.text(this.files.length);
        });
        $('#btnAddfiles').on('click', function () {
            $("#fileUpload").trigger('click');
        });
        $('#btnClearList').on('click', function () {
            $('#fileList').html('');
            $('.totalfiles').html("<span>0</span> of <span>0</span> Files Uploaded");
        });
        $dialog.find("#fileUpload").fileupload({
            url: "Admin/UploadFile",
            dataType: "json",
            done: function (e, data) {
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');
                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');
                        var itms = $('.upload-container .totalfiles span:eq(0)');
                        itms.text(parseInt(itms.text()) + 1);
                    }
                    return false;
                }
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
            },
            add: function (e, data) {
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xslx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                                    'csv', 'txt', 'rtf', 'zip', 'mp3', 'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif'];

                $.each(files, function (x, i) {
                    var err = "";
                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    //if (this.type.length == 0 || ext)
                    //    err = "The file type is not supported.";
                    //else if (this.type.length && !acceptFileTypes.test(this.type)) {
                    //    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    //    err = "The file type of '." + ext + "' is not supported.";
                    //}
                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('#fileList');
                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('span.size').text(size);
                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        return true;
                    } else {
                        item.find('a:eq(0)').show();
                        item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () { item.remove(); });
                    }
                    data.context = item;
                    data.urlparam = { name: this.name, size: this.size, type: this.type, file: this.file };
                    data.submit();

                });
            },
            submit: function (e, data) {
                var rid = $('#grid').jqGrid('getGridParam', 'selrow');
                data.formData = {
                    paramsid: "documents",
                    fileparams: JSON.stringify(data.urlparam),
                    rid: rid
                };
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({ message: "Error: " + jqXHR.responseText, delay: 6000 });
            }
        });
    }

    var createExcelFromGrid = function (gridID, filename) {
        var grid = $('#' + gridID);
        var rowIDList = grid.getDataIDs();
        var row = grid.getRowData(rowIDList[0]);
        var colNames = [];
        var i = 0;
        for (var cName in row) {
            colNames[i++] = cName; // Capture Column Names
        }
        var html = "";
        for (var j = 0; j < rowIDList.length; j++) {
            row = grid.getRowData(rowIDList[j]); // Get Each Row
            for (var i = 0 ; i < colNames.length ; i++) {
                html += row[colNames[i]] + ';'; // Create a CSV delimited with ;
            }
            html += '\n';
        }
        html += '\n';

        var a = document.createElement('a');
        a.id = 'ExcelDL';
        a.href = 'data:application/vnd.ms-excel,' + html;
        a.download = filename ? filename + ".xls" : 'DataList.xls';
        document.body.appendChild(a);
        a.click(); // Downloads the excel document
        document.getElementById('ExcelDL').remove();
    }

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        $("td:hidden,th:hidden", "#grid").remove();
        $("td img").remove();
        var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Room</td><td bgcolor='#87AFC6'>Gender</td><td bgcolor='#87AFC6'>SSN</td><td bgcolor='#87AFC6'>Birthdate</td><td bgcolor='#87AFC6'>Admission Date</td><td bgcolor='#87AFC6'>Admit Status</td> </tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_ResidentsMoveIn.xls");
        }
        else                 //other browser not tested on IE 11
            a.id = 'ExcelDL';
            a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            a.download = 'ALC_ResidentsMoveIn' + '.xls';
            document.body.appendChild(a);
            a.click(); // Downloads the excel document
            document.getElementById('ExcelDL').remove();
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    });
    //===================

});
