﻿$(function () {
    // START: DATATABLES INITIALIZATION
    var createDatatable = function () {

        $("#csr_table").DataTable({
            responsive: !0,
            pagingType: "full_numbers",
            select: true,
            "scrollX": true,
            "scrollY": true,
            "scrollCollapse": true,
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="la la-download"></i> Export to Excel',
                    title: 'ALC_Rooms',
                    className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                    init: function (api, node, config) {
                        $(node).removeClass('dt-button')
                    }
                }
            ]

        });
        getHeight();

    }
    // END: DATATABLES INITIALIZATION
    //FUNCTIONS
    createDatatable();
    $.ajax({
        url: "Admin/GetGridData?func=resident_admission_list&param=",
        datatype: 'json',
        type: "GET",
        success: function (m) {
            $("#resident_list").html("");

            $.each(m, function (i, v) {

                $('#resident_list').append($('<option>', {
                    value: v.ResidentId,
                    text: v.Firstname + ' ' + (v.MiddleInitial != '' ? v.MiddleInitial + '. ' : '') + v.Lastname,
                    adm: v.AdmissionId
                }));
            })

        }
    });

    setTimeout(function () {
        first_index = $("select#resident_list option:eq(0)").val();
        $("select#resident_list")
            .val(first_index)
            .trigger('change');
        $(".loader").hide();
    }, 1000);


    $.ajax({
        url: "Admin/GetGridData?func=resident_admission_list&param=admission_only",
        datatype: 'json',
        type: "GET",
        success: function (m) {
            
            var html_list = "";
            var gender = "";
            var gender_text;
            if (m) {
                $.each(m, function (i, v) {
                    gender = "";
                    gender_text = "";

                    if (v.Gender == "M") {
                        gender = '<i class="fas fa-mars"></i>';
                        gender_text = 'Male';
                    } else if (v.Gender == "F") {
                        gender = '<i class="fas fa-venus"></i>';
                        gender_text = 'Female';
                    } else if (v.Gender == "") {
                        gender = '<i class="fas fa-genderless"></i>';
                    }

                    html_list += '<div class="m-widget4__item col-lg-3 parent">';
                    html_list += '<div class="m-widget4__img m-widget4__img--pic">';
                    html_list += '<img src="Resource/ResidentImage?id=' + v.ResidentId + '&r=' + Date.now() + '" alt="" style="border-radius: 0px;">';
                    html_list += '</div>';
                    html_list += '<div class="m-widget4__info">';
                    html_list += '<span class="m-widget4__title">';
                    html_list += '<a href="#" class="csr_resident" id="' + v.ResidentId + '" adm="' + v.AdmissionId + '">' + v.Firstname + ' ' + (v.MiddleInitial != '' ? v.MiddleInitial + ' ' : '') + v.Lastname + '</a>';
                    html_list += '</span><br>';
                    html_list += '<span class="m-widget4__sub">';
                    html_list += 'SSN: ' + (v.SSN != "" ? v.SSN : 'N/A');
                    html_list += '</span>';
                    html_list += '</div>';
                    html_list += '</div>';

                })
            }

            $("#csrtile").html(html_list);
            //var scroll1 = new PerfectScrollbar('#csrtile');
        }
    })


    //ON CLICKS

    $(document).on("change", "#resident_list", function () {
    //$(document).on("click", ".csr_resident", function () {
        var admissionid = $('option:selected', this).attr('adm');
        var name = $("#resident_list option:selected").text();
        $.ajax({
            url: "Admin/GetGridData?func=csr&param=" + admissionid,
            datatype: 'json',
            type: "GET",
            success: function (m) {
                //var html_table = "NO DATA TO SHOW";
                var html_table = "";

                if (m.length != 0) {
                    $.each(m, function (i, v) {
                        html_table += "<tr>";
                        html_table += "<td>" + (v.description != null ? v.description : "") + "</td>";
                        html_table += "<td>" + (v.generic != null ? v.generic : "") + "</td>";
                        html_table += "<td>" + (v.strength != null ? v.strength : "") + "</td>";
                        html_table += "<td>" + v.qty + "</td>";
                        html_table += "</tr>";
                    })
                }

                //$("#csrname").html(name);
                //$("#csrqty").html(html_table);
                $("#csrname").html(name);
               // $("#csrqty").html("");
                $("#csr_table").DataTable().destroy();
                $("#csrqty").html(html_table);
                $("#csr_table").DataTable({
                    "scrollX": true,
                    "scrollY": true,
                    "scrollCollapse": true,
                });
                getHeight();
            }

        })
       // var scroll1 = new PerfectScrollbar('#resident_csr');
        
    })

    $("#search_csr_value").keyup(function () {
        $("#csrname").html("");
        $("#csr_table").DataTable().destroy();
        $("#csr_table").DataTable({
            "scrollX": true,
            "scrollY": true,
            "scrollCollapse": true,
        });
       // $("#csrqty").html("");
        var string = $(this).val().toLowerCase();
     
        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
    })

    $("#showall").on("click", function () {
        $(".parent").each(function (i, e) {
            $(this).show();
        });
    })


    function getHeight() {       
        if ($(window).width() > 1024) {
            $('#headerRoom').height($(window).height() - 140);
            $('.dataTables_scrollBody').height($(window).height() - 450);
        }
        $(window).resize(function () {           
            if ($(window).width() > 1024) {
                $('#headerRoom').height($(window).height() - 140);
                $('.dataTables_scrollBody').height($(window).height() - 450);
            }
        })
        var scroll = new PerfectScrollbar('.dataTables_scrollBody');
        //$('#headerRoom').height($(window).height() - 140);
        //if ($(window).width() > 991) {
        //    $('#csrtile').height($(window).height() - 260);
        //    $('#resident_csr').height($(window).height() - 260);
        //} else {
        //    $('#csrtile').height($(window).height() /3);
        //    $('#resident_csr').height($(window).height() /3);
        //}
       
        //$(window).resize(function () {
        //    $('#headerRoom').height($(window).height() - 140);
            //if ($(window).width() > 991) {
            //    $('#csrtile').height($(window).height() - 260);
            //    $('#resident_csr').height($(window).height() - 260);
            //} else {
            //    $('#csrtile').height($(window).height() / 3);
            //    $('#resident_csr').height($(window).height() / 3);
        //    //}
        //})
    }
    getHeight()


})