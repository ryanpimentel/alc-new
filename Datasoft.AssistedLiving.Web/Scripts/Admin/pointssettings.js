﻿var PointSettingState = {
    orig: '',
    current: ''
};
function setHeight() {
    $('.dataTables_scrollBody').height($(window).height() - 425);
    if ($(window).width() < 1024) {
        $('#headerRoom').height($("#headerRoom .m-portlet__body").height() + 100);
    } else {
        $('#headerRoom').height($(window).height() - 140);
    }
    if ($(window).height() < 400) {
        $('.dataTables_scrollBody').height(200);
        $('#headerRoom').height($('#score_table_wrapper').height() + 180);
    }
    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('#headerRoom').height($("#headerRoom .m-portlet__body").height() + 100);
        } else {
            $('#headerRoom').height($(window).height() - 140);
            $('.dataTables_scrollBody').height($(window).height() - 425);
        }
        if ($(window).height() < 400) {
            $('.dataTables_scrollBody').height(200);
            $('#headerRoom').height($('#score_table_wrapper').height() + 180);
        }
        $('#score_table').DataTable().columns.adjust();
    })

    var scroll = new PerfectScrollbar('.dataTables_scrollBody');
    $('#score_table').DataTable().columns.adjust();

}


$(function () {
    $(".loader").show();

    var scoreDatatable = $("#score_table").DataTable();

    var init = function () {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=pointssettings&param=" + $('#assessGroup').val(),
            dataType: "json",
            success: function (m) {
                var html_table = "";

                if (m.length != 0) {

                    $.each(m, function (i, v) {
                      //  debugger
                        var status = v.is_active == 1 ? '<span class="m-badge  m-badge--success m-badge--wide">Active</span>' : '<span class="m-badge m-badge--danger m-badge--wide">Inactive</span>';
                       
                        html_table += "<tr>";
                        html_table += "<td>" + "<a href='#' class='edit_score' id='" + v.AssessmentPointSettingId + "' data-toggle='modal' data-target='#score_modal' fid='" + v.FieldId + "' ss='" + v.ScoreValue + "' ft='" + v.FieldText + "'>" + v.FieldText + "</a></td>";
                        //add kim for edit of scorevalue 02/21/22                                                         
                        html_table += "<td  class='edit_score_value'   id='" + v.AssessmentPointSettingId + "'>" + v.ScoreValue + "</td>";  
                        html_table += "<td  id='" + v.AssessmentPointSettingId + "'>" + status + "</td>"; 
                       // html_table += "<td  contenteditable='true' class='edit_score_value'   id='" + v.AssessmentPointSettingId + "'>" + v.ScoreValue + "</td>";         
                       // html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del ft_deactive' href='#' id='" + v.AssessmentPointSettingId + "'><i class='la la-trash-o'></i> Deactivate</a></div>\n</span></td>";
                        // html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-trash-o'></i>\n</a> <div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del ft_deactive' id='" + v.AssessmentPointSettingId + "' href='#'><i class='la la-edit'></i> Deactivate</a>\n< a class='dropdown-item del edit_score_value' href = '#' id = '" + v.AssessmentPointSettingId  + "' > <i class='la la-edit'></i> Edit</a ></div >\n</span ></td > ";

                        var active = "";
                        if (v.is_active == 1) {
                            active += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del edit_score' href='#' id='" + v.AssessmentPointSettingId + "' data-toggle='modal' data-target='#score_modal' fid='" + v.FieldId + "' ss='" + v.ScoreValue + "' ft='" + v.FieldText + "' > <i class='la la-edit' ></i > Edit</a >\n <a class='dropdown-item del ft_deactive' href = '#' id = '" + v.AssessmentPointSettingId + "' > <i class='la la-trash-o'></i> Deactivate </a></div>\n</span ></td > ";

                        }
                        else if (v.is_active == 0) {
                            active += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item del edit_score' href='#' id='" + v.AssessmentPointSettingId + "' data-toggle='modal' data-target='#score_modal' fid='" + v.FieldId + "' ss='" + v.ScoreValue + "' ft='" + v.FieldText + "' > <i class='la la-edit' ></i > Edit</a >\n <a class='dropdown-item del ft_reactive' href = '#' id = '" + v.AssessmentPointSettingId + "' > <i class='la la-trash-o'></i> Reactivate</a></div>\n</span ></td > ";

                        }



                        html_table += active;
                          
                     
                          

                        //kim modified code 02/28/22
                       
                        html_table += "</tr>";                       

                    })

                }
               
                $("#score_table").DataTable().destroy();
                $("#score_body").html(html_table);

                $("#score_table").DataTable({
                    "scrollY": true,
                    "scrollCollapse": true,
                    responsive: !0,
                    pagingType: "simple_numbers",
                    dom: 'lBfrtip',
                    buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="la la-download"></i> Export to Excel',
                        title: 'ALC_AssessmentPointSetting_' + $('#assessGroup').val(),
                        className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }]
                });

                setHeight();
                setTimeout(function () { $(".loader").hide(); }, 500)
            }
        })
        //kim comment when this cell click it will automatic select all value 02/23/22
        $(document).on("focus", ".edit_score_value", function (e) {
            setTimeout(function () {
                document.execCommand('selectAll', false, null)
            }, 0);
        })

      
        $(document).on("keydown", ".edit_score_value", function (e) {            
                var txt = $(this).html();
                var rex = /(<([^>]+)>)/ig;
            var old_scorev = txt.replace(rex, "");
            //e.stopPropagation();               
            
            if (e.keyCode == 8) {
                return true;
            }           
            //kim only numeric are allowed to enter 02/23/22
            if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
            if (e.keyCode == 32 || $(this).text().length == 3) { //disable space button and 3 digit only
                e.preventDefault();
            }
            //kim comment here for press enter to save the data 02/23/22
            if (e.keyCode == 13) {
                e.preventDefault();             
                var txt = $(this).html();
                var rex = /(<([^>]+)>)/ig;
                var scorev = txt.replace(rex, "");
                if (scorev == "" || scorev == null ) {
                    swal("Please entere score value");
                    return
                }
                var aspid = $(this).attr('id');               

                if (!scorev || !aspid) return;
                var Data = {
                    func: "pointssettings", mode: 2, Data: { AssessmentPointSettingId: aspid, ScoreValue: scorev }
                };

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify(Data),
                    dataType: "json",
                    success: function (m) {
                        if (m.result == 0) {
                            toastr.success("Score updated successfully!", "", "success");
                            init();
                        }
                    }
                });
            }               
            
        })
        //add kim delete function 02/22/22
        $(document).on("click", ".ft_deactive", function () {
            var did = $(this);
            var asp = did.attr('id');
            var active = "deactive";
            if (!asp) return;           
            swal({
                title: "Are you sure you want to deactivate this Field Text?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, deactivate it!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify({
                            func: 'pointssettings',
                            mode: 4,
                            data: {
                                AssessmentPointSettingId: asp,
                                active: active
                            }
                        }),
                        dataType: "json",
                        success: function (m) {
                            if (m.result == 0) {                              
                                toastr.success("Field Text has been Inactive successfully!");
                                //refresh table after delete kim 11/25/2021
                                init();
                            } else
                                $.growlError({
                                    message: m.message,
                                    delay: 6000
                                });
                        }
                    });
            });
        });
    };
    $(document).on("click", ".ft_reactive", function () {
        var did = $(this);
        var asp = did.attr('id');
        var active = "reactive";
        if (!asp) return;
        swal({
            title: "Are you sure you want to Reactivate this Field Text?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, Reactivate it!"
        }).then(function (e) {
            e.value &&
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify({
                        func: 'pointssettings',
                        mode: 4,
                        data: {
                            AssessmentPointSettingId: asp,
                            active: active
                        }
                    }),
                    dataType: "json",
                    success: function (m) {
                        if (m.result == 0) {
                            toastr.success("Field Text has been Reactivated successfully!");
                            //refresh table after delete kim 11/25/2021
                            init();
                        } else
                            $.growlError({
                                message: m.message,
                                delay: 6000
                            });
                    }
                });
        });
    });
    init();


    $("#assessGroup").on("change", function () {
        init();
    })

    $(document).on("click", ".edit_score", function () {

        $("#AssessmentPointSettingId").val($(this).attr("id"));
        $("#FieldId").val($(this).attr("fid"));
        $("#FieldText").val($(this).attr("ft"));
        $("#ScoreValue").val($(this).attr("ss"));
        PointSettingState.orig = JSON.stringify($('#score_modal, .form').extractJson());
    })
   
    
  

    $("#update_score").click(function () {
        
        var row = $('#score_modal .form').extractJson();
        PointSettingState.current = JSON.stringify(row);

        if (PointSettingState.current == PointSettingState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }

        var data = { func: "pointssettings", mode: 2, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.result == 0) {
                    swal("Score updated successfully!", "", "success");
                    $("#score_modal").modal("hide");
                    init();
                }
            }
        });

    })

})