﻿$(document).ready(function () {
        var token = window.location.search.substring(1);

        if (token.length >= 1) {

            var converted_token = atob(token);
            var split1_token = converted_token.split('=');
            var agency = split1_token[0];
            var guestemail = split1_token[1];
            $('#agency').text(agency);
            $('#userid').text(guestemail);

            var data = { AgencyId: agency, Email: guestemail };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SendGuestActivation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    //$('#loader').hide();
                    //if (res.Result == -1) {
                    //    $('#btnSubmit').removeAttr('disabled').attr('class', 'primaryBtn');
                    //    $('#error').show().html(res.ErrorMsg);
                    //    return;
                    //}

                    //$('#enter').hide();
                    $("#loginlink").attr("href", "/ALS/Login?" + token);
                    $('#success').show();                  

                }
            });
        };
    });