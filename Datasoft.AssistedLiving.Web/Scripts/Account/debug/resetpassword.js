﻿$(document).ready(function () {

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $('#btnSubmit').click(function () {
        var email = $('#txtEmail').val();
        var agency = $('#txtAgencyID').val();

        if (email == '' || agency == '') {
            $('#error').show().html("Agency ID and email address are required fields.");
            return;
        }

        //validate email address.
        if (!validateEmail(email)) {
            $('#error').show().html("Invalid email address.");
            return;
        }

        $('#btnSubmit').attr('disabled', 'disabled').attr('class','primaryBtn-disabled');
        $('#loader').show();

        var data = { AgencyId: agency, Email: email };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "SendResetInstruction",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (res) {
                $('#loader').hide();
                if (res.Result == -1) {
                    $('#btnSubmit').removeAttr('disabled').attr('class', 'primaryBtn');
                    $('#error').show().html(res.ErrorMsg);
                    return;
                }
               
                $('#enter').hide();
                $('#success').show();
            }
        });
    });

    $('#btnSubmitNP').click(function () {
        var agency = $('#agencyid').val();
        var userid = $('#userid').val();
        var pass1 = $('#txtPassword').val();
        var pass2 = $('#txtCPassword').val();

        if (pass1 == '' || pass2 == '') {
            $('#errorNP').show().html("New password and confirm new password are required fields.");
            return;
        }

        //validate passwords
        if (pass1 != pass2) {
            $('#errorNP').show().html("New password and confirm new password does not match.");
            return;
        }

        $('#btnSubmitNP').attr('disabled', 'disabled').attr('class', 'primaryBtn-disabled');
        $('#loaderNP').show();

        var data = { Id: userid, Agency: agency, Password: pass1, ConfirmPassword: pass2};
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "../ChangePassword",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (res) {
                if (res.Result == -1) {
                    $('#btnSubmitNP').removeAttr('disabled').attr('class', 'primaryBtn');
                    $('#errorNP').show().html(res.ErrorMsg);
                    return;
                }

                $('#enterNP').hide();
                $('#successNP').show();
            }
        });
    });

    $('#txtEmail,#txtAgencyID').keypress(function () {
        $('#error').hide();
    });

    $('#txtCPassword,#txtPassword').keypress(function () {
        $('#errorNP').hide();
    });

});