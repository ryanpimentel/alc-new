﻿var setHeight = function () {
    if ($(window).width() < 1024) {
        $('#res_container').height($(window).height() / 2);
    } else {
        $('#res_container').height($(window).height() - 120);
    }

    
    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('#res_container').height($(window).height() / 2);
        } else {
            $('#res_container').height($(window).height() - 120);
        }
    })

    
    $('#rm_container2').height($(window).height() - 120);
    $(window).resize(function () {
        $('#rm_container2').height($(window).height() - 120);
    })

    $('.dataTables_scrollBody').height($(window).height() - 380);
    $(window).resize(function () {
        $('.dataTables_scrollBody').height($(window).height() - 380);
    })

    //$("#guestDiag .modal-body").height($(window).height() - 290);

    $("#guestDiag .modal-body").css("max-height", ($(window).height() - 400));
    $("#guestDiag .modal-body").css("overflow", "auto");
    $(window).resize(function () {
        $("#guestDiag .modal-body").css("max-height", ($(window).height() - 400));
        $("#guestDiag .modal-body").css("overflow", "auto");
    })
}

var getUIR_Data = function (uir_id, resident_id, m) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "GuestNotification/GetGridData?func=incident_report&param=" + uir_id + "&param2=" + resident_id + "&param3=" + m,
        dataType: "json",
        success: function (m) {
            if (m.length != "") {

                var id = m[0].resident_id;
                var resident = m[0].UIRResidentInvolved1;

                $("#resident").val(id);
                $("#resiName").text(resident);
                $('#resident option:selected').text(m[0].UIRResidentInvolved1);
                $("#generateUIR").val(m[0].resident_id);
                var string = "";
                var readStatus = "";
               
                    for (var i = 0; i <= m.length - 1; i++)
                    {
                        if (m[i].UIRDateViewed == null) {
                            readStatus = "unread";
                        } else {
                            readStatus = "read";
                        }

                        string += "<tr id='" + m[i].resident_id + "' read='" + readStatus + "'>";
                       
                        string += "<td class=\"res\" id=\"" + m[i].UIR_id + "." + m[0].resident_id + "\"><a href=#\"" + m[i].UIR_id + "\">" + m[i].UIRResidentInvolved1 + "</td>";
                        string += "<td>" + m[i].UIRDateOccured1 + "</td>";
                        
                        string += "<td>" + m[i].UIRIncidentDescription + "</td>";
                        string += "<td>" + m[i].UIRActionTaken + "</td>";
                        if (m[i].UIRDateViewed == null) {
                            string += "<td><span class=\"unread\">Not Viewed</span></td>";
                        } else {
                            string += "<td><span class=\"read\">Viewed</span></td>";
                        }
                        string += "</tr>";
                    }
                    $("#incident_report").html("");
                    $("#uir_tbl").DataTable().destroy();
                    $("#incident_report").html(string);
                   // $("#uir_tbl").DataTable();
                    $("#uir_tbl").DataTable({
                        stateSave: true,
                        "scrollY": "true",
                        "scrollX": true,
                        "scrollCollapse": true
                    });
                    $("#uir_tbl").DataTable().columns.adjust();
                    setHeight();
                
            } else {
                var str = "<tr><td></td><td></td><td>No data to show.</td><td></td><td></td></tr>";
                $("#incident_report").html(str);
            }
        }
    });
}

function getData(resident_id, g_id, completion_date, end_date) {
    end_date = end_date || completion_date;
    var string = "";
    var data = [];
    var status;
    var did;
    var completion = completion_date.split(" ");
    var end = end_date.split(" ");
    var date = new Date(completion_date);
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "GuestNotification/GetGridData?func=activity_schedule&param=" + completion[0] + "&param2=" + end[0] + "&param3=" + resident_id,
        dataType: "json",
        async: false,
        success: function (m) {
            var indexes = $.map(m, function (obj, index) {
                data.push(obj);
            })

            $("#resident_task").html("");

            if (data.length > 0) {

                var diff = Math.floor(new Date(end_date).getTime() - new Date(completion_date).getTime());
                var day = 1000 * 60 * 60 * 24;

                var days = Math.floor(diff / day);


                for (var i = 0; i <= days; i++) {
                    $.each(data, function (index, obj) {
                        

                        if (obj.activity_status == 0) {
                            status = "<span class=\"label label-warning\">Incomplete</span>";
                        } else {
                            status = "<span class=\"label label-success\">Completed</span>";
                        }

                        did = g_id + "." + obj.resident_id + "." + obj.carestaff_activity_id;

                        $("#resident_name").html(obj.resident);
                        $("#resident_room").html(obj.room);

                        string += "<tr id=\"" + obj.carestaff_activity_id + "\">";
                        string += "<td>" + monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + "</td>";
                        string += "<td>" + obj.activity_desc + "</td>";
                        string += "<td>" + obj.carestaff2 + "</td>";
                        string += "<td>" + obj.department + "</td>";
                        string += "<td>" + obj.start_time + "</td>";
                        string += "<td>" + obj.service_duration + "</td>";
                        string += "<td>" + status + "</td>";
                        string += "<td>" + obj.remarks + "</td>";
                        string += "<td>" + obj.acknowledged_by + "</td>";
                        string += "<td><button class=\"btn btn-default addComment\" did=\"" + did + "\">Add a Comment</button></td>";
                        string += "</tr>";
                    });

                    date.setDate(date.getDate() + 1);
                }

                $("#resident_task").append(string);



            } else {


                $("#resident_room").html("");

            }

        }
    });

  
}
//$(document).ready(function () {

var createDatatable = function () {

    $("#services_tbl").DataTable({
        stateSave: true,
        "scrollY": "true",
        "scrollX": true,
        "scrollCollapse": true,
        responsive: !0,
        pagingType: "full_numbers",
        dom: 'Bfrtip',
        "ordering": false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="la la-download"></i> Download as Excel',
                title: 'ALC_ResidentServices',
                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air msg_button',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
                
            }
        ]
    });
    

    $("#uir_tbl").DataTable({
        stateSave: true,
        "scrollY": "true",
        "scrollX": true,
        "scrollCollapse": true,
        responsive: !0,
        pagingType: "full_numbers",
        dom: 'Bfrtip',
        "ordering": false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="la la-download"></i> Download as Excel',
                title: 'ALC_IncidentReport',
                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air msg_button',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            }
        ]
    });   
}
 
//});




$(document).ready(function () {
    createDatatable();   
    setHeight();   

    var resident_id = "";
    $.ajax({
        url: 'GuestNotification/GetResidents',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        success: function (d) {
            var html_list = "";
            var counter = 1;
            if (d.resident_data.length > 0) {
                $("#res_id").val(d.resident_data[0].ResidentId);
                $.each(d.resident_data, function (i, data) {
                    var room = data.RoomName == null ? '' : data.RoomName;
                   
                    //  console.log(data);
                    html_list += '<div class="m-widget4__item parent">';
                    html_list += '<div class="m-widget4__img m-widget4__img--pic">';

                    html_list += '<img src="Resource/ResidentImage?id=' + data.ResidentId  + '&r=' + Date.now() + '" alt="" style="border-radius: 0px;">';
                    html_list += '</div>';
                    html_list += '<div class="m-widget4__info">';
                    html_list += '<span class="m-widget4__title">';
                    html_list += '<a href="#" name= "' + data.Name + '" counter=" ' + counter + '"class="resident_name" id="' + data.ResidentId + '">' + data.Name + '&nbsp;&nbsp;&nbsp;&nbsp;<i></i></a>';
                    html_list += '</span><br>';
                    html_list += '<span class="m-widget4__sub">';
                    html_list += 'Room: ' + room;
                    html_list += '</span>';


                    html_list += '</div>';
                    html_list += '</div>';
                    counter += 1;
                })
            }
            $("#resident_table").html(html_list);
            if (d.with_comment.length > 0) {
                $.each(d.with_comment, function (i, data) {
                    var rid = data.resident_id;

                    $('#resident_table').find(".resident_name[id='" + rid + "']").css("color", "red");
                    $('#resident_table').find(".resident_name[id='" + rid + "'] i").addClass("fa fa-envelope");
                });
            }

          
        }
    });


    setTimeout(function () {
        $("#resident_table").find(".parent .m-widget4__info .m-widget4__title .resident_name:eq(0)").click();
        setHeight();
        $(".loader").hide();
    }, 300);
    $("#TelNumber").forceNumericOnly().mask("(999)999-9999");


    $(document).on("click", "#seldaterange", function () {
        $("#daterange").modal("toggle");
    });

    var token = window.location.search.substring(1);
    var split1_token = token.split('.');
    var data;
    var isUIR = 0;

    if (split1_token.length > 1 && split1_token[1] != "") {
        isUIR = 1;
        var convert_token = atob(split1_token[1]);
        var split2_token = convert_token.split('=');
        var uirtoken = split2_token[1];
    } else {
        data = { token: token }
    }

    if (isUIR == 0) {
        if (token.length > 1) {
            $.ajax({
                url: 'GuestNotification/ParseToken',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: JSON.stringify(data),
                success: function (d) {
                    if (d.result == 1) {
                        getData(d.resident_id, d.gid, d.completion);

                        $("#uir").hide();

                        $("#resident").val(d.resident_id);
                    } else {
                        $("#message").html(d.message);
                    }
                }
            });
        }
    } else {
        $('#resident').attr("disabled", true);
        $("#guest").hide();
        $("#dateLabel").hide();
        $("#completion_date").hide();
        getUIR_Data(0, uirtoken, 0);
    };
    $("#completion_date, #completion_date_end").datepicker({
        defaultDate: new Date(),
        onSelect: function (dateText) {
            var date = this.value;
            var resident_id = $("#resident").val();

            if (!isNaN(resident_id)) {
                getData(resident_id, 0, date + " 12:00:00 AM");
            }

        }
    }).on('changeDate', function () {
                $(this).datepicker('hide');
            });



    $("#generateUIR").click(function () {

        var resident_id = $(this).val();
        getUIR_Data(0, resident_id, 0);
    });
    //$(".addComment").click(function () {

    //    handleComment($(this).attr("did"), $(this).attr("cid"), 0);
    //    $("#comment").html("");
    //    $("#guestDiag").modal("toggle");
    //})
    $(document).on("click", ".res", function () {
        var id = $(this).attr('id');
        var split1_token = id.split('.');
        var uir_id = split1_token[0];
        var resid = split1_token[1];
        var read = $(this).closest("tr").attr("read");

        data = { uir_id: uir_id }

        $.ajax({
            url: 'GuestNotification/AddDateViewed',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            success: function (d) {                
                window.open(ALC_URI.GuestNotification + "/ViewIncidentReport?id=" + uir_id, '_blank');
                //resid = $("#r_id").val();
                $("#generateUIR").click();
                getUIR_Data(0, resid, 0);

                if (read == "unread") {
                    swal("Time of receipt is being recorded", "", "success");
                }               
            }
        });
    });

   

    $(document).on("click", ".resident_name", function () {
        //setTimeout(function () {
          
            $("#residentForm").find("ul li a:eq(0)").click();
        //}, 100);
        var completion;
        var rid = $(this).attr("id");
        //resident_id = rid.split("_")[1];
        resident_id = rid;
        var completion_date = $("#completion_date").val();
        $("#res_name").html($(this).attr("name"));
     //   $("#current_resident_id").val(resident_id);
        $("#current_resident_id").val(rid);

        if (isNaN(resident_id)) {
            $("#resident_name").html("");
        } else {
            //$("#resident_name").html(this.options[this.selectedIndex].innerHTML);
            //$("#resiName").html(this.options[this.selectedIndex].innerHTML);
            $("#r_id").val(resident_id);
        }

      //  $("#incident_report").html("<tr><td colspan='2'></td><td colspan='2'>No data to show.</td><td colspan='2'></td></tr>");

        if (completion_date == "") {
            var date = new Date();
            completion = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
        } else {
            completion = completion_date + " 12:00:00 AM";
        }

        //getData(resident_id, 0, completion);

        var today = new Date();

        //$("#resident_task").html("");
        $("#services_tbl").DataTable().destroy();
        generateDailySchedule((today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear(), "All");
        $("#services_tbl").DataTable({
            stateSave: true,
            "scrollY": "true",
            "scrollX": true,
            "scrollCollapse": true,
            responsive: !0,
            pagingType: "full_numbers",
            dom: 'Bfrtip',
            "ordering": false,
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="la la-download"></i> Download as Excel',
                    title: 'ALC_ResidentServices',
                    className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air msg_button',
                    init: function (api, node, config) {
                        $(node).removeClass('dt-button')
                    }

                }
            ]
        });
        $("#services_tbl").DataTable().columns.adjust();
        // $("#incident_report").html("");
        $("#uir_tbl").DataTable().destroy();
        getUIR_Data(0, resident_id, 0); 
        getGuestCommentRead(resident_id);
     
        $("#generateUIR").val(resident_id);
        //$(this).addClass('active').siblings().removeClass('active');
        setHeight();
        setTimeout(function () {
            $('#services_tbl').DataTable().columns.adjust();
            $('#uir_tbl').DataTable().columns.adjust();
        }, 100)
    })
    

    $("#changePassword").click(function () {
        $("#changePasswordDiag").modal("toggle");
    });

    $("#changeContactInfo").click(function () {
        var id = $("#res_id").val();

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "GuestNotification/GetGridData?func=resident&param=" + id,
            dataType: "json",
            success: function (m) {
                $('#changeContactInfoDiag').mapJson(m);
               // showdiag();
            }
        });


    });
    $(document).on("click", "#save_contactInfo", function () {
        var data = $('#changeContactInfoDiag').extractJson('id');
        if ((data.TelNumber == "") && (data.Name == "") && (data.Address == "")) {
            swal("No change has been done!", "", "warning");
        } else {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "GuestNotification/ChangeContactInformation",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (m) {
                        
                        swal("Your Contact Information has been updated successfully!", "", "success");

                        $('.close').click();
                    }
                }
            });
        }
    })


    $(document).on("click", "#save_password", function () {
        var data = $('#changePasswordDiag').extractJson('id');

        if (data.Password == "") {
            swal("Invalid Password!", "", "warning");
        } else {

            if (data.Password == data.ConfirmPassword) {
                if (data.Password.length > 7) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "GuestNotification/ChangePassword",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (m) {
                            if (m) {

                                swal("Your password has been updated successfully!", "", "success");

                                $('.close').click();
                            }
                        }
                    });
                } else {
                    swal("Password must be at least 8 alphanumeric characters.", "", "warning");
                }
            } else {
                swal("Passwords don't match.", "", "error");
            }

        }
    })


    $(document).on("click","#download", function () {
        //doc.fromHTML($('#resident_task').html(), 15, 15, {
        //    'width': 170,
        //    'elementHandlers': specialElementHandlers
        //});
        var doc = new jsPDF('l', 'pt');

        var res_id = $("#current_resident_id").val();
        var name = $("#rn_" + res_id).find(".rname").text();


        var res = doc.autoTableHtmlToJson(document.getElementById("table_resident_task"));
        //doc.autoTable(res.columns, res.data, { margin: { top: 80 } });

        var header = function (data) {
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            //doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
            doc.text(name, data.settings.margin.left, 50);
        };

        var options = {
            beforePageContent: header,
            margin: {
                top: 80
            }
        };

        doc.autoTable(res.columns, res.data, options);

        doc.save("Services - " + name + ".pdf");
    })
    $(document).on("click", "#generatedataRange", function () {
        var resident_id = $("#current_resident_id").val();
        var completion, completion_end;
        var completion_date = $("#completion_date").val();
        var completion_date_end = $("#completion_date_end").val();

        if (completion_date == "") {
            var date = new Date();
            completion = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
        } else {
            completion = completion_date + " 12:00:00 AM";
        }

        if (completion_date_end == "") {
            var date = new Date();
            completion_end = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " 12:00:00 AM";
        } else {
            completion_end = completion_date_end + " 12:00:00 AM";
        }


        //getData(resident_id, 0, completion, completion_end);
        $(".loader").show();
        setTimeout(function () {      
            generateWeeklySchedule(completion, completion_end, "All");
            $(".close").click();
            $(".loader").hide();
            getGuestCommentRead(resident_id);
        }, 500);
        
        
       

    })


});