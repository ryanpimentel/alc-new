﻿    var settings = {
        startDate: null,
        endDate: null,
        appointments: []
    };
var cid, did = "";
    var global_html = [];
    var optsRes = [],
        optsCS = [''],
        optsDP = [''],
        optsStat = [''],
        optsTask = [''];
    var calftr = "";
    var startDateNew = "";
    var endDateNew = "";
    var filtercal = "";
    var currentId = "";
    var statt = "";
    var roleID = "";
    var responsible_person_email = "";
    var tasks = [];
    var current_cid = "";
   

    var adminRoles = ["1", "9", "10", "11", "12", "13"];

    function ampm(e) {

        var s = e.split(":");

        if (s[0] < 12) {
            return e + " AM";
        } else if (s[0] > 12) {
            s[0] -= 12;

            return s[0] + ":" + s[1] + " PM";
        } else {
            return e + " PM";
        }
    }


    function buildCal(date) {

        var facilityId = null,
            roomId = null;
        var fr = "";

        var data = {
            startDate: date.start,
            endDate: date.end
        };

        $.ajax({
            url: "Transsched/GetAdmissionCalendar",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            success: function (response) {
                if (response) {
                    Calendar.Build({
                        startDate: data.startDate,
                        endDate: data.endDate,
                        appointments: response.Appointments
                    });
                }
            }
        });

    }

    function generateTable(item, rowId, str_date2, class_icon, sched_time, count, resident_id, appointment_id) {
       // debugger
      //  alert(count);
        count = count || "00";
        var html2 = "";
        var ddate = str_date2.split("/");
        
        var dday = ddate[1];
        var carestff = "";
        var status = item.activity_status == 1 ? '<span class="m-badge  m-badge--success m-badge--wide">Completed</span>' : '<span class="m-badge m-badge--primary m-badge--wide">Not Completed</span>';

        if (item.carestaff_activity_id == null && item.activity_status == 0)
            status = '<span class="m-badge  m-badge--metal m-badge--wide">N/A</span';
           // carestff = -1;
        if (item.carestaff_activity_id == null && item.timelapsed) {
            status = '<span class="m-badge m-badge--danger m-badge--wide">Missed</span>';
            carestff = -1;
        }
        
        if (item.carestaff_activity_id != null){
            carestff = item.carestaff_activity_id ;
        }else{
            carestff = "0";
        };
        var did = carestff +"." + item.resident_id + "." + (item.carestaff_activity_id != null ? item.carestaff_activity_id : "0");

        //to determine comments in different days
        //date.count.resident_id
        //changed count to appointment_id(activity_schedule_id) to solve conflict on diff cids - alona
        var cid = ddate[0] + ddate[1] + ddate[2];
        //cid += "." + count + "." + item.resident_id;
        cid += "." + appointment_id + "." + item.resident_id;

        if (item.resident_id == resident_id) {

            html2 += "<tr>";
            html2 += "<td>" + str_date2 + "</td>";
            html2 += "<td>" + item.activity_desc + "</td>";
            html2 += "<td>" + item.carestaff + "</td>";
            html2 += "<td>" + item.department + "</td>";
            html2 += "<td>" + item.start_time + "</td>";
            html2 += "<td>" + item.service_duration + " Minutes</td>";
            html2 += "<td>" + status + "</td>";
            html2 += "<td>" + item.remarks + "</td>";
            html2 += "<td>" + item.acknowledged_by + "</td>";
            html2 += "<td><a href=\"#\" class=\"btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air addComment\"  data-target=\"#guestDiag\" data-toggle=\"modal\"  did=\"" + did + "\" cid=\"" + cid + "\"><span class='buttontext'> <span class='btext'>Add Comment</span>  &nbsp;<i></i></span></a></td>";
            html2 += "</tr>";
        }
        tasks[cid] = item;
        return html2;
        
    }

    var getCellDesc = function (d) {
     //   debugger
        global_html = [];
        var html = "";
        var string = "";
        var string2 = "";
        var recurrence, cl;
        var currentdate = new Date(d);
        var end;

        var byTime = settings.appointments.slice(0);

        byTime.sort(function (a, b) {
            return a.start_time - b.start_time;
        });

        $.each(byTime, function (i, item) {
            recurrence = item.recurrence;
            cl = "cl_" + recurrence;
            string = "<a appointment_type=\"transpo\" appointmentid=\"" + item.appointment_id + "\" id=\"" + item.resident_id + "\" href=\"#\" class=\"mcl " + cl + "\">" + item.start_time + " " + ampm(item.start_time) + " - " + item.end_time + " " + ampm(item.end_time) + " " + item.name + "</a><br>";

            if (item.end_date != null) {
                end = (d <= item.end_date);
            } else {
                end = true;
            };

            if (recurrence == "once") {
                //this should be "if equal" but "==" doesn't work. idk why

                if (!(d > item.start_date) && !(d < item.start_date)) {

                    html += string;
                    global_html.push(item);
                }

            } else if (recurrence == "weekly") {

                if (d >= item.start_date && d.getDay() == item.day_of_the_week && end) {

                    html += string;
                    global_html.push(item);
                }

            } else if (recurrence == "daily") {

                if (d >= item.start_date && end) {

                    html += string;
                    global_html.push(item);
                }

            } else if (recurrence == "biweekly") {

                var dw = item.day_of_the_week.split(",");



                if (((d >= item.start_date && d.getDay() == dw[0]) || (d >= item.start_date && d.getDay() == dw[1])) && end) {

                    html += string;
                    global_html.push(item);
                }

            } else if (recurrence == "monthly") {

                if (d >= item.start_date && d.getDate() == item.day_of_the_month && end) {

                    html += string;
                    global_html.push(item);
                }

            } else if (recurrence == "quarterly") {

                var qd = item.day_of_the_month.split(",");
                var date;

                $.each(qd, function (q, e) {

                    date = new Date(e);

                    if (d >= item.start_date && d.getMonth() == date.getMonth() && d.getDate() == date.getDate() && end) {

                        html += string;
                        global_html.push(item);
                    }

                });


            } else if (recurrence == "annually") {

                date = new Date(item.start_date);

                if (d >= item.start_date && d.getMonth() == date.getMonth() && d.getDate() == date.getDate() && end) {

                    html += string;
                    global_html.push(item);
                }
            }


        });

        return html;
    }

    function generateDailySchedule(startDay, filtercal) {
        //debugger
        tasks = [];
        $("#weekly").show();
        $("#calendar").hide();
        loadCurrentWeek = false;
        var startWeek = new Date(startDay);
        var tempDate = new Date(startWeek);
        var html = "<table>";
        var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
        var months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
        var byTime;
        var html2 = "";
        var class_icon = "";

        var container = new Array();
        var data, data2;

        var str_date;
        var stringdate;
        var schedule_task;
        var sched_desc = "";
        var sched_time = "";
        var str_date2;
        var rowId;

        var status = "";
        var na = "<span class=\"na\">N/A</span>";

        tempDate = new Date(startWeek);
        tempDate.setDate(startWeek.getDate());
        tempDate.setHours(0, 0, 0, 0);
        getCellDesc(tempDate);
        container = [];

        byTime = global_html.slice(0);

        $.each(byTime, function (i, item) {
            data = {
                'appointment_id': item.appointment_id,
                'activity_id': "",
                'carestaff': item.assigned_staff,
                'type': 'transpo',
                'resident_id': item.resident_id,
                'resident': item.name,
                'start': item.start_time,
                'end_time': item.end_time,
                'room': item.room,
                'activity_desc': "",
                'activity_status': "",
                'acknowledged_by': "",
                'carestaff_activity_id': "",
                'timelapsed': "",
                'department': item.department

            };

            if (data != "undefined") {
                container.push(data);
            }
            optsRes.push('<option>' + item.name + '</option>');
            optsTask.push('<option>Transportation Schedule</option>');
            optsDP.push('<option>Transportation</option>')
        })


        str_date2 = (tempDate.getMonth() + 1).toString() + "/" + tempDate.getDate().toString() + "/" + tempDate.getFullYear().toString();

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Staff/GetGridData?func=activity_schedule&param=" + str_date2,
            dataType: "json",
            async: false,
            success: function (m) {

                schedule_task = m;

            }
        });

        $.ajax({
            url: 'GuestNotification/GetUserData',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (d) {
                responsible_person_email = d.user_id;
            }
        });

        var current_resident_id = $("#current_resident_id").val();
        var tempDateNow = new Date(Date.now());
        $.each(schedule_task, function (i, item) {
           // if (item.responsible_person_email == responsible_person_email) {
               // if (current_resident_id != "") {            // if (current_resident_id == item.resident_id) {
          
            if (item.active_until != null) {
                nDate = new Date(item.active_until);
            }
            if ((item.is_active == true) || ((item.is_active == false) && (nDate > tempDateNow)))
            {
            //if (item.is_active == true){
                        data2 = {
                            'activity_id': item.activity_id,
                            'appointment_id': item.activity_schedule_id,
                            'type': 'dailytask',
                            'resident_id': item.resident_id,
                            'resident': item.resident,
                            'start': item.start,
                            'carestaff': item.carestaff_name,
                            'activity_desc': item.activity_desc,
                            'room': item.room,
                            'activity_status': item.activity_status,
                            'acknowledged_by': item.acknowledged_by,
                            'carestaff_activity_id': item.carestaff_activity_id,
                            'timelapsed': item.timelapsed,
                            'end_time': "",
                            'department': item.department,
                            'remarks': item.remarks,
                            'service_duration': item.service_duration,
                            'start_time' : item.start_time

                        };

                        if (data2 != "undefined") {
                            container.push(data2);
                        }
                        if (item.resident != null) {
                            optsRes.push('<option>' + item.resident + '</option>');
                            optsCS.push('<option>' + item.carestaff_name + '</option>');
                            optsDP.push('<option>' + item.department + '</option>');
                            optsTask.push('<option>' + item.activity_desc + '</option>');
                        } else {
                            optsRes.push('<option>--</option>');
                            optsCS.push('<option>' + item.carestaff_name + '</option>');
                            optsDP.push('<option>' + item.department + '</option>')
                            optsTask.push('<option>' + item.activity_desc + '</option>');
                        }
                    //}
              //  }
           }
        })

        container.sort(function (a, b) {
            var d = a.start.split(":");
            var e = d[0] + "" + d[1];

            var f = b.start.split(":");
            var g = f[0] + "" + f[1];
            return e - g;
        });
        str_date = (tempDate.getMonth() + 1).toString() + "_" + tempDate.getDate().toString() + "_" + tempDate.getFullYear().toString();

      //  stringdate = "<div class=\"dateblock\" id=\"" + str_date + "fcs\">";
      //  stringdate += "<span class=\"yearblock\">" + tempDate.getFullYear().toString() + "</span><br/>";
      //  stringdate += "<span class=\"monthblock\">" + days[tempDate.getDay()] + ", " + months[tempDate.getMonth()] + " " + tempDate.getDate().toString() + "</span>";

        //html += stringdate;
        var count = 1;
        var appointmentIdcom = 0;
        $.each(container, function (i, item) {
            appointmentIdcom = item.appointment_id;
            sched_time = ampm(item.start);
            rowId = item.type + "" + item.appointment_id + "" + item.resident_id;
           
            if (filtercal != "All") {
                if (filtercal == "--") {
                    filtercal = "Housekeeper";
                }

                if (filtercal == "Transportation") {
                    filtercal = "transpo";
                }

                statt = item.activity_status == 1 ? 'Completed' : 'Not Completed';
                if (item.carestaff_activity_id == null && item.timelapsed) {
                    statt = "Missed";
                } else if (item.type == "transpo") {
                    statt = "";
                } else if (item.carestaff_activity_id == null && item.activity_status == 0) {
                    statt = "Not Completed";
                }

                if (item.resident_id == null) {
                    item.type = "largetask";
                    class_icon = "largetask_icon";

                } else {

                    if (item.type == "dailytask") {
                        class_icon = "dailytask_icon";
                    } else {
                        sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                        item.activity_desc = "Transportation Schedule";
                        class_icon = "transpo_icon";
                    }
                }

                if (item.resident == filtercal || item.carestaff == filtercal || item.department == filtercal || item.type == filtercal || statt == filtercal || item.activity_desc == filtercal) {

                    html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count, current_resident_id, appointmentIdcom);
                }

            } else {
                if (item.resident_id == null) {
                    item.type = "largetask";
                    class_icon = "largetask_icon";

                } else {

                    if (item.type == "dailytask") {
                        class_icon = "dailytask_icon";
                    } else {
                        sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                        item.activity_desc = "Transportation Schedule";
                        class_icon = "transpo_icon";
                    }
                }


                html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count, current_resident_id, appointmentIdcom);

            }
            count++;
        });


        html += html2;
        html += "</table>";
        $("#resident_task").html("");
       // $("#services_tbl").DataTable().destroy();
        $("#resident_task").html(html);
    }

    function generateWeeklySchedule(startDay, endDay, filtercal, focus) {
       // debugger
        focus = focus || "";
        tasks = [];
        //console.log(startDay);
        var startWeek = new Date(startDay);
        var tempDate = new Date(startWeek);
        var html = "<table>";
        var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
        var months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
        var byTime;
        var html2 = "";
        var class_icon = "";

        var container = new Array();
        var data, data2;

        var endWeek = new Date(endDay);
        //endWeek.setDate(cdate.getDate() + (6 - current_day));

        var str_date;
        var stringdate;
        var schedule_task;
        var sched_desc = "";
        var sched_time = "";
        var str_date2;
        var rowId;



        var status = "";
        var na = "<span class=\"na\">N/A</span>";
        var diff = Math.floor(endWeek.getTime() - startWeek.getTime());
        var day = 1000 * 60 * 60 * 24;

        var days_difference = Math.floor(diff / day);

        $.ajax({
            url: 'GuestNotification/GetUserData',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            async: false,
            success: function (d) {
                responsible_person_email = d.user_id;
            }
        });

        var current_resident_id = $("#current_resident_id").val();

        for (var i = 0; i <= days_difference; i++) {

            tempDate = new Date(startWeek);
            tempDate.setDate(startWeek.getDate() + parseInt(i));
            tempDate.setHours(0, 0, 0, 0);
            getCellDesc(tempDate);
            container = [];

            byTime = global_html.slice(0);
            $.each(byTime, function (i, item) {
                data = {
                    'appointment_id': item.appointment_id,
                    'activity_id': "",
                    'carestaff': item.assigned_staff,
                    'type': 'transpo',
                    'resident_id': item.resident_id,
                    'resident': item.name,
                    'start': item.start_time,
                    'end_time': item.end_time,
                    'room': item.room,
                    'activity_desc': "",
                    'activity_status': "",
                    'acknowledged_by': "",
                    'carestaff_activity_id': "",
                    'timelapsed': "",
                    'department': item.department,
                    'remarks': item.remarks,
                    'service_duration': item.service_duration,
                    'start_time': item.start_time

                };

                if (data != "undefined") {
                    container.push(data);
                }
                optsRes.push('<option>' + item.name + '</option>');
                optsTask.push('<option>Transportation Schedule</option>');
                optsDP.push('<option>Transportation</option>')
            })

            str_date2 = (tempDate.getMonth() + 1).toString() + "/" + tempDate.getDate().toString() + "/" + tempDate.getFullYear().toString();

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Staff/GetGridData?func=activity_schedule&param=" + str_date2,
                dataType: "json",
                async: false,
                success: function (m) {
                    schedule_task = m;

                }
            });

            //var tempDateNow = new Date(Date.now());
            $.each(schedule_task, function (i, item) {
              //  if (item.responsible_person_email == responsible_person_email) {
                 //   if (current_resident_id != "") {
                //    if (current_resident_id == item.resident_id) {
                if (item.active_until != null) {
                    nDate = new Date(item.active_until);
                }
                if ((item.is_active == true) || ((item.is_active == false) && (nDate > tempDate)))
                {
               // if (item.is_active == true){
                            data2 = {
                                'activity_id': item.activity_id,
                                'appointment_id': item.activity_schedule_id,
                                'type': 'dailytask',
                                'resident_id': item.resident_id,
                                'resident': item.resident,
                                'start': item.start,
                                'carestaff': item.carestaff_name,
                                'activity_desc': item.activity_desc,
                                'room': item.room,
                                'activity_status': item.activity_status,
                                'acknowledged_by': item.acknowledged_by,
                                'carestaff_activity_id': item.carestaff_activity_id,
                                'timelapsed': item.timelapsed,
                                'end_time': "",
                                'department': item.department,
                                'remarks': item.remarks,
                                'service_duration': item.service_duration,
                                'start_time': item.start_time

                            };

                            if (data2 != "undefined") {
                                container.push(data2);
                            }
                            if (item.resident != null) {
                                optsRes.push('<option>' + item.resident + '</option>');
                                optsCS.push('<option>' + item.carestaff_name + '</option>');
                                optsDP.push('<option>' + item.department + '</option>');
                                optsTask.push('<option>' + item.activity_desc + '</option>');
                            } else {
                                optsRes.push('<option>--</option>');
                                optsCS.push('<option>' + item.carestaff_name + '</option>');
                                optsDP.push('<option>' + item.department + '</option>')
                                optsTask.push('<option>' + item.activity_desc + '</option>');
                            }
                     //   }
                   // }
               }
            })


            container.sort(function (a, b) {
                var d = a.start.split(":");
                var e = d[0] + "" + d[1];

                var f = b.start.split(":");
                var g = f[0] + "" + f[1];
                return e - g;
            });
            str_date = (tempDate.getMonth() + 1).toString() + "_" + tempDate.getDate().toString() + "_" + tempDate.getFullYear().toString();

            stringdate = "<div class=\"dateblock\" id=\"" + str_date + "fcs\">";
            stringdate += "<span class=\"yearblock\">" + tempDate.getFullYear().toString() + "</span><br/>";
            stringdate += "<span class=\"monthblock\">" + days[tempDate.getDay()] + ", " + months[tempDate.getMonth()] + " " + tempDate.getDate().toString() + "</span>";

            //html += stringdate;
            html2 = "";
            var count = 1;
            var appointmentIdcom = 0;
          
            $.each(container, function (i, item) {
                sched_time = ampm(item.start);
                appointmentIdcom = item.appointment_id;
            
                rowId = item.type + "" + item.appointment_id + "" + item.resident_id;
                if (filtercal != "All") {
                    if (filtercal == "--") {
                        filtercal = "Housekeeper";
                    }

                    if (filtercal == "Transportation") {
                        filtercal = "transpo";
                    }

                    statt = item.activity_status == 1 ? 'Completed' : 'Not Completed';
                    if (item.carestaff_activity_id == null && item.timelapsed) {
                        statt = "Missed";
                    } else if (item.type == "transpo") {
                        statt = "";
                    } else if (item.carestaff_activity_id == null && item.activity_status == 0) {
                        statt = "Not Completed";
                    }

                    if (item.resident_id == null) {
                        item.type = "largetask";
                        class_icon = "largetask_icon";

                    } else {

                        if (item.type == "dailytask") {
                            class_icon = "dailytask_icon";
                        } else {
                            sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                            item.activity_desc = "Transportation Schedule";
                            class_icon = "transpo_icon";
                        }
                    }

                    if (item.resident == filtercal || item.carestaff == filtercal || item.department == filtercal || item.type == filtercal || statt == filtercal || item.activity_desc == filtercal) {

                        html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count, current_resident_id, appointmentIdcom);
                    }

                } else {
                    if (item.resident_id == null) {
                        item.type = "largetask";
                        class_icon = "largetask_icon";

                    } else {

                        if (item.type == "dailytask") {
                            class_icon = "dailytask_icon";
                        } else {
                            sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                            item.activity_desc = "Transportation Schedule";
                            class_icon = "transpo_icon";
                        }
                    }


                    html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count, current_resident_id, appointmentIdcom);

                }

                count++;
            });
            
            html += html2;
         
           // html += "<br><br>";
        }

        html += "</table>";

        $("#resident_task").html(html);


        //setTimeout(function () {
        //    $("#btnReload").click();

        //}, 500);

        if (focus) {
            var container = $("#appointments"),
                scrollTo = $("#" + focus + "fcs");

            container.animate({
                scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
            });
        }


    }

    var Calendar = (function () {
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var cellClasses = {
            1: "typeCont",
            2: "typeGen",
            3: "typeInp",
            4: "typeRout"
        };
        var icons = {
            "551": "snp.png",
            "421": "ptp.png",
            "431": "otp.png",
            "441": "stp.png",
            "561": "swp.png",
            "571": "hhap.png"
        };

        var selectedDate = "";

        var init = function (options) {

            if (options.startDate) settings.startDate = new Date(options.startDate);
            if (options.endDate) settings.endDate = new Date(options.endDate);
            if (options.appointments) {

                settings.appointments = [];
                $.each(options.appointments, function (i, item) {

                    var end = null;

                    if (item.end_date != null) {
                        end = new Date(item.end_date);
                    }

                    var loc = {
                        //admission_id: item.admission_id, resident_id: item.resident_id, name: item.name, admission_date: new Date(item.admission_date)
                        appointment_id: item.transpo_id,
                        resident_id: item.resident_id,
                        name: item.name,
                        start_date: new Date(item.date),
                        start_time: item.start_time,
                        end_time: item.end_time,
                        recurrence: item.recurrence,
                        day_of_the_week: item.day_of_the_week,
                        day_of_the_month: item.day_of_the_month,
                        end_date: end,
                        assigned_staff: item.staff_id,
                        room: item.room_name,
                        isAdmitted: item.isAdmitted
                    };
                    settings.appointments.push(loc);
                });
            }

        }

        var limit = function (val, max, step) {
            return val < max ? val - step : max;
        };

        var renderHeaderHtml = function () {
            var html = ''; //"<td>Week</td>";
            for (var i = 0; i < days.length; i++) {
                html += "<td>" + days[i] + "</td>";
            }

            return "<thead><tr>" + html + "</tr><thead>";
        };


        //show schedule


        var renderCellHtml = function (w, d) {
            var html = "<td><table";
            if (d != null) html += " id=\"" + (d.getMonth() + 1) + "_" + d.getDate() + "_" + d.getFullYear() + "\"";
            html += " class=\"planBlock";
            if (d != null) html += " day";
            html += "\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td class=\"date\">";
            //if (w != null) html += w;
            if (d != null) html += months[d.getMonth()] + " " + d.getDate();
            html += "</td></tr><tr><td class=\"planned\"><div>";
            if (d != null) html += getCellDesc(d);
            if (w != null) html += "";
            html += "</div></table></td>";

            return html;
        }

        var renderBodyHtml = function () {
            var html = "<tr>";
            for (var i = 0; i < settings.startDate.getDay() ; i++) {
                //if (i == 0) html += renderCellHtml(1);
                html += renderCellHtml();
            }

            var day = settings.startDate;
            var week = 1;
            for (; day <= settings.endDate;) {
                //if (day.getDay() == 0) html += renderCellHtml(week);
                html += renderCellHtml(null, day);
                if (day.getDay() == 6) {
                    html += "</tr><tr>";
                    week += 1;
                }
                day.setDate(day.getDate() + 1);
            }

            for (var i = settings.endDate.getDay() ; i < 6; i++) {
                html += renderCellHtml();
            }
            html += "</tr>"

            return "<tbody>" + html + "</tbody>";
        }

        var renderTableHtml = function () {
            var html = "<table class=\"clearFloats03\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
            html += renderHeaderHtml();
            html += renderBodyHtml();
            html += "</table>";

            return html;
        }
        var selectedTabIndex = 0;

        var showdiag = function (en, title) {

            $("#diag_transpo").dialog({
                zIndex: 0,
                autoOpen: false,
                modal: true,
                title: "Daily Schedule (" + title + ")",
                dialogClass: "companiesDialog",
                open: function () { },
                close: function () {

                    $("#diag_view").html("");
                }
            });

            $("#diag_transpo").dialog("option", {
                width: $(window).width() * 0.3,
                height: $(window).height() - 350,
                position: "center",
                buttons: {
                    "Save": {
                        click: function () {
                            updateSchedule();
                            var month = $('#monthfilter').val();
                            var year = $('#yrfilter').val();
                            minDate = getMonthDateRange(year, month);
                            buildCal(minDate);
                            setTimeout(function () {
                                $("#calendar").show();
                                $("#diag_transpo").dialog("close");
                            }, 500);
                        },
                        class: "primaryBtn",
                        text: "Save"
                    },
                    "Cancel": function () {
                        $("#diag_transpo").dialog("close");
                    }
                },
                resize: function (event, ui) {

                }
            }).dialog("open");
        }

        return {
            Build: function (options) {
                init(options);
                if (settings.startDate == null && settings.endDate == null) return;

                var str = "<div class=\"toolbar2\"><ul><li><b>Legend</b>:</li>";
                str += "<li><div class=\"foo cl_once\"></div>Once</li>";
                str += "<li><div class=\"foo cl_daily\"></div>Daily</li>";
                str += "<li><div class=\"foo cl_weekly\"></div>Weekly</li>";
                str += "<li><div class=\"foo cl_biweekly\"></div>Bi-Weekly</li>";
                str += "<li><div class=\"foo cl_monthly\"></div> Monthly</li>";
                str += "<li><div class=\"foo cl_quarterly\"></div>Quarterly</li>";
                str += "<li><div class=\"foo cl_annually\"></div>Annually</li></ul></div>";
                str += renderTableHtml()
                $("#calendar").html(str);
                $("#calendar").hide();
                $("#calendar").on("click", ".planned, .actual", function (e) {
                    var $a = $(this);
                    var $d = $a.closest(".day");
                    if ($d[0]) {
                        selectedDate = $d.attr("id");
                        if ($a.is(".planned")) {
                            $("#calendarMenu1").css({
                                top: e.pageY,
                                left: e.pageX
                            }).slideDown(300);
                            $("#calendarMenu2").hide();
                        } else if ($a.is(".actual")) {
                            $("#calendarMenu2").css({
                                top: e.pageY,
                                left: e.pageX
                            }).slideDown(300);
                            $("#calendarMenu1").hide();
                        }
                    }
                });
                $(document).on("click", ".mcl, .wcl, .trcl", function (e) {

                    var classes = $(this).attr("class");
                    var c = classes.split(" ");
                    var id = $(this).attr("id");
                    var resident_id = $(this).attr('resident_id');
                    var appointmentid = $(this).attr('appointmentid');
                    var string = "";
                    var completion_status = "";
                    var remarks = "";
                    var date = $(this).attr('date');
                    var obj;
                    var status;
                    var titleDate;
                    var guardian_comment = "";
                    $(".is_complete").removeAttr("disabled");

                    if (c == "trcl") {
                        $(".trcl").children("td").css("background-color", "");
                        $(this).children("td").css("background-color", "#E3F2FD");

                        currentId = $(this).attr("id");

                    }
                    if (c[0] == "mcl") {
                        var d = $(this).parents("table").attr("id").split("_");
                        var start1 = new Date(d[0] + "/" + d[1] + "/" + d[2]);
                        var temp = start1.toString().split(" ");
                        titleDate = temp[1] + " " + temp[2] + ", " + temp[3];
                    }

                    $("#r_comments").html("");

                    $(".dwv").hide();
                    $(".arrow-right").hide();

                    $(this).find(".arrow-right").show();

                    $("#weekly_task").html("");

                    if ($(this).attr("appointment_type") == "transpo") {
                        var table_id = $(this).closest('table').attr('id');
                        var date = table_id.replace(/_/g, "/");

                        string = "<input class=\"r_currentdate\" type=\"hidden\"/><input class=\"r_resident_id\" type=\"hidden\" /><input class=\"r_resident_name\" type=\"hidden\" /><input class=\"r_transpo_id\" type=\"hidden\" />";
                        string += "<input class=\"r_recurrence\" type=\"hidden\" /><input class=\"r_day_of_the_week\" type=\"hidden\" />";
                        string += "<input class=\"r_day_of_the_month\" type=\"hidden\" /><input class=\"r_date\" type=\"hidden\" />";
                        string += "<input class=\"r_start_time\" type=\"hidden\" /><input class=\"r_end_time\" type=\"hidden\" />";
                        string += "<input class=\"r_purpose\" type=\"hidden\" /><input class=\"r_staff_id\" type=\"hidden\" />";
                        string += "<div class=\"panel panel-default\">";
                        string += "<div class=\"panel-heading\" style=\"background-color: #6ea8cc\"><span class=\"resName\" id=\"r_name\"></span><p class=\"stat dateshow\">Date: <b>" + date + "</p></div>";

                        //panel body
                        string += "<div class=\"panel-body\">";
                        string += "<table class=\"scheduleTable\">";

                        //Only Admin and Dept Heads should be able to add/edit/delete 
                        if ($.inArray(roleID, adminRoles) !== -1) {
                            string += "<tr><td><button class=\"is_complete btn btn-primary reassignTask\" style=\"background-color: #0091EA\" type=\"submit\" onclick=\"editSchedule()\">Edit</button>";
                            string += "<button class=\"is_complete btn btn-primary reassignTask\" style=\"background-color: #0091EA\" type=\"submit\" onclick=\"deleteSchedule()\">Delete</button></td></tr>";
                        }
                        string += "<tr><td class=\"schedTitle\" ><h4><small>Recurrence: </small><span class=\"resValue\" id=\"r_recurrence\"></span></h4></td>";
                        string += "<td class=\"schedTitle\"><h4><small>Start Date: </small><span class=\"resValue\"  id=\"r_date\"></span></h4></td>";
                        string += "<td class=\"schedTitle\"><h4><small>End Date: </small><span class=\"resValue\" id=\"r_end_date\"></span>";
                        string += "</tr><tr>";
                        string += "<td class=\"schedTitle\"><h4><small>Start Time: </small><span  class=\"resValue\" id=\"r_start_time\"></span></h4></td>";
                        string += "<td class=\"schedTitle\"><h4><small>End Time: </small><span  class=\"resValue\" id=\"r_end_time\"></span></h4></td>";
                        //string += "<td><h4><small>Assigned Staff: </small><span  class=\"resValue\" id=\"r_staff_id\"></span></h4></td>";
                        string += "<td><table id=\"datesched\" class=\"scheduleTable\" style=\"width:100%;\"></table></td>";
                        string += "</tr></table>";

                        string += "<table class=\"scheduleTable\"><tr><td class=\"schedTitle\"><h4><small>Purpose: </small><span  class=\"resValue\" id=\"r_purpose\"></span></h4></td></tr></table>";

                        string += "<table class=\"scheduleTable\" style=\"width:50%;\">";
                        string += "<tr><td class=\"schedTitle\" ><small>Comments / Remarks</small></td></tr>";
                        string += "<tr><td class=\"schedTitle\" ><span id=\"r_comments\"></span></td></tr></table>";
                        string += "<button class=\"button\" id=\"create_new_schedule\" value=\"Create New Schedule\" onClick=\"create_new_schedule()\">Create New Schedule</button>";

                        //outer panel end

                        if (c[0] == "mcl") {

                            $("#weekly_task").html("");
                            string += "</div></div></div>";
                            $("#diag_view").html(string);

                        } else {

                            //string += "<button class=\"button\"  type=\"submit\" onClick=\"updateSchedule()\">Save</button>";
                            string += "</div></div>";
                            $("#diag_view").html("");
                            $("#weekly_task").html(string);
                            date = new Date(date);

                            $(this).parent().prev('div').show();
                        }

                        $(".r_currentdate").val(date);

                        $(".date").datepicker({
                            'formatDate': 'yyyy-mm-dd',
                            'autoclose': true
                        });

                        $(".time").timepicker();

                        var data = {
                            transpo_id: appointmentid,
                            date: date
                        };
                        var dw;

                        $.ajax({
                            url: "Transsched/GetScheduleData",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            type: "POST",
                            data: JSON.stringify(data),
                            success: function (response) {

                                var obj = response["data"][0];
                                var isOnce = obj.recurrence;
                                var comments = response["comments"][0];

                                if (comments != undefined) {

                                    var d1 = new Date(date);
                                    var d2 = new Date(comments.current_date);

                                    //i dunno how to equal T_T
                                    if (!(d1 > d2) && !(d1 < d2)) {

                                        $("#r_comments").html(comments.comment);

                                    }

                                }

                                var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

                                for (var key in obj) {
                                    var value = obj[key];

                                    if (obj.hasOwnProperty(key)) {

                                        if (key == "recurrence" || key == "day_of_the_week") {
                                            if (value == "biweekly") {
                                                dw = obj["day_of_the_week"].split(",");

                                                $("#r_day_of_the_week").html(days[dw[0]] + ", " + days[dw[1]]);

                                                $("#datesched").html("<tr><td class=\"schedTitle\"><h4><small>Day of the Week: </small><span class=\"dwv v_weekly v_biweekly resValue\"></span> <span  class=\"resValue\" id=\"r_day_of_the_week\">" + days[dw[0]] + ", " + days[dw[1]] + "</span></h4></td></tr>");

                                            } else if (value == "monthly") {

                                                $("#datesched").html("<tr><td class=\"schedTitle\"><h4><small>Day of the Month: </small><span class=\"dwv v_monthly resValue\" id=\"r_day_of_the_month\">" + obj["day_of_the_week"] + "</span></h4></td>");


                                            } else if (value == "quarterly") {

                                                $("#datesched").html("<tr><td class=\"schedTitle\"><h4><small>Date: <span class=\"dwv v_quarterly resValue\" id=\"r_quarterly_dates\">" + obj["day_of_the_month"] + "</span></small></h4></td></tr>");


                                            } else if (value == "annually" || value == "daily") {

                                                $("#datesched").html("");


                                            } else {
                                                $("#r_day_of_the_week").html(days[value]);
                                                $("#datesched").html("<tr><td class=\"schedTitle\"><h4><small>Day of the Week: </small><span class=\"dwv v_weekly v_biweekly resValue\"></span> <span  class=\"resValue\" id=\"r_day_of_the_week\">" + days[obj["day_of_the_week"]] + "</span></h4></td></tr>");
                                            }

                                            if (value == "once") {
                                                isOnce = true;
                                                $("#r_end_date").hide();
                                                $("#datesched").html("");
                                                //$("#rnd").hide();

                                            } else {

                                                $("#r_end_date").show();
                                                $("#rnd").show();

                                            }

                                            $(".v_" + value).show();

                                            $("#r_recurrence").html(obj["recurrence"]);

                                        } else if (key == "end_date") {
                                            if (value != null) {

                                                $("#r_end_date").html(value);
                                                //$("#r_end_date").removeClass("hasDatepicker");
                                                //$("#r_end_date").removeClass("date");
                                                //$("#r_end_date").attr("disabled", "disabled");
                                                $("#create_new_schedule").removeAttr("disabled");
                                                $("#enddate_notif").html("");

                                            } else {

                                                $("#r_end_date").html("");
                                                if (isOnce != "once") {
                                                    $("#create_new_schedule").attr("disabled", "disabled");
                                                }
                                                $("#enddate_notif").html("Please click 'Save' button to update end date.");
                                            }

                                            if (isOnce == "once") {

                                                $("#enddate_notif").html("N/A");
                                            }

                                        } else if (key == "name") {

                                            $("#r_resident_name").val(value);
                                            $("#r_name").html(value);

                                        } else {


                                            $("#r_" + key).html(value);

                                        }

                                        $(".r_" + key).val(value);

                                    }
                                }
                            }
                        });

                        if (c[0] == "mcl") {
                            showdiag(0, titleDate);
                        }

                    } else {
                        if ($(this).attr("appointment_type") == "largetask") {
                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "Staff/GetGridData?func=activity_schedule&param=" + date,
                                dataType: "json",
                                async: false,
                                success: function (m) {

                                    var indexes = $.map(m, function (obj, index) {
                                        if (obj.activity_schedule_id == appointmentid) {
                                            return index;
                                        }
                                    })

                                    obj = m[indexes[0]];

                                }
                            });
                        } else {

                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "Staff/GetGridData?func=activity_schedule&param=" + date + "&param3=" + resident_id,
                                dataType: "json",
                                async: false,
                                success: function (m) {
                                    var indexes = $.map(m, function (obj, index) {
                                        if (obj.activity_schedule_id == appointmentid) {
                                            return index;
                                        }
                                    })

                                    obj = m[indexes[0]];

                                }
                            });

                            var data = {
                                carestaff_activity_id: $(this).attr("csid"),
                                mode: 0

                            }
                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "GuestNotification/HandleComment",
                                dataType: "json",
                                data: JSON.stringify(data),
                                async: false,
                                success: function (g) {
                                    var comment = "";
                                    var timestamp = "";
                                    var date = new Date();
                                 
                                    if (g != null) {

                                        var gobj = JSON.parse("[" + g.comment + "]");
                                        for (var i = 0; i < gobj.length; i++) {

                                            timestamp = new Date(gobj[i].datetime);
                                            comment += "[" + timestamp.getHours() + ":" + timestamp.getMinutes() + "]  " + gobj[i].comment + "<br/>";

                                        }

                                        guardian_comment += (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "<br><br>" + comment;
                                    }

                                }
                            });

                        }

                        status = obj.activity_status == 1 ? '<span class="completed">Completed</span>' : '<span class="comp-no">Not Completed</span>';
                        var cmplt = obj.activity_status == 1 ? 'Yes' : 'No';

                        remarks = "<b><span>" + obj.remarks + "</span></b>";

                        if (obj.carestaff_activity_id == null && obj.activity_status == 0)
                            status = "<span class=\"na\">N/A</span>";
                        if (obj.carestaff_activity_id == null && obj.timelapsed) {
                            status = '<span class="missed">Missed</span>';
                            cmplt = "Missed";
                            remarks = "<textarea class=\"is_complete\" id=\"scheduleRemarks\" cols=\"40\" name=\"remarks\" rows=\"30\"></textarea>";
                        }

                        completion_status += "<tr><td colspan=\"1\"><label class=\"checkbox\"><input class=\"is_complete chck\" type=\"checkbox\" id=\"taskYes\" value=\"False\" name=\"No\">YES</label></td>";
                        completion_status += "<td  colspan=\"1\"><label class=\"checkbox\"><input class=\"is_complete chck\" type=\"checkbox\" id=\"taskNo\" value=\"True\" name=\"No\">NO</label></td>";

                        string += "<div class=\"panel panel-primary\">";
                        string += "<div class=\"panel-heading\" style=\"background-color: #6ea8cc\"><span class=\"resName\">" + (obj.resident == null ? obj.department : obj.resident) + "</span><p class=\"stat\">Date: <b>" + date + " | </b> Status: " + status + "<br/>Acknowledged By: <b>" + (obj.acknowledged_by != "" ? obj.acknowledged_by : "N/A") + "</b></p><br/><span id=\"admissionstatus\" style=\"color: #ab0404; font-weight: bold;\"></span></div>";


                        string += "<div class=\"panel-body\">";
                        if ($.inArray(roleID, adminRoles) !== -1) {
                            string += "<button class=\"reassched is_complete btn btn-primary reassignTask\" style=\"background-color: #0091EA\" type=\"submit\" activityid=\"" + obj.activity_schedule_id + "\" onclick=\"resched(" + obj.activity_schedule_id + ", '" + $(this).attr("date") + "', 2)\">Reassign</button>";
                            string += "<button class=\"reassched is_complete btn btn-primary reassignTask\" style=\"background-color: #0091EA\" type=\"submit\" onClick=\"resched(" + obj.activity_schedule_id + ", '" + $(this).attr("date") + "', 1)\" >Reschedule</button>";
                        }
                        string += "<table class=\"scheduleTable\"><tr><td width=\"50%\" valign=\"top\"><table>";
                        string += "<tr><td class=\"schedTitle\"><h4><small>Room: </small>" + (obj.room != null ? obj.room : "N/A") + "</h4></td></tr>";
                        string += "<tr><td class=\"schedTitle\"><h4><small>Carestaff: </small>" + (obj.carestaff_name == null ? "N/A" : obj.carestaff_name) + "</h4></td></tr>";
                        string += "<tr><td class=\"schedTitle\"><h4><small>Department: </small>" + (obj.department == null ? "N/A" : obj.department) + "</h4></td></tr>";
                        string += "<tr><td class=\"schedTitle\"><h4><small>Duration (Minutes:) </small>" + obj.service_duration + "</h4></td></tr>";
                        string += "</table></td><td width=\"50%\" valign=\"top\">";

                        string += "<table><tr><td class=\"schedTitle\"><h4><small>Task: </small>" + (obj.activity_desc == null ? "N/A" : obj.activity_desc) + "</h4></td></tr>";
                        string += "<tr><td class=\"schedTitle\"><h4><small>Procedure: </small>" + (obj.activity_proc == null ? "N/A" : obj.activity_proc) + "</h4></td></tr>";
                        string += "<tr><td class=\"schedTitle\"><h4><small>Schedule: </small>" + obj.start_time + "</h4></td></tr>";
                        string += "</table>";

                        string += "</td></tr></table>";

                        //inner panel start
                        string += "<div class=\"panel panel-default\"><div class=\"panel-body\">";
                        string += "<div class=\"row\"><div class=\"col-lg-6\"><div class=\"input-group\">";
                        string += "<span class=\"input-group-addon tskbtn\">Is this task completed? YES <input class=\"is_complete chck\" type=\"checkbox\" id=\"taskYes\" value=\"False\" name=\"No\"></span>";
                        string += "<span class=\"input-group-addon tskbtn\">NO <input class=\"is_complete chck\" type=\"checkbox\" id=\"taskNo\" value=\"True\" name=\"No\"></span></div></div>";

                        //inner panel 2
                        string += "<div class=\"panel-body\">";
                        string += "<br/><br/><span><b>Remarks:</b></span> <span class=\"empty_remarks\" style=\"display:none;\">Remark is required</span><br/><br/>";

                        var cdate = new Date(obj.reschedule_dt);
                        var dday = cdate.getDate();

                        string += (obj.remarks == null ? (obj.reschedule_dt != null ? "Rescheduled - <a href=\"#\" xref=\"arwdailytask" + obj.xref + "" + obj.resident_id + "" + dday + "\" class=\"resched\">" + obj.reschedule_dt + "</a>" : "") : remarks);
                        string += "<br><br><b>Guest Comment/s:</b><br><br>" + guardian_comment;
                        string += "<input type=\"hidden\" id=\"CarestaffActivityId\" name=\"CarestaffActivityId\" value=\"" + obj.carestaff_activity_id + "\">";
                        string += "<input type=\"hidden\" id=\"ActivityScheduleId\"name=\"ActivityScheduleId\" value=\"" + obj.activity_schedule_id + "\">";
                        string += "<input type=\"hidden\" id=\"Resident\"name=\"Resident\" value=\"" + obj.resident + "\">";
                        string += "<input type=\"hidden\" id=\"CarestaffName\"name=\"CarestaffName\" value=\"" + obj.carestaff_name + "\">";
                        string += "<input type=\"hidden\" id=\"ActivityDesc\"name=\"ActivityDesc\" value=\"" + obj.activity_desc + "\">";
                        string += "<input type=\"hidden\" id=\"completion\"name=\"completion\" value=\"" + date + "\">";
                        string += "<input type=\"hidden\" id=\"id\"name=\"id\" value=\"" + id + "\">";
                        string += "<br /><br /><button class=\"is_complete btn btn-primary\" style=\"background-color: #0091EA\" id=\"updateTask\" type=\"submit\" onClick=\"updateTask();\" disabled>Save</button>";

                        //inner panel 2 end
                        string += "</div>";

                        //inner panel
                        string += "</div></div>";

                        //outer panel
                        string += "</div>";
                        $("#weekly_task").html(string);
                        $("#scheduleRemarks").html(obj.remarks);

                        var dt = new Date(date);
                        var dtnow = new Date(Date.now());
                        dtnow.setHours(0, 0, 0, 0);

                        if (cmplt != "Missed" || +dt != +dtnow) {
                            $(".reassched").attr("disabled", "disabled");
                        }
                        if (cmplt != "Missed") {
                            $(".is_complete").attr("disabled", "disabled");
                        }

                        if (obj.isAdmitted == false) {
                            $(".reassched").attr("disabled", "disabled");
                            $(".is_complete").attr("disabled", "disabled");
                            $("#admissionstatus").html("DISCHARGED");
                        }

                        $("#task" + cmplt).prop("checked", true);

                    }
                    $('.chck').on('change', function () {

                        $('.chck').not(this).prop('checked', false);

                        var len = $(".chck:checked").length;

                        if (len > 0) {
                            $("#updateTask").attr("disabled", false);
                        } else {
                            $("#updateTask").attr("disabled", true);
                        }


                    });
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                });
                //  createPopupMenus();
            }

        }
    })();

    var today = new Date();

    generateDailySchedule((today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear(), "All");

    function handleComment(d, c, mode, item) {
        //guest_notification_id, resident_id, carestaff_activity_id
       //debugger
        item = item || null;
        var dataId = d.split(".");
        var comment = {
            datetime: new Date(),
            comment: $("#comment").val()
        };

        
        //$("#guestDiag").dialog({
        //    zIndex: 0,
        //    autoOpen: false,
        //    modal: true,
        //    title: "Add Comment",
        //    dialogClass: "companiesDialog",
        //    width: $(window).width() * 0.25,
        //    height: $(window).height() * 0.4,
        //    position: "center",
        //    buttons: {
        //        "Save": {
        //            click: function () {
        //                debugger;
       
                        //handleComment(d, c, 1);

                        //$("#comment").val('');
                        //$.growl({
                        //    message: "Your comment has been added successfully!",
                        //    delay: 6000
                        //});

                        //// Start the connection.
                        //$.connection.hub.start().done(function () {
                        //    chat.server.send("#message", comment, "");
                        //});


        //            },
        //            class: "primaryBtn",
        //            text: "Save"
        //        },
        //        "Cancel": function () {
        //            $("#guestDiag").dialog("close");
        //        }
        //    },
        //    close: function () {
        //        $("#comment").val("");
        //        $("#well_comment").html("");
        //    },
        //    resize: function (event, ui) { },
        //    open: function () { }
        //});

        var comment_email = "<table border=\"1\" style=\"text-align: left;\"><thead><tr><th>Task Date</th><th>Task</th><th>Carestaff</th><th>Department</th><th>Schedule</th><th>Duration (Minutes)</th>";
        comment_email += "<th>Status</th><th>Remarks</th><th>Acknowledged By</th><th>Comment</th></tr></thead><tbody><tr>";

        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(0)").text() + "</td>";
        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(1)").text() + "</td>";
        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(2)").text() + "</td>";
        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(3)").text() + "</td>";
        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(4)").text() + "</td>";
        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(5)").text() + "</td>";
        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(6)").text() + "</td>";
        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(7)").text() + "</td>";
        comment_email += "<td>" + $("#" + dataId[2] + " td:eq(8)").text() + "</td>";
        comment_email += "<td>" + parseComment(JSON.stringify(comment)) + "</td>";


        comment_email += "</tr></tbody></table>";

        var data = {
            guest_notification_id: dataId[0],
            resident_id: dataId[1],
            carestaff_activity_id: dataId[2],
            mode: mode,
            resident_name: $("#resident_name").text(),
            room: $("#resident_room").text(),
            comment_email: comment_email,
            comment: comment,
            cid: c,
            taskdetail: tasks[current_cid]
        };


        $.ajax({
            url: 'GuestNotification/HandleComment',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            success: function (d) {
                var comment;
                $("#comment").html("");
                if (d.Result != -1) {
                   
                    comment = parseComment(d.comment);

                    $("#well_comment").html(comment);
                }

                //if ($("#guestDiag").modal("open") == false) {
                //    $("#guestDiag").dialog("open");
                //} else {
                    $.connection.hub.start().done(function () {
                        chat.server.send("#message", comment, d.cid);
                    });

                    $(".loader").hide();
                //}

            }
        });

       
    }

    function parseComment(string) {
        var comment = "";
        var date = new Date();
        var timestamp;
      //  debugger
        var obj = JSON.parse("[" + string + "]");
        var role = "";

        for (var i = 0; i < obj.length; i++) {
            role = obj[i].role == "Guest" ? "isGuest" : "isAdmin";
            timestamp = new Date(obj[i].datetime);

            timestamp = new Date(obj[i].datetime);
            //comment += "<span class=\" + + \"" + " >[" + timestamp.getHours() + ":" + timestamp.getMinutes() + "] " + obj[i].userid + ":  " + obj[i].comment + "</span><br/>";
            comment += "<span>";
            comment += "<span style=\"font-weight:bold;font-size:12px\">" + obj[i].userid + " </span>" + "<span style=\"font-size:11px\">" + (timestamp.getMonth() + 1) + "/" + timestamp.getDate() + "/" + timestamp.getFullYear() + " [" + timestamp.getHours() + ":" + timestamp.getMinutes() + "]</span>";
            comment += "<br/>" + obj[i].comment + "</span><br/><br/>";

        }
        //comment = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "<br><br>" + comment
        comment = comment;
        return comment;
    }

    $(document).on("click", ".addComment", function (e) {
        // debugger
        $("#well_comment").text("");
        current_cid = $(this).attr("cid");
        $("#cidtext").val(current_cid);
        did = $(this).attr("did");
        cid = $(this).attr("cid");
        handleComment(did, cid, 0);
        $("#well_comment").attr("cid", cid);
        $('#services_tbl').find("td .addComment[cid='" + cid + "'] .btext").text("Add Comment");
        $('#services_tbl').find("td .addComment[cid='" + cid + "'] i").removeClass("fa fa-envelope");
        $('#services_tbl').find("td .addComment[cid='" + cid + "'] .buttontext").css("color", "white");
        $('#services_tbl').find("td .addComment[cid='" + cid + "'] .buttontext").css("font-weight", "normal");
        $('#services_tbl').find("tr .addComment[cid='" + cid + "']").closest("tr").css("font-weight", "normal");
        $('#resident_table').find(".resident_name[id='" + rid + "']").css("color", "#5867dd");
        $('#resident_table').find(".resident_name[id='" + rid + "'] i").removeClass("fa fa-envelope");
        $("#comment").html("");
       
    });

    
$(document).ready(function () {

    var buildCalendar = function () {
        var date = new Date(),
            n = date.getMonth(),
            y = date.getFullYear();

        var month = n+1;
        var year = y;
        minDate = getMonthDateRange(year, month);
        buildCal(minDate);
    }

    $(document).on("click", "#sendMessage", function () {

        if ($("#comment").val().trim() == "") {
            swal("Please enter a message first", "", "warning");
            return;
        }

        $(".loader").show();
        setTimeout(function () {
            handleComment(did, cid, 1);
            toastr.success("Your comment has been added successfully!");
            $("#comment").val('');
        }, 500);
        

        
        
        // Start the connection.
        //debugger
        //$.connection.hub.start().done(function () {
        //    debugger
        //    chat.server.send("#message", comment, "");
        //});

    });

    
})