﻿/*
* jQuery Plugin Collections
*
* Copyright (c) 2013 Data Soft Logic Corp.
*
*/
;(function ($) {
    $.extend($.fn, {
        extractJson: function (source) {
            var data = {};
            var selector = 'name';
            if (typeof (source) == 'string')
                selector = source;
            else if (typeof (source == 'object'))
                data = $.extend({}, source);

            this.each(function () {
                $("input[" + selector + "],select[" + selector + "],textarea[" + selector + "]", this).each(function () {
                    var c = $(this);
                    if (c.attr('exclude-data') == 1) return true;
                    if (c.is(":password,:text,:radio,input[type='hidden'],select,textarea"))
                        data[c.prop(selector)] = c.val();
                    else if (c.is(":checkbox"))
                        data[c.prop(selector)] = this.checked;
                });
            });
            return data;
        },
        mapJson: function (source) {
            var parentElem = '';
            var selector = 'name';
            if (arguments.length > 1) {
                if(typeof(arguments[1]) == 'string')
                    selector = arguments[1];
                if ( typeof ( arguments[2] ) == 'string' )
                    parentElem = arguments[2] + ' ';
            }
                
            return this.each(function () {
                for (var p in source) {
                    if ( source.hasOwnProperty( p ) ) {
                        var prt = this;
                        if ( parentElem != '' )
                            prt = parentElem;

                        $("input[" + selector + "='" + p + "'],textarea[" + selector + "='" + p + "'],select[" + selector + "='" + p + "']", prt).each(function () {
                            var c = $(this);
                            if (c.is(":text,input[type='hidden']"))
                                c.val(source[p]);
                            else if (c.is("textarea"))
                                c.val(source[p]);
                            else if (c.is("select"))
                                c.find("option[value='" + source[p] + "']").attr("selected", true);
                            else if (c.is(":checkbox")) {
                                //debugger;
                                if ((source[p] != null && source[p].toString().toLowerCase() == 'true') || source[p] == true)
                                    c.attr("checked", true);
                                else
                                    c.attr("checked", false);
                            }
                            else if (c.is(":radio")) {
                                if (c.val() == source[p])
                                    c.attr("checked", "checked");
                            }
                        });
                    }
                }
            })
        },
        enableDisable: function (fields) {
            return this.each(function () {
                for (var p in fields) {
                    if (fields.hasOwnProperty(p)) {
                        $("input[name='" + p + "'],textarea[name='" + p + "'],select[name='" + p + "']", this).each(function () {
                            if (typeof fields[p] === "boolean")
                                $(this).attr("disabled", !fields[p]);
                        });
                    }
                }
            });
        },
        clearFields: function (selector) {
            return this.each(function () {
                if (!selector)
                    selector = "input[name],select[name],textarea[name]";
                $(selector, this).each(function () {
                    var c = $(this);
                    if (c.is(":password,:text,select"))
                        c.val("");
                    else if (c.is("input[type='hidden']") && !c.attr("readonly"))
                        c.val("");
                    else if (c.is("textarea"))
                        c.val("");
                    else if (c.is(":checkbox,:radio"))
                        c.attr("checked", false);
                    c.removeClass("error").next("span.error").remove();
                });
            });
        },
        validate: function (options) {
            var defaults = {};
            var options = $.extend(defaults, options);
            var validated = true;
            this.each(function () {
                $("input.required,textarea.required,select.required", this).each(function () {
                    var c = $(this);
                    if (!c.is(':visible')) return;
                    var email = $("input.email");
                    if (c.val() == "") {
                        
                        if (!c.next("span.error")[0])
                            c.after("<span class=\"error\" title=\"Required field\"></span>").addClass("error");
                        validated = false;
                    }
                    else {
                        c.removeClass("error").next("span.error").remove();
                    }
                    if ($.trim(email.val()) != '') {
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if (!emailReg.test(email.val())) {
                            if (!email.next("span.error")[0])
                                email.after("<span class=\"error\" title=\"Required field\"></span>").addClass("error");
                            validated = false;
                        }
                        else {
                            email.removeClass("error").next("span.error").remove();
                        }
                    }
                });
            });
            return validated;
        },
        forceNumericOnly: function () {
            return this.each(function () {
                $(this).keydown(function (e) {
                    var key = e.charCode || e.keyCode || 0;
                    // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
                    // home, end, period, and numpad decimal
                    return (
             key == 8 ||
             key == 9 ||
             key == 46 ||
             key == 110 ||
             key == 190 ||
             (key >= 35 && key <= 40) ||
             (key >= 48 && key <= 57) ||
             (key >= 96 && key <= 105));
                });
            });
        },
        dropdown: function (options) {
            var defaults = {
                url: "../Services/LookupSvc.asmx/",
                serviceMethod: '',
                params: null,
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false,
                noCache: false
            };
            var options = $.extend(defaults, options);
            if ($("#" + $(this).attr('id') + " option").size() <= 1 || options.noCache) {
                var serviceUrl = options.url + options.serviceMethod + (options.params ? '?' + $.param(options.params) : '');
                var items = [];

                //add user defined default items
                if (options.defaultItems != null) {
                    $.each(options.defaultItems, function (i, o) {
                        if (!o.id || !o.name) return;
                        items.push('<option value="' + o.id + '">' + o.name + '</option>');
                    });
                }
                var that = this; //closure
                //let's get our request 
                $.get(serviceUrl, function (data) {
                    if (options.debug)
                        console.log(JSON.stringify(data));

                    //traverse each item
                    $.each(data, function (i, o) {
                        items.push('<option value="' + o.id + '">' + o.name + '</option>');
                    });

                    that.each(function () {
                        $(this).html(items.join(''));
                        if (options.selectedValue != null)
                            $(this).val(options.selectedValue);
                    });
                });

                return that.each(function () { });
            }
            else {
                if (options.selectedValue != null)
                    $(this).val(options.selectedValue);

                return $(this).each(function () { });
            }
        },
        radiobutton: function (options) {
            var defaults = {
                url: "../Services/LookupSvc.asmx/",
                serviceMethod: '',
                params: null,
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            var serviceUrl = options.url + options.serviceMethod + (options.params ? '?' + $.param(options.params) : '');
            var items = [];
            items.push('<table><tr>');
            var that = this; //closure
            //let's get our request 
            $.get(serviceUrl, function (data) {
                if (options.debug)
                    console.log(JSON.stringify(data));
                //traverse each item
                $.each(data, function (i, o) {
                    if (i == 0)
                        items.push('<td><input checked="checked" type="radio" value="' + o.name + '" name="TempSubject" id="r' + i + '" style="width: 20px;"><label for="r' + i + '" class="fleft">' + o.name + '</label></td>');
                    else
                        items.push('<td><input type="radio" value="' + o.name + '" name="TempSubject" id="r' + i + '" style="width: 20px;"><label for="r' + i + '" class="fleft">' + o.name + '</label></td>');
                    if (((i + 1) % 3) == 0)
                        items.push('</tr><tr>');
                });
                items.push('</tr></table>');
                that.each(function () {
                    $(this).html(items.join(''));
                });
            });

            return that.each(function () { });
        },
        lookupStates: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetStates';
            $(this).bind("change", function () {
                $(this).closest("li").prev("li").find("input").val("");
                $(this).closest("li").next("li").find("input").val("");
            });
            return $(this).dropdown(options)
        },
        lookupDisciplineTypes: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);

            options.serviceMethod = 'GetDisciplineTypes';
            return $(this).dropdown(options);

        },
        lookupGetIntermediaries: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);

            options.serviceMethod = 'GetIntermediaries';
            return $(this).dropdown(options);

        },
        lookupOCTemplateType: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetCommOrderTypeList';
            return $(this).dropdown(options)
        },
        lookupOCTemplateOrderType: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetOrderType';
            return $(this).radiobutton(options)
        },
        lookupGetOrderType: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetOrderType';
            return $(this).dropdown(options);
        },
        lookupGetReferralType: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetReferralType';
            return $(this).dropdown(options);
        },
        lookupGetDischargeType: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetDischargeType';
            return $(this).dropdown(options);
        },
        lookupGetDisaster: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetDisaster';
            return $(this).dropdown(options);
        },
        lookupGetMarketSource: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetMarketSource';
            return $(this).dropdown(options);
        },
        lookupGetLevelOfCareType: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetLevelOfCareType';
            return $(this).dropdown(options);
        },
        lookupGetNonAdmitStatusList: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetNonAdmitStatusList';
            return $(this).dropdown(options);
        },
        lookupGetPharmacies: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetPharmacies';
            return $(this).dropdown(options);
        },
        lookupGetAddressType: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetAddressType';
            return $(this).dropdown(options);
        },
        lookupGetContactInfoType: function (options) {
            var defaults = {
                defaultItems: null, // add more default items here like [{ id: '-1', name: 'All' }, {...}]
                selectedValue: null,
                debug: false
            };
            var options = $.extend(defaults, options);
            options.serviceMethod = 'GetContactInfoType';
            return $(this).dropdown(options);
        },
        autoCompleteZipCode: function (options) {
            var defaults = {
                url: "../Services/LookupSvc.asmx/",
                serviceMethod: 'GetZipCodes',
                params: null,
                debug: false,
                onchange: null
            };
            var options = $.extend(defaults, options);
            var that = this; //closure
            //let's get our request 
            that.each(function (idx) {
                $(this).autocomplete({
                    source: function (request, response) {
                        options.params = { zipcode: request.term };
                        var serviceUrl = options.url + options.serviceMethod + (options.params ? '?' + $.param(options.params) : '');
                        $.get(serviceUrl, function (data) {
                            if (options.debug)
                                console.log(JSON.stringify(data));
                            response($.map(data, function (item) {
                                return {
                                    label: item.zip_code,
                                    value: item.zip_code,
                                    city: item.city,
                                    cbsa: item.cbsa,
                                    state: item.state,
                                    index: idx
                                }
                            }));
                        });
                    },
                    delay: 200,
                    minLength: 2,
                    focus: function (event, ui) {
                        $(this).val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        if (ui.item && ui.item.label) {
                            $(this).val(ui.item.label);
                            if (options.onchange != null && typeof (options.onchange) === 'function') {
                                options.onchange(ui.item);
                            }
                        }
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "<br/><small style=\"font-size:0.9em\">"
                   + item.city + (item.state != '' ? ', ' + item.state : '') + "</small></a>")
                    .appendTo(ul);
                };

            });

            return that.each(function () { });
        },
        autoCompleteCity: function (options) {
            var defaults = {
                url: "../Services/LookupSvc.asmx/",
                serviceMethod: 'GetCitys',
                params: null,
                debug: false,
                stateSelector: null,
                onchange: null
            };
            var options = $.extend(defaults, options);
            var that = this; //closure
            //let's get our request 
            that.each(function (idx) {
                $(this).autocomplete({
                    source: function (request, response) {
                        if (options.params == null) options.params = {};
                        if (options.stateSelector != null)
                            options.params.state = $.isFunction(options.stateSelector.get) ? $(options.stateSelector.get(idx)).val() : options.stateSelector;
                        options.params.cityname = request.term;
                        var serviceUrl = options.url + options.serviceMethod + (options.params ? '?' + $.param(options.params) : '');
                        $.get(serviceUrl, function (data) {
                            if (options.debug)
                                console.log(JSON.stringify(data));
                            response($.map(data, function (item) {
                                return {
                                    label: item.zip_code,
                                    value: item.zip_code,
                                    city: item.city,
                                    state: item.state,
                                    index: idx
                                }
                            }));
                        });
                    },
                    delay: 200,
                    minLength: 2,
                    focus: function (event, ui) {
                        $(this).val(ui.item.city);
                        return false;
                    },
                    select: function (event, ui) {
                        if (ui.item && ui.item.label) {
                            $(this).val(ui.item.city);

                            if (options.onchange != null && typeof (options.onchange) === 'function') {
                                options.onchange(ui.item);
                            }
                        }
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "<br/><small style=\"font-size:0.9em\">"
                   + item.city + (item.state != '' ? ', ' + item.state : '') + "</small></a>")
                    .appendTo(ul);
                };

            });

            return that.each(function () { });
        },
        findPhysicians: function (options) {

            var defaults = {
                url: "../Services/LookupSvc.asmx/",
                serviceMethod: 'FindPhysician',
                params: null,
                debug: false,
                onchange: null
            };
            var options = $.extend(defaults, options);
            var that = this; //closure
            //let's get our request 
            that.each(function (idx) {
                $(this).autocomplete({
                    source: function (request, response) {
                        options.params = { query: request.term };
                        var serviceUrl = options.url + options.serviceMethod + (options.params ? '?' + $.param(options.params) : '');
                        $.get(serviceUrl, function (data) {
                            if (options.debug)
                                console.log(JSON.stringify(data));
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.id,
                                    address: item.address,
                                    index: idx
                                }
                            }));
                        });
                    },
                    delay: 200,
                    minLength: 2,
                    focus: function (event, ui) {
                        $(this).val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        if (ui.item && ui.item.label) {
                            $(this).val(ui.item.label);
                            if (options.onchange != null && typeof (options.onchange) === 'function') {
                                options.onchange(ui.item);
                            }
                        }
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "<br/><small style=\"font-size:0.9em\">"
                   + item.address + "</small></a>")
                    .appendTo(ul);
                };

            });

            return that.each(function () { });
        },
        findDisciplines: function (options) {
            var defaults = {
                url: "../Services/LookupSvc.asmx/",
                serviceMethod: 'FindDiscipline',
                params: null,
                debug: false,
                onchange: null
            };
            var options = $.extend(defaults, options);
            var that = this; //closure
            //let's get our request 
            that.each(function (idx) {
                $(this).autocomplete({
                    source: function (request, response) {
                        options.params = { query: request.term };
                        var serviceUrl = options.url + options.serviceMethod + (options.params ? '?' + $.param(options.params) : '');
                        $.get(serviceUrl, function (data) {
                            if (options.debug)
                                console.log(JSON.stringify(data));
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.id,
                                    type: item.type,
                                    index: idx
                                }
                            }));
                        });
                    },
                    delay: 200,
                    minLength: 2,
                    focus: function (event, ui) {
                        $(this).val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        if (ui.item && ui.item.label) {
                            $(this).val(ui.item.label);
                            if (options.onchange != null && typeof (options.onchange) === 'function') {
                                options.onchange(ui.item);
                            }
                        }
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "<br/><small style=\"font-size:0.9em\">"
                   + item.type + "</small></a>")
                    .appendTo(ul);
                };

            });

            return that.each(function () { });
        },
        findInsurances: function (options) {
            var defaults = {
                url: "../Services/LookupSvc.asmx/",
                serviceMethod: 'FindInsurance',
                params: null,
                debug: false,
                onchange: null
            };
            var options = $.extend(defaults, options);
            var that = this; //closure
            //let's get our request 
            that.each(function (idx) {
                $(this).autocomplete({
                    source: function (request, response) {
                        options.params = { query: request.term };
                        var serviceUrl = options.url + options.serviceMethod + (options.params ? '?' + $.param(options.params) : '');
                        $.get(serviceUrl, function (data) {
                            if (options.debug)
                                console.log(JSON.stringify(data));
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.id,
                                    address: item.address,
                                    index: idx
                                }
                            }));
                        });
                    },
                    delay: 200,
                    minLength: 2,
                    focus: function (event, ui) {
                        $(this).val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        if (ui.item && ui.item.label) {
                            $(this).val(ui.item.label);
                            if (options.onchange != null && typeof (options.onchange) === 'function') {
                                options.onchange(ui.item);
                            }
                        }
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "<br/><small style=\"font-size:0.9em\">"
                   + item.address + "</small></a>")
                    .appendTo(ul);
                };
            });
            return that.each(function () { });
        },
        findPatients: function (options) {
            var defaults = {
                url: "../Services/LookupSvc.asmx/",
                serviceMethod: 'FindPatient',
                params: null,
                debug: false,
                onchange: null
            };
            var options = $.extend(defaults, options);
            var that = this; //closure
            //let's get our request 
            that.each(function (idx) {
                $(this).autocomplete({
                    source: function (request, response) {
                        options.params = { query: request.term };
                        var serviceUrl = options.url + options.serviceMethod + (options.params ? '?' + $.param(options.params) : '');
                        $.get(serviceUrl, function (data) {
                            if (options.debug)
                                console.log(JSON.stringify(data));
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.id,
                                    addr: item.addr,
                                    url: item.url,
                                    index: idx
                                }
                            }));
                        });
                    },
                    delay: 200,
                    minLength: 2,
                    focus: function (event, ui) {
                        $(this).val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        if (ui.item && ui.item.label) {
                            $(this).val(ui.item.label);
                            if (options.onchange != null && typeof (options.onchange) === 'function') {
                                options.onchange(ui.item);
                            }
                        }
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "<br/><small style=\"font-size:0.9em\">" + item.addr + "</small></a>")
                    .appendTo(ul);
                };
            });
            return that.each(function () { });
        }
    });
    $.extend($, {
        growl: function (options) {
            var defaults = {
                message: "",
                delay: 3000,
                style: "notice" // "success", "warning", "error"
            };
            var options = $.extend(true, {}, defaults, options);

            var init = function () {
                if ($("body:not(:has(#growls))")[0]) {
                    $("body").append("<div id='growls' />");
                    $("#growls").on("click", "div.growl-close", function () {
                        $(this).closest(".growl").remove();
                    });
                }
            };

            var render = function () {
                if (options.message != "") {
                    var $g = $("<div class='growl'><div class='growl-close'>&times;</div><div class='growl-message'>" + options.message + "</div></div>").addClass(options.style).appendTo("#growls");
                    if (options.delay > 0) {
                        setTimeout(function () {
                            $g.fadeOut(1000, function () {
                                $(this).remove();
                            });
                        }, options.delay);
                    }
                }
            };

            init();
            render();
        },
        growlError: function (options) {
            $.growl($.extend({ style: "error", delay: 0 }, options));
        },
        growlWarning: function (options) {
            $.growl($.extend({ style: "warning" }, options));
        },
        growlSuccess: function (options) {
            $.growl($.extend({ style: "success" }, options));
        }
    });
    //This is global function for dialog when you close your dialog this will be execute to destroy or dispose.
    $(function () {
        $.ui.dialog.prototype._originalClose = $.ui.dialog.prototype.close;
        $.ui.dialog.prototype.close = function () {
            $.ui.dialog.prototype._originalClose.apply(this, arguments, "destroy");
        };
    });
})(jQuery);