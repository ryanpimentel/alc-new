﻿$(document).ready(function () {
    //$('.schedule').height($(window).height() - 265);
    //$(window).resize(function () {
    //    $('.schedule').height($(window).height() - 265);
    //})

    var setHeight = function () {


        $('.dataTables_scrollBody').height($(window).height() - 80);
     
        if ($(window).height() < 400) {
            $('.dataTables_scrollBody').height($(window).height() - 80);
           
        }

        $(window).resize(function () {
            $('.dataTables_scrollBody').height($(window).height() - 80);

            if ($(window).height() < 400) {
                $('.dataTables_scrollBody').height($(window).height() - 80);
               
                var emp_scroll = new PerfectScrollbar('.dataTables_scrollBody');
            }
        })

    }
    
    function MedTechCalendar(elem, data) {
       
        var $e = $('#' + elem);
       
        //var data = [{ "admission_id": 1, "resident_id": "2", "resident_name": "Joe A. Lambert", "medicines": [{ "receive_detail_id": 5, "medicine": "Asperin", "dosage_times": [{ "time": "08:00", "intake_status": 1, "dosage_schedule_id": 4 }, { "time": "16:00", "intake_status": 0, "dosage_schedule_id": 5, }, { "time": "20:00", "intake_status": null, "dosage_schedule_id": 6 }] }, { "receive_detail_id": "6", "medicine": "Vitamin C", "dosage_times": [{ "time": "08:00", "intake_status": null, "dosage_schedule_id": 2 }, { "time": "16:00", "intake_status": null, "dosage_schedule_id": 3 }] }] }, { "admission_id": "2", "resident_id": "1", "resident_name": "Ron F. Harper", "medicines": [{ "receive_detail_id": "1", "medicine": "Analgesic", "dosage_times": [{ "time": "08:00", "intake_status": null, "dosage_schedule_id": 7 }, { "time": "12:00", "intake_status": null, "dosage_schedule_id": 8 }, { "time": "16:00", "intake_status": null, "dosage_schedule_id": 9 }, { "time": "20:00", "intake_status": null, "dosage_schedule_id": 10 }] }, { "receive_detail_id": "2", "medicine": "Propanolamin", "dosage_times": [{ "time": "08:00", "intake_status": null, "dosage_schedule_id": 2 }, { "time": "16:00", "intake_status": null, "dosage_schedule_id": 3 }] }, { "receive_detail_id": "3", "medicine": "Streptomycin", "dosage_times": [{ "time": "08:00", "intake_status": null, "dosage_schedule_id": 2 }, { "time": "16:00", "intake_status": null, "dosage_schedule_id": 3 }] }, { "receive_detail_id": "4", "medicine": "Asperin", "dosage_times": [{ "time": "08:00", "intake_status": 1, "dosage_schedule_id": 4 }, { "time": "16:00", "intake_status": 0, "dosage_schedule_id": 5 }, { "time": "20:00", "intake_status": null, "dosage_schedule_id": 6 }] }] }];
        //console.log("data:2");
        //console.log(data);
        //cheche was here : 4-06-2022
        var createRow = function (obj) {
          
            var html = [];
            html.push('<tr class="parent">');
            html.push('<td style="font-weight:700">' + obj.resident_name + '</td>');
            var meds = [];
            var medlen = obj.medicines ? obj.medicines.length : 0;

                
                if (medlen > 0) {

                    var oM = obj.medicines;

                    var times = ["23:59", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"];
                    var dosage_schedule_counter = 0;
                    var matchEval = function (o, time) {

                        var dose = $.grep(o.dosage_times, function (o) { return o.time == time });
                        
                        var med_info = dose[0];
                        var interval = 0;


                        if (dose.length > 0) {
                           
                            var css = "class='";

                            if (med_info.start_time != null) {
 
                                //get dosage id and divide it t0 24 to get the interval
                                dosage_id = med_info.dosage;
                                interval = 24 / dosage_id;


                                start_time = med_info.start_time.split('T');
                                if (start_time.length > 0) {

                                        //if counter = 0, it means this is the first scheduled time for intake
                                    if (dosage_schedule_counter == 0) {
                                        time = start_time[1].slice(0, 5);
                                    } else {

                                        stime = start_time[1].slice(0, 2);
                                        new_time = parseInt(stime) + (interval * dosage_schedule_counter);

                                        if (parseInt(new_time) > 24) {
                                            new_time = parseInt(new_time) - 24;
                                        }

                                        if (parseInt(new_time) < 10) {
                                            new_time = "0" + new_time;
                                        }

                                        time = new_time.toString() + start_time[1].slice(2, 5);
                                    }


                                }
                                dosage_schedule_counter += 1;
                            }


                            if (time == "08:00")
                                css += 'col8AM';
                            else if (time == "9:00")
                                css += 'col9AM';
                            else if (time == "10:00")
                                css += 'col10AM';
                            else if (time == "11:00")
                                css += 'col1AM';
                            else if (time == "12:00")
                                css += 'col12PM';
                            else if (time == "13:00")
                                css += 'col1PM';
                            else if (time == "14:00")
                                css += 'col2PM';
                            else if (time == "15:00")
                                css += 'col3PM';
                            else if (time == "16:00")
                                css += 'col4PM';
                            else if (time == "17:00")
                                css += 'col5PM';
                            else if (time == "18:00")
                                css += 'col6PM';
                            else if (time == "19:00")
                                css += 'col7PM';
                            else if (time == "20:00")
                                css += 'col8PM';
                            else if (time == "21:00")
                                css += 'col9PM';
                            else if (time == "22:00")
                                css += 'col10PM';
                            else if (time == "23:00")
                                css += 'col11PM';
                            else if (time == "23:59" || time == "24:00")
                                css += 'col12AM';
                            else if (time == "01:00")
                                css += 'col1AM';
                            else if (time == "02:00")
                                css += 'col2AM';
                            else if (time == "03:00")
                                css += 'col3AM';
                            else if (time == "04:00")
                                css += 'col4AM';
                            else if (time == "05:00")
                                css += 'col5AM';
                            else if (time == "06:00")
                                css += 'col6AM';
                            else if (time == "07:00")
                                css += 'col7AM';


                            var status = dose[0].intake_status;
                          
                            var sign = "<i style=\"font-weight: bold; color:green;\" class=\"la la-check med_sign\"></i>";
                            if (status != null) {

                                if (status == 1) {
                                    //css = "class='complete'";
                                    css += "class=''";
                                } else if (status == 0) {
                                    //css = "class='warning'";
                                    css += "class=''";
                                    sign = "<i style=\" color:deepskyblue;\" class=\"la la-exclamation med_sign\"></i>";
                                }

                            } else {
                            var doseTime = moment(ALC_Util.CurrentDate + ' ' + dose[0].time, 'MM/DD/YYYY HH:mm');                       
                            var curTime = moment($('#spCT').text(), "ddd, MMM DD YYYYY HH:mmA");

                            if (doseTime.isValid() && curTime.isValid() /*&& doseTime.isBefore(curTime)*/) { //comment out this because every hour is use : Cheche 4-07-2022
                                //debugger
                              
                                css += "class=''";
                                sign = "<span style=\"font-weight: bold; color:red;\" class=\"med_sign\">X</span>";
                            } else {
                                css += "'";
                                sign = "";
                                }
                               
                            }
                            return {

                                key: time,
                                value: '<a style="text-decoration-style: dotted;text-decoration-line: underline; font-weight: 500 !important;" status="' + status + '"  med_status="' + o.status + '"  rdid="' + o.receive_detail_id + '" dsid="' + dose[0].dosage_schedule_id + '">' + sign + ' ' + o.medicine + '</a><br/>'
                                //value: '<a style="text-decoration-style: dotted;text-decoration-line: underline; font-weight: 500 !important;"  rdid="' + o.receive_detail_id + '" dsid="' + dose[0].dosage_schedule_id + '" ' + css + '>' + sign + ' ' + o.medicine + '</a><br/>'
                            }
                        } else {










                        }

                        return null;
                    }
                    var links = [];
                    $.each(times, function () {

                        var time = this;
                        $.each(oM, function () {
                            var match = matchEval(this, time);
                            if (match != null)
                                links.push(match);
                        })

                    });

                    $.each(times, function () {

                        var vals = [];
                        var time = this;
                        var meds = $.grep(links, function (o) { return o.key.toString() === time.toString(); });
                        $.each(meds, function () {

                            vals.push(this.value);
                        });
                        html.push('<td>' + vals.join('') + '</td>');
                    });
                }
                html.push('</tr>');
                return html.join('');
            
        }
        //kim add this after saving administer  this will auto refresh and icon will pop up 11/23/22
        $("#medtechCal").DataTable().destroy();
        $("#medtechCal_body").html('');

        for (var h = 0; h < data.length; ++h) {

            $e.append(createRow(data[h]));
        }
     
     
      
        $("#medtechCal").DataTable({
            responsive: !0,
            pagingType: "full_numbers",
            searching: false,
            dom: 'lBfrtip',
            //"scrollX": true,
            //"scrollCollapse": true,
            select: true,
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="la la-download"></i> Export to Excel',
                    title: 'ALC_Employees_ServiceTask_' + id,
                    className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',

                    init: function (api, node, config) {
                        $(node).removeClass('dt-button')
                    }
                }, {
                    text: 'Refresh',
                    action: function (e, dt, node, config) {
                        loadSvcTask(currentId);
                    },
                    className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air refreshTask',
                    init: function (api, node, config) {
                        $(node).removeClass('dt-button')
                    }
                }


            ],
            scrollCollapse: true,
            scrollY: "300px",
            scrollX:        true,
            fixedColumns: {
                left: 1
            }
        });

        setHeight();

        $(".loader").hide();
    }

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Medicine Administration",
        dialogClass: "companiesDialog",
        open: function () {
            $("#diag #Yes").prop('checked', true);
        }
    });


    var showdiag = function (mode) {

        $("#diag").dialog("option", {
            width: 350,//$(window).width() - 100,
            height: 290,//$(window).height() - 100,
            position: "center",
            buttons: {
                "Done": {
                    click: function () {
                        if ($('#diag input[type=checkbox]:checked').length <= 0) {
                            $.growlWarning({ message: "Please choose Yes or No before proceeding to done or click 'Cancel' to cancel changes.", delay: 6000 });
                            return;
                        }

                        if ($('#diag #No').is(":checked") && $('#diag #Comment').val() == "") {
                            $.growlWarning({ message: "Please provide comment first before proceeding to done or click 'Cancel' to cancel changes.", delay: 6000 });
                            return;
                        }

                        //save changes
                        var pdata = $('#diag').extractJson('id');

                        var rdid = $('#param1').val();
                        var dsid = $('#param2').val();

                        pdata.rdid = rdid;
                        pdata.dsid = dsid;
                        var mode = 1;
                        if ($('#Id').val() != '')
                            mode = 2;

                        var data = { func: "mar_info", mode: mode, Data: pdata };

                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "MAR/PostData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (m != null && m.result == 0) {
                                    $.growlSuccess({ message: "Administration saved successfully.", delay: 4000 });
                                    $('#medtechCal_body tr').slice(1).detach();
                                 
                                    build();
                                }
                            }
                        });

                        $("#diag").dialog("close");
                    },
                    class: "primaryBtn",
                    text: 'Done'
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $("input:checkbox").on('click', function () {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            if ($('#mar_modal input:checked').size() == 0)
                $box.prop("checked", true);
            else
                $box.prop("checked", false);
        }
        if ($box.attr('Id') == 'No' && $box.is(":checked"))
            $('#Comment').removeAttr('disabled');
        else
            $('#Comment').attr('disabled', 'disabled');
    });


    $("#administer").on("click", function () {

        if ($('#mar_modal input[type=checkbox]:checked').length <= 0) {

            swal({
                title: "Please choose Yes or No before proceeding to done or click 'Cancel' to cancel changes.",
                type: "warning",
            });
            return;
        }

        if ($('#mar_modal #No').is(":checked") && $('#mar_modal #Comment').val() == "") {
            swal({
                title: "Please provide comment first before saving or click 'Cancel' to cancel changes.",
                type: "warning",
            });
            return;
        }

        //save changes
        var pdata = $('#mar_modal').extractJson('id');

        var rdid = $('#param1').val();
        var dsid = $('#param2').val();

        pdata.rdid = rdid;
        pdata.dsid = dsid;
        var mode = 1;
        if ($('#Id').val() != '')
            mode = 2;

        var data = { func: "mar_info", mode: mode, Data: pdata };


        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MAR/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (m != null && m.result == 0) {
                  
                 
                    //$("#mar_form").trigger("reset");
                    swal("Administration saved successfully.", "", "success");
                  
                    $(".close").trigger("click");
                 
                    // $('#medtechCal tr').slice(2).detach();

                    build();
                }
                if (m.message == "negative") {
                    swal("Unable to save administration.There is an insufficient quantity of supply/medicine", "", "warning");
                    return
                }
            }
        });
    })


    $('#medtechCal').on('click', 'a', function (ev) {

        med_status = $(this).attr('med_status');
        d_class_status = $(this).attr('class');
        $("#deletedInfo").attr('hidden', true);
        $('#Yes').attr('disabled', false);
        $('#No').attr('disabled', false);
        $("#administer").attr('disabled', false);

        //For Med status from Med & Supplies
        if (med_status == "null" || med_status == "false" && d_class_status != "warning") {

            $("#Comment").attr('disabled', true);
            $("#administer").attr('disabled', true);
            $('#Yes').attr('disabled', true);
            $('#No').attr('disabled', true);
            $("#deletedInfo").attr('hidden', false);
            $(".deleted_med_lbl").html("This medicine was deleted from the medicines and supplies page.");
        }



        $("#mar_modal").modal("toggle");

        var target = ev.target || ev.targetElement
        var dis = $(target);
        var rdid = dis.attr('rdid');
        var dsid = dis.attr('dsid');
        var intake_stat = dis.attr('status');

        //For med intake status
        if (intake_stat == "true" || intake_stat == "false") {
            $("#administer").prop("disabled", true);
            $('#Yes').attr('disabled', true);
            $('#No').attr('disabled', true);
        }


        $('#param1').val(rdid);
        $('#param2').val(dsid);
        var res = $(dis.parent().siblings().get(0)).text() || '';
        $('#medInfo').html('Administer ' + dis.text() + ' to ' + '<span style="font-weight:600">' + res + '</span>' + ' ?');
        var data = { func: "mar_info", param1: rdid, param2: dsid };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MAR/GetData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m != null) {
                    $('#Id').val(m.dmid);
                    $('#Yes,#No').removeAttr('checked');
                    if (m.status == true)
                        $('#Yes').attr('checked', 'checked');
                    else
                        $('#No').attr('checked', 'checked');
                    $('#Comment').val(m.comment);
                }
            }
        });

    });


    $(document).keyup(function (e) {
        if (e.keyCode === 27) $('.close').click();   // esc
    });

    $("#search_mar_value").keyup(function () {
        var string = $(this).val().toLowerCase();

        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
    })

    $("#showall").on("click", function () {

        $(".parent").each(function (i, e) {
            $(this).show();
        });
        //Added this on 01-25-22: Angelie
        $("#search_mar_value").val("");
    })

    //Added this on 11-10-22: Angelie
    $(document).on("click", ".med_sign", function (i, v) {
        $(this).parent('a').trigger("click");
    });


    //$(document).on("click", ".close", function (i, v) {
    //    build();
    //});

    var build = function pullData(cb) {

        $("#medtechCal_body").html("");
        $("#medtechCal").DataTable().destroy();
      
        var data = { func: "mar", param1: ALC_Util.CurrentDate };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MAR/GetData",
            data: JSON.stringify(data),
            dataType: "json",
            async: "false",
            success: function (m) {
                
                MedTechCalendar('medtechCal_body', m);
            }
        });
    };
    build();
    var scroll1 = new PerfectScrollbar('#medtechCal_body');
});