﻿$(document).ready(function () {
    function resizeContainer() {
        var contentHeight = $(document).height() - ($('footer').height() + $('header').height());
        $('#contentContainer').height(contentHeight);
    }
    resizeContainer();
    AHL = $('#contentContainer');
    AAHL = AHL.layout({
        center: {
            paneSelector: '#mainPanel',
            size: 'auto',
            closable: false,
            resizable: false,
            slidable: false
        },
        west: {
            paneSelector: "#treeView",
            size: 250,
            minSize: 100,
            maxSize: 1000,
            togglerClass: 'toggler',
            spacing_closed: 38,
            togglerTip_open: 'Collapse',
            togglerTip_closed: 'Expand',
            togglerLength_closed: 34,
            togglerAlign_closed: 'top',
            togglerLength_open: 0
            , slideTrigger_open: ''
            , hideTogglerOnSlide: true
            , sliderCursor: 'normal'
            , onopen_start: function () {
                $('#treeView').css({ 'z-index': 0, 'width': '250px', 'background-color': '#fff !important', 'top': '0', 'margin-left': '0' }).show();
                $('.treeNav').css({ 'top': '34px', 'background-color': '#2F94C5 !important' });
                $('#treeViewHeader').show();
                $('.jstree a').css({ 'color': '#eff0f1' }).removeClass('nohover');
                $('a ins.jstree-icon').removeClass('jstree-icon-close');
                $('#jsTreeView li:first').removeAttr('style');
                $('#jsTreeView li[node-level=3]:last').removeAttr('style');
                $('.toggler-cus-west-open').hide();
            }
            , onopen_end: function () {

            }
            , onclose_start: function () {

            }
            , onclose_end: function () {
                $('#treeView').css({ 'z-index': 9, 'width': '60px', 'background-color': 'transparent !important', 'top': '34px', 'margin-left': '-24px' }).show();
                $('.treeNav').css({ 'top': '0', 'background-color': 'transparent !important' });
                $('#treeViewHeader').hide();
                $('.jstree a').css({ 'color': '#2F94C5' }).addClass('nohover');
                $('a ins.jstree-icon').addClass('jstree-icon-close');
                $('#jsTreeView li:first').attr('style', 'padding-top:10px');
                $('#jsTreeView li[node-level=3]:last').attr('style', 'padding-bottom: 10px');
                $('.toggler-cus-west-open').show();

            }
        },
        onresize: function () {
            resizeContainer();
            if (typeof (THL) !== 'undefined') {
                THL.layout('resizeAll');
            }
            if (typeof (MPL) !== 'undefined') {
                MPL.layout('resizeAll');
            }
        }
    });

    THL = $("#treeView");
    THL.layout({
        center: {
            paneSelector: ".treeNav",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
        },
        north: {
            paneSelector: "#treeViewHeader",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
        }
    });
    MPL = $("#mainPanel");
    MPL.layout({
        center: {
            paneSelector: "#mainContent",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
        },
        north: {
            paneSelector: "#mainHeader",
            closable: false,
            slidable: false,
            resizable: false,
            size: 35,
            spacing_open: 0
        },
        onresize: function () {
            if (typeof (PHL) !== 'undefined') {
                PHL.layout('resizeAll');
            }
        }
    });



    $("#jsTreeView").jstree({
        plugins: ["json_data", "ui"],
        core: {
            animation: 0,
            html_titles: true
        },
        ui: {
            select_limit: 1,
            selected_parent_close: false
        },
        themes: {
            theme: "classic",
            dots: true,
            icons: true,
            url: false
        },
        json_data: {
            ajax: {
                cache: false,
                url: "Admin/GetNodes",
                async: true,
                contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: "json",
                data: function (n) {
                    return JSON.stringify({ treeId: "admin", nodeId: n.attr ? n.attr("id") : "0", id: "0" });
                },
                success: function (data) {
                    return data;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                complete: function () {
                }
            }
        }
    }).bind("loaded.jstree", function (e, data) {
        data.inst.select_node('ul > li:eq(1)');
    }).bind("select_node.jstree", function (e, data) {
        var obj = data.rslt.obj;
        if (!data.inst.is_open(obj))
            data.inst.open_node(obj);
        else {
            var first = data.inst._get_next(obj);
            if (data.inst.is_leaf(first)) {
                data.inst.deselect_node(data.inst.get_selected());
                data.inst.select_node(first);
            }
        }
        if (data.inst.is_leaf(obj)) {
            getNodes(obj, function () {
                setHeaders(obj);
            });
        }
    }).bind("after_open.jstree", function (e, data) {
    });
    var setHeaders = function (obj) {
        var url = obj.find("a > ins.jstree-icon").css("background-image").replace(/\url|\(|\"|\"|\'|\)/g, "");
        $("#labelHeader ~ img").remove();
        $("#labelHeader").text(obj.attr("title"));
        if (url.split('/').pop() != "treeview.png")
            $("#labelHeader").after("<img src=\"" + url + "\" alt=\"\" style=\"float: right; margin-top: 10px; margin-right: 10px\"/>");
    }

    var getNodes = function getNodes(obj, callback) {
        if (obj.attr('id').indexOf("marketing") != -1) {
            window.location.href = ALC_URI.Marketer;
            return;
        }
        if (obj.attr('id').indexOf("transsched") != -1) {
            window.location.href = ALC_URI.Transsched;
            return;
        }
        if (obj.attr('id').indexOf("mar") != -1) {
            window.location.href = ALC_URI.Transsched;
            return;
        }
        $.ajax({
            url: "Admin/GetNodeData",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            data: '{id:"' + obj.attr("id") + '"}',
            success: function (d) {
                $('.ui-dialog').remove();
                $("#mainContent").empty();
                $("#mainContent").html(d);
            },
            complete: function () {
                if (callback) callback();
            },
            error: function (response) {
                $('.ui-dialog').remove();
                $("#mainContent").empty();

                if (response && response.responseText) {
                    var $err_dialog = $("<div class='form'><div style=\"padding: 10px;\">" + response.responseText + "</div></div>").dialog({
                        modal: true,
                        title: "Error",
                        width: 800,
                        height: 500,
                        close: function () {
                            $err_dialog.dialog("destroy").remove();
                        },
                        buttons: {
                            "Close": function () {
                                $err_dialog.dialog("close");
                            }
                        }
                    });
                }
            }
        });
    }

    $('.toggler-cus-west-open').click(function () {
        if (AAHL !== 'undefined') {
            AAHL.toggle('west');
        }
    })
    $('#treeView-toggler').off('click').on('click', function () {
        if (AAHL !== 'undefined') {
            AAHL.toggle('west');
            $('.toggler-cus-west-open').show();
        }
    });
});