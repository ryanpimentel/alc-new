﻿$(window).resize(function () {
    var diff = $(window).height() - 155;
    $('.form-container').height(diff);
});
$(document).ready(function () {
    var diff = $(window).height() - 155;
    $('.form-container').height(diff);

    $("#Age").forceNumericOnly();
    $(".refdate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });

    //$('#TourTime').timepicker({
    //    showPeriod: true,
    //    showLeadingZero: false,
    //    showOn: 'focus'
    //});

    $('#ContactPersonPhone,#ResidentPhone').mask("(999)999-9999");

    $("input:checkbox").on('click', function () {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });

    $('#cbspecneed').click(function () {
        var d = $(this);
        if (d.prop('checked'))
            $('#SpecNeed').removeAttr('disabled');
        else {
            $('#SpecNeed').attr('disabled', 'disabled');
            $('#SpecNeed').val('');
        }
    });

    $('#btnSave').click(function () {
        var data = { func: "postform" };
        var isValid = $('#divCon').validate();

        if (!isValid) return;
        data.data = $('#divCon').extractJson('id');
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Referral + "/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.Result == 0) {
                    $.growlSuccess({ message: "Referral saved successfully!", delay: 6000 });
                    $('.form-container').clearFields();
                    if (typeof (Dashboard.LoadWidgets) !== 'undefined') {
                        Dashboard.LoadWidgets();
                        Dashboard.HideDialog();
                    }
                }
            }
        });
        return false;
    });

    $('#btnClear').click(function () {
        $('.form-container').clearFields();
    });

    //added by Mariel - 2017-07-19
    var SearchReference = "Search Reference";
    $("#RefSource").val(SearchReference).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchReference);
    }).focus(function (e) {
        if ($(this).val() == SearchReference)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_reference", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.reference,
                            value: item.id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 2,
        focus: function (event, ui) {
            $("#RefSource").val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };
});