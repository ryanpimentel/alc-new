﻿var marketerState = {
    orig: '',
    current: ''
};
var marketerAssState = {
    orig: '',
    current: ''
};
(function () {
    $(".loader").show();
})
var tags = [];
var array = [];
var asessment_params;
var css;
var mode = false;
var Dashboard = {
    BuildPendingAdmission: function () { }
}

function capsFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
var setHeight = function () {
    $('#Home').height($(window).height() - 150);
    $('#Home .col-xs-3').height($("#Home").height() / 2);
    $('#Home .col-xs-3 .tab-content').height($(".col-xs-3").height() - 100);
    $('#referral_modal .m-portlet__body').height($(window).height() - 250);
    $('#admission_modal .adm-content').height($(window).height() - 320);
    $('#assessment_modal #content-assessment').height($(window).height() - 320);

    if ($(window).height() < 400 || $(window).width() < 1024) {
        $('#referral_modal .m-portlet__body').height(500);
        $('#assessment_modal #content-assessment').height(500);
        $('#admission_modal .adm-content').height(500);

        $('#Home .col-xs-3').height(800);
        $('#Home .col-xs-3 .tab-content').height(300);
        $('#Home .col-xs-3 .m-portlet--full-height .tab-content').height(300);
        $("#scroll_8").parent().parent().css("margin-bottom", "5rem");
    }

    $(window).resize(function () {
        $('#Home').height($(window).height() - 150);
        $('#Home .col-xs-3').height($("#Home").height() / 2);
        $('#Home .col-xs-3 .tab-content').height($(".col-xs-3").height() - 100);
        $('#referral_modal .m-portlet__body').height($(window).height() - 250);
        $('#assessment_modal #content-assessment').height($(window).height() - 320);
        $('#admission_modal .adm-content').height($(window).height() - 320);

        if ($(window).height() < 400 || $(window).width() < 1024) {
            $('#referral_modal .m-portlet__body').height(500);
            $('#assessment_modal #content-assessment').height(500);
            $('#admission_modal .adm-content').height(500);

            $('#Home .col-xs-3').height(800);
            $('#Home .col-xs-3 .tab-content').height(300);
            $('#Home .col-xs-3 .m-portlet--full-height .tab-content').height(300);
            $("#scroll_8").parent().parent().css("margin-bottom", "5rem");
        }
    })

    $('#assessment_modal #content-assessment').css("overflow", "hidden auto");
    $('#admission_modal .adm-content').css("overflow", "hidden auto");

}

var buildPendingAdmission = function () {
    
    var data = {
        func: "dashboard_preadmission_nav",
        id: "0"
    };
    $.ajax({
        url: ALC_URI.Admin + "/GetFormData",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (res) {
            var x = [];
            $.each(res, function () {
                var name = this.Fname + " ";
                if (this.MI != '')
                    name += this.MI + " ";
                name += this.Lname;
                x.push("<tr class=''><td style='text-align:center;width:20px'><input name='chkresidents' value='" + this.ID + "' type='checkbox'/></td><td>");
                x.push("<a id='" + this.ID + "' tab='" + this.Step + "'>" + name + "</a>");
                x.push("</td><td>");
                x.push(this.PreAdmissionDate);
                x.push("</td><td>");
                x.push(this.AdmissionStatus);
                x.push("</td></tr>");
            });

            $('#dataTable tbody').html(x.join(''));

            // Usage: $form.find('input[type="checkbox"]').shiftSelectable();
            // replace input[type="checkbox"] with the selector to match your list of checkboxes

            $.fn.shiftSelectable = function () {
                var lastChecked,
                    $boxes = this;
                $boxes.click(function (evt) {
                    if (!lastChecked) {
                        lastChecked = this;
                        return;
                    }

                    if (evt.shiftKey) {
                        var start = $boxes.index(this),
                            end = $boxes.index(lastChecked);
                        $boxes.slice(Math.min(start, end), Math.max(start, end) + 1)
                            .attr('checked', lastChecked.checked)
                            .trigger('change');
                    }

                    lastChecked = this;
                });
            };

            $('#dataTable').find('input[name="chkresidents"]').shiftSelectable();
        }
    });
}

Dashboard.LoadWidgets = function () {

    $('.content-body-referral').html("");
    $('.content-body-referral-followup').html("");
    $('.content-body-tour').html("");
    $('.content-body-tour-schedule').html("");
    $('.content-body-assessment').html("");
    $('.content-body-tour-schedule').html(""); 
    $('.content-body-movein').html("");
    $('.content-body-movein-followup').html("");
    $('.m-portlet__head .TotalLeads').html(" <span>Count: </span><span>0</span>");
    $('.m-portlet__head-title .TotalLeadsFollow').html(" <span>Count: </span><span>0</span>");
    $('.m-portlet__head .TotalTour').html(" <span>Count: </span><span>0</span>");
    $('.m-portlet__head-title .TotalTourFollow').html(" <span>Count: </span><span>0</span>");
    $('.m-portlet__head .TotalAsses').html(" <span>Count: </span><span>0</span>");
    $('.m-portlet__head-title .TotalAssesComp').html(" <span>Count: </span><span>0</span>");
    $('.m-portlet__head .TotalMoveIn').html(" <span>Count: </span><span>0</span>");
    $('.m-portlet__head-title .TotalRecent').html(" <span>Count: </span><span>0</span>");

    $([1, 2, 3, 4, 5, 6, 7, 8]).each(function () {
        var type = this;
        var params = {
            func: "referrals",
            type: type
        };
        $('.content-body-lime tbody:eq(0)').html(''); //FOR APPEND TO WORK FOR STATUS 8 AND NINE (ABC)
       
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Referral + "/GetFormData",
            data: JSON.stringify(params),
            dataType: "json",
            success: function (m) {
                var elems = [];

                var cls = 'class="blue m-link no-revert "';

                if (type == 3)

                    cls = 'class="blue-followup m-link no-revert"';

                else if (type == 4)

                    cls = 'class="red m-link "';

                else if (type == 5)

                    cls = 'class="red-followup m-link "';
                else if (type == 6)
                    cls = 'class="purple m-link "';
                else if (type == 7)                    
                    cls = 'class="purple-followup m-link "';                
                else if (type == 8 )  //COMBINED STATUS 8 AND NINE (ABC)                  
                    cls = 'class="lime m-link ';
                else if (type == 1) //CHANGED FROM 9 TO STATUS 1 (ABC)             
                    cls = 'class="lime-followup m-link "';
               
                $(m).each(function () {
                    var regExp = /\(([^)]+)\)/;      
                    var now = new Date();                    
                    var t_cls = cls;
                    var date = regExp.exec(this.name);
                    if (date == undefined || date == null || date == "") {
                        date = "";                       
                    }
                    var onem = new Date();
                    onem.setDate(onem.getDate() - 31);
                    adm_date1 = new Date(date[1]);
                    
                    if (type == 1) {
                      
                        if (adm_date1.getFullYear() == now.getFullYear() && adm_date1 >= onem && now >= adm_date1) {

                            //   if (type == 1 && adm_date.getFullYear() == now.getFullYear() && (adm_date.getMonth()) >= (now.getMonth() - 1)) {                       

                            var recent = $('.TotalRecent span:eq(1)');
                            recent.text(parseInt(recent.text()) + 1);
                            //kim code transfer here so that the date will only filter and proceed to display 02/07/22
                            elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');
                        }
                    }


                    if (type == 2) {

                        //kim add code 01/17/22 add code below for count of every dashboard
                        var lead = $('.TotalLeads span:eq(1)');
                        lead.text(parseInt(lead.text()) + 1);
                        //kim code transfer here so that the date will only filter and proceed to display 02/07/22
                        elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');


                    }
                    if (type == 3) {
                        var leadf = $('.TotalLeadsFollow span:eq(1)');
                        leadf.text(parseInt(leadf.text()) + 1);
                        //kim code transfer here so that the date will only filter and proceed to display 02/07/22
                        elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');


                    }
                    if (type == 4) {
                        var tour = $('.TotalTour span:eq(1)');
                        tour.text(parseInt(tour.text()) + 1);
                        //kim code transfer here so that the date will only filter and proceed to display 02/07/22
                        elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');


                    }
                    if (type == 5) {
                        var tourf = $('.TotalTourFollow span:eq(1)');
                        tourf.text(parseInt(tourf.text()) + 1);
                        elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');


                    }
                    if (type == 6) {            
                        
                        var asses = $('.TotalAsses span:eq(1)');
                        asses.text(parseInt(asses.text()) + 1);
                        //kim code transfer here so that the date will only filter and proceed to display 02/07/22
                        elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');

                    }
                    if (type == 7) {
                        
                        var assesc = $('.TotalAssesComp span:eq(1)');
                        assesc.text(parseInt(assesc.text()) + 1);
                        //kim code transfer here so that the date will only filter and proceed to display 02/07/22
                       // elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');
                          elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');
          
                    }
                    if (type == 8) {
                      
                        var move = $('.TotalMoveIn span:eq(1)');
                        move.text(parseInt(move.text()) + 1);
                        var iscomplete = this.movein_status != null && this.movein_status == 1 ? true : false;

                        if (iscomplete) {
                            t_cls += ' complete "';
                        }
                        else {
                            t_cls += ' incomplete"';
                        }
                        elems.push('<div class="row m-row--no-padding align-items-center"><div class="col"><h5 class="m-widget24__title"><a style="text-transform: capitalize;" resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + ' data-toggle="modal" data-target="#referral_modal">' + this.name.replace(/ *\([^)]*\) */g, "") + '</a></h5></div><div class="col m--align-right"><h5 class="m-widget24__title">' + (date != null ? date[1] : "") + '</h5></div></div>');
                    }

                });

                  //kim 07/14/22
                    if (elems.length != 0) {
                        txtAdmissionDate
                        string = $(elems.join(''));

                    } else {
                        string = '<h5 class="m-widget24__title">NO DATA TO SHOW</h5>';
                    }
                    if (type == 2) {
                        //new leads
                        $('.content-body-referral').html(string);

                    } else if (type == 3) {
                        //new leads followup
                        $('.content-body-referral-followup').html(string);

                    } else if (type == 4) {
                        //tour schedule
                        $('.content-body-tour').html(string);
                    } else if (type == 5) {
                        //tour schedule followup
                        $('.content-body-tour-schedule').html(string);
                    } else if (type == 6) {
                        //assessment completion
                        $('.content-body-assessment').html(string);
                    } else if (type == 7) {                        
                        //assessment completion followup                    
                        $('.content-body-assessment-followup').html(string);
                    } else if (type == 8) {
                        //movein schedule ----> modified(by ABC): MOVEIN FOR SCHEDULE AND FOLLOWUP
                        $('.content-body-movein').append(string);
                    } else if (type == 1) {
                        //movein schedule followup ----> modified(by ABC): RECENT ADMISSIONS
                        $('.content-body-movein-followup').html(string);
                    }
                   
              
                $('.lime-followup').each(function (i, obj) {
                    $(this).attr("data-target", "#admission_modal");
                });



            }
        });
    });
};

function assessmentWizardGoToPage(page) {
    var $current = $("#tabs-" + tab.toString() + 'b .container-display');
    var gotopage = page;
    if (gotopage == pagecount)
        return;
    if (gotopage > 34 || gotopage < 1) {
        $("#pager-no").val(pagecount);
        return;
    } else
        $("#pager-no").val(gotopage);
    var x = 0;
    var tab_col = {
        1: x += $('#tabs-1b .container').length,
        2: x += $('#tabs-2b .container').length,
        3: x += $('#tabs-3b .container').length,
        4: x += $('#tabs-4b .container').length,
        5: x += $('#tabs-5b .container').length,
        6: x += $('#tabs-6b .container').length,
        7: x += $('#tabs-7b .container').length,
    };
    for (var i = 1; i <= 7; i++) {
        if (gotopage <= tab_col[i]) {
            tab = i;
            pagecount = gotopage;
            var $go = $("#tabs-" + tab.toString() + 'b .container').slice(((tab_col[i] - ((i == 1) ? 0 : tab_col[i - 1])) - (tab_col[i] - pagecount)) - 1).slice(0, 1);

            $("#AssessmentForm").tabs("option", "selected", tab - 1);
            $go.addClass('container-display');
            $current.removeClass('container-display');
            return;
        }
    }
}

var assessment = function (dparam, win_type, cs) {

    $("#saveassessment").removeAttr("disabled", true);
    var residentId = dparam.resid;
    var referralId = dparam.refid;

   
    //EXH 2
    $.ajax({
        url: ALC_URI.Admin + "/GetNodeData",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        type: "POST",
        data: (win_type == 'wiz') ? '{id:"assessmentwizard_0"}' : '{id:"assessment_0"}',
        success: function (d) {          
            
            $("#CompletedDate").datepicker({
                formatDate: "MM/dd/yyyy",
                onClose: function () {
                }
            }).on('changeDate', function (selected) {               
                var minDate = new Date(selected.date.valueOf());
               
                $('#AssesMoveInDate').datepicker('setStartDate', minDate);
                $(this).datepicker('hide');
                
                if (moment($("#AssesMoveInDate").val()) < moment($("#CompletedDate").val())) {
                    swal('"Move in Date" should be greather or equal to "Completed Date"');
                    return;
                }             
                    });

          

            var params = {
                func: "assessment_wizard",
                id: residentId
            };

            //EXH 2.1
            $.ajax({
                url: ALC_URI.Admin + "/GetNodeData",
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                type: "POST",
                data: '{id:"morsefallscale_0"}',
                success: function (d) {
                   
                    $("#mfs-container").empty();
                    $("#mfs-container").html(d);
                    $("#MFSHistoryofScoring, #MFSSecondaryDiagnosis, #MFSAmbulatoryAid, #MFSHeparinLock, #MFSGait, #MFSMentalStatus").forceNumericOnly();
                    $("#MFSHistoryofScoring, #MFSSecondaryDiagnosis, #MFSAmbulatoryAid, #MFSHeparinLock, #MFSGait, #MFSMentalStatus").attr('maxlength', '2');

                    $("#MFSHistoryofScoring, #MFSSecondaryDiagnosis, #MFSAmbulatoryAid, #MFSHeparinLock, #MFSGait, #MFSMentalStatus").keyup(function () {
                
                        var val = $(this).val()
                        var id = $(this).attr('id');
                        len = val.length;

                       
                        //added on 11-11-2022 : Cheche : To determine Total Score of Risk dynamically
                        var score = 0;
                        $([id]).each(function(){
                            if(val.length != 0){
                              score = score + parseInt(val);
                            }
                            if(id == "MFSHistoryofScoring"){
                                $("#totalrisk").html(score);
                            }
                            else if(id == "MFSSecondaryDiagnosis"){
                                var res1 = $('#MFSHistoryofScoring').val();
                               $("#totalrisk").html(parseInt(res1) + parseInt(score));
                            }
                            else if(id == "MFSAmbulatoryAid"){
                                 var res1 = $('#MFSHistoryofScoring').val();
                                 var res2 = $('#MFSSecondaryDiagnosis').val();
                                 $("#totalrisk").html(parseInt(score) + parseInt(res1) +  parseInt(res2));
                            }
                            else if(id == "MFSHeparinLock"){
                                 var res1 = $('#MFSHistoryofScoring').val();
                                 var res2 = $('#MFSSecondaryDiagnosis').val();
                                 var res3 = $('#MFSAmbulatoryAid').val()
                                 $("#totalrisk").html(parseInt(score) + parseInt(res1) +  parseInt(res2) + parseInt(res3));
                            }
                             else if(id == "MFSGait"){
                                 var res1 = $('#MFSHistoryofScoring').val();
                                 var res2 = $('#MFSSecondaryDiagnosis').val();
                                 var res3 = $('#MFSAmbulatoryAid').val()
                                 var res4 = $('#MFSHeparinLock').val();
                                 $("#totalrisk").html(parseInt(score) + parseInt(res1) +  parseInt(res2) + parseInt(res3) + parseInt(res4));
                            }
                             else if(id == "MFSMentalStatus"){
                                  var res1 = $('#MFSHistoryofScoring').val();
                                 var res2 = $('#MFSSecondaryDiagnosis').val();
                                 var res3 = $('#MFSAmbulatoryAid').val()
                                 var res4 = $('#MFSHeparinLock').val();
                                 var res5 = $('#MFSGait').val();
                                 $("#totalrisk").html(parseInt(score) + parseInt(res1) +  parseInt(res2) + parseInt(res3) + parseInt(res4) + parseInt(res5));
                            }
                        })
                       
                        //ended here
                       
                      
                        if (id == "MFSHistoryofScoring") {
                            if (len == 2) {
                                if (val > 25) {
                                    $('#MFSHistoryofScoring').val("")
                                    swal("Score out of scale!");
                                }
                            }
                        } else if (id == "MFSSecondaryDiagnosis") {
                            if (len == 2) {
                                if (val > 15) {
                                    $('#MFSSecondaryDiagnosis').val("")
                                    swal("Score out of scale!");
                                }
                            }
                        } else if (id == "MFSAmbulatoryAid") {
                            if (len == 2) {
                                if (val > 30) {
                                    $('#MFSAmbulatoryAid').val("")
                                    swal("Score out of scale!");
                                }
                            }
                        } else if (id == "MFSHeparinLock") {
                            if (len == 2) {
                                if (val > 20) {
                                    $('#MFSHeparinLock').val("")
                                    swal("Score out of scale!");
                                }
                            }
                        } else if (id == "MFSGait") {
                            if (len == 2) {
                                if (val > 20) {
                                    $('#MFSGait').val("")
                                    swal("Score out of scale!");
                                }
                            }
                        } else if (id == "MFSMentalStatus") {
                            if (len == 2) {
                                if (val > 15) {
                                    $('#MFSMentalStatus').val("")
                                    swal("Score out of scale!");
                                }
                            }
                        }


                    });

                },
                complete: function () { },
                error: function () { }
            });

            //EXH 2.2
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Marketer + "/GetFormData",
                data: JSON.stringify(params),
                dataType: "json",
                async: false,
                success: function (m) {
                    
                    if (m.Result == 1)
                    {                      
                      
                        $("#FirstName").val(m.Data.FirstName);
                        $("#LastName").val(m.Data.LastName);
                        $("#assessment_modal #MiddleInitial").val(m.Data.MiddleInitial);
                        $("#MiddleInitial").val(m.Data.MiddleInitial);
                        $("#label-TotalScore").html("0");
                        var info = $.cookie('ALCInfo');
                        if (info != '') {
                            var ins = info.split('|');
                            var name = '';
                            if (ins.length > 1) {
                                name = ins[1];
                            }
                            $("#CompletedBy").val(capsFirstLetter(name));
                        }

                        $("#AssessmentId").val('');  //added this for when you previously opened a resident with assessment, assessmentId is being copied to assessment form of a resident with no assessment (alona 11-24-22)
                    }
                                     
                    else {
                        //$(".form").serializeArray().map(function (x) { m[x.name] = x.value; },'id');
                        $(".form").mapJson(m.Data, 'id');
                        $("#label-TotalScore").html(m.Data.TotalScore);
                      //  console.log(m.Data.TotalScore);
                        var a = $("#AssesMoveInDate").val();
                        if (a != null) {
                            $("#AssesMoveInDate").val("");
                        }
                                             
                    }
                   

                    $("#AssResidentId").val(residentId);
                    $("#AssReferralId").val(referralId);
                    for (var ps in m.Points) {

                        $("#" + ps).val(m.Points[ps]);
                        //$($("#" + ps).parents().children()[1]).html(m.Points[ps])
                        $($("#" + ps).next().html(m.Points[ps] + " pt"));
                    }
                    if (win_type == 'wiz') {

                        tab = 1;
                        pagecount = 1;
                        var $foot = $('<div style="padding-top:5px;float:left;"> <label><strong>Total Score: <span id="label-TotalScore">0</span></strong> </label></div> ' +
                            '<div style="padding:0;"> <center> <input class="navi-button" id="ass-wiz-prev" type="button" value="<<" /> ' +
                            ' <span><label id="pager" style="float:none;">Page  <input type="number" id="pager-no" min ="1" max="34"  /> of 34</label></span> <input class="navi-button" id="ass-wiz-next" type="button" value=">>" /> </center> </div>');

                        $('.ui-dialog-buttonpane').append($foot);
                        $("#label-TotalScore").text(m.Data.TotalScore);
                        //loadPager();

                    }
                    if (!$("#AssessmentId").val()) {
                       
                        $('#AssessmentPrint').prop("disabled", true);
                    } else {
                        $('#AssessmentPrint').prop("disabled", false);
                    }

                    $("#addNewAssessment").hide();

                    $("#content-assessment-tab #CompletedDate").val("");
                    $("#content-assessment input").removeAttr("disabled");

                    $("#assessmentlist_container").hide();

                    marketerAssState.orig = JSON.stringify($('#assessment_modal .form').extractJson('id'));

                }
            });

        },
        complete: function () { },
        error: function () { }
    });
    return false;
};


//kim for allergy 11/15/22
function getalle(array) {
    const input = document.querySelector('.tag-container input');

    function createTag(label) {
        const divs = document.createElement('div');
        divs.setAttribute('class', 'tag');
        const span = document.createElement('span');
        span.innerHTML = label;
        const closeBtn = document.createElement('c');
        closeBtn.setAttribute('class', label);
        closeBtn.innerHTML = 'x';
        divs.appendChild(span);
        divs.appendChild(closeBtn);
        return divs;
    }

        function reset() {

            document.querySelectorAll('.tag').forEach(function (tag) {
                tag.parentElement.removeChild(tag);
            })
        }

        function addTag() {
            const tagContainer = document.querySelector('.tag-container');
            reset();
            tags.slice().reverse().forEach(function (tag) {

                if (tag != " ") {
                    const input = createTag(tag);
                    tagContainer.prepend(input);
                }
            })
        }
        if (array != undefined) {
            tags.push(array);
            addTag();
            input.value = '';
        }

        input.addEventListener('keyup', function (e) {

            if (e.key == ' ') {
                if
                (input.value.length != 0) {
                    tags.push(input.value);
                    addTag();
                    input.value = '';
                }

            }
        })
        document.addEventListener('click', function (e) {
            if (e.target.tagName == 'C') {

                const value = e.target.getAttribute('class');
                const index = tags.indexOf(value);
                if (index >= 0) {
                    tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
                    addTag();
                }

            }
        })
        return
    
}

//kim for allergy 11/15/22
function getallergy() {
    $.ajax({
        url: "Admin/Search",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        data: '{func: "search_allergy"}',
        success: function (d) {
            document.getElementById('Allergies').innerHTML = "";
            //   var tagInputEle = $('#txtAllergy');
            //   tagInputEle.tagsinput();
            for (let i = 0; i < d.length; i++) {
                if (d[i].allergy_name != null) {
                    var option = "<option  id='" + d[i].allergy_id + "' value='" + d[i].allergy_name + "'></option>"
                    document.getElementById('Allergies').innerHTML += option;


                }


            }

        }
    });
}

function getAge(birthday) {
    if (birthday == '') return '';
    var b = moment(birthday)._d;
    var ageDifMs = Date.now() - b.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}


var admission = function (d, hasmoveindate) {
    tags = [];
    array = [];
    var residentId = d.resid;

    var referralId = d.refid;
    var _date = d.date;
    var id = 0;

    var roleID = d.roleID;
    selectedTabIndex = 0;

    var $canceldialog;

    $.ajax({
        url: ALC_URI.Admin + "/GetNodeData",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        type: "POST",
        data: '{id:"Admission_0"}',
        success: function (d) {
            $("#admission_content").empty();
            $("#admission_content").html(d);

            $("#admForm").tabs();
            $("#admForm").tabs("option", "selected", 0);
            //kim for allergy 11/15/22
            getallergy();
            getalle();
            //$("#admForm").tabs('option', 'disabled', [1, 2, 3, 4,5,6,7,8,9,10,11,12,13,14,15]);
            //var diag = $('.ui-dialog-content');
            //diag = diag.find('#admForm').parents('.ui-dialog-content');
            //var height = 0;
            //height = diag.eq(0).height() || 0;
            //if (height <= 0)
            //    height = diag.eq(1).height() || 0;
            //$("#AdmissionContainer,#admForm").height(height);
            //$('#AdmissionContent').height(height - 20);
            //$('#tabs-1a,#tabs-2a,#tabs-3a,#tabs-4a,#tabs-5a,#tabs-7a,#tabs-8a,#tabs-9a,#tabs-10a,#tabs-11a,#tabs-12a,#tabs-13a,#tabs-14a,#tabs-15a').height(height - 90);
            //resizeAdmForm();
            //$("#load_grid").show();
            setHeight();
           
            $("input:checkbox").on('click', function () {
                
                // in the handler, 'this' refers to the box clicked on
                var $box = $(this);
                if ($box.is(":checked")) {
                    
                    // the name of the box is retrieved using the .attr() method
                    // as it is assumed and expected to be immutable
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    // the checked state of the group/box on the other hand will change
                    // and the current value is retrieved using .prop() method
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                } else {
                    $box.prop("checked", false);
                }
            });
            $('#txtHospitalPhone,#txtPhysicianPhone,#txtPhysicianFax,#txtPharmacyPhone').mask("(999)999-9999");
            $(".content-datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            }).on('changeDate', function () {
                $(this).datepicker('hide');
            });
            $("#txtDischargeDate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            }).on('changeDate', function () {
                $(this).datepicker('hide');
            });
            //("option", "dateFormat", "mm/dd/yy");
            //var SearchText = "Search resident name...";
            //$("#txtResidentName").val(SearchText).blur(function (e) {
            //    if ($(this).val().length == 0)
            //        $(this).val(SearchText);
            //}).focus(function (e) {
            //    if ($(this).val() == SearchText)
            //        $(this).val("");
            //}).autocomplete({
            //    source: function (request, response) {
            //        $.ajax({
            //            url: ALC_URI.Admin + "/Search",
            //            contentType: "application/json; charset=utf-8",
            //            dataType: "json",
            //            type: "POST",
            //            data: '{func: "search_resident", name:"' + request.term + '"}',
            //            success: function (data) {
            //                response($.map(data, function (item) {
            //                    return {
            //                        label: item.name,
            //                        value: item.id
            //                    }
            //                }));
            //            }
            //        });
            //    },
            //    delay: 200,
            //    minLength: 2,
            //    focus: function (event, ui) {
            //        $("#txtResidentName").val(ui.item.label);
            //        $('#txtResidentId').val(ui.item.value);
            //        return false;
            //    },
            //    select: function (event, ui) {
            //        return false;
            //    }
            //}).data("ui-autocomplete")._renderItem = function (ul, item) {
            //    return $("<li></li>")
            //            .data("item.autocomplete", item)
            //            .append("<a>" + item.label + "</a>")
            //            .appendTo(ul);
            //};
           
            $.each(_FR.Rooms, function (index, value) {                
                $('#room_facility').append($('<option>', {
                    value: 'room_' + value.Id.toString(),
                    text: value.Name
                }));
            });

            $('#room_facility').change(function () {
                
                $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#room_facility option:selected').text());
            });

            //DISABLES THE OTHER TABS
            //$('#admForm').find('li:eq(1) a, li:eq(2) a, li:eq(3) a, li:eq(4) a, li:eq(5) a, li:eq(6) a, li:eq(7) a, li:eq(8) a, li:eq(9) a, li:eq(10) a, li:eq(11) a, li:eq(12) a, li:eq(13) a, li:eq(14) a, li:eq(15) a').addClass('disabled');
            //$("#admForm").find("li a").not(":eq(0)").addClass('disabled');
            $("#admForm").find("li a").not(":eq(0)").hide();
            
            var data = {
                func: "resident_admission_marketer",
                id: residentId
            };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Admin + "/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    
                    //var result = $.extend(m.obj1, m.obj2, m.obj3);     
                    //$("#PhysicianSignatureDate, #CEMTDateForSignField, #IEDateOfBirthField, #IEDateAdmittedToFacility, #IEDateLeft, #IEDateField, #releaseDate, #authExpiration, #residentBirthDateField, #authorizationDateCompletedField, #diagExamDateField, #diagDateTBTestGivenField,#diagDateTBTestReadField, #bedriddenEstimatedDateIllnessField").datepicker({
                    //    formatDate: "MM/dd/yyyy"
                    //});

                    //$("#IEPlacementAgencyTelephone, #IETelephoneOfNearestRelative, #IEPersonResponsibleForFinancialAffairsOneTelephone, #IEPersonResponsibleForFinancialAffairsTwoTelephone, #IEPersonResponsibleForFinancialAffairsThreeTelephone, #facilityTelephoneField,#physicianPhoneField,#CEMTWorkPhoneField,#CEMTHomePhoneField,#PERLAareatel").mask("(999)999-9999");
                    //$("#IEOtherPersonToBeNotifiedInEmergencyPhysicianTelephone, #IEOtherPersonToBeNotifiedInEmergencyMentalHealthProviderTelephone, #IEOtherPersonToBeNotifiedInEmergencyDentistTelephone, #IEOtherPersonToBeNotifiedInEmergencyRelativeTelephone, #IEOtherPersonToBeNotifiedInEmergencyFriendTelephone, #IEReligiousAdvisorTelephone").mask("(999)999-9999");
                    //$("#appDateComp, #ARdateComp, #LdateComp, #TBdate, #TDNclientSignatureDate, #TDNauthrepSignatureDate, #TDNfacilityAuthRepSignatureDate").datepicker({
                    //    formatDate: "MM/dd/yyyy"
                    //});


                    $(".form").mapJson(m, 'id');
                    if (m.txtAdmittedBy == null) {
                        $("#txtAdmittedBy").val($("#usrname").text())
                    }
                    id = m.txtAdmissionId;
                    mode = $.isNumeric(id) ? 2 : 1;
                    
                    if (m.room_facility == null) {
                        $("#room_facility").val("");
                    }


                    var al = $("#txtAllergy").val();
                    if (al != null) {

                        var aname = al.split(',');
                        array = aname.filter(item => item);
                        var len = array.length - 1;
                        for (var i = 0; i <= len; i++) {

                            getalle(array[i]);
                        }
                    }
                    else {
                        tags = [];
                        array = [];
                        $('.tag-container').find('.tag').remove()
                    }

                   

                    //if (mode == 2)
                    //$("#admForm").tabs('option', 'disabled', []);
                    //$('#txtResidentName').focus();

                    var tabindex = $("#admForm").tabs('option', 'selected');

                    if ($("#txtAdmissionDate").val() != "") {
                        $("#txtAdmissionDate").css("background-color", "white");
                    } else {
                        $("#txtAdmissionDate").css("background-color", "#FFFF99");
                    }

                    //if ($('#ANSAppraisalID').val() == '') {
                    //    $('#ANSAppraisalDate').val(ALC_Util.CurrentDate).show();
                    //    $('#SelectAppraisalDate').hide();
                    //    $('#btnNew').hide();
                    //} else {
                    //    //load selection appraisal dates    
                    //    $('#ANSAppraisalDate').hide();
                    //    $('#btnNew').show();
                    //    $('#SelectAppraisalDate').html('');
                    //    if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                    //        return;
                    //    $.get(ALC_URI.Admin + "/GetAppraisalNSDates?id=" + $('#txtAdmissionId').val(), function (data) {
                    //        $.each(data, function () {
                    //            $('#SelectAppraisalDate').append($('<option>', {
                    //                value: this.id,
                    //                text: this.date
                    //            }));
                    //        });
                    //        $('#SelectAppraisalDate').val($('#ANSAppraisalID').val()).show();
                    //        $('#SelectAppraisalDate').off('change').on('change', function () {
                    //            $('#ANSAppraisalID').val($(this).val());
                    //            $.get(ALC_URI.Admin + "/GetAppraisalNS?id=" + $(this).val(), function (dt) {
                    //                $('#tabs-3a section').clearFields();
                    //                $('#ANSResidentName').val(name);
                    //                //$('#ANSDOB').val(result.ANSDOB);
                    //                //if ($('#ANSAge').val() == "") {
                    //                //    var age = getAge(result.ANSDOB);
                    //                //    $('#ANSAge,#ANSAge2').val(age);
                    //                //}

                    //                //$('#ANSAge2').val($('#ANSAge').val());
                    //                $('#tabs-3a section').mapJson(dt, 'id');
                    //            });
                    //        });
                    //        $('#btnNew').off('click').on('click', function () {
                    //            showdiagNewAppraisal();
                    //        });
                    //    });
                    //}

                    //if ($('#AAAgreementID').val() == '') {
                    //    $('#AAAgreementDate').val(ALC_Util.CurrentDate).show();
                    //    $('#SelectAgreementDate').hide();
                    //    $('#btnNewAgr').hide();
                    //} else {
                    //    //load selection appraisal dates    
                    //    $('#AAAgreementDate').hide();
                    //    $('#btnNewAgr').show();
                    //    $('#SelectAgreementDate').html('');
                    //    if ($('#txtAdmissionId').val() == '' || $('#txtAdmissionId').val() == undefined)
                    //        return;
                    //    $.get(ALC_URI.Admin + "/GetAgreementDates?id=" + $('#txtAdmissionId').val(), function (data) {
                    //        $.each(data, function () {
                    //            $('#SelectAgreementDate').append($('<option>', {
                    //                value: this.id,
                    //                text: this.date
                    //            }));
                    //        });
                    //        $('#SelectAgreementDate').val($('#AAAgreementID').val()).show();
                    //        $('#SelectAgreementDate').off('change').on('change', function () {
                    //            $('#AAAgreementID').val($(this).val());
                    //            $.get(ALC_URI.Admin + "/GetAgreement?id=" + $(this).val(), function (dt) {
                    //                $('#tabs-4a section').clearFields();
                    //                $('#AAResidentName').val(name);
                    //                $('#AADOB').val($('#txtBirthdate').val());
                    //                $('#AARoomNo').val($('#DRoomId option:selected').text());
                    //                $('#AASSNo').val($('#txtSSN').val());
                    //                $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                    //                $('#tabs-4a section').mapJson(dt, 'id');
                    //            });
                    //        });
                    //        $('#btnNewAgr').off('click').on('click', function () {
                    //            showdiagNewAgreement();
                    //        });
                    //    });
                    //}


                    //$('#ANSDOB').val(result.ANSDOB);
                    //$('#ANSAge,#residentAgeField,#applicantage,#IEAgeField,#RESAPPapplicantage').val(getAge(result.ANSDOB));
                    //$('#AADOB,#ANSDOB,#txtBirthdate').off('change').on('change', function () {
                    //    var id = this.id;
                    //    if (id == 'AADOB') {
                    //        $('#ANSDOB,#txtBirthdate,#residentBirthDateField').val(this.value);
                    //    } else if (id == 'ANSDOB') {
                    //        $('#AADOB,#txtBirthdate,#residentBirthDateField').val(this.value);
                    //    } else if (id == 'txtBirthdate') {
                    //        $('#AADOB,#ANSDOB,#residentBirthDateField').val(this.value);
                    //    }

                    //    var age = getAge(this.value);
                    //    //$('#ANSAge').val(age);
                    //    $('#ANSAge,#ANSAge2,#residentAgeField,#applicantage,#IEAgeField,#RESAPPapplicantage').val(age);
                    //});

                    //if (tabindex == 0) {
                    //    $('#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val($('#txtResidentName').val());
                    //    $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                    //    $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#room_facility option:selected').text());

                    //} else if (tabindex == 2) {
                    //    $('#AADOB').val($('#ANSDOB').val());
                    //    $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#room_facility option:selected').text());
                    //    ('#ANSResidentName').focus();
                    //    //  alert($('#ANSDOB').val());

                    //} else if (tabindex == 3) {
                    //    $('#AADOB').val($('#ANSDOB').val());
                    //}

                    //$("#load_grid").hide();

                    //if ($('#ANSAppraisalID').val() == '') {
                    //    $('#ANSAppraisalDate').val(ALC_Util.CurrentDate).show();
                    //} else {
                    //    //load selection appraisal dates
                    //    $.get(ALC_URI.Admin + "/GetAppraisalNSDates?id=" + id, function (data) {
                    //        $.each(data, function () {
                    //            $('#SelectAppraisalDate').append($('<option>', { value: this.id, text: this.date }));
                    //        });
                    //        $('#SelectAppraisalDate').val($('#ANSAppraisalID').val()).show();
                    //    });

                    //}
                    //if ($('#AAAgreementID').val() == '') {
                    //    $('#AAAgreementDate').val(ALC_Util.CurrentDate).show();
                    //} else {
                    //    $.get(ALC_URI.Admin + "/GetAgreementDates?id=" + $('#txtAdmissionId').val(), function (data) {
                    //        $.each(data, function () {
                    //            $('#SelectAgreementDate').append($('<option>', { value: this.id, text: this.date }));
                    //        });
                    //        $('#SelectAgreementDate').val($('#AAAgreementID').val()).show();
                    //    });
                    //}



                }

            });


        }
    });

    return false;
};


var ShowUPSDiag = function (en, cs) {
    var x = {
        "blue": 2,
        "blue-followup": 3,
        "red": 4,
        "red-followup": 5,
        "purple": 6,
        "purple-followup": 7,
        "lime": 8,
        "lime-followup": 9
    };

    //$(document).on("click", "#save_reverted_status", function () {

    //MOVED THE SAVE FUNCTION TO "SaveStatus()"
    //var $sdialog = $("<div><div id=\"diag\" ></div></div>").dialog({
    //    modal: true,
    //    closeOnEscape: false,
    //    title: "Revert Status",
    //    width: 400,
    //    height: 120,
    //    close: function () {
    //        $sdialog.dialog("destroy").remove();
    //    },
    //    buttons: {
    //        "Save": {
    //            click: function () {
    //                var data = {
    //                    func: "updatestatus"
    //                };
    //                var isValid = $('#txtStatus').val() != "0";

    //                if (!isValid) return;
    //                data.data = {
    //                    residentId: en.resid,
    //                    referralId: en.refid,
    //                    status: $('#txtStatus').val()
    //                };
    //                $.ajax({
    //                    type: "POST",
    //                    contentType: "application/json; charset=utf-8",
    //                    url: ALC_URI.Referral + "/PostData",
    //                    data: JSON.stringify(data),
    //                    dataType: "json",
    //                    success: function (m) {
    //                        if (m.Result == 0) {
    //                            $.growlSuccess({
    //                                message: "Status updated successfully!",
    //                                delay: 6000
    //                            });

    //                            Dashboard.LoadWidgets();
    //                            $sdialog.dialog("destroy").remove();
    //                        }
    //                    }
    //                });
    //                return false;
    //            },
    //            class: "primaryBtn",
    //            text: "Save"
    //        },
    //        "Cancel": function () {
    //            $sdialog.dialog("destroy").remove();
    //        }
    //    },
    //    resize: function (event, ui) { }
    //});

    //$("#diag").empty();
    //$('#diag').html("<div style='overflow:hidden!important;padding:02px;margin:30px'><fieldset><ul><li><label style='float:none;'>Please select previous status</label><select name='Status'id='txtStatus'class='right'style='margin-top:-5px!important'></select></li></ul></fieldset></div>");
    $('#txtStatus').find('option').remove().end();
    //$('#txtStatus').append($("<option></option>").attr("value", 0).text("[Select Status]").selected().disabled);
    $('#txtStatus').append($("<option></option>").attr("value", 3).text("Referral - Follow Up"));
    $('#txtStatus').append($("<option></option>").attr("value", 4).text("Tour - Schedule"));
    $('#txtStatus').append($("<option></option>").attr("value", 6).text("Assessment - Schedule"));
    $('#txtStatus option').each(function () {
        currentItem = parseInt($(this).attr("value"), 10);
        if (currentItem >= x[cs]) {
            $(this).remove();
        }
    });
}


$(document).ready(function () {

    var scroll1 = new PerfectScrollbar('#scroll_1');
    var scroll2 = new PerfectScrollbar('#scroll_2');
    var scroll3 = new PerfectScrollbar('#scroll_3');
    var scroll4 = new PerfectScrollbar('#scroll_4');
    var scroll5 = new PerfectScrollbar('#scroll_5');
    var scroll6 = new PerfectScrollbar('#scroll_6');
    var scroll7 = new PerfectScrollbar('#scroll_7');
    var scroll8 = new PerfectScrollbar('#scroll_8');
    var scroll9 = new PerfectScrollbar('#referral_modal .modal-body');
    var scroll10 = new PerfectScrollbar('#assessment_modal #content-assessment');
    var scroll11 = new PerfectScrollbar('#admission_modal .adm-content');

   
    $('.btn[data-toggle=modal]').on('click', function () {
        var $btn = $(this);
        var currentDialog = $btn.closest('.modal-dialog'),
            targetDialog = $($btn.attr('data-target'));;
        if (!currentDialog.length)
            return;
        targetDialog.data('previous-dialog', currentDialog);
        currentDialog.addClass('aside');
        var stackedDialogCount = $('.modal.in .modal-dialog.aside').length;
        if (stackedDialogCount <= 5) {
            currentDialog.addClass('aside-' + stackedDialogCount);
        }
    });

    $('.modal').on('hide.bs.modal', function () {
        var $dialog = $(this);
        var previousDialog = $dialog.data('previous-dialog');
        if (previousDialog) {
            previousDialog.removeClass('aside');
            $dialog.data('previous-dialog', undefined);
        }
    });


    Dashboard.BuildPendingAdmission = buildPendingAdmission;
    buildPendingAdmission();

    $('#dataTable tbody').on('click', 'a', function () {
        var dis = $(this);
        var rid = parseInt(dis.attr('id'));
        var tidx = parseInt(dis.attr('tab'));
        if (isNaN(tidx))
            tidx = 1;
        var cltidx = tidx - 1;
        $('#preAdmissionLink').trigger('click');
        var interval = setInterval(function () {
            var tab = $("#PlacementForm");
            if (tab[0]) {
                clearInterval(interval);
                tab.tabs('option', 'disabled', []);
                tab.tabs('option', 'selected', cltidx);

                var data = {
                    func: "preplacement_wizard",
                    id: rid.toString()
                };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: ALC_URI.Admin + "/GetFormData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        res.residentIdField = rid;
                        $(".form").mapJson(res, 'id');
                    }
                });
            }
        });
    });

    $(document).on("click", "#admitResidents", function () {
        // $('#admitResidents').click(function () {
        var values = [];
        $.each($('#dataTable').find('input[name="chkresidents"]'), function () {
            if ($(this).is(':checked'))
                values.push($(this).val());
        });
        var ids = values.join(',');
        if (values.length <= 0) {
            alert("Please select resident(s) to admit first and try again.");
            return;
        }
        if (!confirm("Are you sure you want to admit selected resident(s)?")) return;

        var data = {
            Func: 'dashboard_pending_admission',
            Data: ids
        };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Home + "/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.Result == 0) {
                    swal(m.Message + " new resident(s) has been admitted successfully!", "", "success")
                    buildPendingAdmission();
                }
            }
        });

    });

    var resize = function () {
        var wH = $(window).height() - 137;
        $("#Home .box-left").height(wH - 50);
        $("#Home table.box").width($(window).width() - 20);
        //$('#divdataTable').height($('#dashTabsContent').height() - 54);

        var bh = $('#Home .box-left').height();

        $('.content-body-blue,.content-body-red,.content-body-purple,.content-body-lime').height((bh / 2) - 72);
        //$('.content-body-lime').height(bh - 70);
    };
    resize();
    resize();
    $(window).resize(resize);


    Dashboard.LoadWidgets();

    var tab = 1;
    var pagecount = 1;

    //assessment here

    $(document).on('click', '#ass-wiz-next', function (e) {
        var $current = $("#tabs-" + tab.toString() + 'b .container-display');
        var $next = $("#tabs-" + tab.toString() + 'b .container-display').next('.container');
        if ($next.length == 0) {
            if (tab == 7)
                return;
            tab++;
            $next = $("#tabs-" + tab.toString() + 'b .container').first('.container');
        }
        $("#AssessmentForm").tabs("option", "selected", tab - 1);
        $next.addClass('container-display');
        $current.removeClass('container-display');
        pagecount++;
        loadPager();
    });
    $(document).on('click', '#ass-wiz-prev', function (e) {
        var $current = $("#tabs-" + tab.toString() + 'b .container-display');
        var $prev = $("#tabs-" + tab.toString() + 'b .container-display').prev('.container');
        if ($prev.length == 0) {
            if (tab == 1)
                return;
            tab--;
            $prev = $("#tabs-" + tab.toString() + 'b .container').last('.container');
        }
        $("#AssessmentForm").tabs("option", "selected", tab - 1);
        $prev.addClass('container-display');
        $current.removeClass('container-display');
        pagecount--;
        loadPager();
    });

    var loadPager = function () {
        //var pagesize = $('.container').length; 56
        $("#pager-no").val(pagecount);
    }



    $(document).on('click', '#assessmentTreeView li.ass-sub-node a', function (e) {
        var i = $(this).attr('page');
        assessmentWizardGoToPage(i);

    });
    $(document).on('keypress', '#pager-no', function (e) {
        if (e.which == 13) {
            assessmentWizardGoToPage($("#pager-no").val());
        }
    });

    $("#newleads").on("click", function () {

        $("#referral_modal form").trigger("reset");
        $("#referral_modal form").clearFields();
        $('#referral_modal input:checkbox').removeAttr('checked');
        setTimeout(function () {
            $("#addcalllog").css("display", "none");

        }, 500)
    });

    //Added on 3-31-2022 : Cheche 
    function getrefsource() {

        $.ajax({
            url: "Admin/Search",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: '{func: "search_reference"}',
            success: function (d) {
          
                for (let i = 0; i < d.length; i++) {

                    var option = "<option value='" + d[i].reference + "'></option>"
                    document.getElementById('RefSources').innerHTML += option;
                }
                //response($.map(d, function (item) {// not needed : 4-4-2022 changed data to d
                //    debugger
                //        return {
                //            label: item.reference,
                //            value: item.id
                //        }
                //}));
            }
        });
    }

    getrefsource();
    //ends here


    $('#Home').on('click', 'a', function () {
       // debugger
        $(".hiddencal").hide();
        asessment_params = {
            resid: $(this).attr('resid'),
            refid: $(this).attr('id'),
            date: ($(this).text().split('(')[1]) ? $(this).text().split('(')[1].slice(0, -1) : null, //change this
            roleID: $("#UsrRoleID").val()
        };

      

        var dis = $(this);
        css = dis.attr('class');
        var rid = dis.attr('id');

        if ($(this).hasClass('purple')) {
            $("#assessment_modal").modal("toggle");
            assessment(asessment_params, 'wiz', css.split(' ')[0]);
            return false;
        }
        if ($(this).hasClass('lime-followup')) {
            admission(asessment_params);
            return;
        }

        //referral and referral-follow up
        if ($(this).hasClass("no-revert")) { $("#revert_status_div").hide(); } else { $("#revert_status_div").show(); }

        if ($(this).hasClass("blue") || $(this).hasClass("blue-followup")) {
            $(".hiddencal").hide();
            $("#planned_tourdate").show();

        } else {
            setTimeout(function () {
                $("#addcalllog").css("display", "none");

            }, 500)
        }
        //tour
        if ($(this).hasClass("red")) {
            $(".hiddencal").hide();
            $("#planned_tourdate").show();
            $("#completed_tourdate").show();
        }

        //complete tour
        if ($(this).hasClass("red-followup")) {
            $(".hiddencal").hide();
            $("#assessment_date").show();
        }

        //completed assessment
        if ($(this).hasClass("purple-followup")) {
            $(".hiddencal").hide();
            $("#movein_date").show();
        }

        //move in
        if ($(this).hasClass("lime")) {
            $(".hiddencal").hide();
            $("#showadmissionform").show();
        }
        var $canceldialog;

        //CHANGED THIS TO A MODAL. 
        //var $dialog = $("<div><div id=\"referral_form\" ></div></div>").dialog({
        //    modal: true,
        //    closeOnEscape: false,
        //    title: "Referral Form",
        //    width: $(window).width() - 300,
        //    height: $(window).height() - 100,
        //    close: function () {
        //        $dialog.dialog("destroy").remove();
        //    },
        //    beforeClose: function (e, ui) {
        //        $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
        //            modal: true,
        //            title: "Confirmation",
        //            width: 400,
        //            buttons: {
        //                "Yes": function () {
        //                    $canceldialog.dialog("destroy").remove();
        //                    $dialog.dialog("destroy").remove();
        //                    //window.location = ClientUrls["Home"];
        //                },
        //                "No": function () {
        //                    $canceldialog.dialog("destroy").remove();
        //                }
        //            }
        //        });
        //        return false;
        //    },
        //    buttons: {
        //        "Save": {
        //            click: function () {
        //                $('#btnSave').trigger('click');
        //                Dashboard.HideDialog = function () {
        //                    $dialog.dialog("destroy").remove();
        //                }
        //            },
        //            class: "primaryBtn",
        //            text: "Save"
        //        },
        //        "Cancel": function () {
        //            $dialog.dialog("destroy").remove();
        //        }
        //    },
        //    resize: function (event, ui) {

        //    }
        //});

        //if (target.attr('id') == 'btnCreate')
        //    $('#Home').clearFields();

        $.get(ALC_URI.Marketer + "?diag=1", function (data) {
           // debugger
            /* EXH 1*/
            /* START: EXH 1*/
            var params = {
                func: "getform",
                id: rid
            };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Referral + "/GetFormData",
                data: JSON.stringify(params),
                dataType: "json",
                success: function (m) {
                
                    if (m.message == "Error") {
                        swal(m.message + "", "", "warning");
                        return;
                    }
                    //console.log(m);
                    $('#referral_form').mapJson(m, 'id');


                    //if (m.SpecNeed != "") {
                    //    // add false in   $("#cbspecneed").prop("checked", false); kim 11/19/2021
                    //    $("#cbspecneed").prop("checked", false);  
                    //}

                    // changed by Cheche 11/29/2021 
                    if (m.SpecNeed != "" && m.SpecNeed != undefined) {
                        $("#cbspecneed").prop("checked", true);
                    }

                    $("#CallLog").attr("rid", rid);
                    $("#txtSSN").mask("999-99-9999");
                    $('#ContactPersonPhone,#ResidentPhone').mask("(999)999-9999");
                       //kim modified to app time 04/28/22   
                        var cate = $("#spCT").text();
                        var a = moment(cate, "ddd, MMM DD YYYYY HH:mmA").format('MM/DD/YYYY');
                        var cdate = new Date(a);
                    if (cdate != null )
                    {
                        
                        var ptd = $("#PlannedTourDate").val();
                        var td = $("#TouredDate").val();
                        var asd = $("#AssessmentStartDate").val();                        
                        var acd = m.AssessmentCompletedDate;       
                       
                       
                        $("#btnSave").show();
                        var minDate = new Date(cdate);
                        var ptdminDate = new Date(ptd);
                        var tdminDate = new Date(td);
                        var asdminDate = new Date(asd);
                        var cdminDate = new Date(acd);

                        $('#PlannedTourDate').datepicker('setStartDate', minDate);
                        $('#TouredDate').datepicker('setStartDate', ptdminDate);
                        $('#AssessmentStartDate').datepicker('setStartDate', tdminDate);
                        $('#MoveInDate').datepicker('setStartDate', cdminDate);                      
                    }    
                    
                   

                    $("#Rbirthday, #TouredDate, #AssessmentStartDate, #PlannedTourDate, #MoveInDate, #CompletedDate, #AssesMoveInDate").datepicker().on('changeDate', function () {
                    //    $(this).datepicker('hide');                      
                        
                        // added by Cheche 11/23/2021 Past and Future Dates Restrictions


                    });



                    if ($("#ResidentId").val() != "") {
                        $("#addcalllog").show();
                    }

                    if ($("#PlannedTourDate").val() != "") {
                        $("#PlannedTourDate").css("background-color", "white");
                    } else {
                        $("#PlannedTourDate").css("background-color", "#FFFF99");
                    }

                    if ($("#TouredDate").val() != "") {
                        $("#TouredDate").css("background-color", "white");
                    } else {
                        $("#TouredDate").css("background-color", "#FFFF99");
                    }

                    if ($("#MoveInDate").val() != "") {
                        $("#MoveInDate").css("background-color", "white");
                    } else {
                        $("#MoveInDate").css("background-color", "#FFFF99");
                    }

                    if ($("#AssessmentStartDate").val() != "") {
                        $("#AssessmentStartDate").css("background-color", "white");
                    } else {
                        $("#AssessmentStartDate").css("background-color", "#FFFF99");
                    }
                

                    /* EXH 1.A*/

                    /* START: EXH 1.A*/

                    //$('#btnShowAssessment').click(function () {
                    //    assessment(asessment_params, 'for', css.split(' ')[0])
                    //    return;
                    //});
                    //$('#btnShowAdmission').click(function () {
                    //   $(".modal").modal("hide");
                    //    admission(asessment_params, true)
                    //    return;
                    //});

                    /* END: EXH 1.A*/
                 
                    marketerState.orig = JSON.stringify($('#referral_form').extractJson('id'));

                }
            });

            if (rid == undefined) {
           
                setTimeout(function () {
                    $("#addcalllog").css("display", "none");

                }, 1000)
            }

        });
        /* END: EXH 1*/

    });
    
    $('#btnShowAssessment').click(function () {
        assessment(asessment_params, 'for', css.split(' ')[0])
        return;
    });
    $('#btnShowAdmission').click(function () {
        if ($('#genderf').is(":unchecked") && $('#genderm').is(":unchecked")) {            
            swal("Please select gender.")
            return false
        }
      

        var form = document.getElementById('referral_form');
        for (var i = 0; i < form.elements.length; i++) {
            if (form.elements[i].value === '' && form.elements[i].hasAttribute('required')) {
                
                swal("Fill in all required fields.")
                $('.loader').hide();
                return false;
            }
        }
        $(".modal").modal("hide");
        admission(asessment_params, true)
        if ($("#UsrRoleID").val() != 1) {
            swal("Admission by Non-admin User.", "You have no permission to add admission date of the resident. You can only save other details for admission.", "info");
            return;
        }
    });

    $('#btnAddCallLog').click(function () {
        var io = {
            func: "post_call_log"
        };
        var val = $('#CallLog').val();
        if (val == '') {
            alert('Call log is required a field. Please supply before saving.');
            return;
        }
        io.data = val;
        io.id = $('#CallLog').attr('rid');

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Referral + "/PostData",
            data: JSON.stringify(io),
            dataType: "json",
            success: function (m) {
                if (m.Result == 0) {
                    $('#Notes').val(m.Message);
                    $('#CallLog').val("");
                }
            }
        });

    });
    var selectedTabIndex = 0;

    $(document).on("click", "#update-status", function () {
        //$('#update-status').click(function (e) {
        
        ShowUPSDiag(asessment_params, css.split(' ')[0]);
        return;
    });

    //$('#update-status-a').click(function () {
    $(document).on("click", "#update-status-a", function () {
        
        ShowUPSDiag(asessment_params, css.split(' ')[0]);
        return;
    });

    //$(document).on("click", "#save_reverted_status", function () {
    $("#save_reverted_status").on("click", function () {
         
        var data = {
            func: "updatestatus"
        };
        var isValid = $('#txtStatus').val() != "0";

        if (!isValid) return;
        data.data = {
            residentId: asessment_params.resid,
            referralId: asessment_params.refid,
            status: $('#txtStatus').val()
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Referral + "/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.Result == 0) {
                    swal("Status updated successfully!", "", "success")

                    Dashboard.LoadWidgets();

                    $("#referral_modal form").trigger("reset");
                    $('#referral_modal input:checkbox').removeAttr('checked');
                    $("#assessment_modal form").trigger("reset");
                    $("#AssessmentId").val('') //added this for reset does not clear value of assessmentId (alona 11-24-22)
                    $('#assessment_modal input:checkbox').removeAttr('checked');
                    $('#ARMovein, #AR30day,#ARQuarterly,#ARSemiAnual, #ARChangeCondition').prop('checked', false); //4-6-2022 : Cheche
                    $(".modal").modal("hide");

                }
            }
        });
        //return false;
    });

    $(document).on("click", "#saveadmission", function () {
     
        //$("#saveadmission").on("click", function () {
        $(".loader").show();
        var resName = $('#txtResidentName');
        if (resName.val() == "Search resident name...")
            resName.val('');

        var isValid = $('#admission_modal .form').validate();
        //do other modes
        if (!isValid) {
            $('#txtResidentName').val("Search resident name...");
            return;
        }


        var alle = tags.toString();
        //var all = tags;

        //let a = 0;
        //let one = 0;
        //let two = 0;
        //for (let i = 0; i < all.length; i++) {
        //    if (array[a] == all[i]) {
        //        a++;
        //        one++;
        //    }
        //    else if (array[a] != all[i]) {
        //        two++;
        //    }
        //}
        //if (two >= 0 && one == all.length) {
        //    swal("There are no changes to save.", "", "info");
        //    $(".loader").hide();
        //    return;
        //}
        //if admission date is empty kim 10/10/22
        if ($("#txtAdmissionDate").val() == "") {
            $(".loader").hide();
            swal("Please fill up admission date", "", "warning");
            return;
        } 
        var row = $('#admission_modal .form').extractJson('id');

        row['txtAllergy'] = alle;
        row.txtReferalId = asessment_params.refid;
        row.residentId = $('#admForm .admTbs #txtResidentId').val();

        var tabindex = $("#admForm").tabs('option', 'selected');
        var url = "";

        if (tabindex != 2 && tabindex != 15) {
            url = "Admin/PostGridData";
        } else {
            url = "Admin/PostGridDataV2";
        }

        var pTI = tabindex > 4 ? tabindex + 1 : tabindex;
        var data = {
            func: "resident_admission",
            mode: mode,
            data: row,
            TabIndex: pTI.toString()
        };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: url,
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $(".loader").hide();
                if (m.message == "Duplicate") {
                    alert('Admission for resident already exists!');
                    return;
                }
                if (m.message == "Ok") {
                    if ($("#txtAdmissionDate").val() != "") {
                        swal("Resident admission details updated.", "", "success")
                    } else {
                        swal("New resident admitted successfully!", "", "success");
                    }
                    buildPendingAdmission();
                }
                if (m.message == "Error") {
                    swal('"Admission date" should be greater or equal to "Movein Date"', "", "warning");                    
                    return;
                }
                //if (mode == 1) {
                //    $('#txtAdmissionId').val(m.result);
                //}
                var tabindex = $("#admForm").tabs('option', 'selected');
                if (tabindex == 0) {
                    $('#DCResidentName,#ANSResidentName,#AAResidentName,#RMMResidentName').val($('#txtResidentName').val());
                    $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                    $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#room_facility option:selected').text());
                }
                //else if (tabindex == 2) {
                //    $('#AADOB').val($('#ANSDOB').val());
                //    $('#DCRoomNumber,#AARoomNo,#RMMRoomNo').val($('#room_facility option:selected').text());
                //} else if (tabindex == 3) {

                //    if (hasmoveindate) $('#RMMMoveInDate').val(_date);
                //}
                //if (selectedTabIndex > tabindex) {
                //    $("#admForm").tabs("option", "selected", selectedTabIndex);
                //}
                //selectedTabIndex = tabindex + 1;
                //if (tabindex == 0) {
                //    toastr.success("General Information " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 1) {
                //    toastr.success("Document Checklist " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 2) {
                //    toastr.success("Needs and Services Plan Worksheet " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 3) {
                //    toastr.success("Admission Agreement " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 4) {
                //    toastr.success("New Resident Move-In Memo " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 5) {
                //    toastr.success("Physician's Report " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 6) {
                //    toastr.success("Preplacement Appraisal Info " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 7) {
                //    toastr.success("Release of Medical Information " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 8) {
                //    toastr.success("Telecom Device Notification " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 9) {
                //    toastr.success("Identification Emergency Information " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 10) {
                //    toastr.success("Resident Appraisal " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 11) {
                //    toastr.success("Consent to Medical Exam " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 12) {
                //    toastr.success("Consent to Medical Treatment " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //} else if (tabindex == 13) {
                //    toastr.success("Personal Rights " + (mode == 1 ? 'saved' : 'updated') + " successfully!");

                //}

                if (typeof (Dashboard.LoadWidgets) !== 'undefined') {
                    Dashboard.LoadWidgets();
                }

                $("#admission_content .form").trigger("reset");
                $(".modal").modal("hide");

            }
        });
    });
  
    $(document).on("click", "#saveassessment", function () {
   
        $(".loader").show();
        $("#saveassessment").attr("disabled", true);
        $('.loader').show();

       
        var row = {
            Func: 'assessment_wizard'
        };
        row.Mode = ($("#AssessmentId").val() == "") ? 1 : 2;

        var isValid = true;
        isValid = $('.form').validate();
        if (!isValid) {
            $(".loader").hide();
            return;
        }

        //4-4-2022 : Cheche 
        var cb = $('#ARMovein:checked,#AR30day:checked,#ARQuarterly:checked,#ARSemiAnual:checked,#ARChangeCondition:checked').val();

        if (cb != "on") {
            swal("Select One Reason for Assessment!", "Click Reason for Assessment to select.", "warning");
            $('.loader').hide();
            $("#saveassessment").attr("disabled", false);
            $('#m_assessment_2').trigger('click');
            return;
        }
                  //ends here
        row.Data = $('#assessment_modal .form').extractJson('id');

        //================== Compare changes on marketer assessment for
        marketerAssState.current = JSON.stringify(row.Data);
    
        if (marketerAssState.current == marketerAssState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            $("#saveassessment").attr("disabled", false);
            return;
        }
        //==========================================
        // kim assessment wizard date here

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Marketer + "/PostGridData",
            data: JSON.stringify(row),
            dataType: "json",
            success: function (m) {
            
               //Cheche was here to modify : 4-05-2022 
                if (m.Message == "Error on Completed") {
                  //  debugger
                    swal('"Completed Date" should be greater or equal to "Assessment Start Date"', "", "warning");                    
                    Dashboard.LoadWidgets();
                    //$(".modal").modal("hide");
                    //$("#assessment_content form").trigger("reset");                  
                    $('.loader').hide();
                    $("#saveassessment").attr("disabled", false);
                    return false;
                }
                else if(m.Message == "No Completed Date") {
                   // debugger
                    swal('"Please enter Completed Date"', "", "warning");
                    Dashboard.LoadWidgets();
                    //$(".modal").modal("hide");
                    //$("#assessment_content form").trigger("reset");
                    $('.loader').hide();
                    $("#saveassessment").attr("disabled", false);
                    return false;
                }
                else
                {
                    var cb = $('#ARMovein:checked,#AR30day:checked,#ARQuarterly:checked,#ARSemiAnual:checked,#ARChangeCondition:checked').val();

                    if (cb == "on") {
                        swal("Assessment has been saved.", "", "success")
                        //  debugger
                        Dashboard.LoadWidgets();
                        $(".modal").modal("hide");
                        $("#assessment_content form").trigger("reset");
                        $('#assessment_content input:checkbox').removeAttr('checked');
                        $('.loader').hide();
                    } else {
                        swal("Select One Reason for Assessment!", "Click Reason for Assessment to select.", "warning");
                        $('.loader').hide();
                        $("#saveassessment").attr("disabled", false);
                        $('#m_assessment_2').trigger('click');
                        return;
                    }
                   
                     }

            }           
            
        });
    })

    $(document).on("click", "#AssessmentPrint", function () {
        //$("#AssessmentPrint").on("click", function () {
        var ts = $("#AssessmentForm").tabs('option', 'selected');
        if (ts == 6)
            window.open(ALC_URI.Marketer + "/PrintMorseFallScale?id=" + $("#AssessmentId").val(), '_blank');
        else
            window.open(ALC_URI.Marketer + "/PrintAssessment?id=" + $("#AssessmentId").val(), '_blank');
    });

    setHeight();

  
    //var SearchReference = ""; // Remove Search Reference inside the "" by Cheche 11/22/2021
    //$("#RefSource").val(SearchReference).blur(function (e) {
     
    //    //hidden by Cheche Reason: To prevent saving if "Reference Source" field is empty
    //    //if ($(this).val().length == 0) 
    //    // $(this).val(SearchReference);
        

    //}).focus(function (e) {

    //    if ($(this).val() == SearchReference)
    //        $(this).val("");


    //}).autocomplete({
    //    source: function (request, response) {
    //        $.ajax({
    //            url: "Admin/Search",
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "json",
    //            type: "POST",
    //            data: '{func: "search_reference", name:"' + request.term + '"}',
    //            success: function (data) {
    //                console.log(data);
    //                response($.map(data, function (item) {
    //                    return {
    //                        label: item.reference,
    //                        value: item.id
    //                    }
    //                }));
    //            }
    //        });
    //    },
    //    delay: 200,
    //    minLength: 1,
    //    focus: function (event, ui) {
    //        $("#RefSource").val(ui.item.label);
    //        return false;
    //    },
    //    select: function (event, ui) {
    //        return false;
    //    }
    //}).data("ui-autocomplete")._renderItem = function (ul, item) {
    //    console.log(item);
    //    return $("<li></li>")
    //        .data("item.autocomplete", item)
    //        .append("<a>" + item.label + "</a>")
    //        .appendTo(ul);
    //};
    //
    $(".loader").hide();

    $(document).on("click", "#close_referral", function () {
       
        var id = $(this).attr("modalid");

        newState = $('#referral_form').extractJson('id');
        marketerState.current = JSON.stringify(newState);

        if (marketerState.current == marketerState.orig) {
            closeModal(id);
            return;
        } else {
            swal({
                title: "Are you sure you want to close without saving your data?",
                text: "You won't be able to revert this!",
                type: "error",
                showCancelButton: !0,
                confirmButtonText: "Yes"
            }).then(function (e) {
                e.value && closeModal(id)
            })
        }
    });

    $(document).on("click", "#close_assessment", function () {
        var id = $(this).attr("modalid");

        newState = $('#assessment_modal .form').extractJson('id');
        marketerAssState.current = JSON.stringify(newState);

        if (marketerAssState.current == marketerAssState.orig) {
            closeModal(id);
            return;
        } else {
            swal({
                title: "Are you sure you want to close without saving your data?",
                text: "You won't be able to revert this!",
                type: "error",
                showCancelButton: !0,
                confirmButtonText: "Yes"
            }).then(function (e) {
                e.value && closeModal(id)
            })
        }
    });


    //Added on 11-10-22: Angelie
    //starts here
    var incoming_res = localStorage.getItem('incoming_res_from_home');
    var is_from_home = localStorage.getItem('is_from_home');

    if (is_from_home == "1") {

        setTimeout(function () {

            $("#scroll_4 .m-widget2__desc").find('a').attr('resid', incoming_res).trigger("click");

            setTimeout(function () {
                $("#btnShowAdmission").trigger("click");
                $(".loader").hide();
                localStorage.removeItem('incoming_res_from_home');
                localStorage.removeItem('is_from_home');
               
            }, 600);
        }, 600);
    }
    //ends here
});
