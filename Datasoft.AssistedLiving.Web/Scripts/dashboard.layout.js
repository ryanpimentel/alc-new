﻿var Dashboard = { BuildPendingAdmission: function () { } }
$(document).ready(function () {
    function capsFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    var buildPendingAdmission = function () {
        var data = { func: "dashboard_preadmission_nav", id: "0" };
        $.ajax({
            url: ALC_URI.Admin + "/GetFormData",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (res) {
                var x = [];
                $.each(res, function () {
                    var name = this.Fname + " ";
                    if (this.MI != '')
                        name += this.MI + " ";
                    name += this.Lname;
                    x.push("<tr class=''><td style='text-align:center;width:20px'><input name='chkresidents' value='" + this.ID + "' type='checkbox'/></td><td>");
                    x.push("<a id='" + this.ID + "' tab='" + this.Step + "'>" + name + "</a>");
                    x.push("</td><td>");
                    x.push(this.PreAdmissionDate);
                    x.push("</td><td>");
                    x.push(this.AdmissionStatus);
                    x.push("</td></tr>");
                });

                $('#dataTable tbody').html(x.join(''));

                // Usage: $form.find('input[type="checkbox"]').shiftSelectable();
                // replace input[type="checkbox"] with the selector to match your list of checkboxes

                $.fn.shiftSelectable = function () {
                    var lastChecked,
                        $boxes = this;
                    $boxes.click(function (evt) {
                        if (!lastChecked) {
                            lastChecked = this;
                            return;
                        }

                        if (evt.shiftKey) {
                            var start = $boxes.index(this),
                                end = $boxes.index(lastChecked);
                            $boxes.slice(Math.min(start, end), Math.max(start, end) + 1)
                                .attr('checked', lastChecked.checked)
                                .trigger('change');
                        }

                        lastChecked = this;
                    });
                };

                $('#dataTable').find('input[name="chkresidents"]').shiftSelectable();
            }
        });
    }
    Dashboard.BuildPendingAdmission = buildPendingAdmission;
    buildPendingAdmission();

    $('#dataTable tbody').on('click', 'a', function () {
        var dis = $(this);
        var rid = parseInt(dis.attr('id'));
        var tidx = parseInt(dis.attr('tab'));
        if (isNaN(tidx))
            tidx = 1;
        var cltidx = tidx - 1;
        $('#preAdmissionLink').trigger('click');
        var interval = setInterval(function () {
            var tab = $("#PlacementForm");
            if (tab[0]) {
                clearInterval(interval);
                tab.tabs('option', 'disabled', []);
                tab.tabs('option', 'selected', cltidx);

                var data = { func: "preplacement_wizard", id: rid.toString() };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: ALC_URI.Admin + "/GetFormData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        res.residentIdField = rid;
                        $(".form").mapJson(res, 'id');
                    }
                });
            }
        });
    });

    $('#admitResidents').click(function () {
        var values = [];
        $.each($('#dataTable').find('input[name="chkresidents"]'), function () {
            if ($(this).is(':checked'))
                values.push($(this).val());
        });
        var ids = values.join(',');
        if (values.length <= 0) {
            alert("Please select resident(s) to admit first and try again.");
            return;
        }
        if (!confirm("Are you sure you want to admit selected resident(s)?")) return;

        var data = { Func: 'dashboard_pending_admission', Data: ids };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Home + "/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.Result == 0) {
                    $.growlSuccess({ message: m.Message + " new resident(s) has been admitted successfully!", delay: 6000 });
                    buildPendingAdmission();
                }
            }
        });

    });

    $("#DashboardForm").tabs();
    var resize = function () {
        var wH = $(window).height() - 137;
        $("#dashTabsContent").height(wH);
        $("#Home .box-left").height(wH - 50);
        $("#Home table.box").width($(window).width() - 20);
        $('#divdataTable').height($('#dashTabsContent').height() - 54);

        var bh = $('#Home .box-left').height();

        $('.content-body-blue,.content-body-red,.content-body-purple').height((bh / 2) - 72);
        $('.content-body-lime').height(bh - 70);
    };
    resize();
    resize();
    $(window).resize(resize);

    Dashboard.LoadWidgets = function () {
        $([2, 3, 4, 5, 6, 7, 8, 9]).each(function () {
            var type = this;
            var params = { func: "referrals", type: type };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Referral + "/GetFormData",
                data: JSON.stringify(params),
                dataType: "json",
                success: function (m) {
                    var elems = [];
                    var cls = 'class="blue"';
                    if (type == 3)
                        cls = 'class="blue-followup"';
                    else if (type == 4)
                        cls = 'class="red"';
                    else if (type == 5)
                        cls = 'class="red-followup"';
                    else if (type == 6)
                        cls = 'class="purple"';
                    else if (type == 7)
                        cls = 'class="purple-followup"';
                    else if (type == 8) {
                        cls = 'class="lime';

                    }

                    $(m).each(function () {
                        if (type == 8) {
                            var iscomplete = this.movein_status != null && this.movein_status == 1 ? true : false;
                            if (iscomplete)
                                cls += ' complete"';
                            else
                                cls += ' incomplete"';
                        }
                        elems.push('<tr><td><a resid="' + this.resid + '" id="' + this.refid + '" ' + cls + '>' + this.name + '</a></td></tr>');
                    });
                    if (type == 2) {
                        //new leads
                        $('.content-body-blue tbody:eq(0)').html($(elems.join('')));
                    } else if (type == 3) {
                        //new leads followup
                        $('.content-body-blue tbody:eq(1)').html($(elems.join('')));
                    } else if (type == 4) {
                        //tour schedule
                        $('.content-body-red tbody:eq(0)').html($(elems.join('')));
                    } else if (type == 5) {
                        //tour schedule followup
                        $('.content-body-red tbody:eq(1)').html($(elems.join('')));
                    } else if (type == 6) {
                        //assessment completion
                        $('.content-body-purple tbody:eq(0)').html($(elems.join('')));
                    } else if (type == 7) {
                        //assessment completion followup
                        $('.content-body-purple tbody:eq(1)').html($(elems.join('')));
                    }
                    else if (type == 8) {
                        //movein schedule
                        $('.content-body-lime tbody:eq(0)').html($(elems.join('')));
                    } else if (type == 9) {
                        //movein schedule followup
                        $('.content-body-lime tbody:eq(1)').html($(elems.join('')));
                    }
                }
            });
        });
    };
    Dashboard.LoadWidgets();

    var tab = 1;
    var pagecount = 1;
    var assessment = function (d, win_type) {
        var residentId = d.resid;
        var referralId = d.refid;


        var $canceldialog;
        var $dialog = $("<div><div id=\"assessment_content\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Assessment Wizard",
            dialogClass: "assessmentDialog",
            width: $(window).width() - 300,
            height: $(window).height() - 100,
            close: function () {
                $dialog.dialog("destroy").remove();
            },
            beforeClose: function (e, ui) {
                $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
                    modal: true,
                    title: "Confirmation",
                    width: 400,
                    buttons: {
                        "Yes": function () {
                            $canceldialog.dialog("destroy").remove();
                            $dialog.dialog("destroy").remove();
                            //window.location = ClientUrls["Home"];
                        },
                        "No": function () {
                            $canceldialog.dialog("destroy").remove();
                        }
                    }
                });
                return false;
            },
            buttons: {
                "Save": {
                    click: function () {
                        var row = { Func: 'assessment_wizard' };
                        row.Mode = ($("#AssessmentId").val() == "") ? 1 : 2;

                        var isValid = true;
                        isValid = $('.form').validate();
                        if (!isValid) {
                            return;
                        }
                        row.Data = $('.form').extractJson('id');


                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: ALC_URI.Marketer + "/PostGridData",
                            data: JSON.stringify(row),
                            dataType: "json",
                            success: function (m) {

                                $.growlSuccess({ message: "Assessment has been saved.", delay: 6000 });
                                $dialog.dialog("destroy").remove();
                                if (typeof (Dashboard.LoadWidgets) !== 'undefined') {
                                    Dashboard.LoadWidgets();
                                    Dashboard.HideDialog();
                                }
                            }
                        });

                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $dialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {


                var diag = $('.ui-dialog-content');
                diag = diag.find('#AssessmentForm').parents('.ui-dialog-content');
                var height = 0;
                height = diag.eq(0).height() || 0;
                if (height <= 0)
                    height = diag.eq(1).height() || 0;
                $("#assessmentContainer,#AssessmentForm").height(height);
                $('#assessmentContent,#assessmentTreeContainer').height(height - 20);
                $('#tabs-1,#tabs-2,#tabs-3,#tabs-4,#tabs-5,#tabs-6').height(height - 30);
            }
        });
        $.ajax({
            url: ALC_URI.Admin + "/GetNodeData",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            data: (win_type == 'wiz') ? '{id:"assessmentwizard_0"}' : '{id:"assessment_0"}',
            success: function (d) {
                $("#assessment_content").empty();
                $("#assessment_content").html(d);

                var params = { func: "assessment_wizard", id: residentId };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: ALC_URI.Marketer + "/GetFormData",
                    data: JSON.stringify(params),
                    dataType: "json",
                    success: function (m) {
                        if (m.Result == 1) {

                            $("#FirstName").val(m.Data.FirstName);
                            $("#LastName").val(m.Data.LastName);
                            $("#MiddleInitial").val(m.Data.MiddleInitial);
                            var info = $.cookie('ALCInfo');
                            if (info != '') {
                                var ins = info.split('|');
                                var name = '';
                                if (ins.length > 1) {
                                    name = ins[1];
                                }
                                $("#CompletedBy").val(capsFirstLetter(name));
                            }
                        }
                        else {
                            //$(".form").serializeArray().map(function (x) { m[x.name] = x.value; },'id');
                            $(".form").mapJson(m, 'id');
                        }

                        $("#AssResidentId").val(residentId);
                        $("#AssReferralId").val(referralId);

                        if (win_type == 'wiz') {

                            tab = 1;
                            pagecount = 1;
                            var $foot = $('<div style="padding-top:5px;float:left;"> <label><strong>Total Score: <span id="label-TotalScore">0</span></strong> </label></div> ' +
                                    '<div style="padding:0;"> <center> <input class="navi-button" id="ass-wiz-prev" type="button" value="<<" /> ' +
                                    ' <span><label id="pager" style="float:none;">Page  <input type="number" id="pager-no" min ="1" max="33"  /> of 33</label></span> <input class="navi-button" id="ass-wiz-next" type="button" value=">>" /> </center> </div>');

                            $('.ui-dialog-buttonpane').append($foot);
                            $("#label-TotalScore").text(m.TotalScore);
                            loadPager();

                        }
                    }
                });

            },
            complete: function () { },
            error: function () { }
        });
        return false;
    };
    $(document).on('click', '#ass-wiz-next', function (e) {
        var $current = $("#tabs-" + tab.toString() + ' .container-display');
        var $next = $("#tabs-" + tab.toString() + ' .container-display').next('.container');
        if ($next.length == 0) {
            if (tab == 6)
                return;
            tab++;
            $next = $("#tabs-" + tab.toString() + ' .container').first('.container');
        }
        $("#AssessmentForm").tabs("option", "selected", tab - 1);
        $next.addClass('container-display');
        $current.removeClass('container-display');
        pagecount++;
        loadPager();
    });
    $(document).on('click', '#ass-wiz-prev', function (e) {
        var $current = $("#tabs-" + tab.toString() + ' .container-display');
        var $prev = $("#tabs-" + tab.toString() + ' .container-display').prev('.container');
        if ($prev.length == 0) {
            if (tab == 1)
                return;
            tab--;
            $prev = $("#tabs-" + tab.toString() + ' .container').last('.container');
        }
        $("#AssessmentForm").tabs("option", "selected", tab - 1);
        $prev.addClass('container-display');
        $current.removeClass('container-display');
        pagecount--;
        loadPager();
    });

    var loadPager = function () {
        //var pagesize = $('.container').length; 56
        $("#pager-no").val(pagecount);
    }
    function assessmentWizardGoToPage(page) {
        var $current = $("#tabs-" + tab.toString() + ' .container-display');
        var gotopage = page;
        if (gotopage == pagecount)
            return;
        if (gotopage > 33 || gotopage < 1) {
            $("#pager-no").val(pagecount);
            return;
        }
        else
            $("#pager-no").val(gotopage);
        var x = 0;
        var tab_col = {
            1: x += $('#tabs-1 .container').length,
            2: x += $('#tabs-2 .container').length,
            3: x += $('#tabs-3 .container').length,
            4: x += $('#tabs-4 .container').length,
            5: x += $('#tabs-5 .container').length,
            6: x += $('#tabs-6 .container').length,
        };
        for (var i = 1; i <= 6; i++) {
            if (gotopage <= tab_col[i]) {
                tab = i;
                pagecount = gotopage;
                var $go = $("#tabs-" + tab.toString() + ' .container').slice(((tab_col[i] - ((i == 1) ? 0 : tab_col[i - 1])) - (tab_col[i] - pagecount)) - 1).slice(0, 1);

                $("#AssessmentForm").tabs("option", "selected", tab - 1);
                $go.addClass('container-display');
                $current.removeClass('container-display');
                return;
            }
        }
    }

    $(document).on('click', '#assessmentTreeView li.ass-sub-node a', function (e) {
        var i = $(this).attr('page');
        assessmentWizardGoToPage(i);

    });
    $(document).on('keypress', '#pager-no', function (e) {
        if (e.which == 13) {
            assessmentWizardGoToPage($("#pager-no").val());
        }
    });

    $('#Home').on('click', 'a', function () {
        var asessment_params = {
            resid: $(this).attr('resid'),
            refid: $(this).attr('id')
        };

        if ($(this).hasClass('purple')) {
            assessment(asessment_params, 'wiz')
            return;
        }


        var $canceldialog;
        var $dialog = $("<div><div id=\"referral_form\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Referral Form",
            width: $(window).width() - 300,
            height: $(window).height() - 100,
            close: function () {
                $dialog.dialog("destroy").remove();
            },
            beforeClose: function (e, ui) {
                $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
                    modal: true,
                    title: "Confirmation",
                    width: 400,
                    buttons: {
                        "Yes": function () {
                            $canceldialog.dialog("destroy").remove();
                            $dialog.dialog("destroy").remove();
                            //window.location = ClientUrls["Home"];
                        },
                        "No": function () {
                            $canceldialog.dialog("destroy").remove();
                        }
                    }
                });
                return false;
            },
            buttons: {
                "Save": {
                    click: function () {
                        $('#btnSave').trigger('click');
                        Dashboard.HideDialog = function () {
                            $dialog.dialog("destroy").remove();
                        }
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $dialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {

            }
        });
        var dis = $(this);
        var css = dis.attr('class');
        var rid = dis.attr('id');

        //if (target.attr('id') == 'btnCreate')
        //    $('#Home').clearFields();
        $.get(ALC_URI.Referral + "?diag=1&attr=" + css, function (data) {

            var params = { func: "getform", id: rid };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Referral + "/GetFormData",
                data: JSON.stringify(params),
                dataType: "json",
                success: function (m) {

                    $('#referral_form').html(data).mapJson(m, 'id');

                    $('#btnAddCallLog').click(function () {
                        var $dialog = $("<div><div id=\"call_log\" class=\"form-container\" style=\"padding:10px;\"><textarea rid=\"" + rid + "\" name=\"CallLog\" id=\"CallLog\" style=\"border:1px solid #a4a4a4 !important;outline:none;resize:none;width:300px;height:60px;\"/></div></div>").dialog({
                            modal: true,
                            closeOnEscape: false,
                            title: "Add Call Log",
                            width: 335,
                            height: 155,
                            resizable: false,
                            close: function () {
                                $dialog.dialog("destroy").remove();
                            },
                            beforeClose: function (e, ui) {
                                $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
                                    modal: true,
                                    title: "Confirmation",
                                    width: 400,
                                    buttons: {
                                        "Yes": function () {
                                            $canceldialog.dialog("destroy").remove();
                                            $dialog.dialog("destroy").remove();
                                            //window.location = ClientUrls["Home"];
                                        },
                                        "No": function () {
                                            $canceldialog.dialog("destroy").remove();
                                        }
                                    }
                                });
                                return false;
                            },
                            buttons: {
                                "Ok": {
                                    click: function () {
                                        var io = { func: "post_call_log" };
                                        var val = $('#CallLog').val();
                                        if (val == '') {
                                            alert('Call log is required a field. Please supply before saving.');
                                            return;
                                        }
                                        io.data = val;
                                        io.id = $('#CallLog').attr('rid');
                                        $.ajax({
                                            type: "POST",
                                            contentType: "application/json; charset=utf-8",
                                            url: ALC_URI.Referral + "/PostData",
                                            data: JSON.stringify(io),
                                            dataType: "json",
                                            success: function (m) {
                                                if (m.Result == 0) {
                                                    $('#Notes').val(m.Message);
                                                    $dialog.dialog("destroy").remove();
                                                }
                                            }
                                        });
                                        return false;

                                    },
                                    class: "primaryBtn",
                                    text: "Ok"
                                },
                                "Cancel": function () {
                                    $dialog.dialog("destroy").remove();
                                }
                            },
                            resize: function (event, ui) {

                            }
                        });
                    });
                    $('#btnShowAssessment').click(function () {
                        assessment(asessment_params, 'for')
                        return;
                    });
                }
            });

        });

    });
});