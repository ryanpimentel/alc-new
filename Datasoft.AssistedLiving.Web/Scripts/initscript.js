﻿$(document).ready(function () {

    var info = $.cookie('ALCInfo');
    if (info && info != '') {
        
        var ins = info.split('|');
        console.log(ins);
        var name = '', agency = '';
        if (ins.length > 1) {
            name = ins[1] + '!';
            agency = ins[0];
        }
        ////$('#lblUserId').html(capsFirstLetter(name));
        //$(document).find("#lblAgencyName").html(m.toUpperCase());

                //added codes by Cheche 12/23/2021 to display agency name

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Admin + "/GetAgencyName",
            dataType: "json",
            success: function (m) {
               
                //revised this for homecare label: Angelie, 07-13-22
                if (m.is_homecare) {
                    $("#lblAgencyName").html("HOME CARE CENTRE DEMO");
                } else {
                    $(document).find("#lblAgencyName").html(m.agency_name.toUpperCase());
                }
                

               
            }
        })



        if (is_auth == "yes") {
            if (isAuthPage == true){
                $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Admin + "/GetGridData?func=user",
                dataType: "json",
                success: function (m) {


                    $.each(m, function (i, v) {

                        if (v.Id == ins[1]) {
                            
                            var imgsrc = "";

                            if (typeof (btoa) == 'undefined')
                                imgsrc += "Resource/EmpImage?id=" + v.Id + "&b=0&r=" + Date.now() + "' alt='" + v.Name;
                            else
                                imgsrc += "Resource/EmpImage?id=" + btoa(v.Id) + "&b=1&r=" + Date.now() + "' alt='" + v.Name;

                            $("#usrpic").attr("src", imgsrc);
                            $("#usrname").html(v.Name);
                            $("#usremail").html(v.Email);

                      
                        }

                    })
                }
                })
            }
        }




// kim add code for change pass 03/14/22
        $('#savePassword').on('click', function () {
            var dis = ins[1];
            var oldpass = $("#OldPassword").val();
            var newpass = $("#firstpassword").val();
            var newretrypass = $("#secondpassword").val();
            if (oldpass == "" || newpass == "" || newretrypass == "") {
                swal("Please fill up blank field", "", "warning");
                return;
            }
            if (newpass != newretrypass) {
                swal("New Password and Confirm New Password does not match", "", "error");
                return;
            }
            mode = "";
            $('#Iduser').val(dis)
            row = $('#pass_modal').extractJson();
            mode = 2;

            doAjax(mode, row, function (m) {
                if (m.result == 0) {

                    //           swal("Password " + ('updated') + " successfully!", "", "success");        
                    Swal({
                        title: 'Password updated successfully!',
                        type: 'success',
                        confirmButtonColor: '#3085d6',                       
                        confirmButtonText: 'Confirm!',
                    }).then((result) => {
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: ALC_URI.Login + "/LogOff",
                            success: function (m) {
                                if (m.Result == 1) {

                                    location.href = m.RedirectUrl;
                                }
                            }
                        });
                    })

                }
        
                        
                    if (m.result == -4) {
                        swal(m.message + "", "", "error");
                        return;
                    }
                    $('#pass_modal').clearFields();
                    $("#pass_modal").modal("hide");
                });
            
        })

        
        $('#cancelbutton').click(function () {
            $("#OldPassword").val("");
             $("#firstpassword").val("");
             $("#secondpassword").val("");

        });

        $('#logoutLink').click(function () {

            Swal({
                title: 'Are you sure you want to logout?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: ALC_URI.Login + "/LogOff",
                        success: function (m) {
                            if (m.Result == 1) {
                               
                                location.href = m.RedirectUrl;
                            }
                        }
                    });
                }
            })
            
            //$canceldialog = $("<div><div class=\"confirm\">Are you sure you want to logout?</div></div>").dialog({
            //    modal: true,
            //    title: "Confirmation",
            //    width: 400,
            //    buttons: {
            //        "Yes": function () {
            //            $.ajax({
            //                type: "POST",
            //                contentType: "application/json; charset=utf-8",
            //                url: ALC_URI.Login + "/LogOff",
            //                success: function (m) {
            //                    if (m.Result == 1) {
            //                        $canceldialog.dialog("destroy").remove();
            //                        location.href = m.RedirectUrl;
            //                    }
            //                }
            //            });
            //        },
            //        "No": function () {
            //            $canceldialog.dialog("destroy").remove();
            //        }
            //    }
            //});
            //return false;
        });
    }
})
// kim add code for change pass 03/14/22
var doAjax = function (mode, row, cb) {
    if (mode == 0) {
        var data = { func: "user", id: ins[1]  };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
        return;
    }

    if (mode == 1 || mode == 2) {

        row = row;
    }

    var data = { func: "user", mode: mode, data: row };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/PostGridData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);
        }
    });
}