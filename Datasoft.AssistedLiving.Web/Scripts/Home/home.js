﻿$(function () {

    $(".loader").show();
  
    var marketerStats;
    var residentStats;
    //kim change label 04/28/22 
   // ["Admitted as Resident", "New Leads", "Referral / Lead Follow-Up", "Tour Schedule", "Tour Follow-Up", "Assessment Completion", "Assessment Follow-up", "Move In Schedule", "Move In Follow-Up"];

    var admissionLabel = [" Recent Admission", "New Leads", "Referral / Lead Follow-Up", "Tour Schedule", "Completed Tour", " Assessment Schedule", "Completed Assessment", "Move in"];
   
    function random_rgba() {
        var o = Math.round, r = Math.random, s = 255;
        return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ', 0.9)';
    }

    function random_rgba_border() {
        var o = Math.round, r = Math.random, s = 255;
        return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ', 1)';
    }

    var scroll1 = new PerfectScrollbar('#missed_tasks');

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Home/GetGridData?func=activity_schedule",
        dataType: "json",
        async: false,
        success: function (m) {
            var complete = 0;
            var missed = 0;
            var incomplete = 0;
            var html = "";
            var counter = 0;
            var tempDate = new Date(Date.now());

            $.each(m, function (i, v) {
              
                
                if (v.active_until != null)
                {
                    nDate = new Date(v.active_until);
                }
                if (v.room == null) { v.room = ""; }
                if (v.isAdmitted == true) { // added this to prevent showing discharged residents on the dashboard : 2-25-2022

                    if ((v.is_active == true) || ((v.is_active == false) && (nDate > tempDate))) {
                        counter += 1;

                        if (v.carestaff_activity_id != null && v.activity_status == 1 && v.reschedule_dt == null) {

                            complete += 1;
                        }

                        if (v.carestaff_activity_id == null && v.timelapsed == true) {
                            missed += 1;

                            //Cheche was here to modify code on 1/28/2022 
                            if (v.resident != null) {
                                html += '<div class="m-timeline-3__item m-timeline-3__item--info"><span class="m-timeline-3__item-time" style="font-size:12px;line-height:13px">' + v.start_time + '</span><div class="m-timeline-3__item-desc"><span class="m-timeline-3__item-text">' + v.activity_desc + '</span><br><span class="m-timeline-3__item-user-name">' + v.carestaff_name + ' - ' + v.resident + ' - ' + v.room + ' </span ></div ></div > '
                            }
                            else {
                                html += '<div class="m-timeline-3__item m-timeline-3__item--info"><span class="m-timeline-3__item-time" style="font-size:12px;line-height:13px">' + v.start_time + '</span><div class="m-timeline-3__item-desc"><span class="m-timeline-3__item-text">' + v.activity_desc + '</span><br><span class="m-timeline-3__item-user-name">' + v.carestaff_name + ' - ' + v.room + ' </span ></div ></div > '
                            }

                        }
                        if (v.carestaff_activity_id != null && v.activity_status == 0) {
                            incomplete += 1;
                        }
                    }

                }
               
                
            })


            $("#tasks_count").html(counter);
            $("#completed_count").html(complete);
            $("#missed_count").html(missed);
            $("#incomplete_count").html(incomplete);
            $("#missed_tasks").html(html);
           

            //var cnt = m[0].StatCount;
            //var html = "";
            //if (m.length != 0) {
            //    $.each(m, function (i, v) {


            //        if (v.StatCount == cnt) {
            //            html += '<h5>' + v.refsource + '</h5>';
            //        }
            //    })
            //}
            //$("#topreference").html(html);

        }
    })

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Home/GetMarketerStats",
        dataType: "json",
        async: false,
        success: function (m) {
            marketerStats = m;
            var admission = ["", "", "", "", "", "", "", "", "", ""];

            if (m.length > 0) {
                $.each(m, function (i, v) {

                    admission[v.admission_status] = v.StatCount;

                })
            }

            $.each(admission, function (i, v) {
                if (admission[i] == "") {
                    admission[i] = 0;
                }
            })

            admission.splice(0, 1);

            var ctx = document.getElementById("marketerChart").getContext('2d');
           
            var myDoughnutChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: admissionLabel,
                    datasets: [{
                        label: "Marketer Statistics",
                        //backgroundColor: 'rgb(255, 99, 132)',
                        //borderColor: 'rgb(255, 99, 132)',
                        data: admission,
                        backgroundColor: [
                           'rgba(54, 162, 235, 0.9)',
                           'rgba(255, 206, 86, 0.9)',
                           'rgba(75, 192, 192, 0.9)',
                           'rgba(153, 102, 255, 0.9)',
                           'rgba(255, 159, 64, 0.9)',
                           'rgba(136, 221, 200, 0.9)',
                           'rgba(221, 160, 136, 0.9)',
                           'rgba(242, 212, 212, 0.9)',
                           'rgba(208, 241, 221, 0.9)'
                        ],
                        borderColor: [
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(136, 221, 200, 1)',
                            'rgba(221, 160, 136, 1)',
                            'rgba(242, 212, 212, 1)',
                            'rgba(208, 241, 221, 1)'
                        ],
                        borderWidth: 1
                    }]
                }
            });


        }
    })

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Home/GetResidentStats",
        dataType: "json",
        async: false,
        success: function (m) {
            var refLabel = ["All", "Admitted", "Discharged", "DNR"];
            //var isDNR = 0;
            //var isAdmitted = 0;
            //var isDischarged = 0;

            //$.each(m.isAdmitted, function (i, v) {

            //    if (v.isAdmitted != false) {
            //        isAdmitted += v.StatCount;
            //    } else {
            //        isDischarged += v.StatCount;
            //    }

            //});

            //$.each(m.isDNRstat, function (i, v) {
                
            //    if (v.isDNR == true) {
            //        isDNR += v.StatCount;
            //    }
            //})

            var ctx = document.getElementById("referralChart").getContext('2d');

            var myDoughnutChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: refLabel,
                    datasets: [{
                        label: "Residents",
                        //backgroundColor: 'rgb(255, 99, 132)',
                        //borderColor: 'rgb(255, 99, 132)',
                        data: [m.residentCount, m.isAdmitted, m.isDischarged, m.isDNRstat],
                        backgroundColor: [random_rgba(), random_rgba(), random_rgba(), random_rgba()]
                    }]
                }
            });
        }
    })

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Home/GetReferrerStats",
        dataType: "json",
        async: false,
        success: function (m) {

                var html = "";

            if (m.length != 0) {

                var top_five = m.slice(0, 5);

                    $.each(top_five, function(i, v){
                        
                            if (v.refsource == null) {
                                html += '<h5></h5>';
                            } else {
                                html += '<h5>' + v.refsource + '</h5>';
                            }

                    })

                }
                $("#topreference").html(html);
        }
    })

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Home/GetAdmissionDates",
        dataType: "json",
        async: false,
        success: function (m) {
            $("#current_year").html(new Date().getFullYear());
            var admission = ["", "", "", "", "", "", "", "", "", "", "", ""];
            var months = [];

            for (i = 0; i <= 11; i++) {
                months[i] = 0;
            }

            if (m.length != 0) {

                var s;

                $.each(m, function (i, v) {

                    if (v.admission_date != null) {
                        
                        s = new Date(v.admission_date);

                        if (s.getFullYear() == new Date().getFullYear()) {
                            months[s.getMonth()] += 1;
                        }        
                    }
                })

            }

            var labeled_data = [];
            var m = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

            for (i = 0; i < months.length; i++) {
                //console.log(months[i]);
                labeled_data.push({ meta: m[i], value: months[i] });
            }

            var chart = new Chartist.Line('.ct-chart', {
                labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
                series: [
                    labeled_data
                ]
            }, {
                chartPadding: {
                    right: 10,
                    top: 50, 
                    bottom: 50
                },
                lineSmooth: Chartist.Interpolation.cardinal({
                    fillHoles: true,
                }),
                low: 0,
                axisY: {
                    onlyInteger: true
                    },
                    plugins: [
                        Chartist.plugins.ctPointLabels({
                            textAnchor: 'middle',
                            labelInterpolationFnc: function (value) {
                                //console.log(value)
                                //console.log("i was called");
                                if (value == undefined) {
                                    return "";
                                } else {
                                    return value;
                                }
                                
                            }
                        }),
                        Chartist.plugins.tooltip({

                        })
                    ]
                });


        } 
    })

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Home/GetCurrentStats",
        dataType: "json",
        async: false,
        success: function (m) {
    
            var recentcount = 0;
            var incomingcount = 0;
            var html_incoming = "";
            var html_recently = "";
            var html_tour = "";
            var tourcount = 0;
            var adm_date;
            var now = new Date();
            var tendays = new Date();
            var pdate;
            tendays.setDate(tendays.getDate() + 10); 
            var onem = new Date();
            onem.setDate(onem.getDate() - 31);   

            if (m.incomingAdmission.length != 0) {
       
           
                $.each(m.incomingAdmission, function (i, v) {
                    incomingcount += 1;

                    html_incoming += "<h5><a href='#' class='c_incoming_res' resident_id='" + v.resident_id + "' >" + v.last_name + ", " + v.first_name + " " + (v.middle_initial != "" ? v.middle_initial + "." : "") + "</a></h5>";
                 
                    
                 
                   
                });
            }
            $("#incoming_count").html(incomingcount);
            $("#incomingres").html(html_incoming);

            if (m.recentAdmission.length != 0) {
               
                $.each(m.recentAdmission, function (i, v) {
                    
                    adm_date = new Date(v.date_admitted);       
                  
                    if (adm_date.getFullYear() == now.getFullYear() && adm_date >= onem && now >= adm_date) {
                            recentcount += 1;
                            html_recently += "<h5>" + v.last_name + ', ' + v.first_name + ' ' + (v.middle_initial != '' ? v.middle_initial + '.' : '') + '</h5>';
                        
                    }
                })
            }
            $("#recent_count").html(recentcount);
            $("#recentlyadm").html(html_recently);

            if (m.tourdate_sched.length != 0) {
      
                $.each(m.tourdate_sched, function (i, v) {
             
                    pdate = new Date(v.planned_tourdate);
                    //added by Cheche : 4-13-2022 to get the current date && current month : 10-18-2022
                    var ndate = now.getDate();
                    var mdate = now.getMonth() + 1;
                    var pdate2 = pdate.getDate();
                    var mdate2 = pdate.getMonth() + 1;
                    
                    if (pdate <= tendays && now < pdate || pdate <= tendays && now > pdate && mdate2 == mdate && pdate2 == ndate ) { 
             
                    
                    tourcount += 1; 
                    html_tour += "<h6>" + v.last_name + ", " + v.first_name + " " + (v.middle_initial != "" ? v.middle_initial + "." : "")  + " - (" + v.planned_tourdate + ")</h6>";
                    }
                  
                })
            }
            $("#schedule_tour").html(tourcount); 
            $("#scheduledtour").html(html_tour); 
            
        }

    })

})

$(document).ready(function () {
    
  
    $('#incomingres').css("height", (($('#admGraphPanel').height()) / 2) - 50);
    $('#recentlyadm').css("height", (($('#admGraphPanel').height()) / 2) - 50);
    $('#missed_tasks').css("height", (($('#marketerChart').height()) - 200));
    $('#scheduledtour').css("height", (($('#marketerChart').height()) / 2) - 30);
    $('#topreference').css("height", (($('#marketerChart').height()) / 2) - 30);

    $(window).resize(function () {
        setTimeout(function () {
        $('#incomingres').css("height", (($('#admGraphPanel').height()) / 2) - 50);
        $('#recentlyadm').css("height", (($('#admGraphPanel').height()) / 2) - 50);
        $('#missed_tasks').css("height", (($('#marketerChart').height()) - 200));
        $('#scheduledtour').css("height", (($('#marketerChart').height()) / 2) - 30);
        $('#topreference').css("height", (($('#marketerChart').height()) / 2) - 30);
        }, 500);
    })
    $(".loader").hide();


    //Added on 11-10-22: Angelie
    //starts here
    $('#incomingres .c_incoming_res').on('click', function () {

        roleid = $("#rid_home").val();
      
        if (roleid != "1") {
            swal("You are not allowed to navigate from here!", "", "warning");

        } else {
            $(".loader").show();
            res_id = $(this).attr('resident_id');
            localStorage.setItem("incoming_res_from_home", res_id);
            localStorage.setItem("is_from_home", 1);
            console.log()
            $("#jsTreeView .m-menu__submenu").find('#marketing_1 a.m-menu__link').trigger("click");
        } 
       
    });
     //ends here

   

  
});