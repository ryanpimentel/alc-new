﻿function QS(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
$(document).ready(function () {
   
    var hname = window.location.hostname;
    if (hname.includes("dslchcc.com")) {
        title = "<br /><h2>Home Care Centre</h2><h5>Brought to you by: DataSoftLogic Inc.</h5><br> ";
        $(".content_title").html(title);
        $("#c_logo").attr("src", "/images/HomeCareCentre.png");
    }

    var token = window.location.search.substring(1);
    //Modified by RDP 2-21-25
    const substring1 = "%2fHome";
    const substring2 = "%2fMessage";
    var checkifurlhasunnecessarystrings = token.includes(substring1); 
    var checkifurlhasunnecessarystrings2 = token.includes(substring2); 

    if (checkifurlhasunnecessarystrings == true || checkifurlhasunnecessarystrings2 == true) {
        token = '';
    }

    if (token.length >= 1) {
        var converted_token = atob(token);

        var split1_token = converted_token.split('=');
        var company = split1_token[0];
        var username = split1_token[1];

        $('#txtCompany').val(company);
        $('#txtUsername').val(username);
    }
   

    $('#errMsg').hide();
    $("#txtCompany,#txtUsername,#txtPassword").keyup(function (event) {
        if (event.keyCode == 13) {
            $('#btn-login').click();
        }
    });

    $('#btn-forgot').click(function () {  
        location.href = ALC_URI.Account + "/ResetPassword";
    });

    $('#btn-login').click(function () {
        $('#txtCompany,#txtUsername,#txtPassword').removeClass('fielderror');
        $('#errMsg').hide().find('.fieldlabel').html('');

        var agency = $('#txtCompany').val();
        var user = $('#txtUsername').val();
        var pass = $('#txtPassword').val();

        var err = [];
        if (agency == "") {
            $('#txtCompany').addClass('fielderror');
            err.push('Agency ID is required.');
        }
        if (user == "") {
            $('#txtUsername').addClass('fielderror');
            err.push('User ID is required.');
        }
        if (pass == "") {
            $('#txtPassword').addClass('fielderror');
            err.push('Password is required.');
        }
        if (err.length > 0) {
            $('#errMsg').show().find('.fieldlabel').html(err.join('<br/>'));
            return;
        }
        var data = { AgencyId: agency, UserId: user, Password: pass, RetUrl: QS("ReturnUrl") };
        //alert(data);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: loginUrl,
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //Modified by RDP 2-21-25
                if (m.Result == -1) {
                    $('#errMsg').show().find('.fieldlabel').html(m.ErrorMsg);
                    return;
                }
                if (m.RedirectUrl == "/ALC/Home" || m.RedirectUrl == "/ALC/Message") {
                    location.href = "Home";
                }
                else {
                    location.href = m.RedirectUrl;
                }
                
            }
        });
    });
});