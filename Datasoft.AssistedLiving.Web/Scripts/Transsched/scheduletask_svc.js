var mode = 1;
var caregiver_ftr_val = $("#ftrCaregiver option:first").val();
var carestaff_orig = "";
var carestaff_id = "";
var rlId = "";


var triggerDayofWeekClick = function () {

    var a = $('#services_modal #svc_start_time').val();
    if (a == null || a == '') {
        $('#services_modal #svc_txtCarestaff').empty();
        //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
        return;
    }

    var dow = [];
    $('#services_modal .dayofweek').find('input[type="checkbox"]').each(function () {
        var d = $(this);
        if (d.is(':checked')) {
            switch (d.attr('name')) {
                case 'Sun':
                    dow.push('0');
                    break;
                case 'Mon':
                    dow.push('1');
                    break;
                case 'Tue':
                    dow.push('2');
                    break;
                case 'Wed':
                    dow.push('3');
                    break;
                case 'Thu':
                    dow.push('4');
                    break;
                case 'Fri':
                    dow.push('5');
                    break;
                case 'Sat':
                    dow.push('6');
                    break;
            }
        }
    });
    bindCarestaff2(dow);

}

//get values of checked services
var triggerServicesClick = function () {
    var svcs = [];
    $('#services_modal .services').find('input[type="checkbox"]').each(function () {
        var d = $(this);
       
        if (d.is(':checked')) {
            svcs.push(d.val());
        }
    });
    return svcs;
}

var bindCarestaff2 = function (dow) {

    if ($('#services_modal #svc_start_time').val() == '') {
        $('#services_modal #svc_txtCarestaff')[0].options.length = 0;
        return;
    }
    if (dow == null) dow = [];
    var svcs = triggerServicesClick();

    var data = {
        scheduletime: $('#services_modal #svc_start_time').val(),
        activityid: svcs,
        carestaffid: null,
        recurrence: dow.join(',')
    };
    data.isonetime = "1";
    data.clientdt = moment($('#services_modal #svc_Onetimedate').val()).format("MM/DD/YYYY");
    data.type = 1;

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetCarestaffByTime",
        data: JSON.stringify(data),
        async: false,
        dataType: "json",
        success: function (m) {
            //$('#services_modal #svc_txtCarestaff').empty();
            //$('<option/>').val('').html('').appendTo('#services_modal #svc_txtCarestaff');
            $.each(m, function (i, d) {
                if (d.Name != null && $('#services_modal #svc_txtCarestaff').val() != d.Name) {
                    //$('<option/>').val(d.Id).html(d.Name).appendTo('#services_modal #svc_txtCarestaff');
                    alert("Employee has ongoing schedule for this time and date");
                }
            });
        }
    });
}

var doAjaxTask = function (mode, row, cb) {
    var isValid = $('#services_modal').validate();
    var svcs = triggerServicesClick();
    var edit_detail = "";

    if (mode != 1 && mode != 3) {
        m = mode.split('_');
        edit_detail = m[1]; //either mode  = 2_forcompletion or 2_foreditservice or 2_re for resched/reassign

        if (m.length > 0) {
            mode = parseInt(mode[0]);
        }
    }
   
    //do other modes
    if (mode == 1 || mode == 2) {
      //  debugger;
        var form = document.getElementById('services_modal_form');

        for (var i = 0; i < form.elements.length; i++) {
            if (form.elements[i].value === '' && form.elements[i].hasAttribute("required")) {
                swal("<span>Fill in all required (<span style=\"color: red;\">*</span>) fields. </span>")
                return false;
            }
        }
      //  debugger;
        if (!isValid) {
            return;
        }
        
        row = $('#services_modal').extractJson();

        row.ScheduleTime = $('#services_modal #start_time').val();
        row.ForCompletion = 0;
        row.IsCompleted = 0;
        row.IsResched = 0;


        var dow = [];

        delete row['Sun'];
        delete row['Mon'];
        delete row['Tue'];
        delete row['Wed'];
        delete row['Thu'];
        delete row['Fri'];
        delete row['Sat'];
        delete row['recurring'];
        delete row['Activity'];
        delete row['Onetimedate'];


        var isrecurring = $('#services_modal #svc_reccur').is(':checked');
        if (isrecurring) {
           
            row.isonetime = '0';
            $('#services_modal .dayofweek').find('input[type="checkbox"]').each(function () {
                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun':
                            dow.push('0');
                            break;
                        case 'Mon':
                            dow.push('1');
                            break;
                        case 'Tue':
                            dow.push('2');
                            break;
                        case 'Wed':
                            dow.push('3');
                            break;
                        case 'Thu':
                            dow.push('4');
                            break;
                        case 'Fri':
                            dow.push('5');
                            break;
                        case 'Sat':
                            dow.push('6');
                            break;
                    }
                }
            });

            if (dow.length == 0) {
                swal('Please choose day of week schedule.');
                return;
            }

        } else {

            row.isonetime = '1';
            var onetimeval = $('#services_modal #svc_Onetimedate').val();
            if (onetimeval == '') {
                swal("Please choose a one time date schedule.");
                return;
            }


            var onetimeday = moment(onetimeval).day();
            if ($.isNumeric(onetimeday))
                dow.push(onetimeday.toString());


            row.ScheduleTime = onetimeval + ' ' + $('#services_modal #svc_start_time').val();

            var today = new Date(Date.now());
            var new_schedTime = new Date(row.ScheduleTime);

            if (new_schedTime < today) {
                swal("Selected date and time have passed already. Please select again.", "", "warning");
                return;
            }

        }

        row.Recurrence = dow.join(',');
        row.ServiceTasks = svcs.join(','); 
        row.StartDate = $("#svc_start_date").val();
        row.EndDate = $("#svc_end_date").val();
        row.StartTime = $("#svc_start_time").val();
        row.EndTime = $("#svc_end_time").val();
        row.ScheduleRemarks = $("#scheduleRemarks").val();
        row.Id = $("#txtId").val();
       
        row.PresentServiceDate = moment($('#services_modal #presentServiceDate').val()).format("MM/DD/YYYY");


        if (row.ServiceTasks == "") {
            swal("Please select services.", "", "warning");
            return;
        }

        if (row.PresentServiceDate == "Invalid date") {
            row.PresentServiceDate = moment(Date.now()).format("MM/DD/YYYY");
        }

        row.SchedStartTime = row.PresentServiceDate + ' ' + $("#svc_start_time").val();
        row.SchedEndTime = row.PresentServiceDate + ' ' + $("#svc_end_time").val();
        row.PresentServiceDate = moment($('#services_modal #presentServiceDate').val()).format("MM/DD/YYYY hh:mm");

        //row.clientdt = moment($('#services_modal #svc_Onetimedate').val()).format("MM/DD/YYYY");
        if ($('#services_modal #svc_txtCarestaff').val() == "") {
            swal("Please select a carestaff.")
            return;
        }


        if (new Date(row.StartDate) > new Date(row.EndDate)) {
            swal("Invalid date range!", "Start Date should be earlier than the End Date", "warning");
            return;
        }

        if (new Date(row.SchedStartTime) > new Date(row.SchedEndTime)) {
            swal("Invalid time range!", "Start Time should be earlier than the End Time", "warning");
            return;
        } 
        
    }
  
    if (mode == 2) {
      //  debugger;
        transchedState.current = JSON.stringify(row);
        if (transchedState.current == transchedState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }
        //var check_completed_length = $('#services_modal .completion_confirmation').filter(':checked').length;
        //var check_completed_value = $('#services_modal .completion_confirmation').filter(':checked').val();
        var date_of_posting = $("#svc_posted_date").val();
        var scheduleRemarks = $("#scheduleRemarks").val();
        var actual_start_time = $("#svc_actual_start_time").val();
        var actual_end_time = $("#svc_actual_end_time").val();

        if (edit_detail == "forcompletion") {

            row.ForCompletion = 1;
          
            if (date_of_posting != "" && actual_start_time != "" && actual_end_time != "") {
                row.ActualStartTime = date_of_posting + ' ' + actual_start_time;
                row.ActualEndTime = date_of_posting + ' ' + actual_end_time;

                if (new Date(row.ActualStartTime) >= new Date(row.ActualEndTime)) {
                    swal("Actual Start Time should be earlier than the Actual End Time", "Please try again", "warning");
                    return false;
                }

                if (scheduleRemarks == "") {    
                    swal("Please add remarks for the services rendered.", "", "warning");
                    return false;
                }; 
            } else {
                swal("Please fill the date of posting  and the actual time of completion fields.", "", "warning");
                return false;
            }

        } else if (edit_detail == "re") { // if Reschedule or Reassign service
          //  debugger;
            row.isonetime = "1";
            var onetimeval = moment($('#presentServiceDate').val()).format("MM/DD/YYYY");

            row.ScheduleTime = onetimeval + ' ' + $('#new_svc_start_time_re').val();
            row.ScheduleEndTime = onetimeval + ' ' + $('#new_svc_end_time_re').val();
            row.CarestaffId = $("#svc_txtCarestaff_re").val();

            //this is for rescheduling/reassignment
            //row.ScheduleTime = moment(row.ScheduleTime).format("MM/DD/YYYY hh:mm");
            //row.ScheduleEndTime = moment(row.ScheduleEndTime).format("MM/DD/YYYY hh:mm");
          

            is_resched = $("#is_resched").val();
          //  debugger;
            if (is_resched == 1) {
                row.IsResched = 1; //resched
            } else {
                row.IsResched = 2; //reassign
            }
            
        }

    }


    var data = {
        //startDate: soc,
        //endDate: row.,
        carestaff_id: row.CarestaffId,
        resident_id: row.ResidentId
    };


    var proceed = 0;
    if (mode == 1) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Transsched/CheckConflictForSOC",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (parseInt(m) == 1) {
                    swal("There is a conflict with the client's start and end of care.", "", "warning");
                    return false;

                } else {

                    var data = {
                        func: "activity_schedule_svc",
                        mode: mode,
                        data: row
                    };
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/PostGridData",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (m) {
                            if (cb) cb(m);
                        }
                    });
                }


            }
        });
    } else {
       
        var data = {
            func: "activity_schedule_svc",
            mode: mode,
            data: row
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }
   
   
   
}

function sort(a) {
    for (var i = 0; i < a.length - 1; i++) {
        if (a[i].score > a[i + 1].score) {
            var temp = a[i]
            a[i] = a[i + 1]
            a[i + 1] = temp
        }
    }
    return a;
}

function getHeight() {

    $('.body-height').height($(window).height() - 250);
    $(window).resize(function () {
        $('.body-height').height($(window).height() - 250);
    })


    $('.dataTables_scrollBody').height($(window).height() - 200);
    $(window).resize(function () {
        if ($(window).height() > 400) {
            $('.dataTables_scrollBody').height($(window).height() - 200);

        } else {
            $('.dataTables_scrollBody').height($(window).height() - 200);

        }

    })

    if (isCalendarView == 1) {
        $('#transchedContainer').height($(window).height() + 500);
        $(window).resize(function () {
            $('#transchedContainer').height($(window).height() + 500);
        })
    } else {
        $('#transchedContainer').height($(window).height() + 100);
        $(window).resize(function () {
            $('#transchedContainer').height($(window).height() + 100);
        })
    }

    var scroll = new PerfectScrollbar('.dataTables_scrollBody');
    var scroll1 = new PerfectScrollbar('.portlet__body');
    $('#weekly').DataTable().columns.adjust();
}

var buildCalendarForHMC = function () {

    var month = $('#monthfilter').val();
    var year = $('#yrfilter').val();
    var month_year, myDate = "";

    minDate = getMonthDateRange(year, month);
    $("#calendar").fullCalendar('destroy');
    buildCalForHMC(minDate);

    var m = moment([year, month - 1, 1]).format('YYYY-MM-DD');
    $('#calendar').fullCalendar('gotoDate', m);
}

function buildCalForHMC(date, caregiver) {

    var facilityId = null,
        roomId = null;
    var fr = $('#filterExp').val();
 
    caregiver_ftr_val = $("#ftrCaregiver").val();
    var data = {
        startDate: date.start,
        endDate: date.end,
        filter: caregiver_ftr_val
    };

    $.ajax({
        url: "Transsched/GetServicesCalendar",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        async: false,
        data: JSON.stringify(data),
        success: function (response) {
            
            if (response) {
                var events = [];
                console.log(response.Appointments_planned_posted)
                $.each(response.Appointments_planned_posted, function (i, item) {
                    
                    //new_date = item.date.replaceAll('/', '-');
                    var color = "#24dc006b";
                  
                    //FIRST PUSH FOR PLANNED SERVICES
                    events.push({
                        title: item.start_time + "-" + item.end_time + " - " + item.resident,
                        name: item.resident,
                        start: item.planned_date,
                        date: item.planned_date,
                        id: item.client_id,
                        allDay: false,
                        color: color,
                        planned_service_id: item.planned_service_id,
                        carestaff_id: item.carestaff_id,
                        carestaff: item.carestaff,
                        resident_id: item.client_id,
                        resident: item.resident,
                        //name: item.name,

                        //start: new Date(item.date),
                        end_date: new Date(item.end_date),
                        start_time: item.start_time,
                        end_time: item.end_time,

                        services_rendered: item.services_rendered,

                        planned_posted_service_id: item.planned_posted_service_id,
                        posted_date: item.posted_date,
                        planned_date: item.planned_date,
                        actual_start_time: item.actual_start_time,
                        actual_end_time: item.actual_end_time,
                        status: "planned",
                        textColor: '#3f4047',
                        // time: time
                    });
                   // debugger;
                    var stat = "posted";
                    var title_posted = item.start_time + "-" + item.end_time + " - " + item.resident;
                    var eventTextColor = "#3f4047";
                    if (item.posted_date != null) { //posted
                        color = "#34bfa3c9";
                        title_posted = item.actual_start_time + "-" + item.actual_end_time + " - " + item.resident;
                    } else {
                        //set color for for posting service
                        //color = "#c9cbd94f";
                        color = "#ebedf2";
                        stat = "for_posting";
                        eventTextColor = "#ebedf2";
                    } 

                    

                     //DUPLICATE FOR POSTED/FOR POSTING SERVICES
                    events.push({
                        title: title_posted,
                        name: item.resident,
                        start: item.planned_date,
                        date: item.planned_date,
                        id: item.client_id,
                        allDay: false,
                        color: color,
                        planned_service_id: item.planned_service_id,
                        carestaff_id: item.carestaff_id,
                        carestaff: item.carestaff,
                        resident_id: item.client_id,
                        resident: item.resident,
                        //name: item.name,

                        //start: new Date(item.date),
                        end_date: new Date(item.end_date),
                        start_time: item.start_time,
                        end_time: item.end_time,

                        services_rendered: item.services_rendered,

                        planned_posted_service_id: item.planned_posted_service_id,
                        posted_date: item.posted_date,
                        planned_date: item.planned_date,
                        actual_start_time: item.actual_start_time,
                        actual_end_time: item.actual_end_time,
                        status: stat,
                        textColor: eventTextColor
                        // time: time
                    });


                });
               

                $("#calendar").fullCalendar('destroy');
                if (rlId == 1) {
                    GenerateCalendarForHMC(events);
                } else {
                    GenerateCalendarForHMCStaff(events);
                }


            }
        }
    });
}

function EventClickCalendar(event) {
    //Set mode to edit
    mode = "2_forcompletion";
    $(".hcmodalheaderheight_mobile").removeClass("isAddService");

    $(".for_add_service").prop("hidden", true);
    $(".for_edit_service").prop("hidden", false);
    $(".for_completion_service").prop("hidden", false);

    var string = "";
    var e_date = new Date(Date.now());

    var data = {
        planned_posted_service_id: event.planned_posted_service_id,
        date: e_date,
        name: event.name,
        status: event.status
    };

    $.ajax({
        url: "Transsched/GetServiceScheduleData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        data: JSON.stringify(data),
        success: function (response) {
            //   debugger;
            //disable input fields when event is clicked : angelie, 05-02-22
            var form = document.getElementById("services_modal_form");
            var elements = form.elements;
            for (var i = 0; i < elements.length; i++) {
                elements[i].disabled = true;
            }

            var obj1 = response["data"][0];
            var obj2 = response["service"];

            $("#txtId").val(obj1.planned_posted_service_id);

            $("#svc_txtCarestaff").val(obj1.carestaff_id);
            $("#svc_txtResidentId").val(obj1.client_id);
            $("#svc_start_date").val(obj1.soc);
            $("#svc_end_date").val(obj1.eoc);
            $("#svc_start_time").val(obj1.start_time);
            $("#svc_end_time").val(obj1.end_time);
            $("#svc_onetime").val(obj1.is_onetime);
            $("#svc_posted_date").val(obj1.posted_date);
            $("#svc_actual_start_time").val(obj1.actual_start_time);
            $("#svc_actual_end_time").val(obj1.actual_end_time);

            // uncomment the first line coz it did not return value for remarks and add the second line : 9-12-2022 : Cheche
            // $("#scheduleRemarks").val(obj1.comment);
            $("#scheduleRemarks").val(obj1.remarks);
            $("#scheduled_date").html('(' + event.date + ')');
            $("#scheduled_time").html('(' + obj1.start_time + ' - ' + obj1.end_time + ')');

            //automatic assign it for reassign
            $("#new_svc_start_time_re").val(obj1.start_time);
            $("#new_svc_end_time_re").val(obj1.end_time);
            $("#svc_txtCarestaff_re").val(obj1.carestaff_id);


            $("#scheduled_date").val(event.date);
            $("#sched_start_time").val(obj1.start_time);
            $("#sched_end_time").val(obj1.end_time);

            $("#services_modal_lbl").html("Carestaff - " + obj1.carestaff);
            $(".is_required").prop("hidden", true);

            //$(".for_add_service").prop("hidden", false);
            $(".for_edit_service").prop("hidden", false);


            if (obj1.posted_date != null) {

                $(".edit_service_btn").prop("disabled", true);
                $(".delete_service_btn").prop("disabled", true);
                $("#save_newService").prop("disabled", true);
                $(".reassign_service_btn").prop("disabled", true);
                //enable fields for completion of task: angelie, 08-01-22
                $(".completion_confirmation").prop("disabled", true);
                $("#scheduleRemarks").prop("disabled", true);
                $(".for_completion_question").attr("hidden", true);
                $(".completion_confirmation").prop("disabled", true);

            } else {
                $(".for_completion_question .completion_div").attr("hidden", false);
                $(".edit_service_btn").prop("disabled", false);
                $(".delete_service_btn").prop("disabled", false);
                $("#save_newService").prop("disabled", false);
                $(".reassign_service_btn").prop("disabled", false);
                //enable fields for completion of task: angelie, 08-01-22
                $(".completion_confirmation").prop("disabled", false);
                $("#scheduleRemarks").prop("disabled", false);
                $(".for_completion_question").attr("hidden", false);
                $(".completion_confirmation").prop("disabled", false);
            }

            if (event.status == "for_posting") {
                $(".edit_service_btn").prop("disabled", true);
                $(".delete_service_btn").prop("disabled", true);
                $(".reassign_service_btn").prop("disabled", true);
            }
            recurrence = obj1.recurrence.split(",");
            //populate values for the assigned services
            for (var i = 0; i < recurrence.length; i++) {
                $('#services_modal_form .dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);

                    var day = parseInt($(this).val());
                    if (day == recurrence[i]) {
                        d.attr("checked", true);
                    };
                })
            }

            //populate values for the assigned services
            for (var i = 0; i < obj2.length; i++) {
                $('#services_modal_form .services').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    var activity_id = parseInt($(this).val());
                    if (activity_id == obj2[i]) {
                        d.attr("checked", true);
                    };

                })
            }
            $('#services_modal').modal();


            //enable reschedule and reassign if calendar date is equal to present date: angelie, 05-20-22
            event_date = new Date(event.date);
            e_date = e_date.setHours(0, 0, 0, 0);
            event_date = event_date.setHours(0, 0, 0, 0);
            p_date = new Date(obj1.posted_date);
            p_date = p_date.setHours(0, 0, 0, 0);
            if (+e_date === +event_date && p_date != e_date && event.status != "for_posting") {
                //$(".reschedule_service_btn").prop("disabled", false);
                $(".reassign_service_btn").prop("disabled", false);
            } else {
                //$(".reschedule_service_btn").prop("disabled", true);
                $(".reassign_service_btn").prop("disabled", true);
            }
            //  debugger;

            if (e_date > event_date) {
                $(".edit_service_btn").prop("disabled", true);
                $(".delete_service_btn").prop("disabled", true);
            } else {
                $(".edit_service_btn").prop("disabled", false);
                $(".delete_service_btn").prop("disabled", false);
            }


            //Altered some codes for consistency in opening "Planned Service" tile and to disabled button after posting : 9-12-2022 : Cheche
            if (event.status == "planned" && obj1.posted_date == null) {
                $(".completion_div").attr("hidden", true);
                $(".reassign_service_btn").prop("disabled", false);
            } else if (event.status == "planned" && obj1.posted_date != null) {
                $(".completion_div").attr("hidden", true);
                $(".edit_service_btn").prop("disabled", true);
                $(".delete_service_btn").prop("disabled", true);
            } else if (event.status == "posted" && obj1.posted_date != null) {
                $(".completion_div").attr("hidden", false);
                $(".edit_service_btn").prop("disabled", true);
                $(".delete_service_btn").prop("disabled", true);
            } else {
                $(".completion_div").attr("hidden", false);
            }
        }
    });
}

function GenerateCalendarForHMC(events) {

    $("#calendar").fullCalendar({
        editable: true,
        defaultView: 'month',
        header: {
            left: '',
            center: 'prev,title,next',
            right: ''
        },
        buttonText: {
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
        },
        eventLimit: true,
        //eventColor: '#34bfa3',
        editable: false,
        events: events,
        minTime: 0,
        maxTime: 24,
        displayEventTime: false,
        fixedWeekCount: true,
        //timeFormat: "h(:mm)t",
       
        eventClick: function (event, jsEvent, view) {
            
       //   debugger;
            EventClickCalendar(event);

        }
    });
}

function GenerateCalendarForHMCStaff(events) {
    //console.log(events)
    $('#calendar').fullCalendar({
        defaultView: 'listMonth',
        events: events,
        eventRender: function (event, element) {
            element.css({
                'display': 'block',
                'margin': '1%',
                'width': '48%',
                'float': 'left',
            });
            element.find('.fc-list-item-marker').css('vertical-align', 'middle');
            element.find('.fc-list-item-title').html("<h6>" + event.start_time + " - " + event.end_time + "</h6><h6 class='resNamehcc'>" + event.resident + "</h6>");
            
            if (event.color == "#24dc006b") { //styles for planned event (green)
                element.css('background-color', 'rgba(36, 220, 0, 0.15)');
                element.find('.fc-list-item-marker').html('<ins class="jstree-icon cal-icon fas fa-paste fa-fw">&nbsp;</ins>');
                element.find('.fc-list-item-marker').css('background-color', 'rgba(36, 220, 0, 0.25');
            } else { //styles for posting event (gray)
                element.css('background-color', 'rgba(232, 232, 232, 0.5)');
                element.find('.fc-list-item-marker').html('<ins class="jstree-icon cal-icon fas fa-edit">&nbsp;</ins>');
                element.find('.fc-list-item-marker').css('background-color', 'rgba(232, 232, 232, 1)');
            }

            if (event.posted_date != null) { //posted
                element.css('background-color', 'rgba(52, 191, 163, 0.2)');
                element.find('.fc-list-item-marker').css('background-color', 'rgba(52, 191, 163, 0.5)');
                element.find('.fc-list-item-marker').html('<ins class="jstree-icon cal-icon fas fa-calendar-check">&nbsp;</ins>');                
                element.find('.fc-list-item-title').html("<h6>" + event.actual_start_time + " - " + event.actual_end_time + "</h6><h6 class='resNamehcc'>" + event.resident + "</h6>");
            }
        },      
        displayEventTime: false,
        eventAfterAllRender: function (view) {
            //console.log(view);
            $("#calendar .fc-listMonth-view .fc-scroller").css({ 'min-height': '200px' ,'height': '' });
        },
        eventClick: function (event, jsEvent, view) {
            EventClickCalendar(event);
        }
    });


}



$(document).ready(function () {
    rlId = $("#roleID").val();
    isHomeCare = $("#isHomeCare").val();

    // Change UI if Staff -----------------------------------------------
    if (isHomeCare == "True") {
        if (rlId != 1) {
            $(".hcmobileftr1").addClass("col-xl-4 col-lg-4 col-md-4 col-xs-6 col-sm-6 col-6 hcmobilestyle");
            $(".hcmobileftr2").addClass("col-xl-4 col-lg-4 col-md-4 col-xs-12 col-sm-12 col-12 hcmobilestyle");
            $("#add_services_btn").removeClass("m-btn--pill m-btn--air btn-primary");
            $("#add_services_btn").addClass("btn-secondary");
            $("#monthfilter, #yrfilter, #ftrCaregiver").removeClass("form-control");
            $("#monthfilter, #yrfilter, #ftrCaregiver").addClass("custom-select");
            $("#monthfilter, #yrfilter, #ftrCaregiver").css({ 'padding': '' });
            $(".services_mobile").addClass("col-xl-4 col-lg-4 col-md-6 col-xs-6 col-sm-6 col-6");
            $(".hcmodalheader_mobile").addClass("col-xl-6 col-lg-6 col-md-12 col-xs-12 col-sm-12 col-12");
        }
    }
    //------- End of change UI for Homecare staff -------------------------- 

    //$("#caregiver_ftr_val").val("0");
    $("#caregiver_ftr_val").val($("#ftrCaregiver option:first").val());
    

    $('#svc_start_time,#svc_end_time, #new_svc_start_time_re, #new_svc_end_time_re, #svc_actual_start_time, #svc_actual_end_time').timepicker({
        //'timeFormat': 'H:i',
        //defaultTime: '',
        //showPeriod: true,
        //showLeadingZero: true
        timeFormat: 'hh:mm tt',
        defaultTime: '12:00',
        showPeriod: true,
        showLeadingZero: true,
    });


    $('#services_modal #svc_Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#services_modal #svc_Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff2(dow);
    });

    //$('#services_modal #svc_start_time').change(function () {
        
    //    if ($('#svc_onetime').is(':checked') == true && $('#services_modal #svc_Onetimedate').val() != "") {
    //        var dow = [];
    //        dow[0] = moment($('#services_modal #svc_Onetimedate').val()).day();
    //        //bindCarestaff2(dow);
    //    } else if ($('#svc_reccur').is(':checked') == true) {
    //        triggerDayofWeekClick();
    //    }

    //});

    $('#services_modal input[name=recurring]').change(function () {
        $('#services_modal #svc_txtCarestaff')[0].options.length = 0;
    });

    $('#services_modal input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('#services_modal table.dayofweek input').removeAttr('disabled');
            $('#services_modal #svc_Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('#services_modal table.dayofweek input').attr('disabled', 'disabled');
            $('#services_modal table.dayofweek').clearFields();
            $('#services_modal #svc_Onetimedate').removeAttr('disabled');
        }
    });

    $("#add_services_btn").click(function () {
        if (rlId != 1) {
            $(".hcmodalheaderheight_mobile").addClass("isAddService");
        } 

      //  debugger;
        mode = 1;
        $("#services_modal_lbl").html("");
        $(".for_add_service").prop("hidden", false);
        $(".for_edit_service").prop("hidden", true);
        //$(".for_completion_service").prop("hidden", true);
        $(".completion_div").prop("hidden", true);

        //enable input fields when add new service is clicked : angelie, 05-02-22
        var form = document.getElementById("services_modal_form");
        var elements = form.elements;
        for (var i = 0; i < elements.length; i++) {
            elements[i].disabled = false;
            //elements[i].value = "";
        }
        form.reset();
        $('#services_modal_form .services').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            d.attr("checked", false);
        });

        $('#services_modal_form .dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            d.attr("checked", false);
        })
        $(".for_completion_service").prop("hidden", true);
        $("#save_newService").prop("disabled", false);

        //comment out this one & enable start and end date input field upon clicking "Add Services" : 9-14-2022 : Cheche
        // $("#svc_start_date").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" }); 
        // $("#svc_end_date").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
        $("#svc_start_date").css({ "pointer-events": "unset", "background-color": "#ffffff", "opacity": "1" }); 
        $("#svc_end_date").css({ "pointer-events": "unset", "background-color": "#ffffff", "opacity": "1" });

    });

    $(".add_service_btn").click(function () {
     //   debugger;
        //enable input fields when add new service is clicked : angelie, 05-02-22
        var form = document.getElementById("services_modal_form");
        var elements = form.elements;
        for (var i = 0; i < elements.length; i++) {
            elements[i].disabled = false;
            elements[i].value = "";
        }

        $('#services_modal_form .services').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            d.attr("checked", false);
        });

        $('#services_modal_form .dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            d.attr("checked", false);
        })
        $(".for_completion_service").prop("hidden", true);
       

    });

    $(".edit_service_btn").click(function () {

        mode = "2_foreditservice";
        $(".for_completion_service").prop("hidden", true);


        //disable input fields when edit  service is clicked : angelie, 05-02-22
        var form = document.getElementById("services_modal_form");
        var elements = form.elements;

        for (var i = 0; i < elements.length; i++) {
            elements[i].disabled = true;
        };

        $('#services_modal_form .services').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            d.attr("disabled", false);
        });
       

    });

    $('#save_newService').click(function () {
   // debugger;
        row = "";
        doAjaxTask(mode, row, function (m) {

            if (m.result == 0) {
                setTimeout(function () {
                   
                    toastr.success("Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!");
                    $(".cancel").click();
                    $("#ftrCaregiver").val(carestaff_id).trigger("change");
                    

                })

            } else if (m.result == -2) {
                var resVisible = $('#txtResidentId').is(':visible');
                //there is already a schedule for a particular resident or room
                toastr.warning("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".");
            }
        })    
    });
    //kim add function after close it should reset the form 07/28/22
    $('.cancel').click(function () {
        var form = document.getElementById("services_modal_form");
        var elements = form.elements;
        for (var i = 0; i < elements.length; i++) {
            elements[i].disabled = false;
            //elements[i].value = "";
        }
        form.reset();
        $('#services_modal_form .services').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            d.attr("checked", false);
        });

        $('#services_modal_form .dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            d.attr("checked", false);
        })
    })

    $('#save_service_re').click(function () {

        mode = "2_re";
        row = "";
        is_resched = $("#is_resched").val();

        if (carestaff_orig == $("#svc_txtCarestaff_re").val()) {
            swal("There are no changes to be saved.", "", "warning");
            return;
        }

        doAjaxTask(mode, row, function (m) {
        
            if (m.result == 0) {
                setTimeout(function () {
                    toastr.success("Task " + (is_resched == 1 ? 'rescheduled' : 'reassigned') + " successfully!");
                    $(".cancel").click();
                    $("#calendar_view_hmc").trigger("click");
                })

            } else if (m.result == -2) {
                var resVisible = $('#txtResidentId').is(':visible');
                //there is already a schedule for a particular resident or room
                toastr.warning("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".");
            }
        })    

    });

    $("#calendar_view_hmc").on("click", function () {
        dpicker = $("#curday").text();
        wpicker1 = $("#wstartDate").text();
        wpicker2 = $("#wendDate").text();

        setTimeout(function () {
            buildCalendarForHMC();
        }, 100);

        isCalendarView = 1;
        $(".calviewftr").show();
        $(".legend").show();
        $("#legend_list").show();
        $(".cal_header").show();
        //$(".filtr").hide();
        $("#weekly_wrapper").hide();
        $("#calendar").show();
        $(".calendarbtn").hide();
        $(".taskviewftr").hide();
        //$(".currentTaskbtn").show();
        //$("#cd_label").text("Task List Daily View");
        //$("#cw_label").text("Task List Weekly View");

        $(".for_homecare").prop("hidden", false);
        $(".for_alc_only").prop("hidden", true);

        getHeight();
        $(".dw_btns").css("padding-right", "5px");
    });

    $(".delete_service_btn").on("click", function () {

        id = $("#txtId").val();
        row = {
            Id: id
        };

        mode = 3;


        swal({
            title: "Are you sure you want to delete this data?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes"
        }).then(function (e) {
            e.value &&
                doAjaxTask(mode, row, function (m) {

                    if (m.result == 0) {
                        setTimeout(function () {
                            toastr.success("Service deleted successfully!");
                            $(".cancel").click();
                            $("#calendar_view_hmc").trigger("click");
                        })

                    } else if (m.result == -2) {
                        var resVisible = $('#txtResidentId').is(':visible');
                        //there is already a schedule for a particular resident or room
                        toastr.warning("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".");
                    }
                })

        });

      

    });

    //$(".reschedule_service_btn").on("click", function () {
        
    //    $("#is_resched").val(1);

    //    row = $('#services_modal').extractJson();
      
    //    $(".for_reassign_service").prop("hidden", true);
    //    $(".for_resched_service").prop("hidden", false);
    //    $('#resched_reassign_modal').modal();


    //    id = $("#txtId").val();
    //    start_date = $("#scheduled_date").val();
    //    start_time = $("#sched_start_time").val()
    //    end_time = $("#sched_end_time").val();

    //    $("#activity_schedule_id").val(id);
    //    $("#old_svc_start_date").val(start_date);
    //    $("#svc_start_time_re").val(start_time);
    //    $("#svc_end_time_re").val(end_time);
    //    $("#resched_reassign_modal_lbl").html("Reschedule Service");

    //    //$("#svc_txtResidentId").attr("disabled", true);

    //});

    $(".reassign_service_btn").on("click", function () {
     //   debugger;
        row = $('#services_modal').extractJson();


        $("#is_resched").val(0);
        $('#resched_reassign_modal').modal();
        $(".for_completion_service").prop("hidden", true);
        $(".for_reassign_service").prop("hidden", false);
        $(".for_resched_service").prop("hidden", true);

        $("#svc_txtResidentId_re").val($("#svc_txtResidentId").val());

        carestaff_orig = $("#svc_txtCarestaff").val();
        $("#svc_txtCarestaff_re").val(carestaff_orig);
        $("#svc_txtResidentId_re").prop("disabled", true);
        $("#resched_reassign_modal_lbl").html("Reassign Service");

    });

    $("#ftrCaregiver").on("change", function(){
        caregiver_ftr_val = $(this).val();
        $("#calendar_view_hmc").trigger("click");
    })

    $("#svc_txtResidentId").on("change", function () {
    // debugger;


        var data = {
            resident_id: $(this).val()
        };
      
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Transsched/GetSOCAndEOC",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
             //   debugger;
                    // $("#svc_start_date").val(m[0].date_admitted);
                    // $("#svc_end_date").val(m[0].discharge_date);
                    // $("#svc_start_date").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                    // $("#svc_end_date").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                     
                    // enabled end date if value is null upon selecting residents: 9-14-2022 : Cheche
                    if(m[0].date_admitted == null){
                        $("#svc_start_date").attr('disabled', false);
                    }else if($("#svc_start_date").val(m[0].date_admitted) != null){
                       $("#svc_start_date").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                    }

                    if(m[0].discharge_date == null){
                        $("#svc_end_date").attr('disabled', false);
                        $("#svc_end_date").css({ "pointer-events": "unset", "background-color": "#ffffff", "opacity": "1" });
                        $("#svc_end_date").val(m[0].discharge_date).clear();
                    }else if(m[0].discharge_date != null){
                         $("#svc_end_date").val(m[0].discharge_date);
                         $("#svc_end_date").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                    }else{
                        $("#svc_end_date").css({ "pointer-events": "none", "background-color": "#e9ecef", "opacity": "1" });
                    }
                
                    
                }
    
            });

    })

    $("#svc_txtCarestaff").on("change", function () {
      //  debugger;
        // 09-05-22, angelie
        //Added this to store the value of the carestaff chosen for the rendering of the calendar after saving of the added services
        carestaff_id = $(this).val();
    })
    
}) 