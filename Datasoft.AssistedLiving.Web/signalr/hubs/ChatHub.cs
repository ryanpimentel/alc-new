﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Datasoft.AssistedLiving.Web.signalr.hubs
{
    public class ChatHub : Hub
    {
        public void Send(string name, string message, string cid)
        {
            Clients.All.addNewMessageToPage(name, message, cid);
        }
    }
}