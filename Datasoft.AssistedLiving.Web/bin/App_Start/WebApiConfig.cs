﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Datasoft.AssistedLiving.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            // 6-29-2022 : Cheche to enable CORS
            //  config.EnableCors(); // Removing this because we only enable CORS once

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
