﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class ErrorController : Controller
    {
        private string UserID
        {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID
        {
            get { return User.Identity.Name.Split('|')[1]; }
        }
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogError(string module, string message)
        {
            try
            {
                MailMessage mail = new MailMessage();

                //TO BE MODIFIED
                mail.To.Add("ryan.pimentel@datasoftlogic.com, mariel.pulmones@datasoftlogic.com, alona.cabrias@datasoftlogic.com, angelie.alasian@datasoftlogic.com, ignacio.taladua@datasoftlogic.com, cheche.acebido@datasoftlogic.com, kimberly.barrogo@datasoftlogic.com");
            
                mail.From = new MailAddress("dslalc@datasoftlogic.com");
                mail.Subject = "ERROR FOUND";

                mail.Body = Convert.ToString(DateTime.UtcNow);
                mail.Body += "<br><br>User ID: <b>" + UserID + "</b><br>Agency ID: <b>" + AgencyID + "</b><br>Module / Function: <b>" + module + "</b>";
                mail.Body += "<br><br>" + message;

                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Credentials = new System.Net.NetworkCredential
                     ("dslalc@datasoftlogic.com", "dslalciloilo17");
                smtp.Port = 587;

                smtp.EnableSsl = true;
                smtp.Send(mail);

                return null;
            }
            catch (Exception E)
            {
                throw E;
            }


        }

    }
}
