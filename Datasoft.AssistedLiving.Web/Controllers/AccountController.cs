﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web;
using DataSoft.Helpers;
using Datasoft.Ext;
using System.Web.Routing;
using System.Web.Security;
using Datasoft.AssistedLiving.Security;
using Datasoft.AssistedLiving.Web.Models;
using Datasoft.AssistedLiving.Web.Helper;
using System.Net.Mail;

namespace Datasoft.AssistedLiving.Web.Controllers {
    [AllowAnonymous]
    public class AccountController : Controller {
        public ActionResult ResetPassword() {
            if (Request.IsAuthenticated) {
                Session.Abandon();
                FormsAuthentication.SignOut();
            }

            ViewBag.VerifyCodeConfirmed = false;
            ViewBag.FL = false;

            var id = RouteData.Values["id"];
            if (id != null) {
                var strID = id.ToString();
                if(Request["FL"] != null) {
                    ViewBag.FL = true;
                }
                byte[] decodedData = HttpServerUtility.UrlTokenDecode(strID);
                string decodedText = Convert.ToBase64String(decodedData);
                string[] data = decodedText.Decrypt().Split("|".ToArray());
                var agency = data[0];
                var userid = data[1];
                var verifycode = data[2];
                string constring = ALCUserManager.Instance.ConnectionString(agency);
                using (var ctx = new ALSBaseDataContext(constring)) {
                    var user = ctx.ALCUsers.SingleOrDefault(x => x.user_id == userid);
                    if(Request["FL"] != null) {

                    }else if(user != null && user.verify_code == verifycode) {
                        if(DateTime.UtcNow.Subtract(user.verify_created_date.Value).TotalMinutes > 60.0)
                            return new HttpNotFoundResult("Page cannot be found.");
                    }else
                        return new HttpNotFoundResult("Page cannot be found.");
                }
                ViewBag.VerifyCodeConfirmed = true;
                ViewBag.userid = userid;
                ViewBag.agencyid = agency;
            }
            return View();
        }

        
        public ActionResult RegisterGuest()
        {
            return View();
        }

        public ActionResult ActivateGuest()
        {
            return View();
        }


        [HttpPost]
        public JsonResult SendResetInstruction([DynamicJson(MatchName = false)] dynamic obj) {
            if (obj != null) {
                var result = SendInstruction(obj);
                return Json(result);
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult SendGuestVerification([DynamicJson(MatchName = false)] dynamic obj)
        {
            if (obj != null)
            {
                var result = SendVerification(obj);
                return Json(result);
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult SendGuestActivation([DynamicJson(MatchName = false)] dynamic obj)
        {
            if (obj != null)
            {
                var result = SendActivation(obj);
                return Json(result);
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult ChangePassword([DynamicJson(MatchName = false)] dynamic obj)
        {
            var result = JsonResponse.GetDefault();
            if (obj != null)
            {
                string agency = obj.Agency;
                string userid = obj.Id;
                string pass = obj.Password;
                string cpass = obj.ConfirmPassword;
                //if password supplied let's change it and validate first
                if (pass != cpass)
                {
                    result = new JsonResponse(JsonResponseResult.Failed, "Confirm Password did not match.");
                }
                if (!string.IsNullOrEmpty(pass))
                {
                    if (!System.Text.RegularExpressions.Regex.IsMatch(pass, @"\w{8,100}"))
                        result = new JsonResponse(JsonResponseResult.Failed, "Password must be at least 8 or more alphanumeric characters.");
                }
                if (!string.IsNullOrEmpty(pass))
                {
                    string constring = ALCUserManager.Instance.ConnectionString(agency);
                    using (var ALSDB = new ALSBaseDataContext(constring))
                    {
                        var user = ALSDB.ALCUsers.SingleOrDefault(x => x.user_id == userid);
                        user.password = obj.Password;
                        user.verify_code = null;
                        user.verify_created_date = null;
                        user.force_reset_password = null;
                        ALSDB.SubmitChanges();
                        result = new JsonResponse(JsonResponseResult.Success, "");
                    }
                }
            }
            return Json(result);
        }

        private dynamic SendInstruction([DynamicJson] dynamic data) {
            string AgencyId = data.AgencyId,
                   UserId = data.UserId,
                   Email = data.Email;

            string constring = ALCUserManager.Instance.ConnectionString(AgencyId);
            if (string.IsNullOrEmpty(constring)) {
                return new {
                    Result = - 1,
                    ErrorMsg = "AgencyID does not have a match."//Send and don' show error // "Invalid Agency ID"
                };
            }

            using (var ctx = new ALSBaseDataContext(constring)) {
                var user = (from u in ctx.ALCUsers
                            join ur in ctx.ALCUserRoles on u.user_id equals ur.user_id
                            join r in ctx.ALCRoles on ur.role_id equals r.role_id
                            where u.user_id == UserId && u.email == Email //&& u.password == Password
                            select new { u.first_name, u.user_id, u.password, ur.role_id, u.is_active }).SingleOrDefault();

                //1. check if creds are correct and check case sensitivity
                if (user == null) { // || user.user_id != UserId || user.password != Password) {
                    return new {
                        Result = - 1,
                        ErrorMsg = "Please enter your correct username and registered email."// "Invalid User ID or Password"
                    };
                }

                //Read template and send using Mail helper class

                string localPath = Server.MapPath("~/Views/Account/PasswordResetEmailNotification.cshtml");
                string content = System.IO.File.ReadAllText(localPath);
                string firstname = user.first_name ?? "";
                if (!string.IsNullOrEmpty(firstname))
                    firstname = firstname.First().ToString().ToUpper() + firstname.Substring(1);
                var random = new Random();
                string verifycode = Ext.Exts.GetVerifyCode(6, random);

                var ux = ctx.ALCUsers.Where(x => x.user_id == user.user_id).SingleOrDefault();
                if (ux != null) {
                    ux.verify_code = verifycode;
                    ux.verify_created_date = DateTime.UtcNow;
                    ctx.SubmitChanges();
                }

                string code = string.Format(AgencyId + "|" + user.user_id + "|" + verifycode);
                
                content = content.Replace("<%=TP_Name%>", firstname);
                content = content.Replace("<%=TP_CODE%>", verifycode);
                content = content.Replace("<%=TP_HASH%>", HttpServerUtility.UrlTokenEncode(code.EncryptToByte()));
                content = content.Replace("<%=TP_SITE%>", string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
                //#if DEBUG
                //                //empty this so it will use the email override from config
                //                Email = "";
                //#endif
                //                try {
                //                    //send mail
                //                    var mH = new MailHelper() {
                //                        Subject = "Reset Password Request",
                //                        Recipient = Email,
                //                        Body = content
                //                    };
                //                    mH.Send();
                //                } catch {
                //                    return new {
                //                        Result = -1,
                //                        ErrorMsg = "Error sending email."
                //                    };
                //                }

                MailMessage mail = new MailMessage();

                mail.To.Add(Email); //responsiblepersonemail
                mail.From = new MailAddress("dslalc@datasoftlogic.com");
                mail.Subject = "Assisted Living Centre: Forgot Password";
                mail.Body = content;              

                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                                              //smtp.Host = "10.10.1.149"; //Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential
                     ("dslalc@datasoftlogic.com", "dslalciloilo17"); // ***use valid credentials***
                smtp.Port = 587;
                //smtp.Port = 25;

                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);
            }
            return new {
                Result = 1,
                ErrorMsg = ""
            };
        }

        private dynamic SendActivation([DynamicJson] dynamic data)
        {
            string AgencyId = data.AgencyId,
                   Email = data.Email;

            string constring = ALCUserManager.Instance.ConnectionString(AgencyId);
            using (var ALSDB = new ALSBaseDataContext(constring))
            {
                var guest = ALSDB.ALCUsers.SingleOrDefault(x => x.email == Email);
                if (guest != null)
                {
                    guest.is_active = true;
                    ALSDB.SubmitChanges();
                }
            }

                return new
            {
                Result = 1,
                ErrorMsg = ""
            };
        }

        private dynamic SendVerification([DynamicJson] dynamic data)
        {
            string AgencyId = data.AgencyId,
                   LastName = data.LastName,
                   FirstName = data.FirstName,
                   SSN = data.SSN,
                   Email = data.Email,
                   Phone = data.Phone,
                   Password = data.Password;


            string constring = ALCUserManager.Instance.ConnectionString(AgencyId);
            if (string.IsNullOrEmpty(constring))
            {
                return new
                {
                    Result = -1,
                    ErrorMsg = "AgencyID does not have a match."
                };
            }

            using (var ALSDB = new ALSBaseDataContext(constring))
            {
                var guest = ALSDB.ALCUsers.SingleOrDefault(x => x.email == Email);
                var resident = ALSDB.Residents.SingleOrDefault(x => x.first_name == FirstName && x.last_name == LastName);
                var ssn4 = "";

                if (resident == null)
                {
                    return new
                    {
                        Result = -1,
                        ErrorMsg = "Resident not found on Agency. Please enter correct name details of the resident."
                    };
                }

                if (guest != null)
                {
                    return new
                    {
                        Result = -1,
                        ErrorMsg = "User email is already registered. You may login to your account." 
                    };
                }               
                else
                {
                    if (resident.SSN != null || resident.SSN != "")
                    {
                        ssn4 = resident.SSN.Substring(resident.SSN.Length - 4, 4);
                        if (SSN != ssn4) {
                            return new
                            {
                                Result = -1,
                                ErrorMsg = "SSN does not match resident's registered SSN." 
                            };
                        }
                    }                    
                }


                if (Email != resident.responsible_person_email)
                {
                    return new
                    {
                        Result = -1,
                        ErrorMsg = "Email not nominated as responsible person email of the resident." 
                    };
                }



                if (guest == null && resident != null && ssn4 == SSN && Email == resident.responsible_person_email)
                {
                    guest = new ALCUser();
                    guest.user_id = Email;
                    guest.password = Password;
                    guest.email = Email;
                    guest.non_sys_user = true;
                    //guest.is_active = true;
                    guest.date_created = DateTime.Now;
                    guest.date_modified = DateTime.Now;
                    guest.force_reset_password = false;

                    ALSDB.ALCUsers.InsertOnSubmit(guest);
                    ALSDB.SubmitChanges();


                    var role = (from r in ALSDB.ALCRoles
                                where r.description == "Guest"
                                select new
                                {
                                    r.role_id
                                }).SingleOrDefault();

                    var gs_role = new ALCUserRole();
                    gs_role = ALSDB.ALCUserRoles.SingleOrDefault(x => x.user_id == Email);

                    if (gs_role == null)
                    {
                        gs_role = new ALCUserRole();

                        gs_role.user_id = Email;
                        gs_role.role_id = role.role_id;
                        gs_role.is_primary = true;

                        ALSDB.ALCUserRoles.InsertOnSubmit(gs_role);
                        ALSDB.SubmitChanges();
                    }

                    var base64link = System.Text.Encoding.UTF8.GetBytes(AgencyId + "=" + Email);
                    string link = System.Convert.ToBase64String(base64link);

                    string localPath = Server.MapPath("~/Views/Account/EmailGuestRegistration.cshtml");
                    string content = System.IO.File.ReadAllText(localPath);
                    content = content.Replace("<%=TP_RName%>", resident.first_name + " " + resident.last_name);

                    MailMessage mail = new MailMessage();

                    mail.To.Add(Email); //responsiblepersonemail
                    mail.From = new MailAddress("dslalc@datasoftlogic.com");
                    mail.Subject = "Assisted Living Centre: Guest Registration";
                    mail.Body = content;
                    //mail.Body += "<a href=\"http://localhost/ALS/Account/ActivateGuest?" + link + "\"> ActivateGuest </a>";
                    mail.Body += "<a href=\"www.dslalc.com/ALC/Account/ActivateGuest?" + link + "\"> ActivateGuest </a>";
                    mail.Body += "<br/><br/>If you have other concerns, please contact our facility. Thank you.<br/><br/> - The Assisted Living Centre Team ";
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                                                  //smtp.Host = "10.10.1.149"; //Or Your SMTP Server Address
                    smtp.Credentials = new System.Net.NetworkCredential
                         ("dslalc@datasoftlogic.com", "dslalciloilo17"); // ***use valid credentials***
                    smtp.Port = 587;
                    //smtp.Port = 25;

                    //Or your Smtp Email ID and Password
                    smtp.EnableSsl = true;
                    smtp.Send(mail);

                }
            }

            //string constring = ALCUserManager.Instance.ConnectionString(AgencyId);
            //if (string.IsNullOrEmpty(constring))
            //{
            //    return new
            //    {
            //        Result = -1,
            //        ErrorMsg = "AgencyID does not have a match."//Send and don' show error // "Invalid Agency ID"
            //    };
            //}

            //using (var ctx = new ALSBaseDataContext(constring))
            //{
            //    var user = (from u in ctx.ALCUsers
            //                join ur in ctx.ALCUserRoles on u.user_id equals ur.user_id
            //                join r in ctx.ALCRoles on ur.role_id equals r.role_id
            //                where u.user_id == UserId && u.email == Email //&& u.password == Password
            //                select new { u.first_name, u.user_id, u.password, ur.role_id, u.is_active }).SingleOrDefault();

            //    //1. check if creds are correct and check case sensitivity
            //    if (user == null)
            //    { // || user.user_id != UserId || user.password != Password) {
            //        return new
            //        {
            //            Result = -1,
            //            ErrorMsg = "Please enter your correct username and registered email."// "Invalid User ID or Password"
            //        };
            //    }

            //    //Read template and send using Mail helper class

            //    string localPath = Server.MapPath("~/Views/Account/PasswordResetEmailNotification.cshtml");
            //    string content = System.IO.File.ReadAllText(localPath);
            //    string firstname = user.first_name ?? "";
            //    if (!string.IsNullOrEmpty(firstname))
            //        firstname = firstname.First().ToString().ToUpper() + firstname.Substring(1);
            //    var random = new Random();
            //    string verifycode = Ext.Exts.GetVerifyCode(6, random);

            //    var ux = ctx.ALCUsers.Where(x => x.user_id == user.user_id).SingleOrDefault();
            //    if (ux != null)
            //    {
            //        ux.verify_code = verifycode;
            //        ux.verify_created_date = DateTime.UtcNow;
            //        ctx.SubmitChanges();
            //    }

            //    string code = string.Format(AgencyId + "|" + user.user_id + "|" + verifycode);

            //    content = content.Replace("<%=TP_Name%>", firstname);
            //    content = content.Replace("<%=TP_CODE%>", verifycode);
            //    content = content.Replace("<%=TP_HASH%>", HttpServerUtility.UrlTokenEncode(code.EncryptToByte()));
            //    content = content.Replace("<%=TP_SITE%>", string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            //    //#if DEBUG
            //    //                //empty this so it will use the email override from config
            //    //                Email = "";
            //    //#endif
            //    //                try {
            //    //                    //send mail
            //    //                    var mH = new MailHelper() {
            //    //                        Subject = "Reset Password Request",
            //    //                        Recipient = Email,
            //    //                        Body = content
            //    //                    };
            //    //                    mH.Send();
            //    //                } catch {
            //    //                    return new {
            //    //                        Result = -1,
            //    //                        ErrorMsg = "Error sending email."
            //    //                    };
            //    //                }

            //    MailMessage mail = new MailMessage();

            //    mail.To.Add(Email); //responsiblepersonemail
            //    mail.From = new MailAddress("dslalc@datasoftlogic.com");
            //    mail.Subject = "Assisted Living Centre: Forgot Password";
            //    mail.Body = content;

            //    mail.IsBodyHtml = true;
            //    SmtpClient smtp = new SmtpClient();
            //    smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            //                                  //smtp.Host = "10.10.1.149"; //Or Your SMTP Server Address
            //    smtp.Credentials = new System.Net.NetworkCredential
            //         ("dslalc@datasoftlogic.com", "dslalciloilo17"); // ***use valid credentials***
            //    smtp.Port = 587;
            //    //smtp.Port = 25;

            //    //Or your Smtp Email ID and Password
            //    smtp.EnableSsl = true;
            //    smtp.Send(mail);
            //}
            return new
            {
                Result = 1,
                ErrorMsg = ""
            };
        }

    }
}
