﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DataSoft.Helpers;
using Datasoft.AssistedLiving.Web.Models;


namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class SwitchRoleController : Controller
    {
        #region Private Properties
        private string UserID {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        private string RoleID {
            get { return User.Identity.Name.Split('|')[2]; }
        } 
        #endregion

        [ChildActionOnly]
        // GET: SwitchRole
        public ActionResult Index()
        {
            using (var ALS = new ALSBaseDataContext()) {
                int roleid = int.Parse(RoleID);
                string input = UserID;
                ViewBag.Username = input.First().ToString().ToUpper() + String.Join("", input.Skip(1));

                var carestaffRoleList = new int[] { 1,2, 3, 9, 10, 11, 12, 13 };
                var adminMarketerRoleList = new int[] { 1, 8 };
                string url = Request.Url != null ? Request.Url.ToString() : "";
                if (url.Contains("Admin") && !carestaffRoleList.Contains(roleid))
                    Response.Redirect("~/Staff", true);

                var strId = UserID;
                var otherRoles = (from x in ALS.ALCUserRoles
                                  join b in ALS.ALCRoles on x.role_id equals b.role_id
                                  where x.user_id == strId
                                  select new SelectData { Id = b.role_id, Name = b.description }).ToList();
                var model = new DeptData() {
                    Depts = otherRoles,
                    SelectedValue = RoleID
                };
                return View("~/Views/Shared/SwitchRole.cshtml", model);
            }
        }
        public ActionResult LogError(string module, string message)
        {
            var log = new ErrorController();
            log.ControllerContext = ControllerContext;
            return log.LogError(module, message);
        }

        public dynamic Switch([DynamicJson(MatchName = false)] dynamic obj) {
            
            try {
                var selectedRoleID = obj.rid;
                HttpCookie _authCookie = FormsAuthentication.GetAuthCookie(User.Identity.Name, true);
                var ticket = FormsAuthentication.Decrypt(_authCookie.Value);
                var ui = ticket.Name.Split('|');
                string userInfo = string.Format("{0}|{1}|{2}", ui[0], ui[1], selectedRoleID.ToString());
                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                        ticket.Version,    // version
                        userInfo,          // user name
                        ticket.IssueDate,  // creation
                        ticket.Expiration, // expiration
                        false,              // persistent
                        selectedRoleID.ToString()); // user data 

                _authCookie.Value = FormsAuthentication.Encrypt(authTicket);
                Response.Cookies.Set(_authCookie);

                //HttpCookie _authCookie2 = FormsAuthentication.GetAuthCookie(User.Identity.Name, true);
                //var ticket2 = FormsAuthentication.Decrypt(_authCookie2.Value);
                int roleid = int.Parse(selectedRoleID);
                string redirectUrl = FormsAuthentication.GetRedirectUrl(userInfo, false);
                if (redirectUrl == "/" || redirectUrl.ToUpper().IndexOf("DEFAULT.ASPX") != -1)
                    redirectUrl = "Home/Dashboard";

                var carestaffRoleList = new int[] { 4, 5, 6, 7, 9, 10, 11, 12, 13 };
                var adminMarketerRoleList = new int[] { 1, 8 };

                //Super Admin
                if (roleid == 1)
                    redirectUrl = "Staff";// "Admin";
                //Med tech
                else if (roleid == 3)
                    redirectUrl = "MAR";
                //Carestaff
                else if (carestaffRoleList.Contains(roleid))
                    redirectUrl = "Staff";
                //Marketer
                else if (adminMarketerRoleList.Contains(roleid))
                    redirectUrl = "Marketer";
                
                return Json(new {
                    Result = 1,
                    RedirectUrl = redirectUrl
                    //,DebugCookie = ticket2.Name //see the changes if it works
                });
            } catch (Exception E) {

                LogError("SwitchRoleController/Switch", "Error Message: " + E.Message);

                return Json(new {
                    Result = -1,
                    RedirectUrl = Request.UrlReferrer,
                   // DebugCookie = ticket2.Name
                });
            }
        }
    }
}