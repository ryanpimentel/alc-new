﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using DataSoft.Helpers;
using Datasoft.Ext;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Datasoft.AssistedLiving.Web.Models;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class HomeCareStaffController : Controller
    {
        // GET: HomeCare
        public ActionResult Index()
        {
            return View();
        }

        private string UserID
        {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID
        {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        private string RoleID
        {
            get { return User.Identity.Name.Split('|')[2]; }
        }


        [HttpGet]
        public string CT()
        {
            return DataSoft.Helpers.Utility.ToTimezonedDateTimeFriendlyTZ(DateTime.Now, "ddd, MMM dd yyyy h:mmtt");
        }



    }
}