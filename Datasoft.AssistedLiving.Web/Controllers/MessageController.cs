﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datasoft.AssistedLiving.Web.Models;
using DataSoft.Helpers;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class MessageController : Controller
    {
        // GET: Message
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LogError(string module, string message)
        {
            var log = new ErrorController();
            log.ControllerContext = ControllerContext;
            return log.LogError(module, message);
        }

        public dynamic GetResidentsWithComment([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                //Resident r = null;

                using (var ALSDB = new ALSBaseDataContext())
                {

                    var data = (from gcom in ALSDB.GuestComments
                                join res in ALSDB.Residents on gcom.resident_id equals res.resident_id
                                where res.isAdmitted == true
                                select new
                                {
                                    ResidentId = gcom.resident_id,
                                    Name = res.first_name + " " + res.last_name,
                                    Image = res.imagesrc,
                                    Unread_Count = ALSDB.GuestComments.Where(x => x.resident_id == gcom.resident_id ).Sum(x => x.unread_count)
                                    }).Distinct().ToArray();

                    return Json(data);

                }
            }
            catch (Exception E)
            {

                LogError("Messages/GetResidentsWithComment", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }
        
        public dynamic GetConversation([DynamicJson(MatchName = false)] dynamic obj)
        {
            using (var ALSDB = new ALSBaseDataContext())
            {
                try
                {
                    int id = Convert.ToInt32(obj);


                    var data = (from g in ALSDB.GuestComments
                                where g.resident_id == id
                                select new
                                {
                                    g
                                }).ToArray();

                    return Json(data);

                }
                catch (Exception E)
                {
                    LogError("MessageController/GetConversation", "Error Message: " + E.Message);
                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
                }
            }
        }
        public dynamic ChangeToReadStatus([DynamicJson(MatchName = false)] dynamic obj)
        {
            using (var ALSDB = new ALSBaseDataContext())
            {
                try
                {
                    string cid = Convert.ToString(obj);


                   var data = ALSDB.GuestComments.SingleOrDefault(x => x.cid == cid);

                    //var data = (from g in ALSDB.GuestComments
                    //            where g.cid == cid
                    //            select new
                    //            {
                    //                g
                    //            }).SingleOrDefault();

                    data.read_status = "read";
                    data.unread_count = 0;

                    ALSDB.SubmitChanges();

                    return Json(data);

                }
                catch (Exception E)
                {
                    LogError("MessageController/ChangeToReadStatus", "Error Message: " + E.Message);
                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}