﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataSoft.Helpers;
using System.Web.Security;
using Datasoft.Ext;
using Datasoft.AssistedLiving.Security;
using Datasoft.AssistedLiving.Web.Models;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            var lvm = new LoginViewModel();
            System.Web.HttpCookieCollection cookies = Request.Cookies;
            if (cookies["ALCInfo"] != null) {
                lvm.AgencyID = cookies["ALCInfo"].Value.Split('|')[0];
                lvm.UserID = cookies["ALCInfo"].Value.Split('|')[1];
            }

            if (User.Identity.IsAuthenticated)
            {
               // var ishomecare = User.Identity.Name.Split('|')[4];

                //if (ishomecare == "True")
                //{
                //    //homeurl redirect here if ishomecare
                //    if (User.IsInRole("1"))
                //    {
                //        return RedirectToAction("Index", "HomeCare");
                //    }
                //    else
                //    {
                //        return RedirectToAction("Index", "HomeCareStaff");
                //    }
                                      
                //}
                //else
                //{
                    if (User.IsInRole("1"))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else if (User.IsInRole("3"))
                    {
                        return RedirectToAction("Index", "MAR");
                    }
                    else if (User.IsInRole("8"))
                    {
                        return RedirectToAction("Index", "MAR");
                    }
                    else if (User.IsInRole("16") || User.IsInRole("20") || User.IsInRole("19"))
                    {
                        return RedirectToAction("Index", "GuestNotification");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Transsched");
                    }
                //}

                
              
            }
            // ViewBag.ReturnUrl = returnUrl ?? Url.Action("Index", "Dashboard");

            return View(lvm);
        }

        [HttpPost]
        public JsonResult Authenticate([DynamicJson(MatchName = false)] dynamic obj) {
            if (obj != null) {
                var result = ValidateUser(obj);
                return Json(result);
            }
            return Json("");
        }

        #region Validate Login Info
        private dynamic ValidateUser([DynamicJson] dynamic data) {
            string AgencyId = data.AgencyId,
                    UserId = data.UserId,
                    Password = data.Password;

            string constring = ALCUserManager.Instance.ConnectionString(AgencyId);
            if (string.IsNullOrEmpty(constring)) {
                return new {
                    Result = -1,
                    ErrorMsg = "Invalid Agency ID"
                };
            }
            //RYAN WAS HERE!
            using (var ctx = new ALSBaseDataContext(constring)) {
                var user = (from u in ctx.ALCUsers
                            join ur in ctx.ALCUserRoles on u.user_id equals ur.user_id
                            join r in ctx.ALCRoles on ur.role_id equals r.role_id
                            where u.user_id == UserId && u.password == Password && (ur.is_primary.HasValue && ur.is_primary == true)
                            select new { u.user_id, u.password, ur.role_id, r.description, u.is_active, force_reset = u.force_reset_password }).SingleOrDefault();

                //1. check if creds are correct and check case sensitivity
                if (user == null || user.user_id != UserId || user.password != Password) {
                    return new {
                        Result = -1,
                        ErrorMsg = "Invalid User ID or Password"
                    };
                }

                //2. Check if account is active
                if (user.is_active == null || !user.is_active.Value) {
                    return new {
                        Result = -1,
                        ErrorMsg = "Sorry, your account is not active.  Please contact your System Admininstrator."
                    };
                }

                if(user.force_reset != null && user.force_reset.Value) {
                    var random = new Random();
                    string verifycode = Ext.Exts.GetVerifyCode(6, random);
                    string code = string.Format(AgencyId + "|" + user.user_id + "|" + verifycode);
                    return new {
                        Result = -2,
                        ErrorMsg = "Force reset password",
                        RedirectUrl = string.Format("Account/ResetPassword/{0}?FL=1", HttpServerUtility.UrlTokenEncode(code.EncryptToByte()))
                    };
                }
                double timeout = Convert.ToDouble(ALCUserManager.Instance.AuthTimeout(AgencyId));

                bool isHomeCare = false;

                var lastUpdate = "";
                var version = "";
                var updateDetails = ctx.UpdateDetails.OrderByDescending(p => p.alc_update_id).FirstOrDefault();
                lastUpdate = Utility.GetDateFormattedString(updateDetails.date_updated, "ddd, MMM dd yyyy h:mmtt");
                version = Convert.ToString(updateDetails.version_number);

                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    var agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    
                    if (agency != null)
                        isHomeCare = agency.is_home_care;
                };
                
                string userInfo = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}", user.user_id, AgencyId, user.role_id, timeout, isHomeCare, lastUpdate, version);              
                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                    1,                            // version
                    userInfo,                     // user name
                    DateTime.Now,                 // creation
                    DateTime.Now.AddMinutes(timeout), // expiration
                    false,                        // persistent
                    user.role_id.ToString());     // user data 

                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                System.Web.HttpCookie authCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
               // authCookie.Secure = FormsAuthentication.RequireSSL;

                Response.Cookies.Add(authCookie);

                // Create cookies to remember last login (except password)
                System.Web.HttpCookie agencyIdCookie = new System.Web.HttpCookie("ALCInfo", AgencyId + "|" + UserId);
                agencyIdCookie.Expires = DateTime.Now.AddDays(5);
                agencyIdCookie.Path = "/";
                Response.Cookies.Add(agencyIdCookie);

                string redirectUrl = FormsAuthentication.GetRedirectUrl(userInfo, false);
                if (redirectUrl == "/" || redirectUrl.ToUpper().IndexOf("DEFAULT.ASPX") != -1)
                    redirectUrl = "Home";

                var roleid = user.role_id;
                var role = user.description;

                var carestaffRoleList = new int[] { 4, 5, 6, 7, 9, 10, 11, 12, 13 };
                var adminMarketerRoleList = new int[] { 1, 8 };
                var medtechRoleList = new int[] { 3, 9 };

                //Super Admin
                if (roleid == 1)
                    redirectUrl = "Home";
                    //redirectUrl = "Transsched";//"Staff";
                //Med tech
                else if (medtechRoleList.Contains(roleid))
                    redirectUrl = "MAR";
                //Carestaff
                else if (carestaffRoleList.Contains(roleid))
                    redirectUrl = "Transsched";
                else if(role == "Guest")
                {
                    redirectUrl = "GuestNotification";
                }
                //Marketer
                else if (adminMarketerRoleList.Contains(roleid))
                    redirectUrl = "Marketer";

                if (!string.IsNullOrEmpty(data.RetUrl))
                    redirectUrl = data.RetUrl;

                //USER LANDING PAGE IF AGENCY IS HOMECARE
                if (isHomeCare)
                {
                    if (roleid == 1)
                    {
                        redirectUrl = "HomeCare";
                    }
                    else
                    {
                        redirectUrl = "HomeCareStaff";
                    }
                    
                }
                
                return new {
                    Result = 1,
                    RedirectUrl = redirectUrl
                };
            }
        }
        #endregion

        [HttpPost]
        public JsonResult LogOff() {
            if (User.Identity.IsAuthenticated) {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return Json(new {
                    Result = 1,
                    RedirectUrl = Url.Content("~/Login")
                });
            }
            return Json(new {
                Result = -1
            });
        }
    }
}
