﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DataSoft.Helpers;
using Datasoft.Ext;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Datasoft.AssistedLiving.Web.Models;
using System.IO;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class TransschedController : Controller
    {
        private string UserID
        {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID
        {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        private string RoleID
        {
            get { return User.Identity.Name.Split('|')[2]; }
        }

        private string IsHomeCare
        {
            get { return User.Identity.Name.Split('|')[4]; }
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonDataContractResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        // GET: Transsched
        public ActionResult Index()
        {
            using (var ALS = new ALSBaseDataContext())
            {
                int roleId = int.Parse(RoleID);
                var res = (from x in ALS.ALCUsers
                           join y in ALS.CarestaffShifts on x.user_id equals y.carestaff_id into y1
                           from ys in y1.DefaultIfEmpty()
                           join z in ALS.Shifts on ys.shift_id equals z.shift_id into z1
                           from zs in z1.DefaultIfEmpty()
                           join a in ALS.ALCUserRoles on x.user_id equals a.user_id into a1
                           from As in a1.DefaultIfEmpty()
                           join b in ALS.ALCRoles on As.role_id equals b.role_id into b1
                           from bs in b1.DefaultIfEmpty()
                           where x.user_id == UserID && As.role_id == roleId
                           select new
                           {
                               x.middle_initial,
                               x.first_name,
                               x.last_name,
                               role = bs.description,
                               start_time = Utility.GetDateFormattedString(zs.start_time, "hh:mmtt"),
                               end_time = Utility.GetDateFormattedString(zs.end_time, "hh:mmtt")
                           }).SingleOrDefault();
                string mi = !string.IsNullOrEmpty(res.middle_initial) ? res.middle_initial + ". " : "";
                ViewBag.Name = res.first_name + " " + mi + res.last_name;
                ViewBag.Role = res.role;
                ViewBag.StartShiftTime = res.start_time;
                ViewBag.EndShiftTime = res.end_time;

                var headlist = new int[] { 9, 10, 11, 12, 13 };
                if (roleId == 1)
                    ViewBag.Role = "Administrator";

                ViewBag.IsDeptHead = headlist.Contains(roleId) ? true : false;
                ViewBag.IsAdmin = roleId == 1 ? true : false;
                ViewBag.RoleID = roleId;
                ViewBag.IsHomeCare = IsHomeCare;

                //var tz = TimeZoneInfo.GetSystemTimeZones();
                //foreach (var t in tz) {
                //    System.Diagnostics.Debug.WriteLine("DisplayName:{0}", t.DisplayName);
                //    System.Diagnostics.Debug.WriteLine("Id:{0}", t.Id);
                //    System.Diagnostics.Debug.WriteLine("StandardName:{0}", t.StandardName);
                //    System.Diagnostics.Debug.WriteLine("Ba""seUtcOffset:{0}", t.BaseUtcOffset);
                //    System.Diagnostics.Debug.WriteLine("DaylightName:{0}", t.DaylightName);
                //}

            }

            var objModel = GetActivityScheduleData();

            return View(objModel);
        }

        public ActionResult LogError(string module, string message)
        {
            var log = new ErrorController();
            log.ControllerContext = ControllerContext;
            return log.LogError(module, message);
        }


        [HttpPost]
        public JsonResult GetAdmissionCalendar([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                var result = JsonResponse.GetDefault();
                if (obj != null)
                {
                    DateTime? startDate = ((string)obj.startDate).TryParseNullableDate();
                    DateTime? endDate = ((string)obj.endDate).TryParseNullableDate();

                    using (var ALS = new ALSBaseDataContext())
                    {

                        var d = (from t in ALS.TransportationSchedules
                                 join r in ALS.Residents on t.resident_id equals r.resident_id
                                 join a in ALS.Admissions on t.resident_id equals a.resident_id
                                 //join rm in ALS.Rooms on a.room_id equals rm.room_id
                                 where t.is_active == true && r.isAdmitted == true
                                 //join c in ALS.ALCUsers on t.staff_id equals c.user_id
                                 select new
                                 {
                                     t.resident_id,
                                     t.transpo_id,
                                     date = Utility.GetDateFormattedString(t.date, "MM/dd/yyyy"),
                                     end_date = Utility.GetDateFormattedString(t.end_date, "MM/dd/yyyy"),
                                     //staff_id = c.first_name + " " + c.last_name,
                                     start_time = Utility.GetDateFormattedString(t.start_time, "HH:mm"),
                                     end_time = Utility.GetDateFormattedString(t.end_time, "HH:mm"),
                                     name = r.first_name + " " + r.last_name,
                                     t.day_of_the_week,
                                     t.recurrence,
                                     t.day_of_the_month,
                                     //rm.room_name,
                                     r.isAdmitted
                                 }).ToList();

                        var data = new
                        {
                            StartDate = obj.startDate,
                            EndDate = obj.endDate,
                            Appointments = d
                        };

                        return Json(data);
                    }
                }
                return Json(result);

            }
            catch (Exception E)
            {
                LogError("TransschedController/GetAdmissionCalendar", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult CheckConflictForSOC([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                var result = JsonResponse.GetDefault();
                var res = "0";
                DateTime date_today = DateTime.Now;
                if (obj != null)
                {
                    //DateTime? startDate = ((string)obj.startDate).TryParseNullableDate();
                    //DateTime? endDate = ((string)obj.endDate).TryParseNullableDate();
                    int residentId = Convert.ToInt32(obj.resident_id);
                    string carestaff_id = obj.carestaff_id;

                    using (var ALS = new ALSBaseDataContext())
                    {

                        var d = (from ps in ALS.PlannedServices
                                 join r in ALS.Residents on ps.client_id equals r.resident_id
                                 where ps.client_id == residentId && ps.carestaff_id == carestaff_id
                                 select new
                                 {
                                     ps.soc,
                                     ps.eoc
                                 }).ToList();

                        if (d.Count() > 0)
                        {
                            foreach (var o in d)
                            {
                                if ((o.soc <= date_today && o.eoc >= date_today) || (o.soc <= date_today && o.eoc >= date_today) || (o.soc <= date_today && o.eoc >= date_today))
                                {
                                    res = "1";
                                }
                            }
                        }



                        return Json(res);
                    }
                }
                return Json(result);

            }
            catch (Exception E)
            {
                LogError("TransschedController/GetAdmissionCalendar", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult GetSOCAndEOC([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                var result = JsonResponse.GetDefault();
                var res = "0";
                if (obj != null)
                {

                    int id = Convert.ToInt32(obj.resident_id);

                    using (var ALS = new ALSBaseDataContext())
                    {

                        var d = (from r in ALS.Residents
                                 join ad in ALS.Admissions on r.resident_id equals ad.resident_id
                                 where r.resident_id == id
                                 select new
                                 {
                                     date_admitted = Utility.GetDateFormattedString(r.date_admitted, "MM/dd/yyyy"),
                                     discharge_date = Utility.GetDateFormattedString(ad.discharge_date, "MM/dd/yyyy")
                                 }).ToList();

                        return Json(d);
                    }
                }
                return Json(result);

            }
            catch (Exception E)
            {
                LogError("TransschedController/GetAdmissionCalendar", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public JsonResult GetServicesCalendar([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                var result = JsonResponse.GetDefault();
                if (obj != null)
                {
                    DateTime? startDate = ((string)obj.startDate).TryParseNullableDate();
                    DateTime? endDate = ((string)obj.endDate).TryParseNullableDate();
                    string filter = obj.filter;

                    using (var ALS = new ALSBaseDataContext())
                    {
                        if (filter == "0")
                        {

                            var planned_posted = (from ps in ALS.PlannedServices
                                                  join paps in ALS.PlannedAndPostedServices on ps.planned_service_id equals paps.planned_service_id
                                                  join r in ALS.Residents on ps.client_id equals r.resident_id
                                                  join a in ALS.Admissions on ps.client_id equals a.resident_id
                                                  join c in ALS.ALCUsers on ps.carestaff_id equals c.user_id
                                                  where r.isAdmitted == true && paps.is_active == true
                                                  //where ac.is_active == true && r.isAdmitted == true
                                                  //join c in ALS.ALCUsers on t.staff_id equals c.user_id
                                                  select new
                                                  {

                                                      ps.client_id,
                                                      resident = r.first_name + " " + r.last_name,
                                                      ps.is_active,
                                                      //active_until = Utility.GetDateFormattedString(ps.active_until, "MM/dd/yyyy"),
                                                      date = Utility.GetDateFormattedString(ps.soc, "MM/dd/yyyy"),
                                                      end_date = Utility.GetDateFormattedString(ps.eoc, "MM/dd/yyyy"),
                                                      carestaff = c.first_name + " " + c.last_name,
                                                      carestaff_id = c.user_id,
                                                      start_time = Utility.GetDateFormattedString(ps.start_time, "HH:mm"),
                                                      end_time = Utility.GetDateFormattedString(ps.end_time, "HH:mm"),
                                                      name = r.first_name + " " + r.last_name,
                                                      ps.recurrence,
                                                      r.isAdmitted,
                                                      ps.planned_service_id,
                                                      ps.is_onetime,
                                                      paps.services_rendered,
                                                      paps.planned_posted_service_id,
                                                      planned_date = Utility.GetDateFormattedString(paps.planned_date, "MM/dd/yyyy"),
                                                      //paps.status,
                                                      paps_carestaff = paps.carestaff_id,
                                                      paps.remarks,
                                                      posted_date = Utility.GetDateFormattedString(paps.posted_date, "MM/dd/yyyy"),
                                                      actual_start_time = Utility.GetDateFormattedString(paps.actual_start_time, "HH:mm"),
                                                      actual_end_time = Utility.GetDateFormattedString(paps.actual_end_time, "HH:mm"),
                                                  }).ToList();

                            var data = new
                            {
                                StartDate = obj.startDate,
                                EndDate = obj.endDate,
                                //Appointments_planned= planned,
                                Appointments_planned_posted = planned_posted
                            };

                            return Json(data);

                        }
                        else
                        {
                            var planned_posted = (from ps in ALS.PlannedServices
                                                  join paps in ALS.PlannedAndPostedServices on ps.planned_service_id equals paps.planned_service_id
                                                  join r in ALS.Residents on ps.client_id equals r.resident_id
                                                  join a in ALS.Admissions on ps.client_id equals a.resident_id
                                                  join c in ALS.ALCUsers on ps.carestaff_id equals c.user_id
                                                  where r.isAdmitted == true && c.user_id == filter && paps.is_active == true
                                                  //where ac.is_active == true && r.isAdmitted == true
                                                  //join c in ALS.ALCUsers on t.staff_id equals c.user_id
                                                  select new
                                                  {
                                                      ps.client_id,
                                                      resident = r.first_name + " " + r.last_name,
                                                      ps.is_active,
                                                      //active_until = Utility.GetDateFormattedString(ps.active_until, "MM/dd/yyyy"),
                                                      date = Utility.GetDateFormattedString(ps.soc, "MM/dd/yyyy"),
                                                      end_date = Utility.GetDateFormattedString(ps.eoc, "MM/dd/yyyy"),
                                                      carestaff = c.first_name + " " + c.last_name,
                                                      carestaff_id = c.user_id,
                                                      start_time = Utility.GetDateFormattedString(ps.start_time, "HH:mm"),
                                                      end_time = Utility.GetDateFormattedString(ps.end_time, "HH:mm"),
                                                      name = r.first_name + " " + r.last_name,
                                                      ps.recurrence,
                                                      r.isAdmitted,
                                                      ps.planned_service_id,
                                                      ps.is_onetime,
                                                      paps.services_rendered,
                                                      paps.planned_posted_service_id,
                                                      planned_date = Utility.GetDateFormattedString(paps.planned_date, "MM/dd/yyyy"),
                                                      //paps.status,
                                                      paps_carestaff = paps.carestaff_id,
                                                      paps.remarks,
                                                      posted_date = Utility.GetDateFormattedString(paps.posted_date, "MM/dd/yyyy"),
                                                      actual_start_time = Utility.GetDateFormattedString(paps.actual_start_time, "HH:mm"),
                                                      actual_end_time = Utility.GetDateFormattedString(paps.actual_end_time, "HH:mm"),
                                                  }).ToList();

                            var data = new
                            {
                                StartDate = obj.startDate,
                                EndDate = obj.endDate,
                                //Appointments_planned = planned,
                                Appointments_planned_posted = planned_posted
                            };

                            return Json(data);
                        };
                    }
                }
                return Json(result);

            }
            catch (Exception E)
            {
                LogError("TransschedController/GetServicesCalendar", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult GetScheduleData([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                var result = JsonResponse.GetDefault();
                if (obj != null)
                {

                    DateTime? startDate = ((string)obj.startDate).TryParseNullableDate();
                    DateTime? endDate = ((string)obj.endDate).TryParseNullableDate();
                    DateTime currentDate = Convert.ToDateTime(obj.date);
                    Int32 id = Convert.ToInt32(obj.transpo_id);

                    using (var ALS = new ALSBaseDataContext())
                    {

                        var d = (from t in ALS.TransportationSchedules
                                 join r in ALS.Residents on t.resident_id equals r.resident_id
                                 //join c in ALS.ALCUsers on t.staff_id equals c.user_id
                                 where t.transpo_id == id
                                 select new
                                 {
                                     t.resident_id,
                                     t.transpo_id,
                                     date = Utility.GetDateFormattedString(t.date, "MM/dd/yyyy"),
                                     end_date = Utility.GetDateFormattedString(t.end_date, "MM/dd/yyyy"),
                                     //staff_id = c.first_name + " " + c.last_name,
                                     start_time = Utility.GetDateFormattedString(t.start_time, "HH:mm"),
                                     end_time = Utility.GetDateFormattedString(t.end_time, "HH:mm"),
                                     name = r.first_name + " " + r.last_name,
                                     t.day_of_the_week,
                                     t.recurrence,
                                     t.day_of_the_month,
                                     t.purpose
                                     //c.first_name,
                                     //c.last_name
                                 }).ToList();

                        var tc = (from g in ALS.TranspoComments
                                  where g.transpo_id == id &&
                                  (DateTime.Compare(g.date.Value.Date, currentDate.Date) == 0)
                                  select new
                                  {
                                      g.comment,
                                      current_date = Utility.GetDateFormattedString(g.date, "MM/dd/yyyy")

                                  }).ToList();

                        var data = new
                        {

                            data = d,
                            comments = tc
                        };

                        return Json(data);
                    }
                }
                return Json(result);
            }
            catch (Exception E)
            {
                LogError("TransschedController/GetScheduleData", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult GetServiceScheduleData([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                var result = JsonResponse.GetDefault();
                if (obj != null)
                {

                    DateTime? startDate = ((string)obj.startDate).TryParseNullableDate();
                    DateTime? endDate = ((string)obj.endDate).TryParseNullableDate();
                    DateTime currentDate = Convert.ToDateTime(obj.date);
                    Int32 id = Convert.ToInt32(obj.planned_posted_service_id);
                    string is_completed = obj.is_completed;

                    using (var ALS = new ALSBaseDataContext())
                    {
                        var planned_posted = (from paps in ALS.PlannedAndPostedServices
                                              join ps in ALS.PlannedServices on paps.planned_service_id equals ps.planned_service_id
                                              join r in ALS.Residents on ps.client_id equals r.resident_id
                                              join a in ALS.Admissions on ps.client_id equals a.resident_id
                                              join c in ALS.ALCUsers on ps.carestaff_id equals c.user_id
                                              where paps.planned_posted_service_id == id


                                              select new
                                              {
                                                  ps.client_id,
                                                  resident = r.first_name + " " + r.last_name,
                                                  ps.is_active,
                                                  //active_until = Utility.GetDateFormattedString(ps.active_until, "MM/dd/yyyy"),
                                                  soc = Utility.GetDateFormattedString(ps.soc, "MM/dd/yyyy"),
                                                  eoc = Utility.GetDateFormattedString(ps.eoc, "MM/dd/yyyy"),
                                                  carestaff = c.first_name + " " + c.last_name,
                                                  carestaff_id = c.user_id,
                                                  start_time = Utility.GetDateFormattedString(ps.start_time, "HH:mm"),
                                                  end_time = Utility.GetDateFormattedString(ps.end_time, "HH:mm"),
                                                  name = r.first_name + " " + r.last_name,
                                                  ps.recurrence,
                                                  r.isAdmitted,
                                                  ps.planned_service_id,
                                                  ps.is_onetime,
                                                  ps.services,
                                                  paps.services_rendered,
                                                  paps.planned_posted_service_id,
                                                  planned_date = Utility.GetDateFormattedString(paps.planned_date, "MM/dd/yyyy"),
                                                  //paps.status,
                                                  paps_carestaff = paps.carestaff_id,
                                                  paps.remarks,
                                                  posted_date = Utility.GetDateFormattedString(paps.posted_date, "MM/dd/yyyy"),
                                                  actual_start_time = Utility.GetDateFormattedString(paps.actual_start_time, "HH:mm"),
                                                  actual_end_time = Utility.GetDateFormattedString(paps.actual_end_time, "HH:mm")
                                              }).ToList();

                        var svcs = (from pps in ALS.PlannedAndPostedServices
                                    where pps.planned_posted_service_id == id
                                    select new
                                    {
                                        pps.planned_services
                                    });

                        PlannedAndPostedService papsvc = null;
                        List<int> services = new List<int>();
                        papsvc = ALS.PlannedAndPostedServices.SingleOrDefault(x => x.planned_posted_service_id == id);

                        // Use rendered service if posted date is not null
                        if (papsvc.posted_date != null)
                        {
                            if (papsvc.services_rendered.Contains(','))
                            {
                                var svc_data = papsvc.services_rendered.Split(',');
                                for (int i = 0; i < svc_data.Length; i++)
                                {
                                    Activity act = null;
                                    act = ALS.Activities.SingleOrDefault(x => x.activity_id == Convert.ToInt32(svc_data[i]));

                                    services.Add(act.activity_id);
                                }

                            }
                            else
                            {
                                Activity act = null;
                                act = ALS.Activities.SingleOrDefault(x => x.activity_id == Convert.ToInt32(papsvc.services_rendered));

                                services.Add(act.activity_id);

                            }
                        }
                        else
                        {

                            if (papsvc.planned_services.Contains(','))
                            {
                                var svc_data = papsvc.planned_services.Split(',');
                                for (int i = 0; i < svc_data.Length; i++)
                                {
                                    Activity act = null;
                                    act = ALS.Activities.SingleOrDefault(x => x.activity_id == Convert.ToInt32(svc_data[i]));

                                    services.Add(act.activity_id);
                                }

                            }
                            else
                            {
                                Activity act = null;
                                act = ALS.Activities.SingleOrDefault(x => x.activity_id == Convert.ToInt32(papsvc.planned_services));

                                services.Add(act.activity_id);

                            }

                        }


                        var data = new
                        {
                            data = planned_posted,
                            service = services
                        };
                        return Json(data);


                    }
                }
                return Json(result);
            }
            catch (Exception E)
            {
                LogError("TransschedController/GetServiceScheduleData", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult AddSchedule([DynamicJson(MatchName = false)] dynamic obj)
        {

            try
            {
                using (var ALSDB = new ALSBaseDataContext())
                {
                    var cs = new TransportationSchedule();
                    TimeSpan start_time = TimeSpan.Parse(obj.start_time);
                    TimeSpan end_time = TimeSpan.Parse(obj.end_time);
                    Int32 res_id = Convert.ToInt32(obj.resident_id);

                    if (obj != null)
                    {
                        cs.date = Convert.ToDateTime(obj.start_date);
                        cs.day_of_the_month = obj.day_of_the_month;
                        cs.day_of_the_week = obj.day_of_the_week;
                        cs.end_time = end_time;
                        cs.start_time = start_time;
                        cs.recurrence = obj.recurrence;
                        cs.resident_id = res_id;
                        //cs.staff_id = obj.staff_id;
                        cs.purpose = obj.purpose;
                        cs.is_active = true;
                    }

                    if (obj.end_date != null) { cs.end_date = Convert.ToDateTime(obj.end_date); }

                    ALSDB.TransportationSchedules.InsertOnSubmit(cs);
                    ALSDB.SubmitChanges();
                    ALSDB.saveAction("1", UserID, cs.transpo_id, "transportation", "transpoId=" + cs.transpo_id + "; residentId=" + res_id);

                    return Json(cs);
                }
            }
            catch (Exception E)
            {
                LogError("TransschedController/AddSchedule", "Error Message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }


        }

        [HttpPost]
        public JsonResult UpdateSchedule([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                using (var ALSDB = new ALSBaseDataContext())
                {
                    TransportationSchedule cs = null;
                    Int32 id = Convert.ToInt32(obj.transpo_id);
                    DateTime date = Convert.ToDateTime(obj.date);
                    TimeSpan start_time = TimeSpan.Parse(obj.start_time);
                    TimeSpan end_time = TimeSpan.Parse(obj.end_time);
                    DateTime start_date = Convert.ToDateTime(obj.start_date);

                    cs = ALSDB.TransportationSchedules.SingleOrDefault(x => x.transpo_id == id);

                    cs.day_of_the_month = obj.day_of_the_month;
                    cs.date = start_date;
                    cs.day_of_the_week = obj.day_of_the_week;
                    cs.end_time = end_time;
                    cs.start_time = start_time;
                    cs.recurrence = obj.recurrence;
                    //cs.resident_id = res_id;
                    cs.purpose = obj.purpose;

                    if (obj.end_date != null) { cs.end_date = Convert.ToDateTime(obj.end_date); }

                    if (obj.comments != "")
                    {
                        var tc = new TranspoComment();

                        tc = ALSDB.TranspoComments.SingleOrDefault(x => x.transpo_id == id && x.date == date);

                        if (tc == null)
                        {
                            tc = new TranspoComment();

                            tc.transpo_id = id;
                            tc.date = date;
                            tc.comment = obj.comments;
                            ALSDB.TranspoComments.InsertOnSubmit(tc);

                        }
                        else
                        {
                            tc.comment = obj.comments;
                        }

                    }

                    ALSDB.SubmitChanges();
                    ALSDB.saveAction("2", UserID, cs.transpo_id, "transportation", "transpoId=" + cs.transpo_id + "; residentId=" + cs.resident_id);

                    return Json(cs);
                }
            }
            catch (Exception E)
            {
                LogError("TransschedController/UpdateSchedule", "Error Message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }


        }

        public JsonResult DeleteSchedule([DynamicJson(MatchName = false)] dynamic obj)
        {

            try
            {
                using (var ALSDB = new ALSBaseDataContext())
                {
                    var cs = new TransportationSchedule();
                    Int32 id = Convert.ToInt32(obj.transpo_id);

                    cs = ALSDB.TransportationSchedules.SingleOrDefault(x => x.transpo_id == id);

                    cs.is_active = false;

                    ALSDB.SubmitChanges();
                    ALSDB.saveAction("4", UserID, cs.transpo_id, "transportation", "transpoId=" + cs.transpo_id + "; residentId=" + cs.resident_id);

                    return Json(cs);
                }
            }
            catch (Exception E)
            {
                LogError("TransschedController/DeleteSchedule", "Error Message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }


        }


        [HttpPost]
        public JsonResult GetResidents()
        {
            try
            {
                using (var ALS = new ALSBaseDataContext())
                {
                    var cs = (from r in ALS.Residents
                              where r.admission_status == 1 && r.isAdmitted == true
                              select new
                              {
                                  r.resident_id,
                                  r.first_name,
                                  r.last_name

                              }).ToList();
                    return Json(cs);
                }
            }
            catch (Exception E)
            {
                LogError("TransschedController/GetResidents", "Error Message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }
        }

        [HttpPost]
        public JsonResult GetStaff()
        {
            try
            {
                using (var ALS = new ALSBaseDataContext())
                {
                    var cs = (from r in ALS.ALCUsers
                              select new
                              {
                                  r.user_id,
                                  r.first_name,
                                  r.last_name

                              }).ToList();

                    return Json(cs);

                }
            }
            catch (Exception E)
            {
                LogError("TransschedController/GetStaff", "Error Message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }

        }


        public ActionResult TransschedView(string type)
        {
            if (type == "dailytask")
            {
                return PartialView("../Views/Shared/Preferences.cshtml");
            }
            else
            {
                return null;
            }
        }

        private ActivityScheduleData GetActivityScheduleData()
        {
            using (var ALSDB = new ALSBaseDataContext())
            {
                int roleId = Convert.ToInt32(RoleID);
                int? deptId = Utility.GetDeptIdByDeptHeadId(roleId);
                if (!deptId.HasValue) deptId = 4;

                return new ActivityScheduleData
                {
                    Activities = ALSDB.Activities
                                .Where(x => (!deptId.HasValue) || x.dept_id == deptId)
                                .Select(x => new ActivityData { Id = x.activity_id, Desc = (x.activity_desc ?? ""), Proc = (x.activity_procedure ?? "") })
                                .OrderBy(x => x.Desc).ToList(),
                    Residents = (from x in ALSDB.Residents
                                 join y in ALSDB.Admissions on x.resident_id equals y.resident_id
                                 where (x.admission_status ?? 0) != 2
                                 orderby x.first_name
                                 select new SelectData { Id = x.resident_id, Name = x.last_name + ", " + x.first_name + " " + x.middle_initial ?? "", IsAdmitted = x.isAdmitted.ToString() }).ToList(),
                    //kim modified format name 03/29/22
                    Rooms = (from x in ALSDB.Rooms
                             orderby x.room_name
                             select new SelectData { Id = x.room_id, Name = x.room_name }).ToList(),
                    //Carestaffs = (from x in ALS.getCarestaffServiceTime(time, activityid) select new SelectData2 { Name = x.carestaff, Id = x.user_id }).ToList(),
                    Departments = GetDepartments(),
                    Users = (from u in ALSDB.ALCUsers
                             join ur in ALSDB.ALCUserRoles on u.user_id equals ur.user_id
                             orderby u.last_name
                             select new SelectData2 { Id = u.user_id, Name = u.last_name + ", " + u.first_name + " " + u.middle_initial ?? "", Role = ur.role_id }).ToList()

                };
            };
        }

        private DeptData GetDepartments()
        {
            using (var ALSDB = new ALSBaseDataContext())
            {
                int roleId = int.Parse(RoleID);
                int? deptId = Utility.GetDeptIdByDeptHeadId(roleId);
                if (!deptId.HasValue) deptId = 4;

                return new DeptData
                {
                    //Depts = ALSDB.ALCRoles kim comment this 05/17/22
                    //.Where(x => Utility.Departments.Contains(x.role_id))
                    //.Select(x => new SelectData { Id = x.role_id, Name = x.description }).ToList(),
                    //SelectedValue = deptId.ToString()

                    Depts = ALSDB.Departments
                    //Commented this out to show all departments on dept dropdowns
                    //.Where(x => Utility.Departments.Contains(x.department_id))
                    .Select(x => new SelectData { Id = x.department_id, Name = x.name }).ToList(),
                    SelectedValue = deptId.ToString()


                };
            }
        }

    }
}