﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using DataSoft.Helpers;
using Datasoft.Ext;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Datasoft.AssistedLiving.Web.Models;
using System.Net.Mail;

namespace Datasoft.AssistedLiving.Web.Controllers {
    public class StaffController : Controller {
        private string UserID {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        private string RoleID {
            get { return User.Identity.Name.Split('|')[2]; }
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior) {
            return new JsonDataContractResult {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        public ActionResult LogError(string module, string message)
        {
            var log = new ErrorController();
            log.ControllerContext = ControllerContext;
            return log.LogError(module, message);
        }

        // GET: Carestaff
        public ActionResult Index() {
            //int roleid = int.Parse(RoleID);

            ////caregiver
            //if(roleid == 4)
            //    name = "Caregiver";
            ////Housekeeper
            //else if (roleid == 5)
            //    name = "Housekeeper";
            ////Dietary
            //else if(roleid == 6)
            //    name = "Dietary";
            ////Maintenance
            //else if (roleid == 7)
            //    name = "Maintenance";

            using (var ALS = new ALSBaseDataContext()) {
                int roleId = int.Parse(RoleID);
                var res = (from x in ALS.ALCUsers
                           join y in ALS.CarestaffShifts on x.user_id equals y.carestaff_id into y1 from ys in y1.DefaultIfEmpty()
                           join z in ALS.Shifts on ys.shift_id equals z.shift_id into z1 from zs in z1.DefaultIfEmpty()
                           join a in ALS.ALCUserRoles on x.user_id equals a.user_id into a1 from As in a1.DefaultIfEmpty()
                           join b in ALS.ALCRoles on As.role_id equals b.role_id into b1 from bs in b1.DefaultIfEmpty()
                           where x.user_id == UserID && As.role_id == roleId
                           select new {
                               x.middle_initial, x.first_name, x.last_name, role = bs.description,
                               start_time = Utility.GetDateFormattedString(zs.start_time, "hh:mmtt"),
                               end_time = Utility.GetDateFormattedString(zs.end_time, "hh:mmtt")
                           }).SingleOrDefault();
                string mi = !string.IsNullOrEmpty(res.middle_initial) ? res.middle_initial + ". " : "";
                ViewBag.Name = res.first_name + " " + mi + res.last_name;
                ViewBag.Role = res.role;
                ViewBag.StartShiftTime = res.start_time;
                ViewBag.EndShiftTime = res.end_time;

                var headlist = new int[] { 9, 10, 11, 12, 13 };
                if (roleId == 1)
                    ViewBag.Role = "Administrator";

                ViewBag.IsDeptHead = headlist.Contains(roleId) ? true : false;
                ViewBag.IsAdmin = roleId == 1 ? true : false;
                ViewBag.RoleID = roleId;

                //var tz = TimeZoneInfo.GetSystemTimeZones();
                //foreach (var t in tz) {
                //    System.Diagnostics.Debug.WriteLine("DisplayName:{0}", t.DisplayName);
                //    System.Diagnostics.Debug.WriteLine("Id:{0}", t.Id);
                //    System.Diagnostics.Debug.WriteLine("StandardName:{0}", t.StandardName);
                //    System.Diagnostics.Debug.WriteLine("Ba""seUtcOffset:{0}", t.BaseUtcOffset);
                //    System.Diagnostics.Debug.WriteLine("DaylightName:{0}", t.DaylightName);
                //}

            }

            var objModel = GetActivityScheduleData();

            return View(objModel);
        }

        public ActionResult IStaff() {
            //int roleid = int.Parse(RoleID);

            ////caregiver
            //if(roleid == 4)
            //    name = "Caregiver";
            ////Housekeeper
            //else if (roleid == 5)
            //    name = "Housekeeper";
            ////Dietary
            //else if(roleid == 6)
            //    name = "Dietary";
            ////Maintenance
            //else if (roleid == 7)
            //    name = "Maintenance";

            using (var ALS = new ALSBaseDataContext()) {
                int roleId = int.Parse(RoleID);
                var res = (from x in ALS.ALCUsers
                           join y in ALS.CarestaffShifts on x.user_id equals y.carestaff_id into y1 from ys in y1.DefaultIfEmpty()
                           join z in ALS.Shifts on ys.shift_id equals z.shift_id into z1 from zs in z1.DefaultIfEmpty()
                           join a in ALS.ALCUserRoles on x.user_id equals a.user_id into a1 from As in a1.DefaultIfEmpty()
                           join b in ALS.ALCRoles on As.role_id equals b.role_id into b1 from bs in b1.DefaultIfEmpty()
                           where x.user_id == UserID && As.role_id == roleId
                           select new {
                               x.middle_initial, x.first_name, x.last_name, role = bs.description,
                               start_time = Utility.GetDateFormattedString(zs.start_time, "hh:mmtt"),
                               end_time = Utility.GetDateFormattedString(zs.end_time, "hh:mmtt")
                           }).SingleOrDefault();
                string mi = !string.IsNullOrEmpty(res.middle_initial) ? res.middle_initial + ". " : "";
                ViewBag.Name = res.first_name + " " + mi + res.last_name;
                ViewBag.Role = res.role;
                ViewBag.StartShiftTime = res.start_time;
                ViewBag.EndShiftTime = res.end_time;

                var headlist = new int[] { 9, 10, 11, 12, 13 };
                if (roleId == 1)
                    ViewBag.Role = "Administrator";

                ViewBag.IsDeptHead = headlist.Contains(roleId) ? true : false;
                ViewBag.IsAdmin = roleId == 1 ? true : false;
                ViewBag.RoleID = roleId;

                //var tz = TimeZoneInfo.GetSystemTimeZones();
                //foreach (var t in tz) {
                //    System.Diagnostics.Debug.WriteLine("DisplayName:{0}", t.DisplayName);
                //    System.Diagnostics.Debug.WriteLine("Id:{0}", t.Id);
                //    System.Diagnostics.Debug.WriteLine("StandardName:{0}", t.StandardName);
                //    System.Diagnostics.Debug.WriteLine("Ba""seUtcOffset:{0}", t.BaseUtcOffset);
                //    System.Diagnostics.Debug.WriteLine("DaylightName:{0}", t.DaylightName);
                //}

            }

            var objModel = GetActivityScheduleData();

            return View(objModel);
        }

        //Added "start" and "OrderBy" on 2017-07-17 by Mariel

        public dynamic GetGridData(string func, string param, string param2, string param3, string param4) {

            var ind = "";

            try {
                if (func == AdminConstants.ACTIVITY_SCHEDULE) {
                   // ind = "<b>func == AdminConstants.ACTIVITY_SCHEDULE</b><br/>";
                    using (var ALS = new ALSBaseDataContext()) {                       
                        DateTime? fromDate = param.TryParseNullableDate(false);
                        DateTime? toDate = param2.TryParseNullableDate(false);
                        int roleId = int.Parse(RoleID);

                        //overriding roleId so that guests can access transsched schedule

                        //var role = ALS.ALCRoles.SingleOrDefault(x => x.role_id == roleId);

                        //if (role.description == "Guest")
                        //{
                        //    roleId = 1;
                        //}
                        //modified by Cheche : 10-27-2022 
                        var role = ALS.ALCRoles.Where(z => z.role_id == null).SingleOrDefault();
                        if (role == null)
                        {
                            roleId = 1;
                        }
                        else
                        {
                            var Role = ALS.ALCRoles.SingleOrDefault(x => x.role_id == roleId);
                            if (role.description == "Guest")
                            {
                                roleId = 1;
                            }
                        }
                        //ends here
                        int? p3 = !string.IsNullOrEmpty(param3) ? (int?)int.Parse(param3) : null;
                        int? p4 = !string.IsNullOrEmpty(param4) ? (int?)int.Parse(param4) : null;
                        var cs = (from c in ALS.getActivityScheduleByUser(UserID, roleId, fromDate, toDate, p3, p4)
                                  select new {
                                      c.activity_desc,
                                      c.activity_id,
                                      c.activity_proc,
                                      c.activity_schedule_id,
                                      c.activity_status,
                                      c.oactivity_status,
                                      c.department,
                                      c.carestaff_activity_id,
                                      completion_date = (c.completion_date == null? null : Utility.GetDateFormattedString(c.completion_date, "MM/dd/yyyy hh:mm tt")), 
                                      c.completion_type,
                                      c.remarks,
                                      reschedule_dt = Utility.GetDateFormattedString(c.reschedule_dt, "MM/dd/yyyy hh:mm tt"),
                                      c.carestaff_name,
                                      c.resident,
                                      resident2 = c.resident,
                                      carestaff2 = c.carestaff_name,
                                      activitydesc2 = c.activity_desc,
                                      c.resident_id,
                                      c.room,
                                      c.service_duration,
                                      c.acknowledged_by,
                                      start_time = Utility.GetDateFormattedString(c.start_time, "hh:mm tt"),
                                      c.xref,
                                      actual_completion_date = Utility.GetDateFormattedString(c.actual_completion_date, "hh:mm tt"),
                                      actual_completiondt = Utility.GetDateFormattedString(c.actual_completion_date, "MM/dd/yyyy hh:mm tt"),
                                      timelapsed = Utility.IsTimeLapseFromCurrentTime(c.start_time, param.TryParseNullableDate(false)),
                                      start = Utility.GetDateFormattedString(c.start_time, "HH:mm"),
                                      c.isAdmitted,
                                      c.responsible_person_email,
                                      c.is_active,
                                      active_until = (c.active_until != null? Utility.GetDateFormattedString(c.active_until, "MM/dd/yyyy hh:mm tt"): null),
                                      c.is_maintenance,
                                      c.acknowledgeBy

                                  }).OrderBy(x => x.start).ToList();
                        return Json(cs, JsonRequestBehavior.AllowGet);
                     }
                }
            } catch (Exception E) {

                ind = "<b>func == " + func +"</b><br/>";
                ind += "Line Number: " + E.LineNumber() + "<br>";
                LogError("StaffController/GetGridData", ind + "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("[]", JsonRequestBehavior.AllowGet);
        }
        
        public dynamic GetTaskScheduleChanges(string param) {
            try {
                if (!string.IsNullOrEmpty(param)) {
                    using (var ALS = new ALSBaseDataContext()) {
                        DateTime schedTime = DateTime.MinValue;
                        if(!DateTime.TryParseExact(param, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out schedTime))
                            return Json("[]", JsonRequestBehavior.AllowGet);

                        // var cs = (from c in ALS.getActivityScheduleChangesByUser(UserID, schedTime, null) select new { c.asid, c.status }).ToList();
                        //return Json(cs, JsonRequestBehavior.AllowGet);
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
            } catch (Exception E) {

                LogError("StaffController/GetTaskScheduleChanges", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("[]", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PostData([DynamicJson(MatchName = false)] dynamic obj) {
            var result = JsonResponse.GetDefault();

            if (obj != null)
            {
                if (obj.Func == AdminConstants.CARESTAFF_TASK_COMPLETION)
                {
                    result = ParseStaffCompletionInfo(obj.Data);
                }
                else if (obj.Func == AdminConstants.ACTIVITY_SCHEDULE)
                {
                    CompleteMissedTask(obj.Data);
                    result = ParseActivitySchedule(obj.Data);
                }
            }
            return Json(result);
        }


        private void CompleteMissedTask([DynamicJson] dynamic obj)
        {
            var ind = "";
            try
            {

                int? csa_id = ((string)obj.CarestaffActivityId).TryParseNullableInt();
                int? as_id = ((string)obj.Id).TryParseNullableInt();
                string cs_name = (obj.CarestaffName);
                //string res_name = (obj.Resident);
                //var res_fn = res_name.Split(' ');
                //string act_desc = (obj.ActivityDesc);
                //string resfn = res_fn[0];
                string token = "";
                bool sendToken = false;
                string department = (obj.Department);
                string completion_status;
                int? resident_id = ((string)obj.ResidentId).TryParseNullableInt();
                bool? IsMaintenance = ((string)obj.IsMaintenance).TryParseNullableBool();
                //string ackBy = (string)obj.AcknowledgedBy;
                string remarks = (string)obj.Remarks;
                DateTime? compDate = ((string)obj.completion).TryParseNullableDate(false);

                CarestaffActivity cs = null;
                bool isNew = false;
                using (var ALSDB = new ALSBaseDataContext())
                {
                    if (csa_id == null)
                    {
                        cs = new CarestaffActivity();
                        isNew = true;
                    }
                    else
                        cs = ALSDB.CarestaffActivities.SingleOrDefault(x => x.carestaff_activity_id == csa_id.Value);

                    //Console.WriteLine((compDate.Value.Date == DateTime.UtcNow.Date));
                    //Console.WriteLine(compDate.Value.Date);
                    //Console.WriteLine(DateTime.UtcNow.Date);

                    //for token storage
                    //if ((department != "Housekeeper") && (department != "Maintenance") && IsMaintenance == false)
                    //{
                    //    if (isNew)
                    //    {

                    //        GuestNotification gn = null;

                    //        gn = ALSDB.GuestNotifications.SingleOrDefault(x => x.resident_id == resident_id);

                    //        //creating token
                    //        byte[] time = BitConverter.GetBytes(DateTime.Now.ToBinary());
                    //        byte[] key = Guid.NewGuid().ToByteArray();
                    //        byte[] completion = BitConverter.GetBytes(DateTime.Now.Date.ToBinary());
                    //        token = Convert.ToBase64String(time.Concat(key).Concat(completion).ToArray());

                    //        var completionDate = Convert.ToDateTime(compDate);

                    //        if (gn != null)
                    //        {
                    //            if (completionDate.Date == DateTime.Now.Date)
                    //            {
                    //                if (gn.date_created != DateTime.Now.Date)
                    //                {
                    //                    gn.token = token;
                    //                    gn.date_created = DateTime.Now;

                    //                    sendToken = true;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {

                    //            if (completionDate.Date == DateTime.Now.Date)
                    //            {
                    //                gn = new GuestNotification();

                    //                gn.token = token;
                    //                gn.resident_id = resident_id;
                    //                gn.date_created = DateTime.Now;
                    //                ALSDB.GuestNotifications.InsertOnSubmit(gn);

                    //                sendToken = true;
                    //            }

                    //        }

                    //    }
                    //}


                    cs.activity_schedule_id = as_id ?? 0;
                    cs.completion_date = compDate;
                    cs.carestaff_id = UserID;

                    cs.actual_completion_date = DateTime.Now;
                    //1= acknowledge 2=Overdue
                    //if (obj.No)
                    //{
                    //    cs.activity_status = null;
                    //    completion_status = "Uncompleted";
                    //}
                    //else
                    //{
                        cs.activity_status = 1;
                        completion_status = "Completed";
                    //}

                    if (remarks == "")
                        remarks = "";
                    else
                        remarks = "Remarks: " + remarks;

                    cs.remarks = cs.remarks + Environment.NewLine + "&#x2022; (Rescheduled / Reassigned - " + Utility.ToTimezonedDateTime(DateTime.UtcNow, "MMM dd, yyyy hh:mm tt") + ") " + remarks;


                    //if ((cs.activity_status == 1) && (department != "Housekeeper") && (department != "Maintenance") && (IsMaintenance == false))
                    //{

                    //    //FOR EMAIL TO : IF TASK IS COMPLETED (-ABC)

                    //    try
                    //    {
                    //        ind = "<b>EMAIL TO RESPONSIBLE PERSON</b><br/>";
                    //        var res = (from x in ALSDB.Residents
                    //                   where x.resident_id == resident_id
                    //                   select new
                    //                   {
                    //                       email = x.responsible_person_email,
                    //                       resp_person = x.responsible_person,
                    //                       send_email = x.send_email
                    //                   }).SingleOrDefault();

                    //        Agency a = ALSDB.Agencies.SingleOrDefault(x => x.agency_name == AgencyID);
                    //        var agency_name = a.agency_name;

                    //        var base64link = System.Text.Encoding.UTF8.GetBytes(agency_name + "=" + res.email);
                    //        string link = System.Convert.ToBase64String(base64link);

                    //        var b64link = System.Text.Encoding.UTF8.GetBytes(agency_name);
                    //        string alink = System.Convert.ToBase64String(b64link);


                    //        //string LogInLink = "http://localhost/ALS/Login?" + alink; //uncomment this line for local testing
                    //        string LogInLink = "www.dslalc.com/ALC/Login?" + alink; //comment this line for local testing


                    //        //string sendLink = "http://localhost/ALS/Account/RegisterGuest?" + link; //uncomment this line for local testing
                    //        string sendLink = "www.dslalc.com/ALC/Account/RegisterGuest?" + link; //uncomment this line for local testing


                    //        //if (res.send_email == "True" && sendToken)
                    //        ////if (sendToken)
                    //        //{
                    //        //    string localPath = Server.MapPath("~/Views/Shared/TaskSendEmailNotification.cshtml");
                    //        //    string content = System.IO.File.ReadAllText(localPath);

                    //        //    content = content.Replace("<%=TP_Name%>", res.resp_person);
                    //        //    content = content.Replace("<%=TP_CarestaffName%>", cs_name);
                    //        //    content = content.Replace("<%=TP_TaskName%>", act_desc);
                    //        //    content = content.Replace("<%=TP_ResidentName%>", res_name);
                    //        //    content = content.Replace("<%=TP_CompletionDate%>", cs.actual_completion_date.ToString());

                    //        //    MailMessage mail = new MailMessage();

                    //        //    //mail.To.Add("pmarieljade@gmail.com");

                    //        //    mail.To.Add(res.email); //responsiblepersonemail
                    //        //    mail.From = new MailAddress("dslalc@datasoftlogic.com");
                    //        //    mail.Subject = "Carestaff Service Rendered to Resident " + res_name;
                    //        //    mail.Body = content;

                    //        //    mail.Body += "You can check the services rendered through this link <a href=\"" + LogInLink + "\">Rendered Services</a>.<br/>";
                    //        //    mail.Body += "If you don't have any account yet, <a href=\"" + sendLink + "\"> please click this link to register</a>.";

                    //        //    mail.Body += "<br><br>If you have any questions or other concerns, please contact our facility. <br><br>Thanks,<br> The Assisted Living Centre Team ";

                    //        //    mail.IsBodyHtml = true;
                    //        //    SmtpClient smtp = new SmtpClient();
                    //        //    smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                    //        //                                  //smtp.Host = "10.10.1.149"; //Or Your SMTP Server Address
                    //        //    smtp.Credentials = new System.Net.NetworkCredential
                    //        //         ("dslalc@datasoftlogic.com", "dslalciloilo17"); // ***use valid credentials***
                    //        //    smtp.Port = 587;
                    //        //    //smtp.Port = 25;

                    //        //    //Or your Smtp Email ID and Password
                    //        //    smtp.EnableSsl = true;
                    //        //    smtp.Send(mail);


                    //        //}
                    //        //===================================================

                    //    }
                    //    catch (Exception E)
                    //    {
                    //        ind += "Line Number: " + E.LineNumber() + "<br>";
                    //        LogError("StaffController/ParseStaffCompletionInfo", ind + "Error Message: " + E.Message);

                    //        //return new JsonResponse(JsonResponseResult.Error, E.Message);
                    //    }
                    //}
                    if (isNew)
                        ALSDB.CarestaffActivities.InsertOnSubmit(cs);

                    ALSDB.SubmitChanges();
                }
            }
            catch (Exception E)
            {

                ind += "Line Number: " + E.LineNumber() + "<br>";
                        LogError("StaffController/ParseStaffCompletionInfo", ind + "Error Message: " + E.Message);

                //return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            // return JsonResponse.GetDefault();
        }


        private JsonResponse ParseStaffCompletionInfo([DynamicJson] dynamic obj) {
            var ind = "";
            try {
                int? csa_id = ((string)obj.CarestaffActivityId).TryParseNullableInt();
                int? as_id = ((string)obj.ActivityScheduleId).TryParseNullableInt();
                string cs_name = (obj.CarestaffName);
                string res_name = (obj.Resident);
                var res_fn = res_name.Split(' ');
                string act_desc = (obj.ActivityDesc);
                string resfn = res_fn[0];
                string token = "";
                bool sendToken = false;

                string department = (obj.Department);
                string completion_status;
                int resident_id = (obj.resident_id == "null"? 0 : Convert.ToInt32(obj.resident_id));
                bool? IsMaintenance = ((string)obj.IsMaintenance).TryParseNullableBool();
                //string ackBy = (string)obj.AcknowledgedBy;
                string remarks = (string)obj.Remarks;
                DateTime? compDate = ((string)obj.completion).TryParseNullableDate(false);

                CarestaffActivity cs = null;
                bool isNew = false;
                using (var ALSDB = new ALSBaseDataContext()) {
                    if (csa_id == null) {
                        cs = new CarestaffActivity();
                        isNew = true;
                    } else
                        cs = ALSDB.CarestaffActivities.SingleOrDefault(x => x.carestaff_activity_id == csa_id.Value);

                    //Console.WriteLine((compDate.Value.Date == DateTime.UtcNow.Date));
                    //Console.WriteLine(compDate.Value.Date);
                    //Console.WriteLine(DateTime.UtcNow.Date);

                    //for token storage
                    if ((department != "Housekeeper") && (department != "Maintenance") && IsMaintenance == false )
                    {
                        if (isNew)
                        {

                            GuestNotification gn = null;

                            gn = ALSDB.GuestNotifications.SingleOrDefault(x => x.resident_id == resident_id);

                            //creating token
                            byte[] time = BitConverter.GetBytes(DateTime.Now.ToBinary());
                            byte[] key = Guid.NewGuid().ToByteArray();
                            byte[] completion = BitConverter.GetBytes(DateTime.Now.Date.ToBinary());
                            token = Convert.ToBase64String(time.Concat(key).Concat(completion).ToArray());

                            var completionDate = Convert.ToDateTime(compDate);

                            if (gn != null)
                            {
                                if (completionDate.Date == DateTime.Now.Date)
                                {
                                    if (gn.date_created != DateTime.Now.Date)
                                    {
                                        gn.token = token;
                                        gn.date_created = DateTime.Now;

                                        sendToken = true;
                                    }
                                }
                            }
                            else
                            {

                                if (completionDate.Date == DateTime.Now.Date)
                                {
                                    gn = new GuestNotification();

                                    gn.token = token;
                                    gn.resident_id = resident_id;
                                    gn.date_created = DateTime.Now;
                                    ALSDB.GuestNotifications.InsertOnSubmit(gn);

                                    sendToken = true;
                                }

                            }

                        }
                    }


                    cs.activity_schedule_id = as_id ?? 0;
                    cs.completion_date = compDate;
                    cs.carestaff_id = UserID;

                    cs.actual_completion_date = DateTime.Now;
                    //1= acknowledge 2=Overdue
                    if (obj.No)
                    {
                        cs.activity_status = null;
                        completion_status = "Uncompleted";
                    }
                    else
                    {
                        cs.activity_status = 1;
                        completion_status = "Completed";
                    }

                    if (remarks == "")
                        remarks = "";
                    else
                        remarks = "Remarks: " + remarks;

                    cs.remarks = cs.remarks + Environment.NewLine + "&#x2022; (" + completion_status + " - " + Utility.ToTimezonedDateTime(DateTime.UtcNow, "MMM dd, yyyy hh:mm tt") + ") " + remarks;


                    if ((cs.activity_status == 1) && (department != "Housekeeper") && (department != "Maintenance") && (IsMaintenance == false) && (resident_id != 0))
                    {

                        //FOR EMAIL TO : IF TASK IS COMPLETED (-ABC)
                        
                        try
                        {
                            ind = "<b>EMAIL TO RESPONSIBLE PERSON</b><br/>";
                            var res = (from x in ALSDB.Residents
                                       where x.resident_id == resident_id
                                       select new
                                       {
                                           email = x.responsible_person_email,
                                           resp_person = x.responsible_person,
                                           send_email = x.send_email
                                       }).SingleOrDefault();

                            Agency a = ALSDB.Agencies.SingleOrDefault(x => x.agency_name == AgencyID);
                            var agency_name = a.agency_name;

                            var base64link = System.Text.Encoding.UTF8.GetBytes(agency_name + "=" + res.email);
                            string link = System.Convert.ToBase64String(base64link);

                            var b64link = System.Text.Encoding.UTF8.GetBytes(agency_name);
                            string alink = System.Convert.ToBase64String(b64link);


                            //string LogInLink = "http://localhost/ALS/Login?" + alink; //uncomment this line for local testing
                            string LogInLink = "www.dslalc.com/ALC/Login?" + alink; //comment this line for local testing


                            //string sendLink = "http://localhost/ALS/Account/RegisterGuest?" + link; //uncomment this line for local testing
                            string sendLink = "www.dslalc.com/ALC/Account/RegisterGuest?" + link; //uncomment this line for local testing


                            if (res.send_email == "True" && sendToken)
                            //if (sendToken)
                            {
                                string localPath = Server.MapPath("~/Views/Shared/TaskSendEmailNotification.cshtml");
                                string content = System.IO.File.ReadAllText(localPath);

                                content = content.Replace("<%=TP_Name%>", res.resp_person);
                                content = content.Replace("<%=TP_CarestaffName%>", cs_name);
                                content = content.Replace("<%=TP_TaskName%>", act_desc);
                                content = content.Replace("<%=TP_ResidentName%>", res_name);
                                content = content.Replace("<%=TP_CompletionDate%>", cs.actual_completion_date.ToString());

                                MailMessage mail = new MailMessage();

                                //mail.To.Add("pmarieljade@gmail.com");

                                mail.To.Add(res.email); //responsiblepersonemail
                                mail.From = new MailAddress("dslalc@datasoftlogic.com");
                                mail.Subject = "Carestaff Service Rendered to Resident " + res_name;
                                mail.Body = content;

                                mail.Body += "You can check the services rendered through this link <a href=\"" + LogInLink + "\">Rendered Services</a>.<br/>";
                                mail.Body += "If you don't have any account yet, <a href=\"" + sendLink + "\"> please click this link to register</a>.";

                                mail.Body += "<br><br>If you have any questions or other concerns, please contact our facility. <br><br>Thanks,<br> The Assisted Living Centre Team ";

                                mail.IsBodyHtml = true;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                                                              //smtp.Host = "10.10.1.149"; //Or Your SMTP Server Address
                                smtp.Credentials = new System.Net.NetworkCredential
                                     ("dslalc@datasoftlogic.com", "dslalciloilo17"); // ***use valid credentials***
                                smtp.Port = 587;
                                //smtp.Port = 25;

                                //Or your Smtp Email ID and Password
                                smtp.EnableSsl = true;
                                smtp.Send(mail);


                            }
                            //===================================================

                        }
                        catch (Exception E)
                        {
                            ind += "Line Number: " + E.LineNumber() + "<br>";
                            LogError("StaffController/ParseStaffCompletionInfo", ind + "Error Message: " + E.Message);

                            return new JsonResponse(JsonResponseResult.Error, E.Message);
                        }
                    }
                    if (isNew) 
                        ALSDB.CarestaffActivities.InsertOnSubmit(cs);

                    ALSDB.SubmitChanges();

                    ALSDB.saveAction("2", UserID, as_id, "CARESTAFF_TASK_COMPLETION", "csActivityId=" + cs.carestaff_activity_id + "; activityId=" + as_id + "; residentId=" + resident_id);
                }
            } catch (Exception E) {

                LogError("StaffController/ParseStaffCompletionInfo", "Error Message: " + E.Message);

                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();
        }

        private JsonResponse ParseActivitySchedule(dynamic obj) {

            try {
                string CarestaffId = (string)obj.CarestaffId;
                int? ResidentId = ((string)obj.ResidentId).TryParseNullableInt();
                int? RoomId = ((string)obj.RoomId).TryParseNullableInt();
                int? ActivityScheduleId = ((string)obj.Id).TryParseNullableInt();
                int? ActivityId = ((string)obj.ActivityId).TryParseNullableInt();
                string Recurrence = (string)obj.Recurrence;
                bool? isonetime = ((string)obj.isonetime).TryParseNullableBool();
                DateTime? clientdt = ((string)obj.clientdt).TryParseNullableDate(false);
                string schedTime = (string)obj.ScheduleTime;
                int comptype = Convert.ToInt32(obj.comptype);

                
                bool? isRescheduled = ((string)obj.isRescheduled).TryParseNullableBool();
                DateTime? date_created = ((string)obj.date_created).TryParseNullableDate(false);

                DateTime startTime;
                if (isonetime != null && isonetime.Value)
                    DateTime.TryParseExact(schedTime, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out startTime);
                else
                    DateTime.TryParseExact(schedTime, "hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out startTime);
                DateTime? ScheduleTime = startTime;

                ActivitySchedule cs = null;
                ActivitySchedule originalCS = null;
                using (var ALSDB = new ALSBaseDataContext()) {
                    using (var tran = new System.Transactions.TransactionScope()) {
                        var exists = ALSDB.hasExistingScheduleByRecurrence(ScheduleTime, ActivityId, ActivityScheduleId, ResidentId, RoomId, Recurrence, clientdt, isonetime).SingleOrDefault();
                        if (exists != null && !string.IsNullOrEmpty(exists.Conflict)) {
                            string msg = exists.Conflict;
                            return new JsonResponse(JsonResponseResult.Exists, msg);
                        }


                        cs = new ActivitySchedule();
                        cs.created_by = UserID;

                        if (isRescheduled == true)
                        {
                            cs.date_created = date_created;
                        }
                        else
                        {
                            cs.date_created = DateTime.Now;
                        }
                        
                        cs.resident_id = ResidentId;
                        cs.room_id = RoomId;
                        if (!string.IsNullOrEmpty(CarestaffId))
                            cs.carestaff_id = CarestaffId;
                        else
                            cs.carestaff_id = null;
                        cs.activity_id = ActivityId ?? 0;
                        cs.start_time = ScheduleTime;

                        if (isonetime != null && isonetime.Value)
                            cs.is_onetime = true;
                        else
                            cs.is_onetime = null;

                        cs.recurrence = Recurrence;
                        cs.is_active = true;
                        ALSDB.ActivitySchedules.InsertOnSubmit(cs);
                        ALSDB.SubmitChanges();

                        int Id = Convert.ToInt32(obj.Id);
                        DateTime? compDate = ((string)obj.completion).TryParseNullableDate(false);

                        originalCS = ALSDB.ActivitySchedules.SingleOrDefault(x => x.activity_schedule_id == Id);
                        if (originalCS != null) {
                            originalCS.xref = cs.activity_schedule_id;

                            var csa = new CarestaffActivity();
                            csa.activity_schedule_id = Id;
                            csa.completion_date = (isRescheduled == true? DateTime.Now: compDate);
                            csa.carestaff_id = UserID;
                            csa.remarks = (comptype == 2 ? "Reassigned - " : "Rescheduled - ") + Utility.GetDateFormattedString(ScheduleTime, "MM/dd/yyyy hh:mm tt");
                            csa.activity_status = 1;
                            csa.completion_type = (int)CompletionType.Rescheduled;
                            if (comptype == 2)
                                csa.completion_type = (int)CompletionType.Reassigned;

                            ALSDB.CarestaffActivities.InsertOnSubmit(csa);                          
                            ALSDB.SubmitChanges();
                            ALSDB.saveAction("2", UserID, ActivityScheduleId, "activity_schedule:" + (comptype == 2 ? "reassign" : "reschedule"), "ActivityId=" + ActivityScheduleId);
                        }
                        tran.Complete();
                        
                    }
                }
            } catch (Exception E) {

                LogError("StaffController/ParseActivitySchedule", "Error Message: " + E.Message);
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();

        }

        private ActivityScheduleData GetActivityScheduleData() {

            using (var ALSDB = new ALSBaseDataContext()) {
                int roleId = Convert.ToInt32(RoleID);
                int? deptId = Utility.GetDeptIdByDeptHeadId(roleId);
                if (!deptId.HasValue) deptId = 4;

                return new ActivityScheduleData {
                    Activities = ALSDB.Activities
                                .Where(x => (!deptId.HasValue) || x.dept_id == deptId)
                                .Select(x => new ActivityData { Id = x.activity_id, Desc = (x.activity_desc ?? ""), Proc = (x.activity_procedure ?? "") })
                                .OrderBy(x => x.Desc).ToList(),
                    Residents = (from x in ALSDB.Residents
                                 join y in ALSDB.Admissions on x.resident_id equals y.resident_id
                                 where (x.admission_status ?? 0) != 2
                                 orderby x.first_name
                                 select new SelectData { Id = x.resident_id, Name = x.first_name + " " + x.last_name }).ToList(),
                    Rooms = (from x in ALSDB.Rooms
                             orderby x.room_name
                             select new SelectData { Id = x.room_id, Name = x.room_name }).ToList(),
                    //Carestaffs = (from x in ALS.getCarestaffServiceTime(time, activityid) select new SelectData2 { Name = x.carestaff, Id = x.user_id }).ToList(),
                    Departments = GetDepartments()
                };
            };
        }

        private DeptData GetDepartments() {
            using (var ALSDB = new ALSBaseDataContext()) {
                int roleId = int.Parse(RoleID);
                int? deptId = Utility.GetDeptIdByDeptHeadId(roleId);
                if (!deptId.HasValue) deptId = 4;

                return new DeptData {
                    Depts = ALSDB.ALCRoles
                    .Where(x => Utility.Departments.Contains(x.role_id))
                    .Select(x => new SelectData { Id = x.role_id, Name = x.description }).ToList(),
                    SelectedValue = deptId.ToString()
                };
            }
        }

    }
}