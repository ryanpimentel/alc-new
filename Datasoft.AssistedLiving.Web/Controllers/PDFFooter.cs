﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class PDFFooter : PdfPageEventHelper
    {

        public string form { get; set; }
        string footer = "";
        // write on top of document
        //public override void OnOpenDocument(PdfWriter writer, Document document)
        //{
        //    base.OnOpenDocument(writer, document);
        //    PdfPTable tabFot = new PdfPTable(new float[] { 1F });
        //    tabFot.SpacingAfter = 10F;
        //    PdfPCell cell;
        //    tabFot.TotalWidth = 300F;
        //    cell = new PdfPCell(new Phrase("Header"));
        //    tabFot.AddCell(cell);
        //    tabFot.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
        //}

        // write on start of each page
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
        }

        // write on end of each page
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
            PdfPTable table = new PdfPTable(new float[] { 1F });
            table.TotalWidth = 400F;

            string rbrf = "";


            BaseFont bf = BaseFont.CreateFont(
                        BaseFont.HELVETICA,
                        BaseFont.CP1252,
                        BaseFont.EMBEDDED);
            Font fontH1 = new Font(bf, 8, Font.BOLD);

            if (form == "pr_template")
            {
                if (document.PageNumber > 6)
                {
                    footer = "LIC 602A(12/04) (CONFIDENTIAL)";
                }
                else
                {
                    footer = "LIC 602A(12/04) (CONFIDENTIAL) | RB RF-" + Convert.ToString(document.PageNumber + 9);
                }

            }
            else if (form == "pai_template")
            {
                if (document.PageNumber > 2)
                {
                    footer = "LIC 603 (9/99)";
                }
                else
                {
                    footer = "LIC 603 (9/99) | RB RF-" + Convert.ToString(document.PageNumber + 18);
                }


            }
            else if (form == "rie_template")
            {
                if (document.PageNumber > 1)
                {
                    footer = "LIC 601(1/08)Personal";
                }
                else
                {
                    footer = "LIC 601(1/08)Personal | RB RF-" + Convert.ToString(document.PageNumber + 8);
                }

            }
            else if (form == "cmt_template")
            {
                if (document.PageNumber > 1)
                {
                    footer = "LIC 627C (ENG/SP)(4/00)(CONFIDENTIAL)";
                }
                else
                {
                    footer = "LIC 627C (ENG/SP)(4/00)(CONFIDENTIAL) | RB RF-" + Convert.ToString(document.PageNumber + 17);
                }
            }
            else if (form == "romi_template")
            {
                if (document.PageNumber > 1)
                {
                    footer = "LIC 605 A(5/00)";
                }
                else
                {
                    footer = "LIC 605 A(5/00) | RB RF-" + Convert.ToString(document.PageNumber + 15);
                }
            }
            else if (form == "ra_template")
            {
                if (document.PageNumber > 2)
                {
                    footer = "LIC 603A(7/99)";
                }
                else
                {
                    footer = "LIC 603A(7/99) | RB RF-" + Convert.ToString(document.PageNumber + 20);
                }
            }
            else if (form == "per_template")
            {
                if (document.PageNumber > 2)
                {
                    footer = "LIC 613C(4/04) (Confidential)";
                }
                else
                {
                    footer = "LIC 613C(4/04) (Confidential) | RB RF-" + Convert.ToString(document.PageNumber + 26);
                }
            }
            else if (form == "hs_template")
            {
                
                footer = "LIC 503(3/99)(PERSONAL)";
            }
            else if (form == "csr_template") {

                footer = "LIC 508(3/11)REQUIRED FORM - NO CHANGE PERMITTED";
            }
            else if (form == "eaa_template")
            {
                if (document.PageNumber > 1)
                {
                    footer = " ";
                }
                else
                {
                    footer = "SOC 341A (3/03)";
                }
            }else if (form == "ans_template")
            {
                footer = "LIC 625 (6/12) CONFIDENTIAL";
            }
            else if (form == "rls_template")
            {
                footer = "LIC 9163 (3/11)";
            }
            else if (form == "per_r_template")
            {
                if (document.PageNumber > 1)
                {
                    footer = " ";
                }
                else
                {
                    footer = "LIC 501 (3/99) (OVER)";
                }
            }
            else if (form == "eev_template")
            {
                footer = "Form I-9  03/08/13 N";
            }
            else if (form == "ew_template")
            {
                if (document.PageNumber > 1)
                {
                    footer = "Form W-4 (2018) | Page " + Convert.ToString(document.PageNumber);
                }
                else
                {
                    footer = "For Privacy Act and Paperwork Reduction Act Notice, see page 4. | Cat. No. 10220Q | Form W-4 (2018)";
                }
            }
            else
            {
                footer = "";
            }

            PdfPCell footer1 = new PdfPCell(new Phrase(Convert.ToString(footer), fontH1));
            footer1.BorderWidth = 0;

            table.AddCell(footer1);
            
            table.WriteSelectedRows(0, -1, document.Bottom, document.Left, writer.DirectContent);
        }

        //write on close of document
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
        }
    }
}