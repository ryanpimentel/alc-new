﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using DataSoft.Helpers;
using Datasoft.Ext;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Datasoft.AssistedLiving.Web.Models;

namespace Datasoft.AssistedLiving.Web.Controllers {
    public class MARController : Controller
    {
        private string UserID {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        protected string RoleID {
            get { return User.Identity.Name.Split('|')[2]; }
        }
        // GET: MedicineAdministration
        public ActionResult Index()
        {
            using (var ALS = new ALSBaseDataContext()) {
                var res = (from x in ALS.ALCUsers
                           join y in ALS.CarestaffShifts on x.user_id equals y.carestaff_id into y1 from ys in y1.DefaultIfEmpty()
                           join z in ALS.Shifts on ys.shift_id equals z.shift_id into z1 from zs in z1.DefaultIfEmpty()
                           join a in ALS.ALCUserRoles on x.user_id equals a.user_id
                           join b in ALS.ALCRoles on a.role_id equals b.role_id 
                           where x.user_id == UserID
                           select new {
                               x.middle_initial, x.first_name, x.last_name,
                               start_time = Utility.ToTimezonedDateTime(zs.start_time, "hh:mm tt"),
                               end_time = Utility.ToTimezonedDateTime(zs.end_time, "hh:mm tt"),
                               role_description = b.description
                           }).SingleOrDefault();
                string mi = !string.IsNullOrEmpty(res.middle_initial) ? res.middle_initial + ". " : "";
                ViewBag.Name = res.first_name + " " + mi + res.last_name;
                ViewBag.StartShiftTime = res.start_time;
                ViewBag.EndShiftTime = res.end_time;
                ViewBag.RoleDescription = res.role_description;
            }
            
            return View();
        }
        public ActionResult LogError(string module, string message)
        {
            var log = new ErrorController();
            log.ControllerContext = ControllerContext;
            return log.LogError(module, message);
        }
        public dynamic GetData(string func, string param1 = null, string param2 = null) {
            try {
                if (func == AdminConstants.MEDICAL_ADMINISTRATION_SCHEDULE) {
                    using (
                        
                        
                        var ALS = new ALSBaseDataContext()) {
                        DateTime? dateAdministered = param1.TryParseNullableDate(false);
                        var res = ALS.getMARSchedule(dateAdministered).ToList();
                        var res2 = ALS.Residents.Where(x => x.isAdmitted == true)
                                .Select(f => new { RId = f.resident_id, f.isAdmitted }).ToList();

                        var res_list = (from r in res join r2 in res2
                                         on r.resident_id equals r2.RId
                                        where r2.isAdmitted == true
                                    select new
                                    {
                                       r.admission_id,
                                        r.resident_id,
                                        r.resident_name
                                    }).Distinct().ToList();

                        var marJA = new JArray();
                        var marItems = res_list.Select(x => new { x.admission_id, x.resident_id, x.resident_name }).Distinct().ToList();

                        if (marItems != null)
                            marJA = JArray.FromObject(marItems);

                        var medicines = new JArray();
                        foreach(var o in marJA) {
                            var m = res.Where(x => x.resident_id == o["resident_id"].Value<int?>())
                                .Select(y=> new { y.receive_detail_id, y.medicine , y.status }).Distinct().ToList();
                            if(m != null && m.Count > 0) {
                                medicines = JArray.FromObject(m);
                                foreach (var mm in medicines) {
                                    var mr = res.Where(q => q.receive_detail_id == mm["receive_detail_id"].Value<int>())
                                        .Select(f => new { time = f.dosage_time, f.dosage_schedule_id, f.intake_status, f.start_time, f.dosage }).Distinct().ToList();
                                    mm["dosage_times"] = JArray.FromObject(mr);
                                }
                                o["medicines"] = medicines;
                            }
                        }
                        return Content(marJA.ToString(), "application/json");
                    }
                }else if(func == AdminConstants.MEDICAL_ADMINISTRATION_SCHEDULE_INFO) {
                    using (var ALS = new ALSBaseDataContext()) {
                        int rdid = int.Parse(param1);
                        int dsid = int.Parse(param2);
                        var res = ALS.DailyMedications
                            .Where(x => x.receive_detail_id == rdid && x.dosage_schedule_id == dsid 
                                && (x.actual_date_administered.HasValue ? x.actual_date_administered == Utility.ToTimezonedDateTime(DateTime.Now) : false))
                            .Select(y => new { dmid = y.daily_med_id, status = y.is_given, y.comment })
                            .SingleOrDefault();
                        return Json(res);
                    }
                }
            } catch (Exception E) {                
                var ind = "<b>Func = " + func + "</b><br/>";
                LogError("MARController/GetData", ind + "Error message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("[]", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult PostData([DynamicJson(MatchName = false)] dynamic obj) {
            try
            {
                var result = JsonResponse.GetDefault();
                if (obj != null)
                {
                    if (obj.Func == AdminConstants.MEDICAL_ADMINISTRATION_SCHEDULE_INFO)
                    {
                        result = ParseMedInfo((PostMode)obj.Mode, obj.Data);
                    }
                }
                return Json(result);
            }
            catch (Exception E)
            {
                LogError("MARController/PostData", "Error message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }

        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior) {
            return new JsonDataContractResult {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        private JsonResponse ParseMedInfo(PostMode mode, [DynamicJson] dynamic obj) {
            try {
                double vs = 0;
                double ds = 0;
                double newVS = 0;
                double rm_vs = 0;
                var counter = 0;
                bool supply_isTab = true;
                bool receivemed_isTab = true;
                //double qtty = 0;
                var pt = "";
                DailyMedication cs = null;
              
                using (var ALSDB = new ALSBaseDataContext()) {
                    if (mode == PostMode.Add) {
                        cs = new DailyMedication();
                        cs.actual_date_administered = DataSoft.Helpers.Utility.ToTimezonedDateTime(DateTime.Now);
                        cs.administered_by = UserID;
                    } else if (mode == PostMode.Edit || mode == PostMode.Delete || mode == PostMode.Deactivate) {
                        int id = int.Parse(obj.Id);
                        cs = ALSDB.DailyMedications.SingleOrDefault(x => x.daily_med_id == id);
                    }

                    if (mode == PostMode.Add || mode == PostMode.Edit) {
                        cs.receive_detail_id = Convert.ToInt32(obj.rdid);
                        cs.dosage_schedule_id = Convert.ToInt32(obj.dsid);
                        cs.comment = obj.comment;
                        if (obj.No)
                            cs.is_given = false;
                        else
                            cs.is_given = true;

                        //if c.is_given, deduct from resident stash
                        if (cs.is_given == true)
                        {
                            var med  = (from rmd in ALSDB.ReceiveMedDetails
                                      join rmp in ALSDB.ReceiveMedPrescriptions on rmd.receive_prescription_id equals rmp.receive_prescription_id
                                      join csr in ALSDB.CSRDetails on rmp.resident_id equals csr.resident_id
                                      join sup in ALSDB.Supplies on csr.supply_id equals sup.supply_id
                                      where csr.resident_id == rmp.resident_id && csr.supply_id == rmd.supply_id && rmd.receive_detail_id == cs.receive_detail_id
                                      select new
                                      {
                                          med_dosage = rmd.med_dosage,
                                          volume_size = sup.volume_size,
                                          product_type = sup.product_type,
                                          remaining_vsize = csr.remaining_vsize,
                                          S_isTablet = sup.is_tablet,
                                          RMD_isTablet = rmd.is_tablet,
                                          med_unit = rmd.medicine_unit
                                          //quantity = csr.qty

                                      }).ToList();

                            var rs = (from rmd in ALSDB.ReceiveMedDetails
                                      join rmp in ALSDB.ReceiveMedPrescriptions on rmd.receive_prescription_id equals rmp.receive_prescription_id
                                      join csr in ALSDB.CSRDetails on rmp.resident_id equals csr.resident_id
                                      join sup in ALSDB.Supplies on csr.supply_id equals sup.supply_id
                                      where csr.resident_id == rmp.resident_id && csr.supply_id == rmd.supply_id && rmd.receive_detail_id == cs.receive_detail_id
                                      select new
                                      {
                                          csr,
                                          sup
                                          //csr_detail_id = csr.csr_detail_id,
                                          //csr_resident_id = csr.resident_id,
                                          //csr_supply_id = csr.supply_id,
                                          //csr_qty = csr.qty,
                                          //csr_date_filled = csr.date_filled,
                                          //csr_date_started = csr.date_started,
                                          //csr_total_qty = csr.total_qty,
                                          
                                      }).ToList();

                            foreach (var elem in med)
                            {
                                vs = elem.volume_size;
                                ds = elem.med_dosage;
                                pt = elem.product_type;
                                rm_vs = elem.remaining_vsize;
                                supply_isTab = elem.S_isTablet;
                                receivemed_isTab = elem.RMD_isTablet;
                               // qtty = Convert.ToDouble(elem.quantity);
                            }
                            double? csr_supCount = 0;
                            foreach (var item in rs)
                            {
                              
                                //check if product is supply or medicine
                                if(pt == "2") //if supply proceed here
                                {
                                    item.csr.qty -= 1;
                                    item.sup.supply_count -= 1;
                                    csr_supCount = item.csr.qty;
                                }

                                else //if medicine proceed here
                                {
                                    if(receivemed_isTab == true) //check if tablet
                                    {
                                        item.csr.qty -= ds;
                                        item.sup.supply_count -= Convert.ToInt32(ds);
                                        csr_supCount = item.csr.qty;
                                    }
                                    else //if solution
                                    {
                                        if (rm_vs > 0)
                                        {
                                            newVS = rm_vs;
                                            if (newVS < ds)
                                            {
                                                newVS = vs + rm_vs;
                                                counter = 1;
                                            }
                                        }
                                        else
                                        {
                                            newVS = vs;
                                        }


                                        if (newVS <= 0 || counter == 1)
                                        {
                                            item.csr.qty -= 1;
                                            item.sup.supply_count -= 1;
                                            csr_supCount = item.csr.qty;
                                        }

                                        item.csr.remaining_vsize = newVS;

                                        if (item.csr.qty <= 0)
                                        {
                                            item.csr.remaining_vsize = 0;
                                        }
                                    }
                                }

                                if(item.csr.date_started == null)
                                {
                                    item.csr.date_started = DateTime.Now;
                                }
 
                            }
                           if (csr_supCount > -1)
                           {
                                ALSDB.SubmitChanges();
                            }
                            else
                            {
                               // LogError("AdminController/ParseRoom", ind + "Error Message: " + E.Message);
                                return new JsonResponse(JsonResponseResult.Error, "negative");
                            }
                           
                        }

                    } else if (mode == PostMode.Delete) {
                        ALSDB.DailyMedications.DeleteOnSubmit(cs);
                    } else if (mode == PostMode.Deactivate) {
                        //ALSDB. no status field
                    }
                    if (mode == PostMode.Add) {
                        ALSDB.DailyMedications.InsertOnSubmit(cs);
                    }
                    ALSDB.SubmitChanges();
                }
            } catch (Exception E) {
                LogError("MARController/ParseMedInfo", "Error message: " + E.Message);
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();
        }
    }
}