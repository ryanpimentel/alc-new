﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DataSoft.Helpers;
using Datasoft.Ext;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Datasoft.AssistedLiving.Web.Models;
using System.IO;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.html;
using System.Drawing;

namespace Datasoft.AssistedLiving.Web.Controllers
{
   
    public class GuestNotificationController : Controller
    {

        private string AgencyID
        {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        private string UserID
        {
            get { return User.Identity.Name.Split('|')[0]; }
        }
        private string RoleID
        {
            get { return User.Identity.Name.Split('|')[2]; }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogError(string module, string message)
        {
            var log = new ErrorController();
            log.ControllerContext = ControllerContext;
            return log.LogError(module, message);
        }

        [HttpPost]
        public JsonResult ParseToken([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                var message = "";
                Int32 result = 0;
                var resident_id = "";
                string date_completion = "";

                using (var ALSDB = new ALSBaseDataContext())
                {
                    //var gn = new GuestNotification();
                    GuestNotification gn = null;
                    String token = Convert.ToString(obj.token);


                    var resident = (from x in ALSDB.GuestNotifications
                                    where x.token == token
                                    select new
                                    { id = x.resident_id, gid = x.id }).ToList();

                    gn = ALSDB.GuestNotifications.SingleOrDefault(x => x.id == resident[0].gid);

                    if (gn != null)
                    {

                        if (resident.Count > 0)
                        {

                            Resident r = null;
                            r = ALSDB.Residents.SingleOrDefault(x => x.resident_id == resident[0].id);

                            if (r != null)
                            {
                                if (r.responsible_person_email == UserID)
                                {
                                    byte[] data = Convert.FromBase64String(token);
                                    //DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));


                                    byte[] _time = data.Take(8).ToArray();
                                    byte[] _key = data.Skip(8).Take(16).ToArray();
                                    byte[] _completion = data.Skip(24).ToArray();


                                    DateTime when = DateTime.FromBinary(BitConverter.ToInt64(_time, 0));
                                    string datetime = when.ToString();

                                    DateTime completion = DateTime.FromBinary(BitConverter.ToInt64(_completion, 0));
                                    date_completion = completion.ToString();

                                    if (when < DateTime.UtcNow.AddHours(-24))
                                    {
                                        message = "Sorry, the link has expired.";
                                    }
                                    else
                                    {
                                        result = 1;
                                        resident_id = Convert.ToString(resident[0].id);
                                    }
                                }
                                else
                                {
                                    message = "Please log in using your own account.";
                                }
                            }
                            else
                            {
                                message = "Please contact the facility for assistance.";
                            }

                        }
                    }
                    var tokenData = new
                    {
                        message = message,
                        result = result,
                        resident_id = resident_id,
                        gid = gn.id,
                        completion = date_completion
                    };

                    return Json(tokenData);

                }
            }
            catch (Exception E)
            {
                LogError("GuestNotificationController/ParseToken", "Error Message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }


        }

        [HttpPost]
        public JsonResult HandleComment([DynamicJson(MatchName = false)] dynamic obj)
        {
            //int? ActivityScheduleId = ((string)obj.Id).TryParseNullableInt();

            //mode --> 0 - get comments , 1 - add comment
            
            using (var ALSDB = new ALSBaseDataContext())
            {
                var ind = "";
                var data_null = true;

                try
                {
                    int? resident_id = ((string)obj.resident_id).TryParseNullableInt();
                    int? guest_notification_id = ((string)obj.guest_notification_id).TryParseNullableInt();
                    string cid = obj.cid.ToString();
                    
                    //cid is to determine the certain task in a certain day in which the comment was added

                    GuestComment gc = null;
                    var data = (from g in ALSDB.GuestComments
                                where g.cid == cid
                                && g.resident_id == resident_id
                                select new
                                {
                                    g.id,
                                    g.comment
                                }).SingleOrDefault();

                    var role = (from g in ALSDB.ALCRoles
                                where g.role_id == Convert.ToInt32(RoleID)
                                select new
                                {
                                    g.description
                                }).SingleOrDefault();


                    if (data != null)
                    {
                        gc = ALSDB.GuestComments.SingleOrDefault(x => x.id == data.id);

                        if (obj.mode == 0)
                        {
                            if (role.description == "Guest")
                            {
                                gc.guest_read_status = "read";
                                ALSDB.SubmitChanges();
                            }
                            return Json(data);
                        }
                        else
                        {

                            var comment_object = new
                            {
                                comment = obj.comment.comment,
                                datetime = obj.comment.datetime,
                                role = role.description,
                                userid = UserID
                            };

                            string json_comment = JsonConvert.SerializeObject(comment_object);

                            //taskdetail is to be parsed on the admin side to determine the task that the comment was added
                            string json_task = JsonConvert.SerializeObject(obj.taskdetail);

                            
                            gc.comment = (gc.comment + "," + json_comment);                            
                            if (role.description == "Guest")
                            {
                                gc.read_status = "unread";
                            }
                            else
                            {
                                gc.guest_read_status = "unread";
                            }                            
                            gc.unread_count += 1;
                            if (json_task != "null")
                            {
                                gc.taskdetail = json_task;                               
                            }
                            else
                            {
                                gc.read_status = "read";
                                gc.unread_count = 0;
                            }

                        }
                    }
                    else
                    {
                        data_null = false;
                        if (obj.mode == 1)
                        {

                            gc = new GuestComment();

                            var comment_object = new
                            {
                                comment = obj.comment.comment,
                                datetime = obj.comment.datetime,
                                role = role.description,
                                userid = UserID
                            };

                            string json_comment = JsonConvert.SerializeObject(comment_object);

                            //taskdetail is to be parsed on the admin side to determine the task that the comment was added
                            string json_task = JsonConvert.SerializeObject(obj.taskdetail);

                            gc.comment = json_comment;
                            gc.resident_id = Convert.ToInt32(obj.resident_id);
                            gc.cid = Convert.ToString(obj.cid);
                            if (role.description == "Guest")
                            {
                                gc.read_status = "unread";
                            }
                            else
                            {
                                gc.guest_read_status = "unread";
                            }
                            gc.date_created = DateTime.Now;
                            gc.unread_count = 1;
                            if (json_task != "null")
                            {
                                gc.taskdetail = json_task;
                            }

                            //if (obj.guest_notification_id == "0")
                            //{
                            //    var guestData = (from g in ALSDB.GuestNotifications
                            //                     where g.resident_id == resident_id
                            //                     select new
                            //                     {
                            //                         g.id,
                            //                     }).SingleOrDefault();

                            //    gc.guest_notification_id = Convert.ToInt32(guestData.id);
                            //}
                            //else
                            //{
                            //    gc.guest_notification_id = Convert.ToInt32(obj.guest_notification_id);
                            //}


                            ALSDB.GuestComments.InsertOnSubmit(gc);

                        }
                        if (obj.mode == 0) {
                          //  return Json(gc);
                            return Json(new JsonResponse(JsonResponseResult.Error,"no data available"));
                        }

                    }


                    ALSDB.SubmitChanges();

                    Agency a = ALSDB.Agencies.SingleOrDefault(x => x.agency_name == AgencyID);
                    var agency_name = a.agency_name;
                    var agency_email = a.agency_email;

                    if (obj.mode == 1)
                    {
                        MailMessage mail = new MailMessage();

                        mail.To.Add(agency_email);
                        mail.From = new MailAddress("dslalc@datasoftlogic.com");
                        mail.Subject = "Comment for " + obj.resident_name + " of " + obj.room;
                        mail.Body = "A comment has been added for the task below: <br><br>";
                        mail.Body += obj.comment_email;
                        mail.Body += "<br><br>This is also reflected on the Daily Schedule module.<br><br>Thank you.";

                        mail.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                                                      //smtp.Host = "10.10.1.149"; //Or Your SMTP Server Address
                        smtp.Credentials = new System.Net.NetworkCredential
                             ("dslalc@datasoftlogic.com", "dslalciloilo17"); // ***use valid credentials***
                        smtp.Port = 587;
                        //smtp.Port = 25;

                        //Or your Smtp Email ID and Password
                        smtp.EnableSsl = true;
                        smtp.Send(mail);
                    }


                    return Json(gc);

                }
                catch (Exception E)
                {
                    ind = "<b>obj.mode == " + obj.mode + "</b><br>";
                    ind += "<b>(data != null) == " + data_null + "</b><br>";
                    ind += "Line Number: " + E.LineNumber() + "<br>";
                    LogError("GuestNotificationController/HandleComment", ind + "Error Message: " + E.Message);

                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
                }
            }
        }

        public dynamic ChangePassword([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                ALCUser cs = null;

                using (var ALSDB = new ALSBaseDataContext())
                {
                    string curpass = obj.CurrentPassword;
                    string pass = obj.Password;
                    string cpass = obj.ConfirmPassword;

                    cs = ALSDB.ALCUsers.SingleOrDefault(x => x.user_id == UserID);

                    if (cs.password == curpass)
                    {
                        cs.password = pass;
                    }

                    ALSDB.SubmitChanges();

                    return Json(cs);

                }
            }
            catch (Exception E)
            {

                LogError("GuestNotificationController/ChangePassword", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }
        public dynamic GetUserData([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                ALCUser cs = null;

                using (var ALSDB = new ALSBaseDataContext())
                {
                  
                    cs = ALSDB.ALCUsers.SingleOrDefault(x => x.user_id == UserID);
                    
                    return Json(cs);

                }
            }
            catch (Exception E)
            {

                LogError("GuestNotificationController/GetUserData", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }
        public dynamic GetGridData(string func, string param, string param2, string param3, string param4)
        {
            var ind = "";
            try
            {
                if (func == AdminConstants.ACTIVITY_SCHEDULE)
                {
                    // ind = "<b>func == AdminConstants.ACTIVITY_SCHEDULE</b><br/>";
                    using (var ALS = new ALSBaseDataContext())
                    {
                        DateTime? fromDate = param.TryParseNullableDate(false);
                        int? p3 = !string.IsNullOrEmpty(param3) ? (int?)int.Parse(param3) : null;
                        int? p4 = !string.IsNullOrEmpty(param4) ? (int?)int.Parse(param4) : null;

                        var cs = (from c in ALS.getActivityScheduleByUser("", 1, fromDate, null, p3, p4)
                                  select new
                                  {
                                      c.activity_desc,
                                      c.activity_id,
                                      c.activity_proc,
                                      c.activity_schedule_id,
                                      c.activity_status,
                                      c.oactivity_status,
                                      c.department,
                                      c.carestaff_activity_id,
                                      c.completion_date,
                                      c.completion_type,
                                      c.remarks,
                                      //reschedule_dt = Utility.GetDateFormattedString(c.reschedule_dt, "MM/dd/yyyy hh:mm tt"),
                                      reschedule_dt = c.reschedule_dt,
                                      c.carestaff_name,
                                      c.resident,
                                      resident2 = c.resident,
                                      carestaff2 = c.carestaff_name,
                                      activitydesc2 = c.activity_desc,
                                      c.resident_id,
                                      c.room,
                                      c.service_duration,
                                      c.acknowledged_by,
                                      start_time = Utility.GetDateFormattedString(c.start_time, "hh:mm tt"),
                                      c.xref,
                                      actual_completion_date = Utility.GetDateFormattedString(c.actual_completion_date, "hh:mm tt"),
                                      timelapsed = Utility.IsTimeLapseFromCurrentTime(c.start_time, param.TryParseNullableDate(false)),
                                      start = Utility.GetDateFormattedString(c.start_time, "HH:mm"),
                                      c.is_active,
                                      active_until = (c.active_until != null ? Utility.GetDateFormattedString(c.active_until, "MM/dd/yyyy hh:mm tt") : null)
                                  }).OrderBy(x => x.start).ToList();
                        return Json(cs, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (func == AdminConstants.INCIDENT_REPORT)
                {
                    //ind = "<b>func == AdminConstants.INCIDENT_REPORT</b><br/>";
                    using (var ALS = new ALSBaseDataContext())
                    {
                        var uirid = param.TryParseNullableInt();
                        var resid = param2.TryParseNullableInt();
                        var m = param3.TryParseNullableInt();
                        if (m == 1)
                        {
                            var uir = (from c in ALS.UnusualIncidentReports
                                       where c.UIR_id == uirid
                                       select new
                                       {
                                           UIR_id = c.UIR_id,
                                           resident_id = c.resident_id,
                                           UIRNameOfFacility = c.UIRNameOfFacility,
                                           UIRFacilityFileNumber = c.UIRFacilityFileNumber,
                                           UIRTelephoneNumber = c.UIRTelephoneNumber,
                                           UIRAddress = c.UIRAddress,
                                           UIRCityStateZip = c.UIRCityStateZip,
                                           UIRResponsiblePersonCC = c.UIRResponsiblePersonCC,
                                           UIRResidentInvolved1 = c.UIRResidentInvolved1,
                                           UIRResidentInvolved2 = c.UIRResidentInvolved2,
                                           UIRResidentInvolved3 = c.UIRResidentInvolved3,
                                           UIRDateOccured1 = Utility.GetDateFormattedString(c.UIRDateOccured1, "MM/dd/yyyy"),
                                           UIRDateOccured2 = Utility.GetDateFormattedString(c.UIRDateOccured2, "MM/dd/yyyy"),
                                           UIRDateOccured3 = Utility.GetDateFormattedString(c.UIRDateOccured3, "MM/dd/yyyy"),
                                           UIRAge1 = c.UIRAge1,
                                           UIRSex1 = c.UIRSex1,
                                           UIRAge2 = c.UIRAge2,
                                           UIRSex2 = c.UIRSex2,
                                           UIRAge3 = c.UIRAge3,
                                           UIRSex3 = c.UIRSex3,
                                           UIRDateAdmission1 = Utility.GetDateFormattedString(c.UIRDateAdmission1, "MM/dd/yyyy"),
                                           UIRDateAdmission2 = Utility.GetDateFormattedString(c.UIRDateAdmission2, "MM/dd/yyyy"),
                                           UIRDateAdmission3 = Utility.GetDateFormattedString(c.UIRDateAdmission3, "MM/dd/yyyy"),
                                           UIRUnathorizedAbsence = Convert.ToBoolean(c.UIRUnathorizedAbsence),
                                           UIRRape = Convert.ToBoolean(c.UIRRape),
                                           UIRInjuryAccident = Convert.ToBoolean(c.UIRInjuryAccident),
                                           UIRMedicalEmergency = Convert.ToBoolean(c.UIRMedicalEmergency),
                                           UIRAggressiveActSelf = Convert.ToBoolean(c.UIRAggressiveActSelf),
                                           UIRSexual = Convert.ToBoolean(c.UIRSexual),
                                           UIRPregnancy = Convert.ToBoolean(c.UIRPregnancy),
                                           UIRInjuryUnknownOrigin = Convert.ToBoolean(c.UIRInjuryUnknownOrigin),
                                           UIROtherSexualIncident = Convert.ToBoolean(c.UIROtherSexualIncident),
                                           UIRAggressiveActAnotherClient = Convert.ToBoolean(c.UIRAggressiveActAnotherClient),
                                           UIRPhysical = Convert.ToBoolean(c.UIRPhysical),
                                           UIRSuicideAttempt = Convert.ToBoolean(c.UIRSuicideAttempt),
                                           UIRInjuryFromAnotherClient = Convert.ToBoolean(c.UIRInjuryFromAnotherClient),
                                           UIRTheft = Convert.ToBoolean(c.UIRTheft),
                                           UIRAggressiveActStaff = Convert.ToBoolean(c.UIRAggressiveActStaff),
                                           UIRPsychological = Convert.ToBoolean(c.UIRPsychological),
                                           UIRS_Other = Convert.ToBoolean(c.UIRS_Other),
                                           UIRInjuryFromBehaviorEpisode = Convert.ToBoolean(c.UIRInjuryFromBehaviorEpisode),
                                           UIRFire = Convert.ToBoolean(c.UIRFire),
                                           UIRAggressiveActFamily = Convert.ToBoolean(c.UIRAggressiveActFamily),
                                           UIRFinancial = Convert.ToBoolean(c.UIRFinancial),
                                           UIRS_OtherComment = c.UIRS_OtherComment,
                                           UIREpidemicOutbreak = Convert.ToBoolean(c.UIREpidemicOutbreak),
                                           UIRPropertyDamage = Convert.ToBoolean(c.UIRPropertyDamage),
                                           UIRAllegedViolationOfRights = Convert.ToBoolean(c.UIRAllegedViolationOfRights),
                                           UIRNeglect = Convert.ToBoolean(c.UIRNeglect),
                                           UIRHospitalization = Convert.ToBoolean(c.UIRHospitalization),
                                           UIROther = Convert.ToBoolean(c.UIROther),
                                           UIR_OtherComment = c.UIR_OtherComment,
                                           UIRIncidentDescription = c.UIRIncidentDescription,
                                           UIRWitnessOfIncident = c.UIRWitnessOfIncident,
                                           UIRImmediateAction = c.UIRImmediateAction,
                                           UIRMedTreatmentYes = Convert.ToBoolean(c.UIRMedTreatmentYes),
                                           UIRMedTreatmentNo = Convert.ToBoolean(c.UIRMedTreatmentNo),
                                           UIRNatureOfTreatment = c.UIRNatureOfTreatment,
                                           UIRAdministeredWhere = c.UIRAdministeredWhere,
                                           UIRAdministeredBy = c.UIRAdministeredBy,
                                           UIRFollowUpTreatment = c.UIRFollowUpTreatment,
                                           UIRActionTaken = c.UIRActionTaken,
                                           UIRSupervisorComments = c.UIRSupervisorComments,
                                           UIRAttendingPhysician = c.UIRAttendingPhysician,
                                           UIRNameTitleOfSubmittee = c.UIRNameTitleOfSubmittee,
                                           UIRDateOfSubmission = Utility.GetDateFormattedString(c.UIRDateOfSubmission, "MM/dd/yyyy"),
                                           UIRNameTitleOfReviewee = c.UIRNameTitleOfReviewee,
                                           UIRDateReviewed = Utility.GetDateFormattedString(c.UIRDateReviewed, "MM/dd/yyyy"),
                                           UIRLicensing = Convert.ToBoolean(c.UIRLicensing),
                                           UIRLicensingComment = c.UIRLicensingComment,
                                           UIRProtectiveServices = Convert.ToBoolean(c.UIRProtectiveServices),
                                           UIRProtectiveServicesComment = c.UIRProtectiveServicesComment,
                                           UIRLongTermCareOmbudsman = Convert.ToBoolean(c.UIRLongTermCareOmbudsman),
                                           UIRLongTermCareOmbudsmanComment = c.UIRLongTermCareOmbudsmanComment,
                                           UIRParentGuardian = Convert.ToBoolean(c.UIRParentGuardian),
                                           UIRParentGuardianComment = c.UIRParentGuardianComment,
                                           UIRLawEnforcement = Convert.ToBoolean(c.UIRLawEnforcement),
                                           UIRLawEnforcementComment = c.UIRLawEnforcementComment,
                                           UIRPlacementAgency = Convert.ToBoolean(c.UIRPlacementAgency),
                                           UIRPlacementAgencyComment = c.UIRPlacementAgencyComment,
                                           UIRDateViewed = c.UIRDateViewed
                                       }).ToList();
                            return Json(uir, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var uir = (from c in ALS.UnusualIncidentReports
                                       where c.resident_id == resid
                                       select new
                                       {
                                           UIR_id = c.UIR_id,
                                           resident_id = c.resident_id,
                                           UIRNameOfFacility = c.UIRNameOfFacility,
                                           UIRFacilityFileNumber = c.UIRFacilityFileNumber,
                                           UIRTelephoneNumber = c.UIRTelephoneNumber,
                                           UIRAddress = c.UIRAddress,
                                           UIRCityStateZip = c.UIRCityStateZip,
                                           UIRResponsiblePersonCC = c.UIRResponsiblePersonCC,
                                           UIRResidentInvolved1 = c.UIRResidentInvolved1,
                                           UIRResidentInvolved2 = c.UIRResidentInvolved2,
                                           UIRResidentInvolved3 = c.UIRResidentInvolved3,
                                           UIRDateOccured1 = Utility.GetDateFormattedString(c.UIRDateOccured1, "MM/dd/yyyy"),
                                           UIRDateOccured2 = Utility.GetDateFormattedString(c.UIRDateOccured2, "MM/dd/yyyy"),
                                           UIRDateOccured3 = Utility.GetDateFormattedString(c.UIRDateOccured3, "MM/dd/yyyy"),
                                           UIRAge1 = c.UIRAge1,
                                           UIRSex1 = c.UIRSex1,
                                           UIRAge2 = c.UIRAge2,
                                           UIRSex2 = c.UIRSex2,
                                           UIRAge3 = c.UIRAge3,
                                           UIRSex3 = c.UIRSex3,
                                           UIRDateAdmission1 = Utility.GetDateFormattedString(c.UIRDateAdmission1, "MM/dd/yyyy"),
                                           UIRDateAdmission2 = Utility.GetDateFormattedString(c.UIRDateAdmission2, "MM/dd/yyyy"),
                                           UIRDateAdmission3 = Utility.GetDateFormattedString(c.UIRDateAdmission3, "MM/dd/yyyy"),
                                           UIRUnathorizedAbsence = Convert.ToBoolean(c.UIRUnathorizedAbsence),
                                           UIRRape = Convert.ToBoolean(c.UIRRape),
                                           UIRInjuryAccident = Convert.ToBoolean(c.UIRInjuryAccident),
                                           UIRMedicalEmergency = Convert.ToBoolean(c.UIRMedicalEmergency),
                                           UIRAggressiveActSelf = Convert.ToBoolean(c.UIRAggressiveActSelf),
                                           UIRSexual = Convert.ToBoolean(c.UIRSexual),
                                           UIRPregnancy = Convert.ToBoolean(c.UIRPregnancy),
                                           UIRInjuryUnknownOrigin = Convert.ToBoolean(c.UIRInjuryUnknownOrigin),
                                           UIROtherSexualIncident = Convert.ToBoolean(c.UIROtherSexualIncident),
                                           UIRAggressiveActAnotherClient = Convert.ToBoolean(c.UIRAggressiveActAnotherClient),
                                           UIRPhysical = Convert.ToBoolean(c.UIRPhysical),
                                           UIRSuicideAttempt = Convert.ToBoolean(c.UIRSuicideAttempt),
                                           UIRInjuryFromAnotherClient = Convert.ToBoolean(c.UIRInjuryFromAnotherClient),
                                           UIRTheft = Convert.ToBoolean(c.UIRTheft),
                                           UIRAggressiveActStaff = Convert.ToBoolean(c.UIRAggressiveActStaff),
                                           UIRPsychological = Convert.ToBoolean(c.UIRPsychological),
                                           UIRS_Other = Convert.ToBoolean(c.UIRS_Other),
                                           UIRInjuryFromBehaviorEpisode = Convert.ToBoolean(c.UIRInjuryFromBehaviorEpisode),
                                           UIRFire = Convert.ToBoolean(c.UIRFire),
                                           UIRAggressiveActFamily = Convert.ToBoolean(c.UIRAggressiveActFamily),
                                           UIRFinancial = Convert.ToBoolean(c.UIRFinancial),
                                           UIRS_OtherComment = c.UIRS_OtherComment,
                                           UIREpidemicOutbreak = Convert.ToBoolean(c.UIREpidemicOutbreak),
                                           UIRPropertyDamage = Convert.ToBoolean(c.UIRPropertyDamage),
                                           UIRAllegedViolationOfRights = Convert.ToBoolean(c.UIRAllegedViolationOfRights),
                                           UIRNeglect = Convert.ToBoolean(c.UIRNeglect),
                                           UIRHospitalization = Convert.ToBoolean(c.UIRHospitalization),
                                           UIROther = Convert.ToBoolean(c.UIROther),
                                           UIR_OtherComment = c.UIR_OtherComment,
                                           UIRIncidentDescription = c.UIRIncidentDescription,
                                           UIRWitnessOfIncident = c.UIRWitnessOfIncident,
                                           UIRImmediateAction = c.UIRImmediateAction,
                                           UIRMedTreatmentYes = Convert.ToBoolean(c.UIRMedTreatmentYes),
                                           UIRMedTreatmentNo = Convert.ToBoolean(c.UIRMedTreatmentNo),
                                           UIRNatureOfTreatment = c.UIRNatureOfTreatment,
                                           UIRAdministeredWhere = c.UIRAdministeredWhere,
                                           UIRAdministeredBy = c.UIRAdministeredBy,
                                           UIRFollowUpTreatment = c.UIRFollowUpTreatment,
                                           UIRActionTaken = c.UIRActionTaken,
                                           UIRSupervisorComments = c.UIRSupervisorComments,
                                           UIRAttendingPhysician = c.UIRAttendingPhysician,
                                           UIRNameTitleOfSubmittee = c.UIRNameTitleOfSubmittee,
                                           UIRDateOfSubmission = Utility.GetDateFormattedString(c.UIRDateOfSubmission, "MM/dd/yyyy"),
                                           UIRNameTitleOfReviewee = c.UIRNameTitleOfReviewee,
                                           UIRDateReviewed = Utility.GetDateFormattedString(c.UIRDateReviewed, "MM/dd/yyyy"),
                                           UIRLicensing = Convert.ToBoolean(c.UIRLicensing),
                                           UIRLicensingComment = c.UIRLicensingComment,
                                           UIRProtectiveServices = Convert.ToBoolean(c.UIRProtectiveServices),
                                           UIRProtectiveServicesComment = c.UIRProtectiveServicesComment,
                                           UIRLongTermCareOmbudsman = Convert.ToBoolean(c.UIRLongTermCareOmbudsman),
                                           UIRLongTermCareOmbudsmanComment = c.UIRLongTermCareOmbudsmanComment,
                                           UIRParentGuardian = Convert.ToBoolean(c.UIRParentGuardian),
                                           UIRParentGuardianComment = c.UIRParentGuardianComment,
                                           UIRLawEnforcement = Convert.ToBoolean(c.UIRLawEnforcement),
                                           UIRLawEnforcementComment = c.UIRLawEnforcementComment,
                                           UIRPlacementAgency = Convert.ToBoolean(c.UIRPlacementAgency),
                                           UIRPlacementAgencyComment = c.UIRPlacementAgencyComment,
                                           UIRDateViewed = c.UIRDateViewed
                                       }).ToList().OrderByDescending(e => e.UIRDateOccured1);
                            return Json(uir, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else if (func == AdminConstants.RESIDENT)
                {
                    Resident r = null;
                    using (var ALSDB = new ALSBaseDataContext())
                    {
                        var resid = param.TryParseNullableInt();
                        r = ALSDB.Residents.SingleOrDefault(x => x.resident_id == resid);

                        var objres = new
                        {
                            Name = r.responsible_person,
                            Address = r.responsible_person_address,
                            TelNumber = r.responsible_person_telephone
                        };

                        return Json(objres);

                    }

                }
                else if (func == AdminConstants.GUEST_COMMENT)
                {
                    //GuestComment gc = null;
                    using (var ALSDB = new ALSBaseDataContext())
                    {
                       var resid = param.TryParseNullableInt();
                        //// r = ALSDB.Residents.SingleOrDefault(x => x.resident_id == resid);
                        var gc = (from g in ALSDB.GuestComments
                                  where g.resident_id == resid
                                  select new
                                  {
                                      g.cid,
                                      g.guest_read_status,
                                      g.resident_id
                                  }).ToList();
                        return Json(gc, JsonRequestBehavior.AllowGet);
                    }

                }


            }
            catch (Exception E)
            {
                ind = "<b>func == " + func + "</b><br/>";
                ind += "Line Number: " + E.LineNumber() + "<br>";
                LogError("GuestNotificationController/GetGridData", ind + "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("[]", JsonRequestBehavior.AllowGet);
        }

        public dynamic GetResidents([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                //Resident r = null;
                
                using (var ALSDB = new ALSBaseDataContext())
                {

                    var data = (from res in ALSDB.Residents
                                join adm in ALSDB.Admissions on res.resident_id equals adm.resident_id into da
                                from dd in da.DefaultIfEmpty()
                                join r in ALSDB.Rooms on dd.room_id equals r.room_id into ra
                                from rr in ra.DefaultIfEmpty()
                                where res.responsible_person_email == UserID && res.isAdmitted == true
                                select new
                                {
                                    ResidentId = res.resident_id,
                                    Name = res.first_name + " " + res.last_name,
                                    RoomName = rr.room_name
                                }).ToArray();

                    var with_comment = (from gc in ALSDB.GuestComments
                                        where gc.guest_read_status == "unread"
                                        select new
                                        {
                                            resident_id = gc.resident_id
                                        }).ToArray();
                    var resident = new
                    {
                        resident_data = data,
                        with_comment = with_comment
                    };


                    return Json(resident);

                }
            }
            catch (Exception E)
            {

                LogError("GuestNotificationController/GetResidents", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }
        
       
        public JsonResult GetCommentCount()
        {

            using (var ALSDB = new ALSBaseDataContext())
            {
                try
                {

                    var comment_count = ALSDB.GuestComments.Count();

                    return Json(comment_count);

                }
                catch (Exception E)
                {

                    LogError("GuestNotificationController/GetCommentCount", "Error Message: " + E.Message);

                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
                }

            }
        }

        public JsonResult GetUnreadCount()
        {

            using (var ALSDB = new ALSBaseDataContext())
            {
                try
                {
                    var comment_count = ALSDB.GuestComments.Sum(x => x.unread_count);
                    var gc = (from g in ALSDB.GuestComments
                              where g.read_status == "unread"
                              select new
                              {
                                  g
                              }).ToArray();
                

                    var res = new
                    {
                        comment_count,
                        gc
                    };

                    return Json(res);

                }
                catch (Exception E)
                {

                    LogError("GuestNotificationController/GetUnreadCount", "Error Message: " + E.Message);

                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpPost]
        public JsonResult GetCommentUpdate([DynamicJson(MatchName = false)] dynamic obj)
        {

            using (var ALSDB = new ALSBaseDataContext())
            {
                try
                {
                    int skip = Convert.ToInt32(obj.skip);
                    int take = Convert.ToInt32(obj.take);

                    //var result = ALSDB.GuestComments.Skip(skip).Take(take).ToList();

                    var result = (from x in ALSDB.GuestComments
                                  join y in ALSDB.Residents on x.resident_id equals y.resident_id
                                  select new { x, y }).Skip(skip).Take(take).ToList();

                    return Json(result);

                }
                catch (Exception E)
                {

                    LogError("GuestNotificationController/GetCommentUpdate", "Error Message: " + E.Message);

                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
                }

            }
        }

        public JsonResult AddDateViewed([DynamicJson(MatchName = false)] dynamic obj)
        {
            using (var ALSDB = new ALSBaseDataContext())
            {
                try
                {
                    String token = Convert.ToString(obj.uir_id);
                    int r_id = Convert.ToInt32(token);
                    UnusualIncidentReport uir = new UnusualIncidentReport();

                    uir = ALSDB.UnusualIncidentReports.SingleOrDefault(x => x.UIR_id == r_id);

                    if (uir.UIRDateViewed == null)
                    {
                        uir.UIRDateViewed = DateTime.Now;
                    }

                    ALSDB.SubmitChanges();

                    return Json(uir, JsonRequestBehavior.AllowGet);

                }
                catch (Exception E)
                {
                    LogError("GuestNotificationController/AddDateViewed", "Error Message: " + E.Message);
                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
                }
            }
        }
        #region ViewIncidentReport
        public ActionResult ViewIncidentReport(string id)
        {
            try
            {
                int _id = 0;
                if (int.TryParse(id, out _id))
                {
                    var ALS = new ALSBaseDataContext();
                    UnusualIncidentReport uir = ALS.UnusualIncidentReports.SingleOrDefault(x => x.UIR_id == _id);
                    Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == uir.resident_id);
                    Admission ad = ALS.Admissions.SingleOrDefault(x => x.resident_id == r.resident_id);

                    Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyID);
                    AgencyList agency = null;
                    using (var ALSM = new ALSMasterBaseDataContext())
                    {
                        agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyID);
                        if (agency == null)
                            agency = new AgencyList();
                    };

                    if (uir == null) uir = new UnusualIncidentReport();

                    Dictionary<string, string> dcval = new Dictionary<string, string>();

                    foreach (var prop in uir.GetType().GetProperties())
                    {
                        string actualval = "";
                        var val = prop.GetValue(uir, null);
                        if (val != null)
                        {
                            DateTime t = new DateTime();
                            actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                        }
                        dcval[prop.Name] = actualval;//replace 'ANS'
                    }
                    var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                    var output = new MemoryStream();
                    var writer = PdfWriter.GetInstance(document, output);
                    writer.PageEvent = new PDFFooter() { form = "uir_template" };
                    document.Open();
                    string contents = System.IO.File.ReadAllText(Server.MapPath("~/Content/HTMTemplate/uir_template.htm"), Encoding.Unicode);
                    foreach (KeyValuePair<string, string> eve in dcval)
                    {
                        if (eve.Value.ToLower() == "true")
                            contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                        else if (eve.Value.ToLower() == "false")
                            contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                        else
                            contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                    }
                    contents = contents.Replace("[FirstName]", r.first_name);
                    string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                    contents = contents.Replace("[MiddleInitial]", mi);
                    contents = contents.Replace("[LastName]", r.last_name);

                    contents = contents.Replace("[FacilityName]", agency.agency_name);
                    contents = contents.Replace("[FacilityAddress]", a.address_1);
                    contents = contents.Replace("[FacilityCity]", a.city);
                    contents = contents.Replace("[FacilityState]", a.state);
                    //contents = contents.Replace("[FacilityZip]", a.zip);

                    FontFactory.Register(Server.MapPath("~/fonts/ARIALUNI.TTF"));

                    StyleSheet ST = new StyleSheet();
                    ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                    ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                    var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                    foreach (var htmlElement in parsedHtmlElements)
                        document.Add(htmlElement as IElement);
                    //======ESIGN BY ARJUN V
                    //if (uir.isSigned == true)
                    //{
                    //    byte[] imgdata = per.ESignature.ToArray();
                    //    //document.Add(new Paragraph("\n\n\n"));
                    //    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    //    jpg.ScaleToFit(140f, 120f);
                    //    jpg.SetAbsolutePosition(60, 270);
                    //    //jpg.Alignment = Element.ALIGN_LEFT;
                    //    //jpg.Border = PdfPCell.BOTTOM_BORDER;
                    //    //jpg.BorderWidth = 1f;
                    //    document.Add(jpg);
                    //    //Paragraph c1 = new Paragraph(per.SignedBy);
                    //    //c1.IndentationLeft = 30f;
                    //    //document.Add(c1);
                    //}
                    //======
                    document.Close();
                    //Response.ContentType = "application/pdf";
                    //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                    //Response.BinaryWrite(output.ToArray());
                    var cd = new System.Net.Mime.ContentDisposition
                    {
                        FileName = "UnusualIncidentReport.pdf",
                        Inline = true
                    };

                    Response.AppendHeader("Content-Disposition", cd.ToString());
                    return File(output.ToArray(), "application/pdf");
                }

                return Json("");
            }
            catch (Exception E)
            {
                LogError("GuestNotificationController/ViewIncidentReport", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));

                throw E;
            }
            #endregion

        }
        #region GetDateOccured
        public ActionResult GetDateOccured(string id)
        {
            using (var ALSDB = new ALSBaseDataContext())
            {
                try
                {
                    int _id = 0;
                    if (int.TryParse(id, out _id))
                    {
                        Admission ad = ALSDB.Admissions.SingleOrDefault(x => x.admission_id == _id);
                        Resident r = ALSDB.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);

                        var uirDateOccured = (from c in ALSDB.UnusualIncidentReports
                                              where c.resident_id == r.resident_id
                                              select new
                                              {
                                                  UIR_id = c.UIR_id,
                                                  resident_id = c.resident_id,
                                                  UIRDateOccured1 = Utility.GetDateFormattedString(c.UIRDateOccured1, "MM/dd/yyyy"),
                                                  dt_year = Utility.GetDateFormattedString(c.UIRDateOccured1, "yyyy"),
                                                  dt_month = Utility.GetDateFormattedString(c.UIRDateOccured1, "MM"),
                                                  dt_day = Utility.GetDateFormattedString(c.UIRDateOccured1, "dd")
                                              }).ToList().OrderByDescending(e => e.dt_year).ThenBy(e => e.dt_month).ThenBy(e => e.dt_day);
                        return Json(uirDateOccured, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception E)
                {

                    LogError("GuestNotificationController/GetDateOccured", "Error Message: " + E.Message);

                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
                }
            }
            return Json("[]", JsonRequestBehavior.AllowGet);

        }
        #endregion
        #region ChangeContactInformation
        public dynamic ChangeContactInformation([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
               // ALCUser cs = null;

               Resident residents = null;
                using (var ALSDB = new ALSBaseDataContext())
                {
                    string TelNumber = obj.TelNumber;
                    string Name = obj.Name;
                    string Address = obj.Address;


                    // cs = ALSDB.ALCUsers.SingleOrDefault(x => x.user_id == UserID);

                    // res = ALSDB.Residents.FirstOrDefault(x => x.responsible_person_email == UserID);

                    var res = from r in ALSDB.Residents
                                where r.responsible_person_email == UserID
                                select new
                                {
                                    resident_id = r.resident_id,
                                    responsible_person = r.responsible_person,
                                    responsible_person_address = r.responsible_person_address,
                                    responsible_person_telephone = r.responsible_person_telephone,
                                };

                    var list = new List<Resident>();
                    foreach ( var c in res) {
                        list.Add(new Resident
                        {
                            resident_id = c.resident_id,
                            responsible_person = c.responsible_person,
                            responsible_person_address = c.responsible_person_address,
                            responsible_person_telephone = c.responsible_person_telephone,
                        });

                    };

                    foreach(var a in list)
                    {
                       var resident_id = Convert.ToInt64(a.resident_id);
                        residents = ALSDB.Residents.SingleOrDefault (x => x.resident_id == resident_id);

                        if (Name != "")
                        {
                            residents.responsible_person = Name;
                        }

                        if (Address != "")
                        {
                            residents.responsible_person_address = Address;
                        }

                        if (TelNumber != "")
                        {
                            residents.responsible_person_telephone = TelNumber;
                        }
                        ALSDB.SubmitChanges();
                    }
                    return Json(residents);
                   // return JsonResponse.GetDefault();

                }
            }
            catch (Exception E)
            {

                LogError("GuestNotificationController/ChangeContactInformation", "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
          
        }
        #endregion
    }
}
