﻿using Datasoft.AssistedLiving.Web.Models;
using Datasoft.Ext;
using DataSoft.Helpers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class ReferralController : Controller
    {
        enum REFERRAL_TYPE
        {
            Admitted_As_Resident = 1,
            New_Leads = 2,
            New_Leads_Followup = 3,
            Tour_Schedule = 4,
            Tour_Schedule_Followup = 5,
            Assessment_Completion = 6,
            Assessment_Followup = 7,
            Move_In_Schedule = 8,
            Move_In_Followup = 9
        }
        private string UserID
        {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID
        {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        private string RoleID
        {
            get { return User.Identity.Name.Split('|')[2]; }
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Assisted Living Centre - Referral Form";
            return View();
        }
        public ActionResult LogError(string module, string message)
        {
            var log = new ErrorController();
            log.ControllerContext = ControllerContext;
            return log.LogError(module, message);
        }
        private string WriteWidgetRoll(string lname, string fname, string mi, DateTime? date)
        {
            string name = "";
            if (!string.IsNullOrEmpty(lname))
            {
                name = lname + ", ";
            }
            if (!string.IsNullOrEmpty(fname))
            {
                name += fname;
            }
            if (!string.IsNullOrEmpty(mi))
            {
                name += " " + mi + ".";
            }
            if (date != null)
                return name + " (" + date.Value.ToShortDateString() + ")";
            else
                return name;
        }

        public dynamic GetFormData([DynamicJson(MatchName = false)] dynamic obj)
        {

            try
            {
                int id = -1;
                string strId = "";

                if (!int.TryParse(obj.Id, out id))
                    strId = obj.Id;

                using (var ALS = new ALSBaseDataContext())
                {
                    if (obj.Func == "getform")
                    {
                        #region getform

                        var c = (from a in ALS.Referrals
                                 join b in ALS.Residents on a.resident_id equals b.resident_id
                                 where a.referral_id == id
                                 select new
                                 {
                                     b.resident_id,
                                     a.referral_id,
                                     b.first_name,
                                     b.last_name,
                                     b.middle_initial,
                                     b.responsible_person,
                                     b.responsible_person_telephone,
                                     b.responsible_person_email,
                                     b.age,
                                     b.SSN,
                                     b.address_prior_admission,
                                     b.gender,
                                     b.nearest_relative_relationship,
                                     b.telephone,
                                     b.otherinfo_comments,
                                     a.ref_source,
                                     a.budget_prefered_room,
                                     a.notes,
                                     a.spec_need,
                                     a.planned_tour_date,
                                     a.toured_date,
                                     a.assessment_start_date,
                                     a.assessment_completed_date,
                                     a.movein_date,
                                     a.is_lives_with_family,
                                     a.is_own_home,
                                     a.is_RCFE,
                                     a.is_rehab,
                                     a.is_alert_oriented,
                                     a.is_ambulatory,
                                     a.need_bathing_assist,
                                     a.is_cane_walker,
                                     a.is_forgetful,
                                     a.need_incontinent_care,
                                     a.need_laundry,
                                     a.need_med_mgnt,
                                     a.is_non_ambulatory,
                                     a.is_o2,
                                     a.need_room_tray_service,
                                     a.is_spec_diet,
                                     a.is_wheelchair,
                                     b.birth_date,
                                     a.create_date
                                 }).SingleOrDefault();
                        if (c != null)
                        {

                            var o = new
                            {

                                ResidentId = c.resident_id,
                                ReferralId = c.referral_id,
                                Firstname = c.first_name,
                                Lastname = c.last_name,
                                txtSSN = c.SSN,
                                txtaddress = c.address_prior_admission,
                                genderf = c.gender == "F" ? true : false,
                                genderm = c.gender == "M" ? true : false,
                                MiddleInitial = c.middle_initial,
                                ContactPerson = c.responsible_person,
                                ContactPersonPhone = c.responsible_person_telephone,
                                Email = c.responsible_person_email,
                                Rbirthday = Utility.GetDateFormattedString(c.birth_date, "MM/dd/yyyy"),
                                Age = c.age,
                                Relationship = c.nearest_relative_relationship,
                                ResidentPhone = c.telephone,
                                Remarks = c.otherinfo_comments,
                                RefSource = c.ref_source,
                                BudgetRoom = c.budget_prefered_room,
                                Notes = c.notes,
                                SpecNeed = c.spec_need,
                                PlannedTourDate = c.planned_tour_date.HasValue ? c.planned_tour_date.Value.ToString("MM/dd/yyyy") : "",
                                TouredDate = c.toured_date.HasValue ? c.toured_date.Value.ToString("MM/dd/yyyy") : "",

                                createdate = c.create_date.HasValue ? c.create_date.Value.ToString("MM/dd/yyyy") : "",
                                // add kim createdate 03/21/2022
                                AssessmentStartDate = c.assessment_start_date.HasValue ? c.assessment_start_date.Value.ToString("MM/dd/yyyy") : "",
                                //  CompletedDate = c.assessment_completed_date.HasValue ? c.assessment_completed_date.Value.ToString("MM/dd/yyyy") : "",
                                AssessmentCompletedDate = c.assessment_completed_date.HasValue ? c.assessment_completed_date.Value.ToString("MM/dd/yyyy") : "",

                                // AssMoveInDate = c.movein_date.HasValue ? c.movein_date.Value.ToString("MM/dd/yyyy") : "",
                                MoveInDate = c.movein_date.HasValue ? c.movein_date.Value.ToString("MM/dd/yyyy") : "",
                                LivesWithFamily = c.is_lives_with_family,
                                OwnHome = c.is_own_home,
                                RCFE = c.is_RCFE,
                                Rehab = c.is_rehab,
                                alertoriented = c.is_alert_oriented,
                                ambulatory = c.is_ambulatory,
                                bathingassist = c.need_bathing_assist,
                                canewalker = c.is_cane_walker,
                                forgetful = c.is_forgetful,
                                incontinentcare = c.need_incontinent_care,
                                laundry = c.need_laundry,
                                medmgnt = c.need_med_mgnt,
                                nonambulatory = c.is_non_ambulatory,
                                o2 = c.is_o2,
                                roomtrayservice = c.need_room_tray_service,
                                specdiet = c.is_spec_diet,
                                wheelchair = c.is_wheelchair,


                            };
                            return Json(o);
                        }

                        #endregion
                    }

                    else if (obj.Func == "referrals")
                    {
                        #region getreferral
                        int type = Convert.ToInt32(obj.Type);
                        bool f = (type == (int)AdmissionStatus.Referral_For_New_Lead);
                        var q = (type != 1) ? (from r in ALS.Residents
                                               join x in ALS.Referrals on r.resident_id equals x.resident_id
                                               where r.admission_status == type
                                               select new
                                               {
                                                   refid = x.referral_id,
                                                   resid = r.resident_id,
                                                   movein_status = x.is_assessment_completed,
                                                   name = WriteWidgetRoll(r.last_name, r.first_name, r.middle_initial,
                                                   (
                                                      (r.admission_status.Value == (int)AdmissionStatus.Referral_For_New_Lead) ? x.create_date :
                                                      (r.admission_status.Value == (int)AdmissionStatus.Referral_For_Follow_Up) ? x.create_date :
                                                      (r.admission_status.Value == (int)AdmissionStatus.Tour_For_Schedule) ? x.planned_tour_date :
                                                      (r.admission_status.Value == (int)AdmissionStatus.Tour_For_Follow_Up) ? x.toured_date :
                                                      (r.admission_status.Value == (int)AdmissionStatus.Assessment_For_Completion) ? x.assessment_start_date :
                                                      (r.admission_status.Value == (int)AdmissionStatus.Assessment_For_Follow_Up) ? x.assessment_completed_date : 
                                                      (r.admission_status.Value == (int)AdmissionStatus.Move_In_For_Schedule) ? x.movein_date :
                                                      (r.admission_status.Value == (int)AdmissionStatus.Admitted_As_Resident) ? r.date_admitted : null
                                                   ))
                                               }).ToList() :
                                               (from r in ALS.Residents
                                                join x in ALS.Referrals on r.resident_id equals x.resident_id
                                              //  join v in ALS.Admissions on r.resident_id equals v.resident_id
                                                //add code isAdmitted for all resident that is not discharge 02/11/22
                                                where r.admission_status == type && r.isAdmitted == true
                                                orderby r.resident_id descending
                                                select new
                                                {
                                                    refid = x.referral_id,
                                                    resid = r.resident_id,
                                                    movein_status = x.is_assessment_completed,
                                                    name = WriteWidgetRoll(r.last_name, r.first_name, r.middle_initial,
                                                    (
                                                        (r.admission_status.Value == (int)AdmissionStatus.Referral_For_New_Lead) ? x.create_date :
                                                        (r.admission_status.Value == (int)AdmissionStatus.Referral_For_Follow_Up) ? x.create_date :
                                                        (r.admission_status.Value == (int)AdmissionStatus.Tour_For_Schedule) ? x.planned_tour_date :
                                                        (r.admission_status.Value == (int)AdmissionStatus.Tour_For_Follow_Up) ? x.toured_date :
                                                        (r.admission_status.Value == (int)AdmissionStatus.Assessment_For_Completion) ? x.assessment_start_date :
                                                        (r.admission_status.Value == (int)AdmissionStatus.Assessment_For_Follow_Up) ? x.assessment_completed_date :
                                                        (r.admission_status.Value == (int)AdmissionStatus.Move_In_For_Schedule) ? x.movein_date :
                                                        (r.admission_status.Value == (int)AdmissionStatus.Admitted_As_Resident) ? r.date_admitted: null
                                                    ))
                                                }).ToList();
                        if (q != null)
                        {
                            return Json(q);
                        }
                        #endregion

                    }
                }
            }
            catch (Exception E)
            {
                var ind = "<b>Func = " + obj.Func + "</b><br/>";
                ind += "Line Number: " + E.LineNumber() + "<br>";
                LogError("Referral Controller/GetFormData", ind + "Error message: " + E.Message);
                return Json(E.Message);
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult PostData([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {

                var result = JsonResponse.GetDefault();
                if (obj != null)
                {
                    int record_id = 0;
                    var mode = obj.Mode;
                    string actionTaken = "", recordUpdated = "", func = "";

                    if (obj.Func == "postform")
                    {                        
                        result = ParseData(obj.Data);
                        func = "marketing_referral_info";
                        try
                        {
                            recordUpdated = obj.Data.Firstname + " " + obj.Data.Firstname;
                            if (obj.Data.ReferralId != "")
                            {
                                record_id = Convert.ToInt32(result.Message);
                                actionTaken = "2";
                            }
                            else
                            {
                                actionTaken = "1";
                                record_id = Convert.ToInt32(result.Message); 
                            }

                        }
                        catch { }
                    }
                    else if (obj.Func == "updatestatus")
                    {
                        result = ParseStatus(obj.Data);
                        func = "marketing_referral_updatestatus";
                        try
                        {
                            recordUpdated = "referralId=" + result.Message + "; residentId=" + result.Return;
                            record_id = Convert.ToInt32(result.Message);
                            actionTaken = "2";
                        }
                        catch { }
                    }
                    else if (obj.Func == "post_call_log")
                    {
                        result = ParseCallLog(obj);
                        func = "marketing_referral_postcallLog";
                        try
                        {
                            recordUpdated = "referralId=" + obj.id + "; residentId=" + result.Return;
                            record_id = Convert.ToInt32(obj.id);
                            actionTaken = "1";
                        }
                        catch { }
                    }

                    using (var ALSDB = new ALSBaseDataContext())
                    {
                        if (result.Result == 0) //added condition to log successful actions only
                        {
                            ALSDB.saveAction(actionTaken, UserID, record_id, func, recordUpdated);
                        }
                    }
                }
                return Json(result);
            }
            catch (Exception E)
            {
                LogError("Referral Controller/PostData", "Error message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }

        }
        private JsonResponse ParseStatus([DynamicJson] dynamic obj)
        {
            try
            {
                using (var ALSDB = new ALSBaseDataContext())
                {
                    Referral r;
                    Resident s;
                    Assessment a;

                    var refId = -1;
                    var resId = -1;
                    var status = obj.status;

                    if (!string.IsNullOrEmpty(obj.referralId))
                    {
                        refId = Convert.ToInt32(obj.referralId);
                    }
                    if (!string.IsNullOrEmpty(obj.residentId))
                    {
                        resId = Convert.ToInt32(obj.residentId);
                    }
                    r = ALSDB.Referrals.SingleOrDefault(x => x.referral_id == refId);
                    s = ALSDB.Residents.SingleOrDefault(x => x.resident_id == resId);
                    a = ALSDB.Assessments.SingleOrDefault(x => x.ResidentId == resId);
                    var currentstatus = s.admission_status;

                    if (s != null)
                    {
                        if (status == "3")
                        {
                            r.planned_tour_date = null;
                            r.toured_date = null;
                            if (r.assessment_start_date != null)
                            {
                                r.assessment_start_date = null;
                            }
                            if (r.assessment_completed_date != null)
                            {
                                r.assessment_completed_date = null;
                            }
                            if (r.movein_date != null)
                            {
                                r.movein_date = null;
                            }
                            if (a != null && a.CompletedDate != null)
                            {
                                a.CompletedDate = null;
                            }
                            if (r.is_assessment_completed != null)
                            {
                                r.is_assessment_completed = null;
                            }

                        }
                        if (status == "4")
                        {
                            r.toured_date = null;
                            if (r.assessment_start_date != null)
                            {
                                r.assessment_start_date = null;
                            }
                            if (r.assessment_completed_date != null)
                            {
                                r.assessment_completed_date = null;
                            }
                            if (r.movein_date != null)
                            {
                                r.movein_date = null;
                            }
                            if (a != null && a.CompletedDate != null)
                            {
                                a.CompletedDate = null;
                            }
                            if (r.is_assessment_completed != null)
                            {
                                r.is_assessment_completed = null;
                            }



                        }
                        if (status == "6")
                        {
                            r.assessment_completed_date = null;
                            r.movein_date = null;
                            r.is_assessment_completed = null;
                            a.CompletedDate = null;

                        }

                        s.admission_status = ((string)obj.status).TryParseNullableInt();
                        ALSDB.SubmitChanges();

                        try
                        {
                            return new JsonResponse(JsonResponseResult.Success, r.referral_id.ToString(), s.resident_id.ToString());
                        }
                        catch { }
                    }
                }
            }
            catch (Exception E)
            {
                LogError("Referral Controller/ParseStatus", "Error message: " + E.Message);
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();
        }
        private JsonResponse ParseData([DynamicJson] dynamic obj)
        {
            try
            {
                using (var ALSDB = new ALSBaseDataContext())
                {
                    var refId = -1;
                    var resId = -1;
                    var mode = PostMode.Add;
                    if (!string.IsNullOrEmpty(obj.ReferralId))
                    {
                        refId = Convert.ToInt32(obj.ReferralId);
                        mode = PostMode.Edit;
                    }
                    if (!string.IsNullOrEmpty(obj.ResidentId))
                    {
                        resId = Convert.ToInt32(obj.ResidentId);
                    }
                    Resident s;
                    Referral r;

                    //Reference Source added by Mariel 2017-07-18
                    ReferenceSource rfs;
                    string refSource = obj.RefSource;

                    rfs = ALSDB.ReferenceSources.FirstOrDefault(x => x.ref_source == refSource);

                    if (rfs == null)
                    {
                        rfs = new ReferenceSource();
                        rfs.ref_source = refSource;
                        rfs.date_created = DateTime.Now;
                        ALSDB.ReferenceSources.InsertOnSubmit(rfs);
                        ALSDB.SubmitChanges();

                    }
                    //kim add address and gender 06/15/22

                    string lastname = obj.Lastname;
                    string firstname = obj.Firstname;
                    string ssn = obj.txtSSN;
                    string address = obj.txtaddress;
                    var genderf = Convert.ToBoolean(obj.genderf);
                    var genderm = Convert.ToBoolean(obj.genderm);
                    string gen = "";
                    if (genderf == true && genderm == false)
                    {
                        gen = "F";
                    }
                    else if (genderm == true && genderf == false)
                    {
                        gen = "M";
                    }
                    else
                    {
                        return new JsonResponse(-3, "Please select gender");
                    }

                    var firstDigits = ssn.Substring(0, 3);
                    var lastDigits = ssn.Substring(7);
                    var sd = ssn.Substring(ssn.Length - 7, 2);
                    var maskedString = string.Concat(firstDigits, sd, lastDigits);
                    var birthday = ((string)obj.Rbirthday).TryParseNullableDate();
                    string res_ssn = obj.SSN;
                    var check_SSN_duplicate = (from x in ALSDB.Residents where x.SSN == ssn && (x.isAdmitted == true || x.isAdmitted == null) select x).FirstOrDefault();
                    var Sn = (from x in ALSDB.Carestaffs where x.ssn == maskedString select x).FirstOrDefault();

                    if (mode == PostMode.Add)
                    {
                        //kim add ssn in dup check 05/27/22
                        //var dup = (from x in ALSDB.Residents where x.last_name == lastname && x.first_name == firstname && x.birth_date == birthday && (x.isAdmitted == true || x.isAdmitted == null) select x).FirstOrDefault(); //Check duplicates from non discharged resident

                        var dup = (from x in ALSDB.Residents where x.last_name == lastname && x.first_name == firstname && x.SSN == ssn && (x.isAdmitted == true || x.isAdmitted == null) select x).FirstOrDefault(); //Check duplicates from non discharged resident
                        if (dup == null && (check_SSN_duplicate == null && Sn == null))
                        {
                            s = new Resident();
                            r = new Referral();
                            s.admission_status = (int)AdmissionStatus.Referral_For_New_Lead;
                            r.create_date = DateTime.UtcNow;
                            s.status = 1;
                        }
                        else if (dup == null && (check_SSN_duplicate != null || Sn != null))

                        {
                            return new JsonResponse(-1, "SSN Duplication");
                        }
                        else
                        {
                            var fnames = " ";
                            var duplicate = (from res in ALSDB.Residents
                                             where res.first_name == firstname && res.last_name == lastname
                                             select new
                                             {
                                                 FirstName = res.first_name,
                                                 LastName = res.last_name,
                                                 MiddleInitial = res.middle_initial,
                                                 Birthday = res.birth_date,
                                                 SSN = res.SSN
                                             }).OrderBy(x => x.MiddleInitial).ToArray();

                            foreach (var dp in duplicate)
                            {
                                var mi = dp.MiddleInitial == null || dp.MiddleInitial == "" ? "" : dp.MiddleInitial.ToString() + ". ";
                                var dssn = dp.SSN != null ? dp.SSN.ToString() : " ";
                                fnames += "* " + dp.FirstName.ToString() + " " + mi + " " + dp.LastName.ToString() +
                                    " Birthdate: " + dp.Birthday.Value.ToString("MM/dd/yyyy") + " SSN: " + dssn + "<br>";
                            }

                            return new JsonResponse(-2, fnames);
                        }
                    }
                    else
                    {
                        s = ALSDB.Residents.SingleOrDefault(x => x.resident_id == resId);
                        r = ALSDB.Referrals.SingleOrDefault(x => x.referral_id == refId);
                    }
                    if (s != null && r != null)
                    {
                        s.first_name = obj.Firstname;
                        s.last_name = obj.Lastname;
                        s.middle_initial = obj.MiddleInitial;
                        s.responsible_person = obj.ContactPerson;
                        s.responsible_person_telephone = obj.ContactPersonPhone;
                        s.responsible_person_email = obj.Email;
                        s.age = ((string)obj.Age).TryParseNullableByte();
                        s.birth_date = ((string)obj.Rbirthday).TryParseNullableDate(false);
                        s.SSN = obj.txtSSN;
                        s.gender = gen;
                        s.address_prior_admission = obj.txtaddress;
                        s.nearest_relative_relationship = obj.Relationship;
                        s.telephone = obj.ResidentPhone;
                        s.otherinfo_comments = obj.Remarks;
                        r.ref_source = obj.RefSource;
                        r.budget_prefered_room = obj.BudgetRoom;
                        r.notes = obj.Notes;
                        r.spec_need = obj.SpecNeed;
                        r.is_lives_with_family = Convert.ToBoolean(obj.LivesWithFamily);
                        r.is_own_home = Convert.ToBoolean(obj.OwnHome);
                        r.is_RCFE = Convert.ToBoolean(obj.RCFE);
                        r.is_rehab = Convert.ToBoolean(obj.Rehab);
                        r.is_alert_oriented = Convert.ToBoolean(obj.alertoriented);
                        r.is_ambulatory = Convert.ToBoolean(obj.ambulatory);
                        r.need_bathing_assist = Convert.ToBoolean(obj.bathingassist);
                        r.is_cane_walker = Convert.ToBoolean(obj.canewalker);
                        r.is_forgetful = Convert.ToBoolean(obj.forgetful);
                        r.need_incontinent_care = Convert.ToBoolean(obj.incontinentcare);
                        r.need_laundry = Convert.ToBoolean(obj.laundry);
                        r.need_med_mgnt = Convert.ToBoolean(obj.medmgnt);
                        r.is_non_ambulatory = Convert.ToBoolean(obj.nonambulatory);
                        r.is_o2 = Convert.ToBoolean(obj.o2);
                        r.need_room_tray_service = Convert.ToBoolean(obj.roomtrayservice);
                        r.is_spec_diet = Convert.ToBoolean(obj.specdiet);
                        r.is_wheelchair = Convert.ToBoolean(obj.wheelchair);
                        //modified ambulatory kim 11/24/22
                        s.otherinfo_ambulatory_status = "";


                        if (Convert.ToBoolean(obj.ambulatory) == true)
                        {
                            s.otherinfo_ambulatory_status = ("Ambulatory");

                        }
                        if (Convert.ToBoolean(obj.nonambulatory) == true)
                        {
                            s.otherinfo_ambulatory_status = (s.otherinfo_ambulatory_status == "" ? "Non-Ambulatory" : s.otherinfo_ambulatory_status + ",Non-Ambulatory");
                        }                        
                            if (Convert.ToBoolean(obj.wheelchair) == true)
                        {
                            s.otherinfo_ambulatory_status = (s.otherinfo_ambulatory_status == "" ? "Wheelchair" : s.otherinfo_ambulatory_status + ",Wheelchair");

                       }
                        if (Convert.ToBoolean(obj.specdiet) == true)
                        {
                            s.otherinfo_ambulatory_status = (s.otherinfo_ambulatory_status == "" ? "Special Diet" : s.otherinfo_ambulatory_status + ",Special Diet");

                        }
                        if (Convert.ToBoolean(obj.o2) == true)
                        {
                            s.otherinfo_ambulatory_status = (s.otherinfo_ambulatory_status == "" ? "O2" : s.otherinfo_ambulatory_status + ",O2");

                        }
                        if (Convert.ToBoolean(obj.forgetful) == true)
                        {
                            s.otherinfo_ambulatory_status = (s.otherinfo_ambulatory_status == "" ? "Forgetful" : s.otherinfo_ambulatory_status + ",Forgetful");

                        }
                        if (Convert.ToBoolean(obj.alertoriented) == true)
                        {
                            s.otherinfo_ambulatory_status = (s.otherinfo_ambulatory_status == "" ? "Alert/Oriented" : s.otherinfo_ambulatory_status + ",Alert/Oriented");

                        }
                        if (Convert.ToBoolean(obj.canewalker) == true)
                        {
                            s.otherinfo_ambulatory_status = (s.otherinfo_ambulatory_status == "" ? "Cane/Walker" : s.otherinfo_ambulatory_status + ",Cane/Walker");

                        }
                      

                        if (check_SSN_duplicate != null || Sn != null)
                        {
                            if (check_SSN_duplicate.resident_id != resId)
                            {
                                return new JsonResponse(-1, "SSN Duplication");
                            }

                        }

                        var PlannedTourDate = ((string)obj.PlannedTourDate).TryParseNullableDate(false);
                        if (PlannedTourDate != null)
                            r.planned_tour_date = PlannedTourDate;

                        var TouredDate = ((string)obj.TouredDate).TryParseNullableDate(false);
                        if (TouredDate != null)
                            r.toured_date = TouredDate;

                        var AssessmentStartDate = ((string)obj.AssessmentStartDate).TryParseNullableDate(false);
                        if (AssessmentStartDate != null)

                            r.assessment_start_date = AssessmentStartDate;

                        //    var AssessmentCompletedDate = ((string)obj.AssessmentCompletedDate).TryParseNullableDate(false);
                        var AssessmentCompletedDate = ((string)obj.AssessmentCompletedDate).TryParseNullableDate(false);
                        if (AssessmentCompletedDate != null)

                            r.assessment_completed_date = AssessmentCompletedDate;

                        //var AssMoveInDate = ((string)obj.MoveInDate).TryParseNullableDate(false);
                        //if (AssMoveInDate != null)
                        //    r.movein_date = AssMoveInDate;

                        var AssMoveInDate = ((string)obj.MoveInDate).TryParseNullableDate(false);
                        if (AssMoveInDate != null)
                            r.movein_date = AssMoveInDate;

                    }
                    if (mode == PostMode.Add)
                    {
                        if (r.planned_tour_date != null && s.admission_status == (int)AdmissionStatus.Referral_For_New_Lead)
                            s.admission_status = (int)AdmissionStatus.Tour_For_Schedule;

                        ALSDB.Residents.InsertOnSubmit(s);
                        ALSDB.SubmitChanges();
                        r.resident_id = s.resident_id;
                        ALSDB.Referrals.InsertOnSubmit(r);
                        ALSDB.SubmitChanges();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(r.notes) && r.planned_tour_date == null && s.admission_status == (int)AdmissionStatus.Referral_For_New_Lead)
                        {
                            s.admission_status = (int)AdmissionStatus.Referral_For_Follow_Up;
                        }

                        else if (r.planned_tour_date != null && s.admission_status == (int)AdmissionStatus.Referral_For_New_Lead)
                        {

                            s.admission_status = (int)AdmissionStatus.Tour_For_Schedule;
                        }

                        else if (r.planned_tour_date != null && s.admission_status == (int)AdmissionStatus.Referral_For_Follow_Up)
                        {
                            s.admission_status = (int)AdmissionStatus.Tour_For_Schedule;
                        }

                        else if (r.toured_date != null && s.admission_status == (int)AdmissionStatus.Tour_For_Schedule)
                        {
                            s.admission_status = (int)AdmissionStatus.Tour_For_Follow_Up;
                        }

                        else if (r.assessment_start_date != null && s.admission_status == (int)AdmissionStatus.Tour_For_Follow_Up)
                        {
                            s.admission_status = (int)AdmissionStatus.Assessment_For_Completion;
                        }

                        else if (r.assessment_completed_date != null && s.admission_status == (int)AdmissionStatus.Assessment_For_Completion)
                        {
                            s.admission_status = (int)AdmissionStatus.Assessment_For_Follow_Up;
                        }

                        else if (r.movein_date != null && s.admission_status == (int)AdmissionStatus.Assessment_For_Follow_Up)
                        {
                            s.admission_status = (int)AdmissionStatus.Move_In_For_Schedule;
                        }

                        ALSDB.SubmitChanges();                       
                    }
                    try
                    {
                        return new JsonResponse(JsonResponseResult.Success, r.referral_id.ToString());
                    }
                    catch { }
                }
            }
            catch (Exception E)
            {
                LogError("Referral Controller/ParseData", "Error message: " + E.Message);
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();
        }

        private string GetCallLogText(string val, string name)
        {
            return "(" + Utility.ToTimezonedDateTime(DateTime.UtcNow, "MMM dd, yyyy hh:mm tt") + " - " + name + "): " + val;
        }

        private JsonResponse ParseCallLog([DynamicJson] dynamic obj)
        {
            try
            {
                int id = Convert.ToInt32(obj.id);
                using (var ALSDB = new ALSBaseDataContext())
                {
                    var referral = ALSDB.Referrals.SingleOrDefault(x => x.referral_id == id);
                    var name = ALSDB.ALCUsers.SingleOrDefault(x => x.user_id == UserID);
                    if (name != null)
                    {
                        if (referral != null)
                        {
                            if (string.IsNullOrEmpty(referral.notes))
                                referral.notes = GetCallLogText(obj.data, name.first_name + " " + name.last_name);
                            else
                                referral.notes = referral.notes + Environment.NewLine + GetCallLogText(obj.data, name.first_name + " " + name.last_name);
                        }
                    }

                    ALSDB.SubmitChanges();
                    return new JsonResponse(0, referral.notes, referral.resident_id.ToString());
                }
            }
            catch (Exception E)
            {
                LogError("Referral Controller/ParseCallLog", "Error message: " + E.Message);
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
        }
    }
}