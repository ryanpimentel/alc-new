﻿using Datasoft.AssistedLiving.Web.Models;
using Datasoft.Ext;
using DataSoft.Helpers;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.html;
using System.Web;
using Newtonsoft.Json;
using System.Web.Http.Cors;


namespace Datasoft.AssistedLiving.Web.Controllers
{

    public class ResRep
    {
        public int RepParentId { get; set; }
        public int RepId { get; set; }
        public string RepParent { get; set; }
        public string RepName { get; set; }
        public string Index { get; set; }
        public int SignedStatus { get; set; }
    }
    //allowing request from other origin : Cheche : 6-27-2022
    // [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class AppPortalController : ApiController
    {
        private bool IsDeptHead(int RoleID)
        {
            var headlist = new int[] { 9, 10, 11, 12, 13 };
            return headlist.Contains(RoleID) ? true : false;
        }
        private int GetDepartmentStaffRoleID(int deptHeadRoleID)
        {
            if (deptHeadRoleID == (int)DepartmentHeads.MedTech)
                return (int)DepartmentStaff.MedTech;
            else if (deptHeadRoleID == (int)DepartmentHeads.Caregiver)
                return (int)DepartmentStaff.Caregiver;
            else if (deptHeadRoleID == (int)DepartmentHeads.Dietary)
                return (int)DepartmentStaff.Dietary;
            else if (deptHeadRoleID == (int)DepartmentHeads.HouseKeeper)
                return (int)DepartmentStaff.HouseKeeper;
            else if (deptHeadRoleID == (int)DepartmentHeads.Maintenance)
                return (int)DepartmentStaff.Maintenance;
            return 0;
        }

    


        //[ActionName("alluserlist")]
        [AllowAnonymous]
        [HttpPost]
        //  [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)] // 7-21-2022 : Cheche to Allow CORS 


        public dynamic Authenticate([DynamicJson(MatchName = false)] dynamic data)
        {
            string AgencyId = data.AgencyId,
                    UserId = data.UserId,
                    Password = data.Password;
            using (var ctx = ASLClientBaseContext(AgencyId))
            {
                var user = (from u in ctx.ALCUsers.AsEnumerable()
                            join ur in ctx.ALCUserRoles.AsEnumerable() on u.user_id equals ur.user_id
                            join r in ctx.ALCRoles.AsEnumerable() on ur.role_id equals r.role_id
                            where u.user_id == UserId && u.password == Password
                            select new
                            {
                                name = u.first_name + " " + (!string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + ". " : "") + u.last_name,
                                role = r.description,
                                u.user_id,
                                u.password,
                                ur.role_id,
                                u.is_active
                            }).SingleOrDefault();
                var retval = new
                {
                    AgencyId = AgencyId,
                    UserId = UserId,
                    Name = user.name,
                    Role = user.role,
                    RoleId = user.role_id,
                };
                return Json(retval);
            }
        }

        //Getting Employee for Login on Mobile App : 8-1-2022 : // for Testing Purposes 
        [AllowAnonymous]
        [HttpGet]
        public dynamic LoginUsers(string AgencyId, string UserId, string Password) // URL NOT RECOMMENDED
        {
          
            using (var ctx = ASLClientBaseContext(AgencyId))
            {
                var user = (from u in ctx.ALCUsers.AsEnumerable()
                            join ur in ctx.ALCUserRoles.AsEnumerable() on u.user_id equals ur.user_id
                            join r in ctx.ALCRoles.AsEnumerable() on ur.role_id equals r.role_id
                            where u.user_id == UserId && u.password == Password &&  u.is_active == true
                            select new
                            {
                                name = u.first_name + " " + (!string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + ". " : "") + u.last_name,
                                role = r.description,
                                roleId = r.role_id,
                                u.user_id,
                                u.password,
                                ur.role_id,
                                u.is_active,

                            }).SingleOrDefault();
                if (user == null || user.user_id != UserId || user.password != Password)
                {
                    return new
                    {
                        Result = -1,
                        ErrorMsg = "Invalid User ID or Password"
                    };
                }
                if (user.is_active == null || !user.is_active.Value)
                {
                    return new
                    {
                        Result = -1,
                        ErrorMsg = "Sorry, your account is no longer active. Please contact your System Administrator."
                    };
                }
                var retval = new
                {
                    AgencyId = AgencyId,
                    UserId = UserId,
                    Password = Password,
                    Name = user.name,
                    Role = user.role,
                    RoleId = user.role_id,
                };
                return Json(retval);
            }
        }


        // Added on 8-2-2022 : Cheche 
        [AllowAnonymous]
        [HttpGet]
        public dynamic AllEmployeeList(string AgencyId, string RoleId) // Used for Login : Working
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                int m_crole = int.Parse(RoleId);
                int? m_urole = null;
                if (IsDeptHead(m_crole))
                {
                    m_urole = GetDepartmentStaffRoleID(m_crole);
                }
                var x = ALS.getUserList(m_urole).ToList();
              

                var res = (from c in x
                           from d in ALS.ALCUsers 
                           where c.user_id == d.user_id
                           select new
                           {
                               EmployeeId = c.user_id,
                               Firstname = c.first_name,
                               MiddleInitial = c.middle_initial,
                               Lastname = c.last_name,
                               Password = d.password,
                               IsActive = c.is_active, 
                               SSN = c.ssn,
                               Role = c.role,
                               Shift = c.shift_start.HasValue && c.shift_end.HasValue ? ToTimezonedDateTime(c.shift_start, AgencyId).Value.ToString("hh:mm tt") + " - " + ToTimezonedDateTime(c.shift_end, AgencyId).Value.ToString("hh:mm tt") : "",
                               Image = d.imagesrc,
                               Agency = AgencyId

                           }).ToList();
            
                return Json(res);
            }
        }
        // added on Aug 4-2022 : Cheche for posting rendered services : // Working
        [AllowAnonymous]
        [HttpGet]
        public dynamic GetAllServices(string AgencyId)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var res = (from c in ALS.Activities
                           select new
                           {
                               ActivityId = c.activity_id,
                               ActivityDesc = c.activity_desc,
                               ActivityProc = c.activity_procedure,
                               ActivityStat = c.activity_status,

                           }).ToList();

                return Json(res);
            }
        }


        // Added on 7 : 26 : 2022 : Cheche replicate Login from LoginController :  for Testing Purposes 
        [AllowAnonymous]
        [HttpPost]
        public dynamic MobileAuth([DynamicJson(MatchName = false)] dynamic obj) 
        {
            if (obj != null)
            {
                var result = MobileLogin(obj);
                return Json(result);
            }
            return Json("");
        }
        public dynamic MobileLogin([DynamicJson(MatchName = false)] dynamic data)
        {
             string AgencyId = data.AgencyId,
                    UserId = data.UserId,
                    Password = data.Password;


            using (var ctx = ASLClientBaseContext(AgencyId))
            {
                var user = (from u in ctx.ALCUsers.AsEnumerable()
                            join ur in ctx.ALCUserRoles.AsEnumerable() on u.user_id equals ur.user_id
                            join r in ctx.ALCRoles.AsEnumerable() on ur.role_id equals r.role_id
                            where u.user_id == UserId && u.password == Password
                            select new
                            {
                                name = u.first_name + " " + (!string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + ". " : "") + u.last_name,
                                role = r.description,
                                u.user_id,
                                u.password,
                                ur.role_id,
                                u.is_active
                            }).SingleOrDefault();

                if (user == null || user.user_id != UserId || user.password != Password)
                {
                    return new
                    {
                        Result = -1,
                        ErrorMsg = "Invalid User ID or Password"
                    };
                }
                if (user.is_active == null || !user.is_active.Value)
                {
                    return new
                    {
                        Result = -1,
                        ErrorMsg = "Sorry, your account is no longer active.  Please contact your System Administrator."
                    };
                }

                var retval = new
                {
                    AgencyId = AgencyId,
                    UserId = UserId,
                    Name = user.name,
                    Role = user.role,
                    RoleId = user.role_id,
                };
                return Json(retval);
            }
        }
        //7-29-2022 : Cheche : for Testing Purposes 
        [AllowAnonymous]
        [HttpPost]
        public dynamic MobileAuthenticate([DynamicJson(MatchName = false)] dynamic data)
        {
            string AgencyId = data.AgencyId,
                    UserId = data.UserId,
                    Password = data.Password;
            using (var ctx = ASLClientBaseContext(AgencyId))
            {
                var user = (from u in ctx.ALCUsers.AsEnumerable()
                            join ur in ctx.ALCUserRoles.AsEnumerable() on u.user_id equals ur.user_id
                            join r in ctx.ALCRoles.AsEnumerable() on ur.role_id equals r.role_id
                            where u.user_id == UserId && u.password == Password
                            select new
                            {
                                name = u.first_name + " " + (!string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + ". " : "") + u.last_name,
                                role = r.description,
                                u.user_id,
                                u.password,
                                ur.role_id,
                                u.is_active
                            }).SingleOrDefault();
                var retval = new
                {
                    AgencyId = AgencyId,
                    UserId = UserId,
                    Name = user.name,
                    Role = user.role,
                    RoleId = user.role_id,
                };
                return Json(retval);
            }
        }

        // 7-21-2022 : Cheche : Mobile Login Authentication
        //  [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
        [AllowAnonymous]
        [HttpGet, HttpPost]
        public dynamic MobileLogin(string AgencyId, string UserId, string Password) // For Testing Purposes
        {

            using (var ctx = ASLClientBaseContext(AgencyId))
            {
                var user = (from u in ctx.ALCUsers.AsEnumerable()
                            join ur in ctx.ALCUserRoles.AsEnumerable() on u.user_id equals ur.user_id
                            join r in ctx.ALCRoles.AsEnumerable() on ur.role_id equals r.role_id
                            where u.user_id == UserId && u.password == Password
                            select new
                            {
                                name = u.first_name + " " + (!string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + ". " : "") + u.last_name,
                                role = r.description,
                                u.user_id,
                                u.password,
                                ur.role_id,
                                u.is_active,
                                u.imagesrc,
                            }).SingleOrDefault();
                var retval = new
                {
                   //added some changes here on 8-15-2022
                    AgencyId = AgencyId,
                    UserId = UserId,
                    Password = Password,
                    Name = user.name,
                    Role = user.role,
                    RoleId = user.role_id,
                    IsActive = user.is_active,
                    Image = user.imagesrc,
                };

                return Json(retval);
            }
        }
     
        // Added on 7-19-2022 : Cheche  : Services
        [AllowAnonymous]
        [HttpGet]
        public dynamic GetServices(string AgencyId) //Working 
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var x = (from b in ALS.ActivityScheduleHMCs
                         join bb in ALS.ServiceCompletions on b.activity_schedule_id equals bb.activity_schedule_id
                         join c in ALS.Residents on b.resident_id equals c.resident_id
                         join cc in ALS.ALCUsers on b.carestaff_id equals cc.user_id
                         select new
                         {
                             ActivityScheduleId = b.activity_schedule_id,
                             ResidentId = b.resident_id,
                             CarestaffId = b.carestaff_id,
                             IsActive = b.is_active,
                             Recurrence = b.recurrence,
                             IsOneTime = b.is_onetime,
                             Active_until = Utility.GetDateFormattedString(b.active_until, "MM/dd/yyyy"),
                             Start_date = Utility.GetDateFormattedString(b.start_date, "MM/dd/yyyy"),
                             End_date = Utility.GetDateFormattedString(b.end_date, "MM/dd/yyyy"),
                            // Carestaff = c.first_name + " " + c.last_name, // 8-15-2022 
                             Carestaff = cc.first_name + " " + cc.last_name,
                             Start_time = Utility.GetDateFormattedString(b.start_time, "HH:mm"),
                             End_time = Utility.GetDateFormattedString(b.end_time, "HH:mm"),
                             Name = c.first_name + " " + c.last_name,
                             IsAdmitted = c.isAdmitted,
                             Services = bb.services,
                             ServiceCompletionId = bb.service_completion_id,
                             ServiceDate = bb.service_date,
                             Status = bb.status,
                             CompletedBy = bb.completed_by,
                             Comment = bb.comment,
                             ActualCompletionDate = bb.actual_completion_date

                         }).ToList();
                return Json(x);
            }
        }
     
        //added on 8-15-2022 
        [AllowAnonymous]
        [HttpGet]
        public dynamic GetEmployeeService(string AgencyId, string RoleId)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                int m_crole = int.Parse(RoleId);
                int? m_urole = null;
                if (IsDeptHead(m_crole))
                {
                    m_urole = GetDepartmentStaffRoleID(m_crole);
                }
                var user = ALS.getUserList(m_urole).ToList();

                var res = (from c in user
                           join d in ALS.PlannedServices on c.user_id equals d.carestaff_id
                           join cd in ALS.PlannedAndPostedServices on d.carestaff_id equals cd.carestaff_id
                           where d.planned_service_id == cd.planned_service_id
                           where c.is_active == true && d.is_active == true

                           select new
                           {
                               
                               EmployeeId = c.user_id,
                               Firstname = c.first_name,
                               MiddleInitial = c.middle_initial,
                               Lastname = c.last_name,
                               SSN = c.ssn,
                               Role = c.role,
                               PlannedServiceId = d.planned_service_id,
                               IsOneTime = d.is_onetime,
                               Services = d.services,
                               Shift = c.shift_start.HasValue && c.shift_end.HasValue ? ToTimezonedDateTime(c.shift_start, AgencyId).Value.ToString("hh:mm tt") + " - " + ToTimezonedDateTime(c.shift_end, AgencyId).Value.ToString("hh:mm tt") : ""

                           }).ToList();
                return Json(res);
            }
        }

        //Added by Cheche 8-10-2022 : 
        [AllowAnonymous]
        [HttpGet]
        public dynamic GetPostedAndPlannedServices(string AgencyId) // empty
        {
         
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
             
                var planned_posted = (from ps in ALS.PlannedServices
                                      join paps in ALS.PlannedAndPostedServices on ps.planned_service_id equals paps.planned_service_id
                                      join r in ALS.Residents on ps.client_id equals r.resident_id
                                      join a in ALS.Admissions on ps.client_id equals a.resident_id
                                      join c in ALS.ALCUsers on ps.carestaff_id equals c.user_id
                                      where r.isAdmitted == true && paps.is_active == true
                                      select new
                                      {
                                      
                                          ClientID = ps.client_id,
                                          Resident = r.first_name + " " + r.last_name,
                                          IsActive = ps.is_active,
                                          StartofCare = ps.soc,
                                          EndfCare = ps.eoc,
                                          Carestaff = c.first_name + " " + c.last_name,
                                          CarestaffId = c.user_id,
                                          Start_time = ps.start_time,
                                          End_time = ps.end_time,
                                          Client =  r.first_name + " " + r.last_name,
                                          Recurrence = ps.recurrence,
                                          IsAdmitted = r.isAdmitted,
                                          PlannedServiceId = ps.planned_service_id,
                                          IsOnetime = ps.is_onetime,
                                          ServicesRendered = paps.services_rendered,
                                          PlannedPostedServiceId = paps.planned_posted_service_id,
                                          Planned_date = paps.planned_date,
                                          Paps_carestaff = paps.carestaff_id,
                                          Paps_remarks = paps.remarks,
                                          Posted_date = paps.posted_date,
                                          PlannedServices = paps.planned_services
                                         
                                      }).ToList();

                return Json(planned_posted);
            }
        }


        [AllowAnonymous]
        [HttpGet]

        public dynamic GetResidentList(string AgencyId) //updated data
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var x = (from c in ALS.Residents
                         join d in ALS.Admissions on c.resident_id equals d.resident_id into da
                         from dd in da.DefaultIfEmpty()
                         join r in ALS.Rooms on dd.room_id equals r.room_id into ra
                         from rr in ra.DefaultIfEmpty()
                         join lk in ALS.lk_AdmissionStatus on c.admission_status equals lk.admission_status_id
                         where (c.admission_status ?? 0) == 1
                         orderby c.first_name
                         select new
                         {
                             ResidentId = c.resident_id,
                             Firstname = c.first_name,
                             MiddleInitial = c.middle_initial,
                             Lastname = c.last_name,
                             Gender = c.gender,
                             SSN = c.SSN,
                             Birthdate = c.birth_date,
                             RoomId = (int?)rr.room_id,
                             RoomName = rr.room_name,
                             AdmissionDate = dd.admission_date,
                             AdmissionStatus = lk.description,
                             DateLeft = c.date_left, //added on 8-15-2022 to distinguished active/inactive residents when choosing an option
                             //c.imagesrc
                             Image = c.imagesrc, // added on 7-8-2022 : Cheche for displaying image on mobile apps

                         }).ToList();
                var res = (from c in x
                           select new
                           {
                               c.ResidentId,
                               c.DateLeft, // added on 8-15-2022
                               c.Firstname,
                               c.MiddleInitial,
                               c.Lastname,
                               c.Gender,
                               c.SSN,
                               Birthdate = c.Birthdate == null ? "" : ToTimezonedDateTime(c.Birthdate, AgencyId).Value.ToString("MM/dd/yyyy"),
                               c.RoomId,
                               c.RoomName,
                               AdmissionDate = c.AdmissionDate == null ? "" : ToTimezonedDateTime(c.AdmissionDate, AgencyId).Value.ToString("MM/dd/yyyy"),
                               c.AdmissionStatus,
                               //c.imagesrc
                               c.Image, // added on 7-11-2022 : Cheche

                           }).ToList();
                return Json(res);
            }
        }



        [AllowAnonymous]
        [HttpGet]
        public dynamic GetEmployeeList(string AgencyId, string RoleId)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                int m_crole = int.Parse(RoleId);
                int? m_urole = null;
                if (IsDeptHead(m_crole))
                {
                    m_urole = GetDepartmentStaffRoleID(m_crole);
                }
                var x = ALS.getUserList(m_urole).ToList();

                var res = (from c in x
                           select new
                           {
                               EmployeeId = c.user_id,
                               Firstname = c.first_name,
                               MiddleInitial = c.middle_initial,
                               Lastname = c.last_name,
                               SSN = c.ssn,
                               Role = c.role,
                               Shift = c.shift_start.HasValue && c.shift_end.HasValue ? ToTimezonedDateTime(c.shift_start, AgencyId).Value.ToString("hh:mm tt") + " - " + ToTimezonedDateTime(c.shift_end, AgencyId).Value.ToString("hh:mm tt") : ""
                               //c.imagesrc

                           }).ToList();
                return Json(res);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public dynamic GetReports(string AgencyId, string PersonId, string Type)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                List<ResRep> res = new List<ResRep>();
                #region Resident
                if (Type.ToLower() == "resident")
                {
                    int resid = int.Parse(PersonId);
                    var ad = ALS.Admissions.Where(x => x.resident_id == resid).ToList().LastOrDefault();
                    if (ad != null)//admission
                    {
                        AdmissionAgreement aa = ALS.AdmissionAgreements.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (aa != null)
                            res.Add(new ResRep()
                            {
                                RepId = aa.admission_agreement_id,
                                RepParentId = ad.admission_id,
                                RepName = "Admission Agreement",
                                RepParent = "Admission",
                                Index = "AA",
                                SignedStatus = aa.isSigned.HasValue ? Convert.ToInt32(aa.isSigned.Value) : 0
                            });
                        AppraisalNeedAndService ans = ALS.AppraisalNeedAndServices.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (ans != null)
                            res.Add(new ResRep()
                            {
                                RepId = ans.appraisal_id,
                                RepParentId = ad.admission_id,
                                RepName = "Appraisal Need And Services Plan Worksheet",
                                Index = "ANS",
                                RepParent = "Admission",
                                SignedStatus = ans.isSigned.HasValue ? Convert.ToInt32(ans.isSigned.Value) : 0
                            });
                        DocumentChecklist dc = ALS.DocumentChecklists.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (dc != null)
                            res.Add(new ResRep()
                            {
                                RepId = dc.checklist_id,
                                RepParentId = ad.admission_id,
                                RepName = "Resident File Folder - Document Checklist",
                                Index = "DC",
                                RepParent = "Admission",
                                SignedStatus = dc.isSigned.HasValue ? Convert.ToInt32(dc.isSigned.Value) : 0
                            });
                        ResidentMoveIn mi = ALS.ResidentMoveIns.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (mi != null)
                            res.Add(new ResRep()
                            {
                                RepId = mi.movein_id,
                                RepParentId = ad.admission_id,
                                RepName = "New Resident Move-In Memo",
                                Index = "MI",
                                RepParent = "Admission",
                                SignedStatus = mi.isSigned.HasValue ? Convert.ToInt32(mi.isSigned.Value) : 0
                            });

                        PhysicianReport pr = ALS.PhysicianReports.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (pr != null)
                            res.Add(new ResRep()
                            {
                                RepId = pr.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Physician Report",
                                Index = "PR",
                                RepParent = "Admission",
                                SignedStatus = pr.isSigned.HasValue ? Convert.ToInt32(pr.isSigned.Value) : 0
                            });
                        ResidentIdentificationEmergencyInfo iei = ALS.ResidentIdentificationEmergencyInfos.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (iei != null)
                            res.Add(new ResRep()
                            {
                                RepId = iei.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Identification and Emergency Information",
                                Index = "IEI",
                                RepParent = "Admission",
                                SignedStatus = iei.isSigned.HasValue ? Convert.ToInt32(iei.isSigned.Value) : 0
                            });
                        PreplacementAppraisalInfo pai = ALS.PreplacementAppraisalInfos.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (pai != null)
                            res.Add(new ResRep()
                            {
                                RepId = pai.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Preplacement Appraisal Information",
                                Index = "PAI",
                                RepParent = "Admission",
                                SignedStatus = pai.isSigned.HasValue ? Convert.ToInt32(pai.isSigned.Value) : 0
                            });
                        ResidentReleaseMedicalInfo rmi = ALS.ResidentReleaseMedicalInfos.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (rmi != null)
                            res.Add(new ResRep()
                            {
                                RepId = rmi.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Release of Resident Medical Information",
                                Index = "RMI",
                                RepParent = "Admission",
                                SignedStatus = rmi.isSigned.HasValue ? Convert.ToInt32(rmi.isSigned.Value) : 0
                            });
                        ResidentAppraisal ra = ALS.ResidentAppraisals.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (ra != null)
                            res.Add(new ResRep()
                            {
                                RepId = ra.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Resident Appraisal",
                                Index = "RA",
                                RepParent = "Admission",
                                SignedStatus = ra.isSigned.HasValue ? Convert.ToInt32(ra.isSigned.Value) : 0
                            });
                        TelecommunicationNotif tdn = ALS.TelecommunicationNotifs.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (tdn != null)
                            res.Add(new ResRep()
                            {
                                RepId = tdn.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Telecommunications Device Notification",
                                Index = "TDN",
                                RepParent = "Admission",
                                SignedStatus = tdn.isSigned.HasValue ? Convert.ToInt32(tdn.isSigned.Value) : 0
                            });
                        PersonalRight per = ALS.PersonalRights.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (per != null)
                            res.Add(new ResRep()
                            {
                                RepId = per.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Personal Rights",
                                Index = "PER",
                                RepParent = "Admission",
                                SignedStatus = per.isSigned.HasValue ? Convert.ToInt32(per.isSigned.Value) : 0
                            });
                        ConsentMedicalExam cme = ALS.ConsentMedicalExams.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (cme != null)
                            res.Add(new ResRep()
                            {
                                RepId = cme.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Consent to a Medical Examination",
                                Index = "CME",
                                RepParent = "Admission",
                                SignedStatus = cme.isSigned.HasValue ? Convert.ToInt32(cme.isSigned.Value) : 0
                            });
                        DecisionRightsESign dr = ALS.DecisionRightsESigns.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (dr != null)
                        {
                            res.Add(new ResRep()
                            {
                                RepId = dr.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Right To Make Decisions About Medical Treatment",
                                Index = "DR",
                                RepParent = "Admission",
                                SignedStatus = dr.isSigned.HasValue ? Convert.ToInt32(dr.isSigned.Value) : 0
                            });
                        }
                        else //create blank
                        {
                            res.Add(new ResRep()
                            {
                                RepId = ad.admission_id,
                                RepParentId = ad.admission_id,
                                RepName = "Right To Make Decisions About Medical Treatment",
                                Index = "DR",
                                RepParent = "Admission",
                                SignedStatus = 0
                            });
                        }
                        PhotographyConsentESign pc = ALS.PhotographyConsentESigns.SingleOrDefault(x => x.admission_id == ad.admission_id);
                        if (pc != null)
                        {
                            res.Add(new ResRep()
                            {
                                RepId = pc.admission_id.Value,
                                RepParentId = ad.admission_id,
                                RepName = "Photography Consent",
                                Index = "PC",
                                RepParent = "Admission",
                                SignedStatus = pc.isSigned.HasValue ? Convert.ToInt32(pc.isSigned.Value) : 0
                            });
                        }
                        else //create blank
                        {
                            res.Add(new ResRep()
                            {
                                RepId = ad.admission_id,
                                RepParentId = ad.admission_id,
                                RepName = "Photography Consent",
                                Index = "PC",
                                RepParent = "Admission",
                                SignedStatus = 0
                            });
                        }
                    }
                    var ass = ALS.Assessments.Where(x => x.ResidentId == resid).ToList().LastOrDefault();
                    if (ass != null)//assessment
                    {
                        res.Add(new ResRep()
                        {
                            RepId = (int)ass.AssessmentId,
                            RepParentId = (int)ass.AssessmentId,
                            RepName = "Assessment Form",
                            Index = "ASS",
                            RepParent = "Assessment",
                            SignedStatus = 0
                        });
                        res.Add(new ResRep()
                        {
                            RepId = (int)ass.AssessmentId,
                            RepParentId = (int)ass.AssessmentId,
                            RepName = "Morse Fall Scale",
                            Index = "MFS",
                            RepParent = "Assessment",
                            SignedStatus = 0
                        });
                    }
                }

                #endregion
                else if (Type.ToLower() == "employee")
                {
                    var usr = ALS.ALCUsers.Where(x => x.user_id == PersonId).SingleOrDefault();
                    if (usr != null)
                    {
                        PersonnelFileChecklist pfc = ALS.PersonnelFileChecklists.SingleOrDefault(x => x.user_id == usr.user_id);
                        if (pfc != null)
                            res.Add(new ResRep()
                            {
                                RepId = pfc.personnelfilelist_id,
                                RepParentId = 0,
                                RepName = "Personnel File Checklist",
                                RepParent = usr.user_id,
                                Index = "PFC",
                                SignedStatus = pfc.isSigned.HasValue ? Convert.ToInt32(pfc.isSigned.Value) : 0
                            });

                        HealthScreeningReport hs = ALS.HealthScreeningReports.SingleOrDefault(x => x.user_id == usr.user_id);
                        if (hs != null)
                            res.Add(new ResRep()
                            {
                                RepId = hs.healthscreenrep_id,
                                RepParentId = 0,
                                RepName = "Health Screening Report",
                                RepParent = usr.user_id,
                                Index = "HS",
                                SignedStatus = hs.isSigned.HasValue ? Convert.ToInt32(hs.isSigned.Value) : 0
                            });
                        EmployeeRightsESign er = ALS.EmployeeRightsESigns.SingleOrDefault(x => x.user_id == usr.user_id);
                        if (er != null)
                        {
                            res.Add(new ResRep()
                            {
                                RepId = er.employeerights_id,
                                RepParentId = 0,
                                RepName = "Employee Rights",
                                Index = "ER",
                                RepParent = usr.user_id,
                                SignedStatus = er.isSigned.HasValue ? Convert.ToInt32(er.isSigned.Value) : 0
                            });
                        }
                        else //create blank
                        {
                            res.Add(new ResRep()
                            {
                                RepId = 0,
                                RepParentId = 0,
                                RepName = "Employee Rights",
                                Index = "ER",
                                RepParent = usr.user_id,
                                SignedStatus = 0
                            });
                        }
                        EmpPhotographyConsentESign epc = ALS.EmpPhotographyConsentESigns.SingleOrDefault(x => x.user_id == usr.user_id);
                        if (epc != null)
                        {
                            res.Add(new ResRep()
                            {
                                RepId = epc.empphotographyconsent_id,
                                RepParentId = 0,
                                RepName = "Photography/Video Camera Consent",
                                Index = "EPC",
                                RepParent = usr.user_id,
                                SignedStatus = epc.isSigned.HasValue ? Convert.ToInt32(epc.isSigned.Value) : 0
                            });
                        }
                        else //create blank
                        {
                            res.Add(new ResRep()
                            {
                                RepId = 0,
                                RepParentId = 0,
                                RepName = "Photography/Video Camera Consent",
                                Index = "EPC",
                                RepParent = usr.user_id,
                                SignedStatus = 0
                            });
                        }
                        ElderAbuseAcknowledgementESign eaa = ALS.ElderAbuseAcknowledgementESigns.SingleOrDefault(x => x.user_id == usr.user_id);
                        if (eaa != null)
                        {
                            res.Add(new ResRep()
                            {
                                RepId = eaa.elderlyabuse_id,
                                RepParentId = 0,
                                RepName = "Elder Abuse Acknowledgement",
                                Index = "EAA",
                                RepParent = usr.user_id,
                                SignedStatus = eaa.isSigned.HasValue ? Convert.ToInt32(eaa.isSigned.Value) : 0
                            });
                        }
                        else //create blank
                        {
                            res.Add(new ResRep()
                            {
                                RepId = 0,
                                RepParentId = 0,
                                RepName = "Elder Abuse Acknowledgement",
                                Index = "EAA",
                                RepParent = usr.user_id,
                                SignedStatus = 0
                            });
                        }
                        HepatitisFormESign hep = ALS.HepatitisFormESigns.SingleOrDefault(x => x.user_id == usr.user_id);
                        if (hep != null)
                        {
                            res.Add(new ResRep()
                            {
                                RepId = hep.hepatitisform_id,
                                RepParentId = 0,
                                RepName = "Hepatitis B Vaccine Declination Form",
                                Index = "HEP",
                                RepParent = usr.user_id,
                                SignedStatus = hep.isSigned.HasValue ? Convert.ToInt32(hep.isSigned.Value) : 0
                            });
                        }
                        else //create blank
                        {
                            res.Add(new ResRep()
                            {
                                RepId = 0,
                                RepParentId = 0,
                                RepName = "Hepatitis B Vaccine Declination Form",
                                Index = "HEP",
                                RepParent = usr.user_id,
                                SignedStatus = 0
                            });
                        }
                        EmployeeHandbookReceiptESign ehr = ALS.EmployeeHandbookReceiptESigns.SingleOrDefault(x => x.user_id == usr.user_id);
                        if (ehr != null)
                        {
                            res.Add(new ResRep()
                            {
                                RepId = ehr.employeehandbookreceipt_id,
                                RepParentId = 0,
                                RepName = "Employee Handbook Receipt",
                                Index = "EHR",
                                RepParent = usr.user_id,
                                SignedStatus = ehr.isSigned.HasValue ? Convert.ToInt32(ehr.isSigned.Value) : 0
                            });
                        }
                        else //create blank
                        {
                            res.Add(new ResRep()
                            {
                                RepId = 0,
                                RepParentId = 0,
                                RepName = "Employee Handbook Receipt",
                                Index = "EHR",
                                RepParent = usr.user_id,
                                SignedStatus = 0
                            });
                        }
                        CriminalRecordStatement csr = ALS.CriminalRecordStatements.SingleOrDefault(x => x.user_id == usr.user_id);
                        if (csr != null)
                            res.Add(new ResRep()
                            {
                                RepId = csr.criminal_record_statement_id,
                                RepParentId = 0,
                                RepName = "Criminal Record Statement",
                                RepParent = usr.user_id,
                                Index = "CSR",
                                SignedStatus = csr.isSigned.HasValue ? Convert.ToInt32(csr.isSigned.Value) : 0
                            });
                    }
                }
                return Json(res);
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public dynamic GetUserSDT(string AgencyId, string UserId, string RoleId, string CurrentDate)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {

                DateTime? curDate = CurrentDate.TryParseNullableDate();
                int? roleId = RoleId.TryParseNullableInt();
                var cs = (from c in ALS.getActivityScheduleByUser(UserId, roleId, curDate, null, null, null)
                          select new
                          {
                              c.activity_desc,
                              c.activity_id,
                              c.activity_proc,
                              c.activity_schedule_id,
                              c.activity_status,
                              c.oactivity_status,
                              c.department,
                              c.carestaff_activity_id,
                              c.completion_date,
                              c.remarks,
                              c.carestaff_name,
                              c.resident,
                              c.room,
                              c.service_duration,
                              c.acknowledged_by,
                              start_time = ToTimezonedDateTime(c.start_time, AgencyId).Value.ToString("hh:mm tt"),
                              c.xref
                          }).ToList();

                return Json(cs);
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public dynamic GetActivityScheduleById(string AgencyId, string Id) // returned null
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                int? _id = Id.TryParseNullableInt();
                var cs = (from c in ALS.getActivityScheduleById(_id)
                          select new
                          {
                              c.Id,
                              c.Recurrence,
                              c.ResidentId,
                              c.ResidentName,
                              c.RoomId,
                              c.RoomName,
                              Activity = c.ActivityProcedure,
                              ActivityDesc = c.Activity,
                              c.ActivityId,
                              c.CarestaffId,
                              c.CarestaffName,
                              c.Dept,
                              c.IsOneTime,
                              ScheduleTime = ToTimezonedDateTime(c.ScheduleTime, AgencyId).Value.ToString("hh:mm tt"),
                              Onetimedate = ToTimezonedDateTime(c.ScheduleTime, AgencyId).Value.ToString("MM/dd/yyyy")
                          }).SingleOrDefault();

                return Json(cs);
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public dynamic GetReportLink(string AgencyId, string ReportId, string Index)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                if (Index == "AA")
                {
                    createAA(AgencyId, Convert.ToInt32(ReportId));
                    return Json("AdmissionAgreement");
                }
                else if (Index == "ANS")
                {
                    createANS(AgencyId, Convert.ToInt32(ReportId));
                    return Json("AppraisalNeedAndServicePlan");
                }
                else if (Index == "DC")
                {
                    createDC(AgencyId, Convert.ToInt32(ReportId));
                    return Json("DocumentChecklist");
                }
                else if (Index == "MI")
                {
                    createMI(AgencyId, Convert.ToInt32(ReportId));
                    return Json("MoveIn");
                }
                else if (Index == "IEI")
                {
                    createIEI(AgencyId, Convert.ToInt32(ReportId));
                    return Json("ResidentIdentificationEmergencyInfo");
                }
                else if (Index == "PAI")
                {
                    createPAI(AgencyId, Convert.ToInt32(ReportId));
                    return Json("PreplacementAppraisalInfo");
                }
                else if (Index == "RMI")
                {
                    createRMI(AgencyId, Convert.ToInt32(ReportId));
                    return Json("ResidentReleaseMedicalInfo");
                }
                else if (Index == "PR")
                {
                    createPR(AgencyId, Convert.ToInt32(ReportId));
                    return Json("PhysicianReport");
                }
                else if (Index == "RA")
                {
                    createRA(AgencyId, Convert.ToInt32(ReportId));
                    return Json("ResidentAppraisal");
                }
                else if (Index == "TDN")
                {
                    createTDN(AgencyId, Convert.ToInt32(ReportId));
                    return Json("TelecommunicationDeviceNotification");
                }
                else if (Index == "PER")
                {
                    createPER(AgencyId, Convert.ToInt32(ReportId));
                    return Json("PersonalRights");
                }
                else if (Index == "DR")
                {
                    createDR(AgencyId, Convert.ToInt32(ReportId));
                    return Json("RightToMakeDecision");
                }
                else if (Index == "PC")
                {
                    createPC(AgencyId, Convert.ToInt32(ReportId));
                    return Json("PhotographyConsent");
                }
                else if (Index == "PFC")
                {
                    createPFC(AgencyId, Convert.ToInt32(ReportId));
                    return Json("PersonnelFileChecklist");
                }
                else if (Index == "HS")
                {
                    createHS(AgencyId, Convert.ToInt32(ReportId));
                    return Json("HealthScreeningReport");
                }
                else if (Index == "ER")
                {
                    createER(AgencyId, Convert.ToInt32(ReportId));
                    return Json("EmployeeRights");
                }
                else if (Index == "EPC")
                {
                    createEPC(AgencyId, Convert.ToInt32(ReportId));
                    return Json("EmployeePhotographyConsent");
                }
                else if (Index == "CME")
                {
                    createCME(AgencyId, Convert.ToInt32(ReportId));
                    return Json("ConsentMedicalExam");
                }
                else if (Index == "EAA")
                {
                    createEAA(AgencyId, Convert.ToInt32(ReportId));
                    return Json("ElderAbuseReport");
                }
                else if (Index == "HEP")
                {
                    createHEP(AgencyId, Convert.ToInt32(ReportId));
                    return Json("HepatitisForm");
                }
                else if (Index == "EHR")
                {
                    createEHR(AgencyId, Convert.ToInt32(ReportId));
                    return Json("HandbookReceipt");
                }
                else if (Index == "CSR")
                {
                    createCSR(AgencyId, Convert.ToInt32(ReportId));
                    return Json("CriminalRecordStatement");
                }
                else if (Index == "ASS")
                {
                }
                else if (Index == "MFS")
                {
                }
            }
            return Json("");

        }
        [AllowAnonymous]
        [HttpGet]
        public dynamic GetCarestaffByTime(string AgencyId, string ScheduleTime, string Recurrence, string IsOneTime, string ClientDT, string ActivityId, string CarestaffId)
        {
            var result = JsonResponse.GetDefault();

            bool? _isonetime = (IsOneTime).TryParseNullableBool();
            DateTime? _clientdt = (ClientDT).TryParseNullableDate();

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                DateTime _time = DateTime.ParseExact(ScheduleTime, "h:mm tt", System.Globalization.CultureInfo.InvariantCulture);
                int? _activityid = Convert.ToInt32(ActivityId);
                var c = (from x in ALS.getCarestaffByTime(1, _time, _activityid, CarestaffId, Recurrence, _clientdt, _isonetime) select new SelectData2 { Name = x.user_id + " | " + x.carestaff, Id = x.user_id }).ToList();
                return Json(c);
            }

        }

        [AllowAnonymous]
        [HttpGet]
        public dynamic GetResidentsWithComment(string AgencyId) // working
        {

            using (var ALSDB = ASLClientBaseContext(AgencyId))
            {
                try
                {
                  
                    var data = (from gcom in ALSDB.GuestComments
                                join res in ALSDB.Residents on gcom.resident_id equals res.resident_id
                                select new
                                {
                                    ResidentId = gcom.resident_id,
                                    Name = res.first_name + " " + res.last_name,
                                    Unread_Count = ALSDB.GuestComments.Where(x => x.resident_id == gcom.resident_id).Sum(x => x.unread_count)
                                }).Distinct().ToList();
                    return Json(data);
                }
                catch (Exception ex)
                {
                }
                return Json("");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public dynamic GetResidentsTaskList(string AgencyId,string ResidentId)
        {
            using (var ALSDB = ASLClientBaseContext(AgencyId))
            {
                try
                {
                    int? _residentid = Convert.ToInt32(ResidentId);
                    var data = (from gcom in ALSDB.GuestComments
                                where gcom.resident_id == _residentid
                                select new
                                {
                                   DateCreated = gcom.date_created,
                                    ResidentId = gcom.resident_id,
                                    CID = gcom.cid,
                                    TaskDetail=  gcom.taskdetail,
                                    Unread_Count = gcom.unread_count
                                }).Distinct().ToList();
                    return Json(data);
                }
                catch (Exception ex)
                {
                }
                return Json("");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public dynamic GetConversation(string AgencyId, string ResidentId,string cid)
        {
            using (var ALSDB = ASLClientBaseContext(AgencyId))
            {
              
                try
                {
                    int? _residentid = Convert.ToInt32(ResidentId);
                    var data = ALSDB.GuestComments.SingleOrDefault(x => x.resident_id == _residentid && x.cid == cid); 

                    data.read_status = "read";//update status to read
                    data.unread_count = 0;

                    ALSDB.SubmitChanges();

                    string conversation = ( data == null ? "" : data.comment);

                    return Json(new {
                        rows = conversation
                    });
                }
                catch (Exception ex)
                {
                }
                return Json("");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public dynamic HandleComment([DynamicJson(MatchName = false)] dynamic obj)
        {
            string AgencyId = (string)obj.AgencyId;
            string UserId = (string)obj.UserId;
            string RoleId = (string)obj.RoleId;
            //int? ActivityScheduleId = ((string)obj.Id).TryParseNullableInt();

            //mode --> 0 - get comments , 1 - add comment

            using (var ALSDB = ASLClientBaseContext(AgencyId))
            {
                //var ind = "";
                //var data_null = true;

                try
                {
                    int? resident_id = ((string)obj.resident_id).TryParseNullableInt();
                    int? guest_notification_id = ((string)obj.guest_notification_id).TryParseNullableInt();
                    string cid = obj.cid.ToString();

                    //cid is to determine the certain task in a certain day in which the comment was added

                    GuestComment gc = null;
                    var data = (from g in ALSDB.GuestComments
                                where g.cid == cid
                                && g.resident_id == resident_id
                                select new
                                {
                                    g.id,
                                    g.comment
                                }).SingleOrDefault();

                    var role = (from g in ALSDB.ALCRoles
                                where g.role_id == Convert.ToInt32(RoleId)
                                select new
                                {
                                    g.description
                                }).SingleOrDefault();

                    var comment_object = new
                    {
                        comment = obj.comment.comment,
                        datetime = obj.comment.datetime,
                        role = role.description,
                        userid = UserId
                    };

                    string json_comment = JsonConvert.SerializeObject(comment_object);

                    //taskdetail is to be parsed on the admin side to determine the task that the comment was added
                    string json_task = JsonConvert.SerializeObject(obj.taskdetail);

                    if (data != null)
                    {

                        if (obj.mode == 0)
                        {
                            return Json(data);
                        }
                        else
                        {
                            gc = ALSDB.GuestComments.SingleOrDefault(x => x.id == data.id);
                            gc.comment = (gc.comment + "," + json_comment);
                            gc.read_status = "unread";
                            gc.unread_count += 1;
                            if (json_task != "null")
                            {
                                gc.taskdetail = json_task;
                            }
                            else
                            {
                                gc.read_status = "read";
                                gc.unread_count = 0;
                            }

                        }
                    }
                    else
                    {
                        //data_null = false;
                        if (obj.mode == 1)
                        {

                            gc = new GuestComment();

                            gc.comment = json_comment;
                            gc.resident_id = Convert.ToInt32(obj.resident_id);
                            gc.cid = Convert.ToString(obj.cid);
                            gc.read_status = "unread";
                            gc.date_created = DateTime.Now;
                            gc.unread_count = 1;
                            if (json_task != "null")
                            {
                                gc.taskdetail = json_task;
                            }

                            if (obj.guest_notification_id == "0")
                            {
                                var guestData = (from g in ALSDB.GuestNotifications
                                                 where g.resident_id == resident_id
                                                 select new
                                                 {
                                                     g.id,
                                                 }).SingleOrDefault();

                                gc.guest_notification_id = Convert.ToInt32(guestData.id);
                            }
                            else
                            {
                                gc.guest_notification_id = Convert.ToInt32(obj.guest_notification_id);
                            }


                            ALSDB.GuestComments.InsertOnSubmit(gc);

                        }
                    }


                    ALSDB.SubmitChanges();

                    //Agency a = ALSDB.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                    //var agency_name = a.agency_name;
                    //var agency_email = a.agency_email;

                    //if (obj.mode == 1)
                    //{
                    //    MailMessage mail = new MailMessage();

                    //    mail.To.Add(agency_email);
                    //    mail.From = new MailAddress("dslalc@datasoftlogic.com");
                    //    mail.Subject = "Comment for " + obj.resident_name + " of " + obj.room;
                    //    mail.Body = "A comment has been added for the task below: <br><br>";
                    //    mail.Body += obj.comment_email;
                    //    mail.Body += "<br><br>This is also reflected on the Daily Schedule module.<br><br>Thank you.";

                    //    mail.IsBodyHtml = true;
                    //    SmtpClient smtp = new SmtpClient();
                    //    smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                    //                                  //smtp.Host = "10.10.1.149"; //Or Your SMTP Server Address
                    //    smtp.Credentials = new System.Net.NetworkCredential
                    //         ("dslalc@datasoftlogic.com", "dslalciloilo17"); // ***use valid credentials***
                    //    smtp.Port = 587;
                    //    //smtp.Port = 25;

                    //    //Or your Smtp Email ID and Password
                    //    smtp.EnableSsl = true;
                    //    smtp.Send(mail);
                    //}

                    string conversation = (gc == null ? "" : gc.comment);
                    var context = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<signalr.hubs.ChatHub>();
                    context.Clients.All.addNewMessageToPage("#message", "xxxxxxxxxxxxxxx", gc.cid);
                    return Json(new
                    {
                        rows = conversation
                    });
                    //return Json(gc);

                }
                catch (Exception E)
                {
                    return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
                }
            }
        }
        [AllowAnonymous]
        [HttpPost]
        public dynamic PostStaffCompletionInfo([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                int? csa_id = ((string)obj.CarestaffActivityId).TryParseNullableInt();
                int? as_id = ((string)obj.ActivityScheduleId).TryParseNullableInt();
                string AgencyId = (string)obj.AgencyId;
                string UserID = (string)obj.UserID;
                string remarks = (string)obj.Remarks;
                int? status = ((string)obj.Status).TryParseNullableInt();
                DateTime? compDate = ((string)obj.CompletionDate).TryParseNullableDate();

                CarestaffActivity cs = null;
                using (var ALSDB = ASLClientBaseContext(AgencyId))
                {
                    if (csa_id == null)
                    {
                        cs = new CarestaffActivity();
                        ALSDB.CarestaffActivities.InsertOnSubmit(cs);
                    }
                    else
                        cs = ALSDB.CarestaffActivities.SingleOrDefault(x => x.carestaff_activity_id == csa_id.Value);

                    cs.activity_schedule_id = as_id ?? 0;
                    cs.completion_date = compDate;
                    cs.carestaff_id = UserID;
                    cs.remarks = remarks;
                    //1= acknowledge 2=Overdue
                    cs.activity_status = status;



                    ALSDB.SubmitChanges();
                }
            }
            catch (Exception E)
            {
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();
        }
        [AllowAnonymous]
        [HttpPost]
        public dynamic PostSignedStatus([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                string AgencyId = (string)obj.AgencyId;
                string UserID = (string)obj.UserID;
                string signedby = (string)obj.SignedBy;
                int? rep_id = ((string)obj.RepId).TryParseNullableInt();
                string rep_name = (string)obj.RepName;
                string rep_parent = (string)obj.RepParent;
                string index = (string)obj.Index;
                string img_str = (string)obj.ImgStr;
                byte[] imgdata = Convert.FromBase64String(img_str);
                DateTime? signdate = ((string)obj.SignedDate).TryParseNullableDate();
                //save
                using (var ALS = ASLClientBaseContext(AgencyId))
                {
                    if (index == "AA")
                    {
                        AdmissionAgreement aa = ALS.AdmissionAgreements.SingleOrDefault(x => x.admission_agreement_id == rep_id);
                        aa.isSigned = true;
                        aa.SignedBy = signedby;
                        aa.SignedDate = signdate;
                        aa.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "ANS")
                    {
                        AppraisalNeedAndService ans = ALS.AppraisalNeedAndServices.SingleOrDefault(x => x.appraisal_id == rep_id);
                        ans.isSigned = true;
                        ans.SignedBy = signedby;
                        ans.SignedDate = signdate;
                        ans.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "DC")
                    {
                        DocumentChecklist dc = ALS.DocumentChecklists.SingleOrDefault(x => x.checklist_id == rep_id);
                        dc.isSigned = true;
                        dc.SignedBy = signedby;
                        dc.SignedDate = signdate;
                        dc.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "MI")
                    {
                        ResidentMoveIn mi = ALS.ResidentMoveIns.SingleOrDefault(x => x.movein_id == rep_id);
                        mi.isSigned = true;
                        mi.SignedBy = signedby;
                        mi.SignedDate = signdate;
                        mi.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "PR")
                    {
                        PhysicianReport pr = ALS.PhysicianReports.SingleOrDefault(x => x.admission_id == rep_id);
                        pr.isSigned = true;
                        pr.SignedBy = signedby;
                        pr.SignedDate = signdate;
                        pr.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "IEI")
                    {
                        ResidentIdentificationEmergencyInfo iei = ALS.ResidentIdentificationEmergencyInfos.SingleOrDefault(x => x.admission_id == rep_id);
                        iei.isSigned = true;
                        iei.SignedBy = signedby;
                        iei.SignedDate = signdate;
                        iei.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "PAI")
                    {
                        PreplacementAppraisalInfo pai = ALS.PreplacementAppraisalInfos.SingleOrDefault(x => x.admission_id == rep_id);
                        pai.isSigned = true;
                        pai.SignedBy = signedby;
                        pai.SignedDate = signdate;
                        pai.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "RMI")
                    {
                        ResidentReleaseMedicalInfo rmi = ALS.ResidentReleaseMedicalInfos.SingleOrDefault(x => x.admission_id == rep_id);
                        rmi.isSigned = true;
                        rmi.SignedBy = signedby;
                        rmi.SignedDate = signdate;
                        rmi.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "RA")
                    {
                        ResidentAppraisal ra = ALS.ResidentAppraisals.SingleOrDefault(x => x.admission_id == rep_id);
                        ra.isSigned = true;
                        ra.SignedBy = signedby;
                        ra.SignedDate = signdate;
                        ra.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "TDN")
                    {
                        TelecommunicationNotif tdn = ALS.TelecommunicationNotifs.SingleOrDefault(x => x.admission_id == rep_id);
                        tdn.isSigned = true;
                        tdn.SignedBy = signedby;
                        tdn.SignedDate = signdate;
                        tdn.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "PER")
                    {
                        PersonalRight per = ALS.PersonalRights.SingleOrDefault(x => x.admission_id == rep_id);
                        per.isSigned = true;
                        per.SignedBy = signedby;
                        per.SignedDate = signdate;
                        per.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "DR")
                    {

                        DecisionRightsESign dr = ALS.DecisionRightsESigns.SingleOrDefault(x => x.admission_id == rep_id);
                        if (dr == null)
                        {
                            dr = new DecisionRightsESign();
                            Admission aa = ALS.Admissions.FirstOrDefault(x => x.admission_id == rep_id);
                            dr.resident_id = aa.resident_id;
                            dr.admission_id = rep_id;
                            ALS.DecisionRightsESigns.InsertOnSubmit(dr);
                        }
                        dr.isSigned = true;
                        dr.SignedBy = signedby;
                        dr.SignedDate = signdate;
                        dr.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "PC")
                    {

                        PhotographyConsentESign pc = ALS.PhotographyConsentESigns.SingleOrDefault(x => x.admission_id == rep_id);
                        if (pc == null)
                        {
                            pc = new PhotographyConsentESign();
                            Admission aa = ALS.Admissions.FirstOrDefault(x => x.admission_id == rep_id);
                            pc.resident_id = aa.resident_id;
                            pc.admission_id = rep_id;
                            ALS.PhotographyConsentESigns.InsertOnSubmit(pc);
                        }
                        pc.isSigned = true;
                        pc.SignedBy = signedby;
                        pc.SignedDate = signdate;
                        pc.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "PFC")
                    {
                        PersonnelFileChecklist pfc = ALS.PersonnelFileChecklists.SingleOrDefault(x => x.personnelfilelist_id == rep_id);
                        pfc.isSigned = true;
                        pfc.SignedBy = signedby;
                        pfc.SignedDate = signdate;
                        pfc.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "HS")
                    {
                        HealthScreeningReport hs = ALS.HealthScreeningReports.SingleOrDefault(x => x.healthscreenrep_id == rep_id);
                        hs.isSigned = true;
                        hs.SignedBy = signedby;
                        hs.SignedDate = signdate;
                        hs.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "CME")
                    {
                        ConsentMedicalExam hs = ALS.ConsentMedicalExams.SingleOrDefault(x => x.admission_id == rep_id);
                        hs.isSigned = true;
                        hs.SignedBy = signedby;
                        hs.SignedDate = signdate;
                        hs.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "ER")
                    {

                        EmployeeRightsESign er = ALS.EmployeeRightsESigns.SingleOrDefault(x => x.employeerights_id == rep_id);
                        if (er == null)
                        {
                            er = new EmployeeRightsESign();
                            er.user_id = rep_parent;
                            ALS.EmployeeRightsESigns.InsertOnSubmit(er);
                        }
                        er.isSigned = true;
                        er.SignedBy = signedby;
                        er.SignedDate = signdate;
                        er.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "EPC")
                    {

                        EmpPhotographyConsentESign epc = ALS.EmpPhotographyConsentESigns.SingleOrDefault(x => x.empphotographyconsent_id == rep_id);
                        if (epc == null)
                        {
                            epc = new EmpPhotographyConsentESign();
                            epc.user_id = rep_parent;
                            ALS.EmpPhotographyConsentESigns.InsertOnSubmit(epc);
                        }
                        epc.isSigned = true;
                        epc.SignedBy = signedby;
                        epc.SignedDate = signdate;
                        epc.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "EAA")
                    {

                        ElderAbuseAcknowledgementESign eaa = ALS.ElderAbuseAcknowledgementESigns.SingleOrDefault(x => x.elderlyabuse_id == rep_id);
                        if (eaa == null)
                        {
                            eaa = new ElderAbuseAcknowledgementESign();
                            eaa.user_id = rep_parent;
                            ALS.ElderAbuseAcknowledgementESigns.InsertOnSubmit(eaa);
                        }
                        eaa.isSigned = true;
                        eaa.SignedBy = signedby;
                        eaa.SignedDate = signdate;
                        eaa.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "HEP")
                    {

                        HepatitisFormESign hep = ALS.HepatitisFormESigns.SingleOrDefault(x => x.hepatitisform_id == rep_id);
                        if (hep == null)
                        {
                            hep = new HepatitisFormESign();
                            hep.user_id = rep_parent;
                            ALS.HepatitisFormESigns.InsertOnSubmit(hep);
                        }
                        hep.isSigned = true;
                        hep.SignedBy = signedby;
                        hep.SignedDate = signdate;
                        hep.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }

                    else if (index == "EHR")
                    {

                        EmployeeHandbookReceiptESign ehr = ALS.EmployeeHandbookReceiptESigns.SingleOrDefault(x => x.employeehandbookreceipt_id == rep_id);
                        if (ehr == null)
                        {
                            ehr = new EmployeeHandbookReceiptESign();
                            ehr.user_id = rep_parent;
                            ALS.EmployeeHandbookReceiptESigns.InsertOnSubmit(ehr);
                        }
                        ehr.isSigned = true;
                        ehr.SignedBy = signedby;
                        ehr.SignedDate = signdate;
                        ehr.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }

                    else if (index == "CSR")
                    {
                        CriminalRecordStatement csr = ALS.CriminalRecordStatements.SingleOrDefault(x => x.criminal_record_statement_id == rep_id);
                        csr.isSigned = true;
                        csr.SignedBy = signedby;
                        csr.SignedDate = signdate;
                        csr.ESignature = imgdata;
                        ALS.SubmitChanges();
                    }
                    else if (index == "ASS")
                    {
                        Assessment mi = ALS.Assessments.SingleOrDefault(x => x.AssessmentId == rep_id);
                    }
                    else if (index == "MFS")
                    {
                        Assessment mi = ALS.Assessments.SingleOrDefault(x => x.AssessmentId == rep_id);
                    }
                }

            }
            catch (Exception E)
            {
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();
        }
        [AllowAnonymous]
        [HttpPost]
        public dynamic PostReSchedule([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                string AgencyId = (string)obj.AgencyId;
                string UserID = (string)obj.UserID;
                string CarestaffId = (string)obj.CarestaffId;
                int? ResidentId = ((string)obj.ResidentId).TryParseNullableInt();
                int? RoomId = ((string)obj.RoomId).TryParseNullableInt();
                int? ActivityScheduleId = ((string)obj.Id).TryParseNullableInt();
                int? ActivityId = ((string)obj.ActivityId).TryParseNullableInt();
                string Recurrence = (string)obj.Recurrence;
                bool? isonetime = ((string)obj.isonetime).TryParseNullableBool();
                DateTime? clientdt = ((string)obj.clientdt).TryParseNullableDate();
                string schedTime = (string)obj.ScheduleTime;
                DateTime startTime;
                if (isonetime != null && isonetime.Value)
                    DateTime.TryParseExact(schedTime, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out startTime);
                else
                    DateTime.TryParseExact(schedTime, "hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out startTime);
                DateTime? ScheduleTime = startTime.ToUniversalTime();

                ActivitySchedule cs = null;
                ActivitySchedule originalCS = null;
                string rescheddate = ToTimezonedDateTime(ScheduleTime, AgencyId).Value.ToString("MM/dd/yyyy hh:mm tt");
                using (var ALSDB = ASLClientBaseContext(AgencyId))
                {
                    using (var tran = new System.Transactions.TransactionScope())
                    {
                        var exists = ALSDB.hasExistingScheduleByRecurrence(ScheduleTime, ActivityId, ActivityScheduleId, ResidentId, RoomId, Recurrence, clientdt, isonetime).SingleOrDefault();
                        if (exists != null && !string.IsNullOrEmpty(exists.Conflict))
                        {
                            string msg = exists.Conflict;
                            return new JsonResponse(JsonResponseResult.Exists, msg);
                        }


                        cs = new ActivitySchedule();
                        cs.created_by = UserID;
                        cs.date_created = DateTime.UtcNow;

                        cs.resident_id = ResidentId;
                        cs.room_id = RoomId;
                        if (!string.IsNullOrEmpty(CarestaffId))
                            cs.carestaff_id = CarestaffId;
                        else
                            cs.carestaff_id = null;
                        cs.activity_id = ActivityId ?? 0;
                        cs.start_time = ScheduleTime;

                        if (isonetime != null && isonetime.Value)
                            cs.is_onetime = true;
                        else
                            cs.is_onetime = null;

                        cs.recurrence = Recurrence;
                        ALSDB.ActivitySchedules.InsertOnSubmit(cs);
                        ALSDB.SubmitChanges();

                        int Id = Convert.ToInt32(obj.Id);
                        DateTime? compDate = ((string)obj.completion).TryParseNullableDate();

                        originalCS = ALSDB.ActivitySchedules.SingleOrDefault(x => x.activity_schedule_id == Id);
                        if (originalCS != null)
                        {
                            originalCS.xref = cs.activity_schedule_id;

                            var csa = new CarestaffActivity();
                            csa.activity_schedule_id = Id;
                            csa.completion_date = compDate;
                            csa.carestaff_id = UserID;
                            csa.remarks = "Rescheduled - " + rescheddate;
                            csa.activity_status = 1;

                            ALSDB.CarestaffActivities.InsertOnSubmit(csa);
                            ALSDB.SubmitChanges();
                        }
                        tran.Complete();
                    }
                }
            }
            catch (Exception E)
            {
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();

        }
        private ALSDataContext ASLClientBaseContext(string AgencyId)
        {
            string connection = "";

            using (var cx = new ALSMasterDataContext())
            {
                var agency = cx.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                connection = agency.conn_string;
            }
            return new ALSDataContext(connection);
        }
        private DateTime? ToTimezonedDateTime(DateTime? d, string AgencyId)
        {
            if (d == null || (!d.HasValue)) return null;

            DateTime? retdate = d;
            using (var cx = new ALSMasterDataContext())
            {
                try
                {
                    var agency = (from x in cx.AgencyLists where x.agency_id == AgencyId select x).SingleOrDefault();

                    var tz = TimeZoneInfo.FindSystemTimeZoneById(agency.timezone_id);
                    if (tz != null)
                    {
                        var utcOffset = new DateTimeOffset(retdate.Value.ToUniversalTime(), TimeSpan.Zero);

                        return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).DateTime;
                    }
                }
                catch
                {
                    return retdate;
                }
            }
            return retdate;

        }

        private DateTime? ToTimezonedDateTime(TimeSpan? t, string AgencyId)
        {
            if (t == null || (!t.HasValue)) return null;
            var now = DateTime.Now;
            DateTime? retdate = new DateTime(now.Year, now.Month, now.Day, t.Value.Hours, t.Value.Minutes, t.Value.Seconds);
            using (var cx = new ALSMasterDataContext())
            {
                try
                {
                    var agency = (from x in cx.AgencyLists where x.agency_id == AgencyId select x).SingleOrDefault();

                    var tz = TimeZoneInfo.FindSystemTimeZoneById(agency.timezone_id);
                    if (tz != null)
                    {
                        var utcOffset = new DateTimeOffset(retdate.Value.ToUniversalTime(), TimeSpan.Zero);

                        return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).DateTime;
                    }
                }
                catch
                {
                    return retdate;
                }
            }
            return retdate;

        }

        private void createDC(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                DocumentChecklist dc = ALS.DocumentChecklists.SingleOrDefault(x => x.checklist_id == repid);
                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == dc.admission_id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);

                if (ad == null) ad = new Admission();
                if (r == null) r = new Resident();
                if (rm == null) rm = new Room();
                if (dc == null) dc = new DocumentChecklist();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in dc.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(dc, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToShortDateString() : val.ToString();
                    }
                    dcval[prop.Name] = actualval;
                }

                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/dc_template.htm"), Encoding.Unicode);

                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                if (dc.isSigned == true)
                {
                    byte[] imgdata = dc.ESignature.ToArray();
                    document.Add(new Paragraph("\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);
                    jpg.Alignment = Element.ALIGN_LEFT;
                    jpg.Border = PdfPCell.BOTTOM_BORDER;
                    jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    Paragraph c1 = new Paragraph(dc.SignedBy);
                    c1.IndentationLeft = 30f;
                    document.Add(c1);
                }

                document.Close();
                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_DocumentChecklist_" + dc.checklist_id.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();

            }

        }
        private void createPR(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {

                PhysicianReport pr = ALS.PhysicianReports.SingleOrDefault(x => x.admission_id == repid);

                if (pr == null) pr = new PhysicianReport();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in pr.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(pr, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;
                }

                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);

                writer.PageEvent = new PDFFooter() { form = "pr_template" };

                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/pr_template.htm"), Encoding.Unicode);


                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);



                List<IElement> parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (IElement htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (pr.isSigned == true)
                {
                    byte[] imgdata = pr.ESignature.ToArray();
                    document.Add(new Paragraph("\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);
                    jpg.ScaleToFit(140f, 120f);
                    jpg.SetAbsolutePosition(130, 430);

                    //jpg.Alignment = Element.ALIGN_LEFT;
                    //jpg.Border = PdfPCell.BOTTOM_BORDER;
                    //jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    //Paragraph c1 = new Paragraph(pr.SignedBy);
                    //c1.IndentationLeft = 30f;
                    //document.Add(c1);
                }
                //======
                document.Close();
                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_PhysicianReport_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();

            }
        }
        private void createANS(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var appraisalId = repid;

                AppraisalNeedAndService dc = ALS.AppraisalNeedAndServices.SingleOrDefault(x => x.appraisal_id == appraisalId);
                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == dc.admission_id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);

                if (ad == null) ad = new Admission();
                if (r == null) r = new Resident();
                if (rm == null) rm = new Room();
                if (dc == null) dc = new AppraisalNeedAndService();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in dc.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(dc, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/ans_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "DOB")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (r.birth_date.HasValue) ? Utility.GetDateFormattedString(r.birth_date, "MM/dd/yyyy") : "");
                    }
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }
                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (dc.isSigned == true)
                {
                    byte[] imgdata = dc.ESignature.ToArray();
                    document.Add(new Paragraph("\n\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);
                    jpg.Alignment = Element.ALIGN_LEFT;
                    jpg.Border = PdfPCell.BOTTOM_BORDER;
                    jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    Paragraph c1 = new Paragraph(dc.SignedBy);
                    c1.IndentationLeft = 30f;
                    document.Add(c1);
                }
                //======
                document.Close();
                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_AppraisalNeedAndServicePlan_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }
        }
        private void createIEI(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                ResidentIdentificationEmergencyInfo rie = ALS.ResidentIdentificationEmergencyInfos.SingleOrDefault(x => x.admission_id == _id);

                if (rie == null) rie = new ResidentIdentificationEmergencyInfo();
                if (ad == null) ad = new Admission();
                if (r == null) r = new Resident();

                Dictionary<string, string> dcval = new Dictionary<string, string>();
                Dictionary<string, string> dcval2 = new Dictionary<string, string>();


                foreach (var prop in rie.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(rie, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;
                }

                foreach (var prop in r.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(r, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval2[prop.Name] = actualval;
                }

                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);

                writer.PageEvent = new PDFFooter() { form = "rie_template" };

                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/rie_template.htm"), Encoding.Unicode);

                string admission_date, discharge_date;
                DateTime s = new DateTime();
                admission_date = (DateTime.TryParse(ad.admission_date.ToString(), out s)) ? s.ToString("MMMM dd, yyyy") : ad.admission_date.ToString();
                DateTime q = new DateTime();
                discharge_date = (DateTime.TryParse(ad.discharge_date.ToString(), out q)) ? q.ToString("MMMM dd, yyyy") : ad.discharge_date.ToString();

                contents = contents.Replace("[admission_date]", admission_date);
                contents = contents.Replace("[discharge_date]", discharge_date);
                contents = contents.Replace("[physician]", ad.physician);
                contents = contents.Replace("[physician_phone]", ad.physician_phone);


                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                foreach (KeyValuePair<string, string> eve in dcval2)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);


                //Literal css = new Literal();


                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (rie.isSigned == true)
                {
                    byte[] imgdata = rie.ESignature.ToArray();
                    //document.Add(new Paragraph("\n\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);
                    jpg.SetAbsolutePosition(160, 190);
                    //jpg.Alignment = Element.ALIGN_LEFT;
                    //jpg.Border = PdfPCell.BOTTOM_BORDER;
                    //jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    //Paragraph c1 = new Paragraph(rie.SignedBy);
                    //c1.IndentationLeft = 30f;
                    //document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_ResidentIdentificationEmergencyInfo_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();

            }

        }
        private void createPAI(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);
                PreplacementAppraisalInfo pa = ALS.PreplacementAppraisalInfos.SingleOrDefault(x => x.admission_id == _id);

                if (ad == null) ad = new Admission();
                if (r == null) r = new Resident();
                if (rm == null) rm = new Room();
                if (pa == null) pa = new PreplacementAppraisalInfo();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in pa.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(pa, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                writer.PageEvent = new PDFFooter() { form = "pai_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/pai_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }
                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (pa.isSigned == true)
                {
                    byte[] imgdata = pa.ESignature.ToArray();

                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);

                    jpg.SetAbsolutePosition(160, 640);
                    //jpg.Alignment = Element.ALIGN_LEFT;
                    //jpg.Border = PdfPCell.BOTTOM_BORDER;
                    //jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    //Paragraph c1 = new Paragraph(pa.SignedBy);
                    //c1.IndentationLeft = 30f;
                    //document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_PreplacementAppraisalInfo_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createRMI(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                ResidentReleaseMedicalInfo rmi = ALS.ResidentReleaseMedicalInfos.SingleOrDefault(x => x.admission_id == _id);

                if (rmi == null) rmi = new ResidentReleaseMedicalInfo();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in rmi.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(rmi, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;
                }

                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                writer.PageEvent = new PDFFooter() { form = "romi_template" };
                document.Open();

                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/romi_template.htm"), Encoding.Unicode);


                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);




                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (rmi.isSigned == true)
                {
                    byte[] imgdata = rmi.ESignature.ToArray();
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);
                    jpg.ScaleToFit(140f, 120f);
                    jpg.SetAbsolutePosition(330, 350);

                    //jpg.Alignment = Element.ALIGN_LEFT;
                    //jpg.Border = PdfPCell.BOTTOM_BORDER;
                    //jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    //Paragraph c1 = new Paragraph(rmi.SignedBy);
                    //c1.IndentationLeft = 30f;
                    //document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());
                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_ResidentReleaseMedicalInfo_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();

            }

        }
        private void createRA(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);
                ResidentAppraisal ra = ALS.ResidentAppraisals.SingleOrDefault(x => x.admission_id == _id);

                if (ad == null) ad = new Admission();
                if (r == null) r = new Resident();
                if (rm == null) rm = new Room();
                if (ra == null) ra = new ResidentAppraisal();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in ra.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(ra, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                writer.PageEvent = new PDFFooter() { form = "ra_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/ra_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }
                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);


                //======ESIGN BY ARJUN V
                if (ra.isSigned == true)
                {
                    byte[] imgdata = ra.ESignature.ToArray();
                    document.Add(new Paragraph("\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);
                    jpg.ScaleToFit(140f, 120f);
                    jpg.SetAbsolutePosition(190, 620);

                    //jpg.Alignment = Element.ALIGN_LEFT;
                    //jpg.Border = PdfPCell.BOTTOM_BORDER;
                    //jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    //Paragraph c1 = new Paragraph(ra.SignedBy);
                    //c1.IndentationLeft = 30f;
                    //document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_ResidentAppraisal_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createMI(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                ResidentMoveIn dc = ALS.ResidentMoveIns.SingleOrDefault(x => x.movein_id == _id);
                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == dc.admission_id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);

                if (ad == null) ad = new Admission();
                if (r == null) r = new Resident();
                if (rm == null) rm = new Room();
                if (dc == null) dc = new ResidentMoveIn();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in dc.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(dc, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToShortDateString() : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/rmi_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }
                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (dc.isSigned == true)
                {
                    byte[] imgdata = dc.ESignature.ToArray();
                    document.Add(new Paragraph("\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);
                    jpg.Alignment = Element.ALIGN_LEFT;
                    jpg.Border = PdfPCell.BOTTOM_BORDER;
                    jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    Paragraph c1 = new Paragraph(dc.SignedBy);
                    c1.IndentationLeft = 30f;
                    document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_MoveIn_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createAA(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                AdmissionAgreement dc = ALS.AdmissionAgreements.SingleOrDefault(x => x.admission_agreement_id == _id);
                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == dc.admission_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);

                if (ad == null) ad = new Admission();
                if (r == null) r = new Resident();
                if (rm == null) rm = new Room();
                if (dc == null) dc = new AdmissionAgreement();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in dc.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(dc, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToShortDateString() : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/aa_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "admission_date")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (ad.admission_date.HasValue) ? Utility.GetDateFormattedString(ad.admission_date, "MM/dd/yyyy") : "");
                    }
                    else if (eve.Key == "SSNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(r.SSN)) ? r.SSN.Trim() : "");
                    }
                    else if (eve.Key == "DOB")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (r.birth_date.HasValue) ? Utility.GetDateFormattedString(r.birth_date, "MM/dd/yyyy") : "");
                    }
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (dc.isSigned == true)
                {
                    byte[] imgdata = dc.ESignature.ToArray();
                    document.Add(new Paragraph("\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);
                    jpg.Alignment = Element.ALIGN_LEFT;
                    jpg.Border = PdfPCell.BOTTOM_BORDER;
                    jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    Paragraph c1 = new Paragraph(dc.SignedBy);
                    c1.IndentationLeft = 30f;
                    document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_AdmissionAgreement_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createTDN(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);
                TelecommunicationNotif tn = ALS.TelecommunicationNotifs.SingleOrDefault(x => x.admission_id == _id);

                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                if (ad == null) ad = new Admission();
                if (r == null) r = new Resident();
                if (rm == null) rm = new Room();
                if (tn == null) tn = new TelecommunicationNotif();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in tn.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(tn, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                //writer.PageEvent = new PDFFooter() { form = "pai_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/tdn_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }
                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);
                contents = contents.Replace("[FacilityName]", agency.agency_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //======ESIGN BY ARJUN V
                if (tn.isSigned == true)
                {
                    byte[] imgdata = tn.ESignature.ToArray();

                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);

                    jpg.SetAbsolutePosition(120, 300);
                    //jpg.Alignment = Element.ALIGN_LEFT;
                    //jpg.Border = PdfPCell.BOTTOM_BORDER;
                    //jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    //tnragraph c1 = new tnragraph(tn.SignedBy);
                    //c1.IndentationLeft = 30f;
                    //document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_TelecommunicationDeviceNotification_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createPER(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);
                PersonalRight per = ALS.PersonalRights.SingleOrDefault(x => x.admission_id == _id);

                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                if (per == null) per = new PersonalRight();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in per.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(per, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                writer.PageEvent = new PDFFooter() { form = "per_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/per_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }
                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityAddress]", a.address_1);
                contents = contents.Replace("[FacilityCity]", a.city);
                contents = contents.Replace("[FacilityState]", a.state);
                //contents = contents.Replace("[FacilityZip]", a.zip);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //======ESIGN BY ARJUN V
                if (per.isSigned == true)
                {
                    byte[] imgdata = per.ESignature.ToArray();
                    //document.Add(new Paragraph("\n\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);
                    jpg.SetAbsolutePosition(60, 270);
                    //jpg.Alignment = Element.ALIGN_LEFT;
                    //jpg.Border = PdfPCell.BOTTOM_BORDER;
                    //jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    //Paragraph c1 = new Paragraph(per.SignedBy);
                    //c1.IndentationLeft = 30f;
                    //document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_PersonalRights_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createDR(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);
                DecisionRightsESign dr = ALS.DecisionRightsESigns.SingleOrDefault(x => x.admission_id == ad.admission_id);
                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;

                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                //writer.PageEvent = new PDFFooter() { form = "dr_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/dr_template.htm"), Encoding.Unicode);

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityName2]", agency.agency_name.ToUpper());
                contents = contents.Replace("[FacilityAddress]", a.address_1);
                contents = contents.Replace("[FacilityCity]", a.city);
                contents = contents.Replace("[FacilityState]", a.state);
                contents = contents.Replace("[FacilityZip]", a.zip);
                contents = contents.Replace("[FacilityPhone]", a.phone);
                contents = contents.Replace("[FacilityFax]", a.fax);
                contents = contents.Replace("[SignedDate]", (dr != null) ? dr.SignedDate.Value.ToString("MMMM dd, yyyy") : "");//Sign Date

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //======ESIGN BY ARJUN V
                if (dr != null)
                {
                    if (dr.isSigned == true)
                    {
                        byte[] imgdata = dr.ESignature.ToArray();
                        //document.Add(new Paragraph("\n\n\n"));
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                        jpg.ScaleToFit(140f, 120f);
                        jpg.SetAbsolutePosition(130, 80);
                        //jpg.Alignment = Element.ALIGN_LEFT;
                        //jpg.Border = PdfPCell.BOTTOM_BORDER;
                        //jpg.BorderWidth = 1f;
                        document.Add(jpg);
                        //Paragraph c1 = new Paragraph(per.SignedBy);
                        //c1.IndentationLeft = 30f;
                        //document.Add(c1);
                    }
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_RightToMakeDecision_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createPC(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);

                PhotographyConsentESign pc = ALS.PhotographyConsentESigns.SingleOrDefault(x => x.admission_id == ad.admission_id);
                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                //writer.PageEvent = new PDFFooter() { form = "dr_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/pc_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityAddress]", a.address_1);
                contents = contents.Replace("[FacilityCity]", a.city);
                contents = contents.Replace("[FacilityState]", a.state);
                contents = contents.Replace("[FacilityZip]", a.zip);
                contents = contents.Replace("[FacilityPhone]", a.phone);
                contents = contents.Replace("[FacilityFax]", a.fax);

                contents = contents.Replace("[SignedDate]", (pc != null) ? pc.SignedDate.Value.ToString("MMM dd, yyyy") : "");//Sign Date
                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //======ESIGN BY ARJUN V
                if (pc != null)
                {
                    if (pc.isSigned == true)
                    {
                        byte[] imgdata = pc.ESignature.ToArray();
                        //document.Add(new Paragraph("\n\n\n"));
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                        jpg.ScaleToFit(140f, 120f);
                        jpg.SetAbsolutePosition(150, 210);
                        //jpg.Alignment = Element.ALIGN_LEFT;
                        //jpg.Border = PdfPCell.BOTTOM_BORDER;
                        //jpg.BorderWidth = 1f;
                        document.Add(jpg);
                        //Paragraph c1 = new Paragraph(per.SignedBy);
                        //c1.IndentationLeft = 30f;
                        //document.Add(c1);
                    }
                }
                //======
                document.Close();


                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_PhotographyConsent_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createPFC(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                //Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                //Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                //Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);
                PersonnelFileChecklist pfc = ALS.PersonnelFileChecklists.SingleOrDefault(x => x.personnelfilelist_id == _id);

                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                if (pfc == null) pfc = new PersonnelFileChecklist();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in pfc.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(pfc, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                // writer.PageEvent = new PDFFooter() { form = "pfc_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/pfc_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        //contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }
                contents = contents.Replace("[PFCFacilityName]", agency.agency_name);
                //contents = contents.Replace("[FirstName]", r.first_name);
                //string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                //contents = contents.Replace("[MiddleInitial]", mi);
                //contents = contents.Replace("[LastName]", r.last_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (pfc.isSigned == true)
                {
                    byte[] imgdata = pfc.ESignature.ToArray();
                    document.Add(new Paragraph("\n\n"));
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                    jpg.ScaleToFit(140f, 120f);
                    jpg.Alignment = Element.ALIGN_LEFT;
                    jpg.Border = PdfPCell.BOTTOM_BORDER;
                    jpg.BorderWidth = 1f;
                    document.Add(jpg);
                    Paragraph c1 = new Paragraph(pfc.SignedBy);
                    c1.IndentationLeft = 30f;
                    document.Add(c1);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_PersonnelFileChecklist_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createHS(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                HealthScreeningReport hs = ALS.HealthScreeningReports.SingleOrDefault(x => x.healthscreenrep_id == _id);

                if (hs == null) hs = new HealthScreeningReport();

                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in hs.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(hs, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                writer.PageEvent = new PDFFooter() { form = "hs_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/hs_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        //contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //======ESIGN BY ARJUN V
                if (hs != null)
                {
                    if (hs.isSigned == true)
                    {
                        byte[] imgdata = hs.ESignature.ToArray();
                        //document.Add(new Paragraph("\n\n\n"));
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                        jpg.ScaleToFit(140f, 120f);
                        jpg.SetAbsolutePosition(70, 220);
                        //jpg.Alignment = Element.ALIGN_LEFT;
                        //jpg.Border = PdfPCell.BOTTOM_BORDER;
                        //jpg.BorderWidth = 1f;
                        document.Add(jpg);
                        //Paragraph c1 = new Paragraph(per.SignedBy);
                        //c1.IndentationLeft = 30f;
                        //document.Add(c1);
                    }
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_HealthScreeningReport_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createER(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                EmployeeRightsESign er = ALS.EmployeeRightsESigns.SingleOrDefault(x => x.employeerights_id == _id);
                ALCUser u = ALS.ALCUsers.SingleOrDefault(x => x.user_id == er.user_id);
                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                //if (cme == null) cme = new ConsentMedicalExam();


                Dictionary<string, string> dcval = new Dictionary<string, string>();

                //foreach (var prop in cme.GetType().GetProperties())
                //{
                //    string actualval = "";
                //    var val = prop.GetValue(cme, null);
                //    if (val != null)
                //    {
                //        DateTime t = new DateTime();
                //        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                //    }
                //    dcval[prop.Name] = actualval;//replace 'ANS'
                //}
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                //writer.PageEvent = new PDFFooter() { form = "dr_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/er_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FirstName]", u.first_name);
                string mi = !string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", u.last_name);

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityAddress]", a.address_1);
                contents = contents.Replace("[FacilityCity]", a.city);
                contents = contents.Replace("[FacilityState]", a.state);
                contents = contents.Replace("[FacilityZip]", a.zip);
                contents = contents.Replace("[FacilityPhone]", a.phone);
                contents = contents.Replace("[FacilityFax]", a.fax);

                contents = contents.Replace("[SignedBy]", (er != null) ? er.SignedBy : "");//Sign by
                contents = contents.Replace("[SignedDate]", (er != null) ? er.SignedDate.Value.ToString("MMMM dd, yyyy") : "");//Sign Date

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (er != null)
                {
                    if (er.isSigned == true)
                    {
                        byte[] imgdata = er.ESignature.ToArray();
                        //document.Add(new Paragraph("\n\n\n"));
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                        jpg.ScaleToFit(140f, 120f);
                        jpg.SetAbsolutePosition(130, 90);
                        //jpg.Alignment = Element.ALIGN_LEFT;
                        //jpg.Border = PdfPCell.BOTTOM_BORDER;
                        //jpg.BorderWidth = 1f;
                        document.Add(jpg);
                        //Paragraph c1 = new Paragraph(per.SignedBy);
                        //c1.IndentationLeft = 30f;
                        //document.Add(c1);
                    }
                }
                //======
                document.Close();

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_EmployeeRights_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }
        }
        private void createEPC(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                EmpPhotographyConsentESign epc = ALS.EmpPhotographyConsentESigns.SingleOrDefault(x => x.empphotographyconsent_id == _id);
                ALCUser u = ALS.ALCUsers.SingleOrDefault(x => x.user_id == epc.user_id);

                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                //if (cme == null) cme = new ConsentMedicalExam();


                Dictionary<string, string> dcval = new Dictionary<string, string>();

                //foreach (var prop in cme.GetType().GetProperties())
                //{
                //    string actualval = "";
                //    var val = prop.GetValue(cme, null);
                //    if (val != null)
                //    {
                //        DateTime t = new DateTime();
                //        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                //    }
                //    dcval[prop.Name] = actualval;//replace 'ANS'
                //}
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                //writer.PageEvent = new PDFFooter() { form = "dr_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/pce_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FirstName]", u.first_name);
                string mi = !string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", u.last_name);

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityAddress]", a.address_1);
                contents = contents.Replace("[FacilityCity]", a.city);
                contents = contents.Replace("[FacilityState]", a.state);
                contents = contents.Replace("[FacilityZip]", a.zip);
                contents = contents.Replace("[FacilityPhone]", a.phone);
                contents = contents.Replace("[FacilityFax]", a.fax);


                contents = contents.Replace("[SignedDate]", (epc != null) ? epc.SignedDate.Value.ToString("MMM dd, yyyy") : "");//Sign Date

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (epc != null)
                {
                    if (epc.isSigned == true)
                    {
                        byte[] imgdata = epc.ESignature.ToArray();
                        //document.Add(new Paragraph("\n\n\n"));
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                        jpg.ScaleToFit(140f, 120f);
                        jpg.SetAbsolutePosition(150, 165);
                        //jpg.Alignment = Element.ALIGN_LEFT;
                        //jpg.Border = Pdfepcell.BOTTOM_BORDER;
                        //jpg.BorderWidth = 1f;
                        document.Add(jpg);
                        //Paragraph c1 = new Paragraph(per.SignedBy);
                        //c1.IndentationLeft = 30f;
                        //document.Add(c1);
                    }
                }
                //======
                document.Close();

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_EmployeePhotographyConsent_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }
        }
        private void createCME(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                Admission ad = ALS.Admissions.SingleOrDefault(x => x.admission_id == _id);
                Resident r = ALS.Residents.SingleOrDefault(x => x.resident_id == ad.resident_id);
                Room rm = ALS.Rooms.SingleOrDefault(x => x.room_id == ad.room_id);
                ConsentMedicalExam cme = ALS.ConsentMedicalExams.SingleOrDefault(x => x.admission_id == _id);

                if (cme == null) cme = new ConsentMedicalExam();


                Dictionary<string, string> dcval = new Dictionary<string, string>();

                foreach (var prop in cme.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(cme, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                //writer.PageEvent = new PDFFooter() { form = "ra_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/cme_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }
                contents = contents.Replace("[FirstName]", r.first_name);
                string mi = !string.IsNullOrEmpty(r.middle_initial) ? r.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", r.last_name);

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (cme.isSigned == true)
                {
                    byte[] imgdata = cme.ESignature.ToArray();
                    iTextSharp.text.Image jpg1 = iTextSharp.text.Image.GetInstance(imgdata);
                    jpg1.ScaleToFit(140f, 120f);
                    jpg1.SetAbsolutePosition(360, 165);
                    document.Add(jpg1);

                    iTextSharp.text.Image jpg2 = iTextSharp.text.Image.GetInstance(imgdata);
                    jpg2.ScaleToFit(140f, 120f);
                    jpg2.SetAbsolutePosition(360, 555);
                    document.Add(jpg2);
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_ConsentMedicalExam_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }

        }
        private void createEAA(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                ElderAbuseAcknowledgementESign eaa = ALS.ElderAbuseAcknowledgementESigns.SingleOrDefault(x => x.elderlyabuse_id == _id);
                ALCUser u = ALS.ALCUsers.SingleOrDefault(x => x.user_id == eaa.user_id);
                var position = ALS.ALCUserRoles.SingleOrDefault(x => x.user_id == eaa.user_id);
                var positiontitle = ALS.ALCRoles.Single(x => x.role_id == position.role_id);

                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                //if (cme == null) cme = new ConsentMedicalExam();


                Dictionary<string, string> dcval = new Dictionary<string, string>();

                //foreach (var prop in cme.GetType().GetProperties())
                //{
                //    string actualval = "";
                //    var val = prop.GetValue(cme, null);
                //    if (val != null)
                //    {
                //        DateTime t = new DateTime();
                //        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                //    }
                //    dcval[prop.Name] = actualval;//replace 'ANS'
                //}
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                writer.PageEvent = new PDFFooter() { form = "eaa_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/eaa_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FirstName]", u.first_name);
                string mi = !string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", u.last_name);
                contents = contents.Replace("[EmployeePosition]", positiontitle.description);

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityAddress]", a.address_1);
                contents = contents.Replace("[FacilityCity]", a.city);
                contents = contents.Replace("[FacilityState]", a.state);
                contents = contents.Replace("[FacilityZip]", a.zip);
                contents = contents.Replace("[FacilityPhone]", a.phone);
                contents = contents.Replace("[FacilityFax]", a.fax);

                contents = contents.Replace("[SignedBy]", (eaa != null) ? eaa.SignedBy : "");//Sign by
                contents = contents.Replace("[SignedDate]", (eaa != null) ? eaa.SignedDate.Value.ToString("MMMM dd, yyyy") : "");//Sign Date

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (eaa != null)
                {
                    if (eaa.isSigned == true)
                    {
                        byte[] imgdata = eaa.ESignature.ToArray();
                        //document.Add(new Paragraph("\n\n\n"));
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                        jpg.ScaleToFit(140f, 120f);
                        jpg.SetAbsolutePosition(70, 60);
                        //jpg.Alignment = Element.ALIGN_LEFT;
                        //jpg.Border = PdfPCell.BOTTOM_BORDER;
                        //jpg.BorderWidth = 1f;
                        document.Add(jpg);
                        //Paragraph c1 = new Paragraph(peba.SignedBy);
                        //c1.IndentationLeft = 30f;
                        //document.Add(c1);
                    }
                }
                //======
                document.Close();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                //Response.BinaryWrite(output.ToArray());

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_ElderAbuseReport_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }
        }


        private void createEHR(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                EmployeeHandbookReceiptESign ehr = ALS.EmployeeHandbookReceiptESigns.SingleOrDefault(x => x.employeehandbookreceipt_id == _id);
                ALCUser u = ALS.ALCUsers.SingleOrDefault(x => x.user_id == ehr.user_id);

                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                //if (cme == null) cme = new ConsentMedicalExam();


                Dictionary<string, string> dcval = new Dictionary<string, string>();

                //foreach (var prop in cme.GetType().GetProperties())
                //{
                //    string actualval = "";
                //    var val = prop.GetValue(cme, null);
                //    if (val != null)
                //    {
                //        DateTime t = new DateTime();
                //        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                //    }
                //    dcval[prop.Name] = actualval;//replace 'ANS'
                //}
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                //writer.PageEvent = new PDFFooter() { form = "dr_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/eh_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FirstName]", u.first_name);
                string mi = !string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", u.last_name);

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityAddress]", a.address_1);
                contents = contents.Replace("[FacilityCity]", a.city);
                contents = contents.Replace("[FacilityState]", a.state);
                contents = contents.Replace("[FacilityZip]", a.zip);
                contents = contents.Replace("[FacilityPhone]", a.phone);
                contents = contents.Replace("[FacilityFax]", a.fax);

                contents = contents.Replace("[SignedDate]", (ehr != null) ? ehr.SignedDate.Value.ToString("MMMM dd, yyyy") : "");//Sign Date
                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);
                //======ESIGN BY ARJUN V
                if (ehr != null)
                {
                    if (ehr.isSigned == true)
                    {
                        byte[] imgdata = ehr.ESignature.ToArray();
                        //document.Add(new Paragraph("\n\n\n"));
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                        jpg.ScaleToFit(140f, 120f);
                        jpg.SetAbsolutePosition(250, 440);
                        //jpg.Alignment = Element.ALIGN_LEFT;
                        //jpg.Bordehr = PdfPCell.BOTTOM_BORDER;
                        //jpg.BordehrWidth = 1f;
                        document.Add(jpg);
                        //Paragraph c1 = new Paragraph(pehr.SignedBy);
                        //c1.IndentationLeft = 30f;
                        //document.Add(c1);
                    }
                }
                //======
                document.Close();
                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_HandbookReceipt_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }
        }
        private void createHEP(string AgencyId, int repid)
        {

            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                HepatitisFormESign hep = ALS.HepatitisFormESigns.SingleOrDefault(x => x.hepatitisform_id == _id);
                ALCUser u = ALS.ALCUsers.SingleOrDefault(x => x.user_id == hep.user_id);
                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                //if (cme == null) cme = new ConsentMedicalExam();


                Dictionary<string, string> dcval = new Dictionary<string, string>();

                //foreach (var prop in cme.GetType().GetProperties())
                //{
                //    string actualval = "";
                //    var val = prop.GetValue(cme, null);
                //    if (val != null)
                //    {
                //        DateTime t = new DateTime();
                //        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                //    }
                //    dcval[prop.Name] = actualval;//replace 'ANS'
                //}
                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                //writer.PageEvent = new PDFFooter() { form = "dr_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/hb_template.htm"), Encoding.Unicode);
                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }

                contents = contents.Replace("[FirstName]", u.first_name);
                string mi = !string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + "." : "";
                contents = contents.Replace("[MiddleInitial]", mi);
                contents = contents.Replace("[LastName]", u.last_name);

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityAddress]", a.address_1);
                contents = contents.Replace("[FacilityCity]", a.city);
                contents = contents.Replace("[FacilityState]", a.state);
                contents = contents.Replace("[FacilityZip]", a.zip);
                contents = contents.Replace("[FacilityPhone]", a.phone);
                contents = contents.Replace("[FacilityFax]", a.fax);
                contents = contents.Replace("[SignedDate]", (hep != null) ? hep.SignedDate.Value.ToString("MMM dd, yyyy") : "");//Sign Date

                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                foreach (var htmlElement in parsedHtmlElements)
                    document.Add(htmlElement as IElement);

                //======ESIGN BY ARJUN V
                if (hep != null)
                {
                    if (hep.isSigned == true)
                    {
                        byte[] imgdata = hep.ESignature.ToArray();
                        //document.Add(new Paragraph("\n\n\n"));
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                        jpg.ScaleToFit(140f, 120f);
                        jpg.SetAbsolutePosition(250, 280);
                        //jpg.Alignment = Element.ALIGN_LEFT;
                        //jpg.Border = Pdfhepell.BOTTOM_BORDER;
                        //jpg.BorderWidth = 1f;
                        document.Add(jpg);
                        //Paragraph c1 = new Paragraph(per.SignedBy);
                        //c1.IndentationLeft = 30f;
                        //document.Add(c1);
                    }
                }
                //======
                document.Close();

                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_HepatitisForm_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }
        }
        private void createCSR(string AgencyId, int repid)
        {
            using (var ALS = ASLClientBaseContext(AgencyId))
            {
                var _id = repid;

                CriminalRecordStatement cr = ALS.CriminalRecordStatements.SingleOrDefault(x => x.criminal_record_statement_id == _id);
                ALCUser u = ALS.ALCUsers.SingleOrDefault(x => x.user_id == cr.user_id);
                Agency a = ALS.Agencies.SingleOrDefault(x => x.agency_name == AgencyId);
                Carestaff cs = ALS.Carestaffs.SingleOrDefault(x => x.user_id == cr.user_id);

                AgencyList agency = null;
                using (var ALSM = new ALSMasterBaseDataContext())
                {
                    agency = ALSM.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                    if (agency == null)
                        agency = new AgencyList();
                };

                if (cr == null) cr = new CriminalRecordStatement();
                if (u == null) u = new ALCUser();
                if (cs == null) cs = new Carestaff();

                Dictionary<string, string> dcval = new Dictionary<string, string>();
                Dictionary<string, string> dcval1 = new Dictionary<string, string>();

                foreach (var prop in cr.GetType().GetProperties())
                {
                    string actualval = "";
                    var val = prop.GetValue(cr, null);
                    if (val != null)
                    {
                        DateTime t = new DateTime();
                        actualval = (DateTime.TryParse(val.ToString(), out t)) ? t.ToString("MMMM dd, yyyy") : val.ToString();
                    }
                    dcval[prop.Name] = actualval;//replace 'ANS'
                }

                string bdate;
                DateTime s = new DateTime();
                bdate = (DateTime.TryParse(cs.birth_date.ToString(), out s)) ? s.ToString("MMMM dd, yyyy") : cs.birth_date.ToString();


                var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 50);
                var output = new MemoryStream();
                var writer = PdfWriter.GetInstance(document, output);
                writer.PageEvent = new PDFFooter() { form = "csr_template" };
                document.Open();
                string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/HTMTemplate/csr_template.htm"), Encoding.Unicode);
                string mi = !string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + "." : "";

                contents = contents.Replace("[FacilityName]", agency.agency_name);
                contents = contents.Replace("[FacilityNumber]", a.license_number);
                contents = contents.Replace("[EmployeeName]", u.first_name + " " + mi + " " + u.last_name);
                contents = contents.Replace("[CRAddress]", cs.address_1 + " " + cs.address_2);
                contents = contents.Replace("[city]", cs.city);
                contents = contents.Replace("[zip]", cs.zip);
                contents = contents.Replace("[ssn]", cs.ssn);
                contents = contents.Replace("[birthdate]", bdate);

                foreach (KeyValuePair<string, string> eve in dcval)
                {
                    if (eve.Value.ToLower() == "true")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                    else if (eve.Value.ToLower() == "false")
                        contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                    else if (eve.Key == "RoomNo")
                    {
                        //contents = contents.Replace("[" + eve.Key + "]", (!string.IsNullOrEmpty(rm.room_name)) ? rm.room_name.Trim() : "");
                    }
                    else
                        contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                }


                FontFactory.Register(HttpContext.Current.Server.MapPath("~/fonts/ARIALUNI.TTF"));

                StyleSheet ST = new StyleSheet();
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                int i = 0;//counter
                foreach (var htmlElement in parsedHtmlElements)
                {
                    //ESIGN BY ARJUN
                    if (i == 21) //insert image to page before last
                    {
                        if (cr.isSigned == true)
                        {
                            byte[] imgdata = cr.ESignature.ToArray();
                            //document.Add(new Paragraph("\n\n\n"));
                            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgdata);

                            jpg.ScaleToFit(140f, 120f);
                            jpg.SetAbsolutePosition(150, 325);
                            //jpg.Alignment = Element.ALIGN_LEFT;
                            //jpg.Border = Pdfepcell.BOTTOM_BORDER;
                            //jpg.BorderWidth = 1f;
                            document.Add(jpg);
                            //Paragraph c1 = new Paragraph(per.SignedBy);
                            //c1.IndentationLeft = 30f;
                            //document.Add(c1);
                        }
                    }
                    //==========
                    document.Add(htmlElement as IElement);
                    i++;
                }


                document.Close();
                
                FileStream file = File.Open(HttpContext.Current.Server.MapPath("~/Content/Temp/") + AgencyId + "_CriminalRecordStatement_" + repid.ToString() + ".pdf", FileMode.OpenOrCreate);

                file.Write(output.ToArray(), 0, output.ToArray().Length);
                file.Close();
            }
        }
    }
}
