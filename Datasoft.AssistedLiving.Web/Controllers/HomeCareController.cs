﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using DataSoft.Helpers;
using Datasoft.Ext;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Datasoft.AssistedLiving.Web.Models;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class HomeCareController : Controller
    {
        // GET: HomeCare
        public ActionResult Index()
        {
            return View();
        }

        private string UserID
        {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID
        {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        private string RoleID
        {
            get { return User.Identity.Name.Split('|')[2]; }
        }


        [HttpGet]
        public string CT()
        {
            return DataSoft.Helpers.Utility.ToTimezonedDateTimeFriendlyTZ(DateTime.Now, "ddd, MMM dd yyyy h:mmtt");
        }

        [HttpPost]
        public JsonResult PostData([DynamicJson(MatchName = false)] dynamic obj)
        {
            var result = JsonResponse.GetDefault();
            if (obj != null)
            {
                if (obj.Func == AdminConstants.DASHBOARD_PENDING_ADMISSION)
                {
                    result = ParsePendingAdmission(obj.Data);
                }
            }
            return Json(result);
        }

        #region Parse Changes
        private JsonResponse ParsePendingAdmission([DynamicJson] dynamic data)
        {
            int rowsAffected = 0;
            try
            {
                if (data == null)
                    throw new Exception("Data Param required.");

                using (var ALSDB = new ALSDataContext())
                {
                    //1-Resident, 2-Pending, 3-Non-Resident
                    rowsAffected = ALSDB.updatePendingAdmission(data, 1);
                }
            }
            catch (Exception E)
            {
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return new JsonResponse(JsonResponseResult.Success, rowsAffected.ToString());
        }
        #endregion


        public dynamic GetMarketerStats([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                //Resident r = null;

                using (var ALS = new ALSBaseDataContext())
                {

                    var less30days = DateTime.Now.AddDays(-30);
                    var a = (from r in ALS.Residents
                                 //  kim add code for accurate date 02/03/2022 
                             where r.isAdmitted == true && r.status == 1 && r.admission_status == 1 && r.date_admitted > less30days ||
                             r.admission_status == 8 ||
                             r.admission_status == 2 ||
                             r.admission_status == 3 ||
                             r.admission_status == 4 ||
                             r.admission_status == 5 ||
                             r.admission_status == 6 ||
                             r.admission_status == 7

                             //    r.isAdmitted == true && r.status == 1 && r.admission_status == 1
                             group r by r.admission_status into g
                             select new
                             {
                                 admission_status = g.Key,
                                 StatCount = g.Count()
                             }).ToList();

                    if (a != null)
                    {
                        return Json(a);
                    }


                }
            }
            catch (Exception E)
            {
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("");

        }

        public dynamic GetReferrerStats([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                //Resident r = null;

                using (var ALS = new ALSBaseDataContext())
                {

                    var a = (from r in ALS.Referrals
                             group r by r.ref_source into g
                             where g.Key != null
                             select new
                             {
                                 refsource = g.Key,
                                 StatCount = g.Count()
                             }).OrderBy(y => y.refsource).OrderByDescending(x => x.StatCount).ToList();

                    if (a != null)
                    {
                        return Json(a);
                    }

                }
            }
            catch (Exception E)
            {
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("");

        }


        public dynamic GetResidentStats([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                //Resident r = null;

                using (var ALS = new ALSBaseDataContext())
                {

                    var residentCount = (from r in ALS.Residents
                                         where r.admission_status != null && r.status == 1 && r.responsible_person != null
                                         select new
                                         {
                                             r
                                         }).Count();

                    var isDNR = (from r in ALS.Residents
                                 where r.is_dnr == true && r.isAdmitted == true
                                 select new
                                 {
                                     r
                                 }).Count();

                    var isAdmitted = (from r in ALS.Residents
                                      where r.isAdmitted == true && r.status == 1 && r.admission_status == 1
                                      select new
                                      {
                                          r
                                      }).Count();

                    var isDischarged = (from r in ALS.Residents
                                        where r.isAdmitted == false
                                        select new
                                        {
                                            r
                                        }).Count();

                    var isReferral = (from r in ALS.Residents
                                      where r.isAdmitted == null
                                      select new
                                      {
                                          r
                                      }).Count();

                    var res = new
                    {
                        residentCount = residentCount,
                        isDNRstat = isDNR,
                        isAdmitted = isAdmitted,
                        isDischarged = isDischarged,
                        isReferral = isReferral
                    };


                    return Json(res);


                }
            }
            catch (Exception E)
            {
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }

        }


        public dynamic GetAdmissionDates([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                //Resident r = null;

                using (var ALS = new ALSBaseDataContext())
                {

                    var a = (from r in ALS.Admissions
                             where r.admission_date != null
                             select new
                             {
                                 admission_date = Utility.GetDateFormattedString(r.admission_date, "MM/dd/yyyy")
                             }).ToList();

                    if (a != null)
                    {
                        return Json(a);
                    }

                }
            }
            catch (Exception E)
            {
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("");

        }

        public dynamic GetCurrentStats([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                //Resident r = null;

                using (var ALS = new ALSBaseDataContext())
                {

                    var planned_tourdate = (from a in ALS.Referrals
                                            join b in ALS.Residents on a.resident_id equals b.resident_id
                                            select new
                                            {
                                                b.first_name,
                                                b.last_name,
                                                b.middle_initial,
                                                planned_tourdate = Utility.GetDateFormattedString(a.planned_tour_date, "MM/dd/yyyy"),
                                            }).ToList();

                    var tourdate_sched = (from r in ALS.Residents
                                          join a in ALS.Referrals on r.resident_id equals a.resident_id
                                          where r.admission_status == 4
                                          select new
                                          {
                                              r.first_name,
                                              r.middle_initial,
                                              r.last_name,
                                              planned_tourdate = Utility.GetDateFormattedString(a.planned_tour_date, "MM/dd/yyyy")
                                          }).ToList();

                    var incomingAdmission = (from r in ALS.Residents
                                             where r.admission_status == 8
                                             select new
                                             {
                                                 r.first_name,
                                                 r.middle_initial,
                                                 r.last_name

                                             }).ToList();
                    //kim change code 02/07/22
                    var recentAdmission = (from r in ALS.Residents
                                           join x in ALS.Referrals on r.resident_id equals x.resident_id
                                           join v in ALS.Admissions on r.resident_id equals v.resident_id
                                           //add code isAdmitted for all resident that is not discharge 02/11/22
                                           where r.admission_status == 1 && r.isAdmitted == true
                                           orderby r.resident_id descending
                                           //from r in ALS.Residents
                                           //  where r.admission_status == 1  orderby r.resident_id descending
                                           select new
                                           {
                                               r.first_name,
                                               r.middle_initial,
                                               r.last_name,
                                               date_admitted = Utility.GetDateFormattedString(r.date_admitted, "MM/dd/yyyy"),

                                           }).ToList();

                    var res = new
                    {
                        planned_tourdate = planned_tourdate,
                        incomingAdmission = incomingAdmission,
                        recentAdmission = recentAdmission,
                        tourdate_sched = tourdate_sched
                    };

                    return Json(res);


                }
            }
            catch (Exception E)
            {
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("");

        }


        //[HttpPost]
        //public JsonResult GetAdmissionCalendar([DynamicJson(MatchName = false)] dynamic obj)
        //{
        //    try
        //    {
        //        //var result = JsonResponse.GetDefault();
        //       // if (obj != null)
        //       // {
        //            DateTime? startDate = DateTime.Now;
        //        DateTime? endDate = DateTime.Now;

        //            using (var ALS = new ALSBaseDataContext())
        //            {

        //                var d = (from t in ALS.TransportationSchedules
        //                         join r in ALS.Residents on t.resident_id equals r.resident_id
        //                         join a in ALS.Admissions on t.resident_id equals a.resident_id
        //                         join rm in ALS.Rooms on a.room_id equals rm.room_id
        //                         where t.is_active == true && r.isAdmitted == true
        //                         //join c in ALS.ALCUsers on t.staff_id equals c.user_id
        //                         select new
        //                         {
        //                             t.resident_id,
        //                             t.transpo_id,
        //                             date = Utility.GetDateFormattedString(t.date, "MM/dd/yyyy"),
        //                             end_date = Utility.GetDateFormattedString(t.end_date, "MM/dd/yyyy"),
        //                             //staff_id = c.first_name + " " + c.last_name,
        //                             start_time = Utility.GetDateFormattedString(t.start_time, "HH:mm"),
        //                             end_time = Utility.GetDateFormattedString(t.end_time, "HH:mm"),
        //                             name = r.first_name + " " + r.last_name,
        //                             t.day_of_the_week,
        //                             t.recurrence,
        //                             t.day_of_the_month,
        //                             rm.room_name,
        //                             r.isAdmitted
        //                         }).ToList();

        //                var data = new
        //                {
        //                    StartDate = startDate,
        //                    EndDate = endDate,
        //                    Appointments = d
        //                };

        //                return Json(data);
        //            }
        //       // }
        //        //return Json(result);

        //    }
        //    catch (Exception E)
        //    {
        //        //LogError("TransschedController/GetAdmissionCalendar", "Error Message: " + E.Message);

        //        return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
        //    }

        //}

        public dynamic GetGridData(string func, string param, string param2, string param3, string param4)
        {

            var ind = "";
            try
            {
                if (func == AdminConstants.ACTIVITY_SCHEDULE)
                {
                    // ind = "<b>func == AdminConstants.ACTIVITY_SCHEDULE</b><br/>";
                    using (var ALS = new ALSBaseDataContext())
                    {
                        DateTime? fromDate = DateTime.Now;
                        DateTime? toDate = null;
                        int roleId = int.Parse(RoleID);

                        //overriding roleId so that guests can access transsched schedule

                        var role = ALS.ALCRoles.SingleOrDefault(x => x.role_id == roleId);
                        //  var user = ALS.ALCUsers.SingleOrDefault(x => x.is_active == role.is_system_def);

                        if (role.description == "Guest")
                        {
                            roleId = 1;
                        }

                        // Cheche was here for join Activity Schedule & Transpo to make total counts in dashboard accurate with daily task on 1/27/2022

                        int? p3 = !string.IsNullOrEmpty(param3) ? (int?)int.Parse(param3) : null;
                        int? p4 = !string.IsNullOrEmpty(param4) ? (int?)int.Parse(param4) : null;
                        var cs = (from c in ALS.getActivityScheduleByUser(UserID, roleId, fromDate, toDate, p3, p4)
                                      //where c.isAdmitted == true // Cheche was here on 1/27/2022 
                                  where c.is_active == true
                                  select new
                                  {
                                      c.activity_desc,
                                      c.activity_id,
                                      c.activity_proc,
                                      c.activity_schedule_id,
                                      c.activity_status,
                                      c.oactivity_status,
                                      c.department,
                                      c.carestaff_activity_id,
                                      c.completion_date,
                                      c.completion_type,
                                      c.remarks,
                                      reschedule_dt = Utility.GetDateFormattedString(c.reschedule_dt, "MM/dd/yyyy hh:mm tt"),
                                      c.carestaff_name,
                                      c.resident,
                                      resident2 = c.resident,
                                      carestaff2 = c.carestaff_name,
                                      activitydesc2 = c.activity_desc,
                                      c.resident_id,
                                      c.room,
                                      c.service_duration,
                                      c.acknowledged_by,
                                      start_time = Utility.GetDateFormattedString(c.start_time, "hh:mm tt"),
                                      c.xref,
                                      actual_completion_date = Utility.GetDateFormattedString(c.actual_completion_date, "hh:mm tt"),
                                      timelapsed = Utility.IsTimeLapseFromCurrentTime(c.start_time, fromDate),
                                      start = Utility.GetDateFormattedString(c.start_time, "HH:mm"),
                                      c.isAdmitted,
                                      c.responsible_person_email,
                                      c.is_active,
                                      active_until = (c.active_until != null ? Utility.GetDateFormattedString(c.active_until, "MM/dd/yyyy hh:mm tt") : null),
                                  }).OrderBy(x => x.start).ToList();

                        return Json(cs, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception E)
            {

                //ind = "<b>func == " + func + "</b><br/>";
                //LogError("StaffController/GetGridData", ind + "Error Message: " + E.Message);

                return Json(new JsonResponse(JsonResponseResult.Error, E.Message), JsonRequestBehavior.AllowGet);
            }
            return Json("[]", JsonRequestBehavior.AllowGet);
        }


    }
}