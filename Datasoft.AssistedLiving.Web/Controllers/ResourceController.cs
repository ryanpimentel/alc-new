﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataSoft.Helpers;
using Datasoft.AssistedLiving.Web.Models;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class ResourceController : Controller { 
        public FileResult ResidentImage(int id) {
            using (var ALSDB = new ALSBaseDataContext()) {
                var res = ALSDB.Residents.SingleOrDefault(x => x.resident_id == id);
                if (res != null && res.imagesrc != null) {
                    try {
                        return File(res.imagesrc.ToArray(), "image/png");
                    } catch { 
                        return File("~/Images/user.png", "image/png");
                    }
                } else {
                    return File("~/Images/user.png", "image/png");
                }
            }
        }

        public FileResult EmpImage(string id, int b=0) {
            using (var ALSDB = new ALSBaseDataContext()) {
                if (b == 1) {
                    var byts = Convert.FromBase64String(id);
                    id = System.Text.Encoding.UTF8.GetString(byts);
                }
                var res = ALSDB.ALCUsers.SingleOrDefault(x => x.user_id == id);
                if (res != null && res.imagesrc != null) {
                    try {
                        return File(res.imagesrc.ToArray(), "image/png");
                    } catch {
                        return File("~/Images/user.png", "image/png");
                    }
                } else {
                    return File("~/Images/user.png", "image/png");
                }
            }
        }
    }
}