﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataSoft.Helpers;
using Datasoft.Ext;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Datasoft.AssistedLiving.Web.Models;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.html;
using System.Text;

namespace Datasoft.AssistedLiving.Web.Controllers
{
    public class MarketerController : Controller
    {
        private string UserID
        {
            get { return User.Identity.Name.Split('|')[0]; }
        }

        private string AgencyID
        {
            get { return User.Identity.Name.Split('|')[1]; }
        }

        private string RoleID
        {
            get { return User.Identity.Name.Split('|')[2]; }
        }
        public ActionResult Index()
        {
            int roleId = int.Parse(RoleID);
            ViewBag.RoleID = roleId;
            var objModel = GetResidentdmissionData();
            return View(objModel);
        }
        public ActionResult LogError(string module, string message)
        {
            var log = new ErrorController();
            log.ControllerContext = ControllerContext;
            return log.LogError(module, message);
        }
        private ResidentAdmissionData GetResidentdmissionData()
        {
            using (var ALSDB = new ALSBaseDataContext())
            {

                return new ResidentAdmissionData
                {
                
                    FC = ALSDB.AssessmentPointSettings.Where(x => x.Group == "Functional Capabilities" && x.is_active == 1).Select(x => new SelectDataAPS { Id = x.FieldId, Score = x.ScoreValue, Name = x.FieldText, Group = x.Group, SubGroup = x.SubGroup, Active=x.is_active}).ToList(),
                    PSC = ALSDB.AssessmentPointSettings.Where(x => x.Group == "Psycho/Social Capabilities" && x.is_active == 1).Select(x => new SelectDataAPS { Id = x.FieldId, Score = x.ScoreValue, Name = x.FieldText, Group = x.Group, SubGroup = x.SubGroup, Active = x.is_active }).ToList(),
                    CC = ALSDB.AssessmentPointSettings.Where(x => x.Group == "Cognitive Capabilities" && x.is_active == 1).Select(x => new SelectDataAPS { Id = x.FieldId, Score = x.ScoreValue, Name = x.FieldText, Group = x.Group, SubGroup = x.SubGroup, Active = x.is_active }).ToList(),
                    SMN = ALSDB.AssessmentPointSettings.Where(x => x.Group == "Special Medicinal Needs" && x.is_active == 1).Select(x => new SelectDataAPS { Id = x.FieldId, Score = x.ScoreValue, Name = x.FieldText, Group = x.Group, SubGroup = x.SubGroup, Active = x.is_active }).ToList(),

                    Facilities = ALSDB.Facilities.Select(x => new SelectData { Id = x.facility_id, Name = x.facility_name }).ToList(),
                    Rooms = ALSDB.Rooms.Select(x => new SelectData { Id = x.room_id, Name = x.room_name }).ToList()
                };
            };
        }

        [HttpPost]
        public JsonResult PostGridData([DynamicJson(MatchName = false)] dynamic obj)
        {
            try
            {
                var result = JsonResponse.GetDefault();
                if (obj != null)
                {
                    int record_id = 0, resId = 0;
                    var mode = obj.Mode;
                    string actionTaken = "", recordUpdated = "";
                    try { actionTaken = Convert.ToString(obj.Mode); }
                    catch { actionTaken = "1"; }

                    if (obj.Func == AdminConstants.ASSESSMENT_WIZARD)
                    {
                        result = ParseAssessmentWizard((PostMode)obj.Mode, obj.Data);
                        try
                        {
                            var reId = int.Parse(obj.Data.AssResidentId);
                            recordUpdated = "assessmentId=" + result.Message + "; assessmentInfoId=" + result.Return + "; residentId=" + reId;
                            record_id = Convert.ToInt32(result.Message);

                            //updated = obj.Data.Firstname + " " + obj.Data.MiddleInitial + " " + obj.Data.Lastname;
                            //recordUpdated = obj.Data.FirstName + " " + obj.Data.LastName;
                            //record_id = Convert.ToInt32(obj.Data.Id);
                            //resId = int.Parse(obj.Data.AssResidentId);
                        }
                        catch { }
                    }
                    using (var ALSDB = new ALSBaseDataContext())
                    {
                        ALSDB.saveAction(actionTaken, UserID, record_id, obj.Func, recordUpdated);
                    }
                }
                return Json(result);
            }
            catch (Exception E)
            {
                LogError("MarketerController/PostGridData", "Error message: " + E.Message);
                return Json(new JsonResponse(JsonResponseResult.Error, E.Message));
            }
        }
        private JsonResponse ParseAssessmentWizard(PostMode mode, [DynamicJson] dynamic obj)
        {
            try
            {
                Assessment ass = null;
                Resident res = null;
                Referral re = null;
                AssessmentInfo ra = null;
                List<AssessmentInfo> listAssessmentInfo = null;
                using (var ALSDB = new ALSBaseDataContext())
                {
                    if (mode == PostMode.Add)
                    {
                        // kim add code here for comparison of the date of assessment start and completed date  01/25/22
                        ass = new Assessment();

                        ass.ResidentId = ((string)obj.AssResidentId).TryParseNullableInt();
                        ass.referral_id = ((string)obj.AssReferralId).TryParseNullableInt();

                        re = ALSDB.Referrals.SingleOrDefault(x => x.resident_id == ass.ResidentId);

                        var CompletedStartDate = re.assessment_start_date.HasValue ? re.assessment_start_date.Value.ToString("MM/dd/yyyy") : "";
                        DateTime date = DateTime.Parse(CompletedStartDate);

                        ass.CompletedBy = obj.CompletedBy;
                        ass.TotalScore = obj.TotalScore;
                        //kim modified code below to get value of the completedate when first try saving the assessment 04/28/22 mod 05/11/22
                        ass.CompletedDate = ((string)obj.CompletedDate).TryParseNullableDate(false);                       
                        var completeddated = ass.CompletedDate.HasValue ? ass.CompletedDate.Value.ToString("MM/dd/yyyy") : "";
                        if (ass.CompletedDate == null) // Cheche was here to modify : 11-25-2022 
                        { 
                            // return new JsonResponse(0, "No Completed Date");
                            if (ass.AssessmentId == 0 || ass.AssessmentId == null) {

                                ass = new Assessment();
                                ass.CompletedDate = ((string)obj.CompletedDate).TryParseNullableDate(false);
                                ass.ResidentId = ((string)obj.AssResidentId).TryParseNullableInt();
                                ass.referral_id = ((string)obj.AssReferralId).TryParseNullableInt();
                                ass.CompletedBy = obj.CompletedBy;
                                ass.TotalScore = obj.TotalScore;
                            }
                            ALSDB.Assessments.InsertOnSubmit(ass);
                            ALSDB.SubmitChanges();

                            listAssessmentInfo = ALSDB.AssessmentInfos.Where(x => x.AssessmentId == ass.AssessmentId).ToList();
                            if (listAssessmentInfo == null || listAssessmentInfo.Count == 0)
                            {
                                var dict0 = (Dictionary<string, object>)obj;
                                if (dict0 != null)
                                {
                                    foreach (var kp in dict0)
                                    {
                                        if (kp.Key == "AssessmentId" || kp.Key == "ResidentId" || kp.Key == "ReferralId" || kp.Key == "FirstName" || kp.Key == "MiddleInitial" || kp.Key == "LastName"
                                        || kp.Key == "CompletedBy" || kp.Key == "CompletedDate" || kp.Key == "TotalScore")
                                            continue;

                                        ra = listAssessmentInfo.SingleOrDefault(x => x.field_name == kp.Key);
                                        if (ra == null)
                                        {
                                            ra = new AssessmentInfo();
                                            ra.AssessmentId = ass.AssessmentId;
                                            ra.field_name = kp.Key;
                                            ra.field_value = kp.Value != null ? kp.Value.ToString() : null;
                                            ALSDB.AssessmentInfos.InsertOnSubmit(ra);
                                        }
                                        else
                                        {
                                            ra.field_value = kp.Value != null ? kp.Value.ToString() : null;
                                        }
                                    }
                                    ALSDB.SubmitChanges();
                                }

                            }
                           
                        }
                        if (completeddated != "") {
                            DateTime completes = DateTime.Parse(completeddated);
                            int resu = DateTime.Compare(date, completes);

                            if (resu == -1 || ass.CompletedDate != null)

                            {
                                ALSDB.Assessments.InsertOnSubmit(ass);
                                ALSDB.SubmitChanges();
                            }

                            else if (resu == 1)
                            {
                                //  return new JsonResponse(-1, "Error on Completed Date");
                                return new JsonResponse(0, "Error on Completed");
                            }
                            var dict = (Dictionary<string, object>)obj;
                            if (dict != null)
                            {
                                listAssessmentInfo = new List<AssessmentInfo>();
                                foreach (var kp in dict)
                                {
                                    if (kp.Key == "AssessmentId" || kp.Key == "ResidentId" || kp.Key == "ReferralId" || kp.Key == "FirstName" || kp.Key == "MiddleInitial" || kp.Key == "LastName"
                                        || kp.Key == "CompletedBy" || kp.Key == "CompletedDate" || kp.Key == "TotalScore")
                                        continue;

                                    ra = new AssessmentInfo();
                                    ra.AssessmentId = ass.AssessmentId;
                                    ra.field_name = kp.Key;
                                    ra.field_value = kp.Value != null ? kp.Value.ToString() : null;
                                    listAssessmentInfo.Add(ra);
                                }
                                ALSDB.AssessmentInfos.InsertAllOnSubmit(listAssessmentInfo);
                                ALSDB.SubmitChanges();
                            }
                        }
                       
                                           }
                    else if (mode == PostMode.Edit)
                    {
                        int? i = ((string)obj.AssessmentId).TryParseNullableInt();
                        ass = ALSDB.Assessments.SingleOrDefault(x => x.AssessmentId == (long)i);
                        re = ALSDB.Referrals.SingleOrDefault(x => x.resident_id == ass.ResidentId);
                        var CompletedStartDate = re.assessment_start_date.HasValue ? re.assessment_start_date.Value.ToString("MM/dd/yyyy") : "";

                        if (ass != null)
                        {

                            ass.CompletedDate = ((string)obj.CompletedDate).TryParseNullableDate(false);
                            ass.ResidentId = ((string)obj.AssResidentId).TryParseNullableInt();
                            ass.referral_id = ((string)obj.AssReferralId).TryParseNullableInt();
                            ass.CompletedBy = obj.CompletedBy;
                            ass.TotalScore = obj.TotalScore;
                            if (ass.CompletedDate == null)
                            {
                                // return new JsonResponse(0, "No Completed Date");
                                listAssessmentInfo = ALSDB.AssessmentInfos.Where(x => x.AssessmentId == (long)i).ToList();
                                if (listAssessmentInfo == null || listAssessmentInfo.Count == 0)
                                {
                                    var dict = (Dictionary<string, object>)obj;
                                    if (dict != null)
                                    {
                                        foreach (var kp in dict)
                                        {
                                            if (kp.Key == "AssessmentId" || kp.Key == "ResidentId" || kp.Key == "ReferralId" || kp.Key == "FirstName" || kp.Key == "MiddleInitial" || kp.Key == "LastName"
                                            || kp.Key == "CompletedBy" || kp.Key == "CompletedDate" || kp.Key == "TotalScore")
                                                continue;

                                            ra = listAssessmentInfo.SingleOrDefault(x => x.field_name == kp.Key);
                                            if (ra == null)
                                            {
                                                ra = new AssessmentInfo();
                                                ra.AssessmentId = ass.AssessmentId;
                                                ra.field_name = kp.Key;
                                                ra.field_value = kp.Value != null ? kp.Value.ToString() : null;
                                                ALSDB.AssessmentInfos.InsertOnSubmit(ra);
                                            }
                                            else
                                            {
                                                ra.field_value = kp.Value != null ? kp.Value.ToString() : null;
                                            }
                                        }
                                        ALSDB.SubmitChanges();
                                    }
                                }
                                else
                                {
                                    if (listAssessmentInfo != null && listAssessmentInfo.Count > 0)
                                    {
                                        var dict = (Dictionary<string, object>)obj;
                                        if (dict != null)
                                        {
                                            foreach (var kp in dict)
                                            {
                                                if (kp.Key == "AssessmentId" || kp.Key == "ResidentId" || kp.Key == "ReferralId" || kp.Key == "FirstName" || kp.Key == "MiddleInitial" || kp.Key == "LastName"
                                                || kp.Key == "CompletedBy" || kp.Key == "CompletedDate" || kp.Key == "TotalScore")
                                                    continue;

                                                ra = listAssessmentInfo.SingleOrDefault(x => x.field_name == kp.Key);
                                                if (ra == null)
                                                {
                                                    ra = new AssessmentInfo();
                                                    ra.AssessmentId = ass.AssessmentId;
                                                    ra.field_name = kp.Key;
                                                    ra.field_value = kp.Value != null ? kp.Value.ToString() : null;
                                                    ALSDB.AssessmentInfos.InsertOnSubmit(ra);
                                                }
                                                else
                                                {
                                                    ra.field_value = kp.Value != null ? kp.Value.ToString() : null;
                                                }
                                            }
                                            ALSDB.SubmitChanges();
                                        }

                                        if (dict != null)
                                        {

                                        }
                                    }
                                }

                            }
                            var completeddated = ass.CompletedDate.HasValue ? ass.CompletedDate.Value.ToString("MM/dd/yyyy") : "";
                            if (completeddated != "")
                            {
                                DateTime date = DateTime.Parse(CompletedStartDate);
                                //DateTime completes = DateTime.Parse(completeddated);
                                DateTime completes = Convert.ToDateTime(completeddated);

                                int resu = DateTime.Compare(date, completes);

                                if (resu == -1)

                                {
                                    ALSDB.SubmitChanges();
                                }

                                else if (resu == 1)
                                {
                                    //  return new JsonResponse(-1, "Error on Completed Date");
                                    return new JsonResponse(0, "Error on Completed");
                                }
                            }
                        }                        
                    // transferred the code above : line 243-271 : Cheche : 11-28-2022
                    }
                    var id = ass.ResidentId;
                    var MoveInDate = ((string)obj.AssesMoveInDate).TryParseNullableDate(false);
                    res = ALSDB.Residents.SingleOrDefault(x => x.resident_id == ass.ResidentId);
                    var c = ALSDB.getAdmissionById(id).SingleOrDefault();
                    if (MoveInDate != null)
                    {
                        if (res.admission_status == (int?)AdmissionStatus.Assessment_For_Completion)
                        {
                            res.admission_status = (int?)AdmissionStatus.Move_In_For_Schedule;
                            ALSDB.SubmitChanges();
                        }
                        re = ALSDB.Referrals.SingleOrDefault(x => x.referral_id == ass.referral_id);
                        if (re != null)
                        {
                            re.movein_date = MoveInDate;
                            re.assessment_completed_date = ass.CompletedDate;
                            re.is_assessment_completed = (ass.CompletedDate != null);
                            ALSDB.SubmitChanges();
                        }

                    }
                    else if (ass.CompletedDate != null)
                    {

                        if (res.admission_status == (int?)AdmissionStatus.Assessment_For_Completion)
                        {
                            res.admission_status = (int?)AdmissionStatus.Assessment_For_Follow_Up;
                            ALSDB.SubmitChanges();
                        }
                        re = ALSDB.Referrals.SingleOrDefault(x => x.referral_id == re.referral_id);
                        re.assessment_completed_date = ass.CompletedDate;
                        re.is_assessment_completed = (ass.CompletedDate != null);
                        ALSDB.SubmitChanges();

                    }

                    try
                    {
                        //var data = new
                        //{
                        //    assmntId = ass.AssessmentId,
                        //    assInfoId = ra.assessmentinfo_id,
                        //    residentId = ass.ResidentId
                        //};

                        //var data2 = JsonConvert.SerializeObject(data);
                        ////return new JsonResponse(JsonResponseResult.Success, data2.ToString());
                        return new JsonResponse(JsonResponseResult.Success, ass.AssessmentId.ToString(), ra.assessmentinfo_id.ToString());
                    }
                    catch { }
                }
            }
            catch (Exception E)
            {
                var ind = "<b>mode == " + mode + "</b><br>";
                ind += "Line Number: " + E.LineNumber() + "<br>";
                LogError("MarketerController/ParseAssessmentWizard", ind + "Error message: " + E.Message);
                return new JsonResponse(JsonResponseResult.Error, E.Message);
            }
            return JsonResponse.GetDefault();
        }

        public dynamic GetFormData([DynamicJson(MatchName = false)] dynamic obj)
        {
            int id = -1;
            string strId = "";
            if (!int.TryParse(obj.Id, out id))
                strId = obj.Id;
            using (var ALS = new ALSBaseDataContext())
            {
                var AssPoints = ALS.AssessmentPointSettings
                            .Select(t => new { t.FieldId, t.ScoreValue })
                            .ToDictionary(t => t.FieldId, t => t.ScoreValue);

                try
                {
                    if (obj.Func == AdminConstants.ASSESSMENT_WIZARD)
                    {
                        #region ASSESSMENT_WIZARD
                        //var Ass = ALS.Assessments.Where(a => (a.ResidentId == id)).OrderBy(a => a.CompletedDate).ToArray();
                        var Ass = ALS.Assessments.Where(a => (a.ResidentId == id)).ToArray();


                        if (Ass == null || Ass.Length == 0)
                        {
                            var c = ALS.Residents.SingleOrDefault(x => x.resident_id == id);
                            if (c != null)
                            {
                                var objres = new
                                {
                                    FirstName = c.first_name,
                                    MiddleInitial = c.middle_initial,
                                    LastName = c.last_name
                                };
                                return Json(new
                                {
                                    Result = 1,
                                    Data = objres,
                                    Points = AssPoints,
                                    Assessments = Ass
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            JObject res1 = null;
                            JObject res2 = null;
                            string json = "";


                            //MODIFY CODE HERE
                            var len = Ass.Length;
                            var latestAssessment = Ass[len - 1];
                            var completedDate = Convert.ToDateTime(latestAssessment.CompletedDate);


                            var r_old = (from x in ALS.Assessments
                                         join y in ALS.Residents on x.ResidentId equals y.resident_id
                                         where x.ResidentId == id
                                         //&& x.CompletedDate == completedDate
                                         select new
                                         {
                                             AssessmentId = x.AssessmentId,
                                             FirstName = y.first_name,
                                             MiddleInitial = y.middle_initial,
                                             LastName = y.last_name,
                                             CompletedBy = x.CompletedBy,
                                             TotalScore = x.TotalScore,
                                             CompletedDate = x.CompletedDate// CHECK: Utility.ToTimezonedDateTime(x.CompletedDate) not working
                                         }
                            ).OrderByDescending(u => u.AssessmentId).FirstOrDefault();

                            var r = new // lol
                            {
                                AssessmentId = r_old.AssessmentId,
                                FirstName = r_old.FirstName,
                                MiddleInitial = r_old.MiddleInitial,
                                LastName = r_old.LastName,
                                CompletedBy = r_old.CompletedBy,
                                TotalScore = r_old.TotalScore,
                                CompletedDate = r_old.CompletedDate == null ? "" : r_old.CompletedDate.Value.ToShortDateString()
                            };
                            if (r != null)
                            {
                                json = JsonConvert.SerializeObject(r);
                                if (!string.IsNullOrEmpty(json))
                                    res1 = JObject.Parse(json);
                            }

                            //get assessment infos
                            var rai = ALS.AssessmentInfos
                                .Where(x => x.AssessmentId == r.AssessmentId)
                                .Select(t => new { t.field_name, t.field_value })
                                .ToDictionary(t => t.field_name, t => t.field_value);
                            if (rai != null)
                            {
                                json = JsonConvert.SerializeObject(rai, new KeyValuePairConverter());
                                if (!string.IsNullOrEmpty(json))
                                    res2 = JObject.Parse(json);
                            }
                            if (res1 != null)
                            {
                                if (res2 != null)
                                    res1.Merge(res2, new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Concat });
                            }
                            return Json(new
                            {
                                Result = 2,
                                Data = res1.ToObject<Dictionary<string, object>>(),
                                Points = AssPoints,
                                Assessments = Ass
                            }, JsonRequestBehavior.AllowGet);
                            //return new ContentResult { Content = res1.ToString(), ContentType = "application/json" };

                        }
                        #endregion
                    }
                    else if (obj.Func == AdminConstants.ASSESSMENT_WIZARD_LIST)
                    {

                        var Ass = ALS.Assessments.Where(a => (a.AssessmentId == id)).SingleOrDefault();
                        var Resid = Ass.ResidentId;
                        var AssNums = ALS.Assessments.Where(a => (a.ResidentId == Resid)).ToArray();

                        JObject res1 = null;
                        JObject res2 = null;
                        string json = "";


                        ////MODIFY CODE HERE
                        //var len = Ass.Length;
                        //var latestAssessment = Ass[len - 1];
                        //var completedDate = Convert.ToDateTime(latestAssessment.CompletedDate);


                        var r_old = (from x in ALS.Assessments
                                     join y in ALS.Residents on x.ResidentId equals y.resident_id
                                     where x.AssessmentId == id
                                     select new
                                     {
                                         AssessmentId = x.AssessmentId,
                                         FirstName = y.first_name,
                                         MiddleInitial = y.middle_initial,
                                         LastName = y.last_name,
                                         CompletedBy = x.CompletedBy,
                                         TotalScore = x.TotalScore,
                                         CompletedDate = x.CompletedDate// CHECK: Utility.ToTimezonedDateTime(x.CompletedDate) not working
                                     }
                        ).OrderByDescending(u => u.AssessmentId).FirstOrDefault();

                        var r = new // lol
                        {
                            AssessmentId = r_old.AssessmentId,
                            FirstName = r_old.FirstName,
                            MiddleInitial = r_old.MiddleInitial,
                            LastName = r_old.LastName,
                            CompletedBy = r_old.CompletedBy,
                            TotalScore = r_old.TotalScore,
                            CompletedDate = r_old.CompletedDate == null ? "" : r_old.CompletedDate.Value.ToShortDateString()
                        };
                        if (r != null)
                        {
                            json = JsonConvert.SerializeObject(r);
                            if (!string.IsNullOrEmpty(json))
                                res1 = JObject.Parse(json);
                        }

                        //get assessment infos
                        var rai = ALS.AssessmentInfos
                            .Where(x => x.AssessmentId == r.AssessmentId)
                            .Select(t => new { t.field_name, t.field_value })
                            .ToDictionary(t => t.field_name, t => t.field_value);
                        if (rai != null)
                        {
                            json = JsonConvert.SerializeObject(rai, new KeyValuePairConverter());
                            if (!string.IsNullOrEmpty(json))
                                res2 = JObject.Parse(json);
                        }
                        if (res1 != null)
                        {
                            if (res2 != null)
                                res1.Merge(res2, new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Concat });
                        }
                        return Json(new
                        {
                            Result = 2,
                            Data = res1.ToObject<Dictionary<string, object>>(),
                            Points = AssPoints,
                            Assessments = AssNums
                        }, JsonRequestBehavior.AllowGet);
                        //return new ContentResult { Content = res1.ToString(), ContentType = "application/json" };
                    }
                    return Json("");
                }
                catch (Exception E)
                {
                    var ind = "<b>obj.Func == " + obj.Func + "</b><br>";
                    ind += "Line Number: " + E.LineNumber() + "<br>";
                    LogError("MarketerController/GetFormData", "Error message: " + E.Message);
                    return Json(E);
                }
            };
        }

        public ActionResult PrintAssessment(string id)
        {
            long _id = 0;

            try
            {
                if (long.TryParse(id, out _id))
                {
                    var ALS = new ALSBaseDataContext();


                    var r_old = (from x in ALS.Assessments
                                 join y in ALS.Residents on x.ResidentId equals y.resident_id
                                 where x.AssessmentId == _id
                                 select new
                                 {
                                     AssessmentId = x.AssessmentId,
                                     FirstName = y.first_name,
                                     MiddleInitial = y.middle_initial,
                                     LastName = y.last_name,
                                     CompletedBy = x.CompletedBy,
                                     TotalScore = x.TotalScore,
                                     CompletedDate = x.CompletedDate// CHECK: Utility.ToTimezonedDateTime(x.CompletedDate) not working
                                 }
                            ).SingleOrDefault();
                    var r = new // lol
                    {
                        AssessmentId = r_old.AssessmentId,
                        FirstName = r_old.FirstName,
                        MiddleInitial = r_old.MiddleInitial,
                        LastName = r_old.LastName,
                        CompletedBy = r_old.CompletedBy,
                        TotalScore = r_old.TotalScore,
                        CompletedDate = r_old.CompletedDate == null ? "" : r_old.CompletedDate.Value.ToShortDateString()
                    };
                    var AssPoints = ALS.AssessmentPointSettings
                                .Select(t => new { t.FieldId, t.ScoreValue })
                                .ToDictionary(t => t.FieldId, t => t.ScoreValue);
                    //var AssPoints = ALS.AssessmentPointSettings
                    //          .Select(t => new { t.FieldId, t.ScoreValue })
                    //          .ToDictionary(t => t.FieldId, t => t.ScoreValue);
                    var rai = ALS.AssessmentInfos
                                .Where(x => x.AssessmentId == _id)
                                .Select(t => new { t.field_name, t.field_value })
                                .ToDictionary(t => t.field_name, t => t.field_value);

                    var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                    var output = new MemoryStream();
                    var writer = PdfWriter.GetInstance(document, output);
                    document.Open();
                    string contents = System.IO.File.ReadAllText(Server.MapPath("~/Content/HTMTemplate/ass_template.htm"), Encoding.Unicode);
                    contents = contents.Replace("[ClientID]", AgencyID);
                    contents = contents.Replace("[FirstName]", r.FirstName);
                    contents = contents.Replace("[MiddleInitial]", r.MiddleInitial);
                    contents = contents.Replace("[LastName]", r.LastName);
                    contents = contents.Replace("[CompletedBy]", r.CompletedBy);
                    contents = contents.Replace("[TotalScore]", r.TotalScore);
                    contents = contents.Replace("[CompletedDate]", r.CompletedDate);
                    foreach (KeyValuePair<string, double?> eve in AssPoints)
                    {
                        contents = contents.Replace("[P_" + eve.Key + "]", eve.Value.ToString() + " pt");
                    }
                    foreach (KeyValuePair<string, string> eve in rai)
                    {
                        if (eve.Value != null)
                        {
                            if (eve.Value.ToLower() == "true")
                                contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                            else if (eve.Value.ToLower() == "false")
                                contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                            else
                                contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                        }

                    }
                    FontFactory.Register(Server.MapPath("~/fonts/ARIALUNI.TTF"));

                    StyleSheet ST = new iTextSharp.text.html.simpleparser.StyleSheet();
                    ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                    ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                    var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                    foreach (var htmlElement in parsedHtmlElements)
                        document.Add(htmlElement as IElement);
                    document.Close();
                    //Response.ContentType = "application/pdf";
                    //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                    //Response.BinaryWrite(output.ToArray());
                    var cd = new System.Net.Mime.ContentDisposition
                    {
                        FileName = "Assessment.pdf",
                        Inline = true
                    };

                    Response.AppendHeader("Content-Disposition", cd.ToString());
                    return File(output.ToArray(), "application/pdf");
                }
            }
            catch (Exception E)
            {
                LogError("MarketerController/PrintAssessment", "Error message: " + E.Message);
            }
            return Json("");
        }
        public ActionResult PrintMorseFallScale(string id)
        {

            long _id = 0;
            try
            {
                if (long.TryParse(id, out _id))
                {

                    var ALS = new ALSBaseDataContext();

                    var rai = ALS.AssessmentInfos
                                .Where(x => x.AssessmentId == _id)
                                .Select(t => new { t.field_name, t.field_value })
                                .ToDictionary(t => t.field_name, t => t.field_value);
                    var document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 25, 25);
                    var output = new MemoryStream();
                    var writer = PdfWriter.GetInstance(document, output);
                    document.Open();
                    string contents = System.IO.File.ReadAllText(Server.MapPath("~/Content/HTMTemplate/mfs_template.htm"), Encoding.Unicode);
                    foreach (KeyValuePair<string, string> eve in rai)
                    {
                        if (eve.Value.ToLower() == "true")
                            contents = contents.Replace("[" + eve.Key + "]", "&#x2611;");
                        else if (eve.Value.ToLower() == "false")
                            contents = contents.Replace("[" + eve.Key + "]", "&#x2610;");
                        else
                            contents = contents.Replace("[" + eve.Key + "]", string.IsNullOrEmpty(eve.Value.Trim()) ? "&nbsp;" : eve.Value);
                    }
                    FontFactory.Register(Server.MapPath("~/fonts/ARIALUNI.TTF"));

                    StyleSheet ST = new StyleSheet();
                    ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.FACE, "Arial Unicode MS");
                    ST.LoadTagStyle(HtmlTags.BODY, HtmlTags.ENCODING, BaseFont.IDENTITY_H);

                    var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), ST);
                    foreach (var htmlElement in parsedHtmlElements)
                        document.Add(htmlElement as IElement);
                    document.Close();
                    //Response.ContentType = "application/pdf";
                    //Response.AddHeader("Content-Disposition", "attachment;filename=Sample.pdf");
                    //Response.BinaryWrite(output.ToArray());
                    var cd = new System.Net.Mime.ContentDisposition
                    {
                        FileName = "MorseFallScale.pdf",
                        Inline = true
                    };

                    Response.AppendHeader("Content-Disposition", cd.ToString());
                    return File(output.ToArray(), "application/pdf");
                }
            }
            catch (Exception E)
            {
                LogError("MarketerController/PrintMorseFallScale", "Error message: " + E.Message);
            }

            return Json("");
        }
    }
}