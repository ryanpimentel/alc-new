﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Datasoft.AssistedLiving.Web.Startup))]
namespace Datasoft.AssistedLiving.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
