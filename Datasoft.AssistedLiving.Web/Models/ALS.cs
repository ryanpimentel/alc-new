using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.ComponentModel;
using System;
namespace Datasoft.AssistedLiving.Web.Models {
    public partial class ALSDataContext {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.getDailyMedInfo")]
        [ResultType(typeof(DailyMedsResidentInfo))]
        [ResultType(typeof(DailyMedsResidentMeds))]
        [ResultType(typeof(DailyMedsScheduleMeds))]
        [ResultType(typeof(DailyMeds))]
        public IMultipleResults getDailyMedInfos([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> residentid, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Date")] System.Nullable<System.DateTime> date_administered) {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), residentid, date_administered);
            return ((IMultipleResults)(result.ReturnValue));
        }
        //[global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.getSupplies")]
        //public ISingleResult<getSuppliesResult> getSupplies(
        //    [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> pagesize, 
        //    [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> pagenum, 
        //    [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(100)")] string sortcol, 
        //    [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> sortorder,
        //    [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(255)")] string searchkey, 
        //    [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> searchcriteria,
        //    [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Bit")] System.Nullable<bool> isactive) {
        //    IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pagesize, pagenum, sortcol, sortorder, searchkey, searchcriteria, isactive);
        //    return ((ISingleResult<getSuppliesResult>)(result.ReturnValue));
        //}
    }
    
    public partial class DailyMedsResidentMeds {

        private System.Nullable<int> _admission_id;

        private System.Nullable<int> _resident_id;

        private System.Nullable<int> _receive_detail_id;

        private string _medicine;

        private string _dosage;

        public DailyMedsResidentMeds() {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_admission_id", DbType = "Int")]
        public System.Nullable<int> admission_id {
            get {
                return this._admission_id;
            }
            set {
                if ((this._admission_id != value)) {
                    this._admission_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_resident_id", DbType = "Int")]
        public System.Nullable<int> resident_id {
            get {
                return this._resident_id;
            }
            set {
                if ((this._resident_id != value)) {
                    this._resident_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_receive_detail_id", DbType = "Int")]
        public System.Nullable<int> receive_detail_id {
            get {
                return this._receive_detail_id;
            }
            set {
                if ((this._receive_detail_id != value)) {
                    this._receive_detail_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_medicine", DbType = "NVarChar(150)")]
        public string medicine {
            get {
                return this._medicine;
            }
            set {
                if ((this._medicine != value)) {
                    this._medicine = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_dosage", DbType = "NVarChar(150)")]
        public string dosage {
            get {
                return this._dosage;
            }
            set {
                if ((this._dosage != value)) {
                    this._dosage = value;
                }
            }
        }
    }

    public partial class DailyMedsScheduleMeds {

        private System.Nullable<int> _admission_id;

        private System.Nullable<int> _resident_id;

        private System.Nullable<int> _receive_detail_id;

        private System.Nullable<int> _daily_med_id;

        private System.Nullable<TimeSpan> _intake_time;


        public DailyMedsScheduleMeds() {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_admission_id", DbType = "Int")]
        public System.Nullable<int> admission_id {
            get {
                return this._admission_id;
            }
            set {
                if ((this._admission_id != value)) {
                    this._admission_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_resident_id", DbType = "Int")]
        public System.Nullable<int> resident_id {
            get {
                return this._resident_id;
            }
            set {
                if ((this._resident_id != value)) {
                    this._resident_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_receive_detail_id", DbType = "Int")]
        public System.Nullable<int> receive_detail_id {
            get {
                return this._receive_detail_id;
            }
            set {
                if ((this._receive_detail_id != value)) {
                    this._receive_detail_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_daily_med_id", DbType = "Int")]
        public System.Nullable<int> daily_med_id {
            get {
                return this._daily_med_id;
            }
            set {
                if ((this._daily_med_id != value)) {
                    this._daily_med_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_intake_time", DbType = "Time")]
        public System.Nullable<TimeSpan> intake_time {
            get {
                return this._intake_time;
            }
            set {
                if ((this._intake_time != value)) {
                    this._intake_time = value;
                }
            }
        }
        
    }

    public partial class DailyMedsResidentInfo {

        private int _resident_id;

        private string _resident_name;

        private string _room_number;

        private System.Nullable<System.DateTime> _dateofbirth;

        private string _pharmacy;

        private string _telephone;

        private string _sex;

        public DailyMedsResidentInfo() {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_resident_id", DbType = "Int NOT NULL")]
        public int resident_id {
            get {
                return this._resident_id;
            }
            set {
                if ((this._resident_id != value)) {
                    this._resident_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_resident_name", DbType = "NVarChar(303)")]
        public string resident_name {
            get {
                return this._resident_name;
            }
            set {
                if ((this._resident_name != value)) {
                    this._resident_name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_room_number", DbType = "NVarChar(150)")]
        public string room_number {
            get {
                return this._room_number;
            }
            set {
                if ((this._room_number != value)) {
                    this._room_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_dateofbirth", DbType = "DateTime")]
        public System.Nullable<System.DateTime> dateofbirth {
            get {
                return this._dateofbirth;
            }
            set {
                if ((this._dateofbirth != value)) {
                    this._dateofbirth = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_pharmacy", DbType = "NVarChar(50)")]
        public string pharmacy {
            get {
                return this._pharmacy;
            }
            set {
                if ((this._pharmacy != value)) {
                    this._pharmacy = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_telephone", DbType = "NVarChar(150)")]
        public string telephone {
            get {
                return this._telephone;
            }
            set {
                if ((this._telephone != value)) {
                    this._telephone = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_sex", DbType = "VarChar(6) NOT NULL", CanBeNull = false)]
        public string sex {
            get {
                return this._sex;
            }
            set {
                if ((this._sex != value)) {
                    this._sex = value;
                }
            }
        }
    }

    public partial class DailyMeds {

        private int _daily_med_id;

        private int _receive_detail_id;

        private bool _is_given;

        private string _comment;

        private System.Nullable<System.DateTime> _date_administered;

        private string _administered_by;

        

        public DailyMeds() {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_daily_med_id", DbType = "Int NOT NULL")]
        public int daily_med_id {
            get {
                return this._daily_med_id;
            }
            set {
                if ((this._daily_med_id != value)) {
                    this._daily_med_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_receive_detail_id", DbType = "Int NOT NULL")]
        public int receive_detail_id {
            get {
                return this._receive_detail_id;
            }
            set {
                if ((this._receive_detail_id != value)) {
                    this._receive_detail_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_is_given", DbType = "Bit NOT NULL")]
        public bool is_given {
            get {
                return this._is_given;
            }
            set {
                if ((this._is_given != value)) {
                    this._is_given = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_comment", DbType = "NVarChar(MAX)")]
        public string comment {
            get {
                return this._comment;
            }
            set {
                if ((this._comment != value)) {
                    this._comment = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_date_administered", DbType = "Date")]
        public System.Nullable<System.DateTime> date_administered {
            get {
                return this._date_administered;
            }
            set {
                if ((this._date_administered != value)) {
                    this._date_administered = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_administered_by", DbType = "VarChar(50)")]
        public string administered_by {
            get {
                return this._administered_by;
            }
            set {
                if ((this._administered_by != value)) {
                    this._administered_by = value;
                }
            }
        }
    }

    //public partial class getSuppliesResult {

    //    private System.Nullable<int> _pagenum;

    //    private System.Nullable<int> _records;

    //    private System.Nullable<decimal> _total;

    //    private int _supply_id;

    //    private string _description;

    //    private string _packaging;

    //    private string _uom;

    //    private string _supply_code;

    //    private System.Nullable<bool> _status;

    //    private System.Nullable<decimal> _cost;

    //    private string _product_code;

    //    private string _product_type;

    //    private string _generic;

    //    private string _form;

    //    private string _strength;

    //    private string _reference_drug;

    //    private string _active_ingredient;

    //    public getSuppliesResult() {
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_pagenum", DbType = "Int")]
    //    public System.Nullable<int> pagenum {
    //        get {
    //            return this._pagenum;
    //        }
    //        set {
    //            if ((this._pagenum != value)) {
    //                this._pagenum = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_records", DbType = "Int")]
    //    public System.Nullable<int> records {
    //        get {
    //            return this._records;
    //        }
    //        set {
    //            if ((this._records != value)) {
    //                this._records = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_total", DbType = "Decimal(37,0)")]
    //    public System.Nullable<decimal> total {
    //        get {
    //            return this._total;
    //        }
    //        set {
    //            if ((this._total != value)) {
    //                this._total = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_supply_id", DbType = "Int NOT NULL")]
    //    public int supply_id {
    //        get {
    //            return this._supply_id;
    //        }
    //        set {
    //            if ((this._supply_id != value)) {
    //                this._supply_id = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_description", DbType = "NVarChar(150)")]
    //    public string description {
    //        get {
    //            return this._description;
    //        }
    //        set {
    //            if ((this._description != value)) {
    //                this._description = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_packaging", DbType = "NVarChar(150)")]
    //    public string packaging {
    //        get {
    //            return this._packaging;
    //        }
    //        set {
    //            if ((this._packaging != value)) {
    //                this._packaging = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_uom", DbType = "NVarChar(50)")]
    //    public string uom {
    //        get {
    //            return this._uom;
    //        }
    //        set {
    //            if ((this._uom != value)) {
    //                this._uom = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_supply_code", DbType = "VarChar(20)")]
    //    public string supply_code {
    //        get {
    //            return this._supply_code;
    //        }
    //        set {
    //            if ((this._supply_code != value)) {
    //                this._supply_code = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_status", DbType = "Bit")]
    //    public System.Nullable<bool> status {
    //        get {
    //            return this._status;
    //        }
    //        set {
    //            if ((this._status != value)) {
    //                this._status = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cost", DbType = "Decimal(6,2)")]
    //    public System.Nullable<decimal> cost {
    //        get {
    //            return this._cost;
    //        }
    //        set {
    //            if ((this._cost != value)) {
    //                this._cost = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_product_code", DbType = "VarChar(50)")]
    //    public string product_code {
    //        get {
    //            return this._product_code;
    //        }
    //        set {
    //            if ((this._product_code != value)) {
    //                this._product_code = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_product_type", DbType = "VarChar(50)")]
    //    public string product_type {
    //        get {
    //            return this._product_type;
    //        }
    //        set {
    //            if ((this._product_type != value)) {
    //                this._product_type = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_generic", DbType = "NVarChar(1000)")]
    //    public string generic {
    //        get {
    //            return this._generic;
    //        }
    //        set {
    //            if ((this._generic != value)) {
    //                this._generic = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_form", DbType = "NVarChar(1000)")]
    //    public string form {
    //        get {
    //            return this._form;
    //        }
    //        set {
    //            if ((this._form != value)) {
    //                this._form = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_strength", DbType = "NVarChar(1000)")]
    //    public string strength {
    //        get {
    //            return this._strength;
    //        }
    //        set {
    //            if ((this._strength != value)) {
    //                this._strength = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_reference_drug", DbType = "NVarChar(1000)")]
    //    public string reference_drug {
    //        get {
    //            return this._reference_drug;
    //        }
    //        set {
    //            if ((this._reference_drug != value)) {
    //                this._reference_drug = value;
    //            }
    //        }
    //    }

    //    [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_active_ingredient", DbType = "NVarChar(1000)")]
    //    public string active_ingredient {
    //        get {
    //            return this._active_ingredient;
    //        }
    //        set {
    //            if ((this._active_ingredient != value)) {
    //                this._active_ingredient = value;
    //            }
    //        }
    //    }
    //}
}