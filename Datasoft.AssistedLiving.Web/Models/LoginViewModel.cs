﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Datasoft.AssistedLiving.Web.Models { 
    public class LoginViewModel {
        public string AgencyID {  get; set; }
        public string UserID { get; set; }
    }
}