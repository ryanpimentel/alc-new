﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Datasoft.AssistedLiving.Web.Models
{
    public class ResidentGridRow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Firstname { get; set; }
        public string MiddleInitial { get; set; }
        public string Lastname { get; set; }
        public string Gender { get; set; }
        public string SSN { get; set; }
        public string Birthdate { get; set; }
        public int? RoomId { get; set; }
        public string RoomName { get; set; }
        public bool IsDNR { get; set; }
        //'AdmissionDate' added by Mariel P. 
        public string AdmissionDate { get; set; }
        public string ResidentStatus { get; set; }
        //public string Image { get; set; }
        public string ResponsiblePerson { get; set; }
        public string ResponsiblePersonEmail { get; set; }
        public string ResponsiblePersonTelephone { get; set; }
        public string ResponsiblePersonAddress { get; set; }
        //'AddressPriorAdmission' and Check added kim 12/03/2021
        public string AddressPriorAdmission { get; set; }
        public string Check { get; set; }
        public string Physician { get; set; }
        public string PhysicianPhone { get; set; }
        public string AdmissionId { get; set; }
        public string PhysicianFax { get; set; }
        public string PhysicianEmail { get; set; }

        public string comment { get; set; }
        public bool isAdmitted { get; set; }
    }
    public class UserGridRow
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string MiddleInitial { get; set; }
        public string Lastname { get; set; }
        public string Name { get; set; }
        public string RoleDescription { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public string IsActive { get; set; }
        public string Shift { get; set; }
        public string IsForce { get; set; }
        public string DateCreated { get; set; }
        public string DateModified { get; set; }
        public string NonSysUser { get; set; }
        //Inserted column SSN on Admin/Employees - by Alona Cabrias June 21, 2017
        public string SSN { get; set; }
        //inserted column imagesrc on ALCUser - Mariel
        //public string Image { get; set; }
        public string ShiftType { get; set; }
        public string DaySchedule { get; set; }
        public string DayScheduleDisplay { get; set; }
        public string CreatedBy { get; set; }
        public string ShiftDateCreated { get; set; }
    }

    public class RoleGridRow
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool SysDefined { get; set; }
        public string DateCreated { get; set; }
        public string DateModified { get; set; }
        public string Responsibility { get; set; }
    }


    public class ReceiveMedsRow
    {
        public int Id { get; set; }
        public int? SupplyId { get; set; }
        public double? Qty { get; set; }
        public int? Dosage { get; set; }
        public string Freq { get; set; }
        public string Generic { get; set; }
        public string SupplyName { get; set; }
        public string ReceiveDate { get; set; }
        public bool? IsPRN { get; set; }
        public string PRNDate { get; set; }
        public int? SupplyCount { get; set; }
        public double? MedDosage { get; set; }
        public double? Quantity_Left { get; set; }
    }

    public class RolesData
    {
        public List<SelectData> Roles { get; set; }
    }

    public class DepartmentsData
    {
        public List<SelectData> Departments { get; set; }
    }

    public class DeptData
    {
        public List<SelectData> Depts { get; set; }
        public string SelectedValue { get; set; }
    }

    public class SelectData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IsAdmitted { get; set; }

       
    }

    public class SelectData2
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public int Role { get; set; }
    }

    public class ActivityData
    {
        public int Id { get; set; }
        public string Desc { get; set; }
        public string Proc { get; set; }
    }

    public class UserData
    {
        public RolesData RolesData { get; set; }
        public ActivityScheduleData ActivityScheduleData { get; set; }
        public DeptData DeptData { get; set; }
        public RolesData RolePerm { get; set; }
    }

    public class DosageData
    {
        public List<SelectData> Dosages { get; set; }
    }
    public class SelectDataAPS
    {
        public int Active { get; set; }
        public string Group { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public double? Score { get; set; }
        public string SubGroup { get; set; }
        // public int Active { get; set; }
    }
    public class ResidentAdmissionData
    {
        public List<SelectData> Facilities { get; set; }
        public List<SelectData> Rooms { get; set; }
        //sample insert code
        public List<ActivityData> Activities { get; set; }
        public List<SelectData> ResidentStatus { get; set; }
        public List<SelectData> Residents { get; set; }
        public DeptData Departments { get; set; }
        //kim add code here for display of assessment point setting 04/06/22
        public List<SelectDataAPS> FC { get; set; }
        //Psycho/Social Capabilities (PSC)
        public List<SelectDataAPS> PSC { get; set; }

        //Cognitive Capabilities (CC)
        public List<SelectDataAPS> CC { get; set; }

        //Special Medicinal Needs (SMN)
        public List<SelectDataAPS> SMN { get; set; }

    }

    public class FacilityMgmtData
    {
        public List<SelectData> Facilities { get; set; }
    }

    public class CareStaffAdmissionData
    {
        public List<SelectData2> CareStaffs { get; set; }
        public List<SelectData> Residents { get; set; }
        public List<SelectData> CarestaffTypes { get; set; }
    }
   
    public class ActivityScheduleData
    {
        //kim add code here for display of assessment point setting 04/06/22
        //Functional Capabilities (FC)
        public List<SelectDataAPS> FC { get; set; }
        //Psycho/Social Capabilities (PSC)
        public List<SelectDataAPS> PSC { get; set; }

        //Cognitive Capabilities (CC)
        public List<SelectDataAPS> CC { get; set; }

        //Special Medicinal Needs (SMN)
        public List<SelectDataAPS> SMN { get; set; }

        public List<SelectDataAPS> AddFC { get; set; }
        //Psycho/Social Capabilities (PSC)
        public List<SelectDataAPS> AddPSC { get; set; }

        //Cognitive Capabilities (CC)
        public List<SelectDataAPS> AddCC { get; set; }

        //Special Medicinal Needs (SMN)
        public List<SelectDataAPS> AddSMN { get; set; }
        public List<SelectData> Residents { get; set; }
        public List<SelectData> Rooms { get; set; }
        public List<ActivityData> Activities { get; set; }
        public DeptData Departments { get; set; }
        public List<SelectData> ResidentStatus { get; set; }
        public List<SelectData2> Users { get; set; }
        public List<SelectData2> Users2 { get; internal set; }
    }



    public class PendingPreadmission
    {
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string MI { get; set; }
        public byte Step { get; set; }
        public int ID { get; set; }
        public string PreAdmissionDate { get; set; }
        public string AdmissionStatus { get; set; }
    }

    public class TimezoneList
    {
        public List<SelectData2> Timezones { get; set; }
        public string SelectedValue { get; set; }
    }


    public class ResidentTabReturn
    {
        public string AppraisalID { get; set; }
        public string AgreementID { get; set; }
    }

}