﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Datasoft.AssistedLiving.Web.Models {
    public class ALSBaseDataContext : ALSDataContext {
 
        public ALSBaseDataContext(string connString) : base(connString) {
            //(Datasoft.AssistedLiving.Security.ALCUserManager.Instance.ConnectionString(Datasoft.AssistedLiving.Security.ALCUserContext.AgencyId)){
        }

        public ALSBaseDataContext() : 
            base(Datasoft.AssistedLiving.Security.ALCUserManager.Instance.ConnectionString(Datasoft.AssistedLiving.Security.ALCUserContext.AgencyId)) {
        }
    }

    public class ALSMasterBaseDataContext : ALSMasterDataContext {
        public ALSMasterBaseDataContext(string connString) : base(connString) {
        }

        public ALSMasterBaseDataContext() :
            base(global::System.Configuration.ConfigurationManager.ConnectionStrings["AssistedLivingMasterConnectionString"].ConnectionString) {
        }
    }
}