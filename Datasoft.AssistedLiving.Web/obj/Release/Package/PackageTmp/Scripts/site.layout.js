function Querystring(qs) {
    this.params = new Object()
    this.get = Querystring_get

    if (qs == null)
        qs = location.search.substring(1, location.search.length)

    if (qs.length == 0) return

    qs = qs.replace(/\+/g, ' ')
    var args = qs.split('&') // parse out name/value pairs separated via &

    for (var i = 0; i < args.length; i++) {
        var value;
        var pair = args[i].split('=')
        var name = unescape(pair[0])

        if (pair.length == 2)
            value = unescape(pair[1])
        else
            value = name

        this.params[name] = value
    }
}

function Querystring_get(key, default_) {
    if (default_ == null) default_ = null;

    var value = this.params[key]
    if (value == null) value = default_;

    return value
}
var MHL, PHL, AHL, MPL, THL;
$(document).ready(function () {

    function capsFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    var info = $.cookie('ALCInfo');
    if (info && info != '') {
        var ins = info.split('|');
        var name = '', agency = '';
        if (ins.length > 1) {
            name = ins[1] + '!';
            agency = ins[0];
        }
        //$('#lblUserId').html(capsFirstLetter(name));
        $('#lblAgencyName').html(agency.toUpperCase());
    }

    $('body').layout({
        center: {
            paneSelector: '#mainContainer',
            size: 'auto'
        }
    });
    MHL = $('#mainContainer');
    MHL.layout({
        center: {
            paneSelector: '#contentContainer',
            size: 'auto',
            closable: false,
            resizable: false,
            slidable: false
        },
        north: {
            paneSelector: '#headerContainer',
            size: 40,
            closable: false,
            resizable: false,
            slidable: false,
            spacing_open: 0,
            spacing_close: 0
        },
        south: {
            paneSelector: '#footerContainer',
            size: 40,
            minSize: 40,
            maxSize: 40,
            closable: false,
            resizable: false,
            slidable: false,
            spacing_open: 0,
            spacing_close: 0
        }
    });
    $(window).resize(function () {
        if (typeof (MHL) !== 'undefined') {
            MHL.layout('resizeAll');
        }
        if (typeof (AHL) !== 'undefined') {
            AHL.layout('resizeAll');
        }
        if (typeof (DML) !== 'undefined') {
            DML.resize();
        }
    });

    $("#footerContainer ul.nav li:last-child").addClass('noBorder');

    $('#preAdmissionLink').click(function () {
        var $canceldialog;
        var $dialog = $("<div><div id=\"intake_content\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Pre-Admission Wizard",
            width: $(window).width() - 400,
            height: $(window).height() - 100,
            close: function () {
                $dialog.dialog("destroy").remove();
            },
            beforeClose: function (e, ui) {
                $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
                    modal: true,
                    title: "Confirmation",
                    width: 400,
                    buttons: {
                        "Yes": function () {
                            $canceldialog.dialog("destroy").remove();
                            $dialog.dialog("destroy").remove();
                            //window.location = ClientUrls["Home"];
                        },
                        "No": function () {
                            $canceldialog.dialog("destroy").remove();
                        }
                    }
                });
                return false;
            },
            buttons: {
                "Save": {
                    click: function () {
                        var doAjax = function (idx) {
                            var tabEnum = { APPRAISAL: 1, EMERGENCY: 2, PHYSICIAN_REPORT: 3 };
                            var row = { Func: 'preplacement_wizard' };
                            var isValid = true;
                            var id = $('#residentIdField').val();
                            var mode = $.isNumeric(id) ? 2 : 1;

                            if (idx == 0) {
                                isValid = $('#content-appraisal').validate();
                                //do other modes
                                if (mode == 1 || mode == 2) {
                                    if (!isValid) return false;
                                    row.TabIndex = tabEnum.APPRAISAL;
                                    row.Mode = mode;
                                    row.Data = $('#content-appraisal').extractJson('id');
                                }
                            } else if (idx == 1) {
                                row.Data = $('#content-emergencyinfo').extractJson('id');
                                row.TabIndex = tabEnum.EMERGENCY;
                                row.Mode = mode;
                            } else if (idx == 2) {
                                row.Data = $('#content-physicianreport').extractJson('id');
                                row.Data.residentIdField =
                                row.TabIndex = tabEnum.PHYSICIAN_REPORT;
                                row.Mode = mode;
                            }

                            if (mode == 2)
                                row.Data.residentIdField = id;

                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: ALC_URI.Admin + "/PostGridData",
                                data: JSON.stringify(row),
                                dataType: "json",
                                success: function (m) {
                                    if (mode == 1)
                                        $('#residentIdField').val(m.result);
                                }
                            });

                            return true;
                        }
                        var tab = $("#PlacementForm");
                        var idx = tab.tabs('option', 'active');

                        if (!doAjax(idx)) return;

                        if (idx == 0) {
                            tab.tabs('option', 'disabled', [2]);
                            tab.tabs('option', 'selected', 1);
                        } else if (idx == 1) {
                            tab.tabs('option', 'disabled', []);
                            tab.tabs('option', 'selected', 2);
                        } else if (idx == 2) {
                            $okdialog = $("<div><div class=\"confirm\">Pre-Admission info has been saved.</div></div>").dialog({
                                modal: true,
                                title: "Success",
                                width: 400,
                                buttons: {
                                    "Ok": function () {
                                        $okdialog.dialog("destroy").remove();
                                        $dialog.dialog("destroy").remove();
                                        if (typeof (Dashboard) == 'undefined') return;
                                        Dashboard.BuildPendingAdmission();
                                    }
                                }
                            });
                            return false;
                        }

                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $dialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {
                var diag = $('.ui-dialog-content');
                diag = diag.find('#PlacementForm').parents('.ui-dialog-content');
                var height = 0;
                height = diag.eq(0).height() || 0;
                if (height <= 0)
                    height = diag.eq(1).height() || 0;
                $("#appraisalContainer,#PlacementForm").height(height);
                $('#tabsContent').height(height - 20);
                $('#Appraisal,#EmergencyInfo,#PhysicianReport').height(height - 50);
            }
        });
        $.ajax({
            url: ALC_URI.Admin + "/GetNodeData",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            data: '{id:"preadmission_0"}',
            success: function (d) {
                $("#intake_content").empty();
                $("#intake_content").html(d);
                $('span.checklist').click(function () {
                    var idx = 0;
                    if (this.id == 'chkPhysical')
                        idx = 1;
                    else if (this.id == 'chkMental')
                        idx = 2;
                    else if (this.id == 'chkSocial')
                        idx = 0;
                    else if (this.id == 'chkFunctional')
                        idx = 3;
                    checklistLink(idx, this);
                });
                /**************************************
                // sample how to retrieve data and populate to fields
                var data = { func: "preplacement_wizard", tabindex: 1, id: '24' };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: ALC_URI.Admin + "/GetFormData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        $(".form").mapJson(res, 'id');
                    }
                });
                */
            },
            complete: function () { },
            error: function () { }
        });
        return false;
    });

    //$('#preflink').click(function () {
    //    var $dialog = $("<div><div id=\"prefCont\" ></div></div>").dialog({
    //        modal: true,
    //        closeOnEscape: false,
    //        title: "Preferences",
    //        width: 500,
    //        height: 200,
    //        close: function () {
    //            $dialog.dialog("destroy").remove();
    //        },
    //        buttons: {
    //            "Save": {
    //                click: function () {
    //                    var row = { Func: 'preferences', Data: { timezoneid: $('#ddlTz').val(), authTimeout: $('#authTimeout').val() } };
    //                    $.ajax({
    //                        type: "POST",
    //                        contentType: "application/json; charset=utf-8",
    //                        url: ALC_URI.Admin + "/PostGridData",
    //                        data: JSON.stringify(row),
    //                        dataType: "json",
    //                        success: function (m) {
    //                            if (m.result == 0) {
    //                                $.growlSuccess({ message: "Preferences saved successfully!", delay: 6000 });
    //                                setTimeout(function () {
    //                                    window.location.reload();
    //                                }, 1500);
    //                            } else if (m.result == -4) {
    //                                $.growlWarning({ message: m.message, delay: 6000 });
    //                                return;
    //                            }
    //                            $dialog.dialog("destroy").remove();
    //                        }
    //                    });

    //                },
    //                class: "primaryBtn",
    //                text: "Save"
    //            },
    //            "Cancel": function () {
    //                $dialog.dialog("destroy").remove();
    //            }
    //        },
    //        resize: function (event, ui) {

    //        }
    //    });

    //    //sample how to retrieve data and populate to fields
    //    $.ajax({
    //        url: ALC_URI.Admin + "/GetNodeData",
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "html",
    //        type: "POST",
    //        data: '{id:"preferences_0"}',
    //        success: function (d) {
    //            $('#prefCont').empty();
    //            $('#prefCont').html(d);
    //        },
    //        complete: function () { },
    //        error: function () { }
    //    });
    //    return false;
    //});

    $('#userLink').click(function () {
        var $dialog = $("<div><div id=\"profCont\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Profile",
            width: $(window).width() - 700,
            height: 420,
            close: function () {
                $dialog.dialog("destroy").remove();
            },
            buttons: {
                "Save": {
                    click: function () {
                        if ($('#ProfileForm #txtPassword').val() == '')
                            $('#ProfileForm #txtConfirm').attr('class', 'w200px');
                        else
                            $('#ProfileForm #txtConfirm').attr('class', 'w200px required');

                        var isValid = $('#ProfileForm').validate();
                        if (!isValid) return;

                        var row = $('#ProfileForm').extractJson();
                        var data = { func: "user", mode: 2, data: row };
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: ALC_URI.Admin + "/PostGridData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (m.result == 0)
                                    $.growlSuccess({ message: "Profile saved successfully!", delay: 6000 });
                                else if (m.result == -4) {
                                    $.growlWarning({ message: m.message, delay: 6000 });
                                    return;
                                }
                                $dialog.dialog("destroy").remove();
                            }
                        });

                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $dialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {

            }
        });
        //sample how to retrieve data and populate to fields
        $.ajax({
            url: ALC_URI.Admin + "/GetNodeData",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            data: '{id:"userprofile_0"}',
            success: function (d) {
                $('#profCont').empty();
                $('#profCont').html(d);
                $('#ProfileForm #txtPass').show();
                if (!$("#ProfileForm").data("tabs")) {
                    $("#ProfileForm").tabs();
                }
                $("#ProfileForm").tabs("option", "selected", 0);
                $('#ProfileForm #txtBirthDate2').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+0"
                });
                $('#ProfileForm #txtPhone,#ProfileForm #txtFax').mask("(999)999-9999");

                var u = $('#lblUserId').html();
                var uid = u.substring(0, u.length - 1);
                var data = { func: "user", id: uid };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/GetFormData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {
                        $('#ProfileForm').mapJson(m);
                    }
                });
                $('#ProfileForm #txtPass').focus(function () {
                    $(this).hide();
                    $('#ProfileForm #txtPassword').show().focus();
                });
                $('#ProfileForm #txtPassword').blur(function () {
                    if ($(this).val() == '') {
                        $(this).hide();
                        $('#ProfileForm #txtPass').show();
                    }
                });



            },
            complete: function () { },
            error: function () { }
        });
        return false;
    });

    var checklistLink = function (idx, target) {
        var $canceldialog;
        var ht = 480;
        if (idx == 1 || idx == 2)
            ht = 330;
        else if (idx == 3)
            ht = 430;
        var $dialog = $("<div><div id=\"appraisalChecklist_content\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Checklist",
            width: $(window).width() - 350,
            height: ht,
            close: function () {
                $dialog.dialog("destroy").remove();
            },
            buttons: {
                "Apply": {
                    click: function () {
                        var listVal = [];
                        var checkboxes = $("#ChecklistForm").find('input:checkbox').each(function () {
                            var $box = $(this);
                            if ($box.is(":checked")) {
                                var value = $box.parent().text();
                                var hIdx = $box.parent().parent().index();
                                var header = $box.parents('table:eq(0)').find('thead th:eq(' + hIdx + ')').text();
                                listVal.push(header + ":\r\t");
                                listVal.push(value + '\n\n');
                                if (target && target.id == 'chkFunctional')
                                    $(target).parent().next().find('textarea').val(listVal.join(''));
                                else
                                    $(target).next().find('textarea').val(listVal.join(''));
                            }
                        });
                        $dialog.dialog("destroy").remove();
                    },
                    class: "primaryBtn",
                    text: "Apply"
                },
                "Cancel": function () {
                    $dialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {
                var diag = $('.ui-dialog-content');
                diag = diag.find('#ChecklistForm').parents('.ui-dialog-content');
                var height = 0;
                height = diag.eq(0).height() || 0;
                if (height <= 0)
                    height = diag.eq(1).height() || 0;
                $("#checklistContainer,#ChecklistForm").height(height);
                $('#checklistTabsContent').height(height - 20);
                $('#social_emotional,#mental_physical,#func_skills').height(height - 50);
            }
        });
        $.ajax({
            url: ALC_URI.Admin + "/GetNodeData",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            data: '{id:"prechecklist_0"}',
            success: function (d) {
                $("#appraisalChecklist_content").empty();
                $("#appraisalChecklist_content").html(d);

                var tab = $("#ChecklistForm");
                tab.tabs('option', 'selected', idx);
                tab.find('li:eq(' + idx + ')').removeClass('ui-state-hidden');
                var otherIdx = [];
                if (idx == 0) otherIdx = [1, 2, 3];
                else if (idx == 1) otherIdx = [0, 2, 3];
                else if (idx == 2) otherIdx = [0, 1, 3];
                else if (idx == 3) otherIdx = [0, 1, 2];
                $.each(otherIdx, function () {
                    tab.find('li:eq(' + this + ')').addClass('ui-state-hidden');
                });
            },
            complete: function () { },
            error: function () { }
        });
        return false;
    }
    $('#logoutLink').click(function () {
        $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to logout?</div></div>").dialog({
            modal: true,
            title: "Confirmation",
            width: 400,
            buttons: {
                "Yes": function () {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: ALC_URI.Login + "/LogOff",
                        success: function (m) {
                            if (m.Result == 1) {
                                $canceldialog.dialog("destroy").remove();
                                location.href = m.RedirectUrl;
                            }
                        }
                    });
                },
                "No": function () {
                    $canceldialog.dialog("destroy").remove();
                }
            }
        });
        return false;
    });
    if (typeof ALC_Util != 'undefined') {
        if (ALC_Util.IsAuthenticated) {
            setInterval(function () {
                //var d = moment($('#curDate').val()).format("MM/DD/YYYY");
                //if (d == "Invalid date")
                //    return; //d = moment(new Date()).format("MM/DD/YYYY");
                //var param = d + moment(new Date()).format("hh:mm A");
                $.get(ALC_URI.Home + "/CT", function (data) {
                    $('#spCT').html(data);
                    //if staff landing page, do client side refresh of missed tasks based on current time fetched
                    if (window.location.href.toLowerCase().indexOf("/staff") != -1) {
                        var tds = $("#grid tr.jqgrow > td:nth-child(10)");
                        if (tds != null && tds.size() > 0) {
                            //traverse each time and compare it to the currently fetch time
                            for (var x = 0; x < tds.length; ++x) {
                                var tdtime = moment($('#curDate').val() + ' ' + $(tds[x]).text(), 'MM/DD/YYYY HH:mm A');
                                var cftime = moment(data, "ddd, MMM DD YYYYY HH:mmA"); //data.split(" ");
                                if (cftime.isValid() && tdtime.isValid() && tdtime.isBefore(cftime)) {
                                    //mark row task as missed.
                                    var stat = $("#grid tr.jqgrow > td:nth-child(13):eq(" + x + ")").text();
                                    var ack = $("#grid tr.jqgrow > td:nth-child(14):eq(" + x + ")").text().trim();
                                    //debugger;
                                    if (stat == '0' && ack == "")
                                        $("#grid tr.jqgrow > td:nth-child(12):eq(" + x + ") > div > p:nth-child(1) > label.taskdesc").html("<span class='missed'>Missed</span>");
                                }
                            }
                        }
                    }
                        //if MAR landing page, do client side refresh of missed medication administration based on current time fetched
                    else if (window.location.href.toLowerCase().indexOf("/mar") != -1) {
                        $("td.col8AM,td.col12PM,td.col4PM,td.col8PM").each(function () {
                            var doseTime = moment(ALC_Util.CurrentDate + ' ' + $(this).text(), 'MM/DD/YYYY HH:mm A');
                            var curTime = moment(data, "ddd, MMM DD YYYYY HH:mmA");
                            if (curTime.isValid() && doseTime.isValid() && doseTime.isBefore(curTime)) {
                                //mark medicine as missed administration.
                                //debugger;
                                var cls = this.className && this.className != '' ? this.className.split(' ')[1] : '';
                                if (cls != '') {
                                    var elem = $('a.' + cls);
                                    if (!elem.hasClass('missed'))
                                        elem.addClass('missed');
                                }
                            }
                        });
                    }
                });
                //}, 60000);
            }, 5000);
        }
    }

    //$("#switchLink").on('click', function () {
    //    $(".switch-menu").stop(true, true).slideToggle(200);
    //});
    //$(".switch-menu").on('mouseleave', function () {
    //    $(".switch-menu").stop(true, true).slideToggle(200);
    //});
    $(".switch-menu select").change(function () {
        $.ajax({
            url: ALC_URI.SwitchRole + "/Switch",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: '{rid:"' + $(this).val() + '"}',
            success: function (d) {
                if (d.Result == 1) {
                    window.location.href = d.RedirectUrl;
                }
            },
            complete: function () { },
            error: function () { }
        });
    });
});

if (typeof Grid_Util == 'undefined')
    Grid_Util = {};
Grid_Util.Row = function () {
    var lastSelArrRow = [];
    lastScrollLeft = 0;
    lastSelRow = null;
    this.saveSelection = function () {
        var $grid = $(this);
        lastSelRow = $grid.jqGrid('getGridParam', 'selrow');
        lastSelArrRow = $grid.jqGrid('getGridParam', 'selrow');
        lastSelArrRow = lastSelArrRow ? $.makeArray(lastSelArrRow) : null;
        lastScrollLeft = this.grid.bDiv.scrollLeft;
    }
    this.restoreSelection = function () {
        var p = this.p,
            $grid = $(this);

        p.selrow = null;
        p.selarrrow = [];
        if (p.multiselect && lastSelArrRow && lastSelArrRow.length > 0) {
            for (i = 0; i < lastSelArrRow.length; i++) {
                if (lastSelArrRow[i] !== lastSelRow) {
                    $grid.jqGrid("setSelection", lastSelArrRow[i], false);
                }
            }
            lastSelArrRow = [];
        }
        if (lastSelRow) {
            $grid.jqGrid("setSelection", lastSelRow, false);
            lastSelRow = null;
        }
        this.grid.bDiv.scrollLeft = lastScrollLeft;
    };
}
