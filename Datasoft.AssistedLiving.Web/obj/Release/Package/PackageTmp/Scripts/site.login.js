﻿function QS(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
$(document).ready(function () {
    var token = window.location.search.substring(1);
 
    if (token.length >= 1) {

        var converted_token = atob(token);
        var split1_token = converted_token.split('=');
        var company = split1_token[0];
        var username = split1_token[1];

        $('#txtCompany').val(company);
        $('#txtUsername').val(username);
    }
   

    $('#errMsg').hide();
    $("#txtCompany,#txtUsername,#txtPassword").keyup(function (event) {
        if (event.keyCode == 13) {
            $('#btn-login').click();
        }
    });

    $('#btn-forgot').click(function () {  
        location.href = ALC_URI.Account + "/ResetPassword";
    });

    $('#btn-login').click(function () {
        $('#txtCompany,#txtUsername,#txtPassword').removeClass('fielderror');
        $('#errMsg').hide().find('.fieldlabel').html('');

        var agency = $('#txtCompany').val();
        var user = $('#txtUsername').val();
        var pass = $('#txtPassword').val();

        var err = [];
        if (agency == "") {
            $('#txtCompany').addClass('fielderror');
            err.push('Agency ID is required.');
        }
        if (user == "") {
            $('#txtUsername').addClass('fielderror');
            err.push('User ID is required.');
        }
        if (pass == "") {
            $('#txtPassword').addClass('fielderror');
            err.push('Password is required.');
        }
        if (err.length > 0) {
            $('#errMsg').show().find('.fieldlabel').html(err.join('<br/>'));
            return;
        }
        var data = { AgencyId: agency, UserId: user, Password: pass, RetUrl: QS("ReturnUrl") };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: loginUrl,
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.Result == -1) {
                    $('#errMsg').show().find('.fieldlabel').html(m.ErrorMsg);
                    return;
                }
                location.href = m.RedirectUrl;
            }
        });
    });
});