﻿
$(document).ready(function () {
    
    var info = $.cookie('ALCInfo');
    if (info && info != '') {
        
        var ins = info.split('|');
        var name = '', agency = '';
        if (ins.length > 1) {
            name = ins[1] + '!';
            agency = ins[0];
        }
        //$('#lblUserId').html(capsFirstLetter(name));
        $(document).find("#lblAgencyName").html(agency.toUpperCase());

        if (is_auth == "yes") {
            if (isAuthPage == true){
                $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Admin + "/GetGridData?func=user",
                dataType: "json",
                success: function (m) {


                    $.each(m, function (i, v) {

                        if (v.Id == ins[1]) {
                            var imgsrc = "";

                            if (typeof (btoa) == 'undefined')
                                imgsrc += "Resource/EmpImage?id=" + v.Id + "&b=0&r=" + Date.now() + "' alt='" + v.Name;
                            else
                                imgsrc += "Resource/EmpImage?id=" + btoa(v.Id) + "&b=1&r=" + Date.now() + "' alt='" + v.Name;

                            $("#usrpic").attr("src", imgsrc);
                            $("#usrname").html(v.Name);
                            $("#usremail").html(v.Email);
                        }

                    })
                }
                })
            }
        }

        $('#logoutLink').click(function () {

            Swal({
                title: 'Are you sure you want to logout?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: ALC_URI.Login + "/LogOff",
                        success: function (m) {
                            if (m.Result == 1) {
                               
                                location.href = m.RedirectUrl;
                            }
                        }
                    });
                }
            })
            
            //$canceldialog = $("<div><div class=\"confirm\">Are you sure you want to logout?</div></div>").dialog({
            //    modal: true,
            //    title: "Confirmation",
            //    width: 400,
            //    buttons: {
            //        "Yes": function () {
            //            $.ajax({
            //                type: "POST",
            //                contentType: "application/json; charset=utf-8",
            //                url: ALC_URI.Login + "/LogOff",
            //                success: function (m) {
            //                    if (m.Result == 1) {
            //                        $canceldialog.dialog("destroy").remove();
            //                        location.href = m.RedirectUrl;
            //                    }
            //                }
            //            });
            //        },
            //        "No": function () {
            //            $canceldialog.dialog("destroy").remove();
            //        }
            //    }
            //});
            //return false;
        });
    }
})