$(document).ready(function () {
    $("#dcontainer").layout({
        center: {
            paneSelector: "#staffGrid",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#dnorthcontainer",
            closable: false,
            slidable: false,
            height: 70,
            resizable: false,
            spacing_open: 0,
            north__childOptions: {
                paneSelector: "#dnorth",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            center__childOptions: {
                paneSelector: "#dcenter",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            south_childOptions: {
                paneSelector: "#dsouth",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            }
        },
        onresize: function () {
            $('#staffGrid').height($("#staffGrid").height() - 20);
            resizeGrids();
        }
    });
    $('#staffGrid').height($("#staffGrid").height() - 40);
    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#staffGrid").height() - 24);
            $('#grid').setGridWidth($("#staffGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });


    $('#ftrRole,#ftrStaff,#ftrRoom,#ftrResident,#ftrText').change(function () {
        var res = $('#ftrText').val();
        var ft = $('#ddlFilter').val();
        var rm = $('#ftrRoom :selected').text();
        var rd = $('#ftrResident :selected').text();
        var rl = $('#ftrRole :selected').text();
        var cn = $('#ftrStaff :selected').text();

        var ftrs = {
            groupOp: "AND",
            rules: []
        };

        var col = 'activity_desc';
        if (ft == 2)
            col = "activity_proc";
        else if (rm == 3)
            col = "start_time";

        if (rm != 'All')
            ftrs.rules.push({
                field: 'room',
                op: 'eq',
                data: rm
            });

        if (rd != 'All')
            ftrs.rules.push({
                field: 'resident',
                op: 'eq',
                data: rd
            });

        if (rl != 'All' && rl != '')
            ftrs.rules.push({
                field: 'department',
                op: 'eq',
                data: rl
            });

        if (cn != 'All' && rl != '')
            ftrs.rules.push({
                field: 'carestaff_name',
                op: 'eq',
                data: cn
            });

        if (res != '')
            ftrs.rules.push({
                field: col,
                op: 'cn',
                data: res
            });

        changeTrigger = true;
        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, {
            filters: JSON.stringify(ftrs)
        });
        $("#grid").trigger("reloadGrid");
    });

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Task Completion",
        dialogClass: "companiesDialog",
        open: function () { }
    });

    var doAjax = function (row, cb) {
        $("#load_grid2").show();
        if ($('#diag #No').val() == '' && $('#diag #Yes').val()) {
            alert('Please choose an option first.');
            return;
        }
        //debugger;
        row = $('#diag').extractJson('id');
        if (!row.Yes && row.No && row.Remarks == '') {
            $('#diag #Remarks').addClass('required');
            $('#diag').validate();
            $('#diag span.error').html('Please enter remarks.').insertBefore('#diag #Remarks');
            $('#diag span.error').css({
                display: 'inline-block',
                float: 'none',
                width: 'auto',
                'font-weight': 'bold',
                color: 'red',
                'text-align': 'left',
                padding: '2px 0px 2px 20px',
                'margin-top': '5px',
                height: '16px'
            });

            return;
        } else {
            $('#diag #Remarks').removeClass('required');
            $('#diag span.error').remove();
        }

        var d = $('#curDate').val();
        //curdate = new Date();

        row.completion = d; //moment(curdate).format("MM/DD/YYYY");
        var data = {
            func: "carestaff_taskcompletion",
            data: row
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Staff/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function () {
        $("#diag").dialog("option", {
            width: 350,
            height: 310,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        //$("#load_grid").show();
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({
                                    message: "Task completed successfully!",
                                    delay: 4000
                                });
                            $("#load_grid2").hide();
                            $("#diag").dialog("close");
                            $('#grid').jqGrid('setGridParam', {
                                datatype: 'json'
                            }).trigger('reloadGrid');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $('#diag #Remarks').removeClass('required');
                    $('#diag span.error').remove();
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    var removeByAttr = function (arr, attr, value) {
        var i = arr.length;
        while (i--) {
            if (arr[i] &&
                arr[i].hasOwnProperty(attr) &&
                (arguments.length > 2 && arr[i][attr] === value)) {

                arr.splice(i, 1);

            }
        }
        return arr;
    }

    function beforeProcessing(data, status, xhr) {
        var delList = [];
        if (data && data.length > 0) {
            //search for items with xref and check if it has correspoding acivity_schedule_id in the list
            //if exists then remove item with xref and preserve the record with activity_schedule_id = xref
            var occ = $.grep(data, function (a) {
                return a.xref > 0
            });
            $.each(occ, function (x, y) {
                var matchObj = $.grep(data, function (b) {
                    return b.activity_schedule_id == y.xref && y.reschedule_dt == null;
                });
                if (matchObj.length > 0) {
                    delList.push(y);
                }
            });

            $.each(delList, function (i, a) {
                removeByAttr(data, 'activity_schedule_id', a.activity_schedule_id);
            });
        }
    }


    var renderGrid = function () {
        //var dataArray = [
        //    { Id: '1', Name: 'Suite 101', Description: 'Meeting facility', Level: 'First', Rate: '999.20' },
        //    { Id: '2', Name: 'Fancy 201', Description: 'Lounge', Level: 'Second', Rate: '534.20' },
        //    { Id: '3', Name: 'Lancy 900', Description: '', Level: 'First', Rate: '923.20' },
        //    { Id: '4', Name: 'Hancy 123', Description: '', Level: 'Third', Rate: '912.20' }

        //];
        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();

        var pageWidth = $("#grid").parent().width() - 100;
        $('#grid').jqGrid({
            url: "Staff/GetGridData?func=activity_schedule&param=" + $('#curDate').val(),
            beforeProcessing: function (data, status, xhr) {
                beforeProcessing(data, status, xhr)
            },
            datatype: 'json',
            postData: "",
            ajaxGridOptions: {
                contentType: "application/json",
                cache: false
            },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['ActivityId', 'ActivityScheduleId', 'CarestaffActivityId', 'Task Details', 'Procedure', 'Department', 'Resident Details', 'Room', 'Staff Details', 'Task Schedule', 'Duration (Minutes)', 'Task Status', 'oStat', 'CompletionDate', 'Remarks', 'Reschedule', 'Acknowledged By', 'XRef', 'TimeLapsed', 'Resident2', 'Carestaff2', 'Activitydesc2'],
            colModel: [{
                key: false,
                hidden: true,
                name: 'activity_id',
                index: 'activity_id'
            },
                {
                    key: true,
                    hidden: true,
                    name: 'activity_schedule_id',
                    index: 'activity_schedule_id'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'carestaff_activity_id',
                    index: 'carestaff_activity_id'
                },
                {
                    key: false,
                    name: 'activity_desc',
                    index: 'activity_desc',
                    width: (pageWidth * (18 / 100)),
                    cellattr: setTaskTitle,
                    formatter: taskFormatter
                },
                {
                    key: false,
                    hidden: true,
                    name: 'activity_proc',
                    index: 'activity_proc'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'department',
                    index: 'department',
                    width: (pageWidth * (8 / 100))
                },
                {
                    key: false,
                    name: 'resident',
                    index: 'resident',
                    width: (pageWidth * (12 / 100)),
                    cellattr: setTaskTitle,
                    formatter: taskFormatter
                },
                {
                    key: false,
                    hidden: true,
                    name: 'room',
                    index: 'room',
                    width: (pageWidth * (8 / 100))
                },
                {
                    key: false,
                    name: 'carestaff_name',
                    index: 'carestaff_name',
                    width: (pageWidth * (12 / 100)),
                    cellattr: setTaskTitle,
                    formatter: taskFormatter
                },
                {
                    key: false,
                    name: 'start_time',
                    index: 'start_time',
                    width: (pageWidth * (18 / 100)),
                    cellattr: setTaskTitle,
                    formatter: taskFormatter,
                    sorttype: function (cell, obj) {
                        return new Date('1970/01/01 ' + cell)
                    },
                    width: (pageWidth * (8 / 100))
                },
                {
                    key: false,
                    hidden: true,
                    name: 'service_duration',
                    index: 'service_duration',
                    width: (pageWidth * (8 / 100))
                },
                {
                    key: false,
                    name: 'activity_status',
                    index: 'activity_status',
                    width: (pageWidth * (20 / 100)),
                    cellattr: setTaskTitle,
                    formatter: taskFormatter,
                    sorttype: function (cell, obj) {

                        if (obj.carestaff_activity_id == null && obj.timelapsed)
                            return "Missed";
                        if (obj.remarks == '' && (!obj.acknowledged_by || obj.acknowledged_by == '') && cell == 0)
                            return ''
                        return cell == 0 ? 'No' : 'Completed';
                    }
                },
                {
                    key: false,
                    hidden: true,
                    name: 'oactivity_status',
                    index: 'oactivity_status'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'completion_date',
                    index: 'completion_date'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'remarks',
                    index: 'remarks',
                    formatter: function (cellvalue, options, row) {
                        if (row.reschedule_dt && row.reschedule_dt != '') {
                            var msg = "Rescheduled";
                            if (row.completion_type == 3)
                                msg = "Reassigned";
                            var htm = "<a xref_id='" + row.xref + "'>" + msg + " - " + row.reschedule_dt + "</a>";
                            return htm;
                        } else {
                            return cellvalue;
                        }

                    }
                },
                {
                    key: false,
                    hidden: true,
                    name: 'reschedule_dt',
                    index: 'reschedule_dt'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'acknowledged_by',
                    index: 'acknowledged_by',
                    width: (pageWidth * (8 / 100))
                },
                {
                    key: false,
                    hidden: true,
                    name: 'xref',
                    index: 'xref'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'timelapsed',
                    index: 'timelapsed'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'resident2',
                    index: 'resident2'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'carestaff2',
                    index: 'carestaff2'
                },
                {
                    key: false,
                    hidden: true,
                    name: 'activitydesc2',
                    index: 'activitydesc2'
                }
            ],
            loadComplete: function (data) {

                loaded = true;
                Grid.Settings.init();
                resizeGrids();

                $("a.tasklink").off('click').click(function (rowid, iRow, iCol, cellText, e) {
                    //debugger;
                    var G = $('#grid');
                    var dis = $(this).attr("class");

                    if (dis == "tasklink") {
                        rowid = $(this).parents('tr').attr('id');
                    }
                    G.setSelection(rowid, false);
                    var row = G.jqGrid('getRowData', rowid);
                    if (row) {
                        $('#diag').clearFields();
                        $('#diag #CarestaffActivityId').val(row.carestaff_activity_id);
                        $('#diag #ActivityScheduleId').val(row.activity_schedule_id);
                        $('#diag #Resident').val(row.resident2);
                        $('#diag #CarestaffName').val(row.carestaff2);
                        $('#diag #ActivityDesc').val(row.activitydesc2);
                        //$('#diag #AcknowledgedBy').val(row.acknowledged_by);
                        //$('#diag #Id').val(row.carestaff_activity_id);
                        //$('#diag #Remarks').val(row.remarks);
                        if (row.reschedule_dt && row.reschedule_dt != '') {
                            var remarkMsg = "Rescheduled";
                            if (row.remarks != null && row.remarks.indexOf('Reassigned') != -1)
                                remarkMsg = "Reassigned";
                            $('#diag #Remarks').val(remarkMsg + " - " + row.reschedule_dt);
                        } else
                            $('#diag #Remarks').val(row.remarks);

                        if (row.oactivity_status == false)
                            $('#diag #No').attr('checked', 'checked');
                        if (row.oactivity_status == true)
                            $('#diag #Yes').attr('checked', 'checked');

                        $('#diag #staffInfo').html('Is the selected task completed?');
                        showdiag();
                        var isAdmin = $('#btnAddTask,#btnReschedule').size() > 0 ? true : false;
                        var caid = row.carestaff_activity_id != null && row.carestaff_activity_id != '' ? parseInt(row.carestaff_activity_id) : 0;
                        //if ((moment($('#curDate').val()).format("MM/DD/YYYY") != moment(ALC_Util.CurrentDate).format("MM/DD/YYYY")) || (row.reschedule_dt && row.reschedule_dt != '')) {
                        if ((caid > 0) || (row.reschedule_dt && row.reschedule_dt != '') || (moment($('#curDate').val()).format("MM/DD/YYYY") != moment(ALC_Util.CurrentDate).format("MM/DD/YYYY") && !isAdmin)) {
                            $('#diag').find('input, textarea, button, select').attr('disabled', 'disabled');
                            //$('.ui-dialog-buttonset').hide();
                            $('.ui-dialog-buttonset button:eq(0)').removeClass('primaryBtn').addClass('primaryBtn-disabled').attr("disabled", "disabled");
                            $('#btnReschedule, #btnReassign').attr('disabled', 'disabled');
                            //primaryBtn-disabled
                        } else {
                            $('#diag').find('input, textarea, button, select').removeAttr('disabled');
                            //$('.ui-dialog-buttonset').show();
                            $('.ui-dialog-buttonset button:eq(0)').removeClass('primaryBtn-disabled').addClass('primaryBtn').removeAttr("disabled");
                            $('#btnReschedule, #btnReassign').removeAttr('disabled');
                        }

                    }
                });

                //var firstrow = $('#grid tbody:first-child tr:eq(1)').attr('id');
                //if (firstrow)
                //    $('#grid').setSelection(firstrow, true);

                var p = $('#grid').jqGrid("getGridParam", "postData");

                var dtype = $('#grid').jqGrid("getGridParam", "datatype");

                if (dtype == 'json') {

                    $("#grid").jqGrid("setGridParam", {
                        datatype: 'local'
                    });
                    setTimeout(function () {
                        p.search = true;
                        $("#grid").trigger("reloadGrid");
                    }, 0);
                }

                $('#grid a[xref_id]').off().on('click', function () {
                    var d = $(this);
                    if (!d.attr('xref_id')) return;

                    var rowid = $(this).parents('tr').attr('id');
                    var G = $('#grid');
                    G.setSelection(rowid, false);
                    var row = G.jqGrid('getRowData', rowid);
                    if (row) {
                        var caid = row.carestaff_activity_id != null && row.carestaff_activity_id != '' ? parseInt(row.carestaff_activity_id) : 0;
                        if ((caid > 0) || (row.reschedule_dt && row.reschedule_dt != '') || (moment($('#curDate').val()).format("MM/DD/YYYY") != moment(ALC_Util.CurrentDate).format("MM/DD/YYYY") && !isAdmin)) {
                            $('#diag').find('input, textarea, button, select').attr('disabled', 'disabled');
                            //$('.ui-dialog-buttonset').hide();
                            $('.ui-dialog-buttonset button:eq(0)').removeClass('primaryBtn').addClass('primaryBtn-disabled').attr("disabled", "disabled");
                            $('#btnReschedule, #btnReassign').attr('disabled', 'disabled');
                            //primaryBtn-disabled
                        } else {
                            $('#diag').find('input, textarea, button, select').removeAttr('disabled');
                            //$('.ui-dialog-buttonset').show();
                            $('.ui-dialog-buttonset button:eq(0)').removeClass('primaryBtn-disabled').addClass('primaryBtn').removeAttr("disabled");
                            $('#btnReschedule, #btnReassign').removeAttr('disabled');
                        }
                    }
                    var type = $(this).text().indexOf('Reassigned') != -1 ? 2 : 1;
                    doAjax2(type, 0, {
                        Id: d.attr('xref_id')
                    }, function (res) {
                        $("#diagReschedule").clearFields();
                        $('#diagReschedule #txtActivityR').empty();
                        $('<option/>').val(res.ActivityId).html(res.ActivityDesc).appendTo('#diagReschedule #txtActivityR');
                        $('#diagReschedule #viewActivityR').val(res.ActivityDesc);
                        //if housekeeper or maintenance hide room dropdown
                        if (res.Dept == 5 || res.Dept == 7) {
                            $('#diagReschedule #liRoom, #diagReschedule #liRoom select').show();
                            $('#diagReschedule #liResident, #diagReschedule #liResident select').hide();
                            $('#txtRoomIdR').empty();
                            $('<option/>').val(res.RoomId).html(res.RoomName).appendTo('#diagReschedule #txtRoomIdR');
                        } else {
                            $('#diagReschedule #liResident, #diagReschedule #liResident select').show();
                            $('#diagReschedule #liRoom, #diagReschedule #liRoom select').hide();
                            $('#diagReschedule #txtResidentIdR').empty();
                            $('<option/>').val(res.ResidentId).html(res.ResidentName).appendTo('#diagReschedule #txtResidentIdR');
                        }
                        $('#diagReschedule #txtTimeInR').val(res.ScheduleTime);
                        $('#diagReschedule #OnetimedateR').val(res.Onetimedate);

                        $('#diagReschedule #txtCarestaffR').empty();
                        $('<option/>').val(res.CarestaffId).html(res.CarestaffName).appendTo('#diagReschedule #txtCarestaffR');


                        $('#diagReschedule #txtTimeInR, #diagReschedule #txtCarestaffR, #diagReschedule #OnetimedateR').attr('disabled', 'disabled');

                        var title = "Reschedule Service Task";
                        if (type == 2) {
                            title = "Reassign Service Task";
                        }
                        $('#diagReschedule').dialog('option', 'title', title);

                        showdiag2(1);
                        $('.ui-dialog-buttonset button:eq(0)').removeClass('primaryBtn').addClass('primaryBtn-disabled').attr("disabled", "disabled");
                    });
                });
                var rl = false;
                if (typeof isReloaded != 'undefined')
                    rl = isReloaded;
                populateFilters(data, rl);

                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].activity_schedule_id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.activity_schedule_id > maxId) maxId = d.activity_schedule_id;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;

            },
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: {
                root: 'rows',
                page: 'page',
                total: 'total',
                records: 'records',
                repeatitems: false,
                id: '0'
            },
            autowidth: true,
            multiselect: false,
            sortname: 'start_time',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            onSelectRow: function (id) {
                var G = $('#grid');
                var row = G.jqGrid('getRowData', id);
                if (row) {
                    if (row.carestaff_activity_id != null && row.carestaff_activity_id != '') {
                        $('#btnReschedule, #btnReassign').attr('disabled', 'disabled');
                    } else {
                        $('#btnReschedule, #btnReassign').removeAttr('disabled');
                    }
                }
            }
        });

        $("#grid").jqGrid('bindKeys');
    };
    $(renderGrid);

    function taskFormatter(cellVal, options, row) {
        var col = options.colModel.name;
        var html = "";
        if (col == 'activity_desc') {
            html += "<div class='task'><p><label class='taskname'>Task: </label><a class='tasklink'>" + (row.activity_desc || "") + "</a></p>";
            html += "<p><label class='taskname'>Procedure: </label><label class='taskdesc'>" + (row.activity_proc || "(No procedure indicated)") + "</label></p></div>";
        } else if (col == 'resident') {
            html += "<div class='task'><p><label class='taskname'>Resident: </label><label class='taskdesc'>" + (row.resident || "") + "</label></p>";
            html += "<p><label class='taskname'>Room: </label><label class='taskdesc'>" + (row.room || "") + "</label></p></div>";
        } else if (col == 'carestaff_name') {
            html += "<div class='task'><p><label class='taskname'>Carestaff: </label><label class='taskdesc'>" + (row.carestaff_name || "") + "</label></p>";
            html += "<p><label class='taskname'>Department: </label><label class='taskdesc'>" + (row.department || "") + "</label></p></div>";
        } else if (col == 'start_time') {
            html += "<div class='task'><p><label class='taskname'>Schedule: </label><label class='taskdesc'>" + (row.start_time || "") + "</label></p>";
            html += "<p><label class='taskname'>Duration (Minutes): </label><label class='taskdesc'>" + (row.service_duration || "") + "</label></p></div>";
        } else if (col == 'activity_status') {
            var completed = cellVal == 1 ? '<span class="comp-yes">Completed</span>' : '<span class="comp-no">No</span>';
            //debugger;
            if (row.remarks == '' && (!row.acknowledged_by || row.acknowledged_by == '') && cellVal == 0)
                completed = "";
            if (row.carestaff_activity_id == null && row.timelapsed) {
                completed = '<span class="missed">Missed</span>';
            }
            var remarks = row.remarks || '';
            if (row.reschedule_dt && row.reschedule_dt != '') {
                var msg = "Rescheduled";
                if (row.completion_type == 3)
                    msg = "Reassigned";
                remarks = "<a xref_id='" + row.xref + "'>" + msg + " - " + row.reschedule_dt + "</a>";
            }


            html += "<div class='task'><p><label class='taskname'>Completed: </label><label class='taskdesc'>" + completed + "</label></p>";
            html += "<p><label class='taskname'>Remarks: </label><label class='taskdesc'>" + remarks + "</label></p>";
            html += "<p><label class='taskname'>Acknowledged By: </label><label class='taskdesc'>" + (row.acknowledged_by || '') + "</label></p></div>"
        }
        return html;
    }

    function setTaskTitle(id, cellval, row, colModel) {
        var col = colModel.name;
        var title = '';
        if (col == 'activity_desc') {
            title = 'Task: ' + (row.activity_desc || '') + "\nProcedure: " + (row.activity_proc || '(No procedure indicated)');
        } else if (col == 'resident') {
            title = 'Resident: ' + (row.resident || '') + "\nRoom: " + (row.room || '');
        } else if (col == 'carestaff_name') {
            title = 'Carestaff: ' + (row.resident || '') + "\nDepartment: " + (row.room || '');
        } else if (col == 'start_time') {
            title = 'Schedule: ' + (row.start_time || '') + "\nDuration (Minutes): " + (row.service_duration || '');
        } else if (col == 'activity_status') {
            var completed = row.activity_status == 1 ? 'Completed' : 'No';
            //completed = '<strong>' + completed + '</strong>';
            if (row.carestaff_activity_id == null && row.timelapsed) {
                completed = 'Missed';
            } else if (row.carestaff_activity_id == null && row.activity_status == 0) {
                completed = '';
            }

            var remarks = row.remarks || '';
            if (row.reschedule_dt && row.reschedule_dt != '') {
                var msg = "Rescheduled";
                if (row.completion_type == 3)
                    msg = "Reassigned";
                remarks = msg + " - " + row.reschedule_dt;
            }
            title = 'Completed: ' + (completed || '') + "\nRemarks: " + (remarks || '') + "\nAcknowledged By: " + (row.acknowledged_by || '');
        }
        return 'title="' + title + '"';
    }


    function populateFilters(data, reload) {
        if (typeof changeTrigger != 'undefined' && changeTrigger) {
            changeTrigger = false;
            return;
        }
        if (reload)
            $('#ftrText').val('');
        if ($('#ftrRoom').find('option').size() <= 0 || reload) {
            var optsRm = [];
            //debugger;
            //data = data.rows || data;
            $.each(unique($.map(data, function (o) {
                return o.room
            })), function () {
                optsRm.push('<option>' + this + '</option>');
            });
            $('#ftrRoom').html(optsRm.join(''));

            $("#ftrRoom").html($('#ftrRoom option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $("#ftrRoom").prepend('<option>All</option>');
            var frm = $("#ftrRoom").get(0);
            if (frm) $("#ftrRoom").get(0).selectedIndex = 0;

        }

        if ($('#ftrRole').find('option').size() <= 0 || reload) {
            var optsRm = [];

            $.each(unique($.map(data, function (o) {
                return o.department
            })), function () {
                optsRm.push('<option>' + this + '</option>');
            });
            $('#ftrRole').html(optsRm.join(''));

            $("#ftrRole").html($('#ftrRole option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $("#ftrRole").prepend('<option>All</option>');
            var fr = $("#ftrRole").get(0);
            if (fr) fr.selectedIndex = 0;
        }

        if ($('#ftrResident').find('option').size() <= 0 || reload) {
            var optsRm = [];

            //var byResident = data.slice(0);
            //byResident.sort(function (a, b) {
            //    if (a.resident < b.resident) return -1;
            //    if (a.resident > b.resident) return 1;
            //    return 0;
            //})

            $.each(unique($.map(data, function (o) {
                return o.resident
            })), function () {
                optsRm.push('<option>' + this + '</option>');
            });
            $('#ftrResident').html(optsRm.join(''));

            $("#ftrResident").html($('#ftrResident option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $("#ftrResident").prepend('<option>All</option>');
            var frr = $("#ftrResident").get(0);
            if (frr) $("#ftrResident").get(0).selectedIndex = 0;

        }

        if ($('#ftrStaff').find('option').size() <= 0 || reload) {
            var optsRm = [];

            $.each(unique($.map(data, function (o) {
                return o.carestaff_name
            })), function () {
                optsRm.push('<option>' + this + '</option>');
            });
            $('#ftrStaff').html(optsRm.join(''));

            $("#ftrStaff").html($('#ftrStaff option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $("#ftrStaff").prepend('<option>All</option>');
            var fs = $("#ftrStaff").get(0);
            if (fs) fs.selectedIndex = 0;

        }
    }

    function unique(array) {
        return $.grep(array, function (el, index) {
            return index == $.inArray(el, array);
        });
    }

    $("#diag input:checkbox").on('click', function () {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", true);
        }
    });

    $("#curDate").datepicker({
        formatDate: "MM/dd/yyyy",
        onClose: function () {
            if ($(this).val() == "")
                $(this).val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));
            if ($('#grid')[0] && $('#grid')[0].grid)
                $.jgrid.gridUnload('grid');
            isReloaded = true;
            renderGrid();
        }
    });
    $("#curDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));

    $("#diagReschedule").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Reschedule Service Task",
        dialogClass: "calendarVisitDialog",
        open: function () { }
    });

    $('#diagReschedule #txtTimeInR').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#diagReschedule #txtCarestaffR').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }
            $('#diagReschedule #OnetimedateR').trigger('change');
        }
        //,

        //onHourShow: function (h) {
        //    if (timePickerData) {
        //        var d = timePickerData;
        //        if (d && d.length <= 0) return;
        //        var shiftstart = parseInt(moment(d[0].shift_start, 'HH:mm:ss').format('HH'));
        //        var shiftend = parseInt(moment(d[0].shift_end, 'HH:mm:ss').format('HH'));

        //        if (shiftstart > shiftend) {
        //            if ((h > shiftend) && (h < shiftstart))
        //                return false;
        //        } else {
        //            if (((h > shiftend) || (h < shiftstart)))
        //                return false;
        //        }
        //        var retval = true;
        //        $.each(d, function () {
        //            var st = parseInt(moment(this.service_time, 'HH:mm:ss').format('HH'));;
        //            if (st) {
        //                if (h == st) retval = false;
        //            }
        //        });
        //        return retval;
        //    }
        //    return true;
        //},
        //onMinuteShow: function (h, m) {
        //    /*
        //    if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        //    if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        //    return true;  // valid
        //    */
        //    return true;
        //}

    });

    $('#diagTask #txtTimeIn').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#diagTask #txtCarestaff').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('#diagTask input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#diagTask #Onetimedate').trigger('change');
        }
        //,

        //onHourShow: function (h) {
        //    if (timePickerData) {
        //        var d = timePickerData;
        //        if (d && d.length <= 0) return;
        //        var shiftstart = parseInt(moment(d[0].shift_start, 'HH:mm:ss').format('HH'));
        //        var shiftend = parseInt(moment(d[0].shift_end, 'HH:mm:ss').format('HH'));

        //        if (shiftstart > shiftend) {
        //            if ((h > shiftend) && (h < shiftstart))
        //                return false;
        //        } else {
        //            if (((h > shiftend) || (h < shiftstart)))
        //                return false;
        //        }
        //        var retval = true;
        //        $.each(d, function () {
        //            var st = parseInt(moment(this.service_time, 'HH:mm:ss').format('HH'));;
        //            if (st) {
        //                if (h == st) retval = false;
        //            }
        //        });
        //        return retval;
        //    }
        //    return true;
        //},
        //onMinuteShow: function (h, m) {
        //    /*
        //    if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        //    if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        //    return true;  // valid
        //    */
        //    return true;
        //}

    });

    $('#diagTask #txtTimeIn').mask('99:99 xy');

    $('#diagReschedule #txtTimeInR').mask('99:99 xy');

    var doAjax2 = function (type, mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = {
                func: "activity_schedule",
                id: row.Id
            };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        //debugger;
        var isValid = $('#diagReschedule').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }
            row = $('#diagReschedule').extractJson();
            //validate time

            var dow = [];

            delete row['Onetimedate'];

            row.isonetime = '1';
            var onetimeval = $('#diagReschedule #OnetimedateR').val();
            if (onetimeval == '') {
                alert("Please choose a schedule date.");
                return;
            }

            var onetimeday = moment(onetimeval).day();
            if ($.isNumeric(onetimeday))
                dow.push(onetimeday.toString());

            row.ScheduleTime = onetimeval + ' ' + $("#diagReschedule #txtTimeInR").val();

            row.Recurrence = dow.join(',');

            var d = $('#curDate').val();
            //curdate = new Date();

            row.completion = d //moment(curdate).format("MM/DD/YYYY");
            row.clientdt = d //moment(curdate).format("MM/DD/YYYY");
            row.comptype = type;
        }
        var data = {
            func: "activity_schedule",
            data: row
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Staff/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag2 = function (mode) {
        $("#diagReschedule").dialog("option", {
            width: 500,
            height: 280,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        var type = parseInt($('#diagReschedule #txtCarestaffR').attr('xtype'));
                        doAjax2(type, mode, row, function (m) {
                            var typemsg = "rescheduled";
                            if (type != 1) typemsg = "reassigned";

                            if (m.result == 0) {
                                $.growlSuccess({
                                    message: "Task " + typemsg + " successfully!",
                                    delay: 4000
                                });
                                $("#diagReschedule").dialog("close");
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                }
                                $('#grid').jqGrid('setGridParam', {
                                    datatype: 'json'
                                }).trigger('reloadGrid');
                            } else if (m.result == -2) {
                                var resVisible = $('#txtResidentIdR').is(':visible');
                                $.growlWarning({
                                    message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".",
                                    delay: 6000
                                });
                                //there is already a schedule for a particular resident or room
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diagReschedule").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('#btnReschedule, #btnReassign').on('click', function () {
        var dis = this;
        var type = dis.id == 'btnReassign' ? 2 : 1;
        var G = $('#grid');
        var id = G.jqGrid('getGridParam', 'selrow');
        var row = G.jqGrid('getRowData', id);
        //var $Alert;
        //if (row.carestaff_activity_id != null && row.carestaff_activity_id > 0) {
        //    $.growlWarning({ message: "Tasks that are already rescheduled or reassigned and completed are no longer be edittable.", delay: 6000 });
        //    return;
        //}
        if (!row.activity_schedule_id) {
            var firstrow = $('#grid tbody:first-child tr:eq(1)').attr('id');
            if (firstrow)
                $('#grid').setSelection(firstrow, true);

            id = G.jqGrid('getGridParam', 'selrow');
            row = G.jqGrid('getRowData', id);
        }

        if (row.activity_schedule_id) {
            doAjax2(type, 0, {
                Id: row.activity_schedule_id
            }, function (res) {
                $('#diagReschedule #txtCarestaffR').attr('xtype', type);
                $("#diagReschedule").clearFields();
                $('#diagReschedule #txtActivityR').empty();
                $('#diagReschedule #txtIdR').val(row.activity_schedule_id);
                $('#diagReschedule #txtTimeInR').val(res.ScheduleTime);
                $('#diagReschedule #OnetimedateR').val($('#curDate').val());
                $('<option/>').val(res.ActivityId).html(res.ActivityDesc).appendTo('#diagReschedule #txtActivityR');
                $('#viewActivityR').val(res.Activity);
                //if housekeeper or maintenance hide room dropdown
                if (res.Dept == 5 || res.Dept == 7) {
                    $('#diagReschedule #liRoom, #diagReschedule #liRoom select').show();
                    $('#diagReschedule #liResident, #diagReschedule #liResident select').hide();
                    $('#diagReschedule #txtRoomIdR, #diagReschedule #txtCarestaffR').empty();
                    $('<option/>').val(res.RoomId).html(res.RoomName).appendTo('#diagReschedule #txtRoomIdR');
                } else {
                    $('#diagReschedule #liResident, #diagReschedule #liResident select').show();
                    $('#diagReschedule #liRoom, #diagReschedule #liRoom select').hide();
                    $('#diagReschedule #txtResidentIdR, #diagReschedule #txtCarestaffR').empty();
                    $('<option/>').val(res.ResidentId).html(res.ResidentName).appendTo('#diagReschedule #txtResidentIdR');
                }

                if (type != 2) {
                    $('#diagReschedule #OnetimedateR').show();
                    $('#diagReschedule #txtTimeInR,#diagReschedule #txtCarestaffR,#diagReschedule #OnetimedateR').removeAttr('disabled');
                } else {
                    $('#diagReschedule #OnetimedateR').attr('disabled', 'disabled');
                    $('#diagReschedule #txtTimeInR,#diagReschedule #txtCarestaffR').removeAttr('disabled');
                }
                //$('.ui-dialog-buttonset').show();

                var title = "Reschedule Service Task";
                if (type == 2) {
                    title = "Reassign Service Task";
                }
                $('#diagReschedule').dialog('option', 'title', title);
                showdiag2(1);

                var dow = [];
                dow[0] = moment($('#curDate').val()).day();
                bindCarestaff(dow, type);
            });
        }
    });

    $("#diagTask").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Add Service Task",
        dialogClass: "calendarVisitDialog",
        open: function () { }
    });

    var showdiagTask = function () {
        var mode = 1;
        $("#diagTask").dialog("option", {
            width: 500,
            height: 500,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjaxTask(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({
                                    message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!",
                                    delay: 6000
                                });
                                $("#diagTask").dialog("close");
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                }
                                $('#grid').jqGrid('setGridParam', {
                                    datatype: 'json'
                                }).trigger('reloadGrid')
                            } else if (m.result == -2) {
                                var resVisible = $('#txtResidentId').is(':visible');
                                $.growlWarning({
                                    message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".",
                                    delay: 6000
                                });
                                //there is already a schedule for a particular resident or room
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diagTask").dialog("close");
                }
            }
        }).dialog("open");
    }

    var doAjaxTask = function (mode, row, cb) {
        var isValid = $('#diagTask').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }
            row = $('#diagTask').extractJson();
            //validate time

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];
            delete row['recurring'];
            delete row['Activity'];
            delete row['Dept'];
            delete row['Onetimedate'];

            var isrecurring = $('#diagTask input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring) {
                row.isonetime = '0';
                $('#diagTask .dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    if (d.is(':checked')) {
                        switch (d.attr('name')) {
                            case 'Sun':
                                dow.push('0');
                                break;
                            case 'Mon':
                                dow.push('1');
                                break;
                            case 'Tue':
                                dow.push('2');
                                break;
                            case 'Wed':
                                dow.push('3');
                                break;
                            case 'Thu':
                                dow.push('4');
                                break;
                            case 'Fri':
                                dow.push('5');
                                break;
                            case 'Sat':
                                dow.push('6');
                                break;
                        }
                    }
                });

                if (dow.length == 0) {
                    alert('Please choose day of week schedule.');
                    return;
                }

            } else {
                row.isonetime = '1';
                var onetimeval = $('#diagTask #Onetimedate').val();
                if (onetimeval == '') {
                    alert("Please choose a one time date schedule.");
                    return;
                }

                var onetimeday = moment(onetimeval).day();
                if ($.isNumeric(onetimeday))
                    dow.push(onetimeday.toString());

                row.ScheduleTime = onetimeval + ' ' + $('#diagTask #txtTimeIn').val();
            }
            row.Recurrence = dow.join(',');
        }

        var data = {
            func: "activity_schedule",
            mode: mode,
            data: row
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    $('#btnAddTask').on('click', function () {
        var dept = $('#diagTask #ddlDept').val();
        $('#diagTask').clearFields();
        $('#diagTask input[xid="hdCarestaffId"]').val('');
        $('#diagTask input[name="recurring"]:eq(0)').attr('checked', 'checked');
        $('#diagTask input[name="recurring"]:eq(1)').removeAttr('checked');
        $('#diagTask #Onetimedate').attr('disabled', 'disabled');
        $('#diagTask .dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
        showdiagTask();
        //if housekeeper or maintenance hide room dropdown
        $('#diagTask #ddlDept').val(dept);
        if (dept == 5 || dept == 7) {
            $('#diagTask #liRoom, #diagTask #liRoom select').show();
            $('#diagTask #liResident, #diagTask #liResident select').hide();
        } else {
            $('#diagTask #liResident, #diagTask #liResident select').show();
            $('#diagTask #liRoom, #diagTask #liRoom select').hide();
        }

    });

    var bindCarestaff = function (dow, type) {
        if ($('#diagReschedule #txtTimeInR').val() == '') {
            $('#diagReschedule #txtCarestaffR')[0].options.length = 0;
            return;
        }
        if ($('#diagReschedule #txtActivityR').val() == '') {
            $('#diagReschedule #txtCarestaffR')[0].options.length = 0;
            return;
        }
        if (dow == null) dow = [];
        var data = {
            scheduletime: $('#diagReschedule #txtTimeInR').val(),
            activityid: $('#diagReschedule #txtActivityR').val(),
            carestaffid: null,
            recurrence: dow.join(',')
        };
        data.isonetime = "1";
        data.clientdt = moment($('#diagReschedule #OnetimedateR').val()).format("MM/DD/YYYY");
        data.type = type || 1;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#diagReschedule #txtCarestaffR').empty();
                $('<option/>').val('').html('').appendTo('#diagReschedule #txtCarestaffR');
                $.each(m, function (i, d) {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#diagReschedule #txtCarestaffR');
                });
            }
        });
    }

    var bindCarestaff2 = function (dow) {
        if ($('#diagTask #txtTimeIn').val() == '') {
            $('#diagTask #txtCarestaff')[0].options.length = 0;
            return;
        }
        if ($('#diagTask #txtActivity').val() == '') {
            $('#diagTask #txtCarestaff')[0].options.length = 0;
            return;
        }
        if (dow == null) dow = [];
        var data = {
            scheduletime: $('#diagTask #txtTimeIn').val(),
            activityid: $('#diagTask #txtActivity').val(),
            carestaffid: null,
            recurrence: dow.join(',')
        };
        data.isonetime = "1";
        data.clientdt = moment($('#diagTask #Onetimedate').val()).format("MM/DD/YYYY");
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#diagTask #txtCarestaff').empty();
                $('<option/>').val('').html('').appendTo('#diagTask #txtCarestaff');
                $.each(m, function (i, d) {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#diagTask #txtCarestaff');
                });
            }
        });
    }


    $('#diagReschedule #OnetimedateR').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#diagReschedule #OnetimedateR').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        var type = $('#diagReschedule #txtCarestaffR').attr('xtype');
        bindCarestaff(dow, parseInt(type));
    });

    $('#diagTask #Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#diagTask #Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff2(dow);
    });

    $('#diagTask #txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#diagTask #viewActivity').val(title);
        $('#diagTask #txtCarestaff')[0].options.length = 0;
        toggleCarestafflist();
    });

    $('#diagTask input[name=recurring]').change(function () {
        $('#diagTask #txtCarestaff')[0].options.length = 0;
    });

    var bindDept = function (dptid, cb) {
        var data = {
            deptid: dptid
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetServicesAndCarestaffByDeptId",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //$('#txtCarestaff').empty();
                //$('<option/>').val('').html('').appendTo('#txtCarestaff');
                //$.each(m.Carestaffs, function (i, d) {
                //    $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaff');
                //});

                $('#diagTask #txtActivity').empty();
                $('#diagTask #viewActivity').val('');
                var opt = '<option val=""></option>';
                $.each(m.Services, function (i, d) {
                    opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
                });
                $('#diagTask #txtActivity').append(opt);
                if (typeof (cb) !== 'undefined') cb();
            }
        });
    }

    var toggleCarestafflist = function () {
        var isrecurring = $('#diagTask input[name="recurring"]:eq(0)').attr('checked');
        if (isrecurring)
            triggerDayofWeekClick();
        else
            $('#diagTask #Onetimedate').trigger('change');

        //if housekeeper or maintenance hide room dropdown
        var val = $('#diagTask #ddlDept').val();
        if (val == 5 || val == 7) {
            $('#diagTask #liRoom, #diagTask #liRoom select').show();
            $('#diagTask #liResident, #diagTask #liResident select').hide();
        } else {
            $('#diagTask #liResident, #diagTask #liResident select').show();
            $('#diagTask #liRoom, #diagTask #liRoom select').hide();
        }
    }

    if ($('#diagTask #ddlDept').length > 0) {
        $('#diagTask #ddlDept').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#diagTask #txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('#diagTask input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('#diagTask table.dayofweek input').removeAttr('disabled');
            $('#diagTask #Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('#diagTask table.dayofweek input').attr('disabled', 'disabled');
            $('#diagTask table.dayofweek').clearFields();
            $('#diagTask #Onetimedate').removeAttr('disabled');
        }
    });

    var triggerDayofWeekClick = function () {
        var a = $('#diagTask #txtTimeIn').val();
        if (a == null || a == '') {
            $('#diagTask #txtCarestaff').empty();
            //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
            return;
        }

        var dow = [];
        $('#diagTask .dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun':
                        dow.push('0');
                        break;
                    case 'Mon':
                        dow.push('1');
                        break;
                    case 'Tue':
                        dow.push('2');
                        break;
                    case 'Wed':
                        dow.push('3');
                        break;
                    case 'Thu':
                        dow.push('4');
                        break;
                    case 'Fri':
                        dow.push('5');
                        break;
                    case 'Sat':
                        dow.push('6');
                        break;
                }
            }
        });
        bindCarestaff2(dow);

    }

    $('#diagTask table.dayofweek input').click(triggerDayofWeekClick);

    //do client side refresh of missed tasks based on current time. it is implemented in site.layout.js

    //periodical executer 30 secs
    //setInterval(function () {
    //    fromSort = true;
    //    new Grid_Util.Row().saveSelection.call($('#grid')[0]);
    //    $('#grid').hide();
    //    $('#grid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
    //    var v = setInterval(function () {
    //        if (typeof loaded != 'undefined' && loaded) {
    //            $('#ftrResident').trigger('change'); $('#grid').show();
    //            loaded = false;
    //            clearInterval(v);
    //        }
    //    }, 100);
    //}, 300000);



    function fnExcelReport() {
        //$("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Task Details</td><td bgcolor='#87AFC6'>Resident Details</td><td bgcolor='#87AFC6'>Staff Details</td><td bgcolor='#87AFC6'>Task Schedule</td><td bgcolor='#87AFC6'>Task Status</td></tr>";
        var textRange;
        var j = 0;
        var i = 0;
        tab = document.getElementById('grid'); // id of table

        var deleteCell = function () {
            if (cells[i].style.display == 'none')
                cells[i].outerHTML = '';
        }
        var trs = [];
        for (j = 0; j < tab.rows.length; j++) {
            var row = $(tab.rows[j]).clone();
            var cells = row[0].cells;
            var tds = [];
            for (i = 0; i < cells.length; ++i) {
                //remove cells that are hidden
                //debugger;
                if (cells[i].style.display != 'none')
                    tds.push(cells[i].outerHTML);
            }
            trs.push("<tr>" + tds.join('') + "</tr>");
            //tab_text=tab_text+"</tr>";
        }
        tab_text = tab_text + trs.join('') + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_DailyTasksAndSchedules.xls");
        } else //other browser not tested on IE 11  
            a.id = 'ExcelDL';
        a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
        a.download = 'ALC_DailyTasksAndSchedules' + '.xls';
        document.body.appendChild(a);
        a.click(); // Downloads the excel document
        document.getElementById('ExcelDL').remove();
    }


    $('#btnExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        resizeGrids();
        //$('#grid').trigger('reloadGrid');
    });

});