﻿$(document).ready(function () {
    $('.schedule').height($(window).height() - 265);
    $(window).resize(function () {
        $('.schedule').height($(window).height() - 265);
    })

    function MedTechCalendar(elem, data) {
        var $e = $('#' + elem);

        //var data = [{ "admission_id": "1", "resident_id": "2", "resident_name": "Joe A. Lambert", "medicines": [{ "receive_detail_id": "5", "medicine": "Asperin", "dosage_times": [{ "time": "08:00", "intake_status": 1, "dosage_schedule_id": 4 }, { "time": "16:00", "intake_status": 0, "dosage_schedule_id": 5, }, { "time": "20:00", "intake_status": null, "dosage_schedule_id": 6 }] }, { "receive_detail_id": "6", "medicine": "Vitamin C", "dosage_times": [{ "time": "08:00", "intake_status": null, "dosage_schedule_id": 2 }, { "time": "16:00", "intake_status": null, "dosage_schedule_id": 3 }] }] }, { "admission_id": "2", "resident_id": "1", "resident_name": "Ron F. Harper", "medicines": [{ "receive_detail_id": "1", "medicine": "Analgesic", "dosage_times": [{ "time": "08:00", "intake_status": null, "dosage_schedule_id": 7 }, { "time": "12:00", "intake_status": null, "dosage_schedule_id": 8 }, { "time": "16:00", "intake_status": null, "dosage_schedule_id": 9 }, { "time": "20:00", "intake_status": null, "dosage_schedule_id": 10 }] }, { "receive_detail_id": "2", "medicine": "Propanolamin", "dosage_times": [{ "time": "08:00", "intake_status": null, "dosage_schedule_id": 2 }, { "time": "16:00", "intake_status": null, "dosage_schedule_id": 3 }] }, { "receive_detail_id": "3", "medicine": "Streptomycin", "dosage_times": [{ "time": "08:00", "intake_status": null, "dosage_schedule_id": 2 }, { "time": "16:00", "intake_status": null, "dosage_schedule_id": 3 }] }, { "receive_detail_id": "4", "medicine": "Asperin", "dosage_times": [{ "time": "08:00", "intake_status": 1, "dosage_schedule_id": 4 }, { "time": "16:00", "intake_status": 0, "dosage_schedule_id": 5 }, { "time": "20:00", "intake_status": null, "dosage_schedule_id": 6 }] }] }];


        var createRow = function (obj) {
            var html = [];
            html.push('<tr class="parent">');
            html.push('<td style="font-weight:700">' + obj.resident_name + '</td>');
            var meds = [];
            var medlen = obj.medicines ? obj.medicines.length : 0;
            if (medlen > 0) {
                var oM = obj.medicines;
                var times = ["08:00", "12:00", "16:00", "20:00"];
                var matchEval = function (o, time) {
                    var dose = $.grep(o.dosage_times, function (o) { return o.time == time });
                    if (dose.length > 0) {
                        var css = "class='";

                        if (time == "08:00")
                            css += 'col8AM';
                        else if (time == "12:00")
                            css += 'col12PM';
                        else if (time == "16:00")
                            css += 'col4PM';
                        else if (time == "20:00")
                            css += 'col8PM';

                        var status = dose[0].intake_status;
                        var sign = "<i class=\"la la-check\"></i>";
                        if (status != null) {
                            if (status == 1){
                                css = "class='complete'";
                            } else if (status == 0) {
                                css = "class='warning'";
                                sign = "<i class=\"la la-exclamation\"></i>";
                            }
                                
                        } else {
                            //debugger;
                            var doseTime = moment(ALC_Util.CurrentDate + ' ' + dose[0].time, 'MM/DD/YYYY HH:mm');
                            var curTime = moment($('#spCT').text(), "ddd, MMM DD YYYYY HH:mmA");
                            if (doseTime.isValid() && curTime.isValid() && doseTime.isBefore(curTime)) {
                                css = "class=''";
                                sign = "<span style=\"font-weight: bold; color:red;\">X</span>";
                            } else {
                                css += "'";
                                sign = "";
                            }

                        }
                        return {
                            key: time,
                            value: '<a style="text-decoration-style: dotted;text-decoration-line: underline;" rdid="' + o.receive_detail_id + '" dsid="' + dose[0].dosage_schedule_id + '" ' + css + '>' + sign + ' '+ o.medicine +'</a><br/>'
                        }
                    }
                    return null;
                }
                var links = [];
                $.each(times, function () {
                    var time = this;
                    $.each(oM, function () {
                        var match = matchEval(this, time);
                        if (match != null)
                            links.push(match);
                    })

                });

                $.each(times, function () {
                    var vals = [];
                    var time = this;
                    var meds = $.grep(links, function (o) { return o.key.toString() === time.toString(); });
                    $.each(meds, function () {
                        vals.push(this.value);
                    });
                    html.push('<td>' + vals.join('') + '</td>');
                });
            }
            html.push('</tr>');
            return html.join('');
        }

        for (var h = 0; h < data.length; ++h) {
            $e.append(createRow(data[h]));
        }
        $(".loader").hide();
    }

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Medicine Administration",
        dialogClass: "companiesDialog",
        open: function () {
            $("#diag #Yes").prop('checked', true);
        }
    });


    var showdiag = function (mode) {
        
        $("#diag").dialog("option", {
            width: 350,//$(window).width() - 100,
            height: 290,//$(window).height() - 100,
            position: "center",
            buttons: {
                "Done": {
                    click: function () {
                        if ($('#diag input[type=checkbox]:checked').length <= 0) {
                            $.growlWarning({ message: "Please choose Yes or No before proceeding to done or click 'Cancel' to cancel changes.", delay: 6000 });
                            return;
                        }

                        if ($('#diag #No').is(":checked") && $('#diag #Comment').val() == "") {
                            $.growlWarning({ message: "Please provide comment first before proceeding to done or click 'Cancel' to cancel changes.", delay: 6000 });
                            return;
                        }

                        //save changes
                        var pdata = $('#diag').extractJson('id');

                        var rdid = $('#param1').val();
                        var dsid = $('#param2').val();

                        pdata.rdid = rdid;
                        pdata.dsid = dsid;
                        var mode = 1;
                        if ($('#Id').val() != '')
                            mode = 2;

                        var data = { func: "mar_info", mode: mode, Data: pdata };

                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "MAR/PostData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (m != null && m.result == 0) {
                                    $.growlSuccess({ message: "Administration saved successfully.", delay: 4000 });
                                    $('#medtechCal_body tr').slice(1).detach();
                                    build();
                                }
                            }
                        });

                        $("#diag").dialog("close");
                    },
                    class: "primaryBtn",
                    text: 'Done'
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $("input:checkbox").on('click', function () {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            if ($('#mar_modal input:checked').size() == 0)
                $box.prop("checked", true);
            else
                $box.prop("checked", false);
        }
        if ($box.attr('Id') == 'No' && $box.is(":checked"))
            $('#Comment').removeAttr('disabled');
        else
            $('#Comment').attr('disabled', 'disabled');
    });


    $("#administer").on("click", function () {
        if ($('#mar_modal input[type=checkbox]:checked').length <= 0) {
            
            swal({
                title: "Please choose Yes or No before proceeding to done or click 'Cancel' to cancel changes.",
                type: "warning",
            });
            return;
        }

        if ($('#mar_modal #No').is(":checked") && $('#mar_modal #Comment').val() == "") {
            swal({
                title: "Please provide comment first before saving or click 'Cancel' to cancel changes.",
                type: "warning",
            });
          
            return;
        }

        //save changes
        var pdata = $('#mar_modal').extractJson('id');

        var rdid = $('#param1').val();
        var dsid = $('#param2').val();

        pdata.rdid = rdid;
        pdata.dsid = dsid;
        var mode = 1;
        if ($('#Id').val() != '')
            mode = 2;

        var data = { func: "mar_info", mode: mode, Data: pdata };

        console.log(data);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MAR/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
               
                if (m != null && m.result == 0) {
                    swal("Administration saved successfully.", "", "success");
                    $("#mar_form").trigger("reset");
                    $(".close").trigger("click");
                    $("#medtechCal_body").html("");
                   // $('#medtechCal tr').slice(2).detach();
                    build();
                }
                if (m.message == "negative") {
                    swal("Unable to save administration.There is an insufficient quantity of supply/medicine", "", "warning");
                    return
                }
            }
        });
    })


    $('#medtechCal').on('click', 'a', function (ev) {
        

        $("#mar_modal").modal("toggle");

        var target = ev.target || ev.targetElement
        var dis = $(target);
        var rdid = dis.attr('rdid');
        var dsid = dis.attr('dsid');
        $('#param1').val(rdid);
        $('#param2').val(dsid);
        var res = $(dis.parent().siblings().get(0)).text() || '';
        $('#medInfo').html('Administer ' + dis.text() + ' to ' + '<span style="font-weight:600">' + res + '</span>' + ' ?');
        var data = { func: "mar_info", param1: rdid, param2: dsid };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MAR/GetData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
            console.log(m);
                if (m != null) {
                    $('#Id').val(m.dmid);
                    $('#Yes,#No').removeAttr('checked');
                    if (m.status == true)
                        $('#Yes').attr('checked', 'checked');
                    else
                        $('#No').attr('checked', 'checked');
                    $('#Comment').val(m.comment);
                }
            }
        });

    });



    $(document).keyup(function (e) {
        if (e.keyCode === 27) $('.close').click();   // esc
    });

    $("#search_mar_value").keyup(function () {
        var string = $(this).val().toLowerCase();

        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
    })

    $("#showall").on("click", function () {
        $(".parent").each(function (i, e) {
            $(this).show();
        });
    })

    var build = function pullData(cb) {
        var data = { func: "mar", param1: ALC_Util.CurrentDate };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MAR/GetData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
               
                MedTechCalendar('medtechCal_body', m);
            }
        });
    };
    build();
    var scroll1 = new PerfectScrollbar('#medtechCal_body');
});