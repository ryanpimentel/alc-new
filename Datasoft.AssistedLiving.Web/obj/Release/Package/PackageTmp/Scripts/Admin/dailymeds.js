﻿function setHeight() {

    $('.parent, .card').height(250);
    $(window).resize(function () {
        $('.parent, .card').height(250);
    })

    $('.card-img-top').height(($('.card').height() / 2));
    $(window).resize(function () {
        $('.card-img-top').height(($('.card').height() / 2));
    })
}

$(document).ready(function () {

$(function () {
    var optsRm = [];

    //FUNCTIONS
    $.ajax({
        url: "Admin/GetGridData?func=resident&param=active_only",
        datatype: 'json',
        type: "GET",
        success: function (m) {
          
            var html_tile = "";
            var gender = "";
            var gender_text;
            if (m) {
                $.each(m, function (i, v) {
                  
                    optsRm.push('<option>' + v.RoomName + '</option>');
                    var filteredoptsRm = optsRm.filter(function (item, pos) {
                        return optsRm.indexOf(item) == pos;
                    });

                    $('#ftrRoom').html(filteredoptsRm.join(''));

                    $("#ftrRoom").html($('#ftrRoom option').sort(function (x, y) {
                        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                    }));
                    $("#ftrRoom").prepend('<option>Room: (All)</option>');
                    $("#ftrRoom").get(0).selectedIndex = 0;

                    gender = "";
                    gender_text = "";
                    dis_title = "";

                    if (v.isAdmitted == true) {
                        button_class = "btn m-btn m-btn--icon btn-lg m-btn--icon-only btn-outline-primary";
                        icon_class = "fa fa-sign-out-alt";
                        dis_title = "Discharge resident"
                    } else {
                        button_class = "btn m-btn m-btn--icon btn-lg m-btn--icon-only btn-danger disabled";
                        icon_class = "fa fa-minus-circle";
                        dis_title = "Resident is discharged!";
                    }

                    if (v.Gender == "M") {
                        gender = '<i class="fas fa-mars pop-icon"></i>';
                        gender_text = 'Male';
                    } else if (v.Gender == "F") {
                        gender = '<i class="fas fa-venus pop-icon"></i>';
                        gender_text = 'Female';
                    } else if (v.Gender == "" || v.Gender == null) {
                        gender = '<i class="fas fa-genderless pop-icon"></i>';

                    }

                    html_tile += '<div class="col-xl-2 col-sm-4 col-md-3 col-xs-12 parent" style="padding-bottom: 10px;padding-top: 10px;padding-right: 0.20% !important;padding-left: 0.20% !important;">';
                    html_tile += '<div class="card m-portlet" style="width: 100%;">';
                    html_tile += '<div class="topop" style="width:100%;padding:7px;padding-bottom:0px">';
                    html_tile += '<img style="object-fit: cover" class="card-img-top dam_resident" data-toggle=\"modal\" data-target=\"#resident_modal\" id="' + v.Id + '"src="Resource/ResidentImage?id=' + v.Id + '&r=' + Date.now() + '" alt="Card image cap">';
                    html_tile += '</div><div class="card-body" style="padding-top:5px">';
                    html_tile += '<div class="btn-group" style="width:100%" role="group"><a href="#" style="min-width:100%;min-height:38px" class="card-title btn btn-sm btn-primary dam_resident" data-toggle=\"modal\" data-target=\"#resident_modal\" id="' + v.Id + '">' + v.Lastname + " ,<br/>" + v.Firstname + " " + (v.MiddleInitial == "" ? "" : v.MiddleInitial.toUpperCase() + ".") + '</a>';
                    html_tile += '</div>';
                    html_tile += '<p style="font-size:10px" class="card-text topop" data-toggle="popover" data-trigger="hover" title="Emergency Contact Info" data-content="' + (v.ResponsiblePerson == null || v.ResponsiblePerson == '' ? "<span><i class='fas fa-user'></i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;—<br>" : "<span><i class='fas fa-user'></i></span>&nbsp;&nbsp;&nbsp;" + v.ResponsiblePerson + '<br>') + (v.ResponsiblePersonTelephone == null || v.ResponsiblePersonTelephone == '' ? "<span><i class='fas fa-phone'></i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;—<br>" : "<span><i class='fas fa-phone'></i></span>&nbsp;&nbsp;&nbsp;" + v.ResponsiblePersonTelephone + '<br>') + (v.ResponsiblePersonAddress == null || v.ResponsiblePersonAddress == '' ? "<span><i class='fas fa-home'></i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;—<br>" : "<span><i class='fas fa-home'></i></span>&nbsp;&nbsp;&nbsp;" + v.ResponsiblePersonAddress + '<br>') + '">' + gender + ' ' + gender_text + '' + (v.IsDNR == true ? '<span class="dnr"><span>DNR</span></span>' : "") + '<br/>';
                    html_tile += '<i class="fas fa-bed pop-icon"></i> ' + v.RoomName + '<br/>';
                    html_tile += '<i class="fas fa-birthday-cake pop-icon"></i> ' + v.Birthdate + '<br/>';
                    html_tile += '<i class="fas fa-info pop-icon"></i> <span style="font-size:9.5px"> SSN: ' + v.SSN + ' </span><br/>';
                    html_tile += '</p>';

                    html_tile += '</div></div></div>';
                })
            }

            $("#dailymedstile").html(html_tile);
            setHeight();
            var scroll1 = new PerfectScrollbar('#dailymedstile');
            //$(".card").popover({
            //    html: true
            //});
            $(".loader").hide();
        }
    })


    //ON CLICKS

   //// if ($('#ftrRoom').find('option').size() <= 0) {
   //     $.each(_FR.Rooms, function (index, value) {
   //         $('#ftrRoom').append($('<option>', {
   //             value: 'room_' + value.Id.toString(),
   //             text: value.Name
   //         }));
   //     });
    //}

    $(document).on("click", ".dam_resident", function () {
        //manually toggles the modal since it won't work on HTML side
        var id = $(this).attr("id");

        $("#current_selid").val(id);
        $("#dailymeds_modal").modal("toggle");
     

            var data = { id: 'dailymed_diag' };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetView",
                data: JSON.stringify(data),
                dataType: "html",
                success: function (m) {
                    $('#dailymeds_modal .m-portlet__body').html(m);
                }
            });
            var scroll1 = new PerfectScrollbar('#PCAL');
    })

    $("#search_dam_value").keyup(function () {
        var string = $(this).val().toLowerCase();
     
        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
    })

    $('#ftrRoom').change(function () {
      
       // var string = $(this :selected).text();
        var string = $('#ftrRoom :selected').text();
        if (string == "Room: (All)") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        } else {
            $(".parent").each(function (i, e) {
                var a = $(this).text();

                if (a.indexOf(string) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            })
        }
    })

    $("#search_dam_value").keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    })

    $("#headerRoom #showall").on("click", function () {
        $("#search_dam_value").val("");
        $(".parent").each(function (i, e) {
            $(this).show();
        });
    })
})

})