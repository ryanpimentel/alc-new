﻿$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }

    $("#pointsSettingsContent").layout({
        center: {
            paneSelector: "#pointsSettingsGrid",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#pointsSettingsGridToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
        },
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#pointsSettingsGrid").height() - 25).width($("#supplyGrid").width());
            $('#grid').setGridWidth($("#pointsSettingsGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $("#txtRate").forceNumericOnly();
    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Score Settings",
        dialogClass: "physicianDialog",
        open: function () {
        }
    });

    var doAjax = function (mode, row, cb) {
        //debugger;
        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('.form').extractJson();
        }

        var data = { func: "pointssettings", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: 500,
            height: 220,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        //debugger;
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({ message: "Score updated successfully!", delay: 6000 });
                            $("#diag").dialog("close");
                            if (mode == 1) isReloaded = true;
                            else {
                                fromSort = true;
                                new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                            }
                            $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
        $('#ScoreValue').focus();
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.refresh')) {
            creategrid();
        }
    });

    var creategrid = function () {
        if ($('#grid')[0] && $('#grid')[0].grid)
            $.jgrid.gridUnload('grid');
        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=pointssettings&param=" + $('#assessGroup').val(),
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'FieldId', 'Field Text', 'Score'],
            colModel: [
              { key: true, hidden: true, name: 'AssessmentPointSettingId', index: 'AssessmentPointSettingId' },
              { key: false, hidden: true, name: 'FieldId', index: 'FieldId' },
              {
                  key: false, name: 'FieldText', index: 'FieldText', formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('#assessGroup-field').html($('#assessGroup').val());
                          $('.form').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              $('.form').mapJson(row);
                              showdiag(2);
                          }
                      }
                  }
              },
              { key: false, name: 'ScoreValue', index: 'ScoreValue', width: 50 }],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].AssessmentPointSettingId : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.AssessmentPointSettingId > maxId) maxId = d.AssessmentPointSettingId;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;
            }
        });

        $("#grid").jqGrid('bindKeys');
    };
    creategrid();

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'><td>Field Text</td><td>Score</td> </tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_AssessmentScores.xls");
        }
        else                 //other browser not tested on IE 11
            //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));        
            var data_type = 'data:application/vnd.ms-excel';
        a.href = data_type + ', ' + encodeURIComponent(tab_text);
        a.download = 'ALC_AssessmentScores' + '.xls';
        a.click();

        //return (sa);
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();

    });
    //========================
});