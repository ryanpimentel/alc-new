﻿$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }
    $("#txtPhone, #txtFax").mask("(999)999-9999");
    $("#txtAge").forceNumericOnly();
    $("#txtBirthDate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Carestaff",
        dialogClass: "physicianDialog",
        open: function () {
        }
    });

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "carestaff", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('.form').extractJson();
        }

        var data = { func: "carestaff", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: $(window).width() - 700,
            height: 400,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({ message: "Carestaff " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                            $("#diag").dialog("close");
                            $('#grid').trigger('reloadGrid');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('.form').clearFields();
            showdiag(1);
        } else if (q.is('.del')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Name + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = row.Name + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3)
                                    $('#grid').trigger('reloadGrid');
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.unsign')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
                    setTimeout(function () {
                        if (row) {
                            doAjax(4, { Id: row.Id }, function (m) {
                                if (m.result == 0)
                                    $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to deactivate.")
            }
        }
    });


    $(function () {
        function formatPhoneNumber(cellvalue, options, rowObject) {
            if (cellvalue == undefined) return '';
            var re = /\D/;
            // test for this format: (xxx)xxx-xxxx
            var re2 = /^\({1}\d{3}\)\d{3}-\d{4}/;
            // test for this format: xxx-xxx-xxxx
            //var re2 = /^\d{3}-\d{3}-\d{4}/;
            var num = cellvalue;
            if (num === null) {
                num = "";
            }
            var newNum = num;
            if (num != "" && re2.test(num) != true) {
                if (num != "") {
                    while (re.test(num)) {
                        num = num.replace(re, "");
                    }
                }
                if (num.length == 10) {
                    // for format (xxx)xxx-xxxx
                    newNum = '(' + num.substring(0, 3) + ')' + num.substring(3, 6) + '-' + num.substring(6, 10);
                    // for format xxx-xxx-xxxx
                    // newNum = num.substring(0,3) + '-' + num.substring(3,6) + '-' + num.substring(6,10);
                }
            }
            return newNum;
        }
        var jobtype = { '1': 'Nurse', '2': 'Caregiver', '3': 'Physician', '4': 'Social Worker' };
        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=carestaff&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Name', 'Gender', 'Job Type', 'Address', 'Phone', 'Fax'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Name', index: 'Name', formatter: "dynamicLink", formatoptions: {
                      cellValue: function (val, rowid, row, options) {
                          var name = [];
                          if (row.Firstname != "")
                              name.push(row.Firstname + " ");
                          if (row.MI != "")
                              name.push(row.MI + " ");
                          if (row.Lastname != "")
                              name.push(row.Lastname + " ");
                          return name.join('');
                      },
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('.form').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax(0, row, function (res) {
                                  $('.form').mapJson(res);
                                  showdiag(2);
                              });
                          }
                      }
                  }
              },
              { key: false, name: 'Gender', index: 'Gender', edittype: "select", formatter: 'select', editoptions: { value: { 'M': 'Male', 'F': 'Female' } } },
              { key: false, name: 'JobType', index: 'JobType', edittype: "select", formatter: 'select', editoptions: { value: jobtype } },
              {
                  key: false, name: 'Address', index: 'Address', editable: true, resizable: true, formatter:
                    function (val, rowid, row, options) {
                        var addr = [];
                        if (row.Address1 && row.Address1 != "")
                            addr.push(row.Address1 + " ");
                        if (row.Address2 && row.Address2 != "")
                            addr.push(row.Address2 + " ");
                        if (row.City && row.City != "")
                            addr.push(row.City + " ");
                        if (row.State && row.State != "")
                            addr.push(row.State + " ");
                        if (row.Zip && row.Zip != "")
                            addr.push(row.Zip + " ");
                        return addr.join('');
                    }
              },
              { key: false, name: 'Phone', index: 'Phone', editable: true, formatter: formatPhoneNumber },
              { key: false, name: 'Fax', index: 'Fax', editable: true, formatter: formatPhoneNumber }],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false
        });
    });

});