﻿var fLastRow;
var _selectedSupplyId;
var _admissionid;
var _residentname;
var _residentid;

var _supplyid;
var _supplyname;
var _generic;
var _qty;

$(document).ready(function () {
    $("#txtDisposalDate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    $("#txtDateFilled").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");

    //if (typeof (PHL) !== 'undefined') {
    //    if (PHL.destroy) PHL.destroy();
    //}

    PHL = $("#roomContent");
    PHL.layout({
        north: {
            paneSelector: "#header",
            size: '50%',
            minSize: '40%'
        },
        center: {
            paneSelector: "#middle",
            size: '50%',
            minSize: '50%'
        },
        south: {
            paneSelector: "#bottom",
            size: '0%',
            minSize: '0%'
        },
        autoResize: true,
        resizerDragOpacity: .6,
        livePanelResizing: false,
        closable: false,
        slidable: false,
        onresize: function () {
            //resizeGrids();
        }
    });

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "New Destruction",
        dialogClass: "companiesDialog",
        open: function () {
            //if (!$("#residentForm").data("layoutContainer")) {
            //    $("#residentForm").layout({
            //        center: {
            //            paneSelector: ".tabPanels",
            //            closable: false,
            //            slidable: false,
            //            resizable: true,
            //            spacing_open: 0
            //        },
            //        north: {
            //            paneSelector: ".tabs",
            //            closable: false,
            //            slidable: false,
            //            resizable: false,
            //            spacing_open: 0,
            //            size: 27
            //        }
            //    });
            //}
            //$("#residentForm").layout().resizeAll();
            //if (!$("#residentForm").data("tabs")) {
            //    $("#residentForm").tabs();
            //}
            //$("#residentForm").tabs("option", "selected", 0);
        }
    });

    $("#diag2").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Inventory Movement",
        dialogClass: "companiesDialog",
        open: function () {

        }
    });
    $(function () {
        $("#txtReligiousAdvisorTelephone,#txtResponsiblePersonTelephone,#txtNearestRelativeTelephone").mask("(999)999-9999");
        $("#residentForm").tabs();
        $("#txtBirthdate, #txtDateLeft,#txtDateAdmitted").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0"
        });//("option", "dateFormat", "mm/dd/yy");
    });

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "resident", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('.form').extractJson();
        }
        //validate Tab Grids
        var fGridData = $.grep($('#fGrid').jqGrid('getGridParam', 'data'), function (n, i) {
            return (n.E == true) || (n.D == 'true');//post only marked with E=edited and D=deleted rows
        });
        var oGridData = $.grep($('#oGrid').jqGrid('getGridParam', 'data'), function (n, i) {
            return (n.E == true) || (n.D == 'true');
        });

        var reqObj = null;
        $.each(fGridData, function (i) {
            if (this.Name == '' && this.D != 'true') {
                isValid = false;
                reqObj = this;
                return;
            }
        });
        if (!isValid) {
            $("#residentForm").tabs("option", "selected", 1);
            alert('Name field is required.')
            $('#fGrid').jqGrid('editRow', reqObj.RId);

            return;
        }

        $.each(oGridData, function (i) {
            if (this.Name == '' && this.D != 'true') {
                isValid = false;
                reqObj = this;
                return;
            }
        });
        if (!isValid) {
            $("#residentForm").tabs("option", "selected", 2);
            alert('Name field is required.')
            $('#oGrid').jqGrid('editRow', reqObj.RId);
            return;
        }

        var data = { func: "resident", mode: mode, data: row };
        data.data.fGrid = fGridData;
        data.data.oGrid = oGridData;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var createTabGrids = function (mode) {
        var lastRow = null;
        if ($('#fGrid')[0] && $('#fGrid')[0].grid)
            $.jgrid.gridUnload('fGrid');
        var FGrid = $('#fGrid');

        FGrid.jqGrid({
            datatype: 'local',
            //data: [{ RId: '1', Name: 'Harold Brunner', Address: '123 AB St. 2nd Ave Los Amigos Mexico', Telephone: '(234)123233', E: false }],
            colNames: ['RId', 'Name', 'Address', 'Telephone', 'E', 'D'],
            colModel: [
              { key: true, hidden: true, name: 'RId', index: 'RId' },
              { key: false, name: 'Name', index: 'Name', width: 200, editable: true, edittype: 'text' },
              { key: false, name: 'Address', index: 'Address', width: 300, editable: true, edittype: 'text' },
              {
                  key: false, name: 'Telephone', index: 'Telephone', width: 100, editable: true, edittype: 'custom',
                  editoptions: {
                      custom_element: function (value, options) {
                          var el = document.createElement("input");
                          el.type = "text";
                          el.value = value;
                          $(el).mask("(999)999-9999");
                          return el;
                      },
                      custom_value: function (elem, operation, value) {
                          if (operation === 'get') {
                              return $(elem).val();
                          } else if (operation === 'set') {
                              $('input', elem).val(value);
                          }
                      }
                  }
              },
              { key: false, hidden: true, name: 'E', index: 'E', edittype: 'text' },
              { key: false, hidden: true, name: 'D', index: 'D', edittype: 'text' }],
            //pager: jQuery('#pager'),
            //rowNum: 1000000,
            rowList: [],
            pgbuttons: false,
            pgtext: null,
            height: '100%',
            //cellEdit: true,
            viewrecords: false,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            editurl: 'clientArray',
            cellsubmit: 'clientArray',
            beforeSelectRow: function (rowid, e) {
                if (FGrid.attr('lastRow') != 'undefined') {
                    lastRow = FGrid.attr('lastRow');
                }

                if (lastRow != rowid) {
                    FGrid.jqGrid('saveRow', lastRow);
                    var row = FGrid.jqGrid('getRowData', lastRow);
                    row.E = true;
                    FGrid.jqGrid('setRowData', lastRow, row);
                }

                var attr = $('#fGrid tr[id=' + rowid + ']').attr('editable');
                if (typeof attr === typeof undefined || attr == 0)
                    FGrid.jqGrid('editRow', rowid);

                FGrid.attr('lastRow', rowid);
            }
        });

        if ($('#oGrid')[0] && $('#oGrid')[0].grid)
            $.jgrid.gridUnload('oGrid');
        var oGrid = $('#oGrid');

        oGrid.jqGrid({
            datatype: 'local',
            //data: [{ RId: '1', Name: 'Shanon Stoney', Address: '422 John St. 2nd Ave. West Harbor', Telephone: '(234)856867', E:false }],
            colNames: ['RId', 'Name', 'Address', 'Telephone', 'E', 'D'],
            colModel: [
              { key: true, hidden: true, name: 'RId', index: 'RId' },
              { key: false, name: 'Name', index: 'Name', width: 200, editable: true, edittype: 'text' },
              { key: false, name: 'Address', index: 'Address', width: 300, editable: true, edittype: 'text' },
              {
                  key: false, name: 'Telephone', index: 'Telephone', width: 100, editable: true, edittype: 'custom',
                  editoptions: {
                      custom_element: function (value, options) {
                          var el = document.createElement("input");
                          el.type = "text";
                          el.value = value;
                          $(el).mask("(999)999-9999");
                          return el;
                      },
                      custom_value: function (elem, operation, value) {
                          if (operation === 'get') {
                              return $(elem).val();
                          } else if (operation === 'set') {
                              $('input', elem).val(value);
                          }
                      }
                  }
              },
              { key: false, hidden: true, name: 'E', index: 'E' },
              { key: false, hidden: true, name: 'D', index: 'D' }],
            pager: jQuery('#pager'),
            rowList: [],
            pgbuttons: false,
            pgtext: null,
            //rowNum: 1000000,
            //rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: false,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            editurl: 'clientArray',
            cellsubmit: 'clientArray',
            beforeSelectRow: function (rowid, e) {
                if (oGrid.attr('lastRow') != 'undefined') {
                    lastRow = oGrid.attr('lastRow');
                }
                if (lastRow != rowid) {
                    oGrid.jqGrid('saveRow', lastRow);
                    var row = $('#oGrid').jqGrid('getRowData', lastRow);
                    row.E = true;
                    oGrid.jqGrid('setRowData', lastRow, row);
                }
                var attr = $('#oGrid tr[id=' + rowid + ']').attr('editable');
                if (typeof attr === typeof undefined || attr == 0)
                    oGrid.jqGrid('editRow', rowid);

                oGrid.attr('lastRow', rowid);
            }
        });
    }



    var SearchTextMedicine = "Search Medication...";
    $("#txtSupplyName").val(SearchTextMedicine).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextMedicine);
    }).focus(function (e) {
        if ($(this).val() == SearchTextMedicine)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_medicine", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id,
                            generic: item.generic
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 2,
        focus: function (event, ui) {
            $("#txtSupplyName").val(ui.item.label);
            $("#txtGeneric").val(ui.item.generic);
            $('#txtSupplyId').val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search Medicine name...") {
                $("#addResident").hide();
                return false;
            }
            $("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };



    var showdiag = function (mode) {
        createTabGrids();
        $("#diag").dialog("option", {
            width: 500,
            height: 500,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var isValid = $('.form').validate();
                        //do other modes
                        var a = parseFloat($("#txtQty").val());

                        if (isNaN(a) == true) {
                            alert('Please Enter Proper Qty...');
                            return;
                        }

                        row = $('.form').extractJson();


                        var data = { func: "destruction", mode: mode, data: row };
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Admin/PostGridData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                //if (cb) cb(m);
                            }
                        });
                        alert('Saved!');
                        $("#diag").dialog("close");
                        refreshGrid();
                        refreshGrid2();
                        refreshGrid3();
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }
    var showdiagCSRDetails = function () {
        refreshGrid3();
        $("#diag2").dialog("option", {
            width: $(window).width() - 400,
            height: $(window).height() - 200,
            position: "center",
            buttons: {
                "New Destruction": {
                    click: function () {
                        if ((_admissionid == '') || (_admissionid == null)) {
                            alert('Please Select an Admitted Resident..');
                            return;
                        }
                        $("#txtAdmissionIdAdd").val(_admissionid);
                        $("#txtResidentIdAdd").val(_residentid);
                        $("#txtResidentNameAdd").val(_residentname);

                        $("#txtSupplyId").val(_supplyid);
                        $("#txtSupplyName").val(_supplyname);
                        $("#txtGeneric").val(_generic);
                        $("#txtQty").val(_qty);
                        showdiag(1);
                    },
                    class: "primaryBtn",
                    text: "New Destruction"
                },
                "Close": function () {
                    $("#diag2").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('#tabs-2,#tabs-3').click(function (e) {
        var G = '#fGrid';
        if (this.id == 'tabs-3') G = '#oGrid';
        var trgt = $(e.target);
        //debugger;
        if (trgt.attr('role') == 'gridcell' || trgt.is('.add') || trgt[0].id.indexOf('jqg') == 0 || trgt.hasClass('editable') || trgt.hasClass('customelement')) return true;
        var editRow = $(G + ' tr[editable=1]');
        if (editRow.length > 0) {
            editRow.each(function (i) {
                $(G).jqGrid('saveRow', this.id);
                var row = $(G).jqGrid('getRowData', this.id);
                row.E = true;
                $(G).jqGrid('setRowData', this.id, row);
            });
        }
    });


    $('#receivemedsGridToolbar').on('click', 'a', function () {
        if ((_admissionid == '') || (_admissionid == null)) {
            alert('Please Select an Admitted Resident..');
            return;
        }
        $("#txtAdmissionIdAdd").val(_admissionid);
        $("#txtResidentIdAdd").val(_residentid);
        $("#txtResidentNameAdd").val(_residentname);
        showdiag(1);
    });


    $('#financialGridToolbar,#otherGridToolbar').on('click', 'a', function () {
        var g = $(this).parents('div.toolbar')[0].id == 'financialGridToolbar' ? '#fGrid' : '#oGrid';
        var q = $(this);
        var G = $(g);
        if (q.is('.add')) {
            var editRow = $(g + ' tr[editable=1]');
            if (editRow.length > 0) {
                editRow.each(function (i) {
                    G.jqGrid('saveRow', this.id);
                    var row = $(G).jqGrid('getRowData', this.id);
                    row.E = true;
                    $(G).jqGrid('setRowData', this.id, row);
                });
            }
            G.jqGrid('addRow', "new");
            G.attr('lastRow', G.jqGrid('getGridParam', 'selrow'));
        } else if (q.is('.del')) {
            var id = G.jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = G.jqGrid('getRowData', id);
                        if (row) {
                            row.D = 'true';
                            $(G).jqGrid('setRowData', id, row);
                            $(g + ' tr[id=' + id + ']').addClass("not-editable-row").hide();

                        }
                    }, 100);
                }

            } else {
                alert("Please select row to delete.")
            }
        }
    });


    $('#btnGenerate').on('click', function () {
        refreshGrid();
    });


    var refreshGrid = function () {
        var url = "Admin/GetGridData?func=csr&param=" + _admissionid;
        jQuery("#grid").jqGrid('setGridParam',
       {
           url: url
       }).trigger("reloadGrid");
    }

    var refreshGrid2 = function () {
        var url = "Admin/GetGridData?func=destructionhistory";
        jQuery("#grid2").jqGrid('setGridParam',
       {
           url: url
       }).trigger("reloadGrid");
    }
    var refreshGrid3 = function () {
        var url = "Admin/GetGridData?func=csrmeddetail&param=" + _admissionid + "&param2=" + _selectedSupplyId;
        jQuery("#dgrid").jqGrid('setGridParam',
       {
           url: url
       }).trigger("reloadGrid");
    }




    $(function () {
        $('#grid2').jqGrid({
            url: "Admin/GetGridData?func=destructionhistory",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Resident', 'ResidentId', 'AdmissionDate', 'AttendingPhysician', 'SupplyId', 'Generic', 'DestructionDetailId', 'Medication Name', 'Qty', 'Date Filled', 'Disposal Date', 'Prescription Number', 'Pharmacy'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              { key: false, name: 'Resident', index: 'Resident' },
              { key: false, hidden: true, name: 'ResidentId', index: 'ResidentId' },
               { key: false, hidden: true, name: 'AdmissionDate', index: 'AdmissionDate', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' } },
                { key: false, hidden: true, name: 'AttendingPhysician', index: 'AttendingPhysician' },
               { key: false, hidden: true, name: 'SupplyId', index: 'SupplyId' },
               { key: false, hidden: true, name: 'Generic', index: 'Generic' },
                { key: false, hidden: true, name: 'DestructionDetailId', index: 'DestructionDetailId' },
              {
                  key: false, name: 'Supply', index: 'Supply', formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid2');
                          G.setSelection(rowid, false);
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax(0, row, function (res) {

                                  $("#residentForm").tabs("option", "selected", 0);
                                  $("#grid4").setSelection(row.Id);


                              });
                          }
                      }
                  }
              },
              { key: false, name: 'Qty', index: 'Qty' },

              {
                  key: false, name: 'DateFilled', index: 'DateFilled', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' }

              },
              {
                  key: false, name: 'DisposalDate', index: 'DisposalDate', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' }

              },

              { key: false, name: 'PrescriptionNumber', index: 'PrescriptionNumber', },

              { key: false, name: 'Pharmacy', index: 'Pharmacy', },
            ],
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true
        });

    });


    $(function () {

        $('#dgrid').jqGrid({
            url: "Admin/GetGridData?func=csrmeddetail&param=" + _admissionid + "&param2=" + _selectedSupplyId,
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',

            colNames: ['Id', 'Description', 'Generic', 'Qty', 'Type', 'Date Filled', 'Date Administered/Disposed', 'Prescription Number', 'Pharmacy'],
            colModel: [
                { key: true, hidden: true, name: 'Id', index: 'Id' },
                { key: false, name: 'SupplyName', index: 'SupplyName' },
                { key: false, name: 'Generic', index: 'Generic' },
                { key: false, name: 'Qty', index: 'Qty' },
                { key: false, name: 'Type', index: 'Type' },
                { key: false, name: 'DateFilled', index: 'DateFilled', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' } },
                { key: false, name: 'Date', index: 'Date', formatter: 'date', formatoptions: { srcformat: 'U', newformat: 'm/d/Y' } },
                { key: false, name: 'PrescriptionNumber', index: 'PrescriptionNumber' },
                { key: false, name: 'Pharmacy', index: 'Pharmacy' },
            ],
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true
        });
    });

    $(function () {
        $('#grid4').jqGrid({
            url: "Admin/GetGridData?func=resident_admission_list&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['AdmissionId', 'ResidentId', 'Firstname', 'MiddleInitial', 'Lastname', 'Gender', 'SSN', 'Birthdate'],
            colModel: [
              { key: true, name: 'AdmissionId', index: 'AdmissionId', hidden: true },
              { key: false, name: 'ResidentId', index: 'ResidentId', hidden: true },
              { key: false, name: 'Firstname', index: 'Firstname' },
              { key: false, name: 'MiddleInitial', index: 'MiddleInitial' },
              { key: false, name: 'Lastname', index: 'Lastname' },
              { key: false, name: 'Gender', index: 'Gender', },
              { key: false, name: 'SSN', index: 'SSN', },
              { key: false, name: 'Birthdate', index: 'Birthdate', }],
            onSelectRow: function (id) {
                var rowData = jQuery(this).getRowData(id);
                setTimeout(function () { _admissionid = id; }, 100);
                setTimeout(function () { _residentname = rowData['Firstname']; }, 100);
                setTimeout(function () { _residentid = rowData['ResidentId']; }, 100);
                onFocusAdmissionGrid(id);
            },
            loadComplete: function (data) {
                if (data.length > 0) {
                    $("#grid4").setSelection(data[0].AdmissionId);
                }
            },
            //pager: jQuery('#pager'),
            scroll: true,
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'AdmissionId',
            sortorder: 'asc',
            loadonce: true
        });

        function onFocusAdmissionGrid(admissionid) {
            if ($('#grid')[0] && $('#grid')[0].grid)
                $.jgrid.gridUnload('grid');
            $('#grid').jqGrid({
                url: "Admin/GetGridData?func=csr&param=" + admissionid,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['Id', 'Description', 'Generic', 'Qty'],
                colModel: [
                  { key: true, hidden: true, name: 'Id', index: 'Id' }, {
                      key: false, name: 'SupplyName', index: 'SupplyName', formatter: "dynamicLink", formatoptions: {
                          onClick: function (rowid, iRow, iCol, cellText, e) {
                              var G = $('#grid');
                              G.setSelection(rowid, false);

                              //$('.form').clearFields();
                              var row = G.jqGrid('getRowData', rowid);
                              if (row) {
                                  doAjax(0, row, function (res) {
                                      _selectedSupplyId = row.Id;

                                      _supplyid = row.Id;
                                      _supplyname = row.SupplyName;
                                      _generic = row.Generic;
                                      _qty = row.Qty;
                                      showdiagCSRDetails();

                                  });
                              }
                          }
                      }
                  },
                   { key: false, name: 'Generic', index: 'Generic' },
                   { key: false, name: 'Qty', index: 'Qty' },
                ],
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false,
                sortname: 'Id',
                sortorder: 'asc',
                loadonce: true
            });
        }
    });

});