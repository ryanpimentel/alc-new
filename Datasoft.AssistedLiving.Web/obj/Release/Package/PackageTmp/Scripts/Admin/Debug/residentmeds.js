﻿$(document).ready(function () {
    PHL = $("#residentMedsContent");
    PHL.layout({
        north: {
            paneSelector: "#headerResidentMeds",
            size: '40%',
            minSize: '20%'
        },
        center: {
            paneSelector: "#middleResidentMeds",
            size: '30%',
            minSize: '20%',
            maxSize: '40%'
        },
        south: {
            paneSelector: "#bottomResidentMeds",
            size: '30%',
            minSize: '20%',
            maxSize: '40%'
        },
        autoResize: true,
        resizerDragOpacity: .6,
        livePanelResizing: false,
        closable: false,
        slidable: false,
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#headerResidentMeds").height() - 25).width($("#headerResidentMeds").width());
            $('.ui-jqgrid-bdiv').eq(1).height($("#middleResidentMeds").height() - 55).width($('#middleResidentMeds').width());
            $('.ui-jqgrid-bdiv').eq(2).height($("#bottomResidentMeds").height() - 55).width($('#bottomResidentMeds').width());
        }
        setTimeout(f, 100);
        return f;
    })();

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Resident Medication",
        dialogClass: "physicianDialog",
        open: function () {
        }
    });
    $('#txtHourIntake').timepicker();
    $('#txtHourIntake').focus(function () { $(this).select(); });
    $('#txtHourIntake').mask('99:99');
    $('#txtHourIntake').blur(function (e) {
        var dis = $(this);
        var sp = dis.val().split(':');
        if (sp.length == 1) {
            if (sp[0] > 23) dis.val('00:00');
        } else if (sp.length > 1) {
            if (sp[0] > 23) dis.val(('00' + ':' + sp[1]));
            if (sp[1] > 59) dis.val((sp[0] + ':' + '00'));
            if (sp[1] > 59 && sp[0] > 23) dis.val('00:00');
        }

    });

    var SearchText = "Search medicine...";
    $("#txtMedicine").val(SearchText).blur(function (e) {
        if ($('#txtSupplyId').val().length == 0 || $(this).val().length == 0)
            $(this).val(SearchText);
    }).focus(function (e) {
        if ($(this).val() == SearchText)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_medicine", filter: "medicine", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {
            $("#txtMedicine").val(ui.item.label);
            $('#txtSupplyId').val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item)
                $('#txtSupplyId').val('');
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "resident_medication", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        var resName = $('#txtMedicine');
        if (resName.val() == "Search medicine...")
            resName.val('');

        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                if ($('#txtSupplyId').val().length == 0) {
                    $('#txtMedicine').val("Search medicine...");
                }
                return;
            }
            var ti = moment($('#txtHourIntake').val(), 'HH:mm');

            var dow = [];
            $('.dayofweek').find('input[type="checkbox"]').each(function () {
                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun': dow.push('0'); break;
                        case 'Mon': dow.push('1'); break;
                        case 'Tue': dow.push('2'); break;
                        case 'Wed': dow.push('3'); break;
                        case 'Thu': dow.push('4'); break;
                        case 'Fri': dow.push('5'); break;
                        case 'Sat': dow.push('6'); break;
                    }
                }
            });
            if (dow.length == 0) {
                alert('Please choose day of week schedule.');
                return;
            }

            row = $('.form').extractJson();
            row.DayOfWeek = dow.join(',');
            row.TimeIntake = ti.format('MM/DD/YY HH:mm');
        }

        var data = { func: "resident_medication", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: 520,
            height: 250,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "Resident Medication " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diag").dialog("close");
                                $('#grid').trigger('reloadGrid');
                            } else {
                                $.growlError({ message: m.message, delay: 6000 });
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('.form').clearFields();
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            $('#txtResidentId').val(id);
            showdiag(1);
        } else if (q.is('.del')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Medicine + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = row.Medicine + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3)
                                    $('#grid').trigger('reloadGrid');
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.unsign')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
                    setTimeout(function () {
                        if (row) {
                            doAjax(4, { Id: row.Id }, function (m) {
                                if (m.result == 0)
                                    $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to deactivate.")
            }
        }
    });


    $(function () {
        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=resident&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Firstname', 'MiddleInitial', 'Lastname', 'Gender', 'SSN', 'Birthdate'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              { key: false, name: 'Firstname', index: 'Firstname' },
              { key: false, name: 'MiddleInitial', index: 'MiddleInitial' },
              { key: false, name: 'Lastname', index: 'Lastname' },
              { key: false, name: 'Gender', index: 'Gender', },
              { key: false, name: 'SSN', index: 'SSN', },
              { key: false, name: 'Birthdate', index: 'Birthdate', }],
            onSelectRow: function (id) {
                onFocusResidentMedsGrid(id);
            },
            loadComplete: function (data) {
                $("#grid").setSelection('1');
            },
            //pager: jQuery('#pager'),
            scroll: true,
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false
        });

        $('#pGrid').jqGrid({
            url: "Admin/GetGridData?func=resident&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Firstname', 'MiddleInitial', 'Lastname', 'Gender', 'SSN', 'Birthdate'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              { key: false, name: 'Firstname', index: 'Firstname' },
              { key: false, name: 'MiddleInitial', index: 'MiddleInitial' },
              { key: false, name: 'Lastname', index: 'Lastname' },
              { key: false, name: 'Gender', index: 'Gender', },
              { key: false, name: 'SSN', index: 'SSN', },
              { key: false, name: 'Birthdate', index: 'Birthdate', }],
            onSelectRow: function (id) {
                onFocusResidentMedsGrid(id);
            },
            loadComplete: function (data) {
                $("#pGrid").setSelection('1');
            },
            //pager: jQuery('#pager'),
            scroll: true,
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false
        });

        function onFocusResidentMedsGrid(residentid) {
            if ($('#mGrid')[0] && $('#mGrid')[0].grid)
                $.jgrid.gridUnload('mGrid');

            $('#mGrid').jqGrid({
                url: "Admin/GetGridData?func=resident_medication&param=" + residentid,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['Id', 'Medicine', 'Day Schedule', 'Intake Time', 'Is Active', 'Date Created', 'Date Updated'],
                colModel: [
                  { key: true, hidden: true, name: 'Id', index: 'Id' },
                  {
                      key: false, name: 'Medicine', index: 'Medicine', formatter: "dynamicLink", formatoptions: {
                          onClick: function (rowid, iRow, iCol, cellText, e) {
                              var G = $('#mGrid');
                              G.setSelection(rowid, false);
                              $('.form').clearFields();
                              var row = G.jqGrid('getRowData', rowid);
                              if (row) {
                                  doAjax(0, row, function (res) {
                                      $(".form").mapJson(res);
                                      //map day of week
                                      if (res.DayOfWeek != null && res.DayOfWeek != '') {
                                          var setCheck = function (el, str, idx) {
                                              if (str.indexOf(idx) != -1)
                                                  el.prop('checked', true);
                                          }
                                          var dow = res.DayOfWeek;
                                          $('.dayofweek').find('input[type="checkbox"]').each(function () {
                                              var d = $(this);
                                              switch (d.attr('name')) {
                                                  case 'Sun': setCheck(d, dow, '0'); break;
                                                  case 'Mon': setCheck(d, dow, '1'); break;
                                                  case 'Tue': setCheck(d, dow, '2'); break;
                                                  case 'Wed': setCheck(d, dow, '3'); break;
                                                  case 'Thu': setCheck(d, dow, '4'); break;
                                                  case 'Fri': setCheck(d, dow, '5'); break;
                                                  case 'Sat': setCheck(d, dow, '6'); break;
                                              }
                                          });
                                      }
                                      showdiag(2);
                                  });
                              }
                          }
                      }
                  },
                  { key: false, name: 'DayOfWeek', index: 'DayOfWeek', editable: true },
                  { key: false, name: 'TimeIntake', index: 'TimeIntake', editable: true },
                  { key: false, name: 'IsActive', index: 'IsActive', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Active', '0': 'Inactive' } } },
                  { key: false, name: 'DateCreated', index: 'DateCreated', editable: true },
                  { key: false, name: 'DateModified', index: 'DateModified', editable: true }],
                //pager: jQuery('#pager'),
                loadComplete: function (data) {
                    if ($("#mGrid").getGridParam("records") == 0)
                        $('#mPager').html('No results found.');
                    else
                        $('#mPager').html('');
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false
            });
        }
    });

});