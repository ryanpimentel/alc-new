﻿$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }

    $("#roomContent").layout({
        center: {
            paneSelector: "#supplyGrid",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#wrapToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
            , north__childOptions: {
                paneSelector: "#supplyGridToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            center__childOptions: {
                paneSelector: "#raToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            }
        },
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#supplyGrid").height() - 50).width($("#supplyGrid").width());
            $('#supply_grid').setGridWidth($("#supplyGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });
    $('#ftrText,#isActive').change(function () {
        var res = $('#ftrText').val();
        var rm = $('#ddlFilter').val();
        var rd = $('#ddlFilter :selected').text();
        var ia = $('#isActive').is(':checked');
        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'Description';
        if (rm == 2)
            col = "Generic";

        if (ia)
            ftrs.rules.push({ field: 'Status', op: 'eq', data: ia });
        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });

        $("#load_grid").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=supply&param=",
            dataType: "json",
            success: function (m) {

                $('#supply_grid').jqGrid('setGridParam', { data: m });
                $("#supply_grid")[0].p.search = ftrs.rules.length > 0;
                $("#supply_grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#supply_grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
                $("#supply_grid").trigger("reloadGrid", [{ page: 1 }]);

            }
        });


    });

    $("#txtCost").forceNumericOnly();
    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Medicines & Supplies",
        dialogClass: "companiesDialog",
        open: function () {
        }
    });
    //$.ajax({
    //    url: "Admin/GetGridData?func=supply&param=",
    //    processData: false,
    //    dataType: "json",
    //    type: 'POST',
    //    cache: false,
    //    success: function (data, textStatus, jqXHR) {
    //        var listItems = "";
    //        for (var i = 0; i < data.length; i++) {
    //            $("#ddlRyan").append('<option value="' + data[i].Description + '">' + data[i].Description + '</option>');
    //        }
    //    }
    //});

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "supply", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('.form').extractJson();
        }

        var data = { func: "supply", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: $(window).width() - 600,
            height: 300,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#supply_grid').jqGrid('getGridParam', 'selrow');
                        row = $('#supply_grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({ message: "Supply " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                            $("#diag").dialog("close");

                            if (mode == 1) isReloaded = true;
                            else {
                                fromSort = true;
                                new Grid_Util.Row().saveSelection.call($('#supply_grid')[0]);
                            }
                            $('#supply_grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                            //$('#supplies_1 a').trigger('click');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('.form').clearFields();
            showdiag(1);
        } else if (q.is('.del')) {
            var id = $('#supply_grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#supply_grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Name + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = row.Name + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                if (m.result != -3)
                                    $('#supply_grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                            });
                        }
                    }, 100);
                }

            } else {
                alert("Please select row to delete.")
            }
        }
    });


    $(function () {
        //var dataArray = [
        //    { Id: '1', Description: 'Supply 101', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFFO09', Status: '1', Cost: '655.20', Type: '1' },
        //    { Id: '2', Description: 'Supply 111', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFDO03', Status: '0', Cost: '231.20', Type: '2' },
        //    { Id: '3', Description: 'Supply 201', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFFO02', Status: '1', Cost: '112.20', Type: '1' },
        //    { Id: '4', Description: 'Supply 301', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EF4G01', Status: '0', Cost: '432.20', Type: '2' },
        //    { Id: '5', Description: 'Supply 401', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFRS55', Status: '1', Cost: '211.20', Type: '1' },

        //];

        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();

        $('#supply_grid').jqGrid({
            url: "Admin/GetGridData?func=supply&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Description', 'Form', 'Strength', 'Generic', 'Reference Drug', 'Active Ingredient', 'Supply Code', 'Status', 'Product Code', 'Product Type'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Description', index: 'Description', formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#supply_grid');
                          G.setSelection(rowid, false);
                          $('.form').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              $('.form').mapJson(row);
                              showdiag(2);
                          }
                      }
                  }
              },
              { key: false, name: 'Form', index: 'Form' },
              { key: false, name: 'Strength', index: 'Strength' },
              { key: false, name: 'Generic', index: 'Generic' },
              { key: false, name: 'ReferenceDrug', index: 'ReferenceDrug' },
              { key: false, name: 'ActiveIngredient', index: 'ActiveIngredient' },
              { key: false, name: 'SupplyCode', index: 'SupplyCode', },
              { key: false, name: 'Status', index: 'Status', edittype: "select", formatter: 'select', editoptions: { value: { true: 'Active', false: 'Inactive' } } },
              { key: false, name: 'ProductCode', index: 'ProductCode', },
              { key: false, name: 'ProductType', index: 'ProductType', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Medicine', '2': 'Supply' } } }],
            pager: jQuery('#medpager'),
            toppager: true,
            recordpos: 'left',
            position: 'right',
            shrinkToFit: true,
            rowNum: 500,
            rowList: [500, 200, 100, 40, 10],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#supply_grid").setSelection(maxId);
                delete isReloaded;
            }
        });

        $("#supply_grid").jqGrid('bindKeys');
    });

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        $("td:hidden,th:hidden", "#supply_grid").remove();
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'><td>Description</td><td>Form</td><td>Strength</td><td>Generic</td><td>Reference Drug</td><td>Active Ingredient</td><td>Supply Code</td><td>Status</td><td>Product Code</td><td>Product Type</td></tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('supply_grid'); // id of table

        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";

            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Meds&Supplies.xls");
        }
        else                 //other browser not tested on IE 11
            //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));           
            var data_type = 'data:application/vnd.ms-excel';
        a.href = data_type + ', ' + encodeURIComponent(tab_text);
        a.download = 'ALC_Meds&Supplies' + '.xls';
        a.click();

        //return (sa);        
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();

    });
    //============================

});

