﻿$(document).ready(function () {

    PHL = $("#permissionContent");
    PHL.layout({
        center: {
            paneSelector: "#headerpermission",
            size: '50%',
            minSize: 200,
            maxSize: 450
        },
        south: {
            paneSelector: "#middlepermission",
            size: '50%',
            minSize: 50,
            maxSize: 400
        },
        south__autoResize: true,
        center__autoResize: true,
        resizerDragOpacity: .6,
        livePanelResizing: false,
        closable: false,
        slidable: false,
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#headerpermission").height() - 55).width($("#headerpermission").width());
            $('.ui-jqgrid-bdiv').eq(1).height(($("#middlepermission").height() - 25) - $('#detGridToolbar').height()).width($('#middlepermission').width());
            $('#grid').setGridWidth($("#permissionGrid").width(), true);
            $('#urGrid').setGridWidth($("#userpermissionGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();


    var doAjax = function (rows, func, cb) {
        var data = { func: func, data: rows };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }
    var headGridEditable = false;
    var midGridEditable = false;
    $('#permissionGridToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.edit')) {
            headGridEditable = true;
            q.hide().parents('ul').find('a.save,a.cancel').show();
            $('#middlepermission').block({ message: null });
        } else if (q.is('.cancel') || q.is('.save')) {
            headGridEditable = false;
            q.parents('ul').find('a.save,a.cancel').hide().parents('ul').find('a.edit').show();
            $('#middlepermission').unblock();
            if (q.is('.save')) {
                //do save
                var changes = [];
                $("#grid tr.edited").each(function (i, o) {
                    var oo = $(o);
                    var data = $("#grid").jqGrid('getRowData', oo.attr('id'));
                    if (!data) return;

                    changes.push({ id: data['tree_node_id'], rid: data['role_id'], val: oo.find('input:checked').val() });
                });
                doAjax(changes, "permission", function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({ message: "Permissions saved successfully!", delay: 6000 });
                        fromSort = true;
                        new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                        init();
                    }
                });
            } else {
                init();
            }
        }
    });

    $('#detGridToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.edit')) {
            midGridEditable = true;
            q.hide().parents('ul').find('a.save,a.cancel').show();
            $('#headerpermission').block({ message: null });
        } else if (q.is('.cancel') || q.is('.save')) {
            midGridEditable = false;
            q.parents('ul').find('a.save,a.cancel').hide().parents('ul').find('a.edit').show();
            $('#headerpermission').unblock();
            if (q.is('.save')) {
                //do save
                var changes = [];
                $("#urGrid tr.edited").each(function (i, o) {
                    var oo = $(o);
                    var data = $("#urGrid").jqGrid('getRowData', oo.attr('id'));
                    if (!data) return;

                    changes.push({ id: data['tree_sub_node_id'], rid: data['role_id'], val: oo.find('input:checked').val() });
                });
                doAjax(changes, "permission_detail", function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({ message: "Permissions saved successfully!", delay: 6000 });
                        //reload grid
                        var selRowId = $("#grid").jqGrid('getGridParam', 'selrow');
                        $("#grid").setSelection(selRowId);
                    }
                });
            } else {
                var selRowId = $("#grid").jqGrid('getGridParam', 'selrow');
                $("#grid").setSelection(selRowId);
            }
        }
    });

    $('#roleSelect').change(function () {
        init();
    });

    //this is in site.layout.js.
    var GURow = new Grid_Util.Row();

    var init = function () {
        if ($('#grid')[0] && $('#grid')[0].grid)
            $.jgrid.gridUnload('grid');
        var roleid = $('#roleSelect').val();
        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=permission&param=" + roleid,
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['TreeNodeId', 'RoleId', 'Description', 'Node Permission'],
            colModel: [
              { key: true, hidden: true, name: 'tree_node_id', index: 'tree_node_id' },
              { key: false, hidden: true, name: 'role_id', index: 'role_id' },
              { key: false, name: 'tree_node_description', index: 'tree_node_description', editable: false },
              {
                  key: false, name: 'access_type_id', index: 'access_type_id', align: 'left', formatter: function radio(cellValue, option) {
                      var cv = cellValue;
                      var sel = ' checked="checked"';
                      return '<label><input type="radio" value="1" name="radio_' + option.rowId + '" ' + (cv == 1 ? sel : '') + '  />Read Only</label>' +
                          '<label><input type="radio" value="2" name="radio_' + option.rowId + '" ' + (cv == 2 ? sel : '') + '  />No Access</label>' +
                          '<label><input type="radio" value="3" name="radio_' + option.rowId + '" ' + (cv == 3 ? sel : '') + ' />Has Access</label>';
                  }
              }],
            onSelectRow: function (id) {
                if (headGridEditable) return false;
                var rowData = jQuery(this).getRowData(id);
                onFocuspermissionGrid(id, rowData['role_id']);
            },
            loadComplete: function (data) {
                if (data.length > 0) {
                    //var $this = $(this), ids = $this.jqGrid('getDataIDs'), i, l = ids.length;
                    //for (i = 0; i < l; i++) {
                    //    $this.jqGrid('editRow', ids[i], true);
                    //}

                    //$("#grid").setSelection(data[0].tree_node_id);

                    $('#permissionGrid input[type=radio]').click(function (e) {
                        if (!headGridEditable) return false;

                        var tr = $(e.target).closest('tr');
                        if (tr[0]) {
                            var rowId = tr[0].id;
                            var $grid = $("#grid");
                            $grid.setSelection(rowId);

                            $("#" + $.jgrid.jqID(rowId)).addClass("edited");
                            $grid.jqGrid("setCell", rowId, "access_type_id", "", "dirty-cell");
                        }
                    });
                    if (typeof fromSort != 'undefined') {
                        GURow.restoreSelection.call(this);
                        delete fromSort;
                        return;
                    }

                    var maxId = data && data[0] ? data[0].tree_node_id : '';
                    //if (typeof isReloaded != 'undefined') {
                    //    $.each(data, function (k, d) {
                    //        if (d.tree_node_id > maxId) maxId = d.tree_node_id;
                    //    });
                    //}
                    //if (maxId > 0)
                    $("#grid").setSelection(maxId);
                    delete isReloaded;
                }
            },
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'tree_node_id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            }
        });
        $("#grid").jqGrid('bindKeys');

        function onFocuspermissionGrid(treenodeid, roleid) {
            if ($('#urGrid')[0] && $('#urGrid')[0].grid)
                $.jgrid.gridUnload('urGrid');

            $('#urGrid').jqGrid({
                url: "Admin/GetGridData?func=permission_detail&param=" + treenodeid + "&param2=" + roleid,
                datatype: 'json',
                postData: "",
                ajaxGridOptions: { contentType: "application/json", cache: false },
                //datatype: 'local',
                //data: dataArray,
                //mtype: 'Get',
                colNames: ['TreeSubNodeId', 'RoleId', 'Description', 'Node Permission'],
                colModel: [
                  { key: true, hidden: true, name: 'tree_sub_node_id', index: 'tree_sub_node_id' },
                  { key: false, hidden: true, name: 'role_id', index: 'role_id' },
                  { key: false, name: 'tree_sub_node_description', index: 'tree_sub_node_description', editable: false },
                  {
                      key: false, name: 'access_type_id', index: 'access_type_id', align: 'left', formatter: function radio(cellValue, option) {
                          var cv = cellValue;
                          var sel = ' checked="checked"';
                          return '<label><input type="radio" value="1" name="radio_' + option.rowId + '" ' + (cv == 1 ? sel : '') + '  />Read Only</label>' +
                              '<label><input type="radio" value="2" name="radio_' + option.rowId + '" ' + (cv == 2 ? sel : '') + '  />No Access</label>' +
                              '<label><input type="radio" value="3" name="radio_' + option.rowId + '" ' + (cv == 3 ? sel : '') + ' />Has Access</label>';
                      }
                  }],
                onSortCol: function () {
                    fromSort = true;
                    GURow.saveSelection.call(this);
                },
                //pager: jQuery('#pager'),
                loadComplete: function (data) {
                    if ($("#urGrid").getGridParam("records") == 0)
                        $('#urPager').html('No results found.');
                    else
                        $('#urPager').html('');
                    $('#userpermissionGrid input[type=radio]').click(function (e) {
                        if (!midGridEditable) return false;

                        var tr = $(e.target).closest('tr');
                        if (tr[0]) {
                            var rowId = tr[0].id;
                            var $grid = $("#urGrid");
                            $grid.setSelection(rowId);

                            $("#" + $.jgrid.jqID(rowId)).addClass("edited");
                            $grid.jqGrid("setCell", rowId, "access_type_id", "", "dirty-cell");
                        }
                    });
                    if (typeof fromSort != 'undefined') {
                        GURow.restoreSelection.call(this);
                        delete fromSort;
                        return;
                    }

                    var maxId = data && data[0] ? data[0].tree_sub_node_id : '';
                    //if (typeof isReloaded != 'undefined') {
                    //    $.each(data, function (k, d) {
                    //        if (d.tree_sub_node_id > maxId) maxId = d.tree_sub_node_id;
                    //    });
                    //}
                    //if (maxId > 0)
                    $("#urGrid").setSelection(maxId);
                    delete isReloaded;
                    resizeGrids();
                },
                rowNum: 1000000,
                rowList: [10, 20, 30, 40],
                height: '100%',
                viewrecords: true,
                //caption: 'Care Staff',
                emptyrecords: 'No records to display',
                jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
                autowidth: true,
                multiselect: false,
                sortname: 'tree_sub_node_id',
                sortorder: 'asc',
                loadonce: true
            });
            $("#urGrid").jqGrid('bindKeys');
        }
    };
    init();
});