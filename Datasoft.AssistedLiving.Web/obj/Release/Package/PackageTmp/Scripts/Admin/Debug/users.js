﻿$(document).ready(function () {

    $("#userContent").layout({
        center: {
            paneSelector: "#userGrid",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#wrapToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
            , north__childOptions: {
                paneSelector: "#userGridToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            center__childOptions: {
                paneSelector: "#raToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            }
        },
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#userGrid").height() - 25).width($("#userGrid").width());
            $('#grid').setGridWidth($("#userGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });
    $('#ftrRole,#ftrDepartment,#ftrSysUser,#ftrText,#ftrStatus').change(function () {
        var res = $('#ftrText').val();
        var rm = $('#ddlFilter').val();
        var rd = $('#ddlFilter :selected').text();
        var rl = $('#ftrRole :selected').text();
        var dl = $('#ftrDepartment :selected').text();
        var ia = $('#ftrStatus :selected').val();
        var su = $('#ftrSysUser :selected').val();

        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'Id';
        if (rm == 2)
            col = "Name";
        else if (rm == 3)
            col = "Email";


        if (rl != 'All')
            ftrs.rules.push({ field: 'RoleDescription', op: 'eq', data: rl });
        if (dl != 'All')
            ftrs.rules.push({ field: 'Department', op: 'eq', data: dl });
        if (ia != '-1')
            ftrs.rules.push({ field: 'IsActive', op: 'eq', data: ia });
        if (su != '-1')
            ftrs.rules.push({ field: 'NonSysUser', op: 'eq', data: su });
        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });


    $('#txtBirthDate').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
    $('#txtPhone,#txtFax').mask("(999)999-9999");

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Employee",
        dialogClass: "physicianDialog",
        open: function () {
            if (!$("#userForm").data("layoutContainer")) {
                $("#userForm").layout({
                    center: {
                        paneSelector: ".tabPanels",
                        closable: false,
                        slidable: false,
                        resizable: true,
                        spacing_open: 0
                    },
                    north: {
                        paneSelector: ".tabs",
                        closable: false,
                        slidable: false,
                        resizable: false,
                        spacing_open: 0,
                        size: 27
                    }
                });
            }
            $("#userForm").layout().resizeAll();
            if (!$("#userForm").data("tabs")) {
                $("#userForm").tabs({
                    select: function (event, ui) {
                        if (ui.index == 2) {
                            setTimeout(initService, 1);
                        } else if (ui.index == 3) {
                            setTimeout(initRoles, 1);
                        }
                    }
                });
            }
            $("#userForm").tabs("option", "selected", 0);
        }
    });

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "user", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        if ($('#txtPassword').val() == '')
            $('#txtConfirm').attr('class', 'w200px');
        else
            $('#txtConfirm').attr('class', 'w200px required');

        var isValid = $('#tabs-1').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('#tabs-1,#tabs-2').extractJson();
            row.OtherRoles = $(".checkbox-grid li input").filter(function () {
                return $(this).attr('checked') && !$(this).attr('disabled')
            }).map(function () { return $(this).val() }).get().join(',')
        }

        delete row['Sun'];
        delete row['Mon'];
        delete row['Tue'];
        delete row['Wed'];
        delete row['Thu'];
        delete row['Fri'];
        delete row['Sat'];
        delete row['recurring'];
        delete row['Activity'];
        delete row['Onetimedate'];
        delete row['RoomId'];
        delete row['ResidentId'];
        delete row['ActivityId'];

        var data = { func: "user", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        if (mode == 1) {
            $("#diag").dialog("option", 'title', 'Employee');
        }
        $("#diag").dialog("option", {
            width: 750,
            height: 380,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);

                        doAjax(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "User " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diag").dialog("close");
                                //$('#grid').trigger('reloadGrid');
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                }
                                $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                            } else if (m.result == -4) {
                                $.growlWarning({ message: m.message, delay: 6000 });
                                return;
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }
    $('#txtPass').focus(function () {
        $(this).hide();
        $('#txtPassword').show().focus();
    });
    $('#txtPassword').blur(function () {
        if ($(this).val() == '') {
            $(this).hide();
            $('#txtPass').show();
        }
    });

    $('#chkIsSysUser').click(function () {
        if (this.checked) {
            $('.non-sys-user').show();
            $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
        } else {
            $('.non-sys-user').hide();
            $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
        }
    });

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('#txtId').removeAttr('readonly');
            $('#tabs-1,#tabs-2').clearFields();
            $('#txtPass').show();
            $('#txtPassword').hide();
            $('#chkIsActive').attr('checked', 'checked');
            $('#chkIsSysUser').attr('checked', 'checked');
            $('.non-sys-user').show();
            $('[href="#tabs-4"]').closest('li').show();
            $('[href="#tabs-3"]').closest('li').hide();
            $(".checkbox-grid li input").removeAttr('checked').removeAttr('disabled');
            showdiag(1);
        } else if (q.is('.del')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Name + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = row.Name + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3)
                                    $('#users_1 a').trigger('click');
                                //$('#grid').trigger('reloadGrid');
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.unsign')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
                    setTimeout(function () {
                        if (row) {
                            doAjax(4, { Id: row.Id }, function (m) {
                                if (m.result == 0)
                                    $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
                                if (m.result != -1 || m.result != -3) {
                                    if (mode == 1) isReloaded = true;
                                    else {
                                        fromSort = true;
                                        new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                    }
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                }
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to de-activate.")
            }
        } else if (q.is('.copy')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to copy "' + row.Name + '"?')) {
                    $('#txtId').removeAttr('readonly');
                    $('#tabs-1,#tabs-2').clearFields();
                    $('#txtPass').show();
                    $('#txtPassword').hide();
                    $('#chkIsActive').attr('checked', 'checked');
                    if (row.NonSysUser == "0") {
                        $('#chkIsSysUser').attr('checked', 'checked');
                        $('.non-sys-user').show();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
                    } else {
                        $('#chkIsSysUser').removeAttr('checked');
                        $('.non-sys-user').hide();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
                    }
                    if (row) {
                        doAjax(0, row, function (res) {
                            $("#lblSched").html('');
                            //$('.form').mapJson(res);
                            $('#ddlRole').val(res.Role);
                            $('#ddlDepartment').val(res.Department);
                            //role permission set primary role
                            $(".checkbox-grid li input").filter(function () {
                                return $(this).val() == $('#ddlRole').val()
                            }).attr('checked', true).attr('disabled', true);
                            //set other roles
                            if (res.OtherRoles != null && res.OtherRoles != '') {
                                $.each(res.OtherRoles.split(','), function (x, i) {
                                    $(".checkbox-grid li input").filter(function () {
                                        return $(this).val() == i;
                                    }).attr('checked', true);
                                });
                            }
                            //$('#txtCity').val(res.City);
                            //$('#txtState').val(res.State);
                            //$('#txtZip').val(res.Zip);
                            //$('#ddlGender').val(res.Gender);
                            if (res.ShiftStart != null && res.ShiftEnd != null) {
                                $('#txtShiftId').val(res.ShiftId);
                                $('#txtShift').val(res.Shift);
                                $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");
                            }
                            showdiag(1);
                        });
                    }
                }
            } else {
                alert("Please select row to copy.")
            }
        } else if (q.is('.upload')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            $('#resid').val(id);
            var row = $('#grid').jqGrid('getRowData', id);
            var title = "Upload image of " + row.Name;

            $('#diagupload').dialog({ title: title }).dialog('open');
        }
    });



    //Inserted column SSN on Admin/Employees - by Alona Cabrias June 21 2017
    $(function () {
        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=user",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: [' ', 'User ID', 'Name', 'Email', 'SSN', 'Role', 'Department', 'Is Active', 'User Type', 'Shift', 'Date Created', 'Date Updated', 'Employee', 'Department'],
            colModel: [
                {
                    key: false,
                    name: 'Image',
                    index: 'Image',
                    width: 150,
                    fixed: true,
                    formatter: imageFormatter,
                    //cellattr: setTitle
                },
              { hidden: true, key: true, name: 'Id', index: 'Id' },

              {
                  hidden: true, key: false, name: 'Name', index: 'Name', sortable: true, formatter: "dynamicLink", formatoptions: {
                      //cellValue: function (val, rowid, row, options) {
                      //    var name = [];
                      //    if (row.Firstname != "")
                      //        name.push(row.Firstname + " ");
                      //    if (row.MiddleInitial != "")
                      //        name.push(row.MiddleInitial + " ");
                      //    if (row.Lastname != "")
                      //        name.push(row.Lastname + " ");
                      //    return name.join('');
                      //},
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('#tabs-1,#tabs-2').clearFields();
                          $('#txtId').attr('readonly', 'readonly');
                          $('#txtPass').show();
                          $('#txtPassword').hide();
                          var row = G.jqGrid('getRowData', rowid);

                          if (row.NonSysUser == "0") {
                              $('#chkIsSysUser').attr('checked', 'checked');
                              $('.non-sys-user').show();
                              $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
                          } else {
                              $('#chkIsSysUser').removeAttr('checked');
                              $('.non-sys-user').hide();
                              $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
                          }
                          $(".checkbox-grid li input").removeAttr('checked').removeAttr('disabled');
                          if (row) {
                              doAjax(0, row, function (res) {
                                  $("#lblSched").html('');
                                  $('#tabs-1,#tabs-2').mapJson(res);
                                  if (res.ShiftStart != null && res.ShiftEnd != null)
                                      $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");

                                  if (res.Role == null || $('#carestaffRoles').val().split(",").indexOf(res.Role.toString()) == -1) {
                                      $('[href="#tabs-3"]').closest('li').hide();
                                  } else {
                                      $('[href="#tabs-3"]').closest('li').show();
                                  }
                                  showdiag(2);
                                  var title = $("#diag").dialog("option", 'title');
                                  $("#diag").dialog("option", 'title', 'Employee - ' + row.Name);
                                  //role permission set primary role
                                  $(".checkbox-grid li input").filter(function () {
                                      return $(this).val() == $('#ddlRole').val()
                                  }).attr('checked', true).attr('disabled', true);
                                  //set other roles
                                  $.each(res.OtherRoles.split(','), function (x, i) {
                                      $(".checkbox-grid li input").filter(function () {
                                          return $(this).val() == i;
                                      }).attr('checked', true);
                                  });
                              });
                          }
                      }
                  }
              },
              { hidden: true, key: false, name: 'Email', index: 'Email', editable: true, sortable: true },
              { hidden: true, key: false, name: 'SSN', index: 'SSN', editable: true, sortable: true },
              { hidden: true, key: false, name: 'RoleDescription', index: 'RoleDescription', editable: true, sortable: true },
              { hidden: true, key: false, name: 'Department', index: 'Department', editable: true, sortable: true },
              { hidden: true, key: false, name: 'IsActive', index: 'IsActive', sortable: true, edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Active', '0': 'Inactive' } } },
              { hidden: true, key: false, name: 'NonSysUser', index: 'NonSysUser', sortable: true, edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Non-System User', '0': 'System User' } } },
              { hidden: true, key: false, name: 'Shift', index: 'Shift', editable: false, sortable: false },
              { hidden: true, key: false, name: 'DateCreated', index: 'DateCreated', editable: true, sortable: true },
              { hidden: true, key: false, name: 'DateModified', index: 'DateModified', editable: true, sortable: true },
              { key: false, name: 'Info', index: 'Info', formatter: userFormatter },
              { key: false, name: 'Department', index: 'Department', editable: true, sortable: true, formatter: dataFormatter },
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            gridview: true,
            //caption: 'Care Staff',
            mtype: "GET",
            emptyrecords: 'No records to display',
            //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {

                //added by Mariel 
                $(".imgsrc, .imgname").click(function (rowid, iRow, iCol, cellText, e) {
                    var G = $('#grid');
                    rowid = $(this).parent().parent().parent().attr("id");

                    G.setSelection(rowid, false);
                    $('#tabs-1,#tabs-2').clearFields();
                    $('#txtId').attr('readonly', 'readonly');
                    $('#txtPass').show();
                    $('#txtPassword').hide();
                    var row = G.jqGrid('getRowData', rowid);
                    console.log(row);
                    if (row.NonSysUser == "0") {
                        $('#chkIsSysUser').attr('checked', 'checked');
                        $('.non-sys-user').show();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
                    } else {
                        $('#chkIsSysUser').removeAttr('checked');
                        $('.non-sys-user').hide();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
                    }
                    $(".checkbox-grid li input").removeAttr('checked').removeAttr('disabled');
                    if (row) {
                        doAjax(0, row, function (res) {
                            $("#lblSched").html('');
                            $('#tabs-1,#tabs-2').mapJson(res);
                            if (res.ShiftStart != null && res.ShiftEnd != null)
                                $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");

                            if (res.Role == null || $('#carestaffRoles').val().split(",").indexOf(res.Role.toString()) == -1) {
                                $('[href="#tabs-3"]').closest('li').hide();
                            } else {
                                $('[href="#tabs-3"]').closest('li').show();
                            }
                            showdiag(2);
                            var title = $("#diag").dialog("option", 'title');
                            $("#diag").dialog("option", 'title', 'Employee - ' + row.Name);
                            //role permission set primary role
                            $(".checkbox-grid li input").filter(function () {
                                return $(this).val() == $('#ddlRole').val()
                            }).attr('checked', true).attr('disabled', true);
                            //set other roles
                            $.each(res.OtherRoles.split(','), function (x, i) {
                                $(".checkbox-grid li input").filter(function () {
                                    return $(this).val() == i;
                                }).attr('checked', true);
                            });
                        });
                    }
                });

                if ($('#ftrRole').find('option').size() <= 0) {
                    var optsRm = ['<option>All</option>'];

                    $.each(unique($.map(data, function (o) { return o.RoleDescription })), function () {
                        optsRm.push('<option>' + this + '</option>');
                    });
                    $('#ftrRole').html(optsRm.join(''));
                }
                if ($('#ftrDepartment').find('option').size() <= 0) {
                    optsRm = ['<option>All</option>'];

                    $.each(unique($.map(data, function (o) { return o.Department })), function () {
                        optsRm.push('<option>' + this + '</option>');
                    });
                    $('#ftrDepartment').html(optsRm.join(''));
                }

                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : '';
                //if (typeof isReloaded != 'undefined') {
                //    $.each(data, function (k, d) {
                //        if (d.Id > maxId) maxId = d.Id;
                //    });
                //}
                //if (maxId > 0)
                $("#grid").setSelection(maxId);
                delete isReloaded;
            }
        });
        $("#grid").jqGrid('bindKeys');
        jQuery("#grid").jqGrid('sortableRows');
        function unique(array) {
            return $.grep(array, function (el, index) {
                return index == $.inArray(el, array);
            });
        }
    });

    //added by Mariel ---START---
    function imageFormatter(cellValue, options, rowObject) {
        var name = rowObject.Name;

        if (cellValue) {

            return "<img class='imgsrc' src='" + cellValue + "' alt='" + name + "' width='100%' style='cursor:pointer;'/>";

        } else {

            return "<img class='imgsrc' src='https://www.iconexperience.com/_img/o_collection_png/green_dark_grey/512x512/plain/user.png' alt='" + name + "' width='100%' style='cursor:pointer;'/>";

        }
    }

    $("#diagupload").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Upload Picture",
        // dialogClass: "companiesDialog",
        open: function () {

        }
    });

    $("#uploadimg").click(function () {
        var data = new FormData();
        var files = $("#ImageData").get(0).files;

        if (files.length > 0) {
            data.append("ImageData", files[0]);
            data.append("resid", $("#resid").val());
        }

        $.ajax({
            url: "Admin/UploadImg?func=user",
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (response) {
                $("#diagupload").dialog("close");
                $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
            },
            error: function (er) {
                alert(er);
            }

        });

    })

    function userFormatter(cellValue, options, rowObject) {

        var html = "";

        html += "<br><label class='userData' style='font-style:italic;'>" + (rowObject.Id || "") + "</label>";
        html += "<br><br><label class='imgname_p' ><a class='imgname'>" + (rowObject.Name || "") + "</a></label>";
        html += "<br><label class='userData'><strong>" + (rowObject.Email || "") + "</strong></label>";
        html += "<br><label class='userData'>SSN: <strong>" + (rowObject.SSN || "") + "</strong></label>";

        return html;

    }

    function dataFormatter(cellValue, options, rowObject) {

        var usertype = rowObject.NonSysUser;

        var html = "";

        html += "<br><label class='imgname_p' ><a class='imgname'>" + (rowObject.Department || "") + "</a></label>";
        html += "<br><br><label class='userData'>Role: <strong>" + (rowObject.RoleDescription || "") + "</strong></label>";
        html += "<br><label class='userData'>Shift: <strong>" + (rowObject.Shift || "") + "</strong></label>";

        return html;

    }

    //END

    var SearchShift = "";
    $("#txtShift").val(SearchShift).blur(function (e) {
        if ($(this).val().length == 0) {
            $(this).val(SearchShift);
            $('#txtShiftId').val('-1');
            $("#lblSched").html("");
        }
    }).focus(function (e) {
        if ($(this).val() == SearchShift)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "shift", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id,
                            start: item.start,
                            end: item.end
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {
            $("#txtShift").val(ui.item.label);
            $("#txtShiftId").val(ui.item.value);
            $("#lblSched").html("(" + ui.item.start + ' - ' + ui.item.end + ")");
            return false;
        },
        change: function (event, ui) {

        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a><span>" + item.label + ' - (' + item.start + ' - ' + item.end + ")</span></a>")
                .appendTo(ul);
    };


    $('#AddServiceToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            AddService();
        } else if (q.is('.del')) {
            var id = $('#sGrid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#sGrid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax2(3, { Id: row.Id }, function (m) {
                                var msg = "Task deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = "Task cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3) {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                    $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                }
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.refresh')) {
            fromSort = true;
            new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
            $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
        }
    });

    $("#diagTask").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Edit Service Task",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });

    $('#txtTimeIn').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#txtCarestaff').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#Onetimedate').trigger('change');
        }
    });
    $('#txtTimeIn').mask('99:99 xy');

    var showdiagTask = function (mode) {
        $("#diagTask").dialog("option", {
            width: 500,
            height: 500,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax2(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diagTask").dialog("close");
                                //initService();
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                }
                                $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');

                            } else if (m.result == -2) {
                                var resVisible = $('#txtResidentId').is(':visible');
                                $.growlWarning({ message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", delay: 6000 });
                                //there is already a schedule for a particular resident or room
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diagTask").dialog("close");
                }
            }
        }).dialog("open");
    }

    var doAjax2 = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "activity_schedule", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        var isValid = $('#diagTask .form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }
            row = $('#diagTask .form').extractJson();
            //validate time

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];
            delete row['recurring'];
            delete row['Activity'];
            delete row['Dept'];
            delete row['Onetimedate'];

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring) {
                row.isonetime = '0';
                $('.dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    if (d.is(':checked')) {
                        switch (d.attr('name')) {
                            case 'Sun': dow.push('0'); break;
                            case 'Mon': dow.push('1'); break;
                            case 'Tue': dow.push('2'); break;
                            case 'Wed': dow.push('3'); break;
                            case 'Thu': dow.push('4'); break;
                            case 'Fri': dow.push('5'); break;
                            case 'Sat': dow.push('6'); break;
                        }
                    }
                });

                if (dow.length == 0) {
                    alert('Please choose day of week schedule.');
                    return;
                }

            } else {
                row.isonetime = '1';
                var onetimeval = $('#Onetimedate').val();
                if (onetimeval == '') {
                    alert("Please choose a one time date schedule.");
                    return;
                }

                var onetimeday = moment(onetimeval).day();
                if ($.isNumeric(onetimeday))
                    dow.push(onetimeday.toString());

                row.ScheduleTime = onetimeval + ' ' + $('#txtTimeIn').val();
            }
            row.Recurrence = dow.join(',');
        }

        var data = { func: "activity_schedule", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    $('#txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#viewActivity').val(title);
        $('#txtCarestaff')[0].options.length = 0;
        toggleCarestafflist();
    });

    var bindDept = function (dptid, cb) {
        var data = { deptid: dptid };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetServicesAndCarestaffByDeptId",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //$('#txtCarestaff').empty();
                //$('<option/>').val('').html('').appendTo('#txtCarestaff');
                //$.each(m.Carestaffs, function (i, d) {
                //    $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaff');
                //});

                $('#txtActivity').empty();
                $('#viewActivity').val('');
                var opt = '<option val=""></option>';
                $.each(m.Services, function (i, d) {
                    opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
                });
                $('#txtActivity').append(opt);
                if (typeof (cb) !== 'undefined') cb();
            }
        });
    }

    var toggleCarestafflist = function () {
        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
        if (isrecurring)
            triggerDayofWeekClick();
        else
            $('#Onetimedate').trigger('change');

        //if housekeeper or maintenance hide room dropdown
        var val = $('#ddlDept').val();
        if (val == 5 || val == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    if ($('#ddlDept').length > 0) {
        $('#ddlDept').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('table.dayofweek input').removeAttr('disabled');
            $('#Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('table.dayofweek input').attr('disabled', 'disabled');
            $('table.dayofweek').clearFields();
            $('#Onetimedate').removeAttr('disabled');
        }
    });

    var triggerDayofWeekClick = function () {
        var a = $('#txtTimeIn').val();
        if (a == null || a == '') {
            $('#txtCarestaff').empty();
            //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
            return;
        }

        var dow = [];
        $('.dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun': dow.push('0'); break;
                    case 'Mon': dow.push('1'); break;
                    case 'Tue': dow.push('2'); break;
                    case 'Wed': dow.push('3'); break;
                    case 'Thu': dow.push('4'); break;
                    case 'Fri': dow.push('5'); break;
                    case 'Sat': dow.push('6'); break;
                }
            }
        });
        bindCarestaff(dow);

    }

    $('table.dayofweek input').click(triggerDayofWeekClick);

    var bindCarestaff = function (dow) {
        if ($('#txtTimeIn').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if ($('#txtActivity').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if (dow == null) dow = [];
        var csid = $('input[xid="hdCarestaffId"]').val();
        if (csid == "") csid = null;

        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');

        var data = { scheduletime: $('#txtTimeIn').val(), activityid: $('#txtActivity').val(), carestaffid: csid, recurrence: dow.join(',') };
        data.isonetime = isrecurring ? "0" : "1";
        data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#Onetimedate').val()).format("MM/DD/YYYY");
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#txtCarestaff').empty();
                $('<option/>').val('').html('').appendTo('#txtCarestaff');
                $.each(m, function (i, d) {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                });
            }
        });
    }


    $('#Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff(dow);
    });

    var AddService = function () {
        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#txtId').val();
            if (!id) return;
        }

        $("#ddlDept option[value='5']").remove();
        $("#ddlDept option[value='7']").remove();
        var dept = $('#ddlDept').val();
        $('#diagTask').clearFields();
        $('#liResident select').val(id);
        $('input[xid="hdCarestaffId"]').val('');
        $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
        $('input[name="recurring"]:eq(1)').removeAttr('checked');
        $('#Onetimedate').attr('disabled', 'disabled');
        $('.dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
        showdiagTask(1);
        //if housekeeper or maintenance hide room dropdown
        $('#ddlDept').val(dept);
        if (dept == 5 || dept == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    var initRoles = function () {

    }

    var initService = function () {
        if ($('#sGrid')[0] && $('#sGrid')[0].grid)
            $.jgrid.gridUnload('sGrid');
        var SGrid = $('#sGrid');

        var GURow = new Grid_Util.Row();
        var pageWidth = $("#sGrid").parent().width() - 100;

        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#txtId').val();
            if (!id) return;
        }

        SGrid.jqGrid({
            url: "Admin/GetGridData?func=activity_schedule&param3=" + id,// + $('#ftrRecurrence').val(),
            datatype: 'json',
            postData: '',
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Activity', 'Department', 'Resident', 'Staff', 'Room', 'Schedule', 'Recurrence', 'Duration (Minutes)'/*,'Date',  'Completed', 'Remarks'*/],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Activity', index: 'Activity', sortable: true, width: (pageWidth * (8 / 100)), formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, irow, icol, celltext, e) {
                          var G = $('#sGrid');
                          G.setSelection(rowid, false);
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax2(0, row, function (res) {
                                  $("#diagTask .form").clearFields();
                                  bindDept(res.Dept.toString(), function () {
                                      if (!res.IsOneTime)
                                          res.Onetimedate = '';
                                      $("#diagTask .form").mapJson(res);
                                  });
                                  $("#ddlDept option[value='5']").remove();
                                  $("#ddlDept option[value='7']").remove();
                                  //if housekeeper or maintenance hide room dropdown
                                  if (res.Dept == 5 || res.Dept == 7) {
                                      $('#liRoom, #liRoom select').show();
                                      $('#liResident, #liResident select').hide();
                                  } else {
                                      $('#liResident, #liResident select').show();
                                      $('#liRoom, #liRoom select').hide();
                                  }
                                  //map day of week
                                  if (!res.IsOneTime) {
                                      $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
                                      if (res.Recurrence != null && res.Recurrence != '') {
                                          var setCheck = function (el, str, idx) {
                                              if (str.indexOf(idx) != -1)
                                                  el.prop('checked', true);
                                          }
                                          var dow = res.Recurrence;
                                          $('.dayofweek').find('input[type="checkbox"]').each(function () {
                                              var d = $(this);
                                              switch (d.attr('name')) {
                                                  case 'Sun': setCheck(d, dow, '0'); break;
                                                  case 'Mon': setCheck(d, dow, '1'); break;
                                                  case 'Tue': setCheck(d, dow, '2'); break;
                                                  case 'Wed': setCheck(d, dow, '3'); break;
                                                  case 'Thu': setCheck(d, dow, '4'); break;
                                                  case 'Fri': setCheck(d, dow, '5'); break;
                                                  case 'Sat': setCheck(d, dow, '6'); break;
                                              }
                                          });
                                          //debugger;
                                          $('input[name="recurring"]:eq(0)').trigger('click');
                                          $('#Onetimedate').val('');
                                      }
                                  } else {
                                      $('input[name="recurring"]:eq(1)').trigger('click');
                                      res.Recurrence = moment(res.Onetimedate).day().toString();
                                  }
                                  $('input[xid="hdCarestaffId"]').val(res.CarestaffId);
                                  var data = { scheduletime: res.ScheduleTime, activityid: res.ActivityId, carestaffid: res.CarestaffId, recurrence: res.Recurrence };
                                  data.isonetime = !res.IsOneTime ? "0" : "1";
                                  data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
                                  $.ajax({
                                      type: "POST",
                                      contentType: "application/json; charset=utf-8",
                                      url: "Admin/GetCarestaffByTime",
                                      data: JSON.stringify(data),
                                      dataType: "json",
                                      success: function (m) {
                                          $('#txtCarestaff').empty();
                                          $('<option/>').val('').html('').appendTo('#txtCarestaff');
                                          $.each(m, function (i, d) {
                                              $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                                          });
                                          $('#txtCarestaff').val(res.CarestaffId);
                                          showdiagTask(2);
                                      }
                                  });
                              });
                          }
                      }
                  }
              },
              { key: false, name: 'Department', index: 'Department', sortable: true, width: (pageWidth * (8 / 100)) },
              { key: false, name: 'Resident', index: 'Resident', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'Carestaff', index: 'Carestaff', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'Room', index: 'Room', sortable: true, width: (pageWidth * (4 / 100)) },
              //{ key: false, name: 'ScheduleDate', index: 'ScheduleDate' },
              { key: false, name: 'Schedule', index: 'Schedule', sortable: true, width: (pageWidth * (8 / 100)) },
               { key: false, name: 'Recurrence', index: 'Recurrence', sortable: true, width: (pageWidth * (8 / 100)) },
              { key: false, name: 'Duration', index: 'Duration', sortable: true, width: (pageWidth * (5 / 100)) },
              //{ key: false, name: 'Completed', index: 'Completed', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Yes', '0': 'No' } } },
              //{ key: false, name: 'Remarks', index: 'Remarks' }
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                //if ($('#ftrResident').find('option').size() > 0) return;
                //var optsRes = ['<option>All</option>'], optsCS = ['<option>All</option>'], optsDP = ['<option>All</option>'];
                //$.each(unique($.map(data, function (o) { return o.Resident })), function () {
                //    optsRes.push('<option>' + this + '</option>');
                //});
                //$.each(unique($.map(data, function (o) { return o.Carestaff })), function () {
                //    optsCS.push('<option>' + this + '</option>');
                //});
                //$.each(unique($.map(data, function (o) { return o.Department })), function () {
                //    optsDP.push('<option>' + this + '</option>');
                //});
                //$('#ftrResident').html(optsRes.join(''));
                //$('#ftrCarestaff').html(optsCS.join(''));
                //$('#ftrDept').html(optsDP.join(''));

                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#sGrid").setSelection(maxId);
                delete isReloaded;
            }
        });
        SGrid.jqGrid('bindKeys');
        $('.tabPanels').layout({
            center: {
                paneSelector: "#serviceGrid",
                closable: false,
                slidable: false,
                resizable: true,
                spacing_open: 0
            },
            north: {
                paneSelector: "#AddServiceToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            onresize: function () {
                resizeGridsS();
                //resizeGridHS();
            }
        });
        resizeGridsS();
    }

    var resizeGridsS = (function () {
        //debugger;
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#AddServiceToolbar').height();
            $("#sGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#sGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
            //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
            //console.log($("#sGrid").height())
        }
        setTimeout(f, 100);
        return f;
    });

    $("#ddlRole").change(function () {
        $(".checkbox-grid li input")
            .removeAttr('checked')
            .removeAttr('disabled')
            .filter(function () {
                return $(this).val() == $('#ddlRole').val();
            }).attr('checked', true).attr('disabled', true);
    })

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'><td></td><td>Employee</td><td>Department</td> </tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Employees.xls");
        }
        else                 //other browser not tested on IE 11
            //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));           
            var data_type = 'data:application/vnd.ms-excel';
        a.href = data_type + ', ' + encodeURIComponent(tab_text);
        a.download = 'ALC_Employees' + '.xls';
        a.click();

        //return (sa);
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();

    });
    //============================

});