﻿var Calendar = (function () {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var cellClasses = { 1: "typeCont", 2: "typeGen", 3: "typeInp", 4: "typeRout" };
    var icons = { "551": "snp.png", "421": "ptp.png", "431": "otp.png", "441": "stp.png", "561": "swp.png", "571": "hhap.png" };
    var settings = { startDate: null, endDate: null, facilities: [] };
    var selectedDate = "";

    var init = function (options) {
        if (options.startDate) settings.startDate = new Date(options.startDate);
        if (options.endDate) settings.endDate = new Date(options.endDate);
        if (options.facilities) {
            settings.facilities = [];
            $.each(options.facilities, function (i, item) {
                var loc = {
                    facility_id: item.facility_id, facility_reservation_id: item.facility_reservation_id,
                    schedule: item.schedule, reservation_start: new Date(item.reservation_start), reservation_end: new Date(item.reservation_end)
                };
                settings.facilities.push(loc);
            });
        }
    }

    var limit = function (val, max, step) {
        return val < max ? val - step : max;
    };

    var renderHeaderHtml = function () {
        var html = '';//"<td>Week</td>";
        for (var i = 0; i < days.length; i++) {
            html += "<td>" + days[i] + "</td>";
        }

        return "<thead><tr>" + html + "</tr><thead>";
    };

    var getCellDesc = function (d) {
        var html = "";
        $.each(settings.facilities, function (i, item) {
            if (d.valueOf() == item.reservation_start.valueOf() || d.valueOf() == item.reservation_end.valueOf()) {
                var schedule = item.schedule;
                if (schedule.length > 10)
                    schedule = $.trim(item.schedule).substring(0, 10) + '...';
                html += "<a facility_id=\"" + item.facility_id + "\" id=\"" + item.facility_reservation_id + "\" href=\"#\" class=\"visit_icon\">" + schedule + "</a>";
            }
        });

        return html;
    }

    var renderCellHtml = function (w, d) {
        var html = "<td><table";
        if (d != null) html += " id=\"" + (d.getMonth() + 1) + "_" + d.getDate() + "_" + d.getFullYear() + "\"";
        html += " class=\"planBlock";
        if (d != null) html += " day";
        html += "\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td class=\"date\">";
        //if (w != null) html += w;
        if (d != null) html += months[d.getMonth()] + " " + d.getDate();
        html += "</td></tr><tr><td class=\"planned\"><div>";
        if (d != null) html += getCellDesc(d);
        if (w != null) html += "";
        html += "</div></table></td>";//</td></tr><tr><td class=\"actual\"><div>";
        //html += "</div></td></tr><tr><td class=\"actual\"><div>";
        //if (d != null) html += getCellIcons(d, true);
        //if (w != null) html += "Actual";
        //html += "</div></td></tr></table></td>";

        return html;
    }

    var renderBodyHtml = function () {
        var html = "<tr>";
        for (var i = 0; i < settings.startDate.getDay() ; i++) {
            //if (i == 0) html += renderCellHtml(1);
            html += renderCellHtml();
        }

        var day = settings.startDate;
        var week = 1;
        for (; day <= settings.endDate;) {
            //if (day.getDay() == 0) html += renderCellHtml(week);
            html += renderCellHtml(null, day);
            if (day.getDay() == 6) {
                html += "</tr><tr>";
                week += 1;
            }
            day.setDate(day.getDate() + 1);
        }

        for (var i = settings.endDate.getDay() ; i < 6; i++) {
            html += renderCellHtml();
        }
        html += "</tr>"

        return "<tbody>" + html + "</tbody>";
    }

    var renderTableHtml = function () {
        var html = "<table class=\"clearFloats03\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
        html += renderHeaderHtml();
        html += renderBodyHtml();
        html += "</table>";

        return html;
    }
    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {

            var data = { func: "facility_reservation", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }
            //validate dates and time
            var SD = moment($('#txtStartDate').val());
            var ED = moment($('#txtEndDate').val());
            if(SD.format() == 'Invalid date'){
                var c = $('#txtStartDate');
                if (!c.next("span.error")[0])
                    c.after("<span class=\"error\" title=\"Invalid date format\"></span>").addClass("error");
                return;
            }

            if(ED.format() == 'Invalid date'){
                var c = $('#txtEndDate');
                if (!c.next("span.error")[0])
                    c.after("<span class=\"error\" title=\"Invalid date format\"></span>").addClass("error");
                return;
            }



            if (SD.valueOf() > ED.valueOf()) {
                var c = $('#txtEndDate');
                if (!c.next("span.error")[0])
                    c.after("<span class=\"error\" title=\"Start date should be less than or equal to end date\"></span>").addClass("error");
                return;
            }

            SD = moment($('#txtTimeStart').val(), 'h:mmA');
            ED = moment($('#txtTimeEnd').val(), 'h:mmA');

            if(SD.format() == 'Invalid date'){
                var c = $('#txtTimeStart');
                if (!c.next("span.error")[0])
                    c.after("<span class=\"error\" title=\"Invalid time format\"></span>").addClass("error");
                return;
            }

            if(ED.format() == 'Invalid date'){
                var c = $('#txtTimeEnd');
                if (!c.next("span.error")[0])
                    c.after("<span class=\"error\" title=\"Invalid time format\"></span>").addClass("error");
                return;
            }

            if (SD.valueOf() > ED.valueOf()) {
                var c = $('#txtTimeEnd');
                if (!c.next("span.error")[0])
                    c.after("<span class=\"error\" title=\"Start time should be less than or equal to end time\"></span>").addClass("error");
                return;
            }

            row = $('.form').extractJson();
            row.ReservationStart = moment($('#txtStartDate').val() + ' ' + $('#txtTimeStart').val(), 'MM/DD/YYYY h:mmA').format();
            row.ReservationEnd = moment($('#txtEndDate').val() + ' ' + $('#txtTimeEnd').val(), 'MM/DD/YYYY h:mmA').format();
        }
        var data = { func: "facility_reservation", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
        
    }
    var showdiag = function (en) {
        var id = $('#txtId').val();
        var mode = $.isNumeric(id) ? 2 : 1;
        if (mode == 1) {
            var fr = $('#filterExp').val();
            var frArr = fr.split('_');
            if (frArr != null && frArr.length > 0) {
                if (frArr[0].indexOf('facility') != -1)
                    $('#txtFacilityId').val(frArr[1]);
            }
        }
        $("#diag").dialog("option", {
            width: 500,//$(window).width() - 250,
            height: 350,//$(window).height() - 250,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var row = {};
                        doAjax(mode, row, function (m) {
                                $.growlSuccess({ message: "Reserve Facility " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diag").dialog("close");
                                $('#btnReload').trigger('click');
                        });

                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    var createPopupMenus = function () {
        if ($("body:not(:has(#calendarMenu1))")[0]) {
            var menuOptionsHtml = "<li><a href=\"#\" class=\"visit\">Reserve Facility</a></li>";// +
            //"<li class=\"separator\"></li>" +
            //"<li><a href=\"#\" class=\"template\">Menu 1</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 2</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 3</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 4</a>" +
            //"<li><a href=\"#\" class=\"template\">Menu 5</a>";
            $("body").append("<div id=\"calendarMenu1\" class=\"popupMenu\"><ul>" + menuOptionsHtml + "</ul></div>");
        }
        //if ($("body:not(:has(#calendarMenu2))")[0]) {
        //    var menuOptionsHtml = "<li><a href=\"#\" class=\"supply\">Supply</a></li>" +
        //        "<li><a href=\"#\" class=\"wound\">Wound Note</a></li>" +
        //        "<li class=\"separator\"></li>" +
        //        "<li><a href=\"#\" class=\"levelOfCare\">Level of Care</a></li>" +
        //        "<li><a href=\"#\" class=\"order\">Physician Order</a></li>" +
        //        "<li><a href=\"#\" class=\"communicationNote\">Communication Note</a></li>";
        //    $("body").append("<div id=\"calendarMenu2\" class=\"popupMenu\"><ul>" + menuOptionsHtml + "</ul></div>");
        //}
        $(".popupMenu").on("click", "a", function (e) {
            var $window = $(window);
            var $a = $(this);
            if ($a.is(".visit")) {
                $('.form').clearFields();
                $('#txtStartDate').val(selectedDate.replace(/_/g, '/'));
                showdiag(1);
            }
            else {
                var $dialog = $("<div><iframe frameborder=\"0\" class=\"frame\"></iframe></div>").dialog({
                    modal: true,
                    title: $a.text(),
                    width: $window.width() - 100,
                    height: $window.height() - 100,
                    close: function () {
                        $dialog.dialog("destroy").remove();
                    }
                });
                //$dialog.find(".frame").attr("src", url);
            }
            $(".popupMenu").hide();

            return false;
        });
    }

    return {
        Build: function (options) {
            init(options);
            if (settings.startDate == null && settings.endDate == null) return;
            $("#calendar").html(renderTableHtml());
            $("#calendar").on("click", ".planned, .actual", function (e) {
                var $a = $(this);
                var $d = $a.closest(".day");
                if ($d[0]) {
                    selectedDate = $d.attr("id");
                    if ($a.is(".planned")) {
                        $("#calendarMenu1").css({ top: e.pageY, left: e.pageX }).slideDown(300);
                        $("#calendarMenu2").hide();
                    }
                    else if ($a.is(".actual")) {
                        $("#calendarMenu2").css({ top: e.pageY, left: e.pageX }).slideDown(300);
                        $("#calendarMenu1").hide();
                    }
                }
            });
            $("#calendar").on("click", ".visit_icon", function (e) {
                var row = { Id: $(this).attr('id') };
                doAjax(0, row, function (res) {
                    $(".form").clearFields();
                    //$(".form").mapJson(res);
                    $(".form").mapJson(res);
                    showdiag(0);
                });
                e.preventDefault();
                return false;
            });
            $("a.visit_icon").each(function () {
                $(this).qtip({
                    content: {
                        text: 'Loading...',
                        ajax: {
                            url: "Admin/GetFormData",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            type: "POST",
                            data: JSON.stringify({ func: "facility_reservation", id: $(this).attr('id') }),
                            success: function (data, status) {
                                if (data) {
                                    var htm = [];
                                    var SD = moment(data.StartDate).format('MMM D');
                                    var ED = moment(data.EndDate).format('MMM D');
                                    var Pur = data.Purpose;
                                    if (Pur.length > 250)
                                        Pur = $.trim(Pur).substring(0, 250) + '...';
                                    htm.push('<span class="labeltip">Facility: </span><span class="valuetip">' + data.FacilityName + '</span><br/>');
                                    htm.push('<span class="labeltip">Reservation Schedule: </span><span class="valuetip">' + SD + " " +  data.TimeStart + ' - ' + ED + " " + data.TimeEnd + '</span><br/>');
                                    htm.push('<span class="labeltip">Purpose: </span><span class="valuetip">' + Pur + '</span><br/>');
                                    htm.push('<span class="labeltip">Reserved By: </span><span class="valuetip">' + data.ReservedBy + '</span><br/>');
                                    this.set('content.text', htm.join(''));
                                }
                            }
                        }
                    },
                    style: {
                        classes: "qtip-blue",
                        tip: {
                            corner: 'left center'
                        }
                    },
                    position: {
                        viewport: $('#calendar')
                    }
                });
            });
            createPopupMenus();
        }
    }
})();

$(document).ready(function () {
    $("input:checkbox").on('click', function () {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });
    $('#txtHospitalPhone,#txtPhysicianPhone,#txtPhysicianFax,#txtPharmacyPhone').mask("(999)999-9999");
    $(".content-datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
    $("#txtDischargeDate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    var minDate = new Date();

    for (var i = 2000; i < 2050; i++) {
        $('#yrfilter').append($("<option/>", {
            value: i,
            text: i
        }));
    };

    $('#monthfilter').val(minDate.getMonth() + 1);
    $('#yrfilter').val(minDate.getFullYear());

    $("#calendarLayoutContainer").layout({
        center: {
            paneSelector: "#calendar",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#raToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
        }
    });
    function buildCal(date) {
        var facilityId = null;
        var fr = $('#filterExp').val();
        var frArr = fr.split('_');
        if (frArr != null && frArr.length > 0) {
            if (frArr[0].indexOf('facility') != -1)
                facilityId = frArr[1];
        }
        var data = { startDate: date.start, endDate: date.end, facilityId: facilityId };
        $.ajax({
            url: "Admin/GetFacilityMgmtCalendar",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            success: function (response) {
                if (response) {
                    Calendar.Build({
                        startDate: data.startDate,
                        endDate: data.endDate,
                        facilities: response.Facilities
                    });
                }
            }
        });

    }

    function getMonthDateRange(year, month) {
        // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
        // array is 'year', 'month', 'day', etc
        var startDate = moment([year, month - 1]);

        // Clone the value before .endOf()
        var endDate = moment(startDate).endOf('month');

        // just for demonstration:
        //console.log(startDate.toDate());
        //console.log(endDate.toDate());

        // make sure to call toDate() for plain JavaScript date type
        return { start: startDate, end: endDate };
    }

    var buildCalendar = function () {

        var month = $('#monthfilter').val();
        var year = $('#yrfilter').val();
        minDate = getMonthDateRange(year, month);
        buildCal(minDate);
    }

    buildCalendar();

    $('#btnReload').click(function () {
        buildCalendar();
    })

    $('#filterExp,#monthfilter,#yrfilter').change(function () {
        buildCalendar();
    });


    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Reserve Facility",
        dialogClass: "companiesDialog",
        open: function (event, ui) {
        }
    });

    $('#txtTimeStart,#txtTimeEnd').timepicker({
        showPeriod: true,
        showLeadingZero: false,
        showOn: 'focus',
        timeFormat: 'h:mmp',
        onSelect: function (time) {
            $(this).val($(this).val().replace(' ', ''));
        },
        onClose: function () {
            $(this).val($(this).val().replace(' ', ''));
        }
    });

    $("#txtEndDate").datepicker({
        changeMonth: false,
        changeYear: false,
        dateFormat: 'm/dd/yy',
        yearRange: "-0:+0"
    });
});
$(document).click(function (e) {
    if (!$(e.target).closest(".day")[0] && !$(e.target).is(".popupMenu")[0])
        $(".popupMenu").slideUp(300);
});