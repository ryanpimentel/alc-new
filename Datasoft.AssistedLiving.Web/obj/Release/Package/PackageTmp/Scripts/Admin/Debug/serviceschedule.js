﻿
$(document).ready(function () {

    $("#calendarLayoutContainer").layout({
        center: {
            paneSelector: "#carestaffScheduleGrid",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#wrapToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
            , north__childOptions: {
                paneSelector: "#taskToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            center__childOptions: {
                paneSelector: "#raToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            }
        },
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#carestaffScheduleGrid").height() - 25).width($("#carestaffScheduleGrid").width());
        }
        setTimeout(f, 100);
        return f;
    })();

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Service Schedule",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });

    //var timePickerData = null;

    //var setTimePicker = function () {
    $('#txtTimeIn').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#txtCarestaff').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#Onetimedate').trigger('change');
        }
        //,

        //onHourShow: function (h) {
        //    if (timePickerData) {
        //        var d = timePickerData;
        //        if (d && d.length <= 0) return;
        //        var shiftstart = parseInt(moment(d[0].shift_start, 'HH:mm:ss').format('HH'));
        //        var shiftend = parseInt(moment(d[0].shift_end, 'HH:mm:ss').format('HH'));

        //        if (shiftstart > shiftend) {
        //            if ((h > shiftend) && (h < shiftstart))
        //                return false;
        //        } else {
        //            if (((h > shiftend) || (h < shiftstart)))
        //                return false;
        //        }
        //        var retval = true;
        //        $.each(d, function () {
        //            var st = parseInt(moment(this.service_time, 'HH:mm:ss').format('HH'));;
        //            if (st) {
        //                if (h == st) retval = false;
        //            }
        //        });
        //        return retval;
        //    }
        //    return true;
        //},
        //onMinuteShow: function (h, m) {
        //    /*
        //    if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        //    if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        //    return true;  // valid
        //    */
        //    return true;
        //}

    });
    $('#txtTimeIn').mask('99:99 xy');
    //}

    //setTimePicker();

    //$("#txtCarestaff").change(function () {
    //    //$('#txtTimeIn').val('');
    //    var val = $(this).val();
    //    //if (val == '') {
    //    //    timePickerData = null;
    //    //    setTimePicker();
    //    //    return;
    //    //}

    //    var data = { csid: val };
    //    $.ajax({
    //        type: "POST",
    //        contentType: "application/json; charset=utf-8",
    //        url: "Admin/GetCarestaffByTime",
    //        data: JSON.stringify(data),
    //        dataType: "json",
    //        success: function (m) {
    //            timePickerData = m;
    //            //setTimePicker();
    //        }
    //    });
    //});


    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "activity_schedule", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }
            row = $('.form').extractJson();
            //validate time

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];
            delete row['recurring'];
            delete row['Activity'];
            delete row['Dept'];
            delete row['Onetimedate'];

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring) {
                row.isonetime = '0';
                $('.dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    if (d.is(':checked')) {
                        switch (d.attr('name')) {
                            case 'Sun': dow.push('0'); break;
                            case 'Mon': dow.push('1'); break;
                            case 'Tue': dow.push('2'); break;
                            case 'Wed': dow.push('3'); break;
                            case 'Thu': dow.push('4'); break;
                            case 'Fri': dow.push('5'); break;
                            case 'Sat': dow.push('6'); break;
                        }
                    }
                });

                if (dow.length == 0) {
                    alert('Please choose day of week schedule.');
                    return;
                }

            } else {
                row.isonetime = '1';
                var onetimeval = $('#Onetimedate').val();
                if (onetimeval == '') {
                    alert("Please choose a one time date schedule.");
                    return;
                }

                var onetimeday = moment(onetimeval).day();
                if ($.isNumeric(onetimeday))
                    dow.push(onetimeday.toString());

                row.ScheduleTime = onetimeval + ' ' + $('#txtTimeIn').val();
            }
            row.Recurrence = dow.join(',');
        }

        var data = { func: "activity_schedule", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: 500,
            height: 500,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diag").dialog("close");
                                if ($('#grid')[0] && $('#grid')[0].grid)
                                    $.jgrid.gridUnload('grid');
                                init();
                            } else if (m.result == -2) {
                                var resVisible = $('#txtResidentId').is(':visible');
                                $.growlWarning({ message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", delay: 6000 });
                                //there is already a schedule for a particular resident or room
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            var dept = $('#ddlDept').val();
            $('.form').clearFields();
            $('input[xid="hdCarestaffId"]').val('');
            $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
            $('input[name="recurring"]:eq(1)').removeAttr('checked');
            $('#Onetimedate').attr('disabled', 'disabled');
            $('.dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
            showdiag(1);
            //if housekeeper or maintenance hide room dropdown
            $('#ddlDept').val(dept);
            if (dept == 5 || dept == 7) {
                $('#liRoom, #liRoom select').show();
                $('#liResident, #liResident select').hide();
            } else {
                $('#liResident, #liResident select').show();
                $('#liRoom, #liRoom select').hide();
            }
        } else if (q.is('.del')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = "Task deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = "Task cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3) {
                                    if ($('#grid')[0] && $('#grid')[0].grid)
                                        $.jgrid.gridUnload('grid');
                                    init();
                                }
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.unsign')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
                    setTimeout(function () {
                        if (row) {
                            doAjax(4, { Id: row.Id }, function (m) {
                                if (m.result == 0)
                                    $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to deactivate.")
            }
        }
    });

    var init = function (isAdmin) {
        var GURow = new Grid_Util.Row();
        var pageWidth = $("#grid").parent().width() - 100;
        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=activity_schedule&param=-1",// + $('#ftrRecurrence').val(),
            datatype: 'json',
            postData: '',
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Activity', 'Department', 'Resident', 'Staff', 'Room', 'Schedule', 'Recurrence', 'Duration (Minutes)'/*,'Date',  'Completed', 'Remarks'*/],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Activity', index: 'Activity', sortable: true, width: (pageWidth * (8 / 100)), formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, irow, icol, celltext, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax(0, row, function (res) {
                                  $(".form").clearFields();
                                  bindDept(res.Dept.toString(), function () {
                                      if (!res.IsOneTime)
                                          res.Onetimedate = '';
                                      $(".form").mapJson(res);
                                  });
                                  //if housekeeper or maintenance hide room dropdown
                                  if (res.Dept == 5 || res.Dept == 7) {
                                      $('#liRoom, #liRoom select').show();
                                      $('#liResident, #liResident select').hide();
                                  } else {
                                      $('#liResident, #liResident select').show();
                                      $('#liRoom, #liRoom select').hide();
                                  }
                                  //map day of week
                                  if (!res.IsOneTime) {
                                      if (res.Recurrence != null && res.Recurrence != '') {
                                          var setCheck = function (el, str, idx) {
                                              if (str.indexOf(idx) != -1)
                                                  el.prop('checked', true);
                                          }
                                          var dow = res.Recurrence;
                                          $('.dayofweek').find('input[type="checkbox"]').each(function () {
                                              var d = $(this);
                                              switch (d.attr('name')) {
                                                  case 'Sun': setCheck(d, dow, '0'); break;
                                                  case 'Mon': setCheck(d, dow, '1'); break;
                                                  case 'Tue': setCheck(d, dow, '2'); break;
                                                  case 'Wed': setCheck(d, dow, '3'); break;
                                                  case 'Thu': setCheck(d, dow, '4'); break;
                                                  case 'Fri': setCheck(d, dow, '5'); break;
                                                  case 'Sat': setCheck(d, dow, '6'); break;
                                              }
                                          });
                                          $('input[name="recurring"]:eq(0)').trigger('click');
                                          $('#Onetimedate').val('');
                                      }
                                  } else {
                                      $('input[name="recurring"]:eq(1)').trigger('click');
                                      res.Recurrence = moment(res.Onetimedate).day().toString();
                                  }
                                  $('input[xid="hdCarestaffId"]').val(res.CarestaffId);
                                  var data = { scheduletime: res.ScheduleTime, activityid: res.ActivityId, carestaffid: res.CarestaffId, recurrence: res.Recurrence };
                                  data.isonetime = !res.IsOneTime ? "0" : "1";
                                  data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
                                  $.ajax({
                                      type: "POST",
                                      contentType: "application/json; charset=utf-8",
                                      url: "Admin/GetCarestaffByTime",
                                      data: JSON.stringify(data),
                                      dataType: "json",
                                      success: function (m) {
                                          $('#txtCarestaff').empty();
                                          $('<option/>').val('').html('').appendTo('#txtCarestaff');
                                          $.each(m, function (i, d) {
                                              $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                                          });
                                          $('#txtCarestaff').val(res.CarestaffId);
                                          showdiag(2);
                                      }
                                  });
                              });
                          }
                      }
                  }
              },
              { key: false, name: 'Department', index: 'Department', sortable: true, width: (pageWidth * (8 / 100)) },
              { key: false, name: 'Resident', index: 'Resident', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'Carestaff', index: 'Carestaff', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'Room', index: 'Room', sortable: true, width: (pageWidth * (4 / 100)) },
              //{ key: false, name: 'ScheduleDate', index: 'ScheduleDate' },
              { key: false, name: 'Schedule', index: 'Schedule', sortable: true, width: (pageWidth * (8 / 100)) },
               { key: false, name: 'Recurrence', index: 'Recurrence', sortable: true, width: (pageWidth * (8 / 100)) },
              { key: false, name: 'Duration', index: 'Duration', sortable: true, width: (pageWidth * (5 / 100)) },
              //{ key: false, name: 'Completed', index: 'Completed', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Yes', '0': 'No' } } },
              //{ key: false, name: 'Remarks', index: 'Remarks' }
            ],
            loadComplete: function (data) {
                resizeGrids();
                if ($('#ftrResident').find('option').size() > 0) return;
                var optsRes = ['<option>All</option>'], optsCS = ['<option>All</option>'], optsDP = ['<option>All</option>'];
                $.each(unique($.map(data, function (o) { return o.Resident })), function () {
                    optsRes.push('<option>' + this + '</option>');
                });
                $.each(unique($.map(data, function (o) { return o.Carestaff })), function () {
                    optsCS.push('<option>' + this + '</option>');
                });
                $.each(unique($.map(data, function (o) { return o.Department })), function () {
                    optsDP.push('<option>' + this + '</option>');
                });
                $('#ftrResident').html(optsRes.join(''));
                $('#ftrCarestaff').html(optsCS.join(''));
                $('#ftrDept').html(optsDP.join(''));
                Grid.Settings.init();

                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;
            },
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true
        });

        $("#grid").jqGrid('bindKeys');
    };
    $(init);

    function unique(array) {
        return $.grep(array, function (el, index) {
            return index == $.inArray(el, array);
        });
    }

    $('#txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#viewActivity').val(title);
        $('#txtCarestaff')[0].options.length = 0;
        toggleCarestafflist();
    });

    var bindDept = function (dptid, cb) {
        var data = { deptid: dptid };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetServicesAndCarestaffByDeptId",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //$('#txtCarestaff').empty();
                //$('<option/>').val('').html('').appendTo('#txtCarestaff');
                //$.each(m.Carestaffs, function (i, d) {
                //    $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaff');
                //});

                $('#txtActivity').empty();
                $('#viewActivity').val('');
                var opt = '<option val=""></option>';
                $.each(m.Services, function (i, d) {
                    opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
                });
                $('#txtActivity').append(opt);
                if (typeof (cb) !== 'undefined') cb();
            }
        });
    }

    var toggleCarestafflist = function () {
        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
        if (isrecurring)
            triggerDayofWeekClick();
        else
            $('#Onetimedate').trigger('change');

        //if housekeeper or maintenance hide room dropdown
        var val = $('#ddlDept').val();
        if (val == 5 || val == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    if ($('#ddlDept').length > 0) {
        $('#ddlDept').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('table.dayofweek input').removeAttr('disabled');
            $('#Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('table.dayofweek input').attr('disabled', 'disabled');
            $('table.dayofweek').clearFields();
            $('#Onetimedate').removeAttr('disabled');
        }
    });

    var triggerDayofWeekClick = function () {
        var a = $('#txtTimeIn').val();
        if (a == null || a == '') {
            $('#txtCarestaff').empty();
            //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
            return;
        }

        var dow = [];
        $('.dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun': dow.push('0'); break;
                    case 'Mon': dow.push('1'); break;
                    case 'Tue': dow.push('2'); break;
                    case 'Wed': dow.push('3'); break;
                    case 'Thu': dow.push('4'); break;
                    case 'Fri': dow.push('5'); break;
                    case 'Sat': dow.push('6'); break;
                }
            }
        });
        bindCarestaff(dow);

    }

    $('table.dayofweek input').click(triggerDayofWeekClick);

    var bindCarestaff = function (dow) {
        if ($('#txtTimeIn').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if ($('#txtActivity').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if (dow == null) dow = [];
        var csid = $('input[xid="hdCarestaffId"]').val();
        if (csid == "") csid = null;

        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');

        var data = { scheduletime: $('#txtTimeIn').val(), activityid: $('#txtActivity').val(), carestaffid: csid, recurrence: dow.join(',') };
        data.isonetime = isrecurring ? "0" : "1";
        data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#Onetimedate').val()).format("MM/DD/YYYY");
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#txtCarestaff').empty();
                $('<option/>').val('').html('').appendTo('#txtCarestaff');
                $.each(m, function (i, d) {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                });
            }
        });
    }


    $('#Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff(dow);
    });

    //$('#ftrRecurrence').change(function () {
    //    if ($('#grid')[0] && $('#grid')[0].grid)
    //        $.jgrid.gridUnload('grid');
    //    init();
    //});

    $('#ftrResident,#ftrCarestaff,#ftrRecurrence,#ftrDept').change(function () {
        var res = $('#ftrResident :selected').text();
        var sta = $('#ftrCarestaff :selected').text();
        var dep = $('#ftrDept :selected').text();
        if (!$('#ftrDept')[0]) dep = 'All';
        var rec = $('#ftrRecurrence').val();
        var ftrs = {
            groupOp: "AND",
            rules: []
        };

        if (res != 'All')
            ftrs.rules.push({ field: 'Resident', op: 'eq', data: res });
        if (sta != 'All')
            ftrs.rules.push({ field: 'Carestaff', op: 'cn', data: sta });
        if (rec != '-1')
            ftrs.rules.push({ field: 'Recurrence', op: 'cn', data: rec });
        if (dep != 'All')
            ftrs.rules.push({ field: 'Department', op: 'eq', data: dep });

        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });

    //$('#ftrCarestaff').change(function () {
    //    var res = $('#ftrResident :selected').text();
    //    var sta = $('#ftrCarestaff :selected').text();
    //    $("#grid")[0].p.search = sta == 'All' ? false : true;
    //    $("#grid")[0].p.postData = sta == 'All' ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify({ groupOp: "AND", rules: [{ field: 'Resident', op: 'eq', data: res }, { field: 'Carestaff', op: 'eq', data: sta }] }) });
    //    $("#grid").trigger("reloadGrid");
    //});

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'><td>Activity</td><td>Department</td><td>Resident</td><td>Staff</td><td>Room</td><td>Schedule</td><td>Recurrence</td><td>Duration (Minutes)</td> </tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_ScheduleTask.xls");
        }
        else                 //other browser not tested on IE 11
            //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            var data_type = 'data:application/vnd.ms-excel';
        a.href = data_type + ', ' + encodeURIComponent(tab_text);
        a.download = 'ALC_ScheduleTask' + '.xls';
        a.click();

        //return (sa);

    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();

    });
    //====================

});

