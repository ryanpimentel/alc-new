﻿$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }


    $("#facilityContent").layout({
        center: {
            paneSelector: "#facilityGrid",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#wrapToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
            , north__childOptions: {
                paneSelector: "#gridToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            center__childOptions: {
                paneSelector: "#raToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            }
        },
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#facilityGrid").height() - 25).width($("#facilityGrid").width());
            $('#grid').setGridWidth($("#facilityGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });
    $('#ftrText').change(function () {
        var res = $('#ftrText').val();
        var rm = $('#ddlFilter').val();
        var rd = $('#ddlFilter :selected').text();
        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'Name';
        if (rm == 2)
            col = "Description";

        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });


    $("#txtRate").forceNumericOnly();
    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Facility",
        dialogClass: "companiesDialog",
        open: function () {
        }
    });

    var doAjax = function (mode, row, cb) {
        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('.form').extractJson();
        }

        var data = { func: "facility", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: $(window).width() - 750,
            height: 340,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({ message: "Facility " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                            $("#diag").dialog("close");
                            if (mode == 1) isReloaded = true;
                            else {
                                fromSort = true;
                                new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                            }
                            $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('.form').clearFields();
            showdiag(1);
        } else if (q.is('.del')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Name + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = row.Name + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.unsign')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
                    setTimeout(function () {
                        if (row) {
                            doAjax(4, { Id: row.Id }, function (m) {
                                if (m.result == 0)
                                    $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to deactivate.")
            }
        }
    });


    $(function () {
        //var dataArray = [
        //    { Id: '1', Name: 'Suite 101', Description: 'Meeting facility', Level: 'First', Rate: '999.20' },
        //    { Id: '2', Name: 'Fancy 201', Description: 'Lounge', Level: 'Second', Rate: '534.20' },
        //    { Id: '3', Name: 'Lancy 900', Description: '', Level: 'First', Rate: '923.20' },
        //    { Id: '4', Name: 'Hancy 123', Description: '', Level: 'Third', Rate: '912.20' }

        //];

        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=facility&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Facility', 'Description', 'Level', 'Rate'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Name', index: 'Name', formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('.form').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              $('.form').mapJson(row);
                              showdiag(2);
                          }
                      }
                  }
              },
              { key: false, name: 'Description', index: 'Description' },
              { key: false, name: 'Level', index: 'Level' },
              { key: false, name: 'Rate', index: 'Rate', formatter: 'currency', formatoptions: { decimalSeparator: ".", prefix: "$", defaultValue: '0.00' } }],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;
            }
        });
        $("#grid").jqGrid('bindKeys');
    });

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'><td>Facility</td><td>Description</td><td>Level</td><td>Rate</td> </tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Facilities.xls");
        }
        else                 //other browser not tested on IE 11
            //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));            
            var data_type = 'data:application/vnd.ms-excel';
        a.href = data_type + ', ' + encodeURIComponent(tab_text);
        a.download = 'ALC_Facilities' + '.xls';
        a.click();

        //return (sa);
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();

    });
    //========================
});