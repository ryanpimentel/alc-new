﻿var fLastRow;
$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }
    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Resident Daily Administration of Medication",
        dialogClass: "companiesDialog",
        open: function () {

        }
    });

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: $(window).width() - 100,
            height: $(window).height() - 100,
            position: "center",
            buttons: {
                "Close": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $(function () {
        //var dataArray = [
        //    { Id: '1', Description: 'Supply 101', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFFO09', Status: '1', Cost: '655.20', Type: '1' },
        //    { Id: '2', Description: 'Supply 111', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFDO03', Status: '0', Cost: '231.20', Type: '2' },
        //    { Id: '3', Description: 'Supply 201', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFFO02', Status: '1', Cost: '112.20', Type: '1' },
        //    { Id: '4', Description: 'Supply 301', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EF4G01', Status: '0', Cost: '432.20', Type: '2' },
        //    { Id: '5', Description: 'Supply 401', Packaging: 'MEDICAL/SURGICAL SUPPLIES', UOM: 'ml', Code: 'EFRS55', Status: '1', Cost: '211.20', Type: '1' },

        //];
        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=resident&param=admission_only",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Name', 'MiddleInitial', 'Lastname', 'Gender', 'SSN', 'Birthdate'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Firstname', index: 'Firstname', formatter: "dynamicLink", formatoptions: {
                      cellValue: function (val, rowid, row, options) {
                          var name = [];
                          if (row.Firstname != "")
                              name.push(row.Firstname + " ");
                          if (row.MiddleInitial != "")
                              name.push(row.MiddleInitial + " ");
                          if (row.Lastname != "")
                              name.push(row.Lastname + " ");
                          return name.join('');
                      },
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('.form').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              var data = { id: 'dailymed_diag' };
                              $.ajax({
                                  type: "POST",
                                  contentType: "application/json; charset=utf-8",
                                  url: "Admin/GetView",
                                  data: JSON.stringify(data),
                                  dataType: "html",
                                  success: function (m) {
                                      $('#residentForm').html(m);
                                      showdiag();
                                  }
                              });
                          }
                      }
                  }
              },
              { key: false, hidden: true, name: 'MiddleInitial', index: 'MiddleInitial' },
              { key: false, hidden: true, name: 'Lastname', index: 'Lastname' },
                { key: false, name: 'Gender', index: 'Gender', edittype: "select", formatter: 'select', editoptions: { value: { 'M': 'Male', 'F': 'Female' } } },
              { key: false, name: 'SSN', index: 'SSN' },
              { key: false, name: 'Birthdate', index: 'Birthdate' }],
            //pager: jQuery('#pager'),
            scroll: true,
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true
        });
    });

});