﻿$(document).ready(function () {
    if (typeof (PHL) !== 'undefined') {
        if (PHL.destroy) PHL.destroy();
    }

    PHL = $("#roomContent");
    PHL.layout({
        center: {
            paneSelector: "#headerRoom",
            size: '50%',
            minSize: '50%',
            maxSize: '50%',
            north: {
                paneSelector: "#wrapToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            , north__childOptions: {
                paneSelector: "#roomGridToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
                center__childOptions: {
                    paneSelector: "#raToolbar",
                    closable: false,
                    slidable: false,
                    resizable: false,
                    spacing_open: 0
                }
            },
        },
        south: {
            paneSelector: "#middleRoom",
            size: '50%',
            minSize: '20%',
            maxSize: '50%'
        },
        south__autoResize: true,
        center__autoResize: true,
        resizerDragOpacity: .6,
        livePanelResizing: false,
        closable: false,
        slidable: false,
        onresize: function () {
            resizeGrids();
        }

    });


    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#headerRoom").height() - 75).width($("#headerRoom").width());
            $('.ui-jqgrid-bdiv').eq(1).height($("#middleRoom").height() - 25).width($('#middleRoom').width());
            $('#grid').setGridWidth($("#roomGrid").width(), true);
            $('#urGrid').setGridWidth($("#residentGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });
    $('#ftrText').change(function () {
        var res = $('#ftrText').val();
        var rm = $('#ddlFilter').val();
        var rd = $('#ddlFilter :selected').text();
        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'Name';
        if (rm == 2)
            col = "Level";

        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });

    $("#txtRate").forceNumericOnly();
    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Room",
        dialogClass: "companiesDialog",
        open: function () {
            if (!$("#roomForm").data("layoutContainer")) {
                debugger;
                $("#roomForm").layout({
                    center: {
                        paneSelector: ".tabPanels",
                        closable: false,
                        slidable: false,
                        resizable: true,
                        spacing_open: 0
                    },
                    north: {
                        paneSelector: ".tabs",
                        closable: false,
                        slidable: false,
                        resizable: false,
                        spacing_open: 0,
                        size: 27
                    }
                });
            }

            if (!$("#roomForm").data("tabs")) {
                $("#roomForm").tabs({
                    select: function (event, ui) {
                        if (ui.index == 2) {
                            setTimeout(initHistory, 1);
                        } else if (ui.index == 1) {
                            setTimeout(initService, 1);
                        }
                    }
                });
            }
            $("#roomForm").tabs("option", "selected", 0);
            $("#roomForm").layout().resizeAll();
        }
    });

    var doAjax = function (mode, row, cb) {
        var isValid = $('#diag .form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) return;
            row = $('#diag .form').extractJson();
        }

        var data = { func: "room", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: 850,
            height: 400,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({ message: "Room " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                            $("#diag").dialog("close");
                            if (mode == 1) isReloaded = true;
                            else {
                                fromSort = true;
                                new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                            }
                            $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('#roomGridToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            $('#diag .form').clearFields();
            showdiag(1);
        }
        //else if (q.is('.del')) {
        //    var id = $('#grid').jqGrid('getGridParam', 'selrow');
        //    if (id) {
        //        if (confirm('Are you sure you want to delete the selected row?')) {
        //            setTimeout(function () {
        //                var row = $('#grid').jqGrid('getRowData', id);
        //                if (row) {
        //                    doAjax(3, { Id: row.Id }, function (m) {
        //                        var msg = row.Name + " deleted successfully!";
        //                        if (m.result == -3) //inuse
        //                            msg = row.Name + " cannot be deleted because it is currently in use.";
        //                        $.growlSuccess({ message: msg, delay: 6000 });
        //                    });
        //                }
        //            }, 100);
        //        }
        //    } else {
        //        alert("Please select row to delete.")
        //    }
        //} else if (q.is('.unsign')) {
        //    var id = $('#grid').jqGrid('getGridParam', 'selrow');
        //    if (id) {
        //        var row = $('#grid').jqGrid('getRowData', id);
        //        if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
        //            setTimeout(function () {
        //                if (row) {
        //                    doAjax(4, { Id: row.Id }, function (m) {
        //                        if (m.result == 0)
        //                            $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
        //                    });
        //                }
        //            }, 100);
        //        }
        //    } else {
        //        alert("Please select row to deactivate.")
        //    }
        //}
    });


    $(function () {
        //var dataArray = [
        //    { Id: '1', Name: 'Suite 101', RoomType: '1', Level: 'First', Rate: '999.20' },
        //    { Id: '2', Name: 'Fancy 201', RoomType: '1', Level: 'Second', Rate: '534.20' },
        //    { Id: '3', Name: 'Lancy 900', RoomType: '2', Level: 'First', Rate: '923.20' },
        //    { Id: '4', Name: 'Hancy 123', RoomType: '1', Level: 'Third', Rate: '912.20' }

        //];

        //this is in site.layout.js.
        var GURow = new Grid_Util.Row();

        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=room&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Room', 'Room Type', 'Level', 'Room Rate'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Name', index: 'Name', formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, iRow, iCol, cellText, e) {
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          $('#roomForm').clearFields();
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              $('#roomForm').mapJson(row);
                              showdiag(2);
                              var title = $("#diag").dialog("option", 'title');
                              $("#diag").dialog("option", 'title', 'Room - ' + cellText);
                          }
                      }
                  }
              },
              { key: false, name: 'RoomType', index: 'RoomType', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Suite', '2': 'Residence' } } },
              { key: false, name: 'Level', index: 'Level' },
              { key: false, name: 'Rate', index: 'Rate', formatter: 'currency', formatoptions: { decimalSeparator: ".", prefix: "$", defaultValue: '0.00' } }],
            onSelectRow: function (id) {
                onFocusRoleGrid(id);
            },
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;
            },
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true
        });
        $("#grid").jqGrid('bindKeys');
    });

    function onFocusRoleGrid(roleid) {
        if ($('#urGrid')[0] && $('#urGrid')[0].grid)
            $.jgrid.gridUnload('urGrid');


        var GURow2 = new Grid_Util.Row();

        $('#urGrid').jqGrid({
            url: "Admin/GetResidentByRoom?rid=" + roleid,
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Resident Name', 'MiddleInitial', 'Lastname', 'Room', 'RoomId', 'Gender', 'FullName', 'SSN', 'Birthdate'],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Firstname', index: 'Firstname', formatter:
                  function (cellValue, options, rowData) {
                      var name = [];
                      if (cellValue != "") name.push(cellValue);
                      if (rowData != null) {
                          if (rowData.MiddleInitial && rowData.MiddleInitial != '') {
                              name.push(" " + rowData.MiddleInitial.toUpperCase());
                          }
                          if (rowData.Lastname && rowData.Lastname != '') name.push(" " + rowData.Lastname);
                      }
                      return name.join('');
                  }
              },
              { key: false, hidden: true, name: 'MiddleInitial', index: 'MiddleInitial' },
              { key: false, hidden: true, name: 'Lastname', index: 'Lastname' },
              { key: false, hidden: true, name: 'RoomName', index: 'RoomName' },
              { key: false, hidden: true, name: 'RoomId', index: 'RoomId' },
              {
                  key: false, name: 'Gender', index: 'Gender', formatter: function (cellValue, rowId, rowData, options) {
                      if (cellValue == "M") return "Male";
                      return "Female";
                  }
              },
              { key: false, hidden: true, name: 'Name', index: 'Name', },
              { key: false, name: 'SSN', index: 'SSN', },
              { key: false, name: 'Birthdate', index: 'Birthdate', }
            ],
            onSortCol: function () {
                fromSort2 = true;
                GURow2.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort2 != 'undefined') {
                    GURow2.restoreSelection.call(this);
                    delete fromSort2;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#urGrid").setSelection(maxId);
                delete isReloaded;

                resizeGrids();
            },
            //pager: jQuery('#pager'),
            scroll: true,
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true
        });

        $("#urGrid").jqGrid('bindKeys');
    }

    $("#diagTask").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Add Service Task",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });
    $('#txtTimeIn').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#txtCarestaff').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#Onetimedate').trigger('change');
        }
    });
    $('#txtTimeIn').mask('99:99 xy');

    var showdiagTask = function (mode) {
        $("#diagTask").dialog("option", {
            width: 500,
            height: 500,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax2(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diagTask").dialog("close");
                                //initService();
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                }
                                $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');

                            } else if (m.result == -2) {
                                var resVisible = $('#txtResidentId').is(':visible');
                                $.growlWarning({ message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", delay: 6000 });
                                //there is already a schedule for a particular resident or room
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diagTask").dialog("close");
                }
            }
        }).dialog("open");
    }

    var doAjax2 = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "activity_schedule", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        var isValid = $('#diagTask .form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }
            row = $('#diagTask .form').extractJson();
            //validate time

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];
            delete row['recurring'];
            delete row['Activity'];
            delete row['Dept'];
            delete row['Onetimedate'];

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring) {
                row.isonetime = '0';
                $('.dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    if (d.is(':checked')) {
                        switch (d.attr('name')) {
                            case 'Sun': dow.push('0'); break;
                            case 'Mon': dow.push('1'); break;
                            case 'Tue': dow.push('2'); break;
                            case 'Wed': dow.push('3'); break;
                            case 'Thu': dow.push('4'); break;
                            case 'Fri': dow.push('5'); break;
                            case 'Sat': dow.push('6'); break;
                        }
                    }
                });

                if (dow.length == 0) {
                    alert('Please choose day of week schedule.');
                    return;
                }

            } else {
                row.isonetime = '1';
                var onetimeval = $('#Onetimedate').val();
                if (onetimeval == '') {
                    alert("Please choose a one time date schedule.");
                    return;
                }

                var onetimeday = moment(onetimeval).day();
                if ($.isNumeric(onetimeday))
                    dow.push(onetimeday.toString());

                row.ScheduleTime = onetimeval + ' ' + $('#txtTimeIn').val();
            }
            row.Recurrence = dow.join(',');
        }

        var data = { func: "activity_schedule", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    $('#txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#viewActivity').val(title);
        $('#txtCarestaff')[0].options.length = 0;
        toggleCarestafflist();
    });

    var bindDept = function (dptid, cb) {
        var data = { deptid: dptid };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetServicesAndCarestaffByDeptId",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //$('#txtCarestaff').empty();
                //$('<option/>').val('').html('').appendTo('#txtCarestaff');
                //$.each(m.Carestaffs, function (i, d) {
                //    $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaff');
                //});

                $('#txtActivity').empty();
                $('#viewActivity').val('');
                var opt = '<option val=""></option>';
                $.each(m.Services, function (i, d) {
                    opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
                });
                $('#txtActivity').append(opt);
                if (typeof (cb) !== 'undefined') cb();
            }
        });
    }

    var toggleCarestafflist = function () {
        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
        if (isrecurring)
            triggerDayofWeekClick();
        else
            $('#Onetimedate').trigger('change');

        //if housekeeper or maintenance hide room dropdown
        var val = $('#ddlDept').val();
        if (val == 5 || val == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    if ($('#ddlDept').length > 0) {
        $('#ddlDept').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('table.dayofweek input').removeAttr('disabled');
            $('#Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('table.dayofweek input').attr('disabled', 'disabled');
            $('table.dayofweek').clearFields();
            $('#Onetimedate').removeAttr('disabled');
        }
    });

    var triggerDayofWeekClick = function () {
        var a = $('#txtTimeIn').val();
        if (a == null || a == '') {
            $('#txtCarestaff').empty();
            //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
            return;
        }

        var dow = [];
        $('.dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun': dow.push('0'); break;
                    case 'Mon': dow.push('1'); break;
                    case 'Tue': dow.push('2'); break;
                    case 'Wed': dow.push('3'); break;
                    case 'Thu': dow.push('4'); break;
                    case 'Fri': dow.push('5'); break;
                    case 'Sat': dow.push('6'); break;
                }
            }
        });
        bindCarestaff(dow);

    }

    $('table.dayofweek input').click(triggerDayofWeekClick);

    var bindCarestaff = function (dow) {
        if ($('#txtTimeIn').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if ($('#txtActivity').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if (dow == null) dow = [];
        var csid = $('input[xid="hdCarestaffId"]').val();
        if (csid == "") csid = null;

        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');

        var data = { scheduletime: $('#txtTimeIn').val(), activityid: $('#txtActivity').val(), carestaffid: csid, recurrence: dow.join(',') };
        data.isonetime = isrecurring ? "0" : "1";
        data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#Onetimedate').val()).format("MM/DD/YYYY");
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#txtCarestaff').empty();
                $('<option/>').val('').html('').appendTo('#txtCarestaff');
                $.each(m, function (i, d) {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                });
            }
        });
    }


    $('#Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff(dow);
    });

    $('#AddServiceToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            AddService();
        } else if (q.is('.del')) {
            var id = $('#sGrid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#sGrid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax2(3, { Id: row.Id }, function (m) {
                                var msg = "Task deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = "Task cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3) {
                                    fromSort = true;
                                    new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                    $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                }
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.refresh')) {
            //fromSort = true;
            //new Grid_Util.Row().saveSelection.call( $( '#sGrid' )[0] );
            $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
        }
    });

    var AddService = function () {
        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#roomForm #txtId').val();
            if (!id) return;
        }

        $("#ddlDept option[value='4']").remove();
        $("#ddlDept option[value='6']").remove();
        var dept = $('#ddlDept').val();
        $('#ddlDept').trigger('change')
        $('#diagTask').clearFields();
        $('#liRoom select').val(id);
        $('input[xid="hdCarestaffId"]').val('');
        $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
        $('input[name="recurring"]:eq(1)').removeAttr('checked');
        $('#Onetimedate').attr('disabled', 'disabled');
        $('.dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
        showdiagTask(1);
        //if housekeeper or maintenance hide room dropdown
        $('#ddlDept').val(dept);
        if (dept == 5 || dept == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    $('#historyServiceToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.resetSend')) {
            //to do
        } else if (q.is('.print')) {
            //do print
        } else if (q.is('.refresh')) {
            initHistory();
        }
    });

    var initService = function () {
        if ($('#sGrid')[0] && $('#sGrid')[0].grid)
            $.jgrid.gridUnload('sGrid');
        var SGrid = $('#sGrid');

        var GURow = new Grid_Util.Row();
        var pageWidth = $("#sGrid").parent().width() - 100;

        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#diagTask #txtId').val();
            if (!id) return;
        }

        SGrid.jqGrid({
            url: "Admin/GetGridData?func=activity_schedule&param=-1&param4=" + id,// + $('#ftrRecurrence').val(),
            datatype: 'json',
            postData: '',
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['Id', 'Activity', 'Department', 'Resident', 'Staff', 'Room', 'Schedule', 'Recurrence', 'Duration (Minutes)'/*,'Date',  'Completed', 'Remarks'*/],
            colModel: [
              { key: true, hidden: true, name: 'Id', index: 'Id' },
              {
                  key: false, name: 'Activity', index: 'Activity', sortable: true, width: (pageWidth * (8 / 100)), formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, irow, icol, celltext, e) {
                          var G = $('#sGrid');
                          G.setSelection(rowid, false);
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              doAjax2(0, row, function (res) {
                                  $("#diagTask .form").clearFields();
                                  bindDept(res.Dept.toString(), function () {
                                      if (!res.IsOneTime)
                                          res.Onetimedate = '';
                                      $("#diagTask .form").mapJson(res);
                                  });
                                  $("#ddlDept option[value='5']").remove();
                                  $("#ddlDept option[value='7']").remove();
                                  //if housekeeper or maintenance hide room dropdown
                                  if (res.Dept == 5 || res.Dept == 7) {
                                      $('#liRoom, #liRoom select').show();
                                      $('#liResident, #liResident select').hide();
                                  } else {
                                      $('#liResident, #liResident select').show();
                                      $('#liRoom, #liRoom select').hide();
                                  }
                                  //map day of week
                                  if (!res.IsOneTime) {
                                      $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
                                      if (res.Recurrence != null && res.Recurrence != '') {
                                          var setCheck = function (el, str, idx) {
                                              if (str.indexOf(idx) != -1)
                                                  el.prop('checked', true);
                                          }
                                          var dow = res.Recurrence;
                                          $('.dayofweek').find('input[type="checkbox"]').each(function () {
                                              var d = $(this);
                                              switch (d.attr('name')) {
                                                  case 'Sun': setCheck(d, dow, '0'); break;
                                                  case 'Mon': setCheck(d, dow, '1'); break;
                                                  case 'Tue': setCheck(d, dow, '2'); break;
                                                  case 'Wed': setCheck(d, dow, '3'); break;
                                                  case 'Thu': setCheck(d, dow, '4'); break;
                                                  case 'Fri': setCheck(d, dow, '5'); break;
                                                  case 'Sat': setCheck(d, dow, '6'); break;
                                              }
                                          });
                                          //debugger;
                                          $('input[name="recurring"]:eq(0)').trigger('click');
                                          $('#Onetimedate').val('');
                                      }
                                  } else {
                                      $('input[name="recurring"]:eq(1)').trigger('click');
                                      res.Recurrence = moment(res.Onetimedate).day().toString();
                                  }
                                  $('input[xid="hdCarestaffId"]').val(res.CarestaffId);
                                  var data = { scheduletime: res.ScheduleTime, activityid: res.ActivityId, carestaffid: res.CarestaffId, recurrence: res.Recurrence };
                                  data.isonetime = !res.IsOneTime ? "0" : "1";
                                  data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
                                  $.ajax({
                                      type: "POST",
                                      contentType: "application/json; charset=utf-8",
                                      url: "Admin/GetCarestaffByTime",
                                      data: JSON.stringify(data),
                                      dataType: "json",
                                      success: function (m) {
                                          $('#txtCarestaff').empty();
                                          $('<option/>').val('').html('').appendTo('#txtCarestaff');
                                          $.each(m, function (i, d) {
                                              $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                                          });
                                          $('#txtCarestaff').val(res.CarestaffId);
                                          showdiagTask(2);
                                      }
                                  });
                              });
                          }
                      }
                  }
              },
              { key: false, name: 'Department', index: 'Department', sortable: true, width: (pageWidth * (8 / 100)) },
              { key: false, hidden: true, name: 'Resident', index: 'Resident', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'Carestaff', index: 'Carestaff', sortable: true, width: (pageWidth * (6 / 100)) },
              { key: false, name: 'Room', index: 'Room', sortable: true, width: (pageWidth * (4 / 100)) },
              //{ key: false, name: 'ScheduleDate', index: 'ScheduleDate' },
              { key: false, name: 'Schedule', index: 'Schedule', sortable: true, width: (pageWidth * (8 / 100)) },
               { key: false, name: 'Recurrence', index: 'Recurrence', sortable: true, width: (pageWidth * (8 / 100)) },
              { key: false, name: 'Duration', index: 'Duration', sortable: true, width: (pageWidth * (5 / 100)) },
              //{ key: false, name: 'Completed', index: 'Completed', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Yes', '0': 'No' } } },
              //{ key: false, name: 'Remarks', index: 'Remarks' }
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                //if ($('#ftrResident').find('option').size() > 0) return;
                //var optsRes = ['<option>All</option>'], optsCS = ['<option>All</option>'], optsDP = ['<option>All</option>'];
                //$.each(unique($.map(data, function (o) { return o.Resident })), function () {
                //    optsRes.push('<option>' + this + '</option>');
                //});
                //$.each(unique($.map(data, function (o) { return o.Carestaff })), function () {
                //    optsCS.push('<option>' + this + '</option>');
                //});
                //$.each(unique($.map(data, function (o) { return o.Department })), function () {
                //    optsDP.push('<option>' + this + '</option>');
                //});
                //$('#ftrResident').html(optsRes.join(''));
                //$('#ftrCarestaff').html(optsCS.join(''));
                //$('#ftrDept').html(optsDP.join(''));

                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#sGrid").setSelection(maxId);
                delete isReloaded;
            }
        });
        SGrid.jqGrid('bindKeys');
        $('#roomForm div.tabPanels:eq(0)').layout({
            center: {
                paneSelector: "#serviceGrid",
                closable: false,
                slidable: false,
                resizable: true,
                spacing_open: 0
            },
            north: {
                paneSelector: "#AddServiceToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            onresize: function () {
                resizeGridsS();
                //resizeGridHS();
            }
        });
        resizeGridsS();
    }

    var resizeGridsS = (function () {
        //debugger;
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#AddServiceToolbar').height();
            $("#sGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#sGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
            //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
            //console.log($("#sGrid").height())
        }
        setTimeout(f, 100);
        return f;
    });


    $("#fromDate,#toDate").datepicker({
        formatDate: "MM/dd/yyyy",
        onClose: function () {
            var dis = $(this);
            if (dis.val() == "")
                dis.val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));

            if (moment($("#toDate").val()) < moment($("#fromDate").val())) {
                alert('"To Date" should be greather or equal to "From Date"');
                $("#toDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));
                return;
            }

            initHistory();
        }
    });
    $("#fromDate,#toDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));

    var initHistory = function () {
        var removeByAttr = function (arr, attr, value) {
            var i = arr.length;
            while (i--) {
                if (arr[i]
                    && arr[i].hasOwnProperty(attr)
                    && (arguments.length > 2 && arr[i][attr] === value)) {

                    arr.splice(i, 1);

                }
            }
            return arr;
        }

        function beforeProcessing(data, status, xhr) {
            var delList = [];
            if (data && data.length > 0) {
                //search for items with xref and check if it has correspoding acivity_schedule_id in the list
                //if exists then remove item with xref and preserve the record with activity_schedule_id = xref
                var occ = $.grep(data, function (a) { return a.xref > 0 });
                $.each(occ, function (x, y) {
                    var matchObj = $.grep(data, function (b) {
                        return b.activity_schedule_id == y.xref;
                    });
                    if (matchObj.length > 0) {
                        delList.push(y);
                    }
                });

                $.each(delList, function (i, a) {
                    removeByAttr(data, 'activity_schedule_id', a.activity_schedule_id);
                });
            }
        }

        if ($('#hGrid')[0] && $('#hGrid')[0].grid)
            $.jgrid.gridUnload('hGrid');
        var HGrid = $('#hGrid');

        var GURow = new Grid_Util.Row();
        var pageWidth = $("#hGrid").parent().width() - 100;

        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#diagTask #txtId').val();
            if (!id) return;
        }

        HGrid.jqGrid({
            url: "Staff/GetGridData?func=activity_schedule&param=" + $('#fromDate').val() + "&param2=" + $('#toDate').val() + "&param4=" + id,
            beforeProcessing: function (data, status, xhr) { beforeProcessing(data, status, xhr) },
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['ActivityId', 'ActivityScheduleId', 'CarestaffActivityId', 'Task', 'Procedure', 'Department', 'Resident', 'Room', 'Staff', 'Schedule', 'Duration (Minutes)', 'Completed', 'oStat', 'Acknowledge Date', 'Acknowledge By', 'Remarks', 'Reschedule', 'XRef', 'TimeLapsed'],
            colModel: [
              { key: false, hidden: true, name: 'activity_id', index: 'activity_id' },
              { key: true, hidden: true, name: 'activity_schedule_id', index: 'activity_schedule_id' },
              { key: false, hidden: true, name: 'carestaff_activity_id', index: 'carestaff_activity_id' },
              {
                  key: false, name: 'activity_desc', index: 'activity_desc', width: 200
                  //, formatter: "dynamicLink", formatoptions: {
                  //onClick: function (rowid, irow, icol, celltext, e) {
                  //    var G = $('#grid');
                  //    G.setSelection(rowid, false);
                  //    var row = G.jqGrid('getRowData', rowid);
                  //    if (row) {
                  //        $('#diag').clearFields();
                  //        $('#diag #CarestaffActivityId').val(row.carestaff_activity_id);
                  //        $('#diag #ActivityScheduleId').val(row.activity_schedule_id);
                  //        //$('#diag #AcknowledgedBy').val(row.acknowledged_by);
                  //        //$('#diag #Id').val(row.carestaff_activity_id);
                  //        //$('#diag #Remarks').val(row.remarks);
                  //        if (row.reschedule_dt && row.reschedule_dt != '')
                  //            $('#diag #Remarks').val("Rescheduled - " + row.reschedule_dt);
                  //        else
                  //            $('#diag #Remarks').val(row.remarks);

                  //        if (row.oactivity_status == false)
                  //            $('#diag #No').attr('checked', 'checked');
                  //        if (row.oactivity_status == true)
                  //            $('#diag #Yes').attr('checked', 'checked');

                  //        $('#diag #staffInfo').html('Is the selected task completed?');
                  //        showdiag();
                  //        if ((moment($('#curDate').val()).format("MM/DD/YYYY") != moment(ALC_Util.CurrentDate).format("MM/DD/YYYY")) || (row.reschedule_dt && row.reschedule_dt != '')) {
                  //            $('#diag').find('input, textarea, button, select').attr('disabled', 'disabled');
                  //            $('.ui-dialog-buttonset').hide();
                  //        } else {
                  //            $('#diag').find('input, textarea, button, select').removeAttr('disabled');
                  //            $('.ui-dialog-buttonset').show();
                  //        }

                  //    }
                  //}
                  //}
              },
              { key: false, name: 'activity_proc', width: 200, index: 'activity_proc' },
              { key: false, name: 'department', width: 100, index: 'department' },
              { key: false, name: 'resident', index: 'resident', width: (pageWidth * (12 / 100)) },
              { key: false, name: 'room', index: 'room', width: (pageWidth * (8 / 100)) },
              { key: false, name: 'carestaff_name', index: 'carestaff_name', width: (pageWidth * (12 / 100)) },
              { key: false, name: 'start_time', index: 'start_time', width: (pageWidth * (8 / 100)) },
              { key: false, name: 'service_duration', index: 'service_duration', width: (pageWidth * (12 / 100)) },
              {
                  key: false, name: 'activity_status', index: 'activity_status', width: (pageWidth * (8 / 100)), edittype: "select",
                  formatter: function (cellvalue, options, row) {
                      //do custom formatting here
                      if (row.carestaff_activity_id == null && row.timelapsed) {
                          return '<span style="background:#d61b1b;color:white;font-size: 9px;letter-spacing: 1px;padding: 2px 4px;border-radius: 5px 5px;">Missed</span>';
                      }
                      var selected = cellvalue == 1 ? ' checked="checked"' : '';

                      var htm = "<input disabled type='checkbox'" + selected + "></input>";
                      return htm;
                  }
              },
              { key: false, hidden: true, name: 'oactivity_status', index: 'oactivity_status' },
              {
                  key: false, name: 'completion_date', index: 'completion_date', formatter: function (cellvalue, options, row) {
                      if (row.completion_date)
                          return moment(row.completion_date).format('MM/DD/YYYY');
                      return '';
                  }
              },
              { key: false, name: 'acknowledged_by', index: 'acknowledged_by' },
              {
                  key: false, name: 'remarks', index: 'remarks', formatter: function (cellvalue, options, row) {
                      if (row.reschedule_dt && row.reschedule_dt != '') {
                          var htm = "<a xref_id='" + row.xref + "'>Rescheduled - " + row.reschedule_dt + "</a>";
                          return htm;
                      } else {
                          return cellvalue;
                      }

                  }
              },
              { key: false, hidden: true, name: 'reschedule_dt', index: 'reschedule_dt' },
              { key: false, hidden: true, name: 'xref', index: 'xref' },
              { key: false, hidden: true, name: 'timelapsed', index: 'timelapsed' }
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            shrinkToFit: false,
            forceFit: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#hGrid").setSelection(maxId);
                delete isReloaded;
            }
        });
        HGrid.jqGrid('bindKeys');
        $('#tabs-2').layout({
            center: {
                paneSelector: "#historyGrid",
                closable: false,
                slidable: false,
                resizable: true,
                spacing_open: 0
            },
            north: {
                paneSelector: "#historyServiceToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            onresize: function () {
                resizeGridHS();
                resizeGridsS();
            }
        });
        resizeGridHS();
    }

    var resizeGridHS = (function () {
        //debugger;
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#historyServiceToolbar').height();
            $("#hGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#hGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
            //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
            //console.log($("#sGrid").height())
        }
        setTimeout(f, 100);
        return f;
    });


    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        var myGrid = $('#grid'),
        selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
        iValue = myGrid.jqGrid('getInd', selRowId);

        $("td:hidden,th:hidden", "#urGrid").remove();
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Room</td><td bgcolor='#87AFC6'>Room Type</td><td bgcolor='#87AFC6'> Level </td><td bgcolor='#87AFC6'>Room Rate</td></tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('urGrid'); // id of table
        tab1 = document.getElementById('grid');
        tab_text = tab_text + tab1.rows[iValue].innerHTML + "</tr><tr></tr><tr><td colspan='4'>Room Residents: </td></tr><tr><td bgcolor='#87AFC6'>Resident Name</td><td bgcolor='#87AFC6'>Gender</td><td bgcolor='#87AFC6'>SSN</td><td bgcolor='#87AFC6'>Birthdate</td></tr><tr>";

        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";

            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_RoomResidents.xls");
        }
        else                 //other browser not tested on IE 11
            //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));           
            var data_type = 'data:application/vnd.ms-excel';
        a.href = data_type + ', ' + encodeURIComponent(tab_text);
        a.download = 'ALC_RoomResidents' + '.xls';
        a.click();

        //return (sa);
    }

    //$("#lnkExportExcel").click(function (e) {
    //    //getting values of current time for generating the file name
    //    var dt = new Date();
    //    var day = dt.getDate();
    //    var month = dt.getMonth() + 1;
    //    var year = dt.getFullYear();
    //    var hour = dt.getHours();
    //    var mins = dt.getMinutes();
    //    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    //    //creating a temporary HTML link element (they support setting file names)
    //    var a = document.createElement('a');
    //    //getting data from our div that contains the HTML table
    //    var data_type = 'data:application/vnd.ms-excel';
    //    var table_div = document.getElementById('grid');
    //    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    //    a.href = data_type + ', ' + table_html;
    //    //setting the file name
    //    a.download = 'exported_table_' + postfix + '.xls';
    //    //triggering the function
    //    a.click();
    //    //just in case, prevent default behaviour
    //    e.preventDefault();
    //});

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();

    });
    //============================
});