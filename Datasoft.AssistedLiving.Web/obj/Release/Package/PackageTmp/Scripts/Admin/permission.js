﻿$(function () {
    $(".loader").show();
    var perm1 = $("#perm1").DataTable();
    var perm2 = $("#perm2").DataTable();


    var doAjax = function (rows, func, cb) {
        var data = { func: func, data: rows };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
              
                if (cb) cb(m);
            }
        });
    }


    var init = function () {
        var roleid = $('#roleSelect').val();
        var text = $("#roleSelect option:selected").text();

        $("#role1").html(text);
        $("#description2").html("");
        $("#perm2_tbody").html("");
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=permission&param=" + roleid,
            dataType: "json",
            async: false,
            success: function (m) {
            
                var perm1_html = "";
                var sel = ' checked="checked"';
                if (m.length != 0) {

                    $.each(m, function (i, v) {
                        perm1_html += "<tr id='" + v.tree_node_id + "' class='tr_perm1' rid='" + v. role_id+ "'>";
                        perm1_html += "<td>" + v.tree_node_description + "</td>";
                        perm1_html += "<td>";
                        perm1_html += '<div class="m-radio-inline">';
                        //perm1_html += '<label class="m-radio">';
                        //perm1_html += '<input type="radio" name="radio_' + v.tree_node_id + '" value="1" ' + (v.access_type_id == 1 ? sel : "" ) + ' disabled/> Read Only';
                        //perm1_html += '<span></span>';
                        //perm1_html += '</label>';
                        perm1_html += '<label class="m-radio">';
                        perm1_html += '<input type="radio" name="radio_' + v.tree_node_id + '" value="2" ' + (v.access_type_id == 2 ? sel : "") + ' disabled/> No Access';
                        perm1_html += '<span></span>';
                        perm1_html += '</label>';
                        perm1_html += '<label class="m-radio">';
                        perm1_html += '<input type="radio" name="radio_' + v.tree_node_id + '" value="3"  ' + (v.access_type_id == 3 ? sel : "") + ' disabled/> Has Access';
                        perm1_html += '<span></span>';
                        perm1_html += '</label>';
                        perm1_html += '</div>';
                        perm1_html += '</td>';
                        perm1_html += '</tr>';

                    
                    })
                }

                $("#perm1").DataTable().destroy();

                $("#perm1_tbody").html(perm1_html);

                $("#perm1").DataTable({
                    responsive: !0,
                    pagingType: "full_numbers",
                    dom: 'lBfrtip',
                    select: true,
                    buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="la la-download"></i> Export to Excel',
                        title: 'ALC_Permission',
                        className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },{
                        text: '<i class="la la-edit"></i> Edit Permissions',
                        className: 'edit_perm btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        action: function ( e, dt, node, config ) {
                            $("#perm1_tbody").find("input").removeAttr("disabled");
                            $("#perm1_wrapper .savepermission").removeAttr("disabled");

                             $(document).on("click","#perm1 input[type='radio']", function () {
                                 $("#perm1_wrapper .savepermission").removeAttr("disabled");
                            });
                        }
                    },{
                        text: '<i class="la la-save"></i> Save Permissions',
                        className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air savepermission',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button');
                            $(".edit_perm").attr("disabled", "disabled");
                        },
                        action: function () {
                            $(".edit_perm").removeAttr("disabled");
                            //this.remove();

                            var changes = [];

                            $("#perm1_tbody").find("tr").each(function (i, e) {

                                var id = $(this).attr("id");
                                var rid = $(this).attr("rid");
                                changes.push({ id: id, rid: rid, val: $(this).find('input:checked').val() });

                            });

                            doAjax(changes, "permission", function (m) {
                                if (m.result == 0) {
                                    toastr.success("Permissions saved successfully!");
                                    init();
                                    $("#perm1_wrapper .savepermission").attr("disabled", "disabled");
                                }
                            });
                        }
                    }
                    ]
                });
                $("#perm1_wrapper .savepermission").attr("disabled", "disabled");
                setTimeout(function () { $(".loader").hide(); }, 500);
            }
        })
    }

    init();

   

    $(document).on("click", ".tr_perm1", function () {

       var id = $(this).attr("id");
       var role_id = $(this).attr("rid");

       var text = $(this).find('td:first-child').text();
       $("#description2").html(text);
   
       $.ajax({
           type: "POST",
           contentType: "application/json; charset=utf-8",
           url: "Admin/GetGridData?func=permission_detail&param=" + id + "&param2=" + role_id,
           dataType: "json",
           async: false,
           success: function (m) {

               var perm1_html = "";
               var sel = ' checked="checked"';
               if (m.length != 0) {

                   $.each(m, function (i, v) {
                       perm1_html += "<tr id='" + v.tree_sub_node_id + "' rid='" + v.role_id + "'>";
                       perm1_html += "<td>" + v.tree_sub_node_description + "</td>";
                       perm1_html += "<td>";
                       perm1_html += '<div class="m-radio-inline">';
                       //perm1_html += '<label class="m-radio">';
                       //perm1_html += '<input type="radio" name="radio_' + v.tree_sub_node_id + '" value="1" ' + (v.access_type_id == 1 ? sel : "") + ' disabled/> Read Only';
                       //perm1_html += '<span></span>';
                       //perm1_html += '</label>';
                       perm1_html += '<label class="m-radio">';
                       perm1_html += '<input type="radio" name="radio_' + v.tree_sub_node_id + '" value="2" ' + (v.access_type_id == 2 ? sel : "") + ' disabled/> No Access';
                       perm1_html += '<span></span>';
                       perm1_html += '</label>';
                       perm1_html += '<label class="m-radio">';
                       perm1_html += '<input type="radio" name="radio_' + v.tree_sub_node_id + '" value="3"  ' + (v.access_type_id == 3 ? sel : "") + ' disabled/> Has Access';
                       perm1_html += '<span></span>';
                       perm1_html += '</label>';
                       perm1_html += '</div>';
                       perm1_html += '</td>';
                       perm1_html += '</tr>';

                   })
               }

               $("#perm2").DataTable().destroy();

               $("#perm2_tbody").html(perm1_html);

               $("#perm2").DataTable({
                   responsive: !0,
                   pagingType: "full_numbers",
                   dom: 'lBfrtip',
                   select: true,
                   buttons: [
                   {
                       extend: 'excelHtml5',
                       text: '<i class="la la-download"></i> Export to Excel',
                       title: 'ALC_Permission',
                       className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                       init: function (api, node, config) {
                           $(node).removeClass('dt-button')
                       }
                   }, {
                       text: '<i class="la la-edit"></i> Edit Permissions',
                       className: 'edit_perm btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                       init: function (api, node, config) {
                           $(node).removeClass('dt-button')
                       },
                       action: function (e, dt, node, config) {
                           //dt.button().add(1, {
                              
                         //  });

                           $("#perm2_tbody").find("input").removeAttr("disabled");
                           $("#perm2_wrapper .savepermission").removeAttr("disabled");

                       }
                   },{
                       text: '<i class="la la-save"></i> Save Permissions',
                       className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air savepermission',
                       init: function (api, node, config) {
                           $(node).removeClass('dt-button');
                           $(".edit_perm").attr("disabled", "disabled");
                       },
                       action: function () {
                           $(".edit_perm").removeAttr("disabled");
                           //this.remove();

                           var changes = [];

                           $("#perm2_tbody").find("tr").each(function (i, e) {

                               var id = $(this).attr("id");
                               var rid = $(this).attr("rid");
                               changes.push({ id: id, rid: rid, val: $(this).find('input:checked').val() });

                           });

                           doAjax(changes, "permission_detail", function (m) {
                               if (m.result == 0) {
                                   swal("Permissions saved successfully!", "", "success");
                                   init();
                                   $("#perm2_wrapper .savepermission").attr("disabled", "disabled");
                               }
                           });
                          
                       }
                   }]
               });
               $("#perm2_wrapper .savepermission").attr("disabled", "disabled");
           }
       })

    });

    $("#roleSelect").on("change", function () {

        var text = $("#roleSelect option:selected").text();
        $("#role1").html(text);

        init();

    })

    $(document).on("click", ".edit_p", function () {
        var a = $(this).closest('td').prev('td');

    })

})