﻿var datatable = $("#rm_prescription_tbl").DataTable();
var meddatatable = $("#rm_meddetail_tbl").DataTable();


var scroll1 = new PerfectScrollbar('#resident_list');
//$(function () {


//});

function setHeight() {
    
    if ($(window).width() > 1024) {
        $('#rm_container2, #rm_container3').height($(window).height() - 360);
        $('.dataTables_scrollBody').height($('#rm_container2').height() - 220);
      //  $('.dataTables_scrollBody').height($(window).height() - 600);
    } else {
       // $('.dataTables_scrollBody').height($("#rm_container2").height());
    }


    $(window).resize(function () {
        if ($(window).width() > 1024) {
            $('#rm_container2, #rm_container3').height($(window).height() - 360);
            $('.dataTables_scrollBody').height($('#rm_container2').height() - 220);
            //$('.dataTables_scrollBody').height($("#rm_container2").height() - 600);

        } else {
            //$('#rm_container, #rm_container2, #rm_container3').height($(window).height());
            $('.dataTables_scrollBody').height($('#rm_container2').height() - 250);
            $('.dataTables_scrollBody').css("overflow", "auto");
        }
       
    })
    // var scroll = new PerfectScrollbar('#rm_container3');
}
  var loadPrescription = function (_admissionId) {
        
        $.ajax({
            url: "Admin/GetGridData?func=receivemedsprescription&param=" + _admissionId,
            datatype: 'json',
            type: "GET",
            async: false,
            success: function (m) {
                var html_table = "";

                if (m.length != 0) {
                    
                    $.each(m, function (i, v) {
                        html_table += '<tr id="' + v.Id + '">';
                        html_table += '<td>' + v.PrescriptionNumber + '</td>';
                        html_table += '<td>' + v.PrescriptionDate + '</td>';
                        html_table += '<td>' + v.Pharmacy + '</td>';
                        html_table += '<td>' + v.AttendingPhysician + '</td>';
                        html_table += '</tr>';
                    })

                }

                $("#rm_prescription_tbl").DataTable().destroy();

                $("#prescription_tbl").html(html_table);
                $("#meddetail_tbl").html("");


                datatable = $("#rm_prescription_tbl").DataTable({
                    responsive: !0,
                    "scrollY": "true",
                    "scrollCollapse": true,
                    "scrollX": true,
                    pagingType: "full_numbers",
                    dom: 'lBfrtip',
                    select: true,
                    buttons: [
                        {
                            text: '<i class="la la-plus"></i>&nbsp;&nbsp;&nbsp;Add Prescription',
                            className: 'add btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                            action: function (e, dt, node, config) {
                                $("#add_prescription_modal").modal("toggle");
                            },
                            init: function (api, node, config) {
                                $(node).removeClass('dt-button')
                            }
                        }
                    ]
                    //,
                    //columnDefs: [{
                    //    targets: -1,
                    //    title: "Action",
                    //    orderable: !1,
                    //    render: function (a, e, n, t) {
                    //        return '\n <span>\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill edit" title="Edit" >\n<i class="la la-edit"></i>\n</a></span>\n                        '
                    //    }
                    //}]
                });
                setHeight();                
                var ps = new PerfectScrollbar('#rm_container2 .dataTables_scrollBody');
                $('#rm_prescription_tbl').DataTable().columns.adjust();
            }
        })
    }

  var loadMedicines = function (prescriptionid) {
        
        $.ajax({
            url: "Admin/GetGridData?func=receivemeddetail&param=" + prescriptionid,
            datatype: 'json',
            type: "GET",
            async: false,
            success: function (m) {
              
                var html_table = "";

                if (m.length != 0) {
                   
                    $.each(m, function (i, v) {
                        html_table += '<tr id="' + v.Id + '">';
                        html_table += '<td>' + v.SupplyName + '</td>';
                        html_table += '<td>' + (v.Generic != null? v.Generic: "") + '</td>';
                        html_table += '<td>' + v.Qty + '</td>';
                        html_table += '<td>' + v.Quantity_Left + '</td>';
                        html_table += '<td>' + v.Freq + '</td>';
                        html_table += '<td>' + v.ReceiveDate + '</td>';
                        html_table += '<td>' + v.PRNDate + '</td>';
                        html_table += '</tr>';
                    })

                }
                $("#meddetail_tbl").html("");
                $("#rm_meddetail_tbl").DataTable().destroy();
                $("#meddetail_tbl").html(html_table);


                meddatatable = $("#rm_meddetail_tbl").DataTable({
                    responsive: !0,
                    "scrollY": "true",
                    "scrollX": true,
                    "scrollCollapse": true,
                    pagingType: "full_numbers",
                    dom: 'lBfrtip',
                    select: true,
                    buttons: [
                        {
                            text: '<i class="la la-plus"></i>&nbsp;&nbsp;&nbsp;Add Medicine to Prescription',
                            className: 'add btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air add_med',
                            action: function (e, dt, node, config) {
                                $("#add_medicine_modal").modal("toggle");
                                $('#txtPRNDate').val('').attr('disabled', 'disabled');
                            },
                            init: function (api, node, config) {
                                $(node).removeClass('dt-button')
                            }
                        }
                    ]
                });

                setHeight();
                var ps = new PerfectScrollbar('#rm_container3 .dataTables_scrollBody');
                $('#rm_meddetail_tbl').DataTable().columns.adjust();
            }   
        })
  }
    
  $(document).ready(function () {
      
    var _admissionId;
    var _residentId;
    var _prescriptionId;


    $("#txtPrescriptionDate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");
    $("#txtPRNDate").datepicker({
        minDate: 0,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });//("option", "dateFormat", "mm/dd/yy");

    //FUNCTION
    $.ajax({
        url: "Admin/GetGridData?func=resident_admission_list&param=",
        datatype: 'json',
        type: "GET",
        async: false,
        success: function (m) {           
            $("#resident_list").html("");

            $.each(m, function (i, v) {
               
                $('#resident_list').append($('<option>', {
                    value: v.ResidentId,
                    text: v.Firstname + ' ' + (v.MiddleInitial != '' ? v.MiddleInitial + '. ' : '') + v.Lastname,
                    adm: v.AdmissionId
                }));
            })

                 }
    });

    setTimeout(function () {
        first_index = $("select#resident_list option:eq(0)").val();
        $("select#resident_list")
            .val(first_index)
            .trigger('change');
        $(".loader").hide();
    }, 1000);
    //ON CLICKS

    
    $(document).on("click",".edit", function () {
        
        var data = datatable.row($(this).parents('tr')).data();
        var Id = $(this).parents('tr').attr("id");

        $("#add_prescription_modal").modal("toggle");
        $('#txtId').val(Id);
        $('#txtPrescriptionNumber').val(data[0]);
        $('#txtPrescriptionDate').val(data[1]);
        $('#txtPhysician').val(data[3]);
        $('#txtPharmacy').val(data[2]);

    })
    $(document).on("click", ".rm_residentname", function () {
        _admissionId = $(this).attr("adm");
        _residentId = $(this).attr("id");
        _prescriptionId = "";
        $("#rname").html($(this).text());
        $("#prnumber").html("");
      //  debugger
        $("#rm_meddetail_tbl").DataTable().destroy();
        $("#meddetail_tbl").html("");
        $("#rm_meddetail_tbl").DataTable();
        loadPrescription(_admissionId);

    })

    $(document).on("change", "#resident_list", function () {

        _admissionId = $('option:selected', this).attr('adm');
        _residentId = $(this).val();
        _prescriptionId = "";
        $("#rname").html($('option:selected', this).text());
        $("#prnumber").html("");
        //  debugger
        $("#rm_meddetail_tbl").DataTable().destroy();
        $("#meddetail_tbl").html("");
        $("#rm_meddetail_tbl").DataTable();
        loadPrescription(_admissionId);
    })

    $(document).on("click", "#prescription_tbl tr", function () {
        

        var id = $(this).attr("id");
        _prescriptionId = id;
       
        var c = $(this).find('td:first-child').text();
        
        $("#prnumber").html("Prescription #" + c);
        if ($("#prescription_tbl .dataTables_empty").html() == "No data available in table") {
            $("#prnumber").html("");
        } 
        else {
            loadMedicines(id);
        }
    });

    $(document).on("click", "#save_prescription", function () {
        
        var form = document.getElementById('add_prescription_form');
        for (var i = 0; i < form.elements.length; i++) {
            if (form.elements[i].value === '' && form.elements[i].hasAttribute('required')) {
                swal("Fill in all required fields.")
                return false;
            }
        }
       var data = {
            func: "receivemedsprescription", mode: ($('#txtId').val() == "") ? 1 : 2, PrescriptionNo: $('#txtPrescriptionNumber').val(), PrescriptionDate: $('#txtPrescriptionDate').val(),
            Physician: $('#txtPhysician').val(), Pharmacy: $('#txtPharmacy').val(), AdmissionId: _admissionId,
            ResidentId: _residentId, Id: $('#txtId').val()
       };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
               
                if (m.result == 0) {
                    swal("Prescription saved successfully", "", "success");
                } else {
                    if (m.message == "E183") {
                        Swal({
                            type: 'error',
                            title: 'Duplicate Prescription Number',
                            text: 'Please edit the existing prescription or create a new one.'
                        })
                        return false;
                    }
                }
                
                $("#add_prescription_form").trigger("reset");
                $(".close").trigger("click");
                loadPrescription(_admissionId);
                $("#rm_residents").find("a").each(function(i, e){
                    var l = $(this).attr("adm");
                    if (l == _admissionId) {
                        $(this).trigger("click");
                    }
                });

                $("#prescription_tbl").find("tr").each(function (i, e) {
                    var l = $(this).attr("id");
                    if (l == $('#txtId').val()) {
                        $(this).trigger("click");
                    }
                });
            }
        });


    })

    $(document).on("click", "#save_medicine", function () {
        
        var form = document.getElementById('add_medicine_form');
        for (var i = 0; i < form.elements.length; i++) {
            if (form.elements[i].value === '' && form.elements[i].hasAttribute('required')) {
                swal("Fill in all required fields.")
                return false;
            }
        }

        if (_prescriptionId == "") {
            swal({
                title: "Please select a prescription.",
                type: "warning",
            });
        }

        var stat = $('#cbIsPRN').is(':checked') ? '1' : null;
        var prndate = $('#txtPRNDate').val();
        var supname = $('#txtSupplyName').val();
        var supqty = $('#txtQty').val();
        var supcount = $('#SupplyCount').val();
        var meddosage = $('#MedDosage').val();
        //debugger
        var isTablet = $('input[name=medType]:checked').val();

        if (meddosage == '') {
            meddosage = 1;
        }

        if (stat == 1 && prndate == '') {
            swal({
                title: "Please enter PRN Date if this medication type is PRN.",
                type: "warning",
            });
           
            return;
        }
        if ($('#txtSupplyId').val() == '') {
            swal({
                title: "Please enter valid supply",
                type: "warning",
            });
            return;
        }

        if (prndate == '') prndate = null;

        //if (isTablet == "1") {
        //    isTablet = true;
        //} else if (isTablet == "0") {
        //    isTablet = false;
        //} else isTablet = null;

        if (isTablet != "0") {
            isTablet = true;
        } else isTablet = false;

        var data = {
            func: "receivemeds", mode: 1, Id:0,
            prescriptionid: _prescriptionId,
            supplyid: $('#txtSupplyId').val(),
            qty: $('#txtQty').val(),
            dosage: $('#txtDosage').val(),
            med_dosage: meddosage,
            isprn: stat,
            isTablet: isTablet,
            prndate: prndate
        };


        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (m.result == 0) {
                    swal("Medicine added successfully", "", "success");
                }

                $("#add_medicine_form").trigger("reset");
                $(".close").trigger("click");
                loadMedicines(_prescriptionId);
                $("#prescription_tbl").find("tr").each(function (i, e) {
                    var l = $(this).attr("id");
                    if (l == _prescriptionId) {
                        $(this).trigger("click");
                    }
                });
            }
        });

    })

    $('#cbIsPRN').change(function () {
        if ($('#cbIsPRN').is(':checked')) {
            $('#txtPRNDate').removeAttr('disabled');
        } else {
            $('#txtPRNDate').val('').attr('disabled', 'disabled');
        }
    });

     $("#search_rmd_value").keyup(function () {
        var string = $(this).val().toLowerCase();
       
        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
    })

    $("#showall").on("click", function () {
        $(".parent").each(function (i, e) {
            $(this).show();
        });
    })

    var SearchTextMedicine = "Search Medicine name...";
    $("#txtSupplyName").val(SearchTextMedicine).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchTextMedicine);
    }).focus(function (e) {
        if ($(this).val() == SearchTextMedicine)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "search_medicine", name:"' + request.term + '"}',
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id,
                            generic: item.generic,
                            product_type: item.product_type
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {
            $("#txtSupplyName").val(ui.item.label);
            $("#txtGeneric").val(ui.item.generic);
            $('#txtSupplyId').val(ui.item.value);
            $('#txtProductType').val(ui.item.product_type);
          
            if ($("#txtProductType").val() == 2) {
                $("#dsg_row").hide();
                $("#frqy_row").hide();
            } else {
                $("#dsg_row").show();
                $("#frqy_row").show();
            }
            return false;
        },
        change: function (event, ui) {
            var dis = $(this);
            if (dis.val() == '' || dis.val() == "Search Medicine name...") {
                $("#addResident").hide();
                return false;
            }
            $("#addResident").toggle(!ui.item);
        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li class='tt-suggestion tt-selectable'></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };


    $(".MedDosage").forceNumericOnly();
    $(".Qty").forceNumericOnly();
})