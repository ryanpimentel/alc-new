var optsDpt = [];
var optsRl = [];
var optsTyp = [];
var optsStt = [];
var uniqdpt, uniqrl, uniqtyp, uniqstt;
var age = 0;
var bday;
function _calculateAge(birthday) {
// birthday is a date
    var time = new Date(birthday);
    var ageDifMs = Date.now() - time.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}
var removeEmpData = function () {
    $("#PFCempname, #PFCdepartment, #HSRusername, #HSRuserposition, #CREmployeeName, #CREmployeeAddress,#CRCityField,#CRZipField, #CRSocialSecurityNumber,#CRDateOfBirth,#ERResidentName,#EAusername,#EAuserposition,#EAusername2,#PCFusername,#EHempName,#EHempAdress,#HBDName,#HBDAddress,#PRname,#PRaddress,#PRtelephone,#RLSlastname,#RLSfirstname,#RLSmiddleinitial,#RLSdob,#EEVlastname,#EEVfirstname,#EEVmiddleinitial,#EEVdob,#PWNnamemi,#PWNlastname,#JDAemp_name, #HSRfacilityname, #PCFfacilityname, #PCFNameFaci, #PCFNameFaci2, #PRfacilityname, #PRfacilityaddress, #PRfacilityfilenumber").attr("disabled", false);

}
$(document).ready(function (e) {
    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
        $('.popover').popover('hide');
    }
});
$(document).ready(function () {
    var isSysUser = 0;
    var addUserMode = 0;

    $(".numonly").forceNumericOnly();

    var setCheck2 = function (el, str, idx) {

        if (str.indexOf(idx) != -1)
            el.prop('checked', true);
        else el.prop('checked', false);
    }
    $('a.checkall').click(function () {
        var dis = $(this);
        var dd = dis.attr('data-toggle');
        var dow = '0,1,2,3,4,5,6';
        if (dd != 'cc') {
            dow = '';
            dis.attr('data-toggle', 'cc');
            dis.html('Check All Days');
        } else {
            dis.attr('data-toggle', 'dd');
            dis.html('Uncheck All Days');
        }
        $('.dayofweek2').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            switch (d.attr('name')) {
                case 'Sun': setCheck2(d, dow, '0'); break;
                case 'Mon': setCheck2(d, dow, '1'); break;
                case 'Tue': setCheck2(d, dow, '2'); break;
                case 'Wed': setCheck2(d, dow, '3'); break;
                case 'Thu': setCheck2(d, dow, '4'); break;
                case 'Fri': setCheck2(d, dow, '5'); break;
                case 'Sat': setCheck2(d, dow, '6'); break;
            }
        });

    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#userGrid").height() - 25).width($("#userGrid").width());
            $('#grid').setGridWidth($("#userGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });
    $('#ftrRole,#ftrDepartment,#ftrSysUser,#ftrText,#ftrStatus').change(function () {
        var res = $('#ftrText').val();
        var rm = $('#ddlFilter').val();
        var rd = $('#ddlFilter :selected').text();
        var rl = $('#ftrRole :selected').text();
        var dl = $('#ftrDepartment :selected').text();
        var ia = $('#ftrStatus :selected').val();
        var su = $('#ftrSysUser :selected').val();

        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'Id';
        if (rm == 2)
            col = "Name";
        else if (rm == 3)
            col = "Email";


        if (rl != 'All')
            ftrs.rules.push({ field: 'RoleDescription', op: 'eq', data: rl });
        if (dl != 'All')
            ftrs.rules.push({ field: 'Department', op: 'eq', data: dl });
        if (ia != '-1')
            ftrs.rules.push({ field: 'IsActive', op: 'eq', data: ia });
        if (su != '-1')
            ftrs.rules.push({ field: 'NonSysUser', op: 'eq', data: su });
        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });
    $('#txtBirthDate').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
    }).on("change", function () {
        bday = ($(this).val());
        $("#HSRuserage").val(_calculateAge(bday));

    });
    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
    $('.ndatepicker').datepicker({
        changeMonth: true,
        changeYear: true,
    });
    $('#txtPhone,#txtFax,#PRtelnumber1,#PRtelnumber2, #PRtelnumber3, #PRtelnumber4,#PRtelnumber5, #PRtelnumber6,#PRreftel1, #PRreftel2, #PRreftel3, #PRrelnumber').mask("(999)999-9999");


    $('#upload-dwu').on('click', function () {
        var id = currentId;
        // var resname = $('#admForm #DCResidentName').val();
        $('#uid').val(id);
        //$("#modal-title-ud").html("Upload document for " + resname)
        // console.log("DOCUMENT");
        uploadDocument();
    });

    $('#upload-pdoc').on('click', function () {
        var id = currentId;
        // var resname = $('#admForm #DCResidentName').val();
        $('#pd_uid').val(id);
        //$("#modal-title-ud").html("Upload document for " + resname)
        // console.log("DOCUMENT");
        uploadPersonalDocument();
    });

    $('#upload-pit').on('click', function () {
        var id = currentId;
        // var resname = $('#admForm #DCResidentName').val();
        $('#pit_uid').val(id);
        //$("#modal-title-ud").html("Upload document for " + resname)
        // console.log("DOCUMENT");
        uploadPITDocs();
    });

    $('#loadDWU').on('click', function () {
        initDocuments();
    });

    $('#loadPDOC').on('click', function () {
        initP_Documents();
    });

    $('#loadPIT').on('click', function () {
        initPITDocs();
    });

    //$("#diag").dialog({
    //    zIndex: 0,
    //    autoOpen: false,
    //    modal: true,
    //    title: "Employee",
    //    dialogClass: "physicianDialog",
    //    open: function () {
    //        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
    //                                          .addClass("ui-state-disabled");
    //        if (!$("#userForm").data("layoutContainer")) {
    //            $("#userForm").layout({
    //                center: {
    //                    paneSelector: ".tabPanels",
    //                    closable: false,
    //                    slidable: false,
    //                    resizable: true,
    //                    spacing_open: 0
    //                },
    //                north: {
    //                    paneSelector: ".tabs",
    //                    closable: false,
    //                    slidable: false,
    //                    resizable: false,
    //                    spacing_open: 0,
    //                    size: 55
    //                }
    //            });
    //        }
    //        $("#userForm").layout().resizeAll();
    //        if (!$("#userForm").data("tabs")) {
    //            $("#userForm").tabs({
    //                select: function (event, ui) {
    //                    if (ui.index == 2) {
    //                        setTimeout(initService, 1);
    //                    } else if (ui.index == 3) {
    //                        setTimeout(initRoles, 1);
    //                    } else if (ui.index == 6) {
    //                        setTimeout(initDocuments, 1);
    //                    } else if (ui.index == 7) {
    //                        setTimeout(initP_Documents, 1);
    //                    } else if (ui.index == 8) {
    //                        setTimeout(initPITDocs, 1);
    //                    }

    //                    if (ui.index != 4) {
    //                        $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
    //                                          .addClass("ui-state-disabled");
    //                    } else {
    //                        $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
    //                                                                      .removeClass("ui-state-disabled");
    //                    }
    //                }
    //            });
    //        }
    //        $("#userForm").tabs("option", "selected", 0);
    //        $("#empForm").tabs();
    //        //var id = $('#userForm #txtCSId').val();
    //        //$('#userForm #txtCSId').val(id);

    //        var id = $('#userForm #txtId').val();
    //        //$('#userForm #txtCSId').val(id);
    //    }
    //});

     $(document).on("click", "#userForm #tabs a", function () {
        var tab = $(this).attr("href");
        currentTabIdx = tab;
        if (tab == "#tabs-usr") {
            $("#userForm #tabsviews").css("overflow-y", "hidden");
            $("#empForm #tabsformviews").css("overflow-y", "auto");
            $("#empForm #tabsformviews").css("overflow-x", "hidden");
            $('#empForm #tabsformviews').height($(window).height() - 420);
            $(window).resize(function () {
                $('#userForm #tabsviews').height($(window).height() - 420);
            });
        } else {
            $("#userForm #tabsviews").css("overflow-y", "auto");
        }

        if (tab == "#tabs-1") {
            $("#userForm .ps__rail-x").css("display", "none");
        }

        
        if (tab == "#tabs-3") {
            $(".loader").show();
            setTimeout(function () {
                initService();
                $(".loader").hide();
            }, 100)
        } else if (tab == "#tabs-4") {
            setTimeout(initRoles, 1);
        } else if (tab == "#tabs-dsp") {
            $(".loader").show();
            setTimeout(function () {
                initDocuments();
                $(".loader").hide();
            }, 100)
        } else if (tab == "#tabs-pdocs") {
            $(".loader").show();
            setTimeout(function () {
                initP_Documents();
                $(".loader").hide();
            }, 100)
        } else if (tab == "#tabs-pit") {
            $(".loader").show();
            setTimeout(function () {
                initPITDocs();
                $(".loader").hide();
            }, 100)
        }

        if (tab != "#tabs-usr") {
            $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                              .addClass("ui-state-disabled");
        } else {
            $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
                                                          .removeClass("ui-state-disabled");

            var tbs = $("#tabsForm").find("a");
            $.each(tbs, function (i, e) {

                $(this).removeClass("active");
                $(this).removeClass("show");
            })

            var tbs2 = $("#tabsformviews").find(".tab-pane");
            $.each(tbs2, function (i, e) {
                $(this).removeClass("active");
            })

            $.each(tbs, function (i, e) {
                if ($(this).attr("href") == "#tabs-1a") {
                    $(this).click();
                }

            })

        }

    })

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "user", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormDataUser",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    //console.log(m);
                    if (cb) cb(m);
                    $("#HSRdate").datepicker({ formatDate: "MM/dd/yyyy" });
                    $("#HSRtbdate").datepicker({ formatDate: "MM/dd/yyyy" });
                    $("#HSRhealthscreendate").datepicker({ formatDate: "MM/dd/yyyy" });
                    $("#PFChiredate, #PFCstartdate, #PFCenddate, #PFCpersonalrecorddate,#PFChealthscreendate,#PFCcriminalrecorddate,#PFClivescandate,#PFCbgclearancedate,#PFCemployeerightdate,#PFCelderabusedate,#PFChepatitisdate,#PFCeligibilitydate,#PFCpayrolldate,#PFCsignedjobdate,#PFCfirstaiddate,#PFCinservicedate,#PFCdriverslicensedate,#PFCsocialsecuritydate,#PFCemphandbookdate,#PFCtbclearancedate,#PFCphotographdate,#PWNempdate").datepicker({ formatDate: "MM/dd/yyyy" });
                    $("#PWNallowno,#PWNaddtlamount,#PWN_PAWa,#PWN_PAWb,#PWN_PAWc,#PWN_PAWd,#PWN_PAWe,#PWN_PAWf,#PWN_PAWg,#PWN_PAWh,#PWN_DAA1,#PWN_DAA2,#PWN_DAA3,#PWN_DAA4,#PWN_DAA5,#PWN_DAA6,#PWN_DAA7,#PWN_DAA8,#PWN_DAA9,#PWN_DAA10,#PWN_PAWa,#PWN_TMJ1,#PWN_TMJ2,#PWN_TMJ3,#PWN_TMJ4,#PWN_TMJ5,#PWN_TMJ6,#PWN_TMJ7,#PWN_TMJ8,#PWN_TMJ9").forceNumericOnly();
                    //$("#empForm").tabs({
                    //    select: function (event, ui) {
                    //        if ((ui.index == 3) || (ui.index == 4) || (ui.index == 5) || (ui.index == 6) || (ui.index == 7)){
                    //            $(".ui-dialog-buttonpane button:contains('Print')").attr("disabled", true)
                    //                .addClass("ui-state-disabled");
                    //            $(".ui-dialog-buttonpane button:contains('Save')").attr("disabled", true)
                    //                .addClass("ui-state-disabled");
                    //        } else {
                    //            $(".ui-dialog-buttonpane button:contains('Print')").removeAttr("disabled", true)
                    //                .removeClass("ui-state-disabled");
                    //            $(".ui-dialog-buttonpane button:contains('Save')").removeAttr("disabled", true)
                    //                .removeClass("ui-state-disabled");
                    //        }
                    //    }
                    //});
                    if (m.BirthDate != "" && $("#HSRuserage").val() == "") {
                        age = _calculateAge(m.BirthDate);
                        $("#HSRuserage").val(age);
                    }

                }
            });
            return;
        }

        if ($('#txtPassword').val() == '')
            $('#txtConfirm').attr('class', 'form-control m-input');
        else
            $('#txtConfirm').attr('class', 'form-control m-input');

        //VALIDATE HERE
        //var isValid = $('#tabs-1').validate();
        //do other modes

        if (mode == 1) {

            if (isSysUser == 1) {
                if (($("#txtId").val() == "") || ($("#txtFirstname").val() == "") || ($("#txtLastname").val() == "") || ($("#txtPassword").val() == "") || ($("#txtConfirm").val() == "") || ($("#txtEmail").val() == "")) {
                    swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                    return false;
                }
            } else {
                if (($("#txtFirstname").val() == "") || ($("#txtLastname").val() == "") || ($("#txtEmail").val() == "")) {
                    swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                    return false;
                }
            }

        }
        if (mode == 1 || mode == 2) {

            if (($("#txtFirstname").val() == "") || ($("#txtLastname").val() == "") || ($("#txtEmail").val() == "")) {
                swal("Fill in all required (<span style=\"color: red;\">*</span>) fields.")
                return false;
            }


            //if (!isValid) return;
            row = $('#tabs-1,#tabs-2').extractJson();
            //console.log(row);
            row.OtherRoles = $(".checkbox-grid input").filter(function () {
                return $(this).attr('checked') && !$(this).attr('disabled')
            }).map(function () { return $(this).val() }).get().join(',');

            if (row.IsSysUser == false) {
                row.IsActive = false;
            }

            var dow = [];

            $('.dayofweek2').find('input[type="checkbox"]').each(function () {
                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun': dow.push('0'); break;
                        case 'Mon': dow.push('1'); break;
                        case 'Tue': dow.push('2'); break;
                        case 'Wed': dow.push('3'); break;
                        case 'Thu': dow.push('4'); break;
                        case 'Fri': dow.push('5'); break;
                        case 'Sat': dow.push('6'); break;
                    }
                }
            });
            //console.log(dow);

            //if (dow.length == 0) {
            //    alert('Please choose day of week schedule.');
            //    return;
            //}
            row.DaySchedule = dow.join(',');
        }

        //console.log(row);
        delete row['Mon'];
        delete row['Tue'];
        delete row['Wed'];
        delete row['Thu'];
        delete row['Fri'];
        delete row['Sat'];
        delete row['recurring'];
        delete row['Activity'];
        delete row['Onetimedate'];
        delete row['RoomId'];
        delete row['ResidentId'];
        delete row['ActivityId'];

        var data = { func: "user", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    $(document).on("click", "#saveemployeeinfo", function () {
        var id1 = $('#userForm #txtId').val();
        if (addUserMode == 1) {
            id1 = addUserMode;
        }
        var amode = $.isNumeric(id1) ? 1 : 2;
        var row = container[currentId];

        var tabs = $("#tabs").find("a");
        var tabIdx;
        var tabIdxHref;

        $.each(tabs, function (i, e) {
            if ($(this).hasClass("active")) {
                tabIdx = $(this).parents("li").index();
                tabIdxHref = $(this).attr("href");
            }
        })

        if (tabIdx != 4) {
            doAjax(amode, row, function (m) {
                if (m.result == 0) {
                    swal("User information was " + (amode == 1 ? "saved" : "updated") + " successfully!", "", "success");
                    if (tabIdx == 0) {
                        load_users();
                    }
                } else if (m.result == -4) {
                    toastr.error(m.message);
                    //  $.growlWarning({ message: m.message, delay: 6000 });
                    return;
                }
                if (amode == 1) {
                    load_users();
                }
            });
        } else {
            var row = {};
            //var tabIdx2 = $("#empForm").tabs('option', 'selected');

            var tabs = $("#tabsForm").find("a");
            var tabindex;
            var tabIdxHref;

            $.each(tabs, function (i, e) {
                if ($(this).hasClass("active")) {
                    tabindex = $(this).parents("li").index();
                    tabIdxHref = $(this).attr("href");
                }
            })

            //alert(tabIdx2);

            doAjaxUsr(amode, row, function (m) {
                //var tabindex = $("#empForm").tabs('option', 'selected');
                //if (selectedTabIndex > tabindex) {
                //    $("#admForm").tabs("option", "selected", selectedTabIndex);
                //}
                //else {
                //selectedTabIndex = tabindex +1;
                if (tabindex == 0) {
                    swal("Personnel File Checklist  " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 1) {
                    swal("Health Screening Report " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 2) {
                    swal("Criminal Record Statement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 3) {
                    swal("Employee Rights Form " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 4) {
                    swal("Elder Abuse Acknowledgement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 5) {
                    swal("Photography/Video Consent " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 6) {
                    swal("Employee Handbook Receipt " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 7) {
                    swal("Hepatitis B Vaccine Declination Form " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 8) {
                    swal("Personnel Record " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 9) {
                    swal("Request for Live Scan Services " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 10) {
                    swal("Employment Eligibility Verification " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 11) {
                    swal("Payroll Withholding Notice " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                } else if (tabindex == 12) {
                    swal("Job Description Ackowledgement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");

                }

                //}
            });
        }
        addUserMode = 0;
    });

    //change this to saveemployeeinfo
    var showdiag = function (mode) {
        var id = $('#userForm #txtId').val();
        var amode = $.isNumeric(id) ? 2 : 1;
        if (mode == 1) {
            $("#diag").dialog("option", 'title', 'Employee');
        }
        $("#diag").dialog("option", {
            width: 1040,
            height: $(window).height() - 200,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        var tabIdx = $("#userForm").tabs('option', 'selected');
                        if (tabIdx != 4) {
                            doAjax(mode, row, function (m) {
                                if (m.result == 0) {
                                    $.growlSuccess({ message: "User " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                    $("#diag").dialog("close");
                                    //$('#grid').trigger('reloadGrid');
                                    if (mode == 1) isReloaded = true;
                                    else {
                                        fromSort = true;
                                        //new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                    }
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (m.result == -4) {
                                    $.growlWarning({ message: m.message, delay: 6000 });
                                    return;
                                }
                            });
                        } else {
                            var row = {};
                            var tabIdx2 = $("#empForm").tabs('option', 'selected');
                            //alert(tabIdx2);

                            doAjaxUsr(2, row, function (m) {
                                var tabindex = $("#empForm").tabs('option', 'selected');
                                //if (selectedTabIndex > tabindex) {
                                //    $("#admForm").tabs("option", "selected", selectedTabIndex);
                                //}
                                //else {
                                //selectedTabIndex = tabindex +1;
                                if (tabindex == 0) {
                                    $.growlSuccess({
                                        message: "Personnel File Checklist  " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 1) {
                                    $.growlSuccess({
                                        message: "Health Screening Report " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 2) {
                                    $.growlSuccess({
                                        message: "Criminal Record Statement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 3) {
                                    $.growlSuccess({
                                        message: "Employee Rights Form " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 4) {
                                    $.growlSuccess({
                                        message: "Elder Abuse Acknowledgement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 5) {
                                    $.growlSuccess({
                                        message: "Photography/Video Consent " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });

                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 6) {
                                    $.growlSuccess({
                                        message: "Employee Handbook Receipt " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 7) {
                                    $.growlSuccess({
                                        message: "Hepatitis B Vaccine Declination Form " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 8) {
                                    $.growlSuccess({
                                        message: "Personnel Record " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 9) {
                                    $.growlSuccess({
                                        message: "Request for Live Scan Services " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 10) {
                                    $.growlSuccess({
                                        message: "Employment Eligibility Verification " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 11) {
                                    $.growlSuccess({
                                        message: "Payroll Withholding Notice " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                } else if (tabindex == 12) {
                                    $.growlSuccess({
                                        message: "Job Description Ackowledgement " + (amode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000
                                    });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');

                                }

                                //}
                            });
                        }
                    },
                    class: "primaryBtn",
                    id: "SaveUserFile",
                    text: "Save"
                },
                "Print": {
                    click: function () {
                        var ts = $("#empForm").tabs('option', 'selected');

                        $("#SaveUserFile").click();
                        setTimeout(function () {
                            if (ts == 0) {
                                window.open(ALC_URI.Admin + "/PrintPersonnelFileChecklist?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 1) {
                                window.open(ALC_URI.Admin + "/PrintHealthScreeningReport?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 2) {
                                window.open(ALC_URI.Admin + "/PrintCriminalRecordStatement?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 3) {
                                window.open(ALC_URI.Admin + "/PrintEmployeeRights?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 4) {
                                window.open(ALC_URI.Admin + "/PrintElderAbuseAcknowledgement?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 5) {
                                window.open(ALC_URI.Admin + "/PrintEmpPhotographyConsent?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 6) {
                                window.open(ALC_URI.Admin + "/PrintEmployeeHandbookReceipt?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                            else if (ts == 7) {
                                window.open(ALC_URI.Admin + "/PrintEmpHepatitisForm?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                            else if (ts == 8) {
                                window.open(ALC_URI.Admin + "/PrintPersonnelRecord?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 9) {
                                window.open(ALC_URI.Admin + "/PrintRequestForLiveScanService?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                            else if (ts == 10) {
                                window.open(ALC_URI.Admin + "/PrintEmpEligibilityVerification?id=" + $('#userForm #txtId').val(), '_blank');
                            } else if (ts == 11) {
                                window.open(ALC_URI.Admin + "/PrintEmployeePayrollWithholding?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                            else if (ts == 12) {
                                window.open(ALC_URI.Admin + "/PrintJobDescAcknowledgement?id=" + $('#userForm #txtId').val(), '_blank');
                            }
                        }, 3000);
                    },
                    id: "EmpFilesPrint",
                    text: "Save & Print"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            },
            resize: function (event, ui) {
                resizeEmpForm();
            }
        }).dialog("open");
        var resizeEmpForm = function () {
            var diag = $('.ui-dialog-content');
            diag = diag.find('#empForm').parents('.ui-dialog-content');
            var height = 0;
            height = diag.eq(0).height() || 0;
            if (height <= 0)
                height = diag.eq(1).height() || 0;
            $('#empForm .tabPanels').height(height - 150);
            //$('#empForm .tabs').height(115).css({ 'overflow': 'hidden' });
            $('#empForm .tabs').height(90).css({ 'overflow': 'hidden' });

        }
        resizeEmpForm();
    }


    var doAjaxUsr = function (mode, row, cb) {
        console.log(mode);
        //get record by id     
        if (mode == 0) {
            var data = { func: "users", id: row.Id };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }
        //VALIDATE HERE
        //var isValid = $('#empForm').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            //if (!isValid) {
            //    return;
           //}
            row = $('#empForm .usrTbs').extractJson('id');
            row.userId = $('#userForm #txtId').val();
            //alert(row.userId);
            row.dcd = 1; //dont check duplicates.
        }
        //var tabindex = $("#empForm").tabs('option', 'selected');
        var tabs = $("#tabsForm").find("a");
        var tabindex;

        $.each(tabs, function (i, e) {
            if ($(this).hasClass("active")) {
                tabindex = $(this).parents("li").index();
            }
        })

        var data = { func: "user_files", mode: mode, data: row, TabIndex: tabindex };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });

    }


    $('#txtPass').focus(function () {
        $(this).hide();
        $('#txtPassword').show().focus();
    });
    $('#txtPassword').blur(function () {
        if ($(this).val() == '') {
            $(this).hide();
            $('#txtPass').show();
        }
    });

    $('#chkIsSysUser').click(function () {
        if (this.checked) {
            isSysUser = 1;
            $('.non-sys-user').show();
            $('#usersForm .nonsysuser').attr('required', 'required');
            $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
        } else {
            isSysUser = 0;
            $('.non-sys-user').hide();
            $('#usersForm .nonsysuser').removeAttr('required');
            $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
        }
    });

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);

        if (q.is('.add')) {
            $('#txtId').removeAttr('readonly');
            $('#tabs-1,#tabs-2').clearFields();
            $('#txtPass').hide();
            $('#txtPassword').show();
            $('#chkIsActive').attr('checked', 'checked');
            $('#chkIsSysUser').attr('checked', 'checked');
            $('.non-sys-user').show();
            $('[href="#tabs-4"]').closest('li').show();
            $('[href="#tabs-3"]').closest('li').hide();
            $(".checkbox-grid input").removeAttr('checked').removeAttr('disabled');
            showdiag(1);
        } else if (q.is('.del')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#grid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax(3, { Id: row.Id }, function (m) {
                                var msg = row.Name + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = row.Name + " cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3)
                                    $('#users_1 a').trigger('click');
                                //$('#grid').trigger('reloadGrid');
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.unsign')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');

            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (row.IsActive == true) {
                    if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
                        setTimeout(function () {
                            if (row) {
                                var currId = row.Id;
                                doAjax(4, { Id: row.Id }, function (m) {

                                    if (m.result == 0)
                                        $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
                                    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                    setTimeout("$('#grid').jqGrid('setSelection','" + currId + "')", 100);

                                    if (m.result != -1 || m.result != -3) {
                                        if (mode == 1) isReloaded = true;
                                        else {
                                            fromSort = true;
                                            //new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                                        }
                                        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                    }
                                });
                            }
                        }, 100);
                    }
                } else { $("#lnkdeactivate").attr("disabled", true); }

            } else {
                alert("Please select row to de-activate.")
            }
        } else if (q.is('.copy')) {
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            if (id) {
                var row = $('#grid').jqGrid('getRowData', id);
                if (confirm('Are you sure you want to copy "' + row.Name + '"?')) {
                    $('#txtId').removeAttr('readonly');
                    $('#tabs-1,#tabs-2').clearFields();
                    $('#txtPass').show();
                    $('#txtPassword').hide();
                    $('#chkIsActive').attr('checked', 'checked');
                    if (row.NonSysUser == "0") {
                        $('#chkIsSysUser').attr('checked', 'checked');
                        $('.non-sys-user').show();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
                    } else {
                        $('#chkIsSysUser').removeAttr('checked');
                        $('.non-sys-user').hide();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
                    }
                    if (row) {
                        doAjax(0, row, function (res) {
                            $("#lblSched").html('');
                            //$('.form').mapJson(res);
                            $('#ddlRole').val(res.Role);
                            $('#ddlDepartment').val(res.Department);
                            //role permission set primary role
                            $(".checkbox-grid input").filter(function () {
                                return $(this).val() == $('#ddlRole').val()
                            }).attr('checked', true).attr('disabled', true);
                            //set other roles
                            if (res.OtherRoles != null && res.OtherRoles != '') {
                                $.each(res.OtherRoles.split(','), function (x, i) {
                                    $(".checkbox-grid input").filter(function () {
                                        return $(this).val() == i;
                                    }).attr('checked', true);
                                });
                            }
                            //$('#txtCity').val(res.City);
                            //$('#txtState').val(res.State);
                            //$('#txtZip').val(res.Zip);
                            //$('#ddlGender').val(res.Gender);
                            if (res.ShiftStart != null && res.ShiftEnd != null) {
                                $('#txtShiftId').val(res.ShiftId);
                                $('#txtShift').val(res.Shift);
                                $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");
                            }
                            showdiag(1);
                        });
                    }
                }
            } else {
                alert("Please select row to copy.")
            }
        } else if (q.is('#lnkUpload')) {
            $("#ImageData").val("");
            var id = $('#grid').jqGrid('getGridParam', 'selrow');
            $('#resid').val(id);
            var row = $('#grid').jqGrid('getRowData', id);
            var title = "Upload image of " + row.Name;

            $('#diagupload').dialog({ title: title }).dialog('open');
        }
    });

    $("#upload-resimg").on("click", function () {
        var id = currentId;
        //$("#ImageData").val("");
        //var id = $('#grid').jqGrid('getGridParam', 'selrow');
        $('#resid').val(id);
        //var row = $('#grid').jqGrid('getRowData', id);
        //var title = "Upload image of " + row.Name;

        //$('#diagupload').dialog({
        //    title: title
        //}).dialog('open');
    });

    $("#uploadimg, #btnUploadResImg").click(function () {
        var data = new FormData();
        var files = $("#ImageData").get(0).files;

        if (files.length > 0) {
            data.append("ImageData", files[0]);
            data.append("resid", $("#resid").val());

            $.ajax({
                url: "Admin/UploadImg?func=user",
                type: "POST",
                processData: false,
                contentType: false,
                data: data,
                success: function (response) {
                    $("#uploadresimg_modal").modal("hide");
                    //if ($('#diag').dialog('isOpen')) {

                    var strhtml = $(".resimg img").attr("src", response.image);
                    var s = $("#" + currentId).attr("src", response.image);
                    //$(".card-img-top").attr("src", response.image);

                    //}

                    //$("#diagupload").dialog("close");
                    //$('#grid').setGridParam({
                    //    datatype: 'json'
                    //}).trigger('reloadGrid');

                },
                error: function (er) {
                    alert(er);
                }

            });

        } else {

            alert("No file chosen");

        }



    })

    //initialization
    var container = {};
    var containerTask = {};
    var employeeDatatable = $("#user_emp_tbl").DataTable();
    // var serviceTask = $("#sGrid").DataTable();
    // var discplinarywu = $("#wGrid").DataTable();
    var currentId;
    var currentTabIdx;


    //Inserted column SSN on Admin/Employees - by Alona Cabrias June 21 2017
    var load_users = function () {
        //$(function () {
        //this is in site.layout.js.
        //var GURow = new Grid_Util.Row();

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=user",
            dataType: "json",
            success: function (m) {
                var html_tile = "";
                var gender = "";
                var gender_text;
                if (m) {
                    //console.log(m);
                    $.each(m, function (i, v) {

                        if (v.NonSysUser == "1") {
                            v.NonSysUser = "Non-System User";
                        } else if (v.NonSysUser == "0") {
                            v.NonSysUser = "System User";
                        }

                        if (v.IsActive == "1") {
                            v.IsActive = "Active";
                        } else if (v.IsActive == "0") {
                            v.IsActive = "Inactive";
                        }                       

                        container[v.Id] = v;
                        //console.log(v);

                        gender = "";
                        gender_text = "";                       
                        
                    })
                }

                $(document).on("click", ".employee_name", function () {
                    $('#userForm input,select').prop('disabled', true);
                    $('#saveemployeeinfo').attr("disabled", true);
                    //$(".imgsrc, .imgname").click(function (rowid, iRow, iCol, cellText, e) {
                    var G = $('#grid');
                    // $('#userForm #txtId').val(2);
                    //var this_class = $(this).attr("class");
                    var tbs = $("#tabs").find("a");
                    $.each(tbs, function (i, e) {

                        $(this).removeClass("active");
                        $(this).removeClass("show");
                    })

                    var tbs2 = $("#tabsviews").find(".tab-pane");
                    $.each(tbs2, function (i, e) {
                        if ($(this).attr("id") != "tabs-1a")
                            $(this).removeClass("active");
                    })

                    $.each(tbs, function (i, e) {
                        if ($(this).attr("href") == "#tabs-1") {
                            $(this).click();
                        }

                    })


                    rowid = $(this).attr("id");
                    currentId = rowid; //latest clicked employee

                    //G.setSelection(rowid, false);

                    $('#tabs-1,#tabs-2').clearFields();
                    $('#txtId').attr('readonly', 'readonly');
                    $('#txtPass').show();
                    $('#txtPassword').hide();

                    var row = container[currentId];
                    //console.log(row);

                    var htmll = "";

                    if (typeof (btoa) == 'undefined')
                        htmll += "<img class='imgsrc' src='Resource/EmpImage?id=" + row.Id + "&b=0&r=" + Date.now() + "' alt='" + row.Name + "' width='100%' style='cursor:pointer;border-radius:5%''/>";
                    else
                        htmll += "<img class='imgsrc' src='Resource/EmpImage?id=" + btoa(row.Id) + "&b=1&r=" + Date.now() + "' alt='" + row.Name + "' width='100%' style='cursor:pointer;border-radius:5%''/>";

                    $(".resdform .resimg").html(htmll);

                    var dow = row.DaySchedule;
                    //console.log(dow);
                    if (dow != null) {
                        $('.dayofweek2').find('input[type="checkbox"]').each(function () {
                            var d = $(this);
                            switch (d.attr('name')) {
                                case 'Sun': setCheck2(d, dow, '0'); break;
                                case 'Mon': setCheck2(d, dow, '1'); break;
                                case 'Tue': setCheck2(d, dow, '2'); break;
                                case 'Wed': setCheck2(d, dow, '3'); break;
                                case 'Thu': setCheck2(d, dow, '4'); break;
                                case 'Fri': setCheck2(d, dow, '5'); break;
                                case 'Sat': setCheck2(d, dow, '6'); break;
                            }
                        });
                    }

                    $('a.checkall').attr('data-toggle', 'cc').html('Check All Days');

                    if (row.NonSysUser == "System User") {
                        $('#chkIsSysUser').attr('checked', 'checked');
                        $('.non-sys-user').show();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
                    } else {
                        $('#chkIsSysUser').removeAttr('checked');
                        $('.non-sys-user').hide();
                        $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
                    }
                    $(".checkbox-grid input").removeAttr('checked').removeAttr('disabled');
                    if (row) {
                        $(".loader").show();
                        doAjax(0, row, function (res) {
                            if (res.State != null) {
                                res.State = res.State.trim();
                            }

                            $("#lblSched").html('');
                            $('#tabs-1,#tabs-2').mapJson(res);
                            $("#empForm").mapJson(res, 'id', "#empForm");
                            if (res.ShiftStart != null && res.ShiftEnd != null)
                                $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");

                            if (res.Role == null || $('#carestaffRoles').val().split(",").indexOf(res.Role.toString()) == -1) {
                                $('[href="#tabs-3"]').closest('li').hide();
                            } else {
                                $('[href="#tabs-3"]').closest('li').show();
                            }

                            $("#txtEmail").val(res.Email);

                            //showdiag(2);

                            //var title = $("#diag").dialog("option", 'title');

                            $("#empname").html(row.Name)
                            //$("#diag").dialog("option", 'title', 'Employee - ' + row.Name);
                            $('#ddlRole').val(res.Role);
                            $('#ddlDepartment').val(res.Department);

                            //role permission set primary role
                            $(".checkbox-grid input").filter(function () {
                                return $(this).val() == $('#ddlRole').val()
                            }).attr('checked', true).attr('disabled', true);

                            //set other roles

                            $.each(res.OtherRoles.split(','), function (x, i) {
                                $(".checkbox-grid input").filter(function () {
                                    return $(this).val() == i;
                                }).attr('checked', true);
                            });
                            //$("#empForm").tabs("option", "selected", 0);

                            //doAjaxUsr(0, row, function (res) {
                            //    //debugger;
                            //    //alert("Sdf");
                            //    //$("#empForm").clearFields();
                            //    ////$("#empForm").mapJson(res, 'id', "#empForm");
                            //    //$('#empForm').mapJson(res);

                            //});
                            $(".loader").hide();
                        });


                    }
                });
            }
        });





        //$('#grid').jqGrid({
        //    url: "Admin/GetGridData?func=user",
        //    datatype: 'json',
        //    postData: "",
        //    ajaxGridOptions: { contentType: "application/json", cache: false },
        //    //datatype: 'local',
        //    //data: dataArray,
        //    //mtype: 'Get',
        //    colNames: [' ', 'User ID', 'Name', 'Email', 'SSN', 'Role', 'Department', 'Is Active', 'User Type', 'Shift', 'Date Created', 'DaySched', 'DaySchedDisplay', 'Date Updated', 'Employee', 'Department', 'Shift'],
        //    colModel: [
        //        {
        //            key: false,
        //            name: 'Image',
        //            index: 'Image',
        //            width: 150,
        //            fixed: true,
        //            formatter: imageFormatter,
        //            //cellattr: setTitle
        //        },
        //      { hidden: true, key: true, name: 'Id', index: 'Id' },

        //      {
        //          hidden: true, key: false, name: 'Name', index: 'Name', sortable: true, formatter: "dynamicLink", formatoptions: {
        //              //cellValue: function (val, rowid, row, options) {
        //              //    var name = [];
        //              //    if (row.Firstname != "")
        //              //        name.push(row.Firstname + " ");
        //              //    if (row.MiddleInitial != "")
        //              //        name.push(row.MiddleInitial + " ");
        //              //    if (row.Lastname != "")
        //              //        name.push(row.Lastname + " ");
        //              //    return name.join('');
        //              //},
        //              onClick: function (rowid, iRow, iCol, cellText, e) {

        //                  var G = $('#grid');
        //                  G.setSelection(rowid, false);
        //                  $('#tabs-1,#tabs-2').clearFields();
        //                  $('#txtId').attr('readonly', 'readonly');
        //                  $('#txtPass').show();
        //                  $('#txtPassword').hide();
        //                  var row = G.jqGrid('getRowData', rowid);
        //                  if (row.NonSysUser == "0") {
        //                      $('#chkIsSysUser').attr('checked', 'checked');
        //                      $('.non-sys-user').show();
        //                      $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').show();
        //                  } else {
        //                      $('#chkIsSysUser').removeAttr('checked');
        //                      $('.non-sys-user').hide();
        //                      $('[href="#tabs-3"],[href="#tabs-4"]').closest('li').hide();
        //                  }
        //                  $(".checkbox-grid li input").removeAttr('checked').removeAttr('disabled');
        //                  if (row) {
        //                      doAjax(0, row, function (res) {
        //                          $("#lblSched").html('');
        //                          $('#tabs-1,#tabs-2').mapJson(res);
        //                          if (res.ShiftStart != null && res.ShiftEnd != null)
        //                              $("#lblSched").html("(" + res.ShiftStart + ' - ' + res.ShiftEnd + ")");

        //                          if (res.Role == null || $('#carestaffRoles').val().split(",").indexOf(res.Role.toString()) == -1) {
        //                              $('[href="#tabs-3"]').closest('li').hide();
        //                          } else {
        //                              $('[href="#tabs-3"]').closest('li').show();
        //                          }
        //                          $.trim("#txtState");
        //                          showdiag(2);
        //                          var title = $("#diag").dialog("option", 'title');
        //                          $("#diag").dialog("option", 'title', 'Employee - ' + row.Name);
        //                          //role permission set primary role
        //                          $(".checkbox-grid li input").filter(function () {
        //                              return $(this).val() == $('#ddlRole').val()
        //                          }).attr('checked', true).attr('disabled', true);
        //                          //set other roles
        //                          $.each(res.OtherRoles.split(','), function (x, i) {
        //                              $(".checkbox-grid li input").filter(function () {
        //                                  return $(this).val() == i;
        //                              }).attr('checked', true);
        //                          });
        //                      });
        //                  }
        //              }
        //          }
        //      },
        //      { hidden: true, key: false, name: 'Email', index: 'Email', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'SSN', index: 'SSN', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'RoleDescription', index: 'RoleDescription', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'Department', index: 'Department', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'IsActive', index: 'IsActive', sortable: true, edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Active', '0': 'Inactive' } } },
        //      { hidden: true, key: false, name: 'NonSysUser', index: 'NonSysUser', sortable: true, edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Non-System User', '0': 'System User' } } },
        //      { hidden: true, key: false, name: 'Shift', index: 'Shift', editable: false, sortable: false },
        //      { hidden: true, key: false, name: 'DateCreated', index: 'DateCreated', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'DateModified', index: 'DateModified', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'DaySchedule', index: 'DaySchedule', editable: true, sortable: true },
        //      { hidden: true, key: false, name: 'DayScheduleDisplay', index: 'DayScheduleDisplay', editable: true, sortable: true },
        //      { key: false, name: 'Info', index: 'Name', formatter: userFormatter },
        //      { key: false, name: 'Department', index: 'Department', editable: true, sortable: true, formatter: dataFormatter },
        //      { key: false, name: 'Shift', index: 'Shift', editable: true, sortable: true, formatter: shiftFormatter },
        //    ],
        //    //pager: jQuery('#pager'),
        //    rowNum: 1000000,
        //    rowList: [10, 20, 30, 40],
        //    height: '100%',
        //    viewrecords: true,
        //    gridview: true,
        //    //caption: 'Care Staff',
        //    mtype: "GET",
        //    emptyrecords: 'No records to display',
        //    //jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
        //    autowidth: true,
        //    multiselect: false,
        //    sortname: 'Name',
        //    sortorder: 'asc',
        //    loadonce: true,
        //    onSortCol: function () {
        //        fromSort = true;
        //        GURow.saveSelection.call(this);
        //    },
        //    loadComplete: function (data) {

        //        

        //        if ($('#ftrRole').find('option').size() <= 0) {
        //            var optsRm = [];

        //            $.each(unique($.map(data, function (o) { return o.RoleDescription })), function () {
        //                optsRm.push('<option>' + this + '</option>');
        //            });
        //            $('#ftrRole').html(optsRm.join(''));

        //            $("#ftrRole").html($('#ftrRole option').sort(function (x, y) {
        //                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
        //            }));
        //            $("#ftrRole").prepend('<option>All</option>');
        //            $("#ftrRole").get(0).selectedIndex = 0;
        //        }
        //        if ($('#ftrDepartment').find('option').size() <= 0) {
        //            optsRm = [];

        //            $.each(unique($.map(data, function (o) { return o.Department })), function () {
        //                optsRm.push('<option>' + this + '</option>');
        //            });
        //            $('#ftrDepartment').html(optsRm.join(''));

        //            $("#ftrDepartment").html($('#ftrDepartment option').sort(function (x, y) {
        //                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
        //            }));
        //            $("#ftrDepartment").prepend('<option>All</option>');
        //            $("#ftrDepartment").get(0).selectedIndex = 0;
        //        }

        //        if (typeof fromSort != 'undefined') {
        //            GURow.restoreSelection.call(this);
        //            delete fromSort;
        //            return;
        //        }

        //        var maxId = data && data[0] ? data[0].Id : '';
        //        //if (typeof isReloaded != 'undefined') {
        //        //    $.each(data, function (k, d) {
        //        //        if (d.Id > maxId) maxId = d.Id;
        //        //    });
        //        //}
        //        //if (maxId > 0)
        //        $("#grid").setSelection(maxId);
        //        delete isReloaded;
        //    }
        //});
        //$("#grid").jqGrid('bindKeys');
        //jQuery("#grid").jqGrid('sortableRows');
        //function unique(array) {
        //    return $.grep(array, function (el, index) {
        //        return index == $.inArray(el, array);
        //    });
        //}
        //  });
    }

    function imageFormatter(cellValue, options, rowObject) {
        if (typeof (btoa) == 'undefined')
            return "<img class='imgsrc' src='Resource/EmpImage?id=" + rowObject.Id + "&b=0&r=" + Date.now() + "' alt='" + rowObject.Name + "' width='100%' style='cursor:pointer;'/>";
        else
            return "<img class='imgsrc' src='Resource/EmpImage?id=" + btoa(rowObject.Id) + "&b=1&r=" + Date.now() + "' alt='" + rowObject.Name + "' width='100%' style='cursor:pointer;'/>";
    }

    //$("#diagupload").dialog({
    //    zIndex: 0,
    //    autoOpen: false,
    //    modal: true,
    //    title: "Upload Picture",
    //    // dialogClass: "companiesDialog",
    //    open: function () {

    //    }
    //});

    //$("#uploadimg").click(function () {


    //})

    function userFormatter(cellValue, options, rowObject) {

        var html = "";

        html += "<br><label class='userData' style='font-style:italic;'>" + (rowObject.Id || "") + "</label>";
        html += "<br><br><label class='imgname_p' ><a class='imgname'>" + (rowObject.Name || "") + "</a></label>";
        html += "<br><label class='userData'><strong>" + (rowObject.Email || "") + "</strong></label>";
        html += "<br><label class='userData'>SSN: <strong>" + (rowObject.SSN || "") + "</strong></label>";
        html += "<br><label style='color:red;'class='userData'><strong>" + (rowObject.IsActive == 0 ? "Inactive" : " ") + "</strong></label>";

        return html;

    }

    function dataFormatter(cellValue, options, rowObject) {

        var usertype = rowObject.NonSysUser;

        var html = "";

        html += "<br><label class='imgname_p' ><a class='imgname'>" + (rowObject.Department || "") + "</a></label>";
        html += "<br><br><label class='userData'>Role: <strong>" + (rowObject.RoleDescription || "") + "</strong></label>";
        html += "<br><label class='userData'>Created By: <strong>" + (rowObject.CreatedBy || "") + "</strong></label>";
        html += "<br><label class='userData'>Date Created: <strong>" + (rowObject.ShiftDateCreated || "") + "</strong></label>";

        return html;

    }

    function shiftFormatter(cellValue, options, rowObject) {

        //var usertype = rowObject.NonSysUser;

        var html = "";

        html += "<br><label class='imgname_p' >" + (rowObject.ShiftType || "") + "</a></label>";
        html += "<br><br><label class='userData'>Shift: <strong>" + (rowObject.Shift || "") + "</strong></label>";
        html += "<br><label class='userData'>Shift Days: <strong>" + (rowObject.DayScheduleDisplay || "") + "</strong><br><br></label>";


        return html;

    }

    //END

    var SearchShift = "";
    $("#txtShift").val(SearchShift).blur(function (e) {
        if ($(this).val().length == 0) {
            $(this).val(SearchShift);
            $('#txtShiftId').val('-1');
            $("#lblSched").html("");
        }
    }).focus(function (e) {
        if ($(this).val() == SearchShift)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "shift", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id,
                            start: item.start,
                            end: item.end
                        }
                    }));
                }
            });
        },

        delay: 200,
        minLength: 0,
        focus: function (event, ui) {
            $("#txtShift").val(ui.item.label);
            $("#txtShiftId").val(ui.item.value);
            $("#lblSched").html("(" + ui.item.start + ' - ' + ui.item.end + ")");
            return false;
        },
        change: function (event, ui) {

        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li>")
                .data("ui-autocomplete-item", item)
                .append("<a><span>" + item.label + ' - (' + item.start + ' - ' + item.end + ")</span></a>")
                .appendTo(ul);
    };


    $('#btn-printsr').click(function (e) {
        var prt = $('#printsr').val();
        if ($('#userForm #txtId').val()) {
            if (prt == "pfc") {
                window.open(ALC_URI.Admin + "/PrintPersonnelFileChecklist?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "hsr") {
                window.open(ALC_URI.Admin + "/PrintHealthScreeningReport?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "pf") {
                window.open(ALC_URI.Admin + "/PrintEmpPhotographyConsent?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "hb") {
                window.open(ALC_URI.Admin + "/PrintEmpHepatitisForm?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "csr") {
                window.open(ALC_URI.Admin + "/PrintCriminalRecordStatement?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "eh") {
                window.open(ALC_URI.Admin + "/PrintEmployeeHandbookReceipt?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "er") {
                window.open(ALC_URI.Admin + "/PrintEmployeeRights?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "eaa") {
                window.open(ALC_URI.Admin + "/PrintElderAbuseAcknowledgement?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "per") {
                window.open(ALC_URI.Admin + "/PrintPersonnelRecord?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "rls") {
                window.open(ALC_URI.Admin + "/PrintRequestForLiveScanService?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "eev") {
                window.open(ALC_URI.Admin + "/PrintEmpEligibilityVerification?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "pw") {
                window.open(ALC_URI.Admin + "/PrintEmployeePayrollWithholding?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }
            else if (prt == "sjd") {
                window.open(ALC_URI.Admin + "/PrintJobDescAcknowledgement?id=" + $('#userForm #txtId').val(), '_blank');
                return;
            }

        }
        else {
            alert('Resident has no Admission');
            return;
        }
    });

    $('#AddServiceToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.add')) {
            AddService();
        } else if (q.is('.del')) {
            var id = $('#sGrid').jqGrid('getGridParam', 'selrow');
            if (id) {
                if (confirm('Are you sure you want to delete the selected row?')) {
                    setTimeout(function () {
                        var row = $('#sGrid').jqGrid('getRowData', id);
                        if (row) {
                            doAjax2(3, { Id: row.Id }, function (m) {
                                var msg = "Task deleted successfully!";
                                if (m.result == -3) //inuse
                                    msg = "Task cannot be deleted because it is currently in use.";
                                $.growlSuccess({ message: msg, delay: 6000 });
                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3) {
                                    fromSort = true;
                                    //new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                    $('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                                }
                            });
                        }
                    }, 100);
                }
            } else {
                alert("Please select row to delete.")
            }
        } else if (q.is('.refresh')) {
            fromSort = true;
            //new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
            //$('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
        }
    });

    $("#diagTask").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Edit Service Task",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });

    $('#txtTimeIn').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#txtCarestaff').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#Onetimedate').trigger('change');
        }
    });
    $('#txtTimeIn').mask('99:99 xy');

    $(document).on("click", "#savesvctask", function () {
        var row = container[currentId];

        doAjax2(2, row, function (m) {
            if (m.result == 0) {
                swal("The task was updated successfully!", "", "success");

                loadSvcTask(currentId);


            } else if (m.result == -2) {
                var resVisible = $('#txtResidentId').is(':visible');
                swal("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", "", "error");
                //there is already a schedule for a particular resident or room
            }
        });


    })

    //delete
    var showdiagTask = function (mode) {
        $("#diagTask").dialog("option", {
            width: 500,
            height: 500,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax2(mode, row, function (m) {
                            if (m.result == 0) {
                                $.growlSuccess({ message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                                $("#diagTask").dialog("close");
                                //initService();
                                if (mode == 1) isReloaded = true;
                                else {
                                    fromSort = true;
                                    //new Grid_Util.Row().saveSelection.call($('#sGrid')[0]);
                                }
                                //$('#sGrid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');

                            } else if (m.result == -2) {
                                var resVisible = $('#txtResidentId').is(':visible');
                                $.growlWarning({ message: "Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", delay: 6000 });
                                //there is already a schedule for a particular resident or room
                            }
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diagTask").dialog("close");
                }
            }
        }).dialog("open");
    }

    var doAjax2 = function (mode, row, cb) {
    debugger
        //get record by id
        if (mode == 0) {
            var data = {
                func: "activity_schedule",
                id: row.Id
            };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        //var isValid = $('#diagTask .form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            //if (!isValid) {
            //    return;
            //}
            row = $('#servicetask_modal .form').extractJson();
            //validate time

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];
            delete row['recurring'];
            delete row['Activity'];
            delete row['Dept'];
            delete row['Onetimedate'];

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring) {
                row.isonetime = '0';
                $('.dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    if (d.is(':checked')) {
                        switch (d.attr('name')) {
                            case 'Sun': dow.push('0'); break;
                            case 'Mon': dow.push('1'); break;
                            case 'Tue': dow.push('2'); break;
                            case 'Wed': dow.push('3'); break;
                            case 'Thu': dow.push('4'); break;
                            case 'Fri': dow.push('5'); break;
                            case 'Sat': dow.push('6'); break;
                        }
                    }
                });

                if (dow.length == 0) {
                    alert('Please choose day of week schedule.');
                    return;
                }

            } else {
                row.isonetime = '1';
                var onetimeval = $('#Onetimedate').val();
                if (onetimeval == '') {
                    alert("Please choose a one time date schedule.");
                    return;
                }

                var onetimeday = moment(onetimeval).day();
                if ($.isNumeric(onetimeday))
                    dow.push(onetimeday.toString());

                row.ScheduleTime = onetimeval + ' ' + $('#txtTimeIn').val();
            }
            row.Recurrence = dow.join(',');
        }

        var data = { func: "activity_schedule", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    $('#txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#viewActivity').val(title);
        $('#txtCarestaff')[0].options.length = 0;
        toggleCarestafflist();
    });

    var bindDept = function (dptid, cb) {
        var data = { deptid: dptid };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetServicesAndCarestaffByDeptId",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //$('#txtCarestaff').empty();
                //$('<option/>').val('').html('').appendTo('#txtCarestaff');
                //$.each(m.Carestaffs, function (i, d) {
                //    $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaff');
                //});

                $('#txtActivity').empty();
                $('#viewActivity').val('');
                var opt = '<option val=""></option>';
                $.each(m.Services, function (i, d) {
                    opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
                });
                $('#txtActivity').append(opt);
                if (typeof (cb) !== 'undefined') cb();
            }
        });
    }

    var toggleCarestafflist = function () {
        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
        if (isrecurring)
            triggerDayofWeekClick();
        else
            $('#Onetimedate').trigger('change');

        //if housekeeper or maintenance hide room dropdown
        var val = $('#ddlDept').val();
        if (val == 5 || val == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    if ($('#ddlDept').length > 0) {
        $('#ddlDept').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('table.dayofweek input').removeAttr('disabled');
            $('#Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('table.dayofweek input').attr('disabled', 'disabled');
            $('table.dayofweek').clearFields();
            $('#Onetimedate').removeAttr('disabled');
        }
    });

    var triggerDayofWeekClick = function () {
        var a = $('#txtTimeIn').val();
        if (a == null || a == '') {
            $('#txtCarestaff').empty();
            //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
            return;
        }

        var dow = [];
        $('.dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun': dow.push('0'); break;
                    case 'Mon': dow.push('1'); break;
                    case 'Tue': dow.push('2'); break;
                    case 'Wed': dow.push('3'); break;
                    case 'Thu': dow.push('4'); break;
                    case 'Fri': dow.push('5'); break;
                    case 'Sat': dow.push('6'); break;
                }
            }
        });
        bindCarestaff(dow);

    }

    $('table.dayofweek input').click(triggerDayofWeekClick);

    var bindCarestaff = function (dow) {
        if ($('#txtTimeIn').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if ($('#txtActivity').val() == '') {
            $('#txtCarestaff')[0].options.length = 0;
            return;
        }
        if (dow == null) dow = [];
        var csid = $('input[xid="hdCarestaffId"]').val();
        if (csid == "") csid = null;

        var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');

        var data = { scheduletime: $('#txtTimeIn').val(), activityid: $('#txtActivity').val(), carestaffid: csid, recurrence: dow.join(',') };
        data.isonetime = isrecurring ? "0" : "1";
        data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#Onetimedate').val()).format("MM/DD/YYYY");
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#txtCarestaff').empty();
                $('<option/>').val('').html('').appendTo('#txtCarestaff');
                $.each(m, function (i, d) {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                });
            }
        });
    }


    $('#Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff(dow);
    });

    var AddService = function () {
        var id = $('#grid').jqGrid('getGridParam', 'selrow');
        if (!id) {
            id = $('#txtId').val();
            if (!id) return;
        }

        $("#ddlDept option[value='5']").remove();
        $("#ddlDept option[value='7']").remove();
        var dept = $('#ddlDept').val();
        $('#diagTask').clearFields();
        $('#liResident select').val(id);
        $('input[xid="hdCarestaffId"]').val('');
        $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
        $('input[name="recurring"]:eq(1)').removeAttr('checked');
        $('#Onetimedate').attr('disabled', 'disabled');
        $('.dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
        showdiagTask(1);
        //if housekeeper or maintenance hide room dropdown
        $('#ddlDept').val(dept);
        if (dept == 5 || dept == 7) {
            $('#liRoom, #liRoom select').show();
            $('#liResident, #liResident select').hide();
        } else {
            $('#liResident, #liResident select').show();
            $('#liRoom, #liRoom select').hide();
        }
    }

    var initRoles = function () {

    }

    var loadSvcTask = function (id) {
        containerTask = {};

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=activity_schedule&param3=" + id,
            dataType: "json",
            success: function (m) {
                var html_table = "";
                var svcIdx = "";

                if (m.length != 0) {

                    $.each(m, function (i, v) {
                        svcIdx = currentId + "_" + v.Id;
                        containerTask[svcIdx] = v;

                        html_table += "<tr id='" + v.Id + "'>";
                        html_table += "<td><a href='#' class='svctask' id='" + v.Id + "' data-toggle='modal' data-target='#servicetask_modal'>" + v.Activity + "</a></td>";
                        html_table += "<td>" + v.Department + "</td>";
                        html_table += "<td>" + v.Resident + "</td>";
                        html_table += "<td>" + v.Carestaff + "</td>";
                        html_table += "<td>" + v.Room + "</td>";
                        html_table += "<td>" + v.Schedule + "</td>";
                        html_table += "<td>" + v.Recurrence + "</td>";
                        html_table += "<td>" + v.Duration + " minutes</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";
                        html_table += "</tr>";
                    })
                }

                $("#sGrid").DataTable().destroy();

                $("#sGridtbl").html(html_table);

                serviceTask = $("#sGrid").DataTable({
                    responsive: !0,
                    pagingType: "full_numbers",
                    dom: 'lBfrtip',
                    select: true,
                    buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="la la-download"></i> Export to Excel',
                        title: 'ALC_Employees_ServiceTask_' + id,
                        className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }, {
                        text: 'Refresh',
                        action: function (e, dt, node, config) {
                            loadSvcTask(currentId);
                        }, className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }
                    ]
                });
            }
        });
    }

    var initService = function () {
        //if ($('#sGrid')[0] && $('#sGrid')[0].grid)
        //    $.jgrid.gridUnload('sGrid');
        //var SGrid = $('#sGrid');

        //var GURow = new Grid_Util.Row();
        //var pageWidth = $("#sGrid").parent().width() - 100;

        var id = currentId;
        if (!id) {
            id = $('#txtId').val();
            if (!id) return;
        }

        loadSvcTask(id);


        //SGrid.jqGrid({
        //    url: "Admin/GetGridData?func=activity_schedule&param3=" + id,// + $('#ftrRecurrence').val(),
        //    datatype: 'json',
        //    postData: '',
        //    ajaxGridOptions: { contentType: "application/json", cache: false },
        //    //datatype: 'local',
        //    //data: dataArray,
        //    //mtype: 'Get',
        //    colNames: ['Id', 'Activity', 'Department', 'Resident', 'Staff', 'Room', 'Schedule', 'Recurrence', 'Duration (Minutes)', 'Date Created',  /*'Completed', 'Remarks'*/],
        //    colModel: [
        //      { key: true, hidden: true, name: 'Id', index: 'Id' },
        //      {
        //          key: false, name: 'Activity', index: 'Activity', sortable: true, width: (pageWidth * (8 / 100)), formatter: "dynamicLink", formatoptions: {
        //              onClick: function (rowid, irow, icol, celltext, e) {
        //                  
        //              }
        //          }
        //      },
        //      { key: false, name: 'Department', index: 'Department', sortable: true, width: (pageWidth * (8 / 100)) },
        //      { key: false, name: 'Resident', index: 'Resident', sortable: true, width: (pageWidth * (6 / 100)) },
        //      { key: false, name: 'Carestaff', index: 'Carestaff', sortable: true, width: (pageWidth * (6 / 100)) },
        //      { key: false, name: 'Room', index: 'Room', sortable: true, width: (pageWidth * (4 / 100)) },
        //      //{ key: false, name: 'ScheduleDate', index: 'ScheduleDate' },
        //      { key: false, name: 'Schedule', index: 'Schedule', sortable: true, width: (pageWidth * (8 / 100)) },
        //       { key: false, name: 'Recurrence', index: 'Recurrence', sortable: true, width: (pageWidth * (8 / 100)) },
        //      { key: false, name: 'Duration', index: 'Duration', sortable: true, width: (pageWidth * (5 / 100)) },
        //      {
        //          key: false, name: 'DateCreated', index: 'DateCreated', sortable: true, width: (pageWidth * (8 / 100)),
        //          formatter: function (cellvalue, options, row) {
        //              return (cellvalue || '').replace(/-/g, '/');
        //          }
        //      }
        //      //{ key: false, name: 'Completed', index: 'Completed', edittype: "select", formatter: 'select', editoptions: { value: { '1': 'Yes', '0': 'No' } } },
        //      //{ key: false, name: 'Remarks', index: 'Remarks' }
        //    ],
        //    //pager: jQuery('#pager'),
        //    rowNum: 1000000,
        //    rowList: [10, 20, 30, 40],
        //    height: '100%',
        //    viewrecords: true,
        //    //caption: 'Care Staff',
        //    emptyrecords: 'No records to display',
        //    jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
        //    autowidth: true,
        //    multiselect: false,
        //    sortname: 'Id',
        //    sortorder: 'asc',
        //    loadonce: true,
        //    onSortCol: function () {
        //        fromSort = true;
        //        GURow.saveSelection.call(this);
        //    },
        //    loadComplete: function (data) {
        //        //if ($('#ftrResident').find('option').size() > 0) return;
        //        //var optsRes = ['<option>All</option>'], optsCS = ['<option>All</option>'], optsDP = ['<option>All</option>'];
        //        //$.each(unique($.map(data, function (o) { return o.Resident })), function () {
        //        //    optsRes.push('<option>' + this + '</option>');
        //        //});
        //        //$.each(unique($.map(data, function (o) { return o.Carestaff })), function () {
        //        //    optsCS.push('<option>' + this + '</option>');
        //        //});
        //        //$.each(unique($.map(data, function (o) { return o.Department })), function () {
        //        //    optsDP.push('<option>' + this + '</option>');
        //        //});
        //        //$('#ftrResident').html(optsRes.join(''));
        //        //$('#ftrCarestaff').html(optsCS.join(''));
        //        //$('#ftrDept').html(optsDP.join(''));

        //        if (typeof fromSort != 'undefined') {
        //            GURow.restoreSelection.call(this);
        //            delete fromSort;
        //            return;
        //        }

        //        var maxId = data && data[0] ? data[0].Id : 0;
        //        if (typeof isReloaded != 'undefined') {
        //            $.each(data, function (k, d) {
        //                if (d.Id > maxId) maxId = d.Id;
        //            });
        //        }
        //        if (maxId > 0)
        //            $("#sGrid").setSelection(maxId);
        //        delete isReloaded;
        //    }
        //});
        //SGrid.jqGrid('bindKeys');
        //$('.tabPanels').layout({
        //    center: {
        //        paneSelector: "#serviceGrid",
        //        closable: false,
        //        slidable: false,
        //        resizable: true,
        //        spacing_open: 0
        //    },
        //    north: {
        //        paneSelector: "#AddServiceToolbar",
        //        closable: false,
        //        slidable: false,
        //        resizable: false,
        //        spacing_open: 0
        //    },
        //    onresize: function () {
        //        resizeGridsS();
        //        //resizeGridHS();
        //    }
        //});
        //resizeGridsS();

        $(document).on("click", ".svctask", function () {
            
            var idx = currentId + "_" + $(this).attr("id");
            var row = containerTask[idx];
            var rowid = {
                Id: $(this).attr("id")
            };
            if (row) {
                doAjax2(0, rowid, function (res) {
                    $("#servicetask_modal .form").clearFields();
                    bindDept(res.Dept.toString(), function () {
                        if (!res.IsOneTime)
                            res.Onetimedate = '';
                        $("#servicetask_modal .form").mapJson(res);
                    });

                    $("#txtResidentId").val(res.ResidentId);
                    $("#ddlDept option[value='5']").remove();
                    $("#ddlDept option[value='7']").remove();
                    $("#ddlDept").val(res.Dept);
                    //if housekeeper or maintenance hide room dropdown
                    if (res.Dept == 5 || res.Dept == 7) {
                        $('#liRoom, #liRoom select').show();
                        $('#liResident, #liResident select').hide();
                    } else {
                        $('#liResident, #liResident select').show();
                        $('#liRoom, #liRoom select').hide();
                    }

                    //map day of week
                    if (!res.IsOneTime) {
                        $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
                        var setCheck = function (el, str, idx) {

                            if (str.indexOf(idx) != -1)
                                el.prop('checked', true);
                            else el.prop('checked', false);
                        }

                        if (res.Recurrence != null && res.Recurrence != '') {
                            var dow = res.Recurrence;
                            $('.dayofweek').find('input[type="checkbox"]').each(function () {
                                var d = $(this);
                                switch (d.attr('name')) {
                                    case 'Sun': setCheck(d, dow, '0'); break;
                                    case 'Mon': setCheck(d, dow, '1'); break;
                                    case 'Tue': setCheck(d, dow, '2'); break;
                                    case 'Wed': setCheck(d, dow, '3'); break;
                                    case 'Thu': setCheck(d, dow, '4'); break;
                                    case 'Fri': setCheck(d, dow, '5'); break;
                                    case 'Sat': setCheck(d, dow, '6'); break;
                                }
                            });
                            $('input[name="recurring"]:eq(0)').trigger('click');
                            $('#Onetimedate').val('');
                        }

                    } else {
                        $('input[name="recurring"]:eq(1)').trigger('click');
                        res.Recurrence = moment(res.Onetimedate).day().toString();
                    }
                    $('input[xid="hdCarestaffId"]').val(res.CarestaffId);
                    var data = { scheduletime: res.ScheduleTime, activityid: res.ActivityId, carestaffid: res.CarestaffId, recurrence: res.Recurrence };
                    data.isonetime = !res.IsOneTime ? "0" : "1";
                    data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/GetCarestaffByTime",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (m) {
                            $('#txtCarestaff').empty();
                            $('<option/>').val('').html('').appendTo('#txtCarestaff');
                            $.each(m, function (i, d) {
                                $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaff');
                            });
                            $('#txtCarestaff').val(res.CarestaffId);
                            //showdiagTask(2);
                        }
                    });
                });
            }
        })
    }
    //end initservice

    //=======initDocumentsDisciplinaryWrUp=======================
    var initDocuments = function () {
        var id = currentId;
        if (!id) return;
        $.ajax({
            url: "Admin/GetGridData?func=disciplinary_writeups&param=" + id,
            datatype: 'json',
            postData: '',
            success: function (m) {
                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {

                        html_table += "<tr>";
                        html_table += "<td><a href='#' id='" + v.UUID + "' class='dnld'>" + v.Description + "</a></td>";
                        html_table += "<td>" + v.Discip_reason + "</td>";
                        html_table += "<td>" + v.CreatedBy + "</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";
                        html_table += "<td style='max-width:500px;word-break:break-all'><a href='#' id='" + v.UUID + "' class='dnld'>" + v.Filename + "</a></td>";
                        html_table += "<td>" + v.Size + "KB</td>";
                        html_table += "<td>" + v.Filetype + "</td>";
                        html_table += "</tr>";
                    })

                }

                $("#dwuGrid").DataTable().destroy();
                $("#dwubody").html('');
                $("#dwubody").html(html_table);
                $("#dwuGrid").DataTable();
            }
        });

    }

    $(document).on("click", ".dnld", function (i, v) {
        window.location.href = "Admin/DownloadDisciplinaryWriteUp/?DOCID=" + $(this).attr("id");

    })
    //==========End initDocumentsDisciplinaryWrUp==============

    //=======initPersonalDocuments=======================
    var initP_Documents = function () {
        var id = currentId;
        if (!id) return;
        $.ajax({
            url: "Admin/GetGridData?func=personal_document&param=" + id,
            datatype: 'json',
            postData: '',
            success: function (m) {
                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {

                        html_table += "<tr>";
                        html_table += "<td><a href='#' id='" + v.UUID + "' class='dnld_pd'>" + v.Description + "</a></td>";
                        html_table += "<td>" + v.CreatedBy + "</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";
                        html_table += "<td style='max-width:500px;word-break:break-all'><a href='#' id='" + v.UUID + "' class='dnld_pd'>" + v.Filename + "</a></td>";
                        html_table += "<td>" + v.Size + "KB</td>";
                        html_table += "<td>" + v.Filetype + "</td>";
                        html_table += "<td><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n<a class='dropdown-item dnld_pd' id='" + v.UUID + "' href='#'><i class='la la-edit'></i> Download</a>\n<a class='dropdown-item del doc_delete' href='#' id='" + v.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                        html_table += "</tr>";
                    })

                }

                $("#pdocGrid").DataTable().destroy();
                $("#pdocbody").html('');
                $("#pdocbody").html(html_table);
                $("#pdocGrid").DataTable();
            }
        });

    }

    $(document).on("click", ".doc_delete", function () {
        var did = $(this);
        var doc_uuid = did.attr('id');
        if (!doc_uuid) return;
        if (!confirm("Are you sure you want to delete the selected row?")) return;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify({
                func: 'personal_document',
                mode: 3,
                data: {
                    docId: doc_uuid
                }
            }),
            dataType: "json",
            success: function (m) {
                if (m.result == 0) {
                    //$.growlSuccess({
                    //    message: "Document has beend deleted successfully!",
                    //    delay: 3000
                    //});
                    toastr.success("Document has been deleted successfully!");
                    initP_Documents();
                } else
                    $.growlError({
                        message: m.message,
                        delay: 6000
                    });
            }
        });
    });

    $(document).on("click", ".dnld_pd", function (i, v) {
        window.location.href = "Admin/DownloadPersonalDocument/?DOCID=" + $(this).attr("id");
    })
    $(document).on("click", "#refresh-pdoc", function () {
        initP_Documents();
    })

    //==========End initPersonalDocuments==============

    //=======initProof In Service Training=======================
    var initPITDocs = function () {
        var id = currentId;
        if (!id) return;
        $.ajax({
            url: "Admin/GetGridData?func=proof_inservice_training&param=" + id,
            datatype: 'json',
            postData: '',
            success: function (m) {
                if (m.length != 0) {
                    var html_table = "";

                    $.each(m, function (i, v) {

                        html_table += "<tr>";
                        html_table += "<td><a href='#' id='" + v.UUID + "' class='dnld_pit'>" + v.pit_title + "</a></td>";
                        html_table += "<td>" + v.CreatedBy + "</td>";
                        html_table += "<td>" + v.DateCreated + "</td>";
                        html_table += "<td style='max-width:500px;word-break:break-all'><a href='#' id='" + v.UUID + "' class='dnld_pit'>" + v.Filename + "</a></td>";
                        html_table += "<td>" + v.Size + "KB</td>";
                        html_table += "<td>" + v.Filetype + "</td>";
                        html_table += "</tr>";
                    })

                }

                $("#pitGrid").DataTable().destroy();
                $("#pitbody").html('');
                $("#pitbody").html(html_table);
                $("#pitGrid").DataTable();
            }
        });

    }

    $(document).on("click", ".dnld_pit", function (i, v) {
        window.location.href = "Admin/DownloadPITDocs/?DOCID=" + $(this).attr("id");
    })
    //==========End initProof In Service Training==============

    //var resizeGridsS = (function () {
    //    //debugger;
    //    var f = function () {
    //        var tabH = $('.tabs').height() + 26;
    //        var tBH = $('#DocumentToolbar').height();
    //        $("#dGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
    //        $("#dGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));

    //        var tBH1 = $('#DocumentToolbar1').height();
    //        $("#pdocGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
    //        $("#pdocGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH1));
    //        //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
    //        //console.log($("#sGrid").height())
    //    }
    //    setTimeout(f, 100);
    //    return f;
    //});

    $('#DocumentToolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.upload')) {
            uploadDocument();
        } else if (q.is('.deldsp')) {
            var docid = $('#dGrid').jqGrid('getGridParam', 'selrow');
            if (!docid) return;
            if (!confirm("Are you sure you want to delete the selected row?")) return;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify({
                    func: 'disciplinary_writeups',
                    mode: 3,
                    data: {
                        docId: docid
                    }
                }),
                dataType: "json",
                success: function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({
                            message: "Document has been deleted successfully!",
                            delay: 3000
                        });
                        initDocuments();
                    } else
                        $.growlError({
                            message: m.message,
                            delay: 6000
                        });
                }
            });
        } else if (q.is('.refresh')) {
            initDocuments();
        }
    });

    function truncate(n) {
        var len = 40;
        var ext = n.substring(n.lastIndexOf(".") + 1, n.length).toLowerCase();
        var filename = n.replace('.' + ext, '');
        if (filename.length <= len) {
            return n;
        }
        var flen = filename.length;
        var f1 = filename.substr(0, (len / 2));
        var f2 = filename.substr((flen - (len / 2)), flen);
        filename = f1 + '...' + f2;
        return filename + '.' + ext;
    }

    //$(document).on("click", "#dwu_upld", function () {


    //})

    function uploadDocument() {
        var dialog_html = "<div class='upload-container'><div class='upload-header'>" +
            "<button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Clear List</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +
            "<div class='memoform'><table style='width:100%'><tr><td>Memo Title</td><td><input type='text' id='memotitle' class='form-control m-input w200px'></td></tr>" +
            "<tr><td>Reason of Disciplinary Action</td><td><input type='text' id='memoreason' class='form-control m-input w200px'></td></tr></table></div>" +
            "<div id='fileList' class='upload-list'></div><div class='upload-footer'><div id='divLegend'>" +
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded</div></div></div></div>";

        $(".dwu_div").html(dialog_html);

        var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a class='status-loading' style='display:none'>&nbsp;</a></td>" +
            "<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" +
            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        //var $dialog = $("<div><div class=\"dialogContent\">" + dialog_html + "</div></div>").dialog({
        //    modal: true,
        //    title: "Upload File",
        //    width: 500,
        //    close: function () {
        //        $dialog.find("#fileupload").fileupload("destroy");
        //        initDocuments();
        //        $dialog.dialog("destroy").remove();
        //    },
        //    buttons: {
        //        "Close": function () {
        //            $dialog.dialog("close");
        //        }
        //    }
        //});
        //$("#btnUpload").attr('disabled', 'disabled').removeClass('primaryBtn').addClass('primaryBtn-disabled');
        $("#fileUpload").on('change', function () {
            var itms = $('.upload-container .totalfiles span:eq(1)');
            if (parseInt(itms.text()) > 0)
                itms.text(parseInt(itms.text()) + this.files.length);
            else
                itms.text(this.files.length);
        });
        $('#btnAddfiles').on('click', function () {
            if ($('#memotitle').val() == '') {
                alert("Please enter memo title.");
                return;
            } else if ($('#memoreason').val() == '') {
                alert("Please state reason of disciplary action.");
                return;
            } else {
                $("#fileUpload").trigger('click');
            }
        });
        $('#btnClearList').on('click', function () {
            $('#fileList').html('');
            $('.totalfiles').html("<span>0</span> of <span>0</span> Files Uploaded");
        });

        //$('#fileUpload').on('click', function () {
        $("#dwu_modal").find("#fileUpload").fileupload({
            url: "Admin/UploadDisciplinaryWriteUp?",
            dataType: "json",
            done: function (e, data) {
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');
                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');
                        var itms = $('.upload-container .totalfiles span:eq(0)');
                        itms.text(parseInt(itms.text()) + 1);
                    }
                    return false;
                }
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
                $('#memotitle, #memoreason').val("");
            },
            add: function (e, data) {
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                    'csv', 'txt', 'rtf', 'zip', 'mp3', 'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif'
                ];

                $.each(files, function (x, i) {
                    var err = "";
                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    //if (this.type.length == 0 || ext)
                    //    err = "The file type is not supported.";
                    //else if (this.type.length && !acceptFileTypes.test(this.type)) {
                    //    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    //    err = "The file type of '." + ext + "' is not supported.";
                    //}
                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('#fileList');
                    var mname = $('#memotitle').val();
                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('span.size').text(size);
                    item.find('span.memoname').text(mname);
                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        return true;
                    } else {
                        item.find('a:eq(0)').show();
                        item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {
                            item.remove();
                        });
                    }
                    data.context = item;
                    data.urlparam = {
                        name: this.name,
                        size: this.size,
                        type: this.type,
                        file: this.file
                    };
                    data.submit();

                });
            },
            submit: function (e, data) {
                var rid = $('#uid').val();
                var memt = $('.memoform #memotitle').val();
                var memr = $('.memoform #memoreason').val();
                data.formData = {
                    paramsid: "documents",
                    fileparams: JSON.stringify(data.urlparam),
                    rid: rid,
                    mt: memt,
                    mr: memr
                };
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({
                    message: "Error: " + jqXHR.responseText,
                    delay: 6000
                });
            }
        });
    }
    //-------------------------------------------------------
    function uploadPersonalDocument() {
        var dialog_html = "<div class='upload-container'><div class='upload-header'>" +
            "<button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Clear List</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +
            "<div class='memoform'><table style='width:100%'><tr><td>Name of File</td><td><input type='text' id='nameoffile' class='w200px form-control m-input'></td></tr>" +
            "</table></div>" +
            "<div id='fileList' class='upload-list'></div><div class='upload-footer'><div id='divLegend'>" +
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded</div></div></div></div>";

        $(".pdoc_div").html(dialog_html);

        var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='memoname'></span> : <span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a class='status-loading' style='display:none'>&nbsp;</a></td>" +
            "<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" +
            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        //var $dialog = $("<div><div class=\"dialogContent\">" + dialog_html + "</div></div>").dialog({
        //    modal: true,
        //    title: "Upload File",
        //    width: 500,
        //    close: function () {
        //        $dialog.find("#fileupload").fileupload("destroy");
        //        initDocuments();
        //        $dialog.dialog("destroy").remove();
        //    },
        //    buttons: {
        //        "Close": function () {
        //            $dialog.dialog("close");
        //        }
        //    }
        //});
        //$("#btnUpload").attr('disabled', 'disabled').removeClass('primaryBtn').addClass('primaryBtn-disabled');
        $("#fileUpload").on('change', function () {
            var itms = $('.upload-container .totalfiles span:eq(1)');
            if (parseInt(itms.text()) > 0)
                itms.text(parseInt(itms.text()) + this.files.length);
            else
                itms.text(this.files.length);
        });
        $('#btnAddfiles').on('click', function () {
            if ($('#nameoffile').val() == '') {
                alert("Please enter name of file.");
                return;
            } else {
                $("#fileUpload").trigger('click');
            }
        });
        $('#btnClearList').on('click', function () {
            $('#fileList').html('');
            $('.totalfiles').html("<span>0</span> of <span>0</span> Files Uploaded");
        });

        //$('#fileUpload').on('click', function () {
        $("#pdoc_modal").find("#fileUpload").fileupload({
            url: "Admin/UploadPersonalDocument",
            dataType: "json",
            done: function (e, data) {
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');
                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');
                        var itms = $('.upload-container .totalfiles span:eq(0)');
                        itms.text(parseInt(itms.text()) + 1);
                    }
                    return false;
                }
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
                $('#nameoffile, #description').val("");
            },
            add: function (e, data) {
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                    'csv', 'txt', 'rtf', 'zip', 'mp3', 'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif'
                ];

                $.each(files, function (x, i) {
                    var err = "";
                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    //if (this.type.length == 0 || ext)
                    //    err = "The file type is not supported.";
                    //else if (this.type.length && !acceptFileTypes.test(this.type)) {
                    //    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    //    err = "The file type of '." + ext + "' is not supported.";
                    //}
                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('#fileList');
                    var mname = $('#nameoffile').val();
                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('span.size').text(size);
                    item.find('span.memoname').text(mname);
                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        return true;
                    } else {
                        item.find('a:eq(0)').show();
                        item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {
                            item.remove();
                        });
                    }
                    data.context = item;
                    data.urlparam = {
                        name: this.name,
                        size: this.size,
                        type: this.type,
                        file: this.file
                    };
                    data.submit();

                });
            },
            submit: function (e, data) {
                var rid = $('#pd_uid').val();
                var memt = $('.memoform #nameoffile').val();
                var memr = $('.memoform #description').val();
                data.formData = {
                    paramsid: "documents",
                    fileparams: JSON.stringify(data.urlparam),
                    rid: rid,
                    mt: memt,
                    mr: memr
                };
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({
                    message: "Error: " + jqXHR.responseText,
                    delay: 6000
                });
            }
        });
    }
    //-------------------------------------------------------
    function uploadPITDocs() {
        //var dialog_html = "<div class=\"form\"><ul class=\"w100\">" +
        //    "<li><label for=\"fileName\">Name:</label><input type=\"text\" id=\"fileName\" /></li>" +
        //    "<li><label for=\"fileDescription\">Description:</label><textarea id=\"fileDescription\"></textarea></li>" +
        //    "<li><label for=\"fileUpload\">File:</label><input id=\"fileUpload\" type=\"file\" name=\"files[]\" class=\"fileUpload\">" + 
        //    "<input id=\"fileText\" class=\"fileText\" type=\"text\" disabled=\"disabled\"/></li>" +
        //    "</ul></div><div id=\"uploadProgress\" class=\"progressPanel\"><div class=\"progressBar\" style=\"width: 0%;\"></div></div>";

        var dialog_html = "<div class='upload-container'><div class='upload-header'>" +
            "<button id='btnAddfiles' class='btn btn-outline-primary'>Select Files</button><button id='btnClearList'>Clear List</button>" +
            "<input id='fileUpload' type='file' name='files[]' multiple style='display:none'></div>" +
            "<div class='memoform'><table><tr><td>In-Service Training Title</td><td><input type='text' id='pit_title' class='w200px'></td></tr>" +
            "</table></div>" +
            "<div id='fileList' class='upload-list'></div><div class='upload-footer'><div id='divLegend'>" +
            "<div class='totalfiles'><span>0</span> of <span>0</span> Files Uploaded</div></div></div></div>";

        $(".pit_div").html(dialog_html);

        var uploadItem = "<div class='upload-item'><div class='con'><table style='width:100%'>" +
            "<tr><td style='width:73%'><span class='pitname'></span> : <span class='filename'></span></td>" +
            "<td style='width:23%;text-align:right'><span class='size'></span></td>" +
            "<td style='width:2%'><a class='status-loading' style='display:none'>&nbsp;</a></td>" +
            "<td style='width:2%'><a class='status-done' onclick='Upload.remove()'>&nbsp;</a></td></tr>" +
            "<tr><td colspan='4' style='width:100%'><div class='prog'><div class='innerprog' style='width:0%'></div></div></td></tr></table></div></div>";

        //var $dialog = $("<div><div class=\"dialogContent\">" + dialog_html + "</div></div>").dialog({
        //    modal: true,
        //    title: "Upload File",
        //    width: 500,
        //    close: function () {
        //        $dialog.find("#fileupload").fileupload("destroy");
        //        initPITDocs();
        //        $dialog.dialog("destroy").remove();
        //    },
        //    buttons: {
        //        "Close": function () {
        //            $dialog.dialog("close");
        //        }
        //    }
        //});
        //$("#btnUpload").attr('disabled', 'disabled').removeClass('primaryBtn').addClass('primaryBtn-disabled');
        $("#fileUpload").on('change', function () {
            var itms = $('.upload-container .totalfiles span:eq(1)');
            if (parseInt(itms.text()) > 0)
                itms.text(parseInt(itms.text()) + this.files.length);
            else
                itms.text(this.files.length);
        });
        $('#btnAddfiles').on('click', function () {
            if ($('#pit_title').val() == '') {
                alert("Please enter In-Service Training Title.");
                return;
            } else {
                $("#fileUpload").trigger('click');
            }
        });
        $('#btnClearList').on('click', function () {
            $('#fileList').html('');
            $('.totalfiles').html("<span>0</span> of <span>0</span> Files Uploaded");
        });
        $("#pit_modal").find("#fileUpload").fileupload({
            url: "Admin/UploadPITDocs",
            dataType: "json",
            done: function (e, data) {
                data.context.find('a:eq(0)').hide();
                if (data.files.length > 0) {
                    var dat = data.result;
                    if (dat.result != 0) {
                        data.context.find('.innerprog').css('background', 'red');
                        var tt = $('<div/>').html(dat.message).text();
                        data.context.find('a:eq(1)').attr('class', 'status-warning').attr('title', tt).off('click');
                    } else {
                        data.context.find('a:eq(1)').attr('class', 'status-done').attr('title', 'File uploaded sucessfully.').off('click');
                        var itms = $('.upload-container .totalfiles span:eq(0)');
                        itms.text(parseInt(itms.text()) + 1);
                    }
                    return false;
                }

            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var item = data.context;
                if (item != null) {
                    var size = (data.loaded / 1000) + 'KB/' + (data.total / 1000) + 'KB';
                    item.find('span.size').text(size);
                }
                data.context.find('.innerprog').css("width", progress + "%");
                $('#pit_title').val("");
            },
            add: function (e, data) {
                var files = data.files;
                if (data.items == null)
                    data.items = [];

                //var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptedExts = ['doc', 'docx', 'pdf', 'xls', 'xslx', 'pptx', 'ppt', 'odt', 'ods', 'odp', 'sxw', 'stw', 'sdw',
                    'csv', 'txt', 'rtf', 'zip', 'mp3', 'wma', 'mpg', 'flv', 'avi', 'jpg', 'jpeg', 'png', 'gif'
                ];

                $.each(files, function (x, i) {
                    var err = "";
                    //validation
                    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    if ($.inArray(ext, acceptedExts) == -1)
                        err = "The file type of '." + ext + "' is not supported.";
                    if (this.size > 20000000)
                        err = "The file size limit exceeded the maximum of 20MB.";
                    //if (this.type.length == 0 || ext)
                    //    err = "The file type is not supported.";
                    //else if (this.type.length && !acceptFileTypes.test(this.type)) {
                    //    var ext = this.name.substring(this.name.lastIndexOf(".") + 1, this.name.length).toLowerCase();
                    //    err = "The file type of '." + ext + "' is not supported.";
                    //}
                    var name = truncate(this.name);
                    var size = (this.size / 1000) + 'KB';
                    var item = $(uploadItem).appendTo('#fileList');
                    var pname = $('#pit_title').val();
                    item.find('span.filename').text(name).attr('title', this.name);
                    item.find('span.size').text(size);
                    item.find('span.pitname').text(pname);
                    if (err != '') {
                        item.find('.innerprog').width('100%').css('background', 'red');
                        item.find('a:eq(1)').attr('class', 'status-warning').attr('title', err).off('click');
                        return true;
                    } else {
                        item.find('a:eq(0)').show();
                        item.find('a:eq(1)').attr('class', 'status-remove').attr('title', 'Cancel').off('click').on('click', function () {
                            item.remove();
                        });
                    }
                    data.context = item;
                    data.urlparam = {
                        name: this.name,
                        size: this.size,
                        type: this.type,
                        file: this.file
                    };
                    data.submit();

                });
            },
            submit: function (e, data) {
                var rid = $("#pit_uid").val();
                var pitt = $('.memoform #pit_title').val();
                data.formData = {
                    paramsid: "documents",
                    fileparams: JSON.stringify(data.urlparam),
                    rid: rid,
                    pt: pitt
                };
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.growlError({
                    message: "Error: " + jqXHR.responseText,
                    delay: 6000
                });
            }
        });
    }
    //-------------------------------------------------

    $('#DocumentToolbar1').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.upload')) {
            //uploadP_Document();
        } else if (q.is('.deldsp')) {
            var docid = $('#pdocGrid').jqGrid('getGridParam', 'selrow');
            if (!docid) return;
            if (!confirm("Are you sure you want to delete the selected row?")) return;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/PostGridData",
                data: JSON.stringify({
                    func: 'personal_document',
                    mode: 3,
                    data: {
                        docId: docid
                    }
                }),
                dataType: "json",
                success: function (m) {
                    if (m.result == 0) {
                        $.growlSuccess({
                            message: "Document has been deleted successfully!",
                            delay: 3000
                        });
                        initP_Documents();
                    } else
                        $.growlError({
                            message: m.message,
                            delay: 6000
                        });
                }
            });
        } else if (q.is('.refresh')) {
            initP_Documents();
        }
    });




    //========================================================

    //var initPITDocs = function () {
    //    if ($('#pGrid')[0] && $('#pGrid')[0].grid)
    //        $.jgrid.gridUnload('pGrid');
    //    var DGrid = $('#pGrid');

    //    var GURow = new Grid_Util.Row();
    //    var pageWidth = $("#pGrid").parent().width() - 100;

    //    var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //    if (!id) return;

    //    DGrid.jqGrid({
    //        url: "Admin/GetGridData?func=proof_inservice_training&param=" + id,
    //        datatype: 'json',
    //        postData: '',
    //        ajaxGridOptions: {
    //            contentType: "application/json",
    //            cache: false
    //        },
    //        //datatype: 'local',
    //        //data: dataArray,
    //        //mtype: 'Get',
    //        colNames: ['Id', 'Title', 'Uploaded By', 'Date Uploaded', 'Filename', 'Size', 'UUID', 'Type'],
    //        colModel: [{
    //            key: true,
    //            hidden: true,
    //            name: 'Id',
    //            index: 'Id'
    //        },
    //            {
    //                key: false,
    //                name: 'pit_title',
    //                index: 'pit_title',
    //                sortable: true,
    //                width: (pageWidth * (10 / 100)),
    //                formatter: "dynamicLink",
    //                formatoptions: {
    //                    onClick: function (rowid, irow, icol, celltext, e) {
    //                        var G = $('#pGrid');
    //                        G.setSelection(rowid, false);
    //                        var row = G.jqGrid('getRowData', rowid);
    //                        if (row) {
    //                            window.location.href = "Admin/DownloadPITDocs/?DOCID=" + row.UUID;
    //                        }
    //                    }
    //                }
    //            },
    //            {
    //                key: false,
    //                name: 'CreatedBy',
    //                index: 'CreatedBy',
    //                sortable: true,
    //                width: (pageWidth * (6 / 100))
    //            },
    //            {
    //                key: false,
    //                name: 'DateCreated',
    //                index: 'DateCreated',
    //                sortable: true,
    //                width: (pageWidth * (4 / 100))
    //            },
    //            {
    //                key: false,
    //                name: 'Filename',
    //                index: 'Filename',
    //                sortable: true,
    //                width: (pageWidth * (12 / 100)),
    //                formatter: "dynamicLink",
    //                formatoptions: {
    //                    onClick: function (rowid, irow, icol, celltext, e) {
    //                        var G = $('#pGrid');
    //                        G.setSelection(rowid, false);
    //                        var row = G.jqGrid('getRowData', rowid);
    //                        if (row) {
    //                            window.location.href = "Admin/DownloadPITDocs/?DOCID=" + row.UUID;
    //                        }
    //                    }
    //                }
    //            },
    //            {
    //                key: false,
    //                name: 'Size',
    //                index: 'Size',
    //                sortable: true,
    //                width: (pageWidth * (4 / 100)),
    //                formatter: function (cellvalue, options, row) {
    //                    return (cellvalue / 1000) + "KB";
    //                }
    //            },
    //            {
    //                key: false,
    //                hidden: true,
    //                name: 'UUID',
    //                index: 'UUID'
    //            },
    //            {
    //                key: false,
    //                name: 'Filetype',
    //                index: 'Filetype',
    //                sortable: true,
    //                width: (pageWidth * (10 / 100))
    //            }
    //        ],
    //        //pager: jQuery('#pager'),
    //        rowNum: 1000000,
    //        rowList: [10, 20, 30, 40],
    //        height: '100%',
    //        viewrecords: true,
    //        //caption: 'Care Staff',
    //        emptyrecords: 'No records to display',
    //        jsonReader: {
    //            root: 'rows',
    //            page: 'page',
    //            total: 'total',
    //            records: 'records',
    //            repeatitems: false,
    //            id: '0'
    //        },
    //        autowidth: true,
    //        multiselect: false,
    //        sortname: 'Id',
    //        sortorder: 'asc',
    //        loadonce: true,
    //        onSortCol: function () {
    //            fromSort = true;
    //            GURow.saveSelection.call(this);
    //        },
    //        loadComplete: function (data) {
    //            if (typeof fromSort != 'undefined') {
    //                GURow.restoreSelection.call(this);
    //                delete fromSort;
    //                return;
    //            }

    //            var maxId = data && data[0] ? data[0].Id : 0;
    //            if (typeof isReloaded != 'undefined') {
    //                $.each(data, function (k, d) {
    //                    if (d.Id > maxId) maxId = d.Id;
    //                });
    //            }
    //            if (maxId > 0)
    //                $("#pGrid").setSelection(maxId);
    //            delete isReloaded;
    //        }
    //    });
    //    DGrid.jqGrid('bindKeys');
    //    $('#userForm div.tabPanels:eq(0)').layout({
    //        center: {
    //            paneSelector: "#pit_Grid",
    //            closable: false,
    //            slidable: false,
    //            resizable: true,
    //            spacing_open: 0
    //        },
    //        north: {
    //            paneSelector: "#PIT_Toolbar",
    //            closable: false,
    //            slidable: false,
    //            resizable: false,
    //            spacing_open: 0
    //        },
    //        onresize: function () {
    //            resizeGridsP();
    //            //resizeGridHS();
    //        }
    //    });
    //    resizeGridsP();
    //}

    var resizeGridsP = (function () {
        var f = function () {
            var tabH = $('.tabs').height() + 26;
            var tBH = $('#PIT_Toolbar').height();
            $("#pGrid").jqGrid('setGridWidth', parseInt($('#diag').width()));
            $("#pGrid").jqGrid('setGridHeight', parseInt($('#diag').height()) - (tabH + tBH));
            //$('.ui-jqgrid-bdiv').eq(3).height($("#serviceGrid").height() - 25).width($("#serviceGrid").width());
            //console.log($("#sGrid").height())
        }
        setTimeout(f, 100);
        return f;
    });

    //$('#PIT_Toolbar').on('click', 'a', function () {
    //    var q = $(this);
    //    if (q.is('.upload')) {
    //        uploadPITDocs();
    //    } else if (q.is('.deldsp')) {
    //        var docid = $('#dGrid').jqGrid('getGridParam', 'selrow');
    //        if (!docid) return;
    //        if (!confirm("Are you sure you want to delete the selected row?")) return;
    //        $.ajax({
    //            type: "POST",
    //            contentType: "application/json; charset=utf-8",
    //            url: "Admin/PostGridData",
    //            data: JSON.stringify({
    //                func: 'proof_inservice_training',
    //                mode: 3,
    //                data: {
    //                    docId: docid
    //                }
    //            }),
    //            dataType: "json",
    //            success: function (m) {
    //                if (m.result == 0) {
    //                    $.growlSuccess({
    //                        message: "Document has been deleted successfully!",
    //                        delay: 3000
    //                    });
    //                    initDocuments();
    //                } else
    //                    $.growlError({
    //                        message: m.message,
    //                        delay: 6000
    //                    });
    //            }
    //        });
    //    } else if (q.is('.refresh')) {
    //        initPITDocs();
    //    }
    //});



    $("#ddlRole").change(function () {
        $(".checkbox-grid input")
            .removeAttr('checked')
            .removeAttr('disabled')
            .filter(function () {
                return $(this).val() == $('#ddlRole').val();
            }).attr('checked', true).attr('disabled', true);

        if ($('#ddlRole').val() == 1) {
            $("#dept").css("display", "none");
        } else { $("#dept").css("display", "block"); };
    });


    $("#search_emp_value").keyup(function () {
        var string = $(this).val().toLowerCase();
        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
    })

    $("#search_emp_value").keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    })

    $("#showall").on("click", function () {
        $(".parent").each(function (i, e) {
            $(this).show();
        });
    });

    $('#ftr1').change(function () {
        // var string = $(this :selected).text();
        var string1 = $('#ftr1 :selected').text();
        var string2, all;
        $('#ftr2').html("");
        if (string1 == "Role") {
            $('#ftr2').html(uniqrl.join(''));
            all = "Roles";
        } else if (string1 == "Department") {
            $('#ftr2').html(uniqdpt.join(''));
            all = "Departments";
        } else if (string1 == "Type") {
            $('#ftr2').html(uniqtyp.join(''));
            all = "Types";
        } else if (string1 == "Status") {
            $('#ftr2').html(uniqstt.join(''));
            all = "Status";
        }

        $("#ftr2").html($('#ftr2 option').sort(function (x, y) {
            return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
        }));
        $("#ftr2").prepend('<option>All ' + all + '</option>');
        $("#ftr2").get(0).selectedIndex = 0;
        $("#ftr2").find('option[value=null]').remove();

        $(".parent").each(function (i, e) {
            $(this).show();
        });
    });

    $('#ftr2').change(function () {
        // var string = $(this :selected).text();
        var string = $('#ftr2 :selected').text();
        var s = string.indexOf("All");
        if (s == 0) {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        } else {
            $(".parent").each(function (i, e) {
                var a = $(this).text();

                if (a.indexOf(string) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            })
        }
    })

    $(document).on("click", "#addnew", function () {
        //$(".form").clearFields();
        //$("#empForm").clearFields();
        removeEmpData();
        $("#emp_modal_lbl").text("Add New Employee");
        $("#txtId").removeAttr("readonly");
        $('#chkIsSysUser').attr('checked', 'checked');
        $("#employee_modal").modal("toggle");
        addUserMode = 1;

    });

    //Added for Exporting to Grid to Excel (-ABC)

    function fnExcelReport() {
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr><td  bgcolor='#87AFC6'></td><td  bgcolor='#87AFC6'>Employee</td><td  bgcolor='#87AFC6'>Department</td> </tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Employees.xls");
        }
        else                 //other browser not tested on IE 11
            a.id = 'ExcelDL';
        a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
        a.download = 'ALC_Employees' + '.xls';
        document.body.appendChild(a);
        a.click(); // Downloads the excel document
        document.getElementById('ExcelDL').remove();
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    });
    //============================

    load_users();

});


