﻿
$(document).ready(function () {
    $("#carestaffContent").layout({
        center: {
            paneSelector: "#shiftGrid",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#wrapToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
            , north__childOptions: {
                paneSelector: "#shiftToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            },
            center__childOptions: {
                paneSelector: "#raToolbar",
                closable: false,
                slidable: false,
                resizable: false,
                spacing_open: 0
            }
        },
        onresize: function () {
            resizeGrids();
        }
    });

    var resizeGrids = (function () {
        var f = function () {
            $('.ui-jqgrid-bdiv').eq(0).height($("#shiftGrid").height() - 25).width($("#shiftGrid").width());
            $('#grid').setGridWidth($("#shiftGrid").width(), true);
        }
        setTimeout(f, 100);
        return f;
    })();

    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
        //var rm = $(this).val();
        //if (rm == 2) {
        //    $('#ftrRole').show();
        //    $('#ftrText').hide();
        //} else {
        //    $('#ftrRole').hide();
        //    $('#ftrText').show();
        //}
    });
    $('#ftrRole,#ftrText').change(function () {
        var res = $('#ftrText').val();
        var rm = $('#ddlFilter').val();
        var rd = $('#ddlFilter :selected').text();

        var rl = $('#ftrRole :selected').text();

        var ftrs = {
            groupOp: "AND",
            rules: []
        };
        var col = 'CarestaffName';
        if (rm == 3)
            col = "StartTime";

        if (rl != 'All')
            ftrs.rules.push({ field: 'Role', op: 'eq', data: rl });
        if (res != '')
            ftrs.rules.push({ field: col, op: 'cn', data: res });


        $("#grid")[0].p.search = ftrs.rules.length > 0;
        $("#grid")[0].p.postData = ftrs.rules.length == 0 ? '' : $.extend($("#grid")[0].p.postData, { filters: JSON.stringify(ftrs) });
        $("#grid").trigger("reloadGrid");
    });

    var SearchShift = "";
    $("#txtShift").val(SearchShift).blur(function (e) {
        if ($(this).val().length == 0)
            $(this).val(SearchShift);
    }).focus(function (e) {
        if ($(this).val() == SearchShift)
            $(this).val("");
    }).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "Admin/Search",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: '{func: "shift", name:"' + request.term + '"}',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.id,
                            start: item.start,
                            end: item.end
                        }
                    }));
                }
            });
        },
        delay: 200,
        minLength: 0,
        focus: function (event, ui) {
            $("#txtShift").val(ui.item.label);
            $("#txtShiftId").val(ui.item.value);
            $("#lblSched").html("(" + ui.item.start + ' - ' + ui.item.end + ")");
            return false;
        },
        change: function (event, ui) {

        },
        select: function (event, ui) {
            return false;
        }
    }).on('focus', function () {
        $(this).autocomplete('search', $(this).val());
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a><span>" + item.label + ' - (' + item.start + ' - ' + item.end + ")</span></a>")
                .appendTo(ul);
    };

    $("#diag").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Shift",
        dialogClass: "calendarVisitDialog",
        open: function () {
        }
    });

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "carestaff_shift", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            if (!isValid) {
                return;
            }

            row = $('.form').extractJson();

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];

            $('.dayofweek').find('input[type="checkbox"]').each(function () {
                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun': dow.push('0'); break;
                        case 'Mon': dow.push('1'); break;
                        case 'Tue': dow.push('2'); break;
                        case 'Wed': dow.push('3'); break;
                        case 'Thu': dow.push('4'); break;
                        case 'Fri': dow.push('5'); break;
                        case 'Sat': dow.push('6'); break;
                    }
                }
            });

            if (dow.length == 0) {
                alert('Please choose day of week schedule.');
                return;
            }
            row.DaySchedule = dow.join(',');
        }
        var data = { func: "carestaff_shift", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    var showdiag = function (mode) {
        $("#diag").dialog("option", {
            width: 480,
            height: 240,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        var id = $('#grid').jqGrid('getGridParam', 'selrow');
                        row = $('#grid').jqGrid('getRowData', id);
                        doAjax(mode, row, function (m) {
                            if (m.result == 0)
                                $.growlSuccess({ message: "Shift " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
                            else if (m.result == -3) {
                                $.growlWarning({ message: "Cannot update carestaff shift because there are schedule tasks that are assigned to it.", delay: 6000 });
                                return;
                            }
                            $("#diag").dialog("close");
                            if (mode == 1) isReloaded = true;
                            else {
                                fromSort = true;
                                new Grid_Util.Row().saveSelection.call($('#grid')[0]);
                            }
                            $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
                        });
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag").dialog("close");
                }
            }
        }).dialog("open");
    }

    $('.toolbar').on('click', 'a', function () {
        var q = $(this);
        if (q.is('.edit')) {
            var G = $('#grid');
            var rowid = $('#grid').jqGrid('getGridParam', 'selrow');
            G.setSelection(rowid, false);

            var row = G.jqGrid('getRowData', rowid);
            if (row) {
                $(".form").clearFields();
                $("#lblSched").html('');
                $('#txtCarestaffName').val(row.CarestaffName);
                $('#txtCarestaffId').val(row.CarestaffId);
                $('#txtCSId').val(row.CSId);
                $('#txtShift').val(row.Shift);
                $('#txtShiftId').val(row.ShiftId);
                if (row.StartTime != "" && row.EndTime != "")
                    $("#lblSched").html("(" + row.StartTime + ' - ' + row.EndTime + ")");

                var dow = row.DaySchedule;
                $('.dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    switch (d.attr('name')) {
                        case 'Sun': setCheck(d, dow, '0'); break;
                        case 'Mon': setCheck(d, dow, '1'); break;
                        case 'Tue': setCheck(d, dow, '2'); break;
                        case 'Wed': setCheck(d, dow, '3'); break;
                        case 'Thu': setCheck(d, dow, '4'); break;
                        case 'Fri': setCheck(d, dow, '5'); break;
                        case 'Sat': setCheck(d, dow, '6'); break;
                    }
                });
                $('a.checkall').attr('data-toggle', 'cc').html('Check All Days');
                showdiag(2);
                //doAjax(0, row, function (res) {

                //    $(".form").mapJson(res);

                //});
            }
        }
    });
    var setCheck = function (el, str, idx) {
        if (str.indexOf(idx) != -1)
            el.prop('checked', true);
        else el.prop('checked', false);
    }
    $('a.checkall').click(function () {
        var dis = $(this);
        var dd = dis.attr('data-toggle');
        var dow = '0,1,2,3,4,5,6';
        if (dd != 'cc') {
            dow = '';
            dis.attr('data-toggle', 'cc');
            dis.html('Check All Days');
        } else {
            dis.attr('data-toggle', 'dd');
            dis.html('Uncheck All Days');
        }
        $('.dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            switch (d.attr('name')) {
                case 'Sun': setCheck(d, dow, '0'); break;
                case 'Mon': setCheck(d, dow, '1'); break;
                case 'Tue': setCheck(d, dow, '2'); break;
                case 'Wed': setCheck(d, dow, '3'); break;
                case 'Thu': setCheck(d, dow, '4'); break;
                case 'Fri': setCheck(d, dow, '5'); break;
                case 'Sat': setCheck(d, dow, '6'); break;
            }
        });

    });

    (function () {
        //var dataArray = [
        //    { Id: '1', Name: 'Suite 101', Description: 'Meeting facility', Level: 'First', Rate: '999.20' },
        //    { Id: '2', Name: 'Fancy 201', Description: 'Lounge', Level: 'Second', Rate: '534.20' },
        //    { Id: '3', Name: 'Lancy 900', Description: '', Level: 'First', Rate: '923.20' },
        //    { Id: '4', Name: 'Hancy 123', Description: '', Level: 'Third', Rate: '912.20' }

        //];

        var GURow = new Grid_Util.Row();
        $('#grid').jqGrid({
            url: "Admin/GetGridData?func=carestaff_shift&param=",
            datatype: 'json',
            postData: "",
            ajaxGridOptions: { contentType: "application/json", cache: false },
            //datatype: 'local',
            //data: dataArray,
            //mtype: 'Get',
            colNames: ['CSId', 'ShiftId', 'CarestaffId', 'Carestaff', 'Department', 'Shift', 'Start Time', 'End Time', 'Day Schedule', 'DaySchedule', 'Created By', 'Date Created'],
            colModel: [
              { key: true, hidden: true, name: 'CSId', index: 'CSId' },
              { key: true, hidden: true, name: 'ShiftId', index: 'ShiftId' },
              { key: true, hidden: true, name: 'CarestaffId', index: 'CarestaffId' },
              {
                  key: false, name: 'CarestaffName', index: 'CarestaffName'
                  , formatter: "dynamicLink", formatoptions: {
                      onClick: function (rowid, irow, icol, celltext, e) {
                          $("#lblSched").html('');
                          var G = $('#grid');
                          G.setSelection(rowid, false);
                          var row = G.jqGrid('getRowData', rowid);
                          if (row) {
                              $(".form").clearFields();
                              $('#txtCarestaffName').val(row.CarestaffName);
                              $('#txtCarestaffId').val(row.CarestaffId);
                              $('#txtCSId').val(row.CSId);
                              $('#txtShift').val(row.Shift);
                              $('#txtShiftId').val(row.ShiftId);
                              if (row.StartTime != "" && row.EndTime != "")
                                  $("#lblSched").html("(" + row.StartTime + ' - ' + row.EndTime + ")");
                              var setCheck = function (el, str, idx) {
                                  if (str.indexOf(idx) != -1)
                                      el.prop('checked', true);
                              }
                              var dow = row.DaySchedule;
                              $('.dayofweek').find('input[type="checkbox"]').each(function () {
                                  var d = $(this);
                                  switch (d.attr('name')) {
                                      case 'Sun': setCheck(d, dow, '0'); break;
                                      case 'Mon': setCheck(d, dow, '1'); break;
                                      case 'Tue': setCheck(d, dow, '2'); break;
                                      case 'Wed': setCheck(d, dow, '3'); break;
                                      case 'Thu': setCheck(d, dow, '4'); break;
                                      case 'Fri': setCheck(d, dow, '5'); break;
                                      case 'Sat': setCheck(d, dow, '6'); break;
                                  }
                              });
                              $('a.checkall').attr('data-toggle', 'cc').html('Check All Days');
                              showdiag(2);
                              //doAjax(0, row, function (res) {

                              //    $(".form").mapJson(res);

                              //});
                          }
                      }
                  }

              },
              { key: false, name: 'Role', index: 'Role' },
              { key: false, name: 'Shift', index: 'Shift' },
              { key: false, name: 'StartTime', index: 'StartTime' },
              { key: false, name: 'EndTime', index: 'EndTime' },
              { key: false, name: 'DayScheduleDisplay', index: 'DayScheduleDisplay' },
              { key: false, hidden: true, name: 'DaySchedule', index: 'DaySchedule' },
              { key: false, name: 'CreatedBy', index: 'CreatedBy' },
              { key: false, name: 'DateCreated', index: 'DateCreated' }
            ],
            //pager: jQuery('#pager'),
            rowNum: 1000000,
            rowList: [10, 20, 30, 40],
            height: '100%',
            viewrecords: true,
            //caption: 'Care Staff',
            emptyrecords: 'No records to display',
            jsonReader: { root: 'rows', page: 'page', total: 'total', records: 'records', repeatitems: false, id: '0' },
            autowidth: true,
            multiselect: false,
            sortname: 'Id',
            sortorder: 'asc',
            loadonce: true,
            onSortCol: function () {
                fromSort = true;
                GURow.saveSelection.call(this);
            },
            loadComplete: function (data) {
                if ($('#ftrRole').find('option').size() <= 0) {
                    var optsRm = [];

                    $.each(unique($.map(data, function (o) { return o.Role })), function () {
                        optsRm.push('<option>' + this + '</option>');
                    });
                    $('#ftrRole').html(optsRm.join(''));

                    $("#ftrRole").html($('#ftrRole option').sort(function (x, y) {
                        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                    }));
                    $("#ftrRole").prepend('<option>All</option>');
                    $("#ftrRole").get(0).selectedIndex = 0;
                }
                if (typeof fromSort != 'undefined') {
                    GURow.restoreSelection.call(this);
                    delete fromSort;
                    return;
                }

                var maxId = data && data[0] ? data[0].Id : 0;
                if (typeof isReloaded != 'undefined') {
                    $.each(data, function (k, d) {
                        if (d.Id > maxId) maxId = d.Id;
                    });
                }
                if (maxId > 0)
                    $("#grid").setSelection(maxId);
                delete isReloaded;
                //Grid.Settings.init();
            }
        });
        $("#grid").jqGrid('bindKeys');
    })();
    //$('#selt').chosen({ no_results_text: 'No results found!' })
    function unique(array) {
        return $.grep(array, function (el, index) {
            return index == $.inArray(el, array);
        });
    }

    //Added for Exporting to Grid to Excel (-ABC)
    function fnExcelReport() {
        $("td:hidden,th:hidden", "#grid").remove();
        var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Carestaff</td><td bgcolor='#87AFC6'>Department</td><td bgcolor='#87AFC6'>Shift</td><td bgcolor='#87AFC6'>Start Time</td><td bgcolor='#87AFC6'>End Time</td><td bgcolor='#87AFC6'>Day Schedule</td><td bgcolor='#87AFC6'>Created by</td><td bgcolor='#87AFC6'>Date Created</td> </tr><tr>";
        var textRange; var j = 0;
        tab = document.getElementById('grid'); // id of table


        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var a = document.createElement('a');

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "ALC_CarestaffShifts.xls");
        }
        else                 //other browser not tested on IE 11
            a.id = 'ExcelDL';
            a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            a.download = 'ALC_CarestaffShifts' + '.xls';
            document.body.appendChild(a);
            a.click(); // Downloads the excel document
            document.getElementById('ExcelDL').remove();
    }

    $('#lnkExportExcel').on('click', function () {
        //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
        fnExcelReport();
        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    });
    //=============
});