﻿var AgencySettingState = {
    orig: '',
    current: ''
};
var load_agency_info = function () {
    $.ajax({
        url: "Admin/GetGridData?func=settings&param=",
        datatype: 'json',
        type: "POST",
        contentType: "application/json",
        success: function (info) {
            a = info;
            $("#a_form").mapJson(a[0]);
            $(".btns").prop("disabled", true);

            setTimeout(function () { $(".loader").hide(); }, 500)
            AgencySettingState.orig = JSON.stringify($('#a_form').extractJson());
        }
    });
}

function validateEmail(e) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(e)) {
        return true;
    } else {
        return false;
    }
}
$(document).ready(function () {
    $(".loader").show();
    load_agency_info();
    
    $(document).on("click", "#cancel", function () {
        load_agency_info();
        $(".form-control").prop("disabled", true);
        $("#save").prop("disabled", true);
        $("input.error").css("border", "#ddd 1px solid");
        $("span.error").css("display", "none"); 
    });

    $(document).on("click", "#edit_agency", function () {
        $(".form-control").prop("disabled", false);
        $("#agencyName").prop("disabled", true);
        $(".btn").prop("disabled", false);

        $("#zip").forceNumericOnly();
        $("#phone").forceNumericOnly();
        $("#fax").forceNumericOnly();
        $("#lnumber").forceNumericOnly();
        $("#lphone").forceNumericOnly();

    });
    $(document).on("click", ".save", function () {
        
        var mode = 2;
        email = $("#agencyEmail").val();
        var isValid = $('#a_form').validate();
        var isValidEmail;

        if (($.trim($('#address1').val()) == "") || ($.trim($('#address2').val()) == "") || ($.trim($('#city').val()) == "") || ($.trim($('#state').val()) == "") || ($.trim($('#zip').val()) == "") || ($.trim($('#phone').val()) == "") || ($.trim($('#fax').val()) == "") || ($.trim($('#agencyEmail').val()) == "") || ($.trim($('#lname').val()) == "") || ($.trim($('#lnumber').val()) == "") || ($.trim($('#lphone').val()) == "")) {

            swal('Please fill in everything!', "", "warning");
      
        }

        if (isValid) {
            isValidEmail = validateEmail(email);
            if (isValidEmail) {
                row = $('#a_form').extractJson();
            } else {
                swal("Please provide a valid email.")
                $(".form-control").prop("disabled", false);
                $("#agencyName").prop("disabled", true);
            }
        } else return;

        AgencySettingState.current = JSON.stringify(row);
        if (AgencySettingState.current == AgencySettingState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }

        var data = { func: "settings", mode: mode, data: row }
     
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.result == 0) {
                    setTimeout(function () {
                        //toastr.success("Agency Information updated successfully!");
                        swal('Agency Information updated successfully!', "", "success");
                    }, 300);

                    $(".form-control").prop("disabled", true);
                    $(".btns").prop("disabled", true);
                    AgencySettingState.orig = JSON.stringify($('#a_form').extractJson());
                }
            }
        });
    });
});