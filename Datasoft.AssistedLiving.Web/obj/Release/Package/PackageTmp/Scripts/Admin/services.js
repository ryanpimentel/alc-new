﻿var serviceState = {
    orig: '',
    current: ''
};
function setHeight() {
    $('.dataTables_scrollBody').height($(window).height() - 500);
    if ($(window).width() < 1024) {
        $('#headerRoom').height($("#headerRoom .row").height() + 100);
    } else {
        $('#headerRoom').height($(window).height() - 140);
    }

    if ($(window).height() < 400) {
        $('.dataTables_scrollBody').height(200);
        $('#headerRoom').height($('#ServGrid').height() + 380);
    }

    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('#headerRoom').height($("#headerRoom .row").height() + 100);
        }else{
            $('#headerRoom').height($(window).height() - 140);
            $('.dataTables_scrollBody').height($(window).height() - 500);
        }

        if ($(window).height() < 400) {
            $('.dataTables_scrollBody').height(200);
            $('#headerRoom').height($('#ServGrid').height() + 380);
        }

        $('#srvcsGrd').DataTable().columns.adjust();
    })
    $('#srvcsGrd').DataTable().columns.adjust();

    var scroll = new PerfectScrollbar('.dataTables_scrollBody');
    $('#srvcsGrd').DataTable().columns.adjust();

}
$(document).ready(function () {
    $(".loader").show();
    var col_num = 0;
    var column_value="";
    var table_services= "";
    var load_services = function () {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=activity&param=",
            dataType: "json",
            async: false,
            success: function (d) {
               
                doc_html = " ";
                $.each(d, function (i, doc) {
              
                    doc_html += "<tr><td class=\"select-filter\"><a href='#' class='sname' id='" + doc.Id + "' data-target='#services_modal' data-toggle='modal' dur='" + doc.Duration + "' act='" + doc.Activity + "' dept='" + doc.Department + "' proc='" + doc.Proc + "'>" + doc.Activity + "</a></td>";
                    doc_html += "<td class=\"select-filter\">" + doc.Proc + "</td>";
                    doc_html += "<td class=\"select-filter\">" + doc.Duration + "</td>";
                    doc_html += "<td class=\"select-filter\">" + doc.Department + "</td>";
                    doc_html += "<td>" + (doc.CreatedBy == null? "N/A" : doc.CreatedBy) + "</td>";
                    doc_html += "<td>" + (doc.DateCreated == null? "N/A" : doc.DateCreated) + "</td>";
                    doc_html += "<td class='toolbar'><span class='dropdown'>\n<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='true'>\n<i class='la la-edit'></i>\n</a><div class='dropdown-menu dropdown-menu-right'>\n\n<a class='dropdown-item del doc_delete' href='#' id='" + doc.Id + "'><i class='la la-trash-o'></i> Delete</a></div>\n</span></td>";
                    doc_html += "</tr>";
                    //<td>" + doc.Description + "</td><td>" + doc.Size + "</td><td>" + doc.Filetype + "</td><td>" + doc.CreatedBy + "</td><td>" + doc.CreatedBy + "</td></tr>";


                })

                $("#srvcsGrd").DataTable().destroy();;
                $("#srvcs_btable").html(doc_html);
                // $("#srvcsGrd").DataTable();
                table_services = $("#srvcsGrd").DataTable({
                    // stateSave: true,
                    "scrollY": "500px",
                    "scrollX": true,
                    "scrollCollapse": true,
                    responsive: !0,
                    pagingType: "full_numbers",
                    select: true,
                    dom: 'lBfrtip',
                    buttons: [
                         {
                             extend: 'excelHtml5',
                             text: '<i class="la la-download"></i> Export to Excel',
                             title: 'ALC_Services',
                             className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                             init: function (api, node, config) {
                                 $(node).removeClass('dt-button')
                             }
                         }

                    ]
                });

                var optsRm = [];
                var act = [];
                var proc = [];
                var duration = [];

                //ACTIVITY FILTER
                $.each(unique($.map(d, function (o) { return o.Activity })), function () {
                    if (this != " ") {
                        var optionTxt = this;
                        var nwOption = "";
                        
                        if (optionTxt.length >45) {
                            nwOption = optionTxt.substring(0, 45);
                            act.push('<option>' + nwOption + "..." +'</option>');
                        } else {
                            act.push('<option>' + optionTxt + '</option>');
                        }
                    }
                });
                $('#ftrActivity').html(act.join(''));
                $("#ftrActivity").html($('#ftrActivity option').sort(function (x, y) {
                    return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                }));
                $("#ftrActivity").prepend('<option>All</option>');
                $("#ftrActivity").get(0).selectedIndex = 0;
                //ACTIVITY FILTER : ENDS HERE

                //PROCEDURE FILTER
                $.each(unique($.map(d, function (o) { return o.Proc })), function () {
                    if (this != " ") {
                        var optionText = this;
                        var newOption = "";

                        if (optionText.length > 45) {
                            newOption = optionText.substring(0, 42);
                            proc.push('<option>' + newOption + "..." + '</option>');
                        } else {
                            proc.push('<option>' + optionText + '</option>');
                        }
                       
                    }
                    
                });
                $('#ftrProc').html(proc.join(''));
                $("#ftrProc").html($('#ftrProc option').sort(function (x, y) {
                    return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                }));
                $("#ftrProc").prepend('<option>All</option>');
                $("#ftrProc").get(0).selectedIndex = 0;
                //PROCEDURE FILTER : ENDS HERE

                //DURATION FILTER
                $.each(unique($.map(d, function (o) { return o.Duration })), function () {
                    duration.push('<option>' + this + '</option>');
                });
                $('#ftrDuration').html(duration.join(''));
                $("#ftrDuration").html($('#ftrDuration option').sort(function (x, y) {
                    return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                }));
                $("#ftrDuration").prepend('<option>All</option>');
                $("#ftrDuration").get(0).selectedIndex = 0;
                //DURATION FILTER : ENDS HERE


                //DEPARTMENT FILTER
                $.each(unique($.map(d, function (o) { return o.Department })), function () {
                    optsRm.push('<option>' + this + '</option>');
                });
                $('#ftrRole').html(optsRm.join(''));
                $("#ftrRole").html($('#ftrRole option').sort(function (x, y) {
                    return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                }));
                $("#ftrRole").prepend('<option>All</option>');
                $("#ftrRole").get(0).selectedIndex = 0;
                //DEPARTMENT FILTER: ENDS HERE

                setTimeout(function () { $(".loader").hide(); }, 500)
            }
        });
    }
    var dropdown_ftr = function(i,data) {

        table_services.column(i)
           //  .search(data ? '^' + data + '$' : data, true, false)
             .search(data , true, false)
             .draw();
    };


    $('#addServices').on('click', function () {
        $('#services_modal').clearFields();
    });

    $('#srvcsGrd').on('click', ".sname", function () {
        var dept2;
        var dis = $(this);
        var rid = dis.attr('id');
        var act = dis.text();
        var dur = dis.attr('dur');
        var proc = dis.attr('proc');
        var dept = dis.attr('dept');
        if (dept == "Housekeeper") {
            dept2 = "Housekeeping";
        } else {
            dept2 = dept;
        }


        $('#Id').val(rid);
        $('#txtActivity').val(act);
        $('#txtProc').val(proc);
        $('#txtDuration').val(dur);
        //$("#services_modal #ddlDept option[value=" + roomNo + "]").attr('selected', 'selected');
        $("#services_modal #ddlDept option:contains(" + dept2 + ")").attr('selected', 'selected');
        serviceState.orig = JSON.stringify($('#services_modal').extractJson());
    });

    $('#srvcsGrd').on('click', " .doc_delete", function () {
        var did = $(this);
        var id = did.attr('id');

        doAjax(3, { Id: id }, function (m) {
            if (m.result == 0) {
                swal("Service deleted successfully!", "", "success");
            }

            $("#services_modal").modal("hide");
            setTimeout(function () {
                $("#services_1 a").trigger("click");
            }, 200);

        });
    });

    //$("#taskContent").layout({
    //    center: {
    //        paneSelector: "#activityGrid",
    //        closable: false,
    //        slidable: false,
    //        resizable: true,
    //        spacing_open: 0
    //    },
    //    north: {
    //        paneSelector: "#wrapToolbar",
    //        closable: false,
    //        slidable: false,
    //        resizable: false,
    //        spacing_open: 0
    //        , north__childOptions: {
    //            paneSelector: "#taskToolbar",
    //            closable: false,
    //            slidable: false,
    //            resizable: false,
    //            spacing_open: 0
    //        },
    //        center__childOptions: {
    //            paneSelector: "#raToolbar",
    //            closable: false,
    //            slidable: false,
    //            resizable: false,
    //            spacing_open: 0
    //        }
    //    },
    //    onresize: function () {
    //        resizeGrids();
    //    }
    //});

    //var resizeGrids = (function () {
    //    var f = function () {
    //        $('.ui-jqgrid-bdiv').eq(0).height($("#activityGrid").height() - 25).width($("#activityGrid").width());
    //        $('#grid').setGridWidth($("#activityGrid").width(), true);
    //    }
    //    setTimeout(f, 100);
    //    return f;
    //})();


    if (($('#ftrActivity :selected').text() == "All") && ($('#ftrProc :selected').text() == "All") && ($('#ftrDuration :selected').text() == "All") &&
        ($('#ftrRole :selected').text() == "All")) {
        load_services();
        setHeight();
    }
    $('#ftrActivity').change(function () {
        var act = $('#ftrActivity :selected').text();
        var index_a = $('#ftrActivity').prop("selectedIndex");

        if (act != "All") {
            col_num = 0;
            column_value = $('#ftrActivity').val();
            $("#ftrProc").get(0).selectedIndex = 0;
            $("#ftrDuration").get(0).selectedIndex = 0;
            $("#ftrRole").get(0).selectedIndex = 0;
            load_services();
            setHeight();
            dropdown_ftr(col_num, column_value);
            $('#ftrActivity').get(0).selectedIndex = index_a;
            var e = $('#ftrActivity :selected').text();
            if (e.length > 45) {
                $('#ftrActivity').text( substring(0, 43) + "...");
            }
            
            
        } else {
            load_services();
            setHeight();
        }

       
    });
    $('#ftrProc').change(function () {

        var proc = $('#ftrProc :selected').text();
        var index_p = $('#ftrProc').prop("selectedIndex");
        if (proc != "All") {
            col_num = 1;
            column_value = $('#ftrProc').val();
            $("#ftrActivity").get(0).selectedIndex = 0;
            $("#ftrDuration").get(0).selectedIndex = 0;
            $("#ftrRole").get(0).selectedIndex = 0;
            load_services();
            setHeight();
            dropdown_ftr(col_num, column_value);
            $('#ftrProc').get(0).selectedIndex = index_p;
            var a = $('#ftrProc :selected').text();
            if (a.length > 40) {
                $('#ftrProc').text(substring(0, 30) + "...");
            }

        } else {
            load_services();
            setHeight();
        }

    });

    $('#ftrDuration').change(function () {

        var dur = $('#ftrDuration :selected').text();
        var index_d = $('#ftrDuration').prop("selectedIndex");
        if (dur != "All") {
            col_num = 2;
            column_value = $('#ftrDuration').val();
            $("#ftrActivity").get(0).selectedIndex = 0;
            $("#ftrProc").get(0).selectedIndex = 0;
            $("#ftrRole").get(0).selectedIndex = 0;
            load_services();
            setHeight();
            dropdown_ftr(col_num, column_value);
            $('#ftrDuration').get(0).selectedIndex = index_d;
        } else {
            load_services();
            setHeight();
        }

        
    });
    $('#ftrRole').change(function () {
        var dep = $('#ftrRole :selected').text();
        var index_dep = $('#ftrRole').prop("selectedIndex");
        if (dep != "All") {
            col_num = 3;
            column_value = $('#ftrRole').val();
            $("#ftrActivity").get(0).selectedIndex = 0;
            $("#ftrProc").get(0).selectedIndex = 0;
            $("#ftrDuration").get(0).selectedIndex = 0;
            load_services();
            setHeight();
            dropdown_ftr(col_num, column_value);
            $('#ftrRole').get(0).selectedIndex = index_dep;
        } else {
            load_services();
            setHeight();
        }

       
    });

    //$("#diag").dialog({
    //    zIndex: 0,
    //    autoOpen: false,
    //    modal: true,
    //    title: "Service",
    //    dialogClass: "calendarVisitDialog",
    //    open: function () {
    //    }
    //});

    var doAjax = function (mode, row, cb) {
        //get record by id
        if (mode == 0) {
            var data = { func: "activity", id: row.Id };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Admin/GetFormData",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (m) {
                    if (cb) cb(m);
                }
            });
            return;
        }

        if ($('#txtDuration').val() == "00") $('#txtDuration').val("");
        //var isValid = $('.form').validate();
        //do other modes
        if (mode == 1 || mode == 2) {
            //if (!isValid) {
            //    return;
            //}
            var form = document.getElementById('service_modal_form');
            for (var i = 0; i < form.elements.length; i++) {
                if (form.elements[i].value === '' && form.elements[i].hasAttribute('required')) {
                    swal("<span>Fill in all required (<span style=\"color: red;\">*</span>) fields. </span>")
                    return false;
                }
            }
            
            row = $('#services_modal').extractJson();
        }
        var data = { func: "activity", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            async: false,
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }

    //var showdiag = function (mode) {
    //    $("#diag").dialog("option", {
    //        width: 480,
    //        height: 330,
    //        position: "center",
    //        buttons: {
    //            "Save": {
    //                click: function () {
    //                    var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //                    row = $('#grid').jqGrid('getRowData', id);
    //                    doAjax(mode, row, function (m) {
    //                        if (m.result == 0)
    //                            $.growlSuccess({ message: "Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });
    //                        $("#diag").dialog("close");
    //                        if (mode == 1) isReloaded = true;
    //                        else {
    //                            fromSort = true;
    //                            new Grid_Util.Row().saveSelection.call($('#grid')[0]);
    //                        }
    //                        $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    //                    });
    //                },
    //                class: "primaryBtn",
    //                text: "Save"
    //            },
    //            "Cancel": function () {
    //                $("#diag").dialog("close");
    //            }
    //        }
    //    }).dialog("open");
    //}

    //$('.toolbar').on('click', 'a', function () {
    //    var q = $(this);
    //    if (q.is('.add')) {
    //        $('#txtId,#txtActivity,#txtProc,#txtDuration').val('');
    //        showdiag(1);
    //    } else if (q.is('.del')) {
    //        var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //        if (id) {
    //            if (confirm('Are you sure you want to delete the selected row?')) {
    //                setTimeout(function () {
    //                    var row = $('#grid').jqGrid('getRowData', id);
    //                    if (row) {
    //                        doAjax(3, { Id: row.Id }, function (m) {
    //                            var msg = "Service deleted successfully!";
    //                            if (m.result == -3) //inuse
    //                            {
    //                                msg = "Service cannot be deleted because it is currently in use.";
    //                                $.growlWarning({ message: msg, delay: 6000 });
    //                            } else
    //                                $.growlSuccess({ message: msg, delay: 6000 });
    //                            //if no error or not in use then reload grid
    //                            if (m.result != -1 || m.result != -3)
    //                                $('#grid').trigger('reloadGrid');
    //                        });
    //                    }
    //                }, 100);
    //            }
    //        } else {
    //            alert("Please select row to delete.")
    //        }
    //    } else if (q.is('.unsign')) {
    //        var id = $('#grid').jqGrid('getGridParam', 'selrow');
    //        if (id) {
    //            var row = $('#grid').jqGrid('getRowData', id);
    //            if (confirm('Are you sure you want to de-activate "' + row.Name + '"?')) {
    //                setTimeout(function () {
    //                    if (row) {
    //                        doAjax(4, { Id: row.Id }, function (m) {
    //                            if (m.result == 0)
    //                                $.growlSuccess({ message: row.Name + " de-activated successfully!", delay: 6000 });
    //                        });
    //                    }
    //                }, 100);
    //            }
    //        } else {
    //            alert("Please select row to deactivate.")
    //        }
    //    }

    //});



    $('#addService').on('click', function () {
        $('#services_modal').clearFields();
    });


    $('#saveServices').on('click', function () {
        mode = "";
        row = $('#services_modal').extractJson();
        if (row.Id == "") {
            mode = 1;
        } else {
            mode = 2;
            serviceState.current = JSON.stringify($('#services_modal').extractJson());

            if (serviceState.current == serviceState.orig) {
                swal("There are no changes to save.", "", "info");
                $(".loader").hide();
                return;
            }
        }

        doAjax(mode, row, function (m) {
            if (m.result == 0) {
                swal("Service " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                serviceState.orig = JSON.stringify($('#services_modal').extractJson());
            }

            $("#services_modal").modal("hide");
            setTimeout(function () {
                $("#services_1 a").trigger("click");
            }, 200);

        });
    });

    function unique(array) {
        return $.grep(array, function (el, index) {
            return index == $.inArray(el, array);
        });
    }

    //$('#selt').chosen({ no_results_text: 'No results found!' })
    $('#txtDuration').mask('99');

    //Added for Exporting to Grid to Excel (-ABC)
    //function fnExcelReport() {
    //    $("td:hidden,th:hidden", "#grid").remove();
    //    var tab_text = "<table border='2px'><tr><td bgcolor='#87AFC6'>Activity</td><td bgcolor='#87AFC6'>Procedure</td><td bgcolor='#87AFC6'>Duration (Minutes)</td><td bgcolor='#87AFC6'>Department</td><td bgcolor='#87AFC6'>Created By</td><td bgcolor='#87AFC6'>Date Created</td> </tr><tr>";
    //    var textRange; var j = 0;
    //    tab = document.getElementById('grid'); // id of table


    //    for (j = 0 ; j < tab.rows.length ; j++) {
    //        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
    //        //tab_text=tab_text+"</tr>";
    //    }

    //    tab_text = tab_text + "</table>";
    //    tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");//remove if u want links in your table
    //    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    //    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    //    var ua = window.navigator.userAgent;
    //    var msie = ua.indexOf("MSIE ");
    //    var a = document.createElement('a');

    //    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    //    {
    //        txtArea1.document.open("txt/html", "replace");
    //        txtArea1.document.write(tab_text);
    //        txtArea1.document.close();
    //        txtArea1.focus();
    //        sa = txtArea1.document.execCommand("SaveAs", true, "ALC_Services.xls");
    //    }
    //    else                 //other browser not tested on IE 11
    //        a.id = 'ExcelDL';
    //        a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
    //        a.download = 'ALC_Services' + '.xls';
    //        document.body.appendChild(a);
    //        a.click(); // Downloads the excel document
    //        document.getElementById('ExcelDL').remove();

    //}

    //$('#lnkExportExcel').on('click', function () {
    //    //$('#grid').tableExport({ type: 'excel', escape: 'false', headers: true });
    //    fnExcelReport();
    //    $('#grid').setGridParam({ datatype: 'json' }).trigger('reloadGrid');
    //});
    //====================================


    load_services();
    setHeight();
});