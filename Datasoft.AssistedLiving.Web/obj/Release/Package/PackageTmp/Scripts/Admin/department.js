﻿var setHeight = function () {
    if ($(window).width() < 1024) {
        $('#DeptBody').height($(window).height() - 200);
        $('.dataTables_scrollBody').height($(window).height() - 565);
        $('#userForm #tabsviews').height($(window).height() - 400);
    } else {
        $('#DeptBody').height($(window).height() - 225);
        $('.dataTables_scrollBody').height($(window).height() - 600);
        $('#userForm #tabsviews').height($(window).height() - 320);
    }

    if ($(window).height() < 400) {  
        $('.dataTables_scrollBody').height(200);        
        $('#DeptBody').height($('#DeptTableDiv').height() + 100);
        $('#userForm #tabsviews').height(600);
    }


    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('#DeptBody').height($(window).height() - 200);
            $('.dataTables_scrollBody').height($(window).height() - 565);
            $('#userForm #tabsviews').height($(window).height() - 400);
        } else {
            $('#DeptBody').height($(window).height() - 225);
            $('.dataTables_scrollBody').height($(window).height() - 600);
            $('#userForm #tabsviews').height($(window).height() - 320);
        }

        if ($(window).height() < 400) {
            $('.dataTables_scrollBody').height(200);
            $('#DeptBody').height($('#DeptTableDiv').height() + 100);
            $('#userForm #tabsviews').height(600);
        }
    })

}


var SelectDepartments = function () {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=department&param=",
        dataType: "json",
        success: function (m) {

            var dept_tiles = "";
            $('#SelectDept').html('');

            if (m.length != 0) {

                $.each(m, function (i, v) {
                    $('#SelectDept').append($('<option>', {
                        value: this.Id,
                        text: this.Description
                    }));
                })

                $("#SelectDept").html($('#SelectDept option').sort(function (x, y) {
                    return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                }));
                $("#SelectDept").get(0).selectedIndex = 0;

            }
        }
    });
}


var idText = '';
var id = '';
var createTable = function () {
    $("#DeptTableBody").html("");
    id = $("#SelectDept").val();
    idText = $('#SelectDept option:selected').text()

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetGridData?func=department_user&param=" + id,
        dataType: "json",
        success: function (m) {
            var html_table = "";

            if (m.length != 0) {
                $.each(m, function (k, s) {

                    html_table += "<tr>";
                    html_table += "<td>" + s.Id + "</td>";
                    html_table += "<td>";
                    html_table += '<a href="#" class="employee_name" data-toggle=\"modal\" data-target=\"#employee_modal\" id="' + s.Id + '">';
                     html_table += s.Name + "</td>";
                    html_table += "<td>" + (s.Email != null ? s.Email : "") + "</td>";
                    html_table += "<td>" + (s.IsActive == 1 ? "Active" : "Inactive") + "</td>";
                    html_table += "<td>" + s.DateCreated + "</td>";
                    html_table += "<td>" + s.DateModified + "</td>";
                    html_table += "</tr>";
                    
                })
                $("#DeptTableBody").html(html_table);
                
                $("#DeptTable").DataTable({
                    "scrollX": true,
                    stateSave: true,
                    "scrollY": "true",
                    "scrollCollapse": true,
                    responsive: !0,
                    pagingType: "full_numbers",
                    dom: 'lBfrtip',
                    "ordering": false,
                    "bFilter": false,
                    "bInfo": false,
                    select: true,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            text: '<i class="la la-download"></i> Export to Excel',
                            title: 'ALC_Department_' + idText,
                            className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                            init: function (api, node, config) {
                                $(node).removeClass('dt-button')
                            }
                        }
                    ]
                });
                var scroll_dt = new PerfectScrollbar('.dataTables_scrollBody');                
                setHeight();
                $('#DeptTable').DataTable().columns.adjust();

            } else {
                swal("No employee under selected department.", "", "info");
                return;
            }

            setHeight();

            //$(".dept_delete").attr("id", id);
            //$(".dept_edit").attr("id", id);
        }
    });
}




$(document).ready(function () {
    var scroll = new PerfectScrollbar('#tabsviews');

    setHeight();
    var mode = 0;

    SelectDepartments();
    setTimeout(function () {
        createTable();
        $(".dept_delete").attr("id", id);
        $(".dept_edit").attr("id", id);
        $(".loader").hide();
    }, 1000);


    $("#SelectDept").on("change", function () {
        setTimeout(function () {
            $("#DeptTable").DataTable().destroy();
            createTable();
            $(".dept_delete").attr("id", id);
            $(".dept_edit").attr("id", id);
            setHeight();
        }, 300);
    });
    

    $(document).on("click", "#dept_add", function () {

        mode = 1;
        $('#dept_modal .form').clearFields();

    })

    $(document).on("click", ".dept_delete", function () {
        var id_del = $(this).attr("id");
        mode = 3;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                //swal(
                //  'Deleted!',
                //  'The department has been removed.',
                //  'success'
                //)
                var data = { func: "department", id: id_del };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/GetFormData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (s) {

                        if (s.IsSystemDefined == true) {
                            swal(
                              'Sorry!',
                              'A system defined department cannot be deleted.',
                              'error'
                            )
                        } else {
                            doAjax(mode, { Id: id }, function (m) {
                                var msg = s.Description + " deleted successfully!";
                                if (m.result == -3) //inuse
                                    swal(m.Description + " cannot be deleted", "It is currently in use.", "", "error");

                                //if no error or not in use then reload grid
                                if (m.result != -1 || m.result != -3) {
                                    toastr.success("Department " + s.Description + " was deleted successfully!");
                                    SelectDepartments();
                                    setTimeout(function () {
                                        $("#DeptTable").DataTable().destroy();
                                        createTable();
                                        $(".dept_delete").attr("id", id);
                                        $(".dept_edit").attr("id", id);
                                    }, 300);
                                }
                            });
                        }

                    }
                });
            }
        })

    })




    $(document).on("click", ".dept_edit", function () {
        mode = 2;
        var id_edt = $(this).attr("id");
        var data = { func: "department", id: id_edt };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('#dept_modal .form').mapJson(m);

            }
        });

    })

    $("#dept_save").click(function () {
        var form = document.getElementById('dept_form');
        for (var i = 0; i < form.elements.length; i++) {
            if (form.elements[i].value === '' && form.elements[i].hasAttribute('required')) {
                swal("Fill in all required fields.")
                return false;
            }
        }
        var row = $('#dept_modal .form').extractJson();

        doAjax(mode, row, function (m) {
            if (m.result == 0) {
                SelectDepartments();
                setTimeout(function () {
                    $("#DeptTable").DataTable().destroy();
                    createTable();
                    $(".dept_delete").attr("id", id);
                    $(".dept_edit").attr("id", id);
                }, 300);
                toastr.success("Department " + row.Description + "  was " + (mode == 1 ? "added" : "updated") + " successfully!");
                $("#dept_modal").modal("hide");
            } else {
                swal(m.message, "", "error");
            }
        });

    })

    var doAjax = function (mode, row, cb) {

        var data = { func: "department", mode: mode, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
    }


    $("#search_dept_value").keyup(function () {
        var string = $(this).val().toLowerCase();
        if (string == "") {
            $(".parent").each(function (i, e) {
                $(this).show();
            });
        }

        $(".parent").each(function (i, e) {
            var a = $(this).text().toLowerCase();

            if (a.indexOf(string) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
    })

    $("#showall").on("click", function () {
        $(".parent").each(function (i, e) {
            $(this).show();
        });
    });

    $("#tabsFormselect").change(function () {
        $('[href="' + $(this).find("option:selected").val() + '"]').tab('show');
    });

    $("#tabsSelect").change(function () {
        $('[href="' + $(this).find("option:selected").val() + '"]').tab('show');
    });

    $(document).on("click", ".close", function () {
       
        $("#SelectDept").removeAttr("disabled");
        $("#DeptTableDiv").find("select").removeAttr("disabled");
      
    })
    $(document).keyup(function (e) {
        if (e.keyCode === 27) $('.close').click();   // esc
    });
})

