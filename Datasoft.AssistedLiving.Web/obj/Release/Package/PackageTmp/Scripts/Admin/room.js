﻿var roomInfoState = {
    orig: '',
    current: ''
};
$(function () {
    $(".loader").show();
})
var rmId = "";
var rmName = "";
var settings = {
    startDate: null,
    endDate: null,
    appointments: []
};
var global_html = [];
var optsRes = [],
    optsCS = [''],
    optsDP = [''],
    optsStat = [''],
    optsTask = [''];
var calftr = "";
var startDateNew = "";
var endDateNew = "";
var filtercal = "";
var currentId = "";
var statt = "";
var roleID = "";
var responsible_person_email = "";
var tasks = [];
var current_cid = "";
var adminRoles = ["1", "9", "10", "11", "12", "13"];
var saveMode = "";
function ampm(e) {

    var s = e.split(":");

    if (s[0] < 12) {
        return e + " AM";
    } else if (s[0] > 12) {
        s[0] -= 12;

        return s[0] + ":" + s[1] + " PM";
    } else {
        return e + " PM";
    }
}
function generateTable(item, rowId, str_date2, class_icon, sched_time, count) {

   
    count = count || "00";
    var html4 = "";
    var ddate = str_date2.split("/");

    var dday = ddate[1];
    var carestff = "";


    //to determine comments in different days
    //date.count.resident_id
    var cid = ddate[0] + ddate[1] + ddate[2];
    cid += "." + count + "." + item.resident_id;


    var stat = (item.activity_status == 1 ? '<span class="m-badge  m-badge--success m-badge--wide">Completed</span>' : '<span class="m-badge m-badge--primary m-badge--wide">Not Completed</span>');

    if (item.carestaff_activity_id == null && item.timelapsed) {
        stat = '<span style="background:#d61b1b;color:white;font-size: 9px;letter-spacing: 1px;padding: 2px 4px;border-radius: 5px 5px;">Missed</span>';
        carestff = -1;
    } else if (item.carestaff_activity_id == null && item.timelapsed == false) {
        stat = '<span style="background:##00c5dc;color:white;font-size: 9px;letter-spacing: 1px;padding: 2px 4px;border-radius: 5px 5px;">N/A</span>';
    }
    
    if (item.reschedule_dt != null) {
        stat = '<span class="m-badge  m-badge--success m-badge--wide">Reassigned / Rescheduled</span>';
    }

    if (item.carestaff_activity_id != null) {
        carestff = item.carestaff_activity_id;
    } else {
        carestff = "0";
    };

    var did = carestff + "." + item.resident_id + "." + (item.carestaff_activity_id != null ? item.carestaff_activity_id : "0");

    var date_now = new Date(str_date2);
    var act_until = new Date(item.active_until);

    if (item.remarks == "Deactivated Task" && date_now > act_until) {
        stat = '<span class="m-badge m-badge--danger m-badge--wide">Deactivated</span>';
    }
    if (item.active_until == null || date_now < act_until) {
        html4 += "<tr id=\"" + item.activity_id + "\">";
        html4 += "<td>" + item.activity_desc + "</td>";
        html4 += "<td>" + item.activity_proc + "</td>";
        html4 += "<td>" + item.department + "</td>";
        html4 += "<td>" + (item.resident == null ? "" : item.resident) + "</td>";
        html4 += "<td>" + item.room + "</td>";
        html4 += "<td>" + item.carestaff + "</td>";



        html4 += "<td>" + str_date2 + " - " + item.start_time + "</td>";
        html4 += "<td>" + item.service_duration + "</td>";
        html4 += "<td>" + stat + "</td>";
        //html4 += "<td>" + (item.actual_completion_date != null ? item.actual_completion_date : "") + "</td>";
        //html4 += "<td>" + item.acknowledged_by + "</td>";
        if (item.active_until != null) {//deactivated
            html4 += "<td>" + (item.active_until == null ? "" : item.active_until) + "</td>";
            html4 += "<td>" + (item.acknowledgeBy == null ? "" : item.acknowledgeBy) + "</td>";
        } else if (item.activity_status == 1 && item.actual_completiondt != null) {//completed
            html4 += "<td>" + (item.actual_completiondt == null ? "" : item.actual_completiondt) + "</td>";
            html4 += "<td>" + item.acknowledged_by + "</td>";
        } else {//missed and other
            html4 += "<td>" + (item.completion_date == null ? "" : item.completion_date) + "</td>";
            html4 += "<td>" + item.acknowledged_by + "</td>";
        }
        html4 += "<td>" + item.remarks + "</td>";
        html4 += "</tr>";
    }

    tasks[cid] = item;
    return html4;
}
function generateWeeklySchedule(startDay, endDay, filtercal, rmName, focus) {

    focus = focus || "";
    tasks = [];
    var startWeek = new Date(startDay);
    var tempDate = new Date(startWeek);
    var html = "<table>";
    var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
    var months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
    var byTime;
    var html2 = "";
    var class_icon = "";

    var container = new Array();
    var data, data2;

    var endWeek = new Date(endDay);

    var str_date;
    var stringdate;
    var schedule_task;
    var sched_desc = "";
    var sched_time = "";
    var str_date2;
    var rowId;



    var status = "";
    var na = "<span class=\"na\">N/A</span>";
    var diff = Math.floor(endWeek.getTime() - startWeek.getTime());
    var day = 1000 * 60 * 60 * 24;

    var days_difference = Math.floor(diff / day);

    for (var i = 0; i <= days_difference; i++) {

        tempDate = new Date(startWeek);
        tempDate.setDate(startWeek.getDate() + parseInt(i));
        tempDate.setHours(0, 0, 0, 0);
        getCellDesc(tempDate);
        container = [];

        byTime = global_html.slice(0);
        $.each(byTime, function (i, item) {
            data = {
                'appointment_id': item.appointment_id,
                'activity_id': "",
                'carestaff': item.assigned_staff,
                'type': 'transpo',
                'resident_id': item.resident_id,
                'resident': item.name,
                'start': item.start_time,
                'end_time': item.end_time,
                'room': item.room,
                'activity_desc': "",
                'activity_status': "",
                'acknowledged_by': "",
                'carestaff_activity_id': "",
                'timelapsed': "",
                'department': item.department,
                'remarks': item.remarks,
                'service_duration': item.service_duration,
                'start_time': item.start_time

            };

            if (data != "undefined") {
                container.push(data);
            }
        })

        str_date2 = (tempDate.getMonth() + 1).toString() + "/" + tempDate.getDate().toString() + "/" + tempDate.getFullYear().toString();

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Staff/GetGridData?func=activity_schedule&param=" + str_date2,
            dataType: "json",
            async: false,
            success: function (m) {
                // console.log(m);
                schedule_task = m;


            }
        });

        $.each(schedule_task, function (i, item) {

            if (item.active_until != null && item.remarks == "") {
                item.remarks = "Deactivated Task";
            }
            if (rmName == item.room) {

                data2 = {
                    'activity_id': item.activity_id,
                    'appointment_id': item.activity_schedule_id,
                    'type': 'dailytask',
                    'resident_id': item.resident_id,
                    'resident': item.resident,
                    'start': item.start,
                    'carestaff': item.carestaff_name,
                    'activity_desc': item.activity_desc,
                    'activity_proc': item.activity_proc,
                    'room': item.room,
                    'activity_status': item.activity_status,
                    'acknowledged_by': item.acknowledged_by,
                    'carestaff_activity_id': item.carestaff_activity_id,
                    'timelapsed': item.timelapsed,
                    'end_time': "",
                    'department': item.department,
                    'remarks': item.remarks,
                    'service_duration': item.service_duration,
                    'start_time': item.start_time,
                    'activity_proc': item.activity_proc,
                    'completion_date': item.completion_date,
                    'active_until' : item.active_until,
                    'actual_completiondt': item.actual_completiondt,
                    'acknowledgeBy': item.acknowledgeBy,
                    'reschedule_dt': item.reschedule_dt

                };

                if (data2 != "undefined") {
                    container.push(data2);
                }
            }

        })
        container.sort(function (a, b) {
            var d = a.start.split(":");
            var e = d[0] + "" + d[1];

            var f = b.start.split(":");
            var g = f[0] + "" + f[1];
            return e - g;
        });
        str_date = (tempDate.getMonth() + 1).toString() + "_" + tempDate.getDate().toString() + "_" + tempDate.getFullYear().toString();

        html2 = "";
        var count = 1;

        $.each(container, function (i, item) {
            sched_time = ampm(item.start);

            rowId = item.type + "" + item.appointment_id + "" + item.resident_id;

            if (item.resident_id == null) {
                item.type = "largetask";
                class_icon = "largetask_icon";

            } else {

                if (item.type == "dailytask") {
                    class_icon = "dailytask_icon";
                } else {
                    sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                    item.activity_desc = "Transportation Schedule";
                    class_icon = "transpo_icon";
                }
            }


            html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count);

            count++;
        });

        html += html2;

    }

    html += "</table>";

    $("#hGrid_body").html(html);

}
var buildCalendar = function () {
    var date = new Date(),
        n = date.getMonth(),
        y = date.getFullYear();

    var month = n + 1;
    var year = y;
    minDate = getMonthDateRange(year, month);
    buildCal(minDate);
}
function buildCal(date) {

    var facilityId = null,
        roomId = null;
    var fr = "";

    var data = {
        startDate: date.start,
        endDate: date.end
    };

    $.ajax({
        url: "Transsched/GetAdmissionCalendar",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        data: JSON.stringify(data),
        success: function (response) {
            if (response) {
                Calendar.Build({
                    startDate: data.startDate,
                    endDate: data.endDate,
                    appointments: response.Appointments
                });
            }
        }
    });

}
var getCellDesc = function (d) {
    //debugger
    global_html = [];
    var html = "";
    var string = "";
    var string2 = "";
    var recurrence, cl;
    var currentdate = new Date(d);
    var end;

    var byTime = settings.appointments.slice(0);

    byTime.sort(function (a, b) {
        return a.start_time - b.start_time;
    });
}
function getMonthDateRange(year, month) {
    // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
    // array is 'year', 'month', 'day', etc\

    var startDate = moment([year, month - 1]);

    // Clone the value before .endOf()
    var endDate = moment(startDate).endOf('month');

    // make sure to call toDate() for plain JavaScript date type
    return {
        start: startDate,
        end: endDate
    };
}
var Calendar = (function () {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var cellClasses = {
        1: "typeCont",
        2: "typeGen",
        3: "typeInp",
        4: "typeRout"
    };
    var icons = {
        "551": "snp.png",
        "421": "ptp.png",
        "431": "otp.png",
        "441": "stp.png",
        "561": "swp.png",
        "571": "hhap.png"
    };

    var selectedDate = "";


    var init = function (options) {

        if (options.startDate) settings.startDate = new Date(options.startDate);
        if (options.endDate) settings.endDate = new Date(options.endDate);
        if (options.appointments) {

            settings.appointments = [];
            $.each(options.appointments, function (i, item) {

                var end = null;

                if (item.end_date != null) {
                    end = new Date(item.end_date);
                }

                var loc = {
                    //admission_id: item.admission_id, resident_id: item.resident_id, name: item.name, admission_date: new Date(item.admission_date)
                    appointment_id: item.transpo_id,
                    resident_id: item.resident_id,
                    name: item.name,
                    start_date: new Date(item.date),
                    start_time: item.start_time,
                    end_time: item.end_time,
                    recurrence: item.recurrence,
                    day_of_the_week: item.day_of_the_week,
                    day_of_the_month: item.day_of_the_month,
                    end_date: end,
                    assigned_staff: item.staff_id,
                    room: item.room_name,
                    isAdmitted: item.isAdmitted
                };
                settings.appointments.push(loc);
            });
        }

    }

    var limit = function (val, max, step) {
        return val < max ? val - step : max;
    };

    //show schedule
    var renderCellHtml = function (w, d) {
        var html = "<td><table";
        if (d != null) html += " id=\"" + (d.getMonth() + 1) + "_" + d.getDate() + "_" + d.getFullYear() + "\"";
        html += " class=\"planBlock";
        if (d != null) html += " day";
        html += "\"><tr><td class=\"date\">";
        //if (w != null) html += w;
        if (d != null) html += months[d.getMonth()] + " " + d.getDate();
        html += "</td></tr><tr><td class=\"planned\"><div>";
        if (d != null) html += getCellDesc(d);
        if (w != null) html += "";
        html += "</div></table></td>";

        return html;
    }

    var renderBodyHtml = function () {

        var html = "<tr>";
        for (var i = 0; i < settings.startDate.getDay() ; i++) {
            //if (i == 0) html += renderCellHtml(1);
            html += renderCellHtml();
        }

        var day = settings.startDate;
        var week = 1;
        for (; day <= settings.endDate;) {
            //if (day.getDay() == 0) html += renderCellHtml(week);
            html += renderCellHtml(null, day);
            if (day.getDay() == 6) {
                html += "</tr><tr>";
                week += 1;
            }
            day.setDate(day.getDate() + 1);
        }

        for (var i = settings.endDate.getDay() ; i < 6; i++) {
            html += renderCellHtml();
        }
        html += "</tr>"

        return "<tbody>" + html + "</tbody>";
    }

    var renderTableHtml = function () {
        var html = "<table>";
        // html += renderHeaderHtml();
        html += renderBodyHtml();
        html += "</table>";

        return html;
    }
    var selectedTabIndex = 0;


    return {
        Build: function (options) {

            init(options);
            if (settings.startDate == null && settings.endDate == null) return;
            var str = "";
            str += renderTableHtml();



            var events = [];
            $(str).find('a').each(function () {
                if ($(this).attr("appointment_type") == "transpo") {
                    var name = $(this).attr('resname');
                    var appointment_id = $(this).attr('appointmentid');
                    var id = $(this).attr('id');
                    var recurrence = $(this).attr('recurrence');

                    //recurrence

                    var color = "";
                    if (recurrence == "daily") {
                        color = "#34bfa3c9";
                    } else if (recurrence == "weekly") {
                        color = "#24dc006b";
                    } else if (recurrence == "biweekly") {
                        color = "#00c5dcba";
                    } else if (recurrence == "quarterly") {
                        color = "#abf347de";
                    } else if (recurrence == "monthly") {
                        color = "#de4537db";
                    } else if (recurrence == "annually") {
                        color = "#fd7bb8";
                    } else { color = "#f45171c2"; }


                    //date
                    var edate = "";
                    var start_date = $(this).parents("table").attr("id");
                    var sdate = start_date.split("_");
                    start_date = sdate[2] + "-" + sdate[0] + "-" + sdate[1];

                    //time
                    var time = $(this).attr('time');
                    var t = time.split("-");
                    var start = ampm(t[0]);
                    var end = ampm(t[1]);

                    var st = "T" + t[0] + ":00";
                    var et = "T" + t[1] + ":00";
                    sdate = start_date + "" + st;
                    edate = start_date + "" + et;
                    sdate = moment(start_date, ["YYYY-MM-DDThh:mm:ss"]);
                    events.push({
                        title: start + "-" + end + " -  " + name,
                        name: name,
                        start: sdate,
                        date: start_date,
                        stime: start,
                        end: end,
                        appointment_id: appointment_id,
                        id: id,
                        allDay: false,
                        color: color
                        // time: time
                    });
                };
            });


        }



    }

})();
//----------------------------------------------------Service History until here----------------------------------------

//START: LOAD ROOM LIST
var load_rooms = function () {

    $.ajax({
        url: "Admin/GetGridData?func=room&param=",
        processData: false,
        dataType: "json",
        type: 'POST',
        cache: false,
        success: function (data, textStatus, jqXHR) {
            var html = "";

            $.each(data, function (i, v) {

                html += "<tr id=\"" + v.Id + "\" name=\"" + v.Name + "\" level=\"" + v.Level + "\" rmtype=\"" + v.RoomType + "\" rate=\"" + v.Rate + "\">";
                html += "<td><a id=\"" + v.Id + "\" name=\"" + v.Name + "\" class=\"room_row\" href='#'>" + v.Name + "</a></td>";
                html += "<td>" + (v.RoomType == 1 ? "Suite" : "Residence") + "</td>";
                html += "<td>" + v.Level + "</td>";
                html += "<td>" + (v.Rate == null ? "" : v.Rate) + "</td>";
                html += "</tr>";
            })

            $("#rm1").DataTable().destroy();
            $("#rm1_tbody").html(html);
            $("#rm1").DataTable({
                responsive: !0,
                "scrollX": true,
                "scrollY": true,
                "scrollCollapse": true,
                select: true
            });

        }
    });

}; //END: ROOM LIST

//START: LOAD RESIDENTS IN ROOMS
var loadRoomDetails = function (id) {

    $.ajax({
        url: "Admin/GetResidentByRoom?rid=" + id,
        processData: false,
        dataType: "json",
        type: 'POST',
        cache: false,
        success: function (data, textStatus, jqXHR) {
            var html2 = "";

            $.each(data, function (i, v) {
                html2 += "<tr>";
                html2 += "<td>" + v.Lastname + ", " + v.Firstname + " " + (v.MiddleInitial != "" ? v.MiddleInitial + "." : "") + "</td>";
                html2 += "<td>" + (v.Gender == "M" ? "Male" : "Female") + "</td>";
                html2 += "<td>" + (v.SSN != null ? v.SSN : "") + "</td>";
                html2 += "<td>" + v.Birthdate + "</td>";
                html2 += "</tr>";

            });
            $("#rm2").DataTable().destroy();
            $("#rm2_tbody").html(html2);
            $("#rm2").DataTable({
                responsive: !0,
                "scrollX": true,
                "scrollY": true,
                "scrollCollapse": true,
                select: true
            });

        }
    });
} //END: RESIDENTS IN PARTICULAR ROOM

//START: LOAD ACTIVITIES IN A PARTICULAR ROOM
var loadActivitySched = function (id) {

    $.ajax({
        url: "Admin/GetGridData?func=activity_schedule&param=-1&param4=" + id,
        processData: false,
        dataType: "json",
        type: 'POST',
        cache: false,
        success: function (data, textStatus, jqXHR) {
            var html3 = "";

            var dtnow = new Date(Date.now());




            $.each(data, function (i, v) {
                var nDate;
                if (v.ActiveUntil != null) {
                    nDate = new Date(v.ActiveUntil);
                }

                if ((v.IsActive == true) || ((v.IsActive == false) && (nDate > dtnow))) {
                    html3 += "<tr id=\"" + v.Id + "\">";
                    html3 += "<td><a id=\"" + v.Id + "\" class=\"rm_service\" href='#' data-target=\"#serviceTask_modal\" data-toggle=\"modal\">" + v.Activity + "</a></td>";
                    html3 += "<td>" + v.Department + "</td>";
                    html3 += "<td>" + v.Carestaff + "</td>";
                    html3 += "<td>" + v.Room + "</td>";
                    html3 += "<td>" + v.Schedule + "</td>";
                    html3 += "<td>" + v.Recurrence + "</td>";
                    html3 += "<td>" + v.Duration + "</td>";
                    html3 += "<td>" + v.DateCreated + "</td>";
                    html3 += "<td><a title=\"Delete\" class='m-portlet__nav-link btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill del delete' href='#' id='" + v.ActivityId + "'><i class='la la-trash-o'></i></a></td>"
                    html3 += "</tr>";
                }

            });

            $("#sGrid").DataTable().destroy();
            $("#sGrid_body").html(html3);
            $("#sGrid").DataTable({
                responsive: !0,
                "scrollX": true,
                "scrollY": true,
                "scrollCollapse": true,
                select: true
            });

        }
    });
};
//END: ACTIVITIES IN A PARTICULAR ROOM

//START: LOAD SERVICE HISTORY
var loadServiceHistory = function (id) {

    var removeByAttr = function (arr, attr, value) {
        var i = arr.length;
        while (i--) {
            if (arr[i]
                && arr[i].hasOwnProperty(attr)
                && (arguments.length > 2 && arr[i][attr] === value)) {

                arr.splice(i, 1);

            }
        }
        return arr;
    }
    function beforeProcessing(data, status, xhr) {
        var delList = [];
        if (data && data.length > 0) {
            //search for items with xref and check if it has correspoding acivity_schedule_id in the list
            //if exists then remove item with xref and preserve the record with activity_schedule_id = xref
            var occ = $.grep(data, function (a) { return a.xref > 0 });
            $.each(occ, function (x, y) {
                var matchObj = $.grep(data, function (b) {
                    return b.activity_schedule_id == y.xref;
                });
                if (matchObj.length > 0) {
                    delList.push(y);
                }
            });

            $.each(delList, function (i, a) {
                removeByAttr(data, 'activity_schedule_id', a.activity_schedule_id);
            });
        }
    }


    $.ajax({

        url: "Staff/GetGridData?func=activity_schedule&param=" + $('#fromDate').val() + "&param2=" + $('#toDate').val() + "&param4=" + id,
        beforeProcessing: function (data, status, xhr) { beforeProcessing(data, status, xhr) },
        datatype: 'json',
        postData: "",
        cache: false,
        success: function (data, textStatus, jqXHR) {
            var html4 = "";

            $.each(data, function (i, v) {

                var stat = '<span style="background:#d61b1b;color:white;font-size: 9px;letter-spacing: 1px;padding: 2px 4px;border-radius: 5px 5px;">Completed</span>';
                if (v.carestaff_activity_id == null && v.timelapsed) {
                    stat = '<span style="background:#d61b1b;color:white;font-size: 9px;letter-spacing: 1px;padding: 2px 4px;border-radius: 5px 5px;">Missed</span>';
                } else if (v.carestaff_activity_id == null && v.timelapsed == false) {
                    stat = '<span style="font-size: 9px;letter-spacing: 1px;padding: 2px 4px;border-radius: 5px 5px;">N/A</span>';
                }
                html4 += "<tr id=\"" + v.activity_id + "\">";
                html4 += "<td>" + v.activity_desc + "</td>";
                html4 += "<td>" + v.activity_proc + "</td>";
                html4 += "<td>" + v.department + "</td>";
                html4 += "<td>" + (v.resident == null ? "" : v.resident) + "</td>";
                html4 += "<td>" + v.room + "</td>";
                html4 += "<td>" + v.carestaff + "</td>";



                html4 += "<td>" + v.start_time + "</td>";
                html4 += "<td>" + v.service_duration + "</td>";
                html4 += "<td>" + stat + "</td>";
                html4 += "<td>" + (v.actual_completion_date != null ? v.actual_completion_date : "") + "</td>";
                html4 += "<td>" + v.acknowledged_by + "</td>";
                html4 += "<td>" + v.remarks + "</td>";
                html4 += "</tr>";

            });
            $("#hGrid").DataTable().destroy();
            $("#hGrid_body").html(html4);
            $("#hGrid").DataTable({
                responsive: !0,
                "scrollX": true,
                "scrollY": true,
                "scrollCollapse": true,
                select: true
            });

        }
    });
} //END: SERVICE HISTORY

// START: DATATABLES INITIALIZATION
var createDatatable = function () {

    $("#rm1").DataTable({
        saveState: true,
        responsive: !0,
        pagingType: "full_numbers",
        select: true,
        "scrollX": true,
        "scrollY": true,
        "scrollCollapse": true,
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="la la-download"></i> Export to Excel',
                title: 'ALC_Rooms',
                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            }
        ]

    });


    $("#rm2").DataTable({
        saveState: true,
        responsive: !0,
        pagingType: "full_numbers",
        select: true,
        "scrollX": true,
        "scrollY": true,
        "scrollCollapse": true,
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="la la-download"></i> Export to Excel',
                title: 'ALC_ResidentsPerRoom',
                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            }
        ]

    });

    $("#sGrid").DataTable({
        saveState: true,
        responsive: !0,
        pagingType: "full_numbers",
        select: true,
        "scrollX": true,
        "scrollY": true,
        "scrollCollapse": true,
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="la la-download"></i> Export to Excel',
                title: 'ALC_ServicesPerRoom',
                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            }
        ]

    });

    $("#hGrid").DataTable({
        saveState: true,
        responsive: !0,
        pagingType: "full_numbers",
        select: true,
        "scrollX": true,
        "scrollY": true,
        "scrollCollapse": true,
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="la la-download"></i> Export to Excel',
                title: 'ALC_ServiceHistory',
                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            }
        ]

    });
}
// END: DATATABLES INITIALIZATION



var doAjax = function (mode, row, cb) {

    
    //do other modes
    if (mode == 1 || mode == 2) {
        //var isValid = $('#room_form').validate();
        //if (!isValid) return;
        row = $('#room_form').extractJson();
    }

    var data = { func: "room", mode: mode, data: row };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/PostGridData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);
        }
    });
}

var doAjax2 = function (mode, row, cb) {

    //get record by id
    if (mode == 0) {
        var data = { func: "activity_schedule", id: row.Id };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });
        return;
    }

    
    //do other modes
    if (mode == 1 || mode == 2) {
        //var isValid = $('#serviceTask_modal').validate();
        //if (!isValid) {
        //    return;
        //}

        var form = document.getElementById('stModal_form');
        for (var i = 0; i < form.elements.length; i++) {
            if (form.elements[i].value === '' && form.elements[i].hasAttribute("required")) {
                swal("<span>Fill in all required (<span style=\"color: red;\">*</span>) fields. </span>")
                return false;
            }
        }

        row = $('#serviceTask_modal').extractJson();

        //validate time
        //var aTask = $('#serviceTask_modal #txtTimeInRM').val().split(":");
        //var aTime = aTask[0] + ":00 " + aTask[1];
        //if (aTask[0] < 10) {
        //    aTime = "0" + aTask[0] + ":" + aTask[1];
        //} else aTime = $('#serviceTask_modal #txtTimeInRM').val();



        row.ScheduleTime = $('#serviceTask_modal #txtTimeInRM').val();



        var dow = [];

        delete row['Sun'];
        delete row['Mon'];
        delete row['Tue'];
        delete row['Wed'];
        delete row['Thu'];
        delete row['Fri'];
        delete row['Sat'];
        delete row['recurring'];
        delete row['Activity'];
        delete row['Dept'];
        delete row['Onetimedate'];


        //debugger
        //var isrecurring = $('#serviceTask_modal input[name="recurring"]:eq(0)').attr('checked');
        var isrecurring = $('#serviceTask_modal #rm_reccur').is(':checked');
        if (isrecurring) {
            row.isonetime = '0';
            $('#serviceTask_modal .dayofweek').find('input[type="checkbox"]').each(function () {
                var d = $(this);
                if (d.is(':checked')) {
                    switch (d.attr('name')) {
                        case 'Sun': dow.push('0'); break;
                        case 'Mon': dow.push('1'); break;
                        case 'Tue': dow.push('2'); break;
                        case 'Wed': dow.push('3'); break;
                        case 'Thu': dow.push('4'); break;
                        case 'Fri': dow.push('5'); break;
                        case 'Sat': dow.push('6'); break;
                    }
                }
            });

            if (dow.length == 0) {
                swal('Please choose day of week schedule.');
                return;
            }



        } else {
            row.isonetime = '1';
            var onetimeval = $('#OnetimedateRM').val();
            if (onetimeval == '') {
                swal("Please choose a one time date schedule.");
                return;
            }

            var d = moment(onetimeval).format("MM/DD/YYYY");
            row.clientdt = d;

            var onetimeday = moment(onetimeval).day();
            if ($.isNumeric(onetimeday))
                dow.push(onetimeday.toString());

            //var aTask = $('#serviceTask_modal #txtTimeInRM').val().split(":");

            //var aTime = "";

            //if (aTask[0] < 10 && aTask[0].length == 1) {
            //    aTime = "0" + aTask[0] + ":" + aTask[1];
            //} else aTime = $('#txtTimeInRM').val();

            row.ScheduleTime = onetimeval + ' ' + $('#serviceTask_modal #txtTimeInRM').val();

            var today = new Date(Date.now());
            var new_schedTime = new Date(row.ScheduleTime);

            if (new_schedTime < today) {
                swal("Selected date and time have passed already. Please select again.", "", "warning");
                return;
            }
        }
        row.Recurrence = dow.join(',');

        //debugger

        if ($('#serviceTask_modal #txtCarestaffRM').val() == "") {
            swal("Please select a carestaff.")
            return;
        }
    }

    row.active_until = new Date(Date.now());
    var data = { func: "activity_schedule", mode: mode, data: row };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/PostGridData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);
        }
    });
}
var bindDept = function (dptid, cb) {
    var data = { deptid: dptid };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetServicesAndCarestaffByDeptId",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            $('#txtCarestaffRM').empty();
            $('<option/>').val('').html('').appendTo('#txtCarestaffRM');
            $.each(m.Carestaffs, function (i, d) {
                $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaffRM');
            });

            $('#txtActivityRM').empty();
            $('#viewActivityRM').val('');
            var opt = '<option val=""></option>';
            $.each(m.Services, function (i, d) {
                opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
            });
            $('#txtActivityRM').append(opt);
            if (typeof (cb) !== 'undefined') cb();
        }
    });
}

var toggleCarestafflist = function () {
    var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
    if (isrecurring)
        triggerDayofWeekClick();
    else
        $('#OnetimedateRM').trigger('change');

    //if housekeeper or maintenance hide room dropdown
    var val = $('#ddlDeptRM').val();
    if (val == 5 || val == 7) {
        $("#rm_room_section").show();
        $("#rm_resident_section").hide();
        //$('#liRoom, #liRoom select').show();
        //$('#liResident, #liResident select').hide();
    } else {
        $("#rm_room_section").hide();
        $("#rm_resident_section").show();
        //$('#liResident, #liResident select').show();
        //$('#liRoom, #liRoom select').hide();
    }
}
var triggerDayofWeekClick = function () {
    var a = $('#txtTimeInRM').val();
    if (a == null || a == '') {
        $('#txtCarestaffRM').empty();
        //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
        return;
    }

    var dow = [];
    $('.dayofweek').find('input[type="checkbox"]').each(function () {
        var d = $(this);
        if (d.is(':checked')) {
            switch (d.attr('name')) {
                case 'Sun': dow.push('0'); break;
                case 'Mon': dow.push('1'); break;
                case 'Tue': dow.push('2'); break;
                case 'Wed': dow.push('3'); break;
                case 'Thu': dow.push('4'); break;
                case 'Fri': dow.push('5'); break;
                case 'Sat': dow.push('6'); break;
            }
        }
    });
    bindCarestaff(dow);

}
var bindCarestaff = function (dow) {
    if ($('#txtTimeInRM').val() == '') {
        $('#txtCarestaffRM')[0].options.length = 0;
        return;
    }
    if ($('#txtActivityRM').val() == '') {
        $('#txtCarestaffRM')[0].options.length = 0;
        return;
    }
    if (dow == null) dow = [];
    var csid = $('input[xid="hdCarestaffId"]').val();
    if (csid == "") csid = null;

    var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');

    var data = { scheduletime: $('#txtTimeInRM').val(), activityid: $('#txtActivityRM').val(), carestaffid: csid, recurrence: dow.join(',') };
    data.isonetime = isrecurring ? "0" : "1";
    data.clientdt = isrecurring ? moment(new Date()).format("MM/DD/YYYY") : moment($('#OnetimedateRM').val()).format("MM/DD/YYYY");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetCarestaffByTime",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            $('#txtCarestaffRM').empty();
            $('<option/>').val('').html('').appendTo('#txtCarestaffRM');
            $.each(m, function (i, d) {
                if (d.Name != '') {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaffRM');
                }
            });
        }
    });
}
var AddService = function () {
    $("#ddlDeptRM").attr("disabled", true);
    $("#txtRoomIdRM").attr("disabled", true);
    $("#serviceTask_modal").clearFields();
    $("#serviceTask_modal input:checkbox").prop("checked", false);
    $('#tabs-2 #serviceGrid').clearFields();
    $("#rm_room_section").show();
    $("#rm_resident_section").hide();

    $("#ddlDeptRM").val(5); //Set to id of housekeeper on rooms services

    var id = $("#rm_form").val();
    if (!id) {
        id = $('#stModal_form #txtIdRM').val();
        if (!id) return;
    }

    $("#ddlDeptRM option[value='3']").remove();
    $("#ddlDeptRM option[value='4']").remove();
    $("#ddlDeptRM option[value='6']").remove();
    $("#ddlDeptRM option[value='8']").remove();
    $("#ddlDeptRM option[value='9']").remove();
    $("#ddlDeptRM option[value='10']").remove();
    $("#ddlDeptRM option[value='12']").remove();
    var dept = $('#ddlDeptRM').val();
    $('#ddlDeptRM').trigger('change');


    $('#serviceTask_modal input[xid="hdCarestaffId"]').val('');
    $('#serviceTask_modal input[name="recurring"]:eq(0)').attr('checked', 'checked');
    $('#serviceTask_modal input[name="recurring"]:eq(1)').removeAttr('checked');
    $('#serviceTask_modal #OnetimedateRM').attr('disabled', 'disabled');
    $('#serviceTask_modal .dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');

    $("#txtRoomIdRM").val(id);
    saveMode = 1;

}
$(document).ready(function () {
    var scroll = new PerfectScrollbar('#tabsviews');
    createDatatable();
    load_rooms();

    setTimeout(function () {
        $("#rm1").find("tbody tr:eq(0)").click();
        buildCalendar();
        $(".loader").hide();
    }, 1000);



    // START: ONCLICK FUNCTIONS

    //Load residents in each room (*right screen )
    $("#rm1_tbody").on("click", "tr", function () {

        $("#rm2").DataTable().destroy();

        $("#resident_dtable_lbl").html("<span>" + $(this).attr("name") + "</span>");
        id = $(this).attr("id");
        rmName = $(this).attr("name");
        loadRoomDetails(id);
    })

    $(document).on("click", ".room_row", function () {

        $("#room_modal").modal("toggle");
        $("#rm_form").find("li a").not(":eq(0)").show();
        $("#rm_information").click();

        var rm_name = $(this).attr("name");
        $("#room_name_lbl").html("<span>" + rm_name + "</span>");

        rmId = $(this).closest('tr').attr('id');
        var name = $(this).closest('tr').attr('name');
        var level = $(this).closest('tr').attr('level');
        var rmtype = $(this).closest('tr').attr('rmtype');
        var rate = $(this).closest('tr').attr('rate');
        //debugger
        $("#txtId").val(rmId);
        $("#rm_form").val(rmId);
        $("#txtName").val(name);
        $("#txtLevel").val(level);
        $("#ddlRoomType").val(rmtype);
        $("#txtRate").val((rate == "null" ? "" : rate));
        roomInfoState.orig = JSON.stringify($("#room_form").extractJson("name"));
    });

    $(document).on("click", "#services", function () {

        $("#sGrid_body").html("");
        loadActivitySched(rmId);
    });

    $(document).on("click", "#serviceHistory", function () {
        $("#hGrid_body").html("");
        $(".refresh").trigger('click');
        //loadServiceHistory(rmId);

    });


    //START: ADD NEW ROOM
    $(document).on("click", "#addRoomBtn", function () {

        rmId = 0;
        $("#room_name_lbl").html("Add Room");
        $("#room_name_lbl").val("addRoom");
        $("#txtId").val("");
        $("#rm_form").find("ul li a:eq(0)").click();
        $("#rm_form").find("li a").not(":eq(0)").hide();
        setTimeout(function () {
            $("#hGrid_body").html("");
            $("#sGrid_body").html("");
            $("#hGrid").DataTable().destroy();
            $("#sGrid").DataTable().destroy();

        }, 1000);

        $("#room_modal").modal("toggle");
        $("#userForm").clearFields();
        roomInfoState.orig = JSON.stringify($("#room_form").extractJson("name"));
    });
    //END: ADD NEW ROOM

    $("#addRoom").on("click", function () {


        var rm = $("#room_name_lbl").val();
        var mode = 2;

        if (rm == "addRoom") {
            mode = 1;
        }
        roomInfoState.current = JSON.stringify($("#room_form").extractJson("name"));

        if (roomInfoState.current == roomInfoState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }

        var row = $("#room_modal").extractJson("name");

        doAjax(mode, row, function (m) {

            if (m.result == 0) {
                swal("Room " + (mode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");
                $(".cancel").click();
                setTimeout(function () {
                    $(".m-menu__submenu ul li#rooms_1 a").click();
                }, 500);
                roomInfoState.orig = JSON.stringify($("#room_form").extractJson("name"));

            }

        });
    })


    $(document).on("click", ".rm_service", function () {
        $(".loader").show();
        var id = $(this).attr('id');
        $("#ddlDeptRM").attr("disabled", true);
        $("#txtRoomIdRM").attr("disabled", true);
        $("#serviceTask_modal").clearFields();
        $("#serviceTask_modal input:checkbox").prop("checked", false);
        $("#serviceTask_modal").modal("toggle");
        $("#new_service_task").val(id);
        if (id) {
            doAjax2(0, { Id: id }, function (res) {
                $("#ddlDeptRM").val(res.Dept);
                $('#ddlDeptRM').trigger('change');
                setTimeout(function () {
                    $("#serviceTask_modal").mapJson(res);
                    $("#ddlDeptRM").val(res.Dept);
                    $("#txtRoomIdRM").val(res.RoomId);
                },500);


                //bindDept(res.Dept.toString(), function () {
                //    debugger
                //    if (!res.IsOneTime)
                //        res.Onetimedate = '';

                //    $("#serviceTask_modal").mapJson(res);
                //});
                $("#ddlDeptRM option[value='4']").remove();
                $("#ddlDeptRM option[value='6']").remove();

                //if housekeeper or maintenance hide room dropdown
                //if (res.Dept == 5 || res.Dept == 7) {
                $("#rm_room_section").show();
                $("#rm_resident_section").hide();
                //} else {
                //    $("#rm_room_section").hide();
                //    $("#rm_resident_section").show();
                //}
                //map day of week
                if (!res.IsOneTime) {
                    $('input[name="recurring"]:eq(0)').attr('checked', 'checked');
                    if (res.Recurrence != null && res.Recurrence != '') {
                        var setCheck = function (el, str, idx) {
                            if (str.indexOf(idx) != -1)
                                el.prop('checked', true);
                        }
                        var dow = res.Recurrence;
                        $('.dayofweek').find('input[type="checkbox"]').each(function () {
                            var d = $(this);
                            switch (d.attr('name')) {
                                case 'Sun': setCheck(d, dow, '0'); break;
                                case 'Mon': setCheck(d, dow, '1'); break;
                                case 'Tue': setCheck(d, dow, '2'); break;
                                case 'Wed': setCheck(d, dow, '3'); break;
                                case 'Thu': setCheck(d, dow, '4'); break;
                                case 'Fri': setCheck(d, dow, '5'); break;
                                case 'Sat': setCheck(d, dow, '6'); break;
                            }
                        });
                        //debugger;
                        $('input[name="recurring"]:eq(0)').trigger('click');
                        $('#OnetimedateRM').val('');
                    }
                } else {
                    $('input[name="recurring"]:eq(1)').trigger('click');
                    res.Recurrence = moment(res.Onetimedate).day().toString();
                }


                //$('input[xid="hdCarestaffId"]').val(res.CarestaffId);
                setTimeout(function () {
                    var data = { scheduletime: res.ScheduleTime, activityid: res.ActivityId, carestaffid: res.CarestaffId, recurrence: res.Recurrence };
                    data.isonetime = !res.IsOneTime ? "0" : "1";
                    data.clientdt = !res.IsOneTime ? moment(new Date()).format("MM/DD/YYYY") : res.Onetimedate;
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Admin/GetCarestaffByTime",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (m) {

                            $('#txtCarestaffRM').empty();
                            $('<option/>').val('').html('').appendTo('#txtCarestaffRM');
                            $.each(m, function (i, d) {
                                $('<option/>').val(d.Id).html(d.Name).appendTo('#txtCarestaffRM');
                            });
                            $('#txtCarestaffRM').val(res.CarestaffId);
                            saveMode = 2;
                        }
                    });
                    $("#txtCarestaffRM").val(res.CarestaffId);
                    $(".loader").hide();
                }, 1000);
            });
        }

    });

    $(document).on("click", ".delete", function () {

        var id = $(this).closest("tr").attr("id");

        swal({
            title: "Are you sure you want to delete this data?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes"
        }).then(function (e) {
            e.value &&
                doAjax2(3, { Id: id }, function (m) {
                    var msg = "Task deleted successfully!";
                    $("#sGrid").DataTable().destroy();

                    if (m.result == -3) {
                        //inuse
                        msg = "Task cannot be deleted because it is currently in use.";

                    }

                    swal(msg, "", "success");

                    if (m.result != -1 || -3) {

                        $("#services").click();

                    }

                });
        });

    });

    // END: ONCLICK FUNCTIONS


    // START: SAVE & EDIT TASK

    $(".dtpicker").datepicker().on('changeDate', function () {
        $(this).datepicker('hide');
    });
    $('#txtActivityRM').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#viewActivityRM').val(title);
        $('#txtCarestaffRM')[0].options.length = 0;
        toggleCarestafflist();
    });

    $('#txtTimeInRM').timepicker({
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#txtCarestaffRM').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#OnetimedateRM').trigger('change');
        }
    });
    $('#txtTimeInRM').change(function () {
        if ($('#onetimeRM').is(':checked') == true && $('#OnetimedateRM').val() != "") {
            var dow = [];
            dow[0] = moment($('#OnetimedateRM').val()).day();
            bindCarestaff2(dow);
        } else if ($('#reccurRM').is(':checked') == true) {
            triggerDayofWeekClick();
        }

    });

    //$('#txtTimeInRM').mask('99:99 xy');

    if ($('#ddlDeptRM').length > 0) {
        $('#ddlDeptRM').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#txtCarestaffRM')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('input[name="recurring"]').click(function () {
        //   //debugger
        var d = $(this);
        if (d.val() == 1) {
            $('table.dayofweek input').removeAttr('disabled');
            $('#OnetimedateRM').attr('disabled', 'disabled').val('');
        } else {
            //debugger
            $('table.dayofweek input').attr('disabled', 'disabled');
            $('table.dayofweek input[type="checkbox"]').removeAttr('checked');
            // $('table.dayofweek').clearFields();
            $('#OnetimedateRM').removeAttr('disabled');
        }
    });

    $('table.dayofweek input').click(triggerDayofWeekClick);

    $('#OnetimedateRM').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#OnetimedateRM').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff(dow);
    });

    $('#AddServiceToolbar').on('click', 'a', function () {

        var q = $(this);
        if (q.is('.add')) {
            AddService();
        } else if (q.is('.refresh')) {
            $("#sGrid_body").html("");
            var id = $("#rm_form").val();
            loadActivitySched(id);
        }
    });

    $('#historyServiceToolbar').on('click', 'a', function () {

        var q = $(this);
        if (q.is('.resetSend')) {
            //to do
        } else if (q.is('.print')) {
            //do print
        } else if (q.is('.refresh')) {
            // initHistory();

            $("#hGrid_body").html("");
            $("#hGrid").DataTable().destroy();
            var id = $("#rm_form").val();
            var dateToday;
            $("#fromDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));
            $("#toDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));
            // loadServiceHistory(id);
            $(".generate").trigger('click');

        } else if (q.is('.generate')) {
            $(".loader").show();

            //var startDate = $("#fromDate").datepicker("getDate");
            //var endDate = $("#toDate").datepicker("getDate");

            var startDate = $("#fromDate").val();
            var endDate = $("#toDate").val();

            var n_startDate = new Date(startDate);
            var n_endDate = new Date(endDate);
            startDate = (n_startDate.getMonth()) + 1 + "/" + n_startDate.getDate() + "/" + n_startDate.getFullYear() + " 12:00:00 AM";
            endDate = $("#toDate").val() + " 12:00:00 AM";

            $("#hGrid_body").html("");
            $("#hGrid").DataTable().destroy();
            setTimeout(function () {
                generateWeeklySchedule(startDate, endDate, "All", rmName);
                $("#hGrid").DataTable({
                    responsive: !0,
                    "scrollX": true,
                    "scrollY": true,
                    "scrollCollapse": true,
                    select: true
                });
                $(".loader").hide();
            }, 500)

        }

    });


    $('#ddlFilter').change(function () {
        $('#ftrText').val('');
    });

    $("#txtRate").forceNumericOnly();

    // Start of Datepicker===============================================
    $('#fromDate').datepicker({
        dateFormat: 'mm-dd-yyyy',
        orientation: "bottom",
        todayHighlight: true,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"

    });

    // Start of Datepicker===============================================
    $('#toDate').datepicker({
        dateFormat: 'mm-dd-yyyy',
        orientation: "bottom",
        todayHighlight: true,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"

    });

    $("#fromDate,#toDate").val(moment(ALC_Util.CurrentDate).format("MM/DD/YYYY"));

    //SAve & EDIT TASKS

    $(document).on("click", "#new_service_task", function () {
        $(".loader").show();

        var id = $("#new_service_task").val();

        doAjax2(saveMode, { Id: id }, function (m) {
            if (m.result == 0) {

                swal("Task " + (saveMode == 1 ? 'saved' : 'updated') + " successfully!", "", "success");

                $("#close_st").click();


                loadActivitySched(rmId);

            } else if (m.result == -2) {
                var resVisible = $('#txtResidentId').is(':visible');

                //there is already a schedule for a particular resident or room
                //  setTimeout(function () {
                swal("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", "", "error");
                // }, 6000);

            }
            $(".loader").hide();
        });

    });

});