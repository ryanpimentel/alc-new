﻿var PointSettingState = {
    orig: '',
    current: ''
};
function setHeight() {
    $('.dataTables_scrollBody').height($(window).height() - 500);
    if ($(window).width() < 1024) {
        $('#headerRoom').height($("#headerRoom .m-portlet__body").height() + 100);
    } else {
        $('#headerRoom').height($(window).height() - 140);
    }
    if ($(window).height() < 400) {
        $('.dataTables_scrollBody').height(200);
        $('#headerRoom').height($('#score_table_wrapper').height() + 180);
    }
    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('#headerRoom').height($("#headerRoom .m-portlet__body").height() + 100);
        } else {
            $('#headerRoom').height($(window).height() - 140);
            $('.dataTables_scrollBody').height($(window).height() - 500);
        }
        if ($(window).height() < 400) {
            $('.dataTables_scrollBody').height(200);
            $('#headerRoom').height($('#score_table_wrapper').height() + 180);
        }
        $('#score_table').DataTable().columns.adjust();
    })

    var scroll = new PerfectScrollbar('.dataTables_scrollBody');
    $('#score_table').DataTable().columns.adjust();

}

$(function () {
    $(".loader").show();

    var scoreDatatable = $("#score_table").DataTable();

    var init = function () {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetGridData?func=pointssettings&param=" + $('#assessGroup').val(),
            dataType: "json",
            success: function (m) {
                var html_table = "";

                if (m.length != 0) {

                    $.each(m, function (i, v) {

                        html_table += "<tr>";
                        html_table += "<td>" + "<a href='#' class='edit_score' id='" + v.AssessmentPointSettingId + "' data-toggle='modal' data-target='#score_modal' fid='" + v.FieldId + "' ss='" + v.ScoreValue + "'>" + v.FieldText + "</a></td>";
                        html_table += "<td>" + v.ScoreValue + "</td>";
                        html_table += "</tr>";

                    })

                }

                $("#score_table").DataTable().destroy();
                $("#score_body").html(html_table);

                $("#score_table").DataTable({
                    "scrollY": true,
                    "scrollCollapse": true,
                    responsive: !0,
                    pagingType: "simple_numbers",
                    dom: 'lBfrtip',
                    buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="la la-download"></i> Export to Excel',
                        title: 'ALC_AssessmentPointSetting_' + $('#assessGroup').val(),
                        className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }]
                });

                setHeight();
                setTimeout(function () { $(".loader").hide(); }, 500)
            }
        })
    };


    init();


    $("#assessGroup").on("change", function () {
        init();
    })

    $(document).on("click", ".edit_score", function () {

        $("#AssessmentPointSettingId").val($(this).attr("id"));
        $("#FieldId").val($(this).attr("fid"));
        $("#FieldText").val($(this).text());
        $("#ScoreValue").val($(this).attr("ss"));
        PointSettingState.orig = JSON.stringify($('#score_modal .form').extractJson());
    })


    $("#update_score").click(function () {

        var row = $('#score_modal .form').extractJson();
        PointSettingState.current = JSON.stringify(row);

        if (PointSettingState.current == PointSettingState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }

        var data = { func: "pointssettings", mode: 2, data: row };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.result == 0) {
                    swal("Score updated successfully!", "", "success");
                    $("#score_modal").modal("hide");
                    init();
                }
            }
        });

    })

})