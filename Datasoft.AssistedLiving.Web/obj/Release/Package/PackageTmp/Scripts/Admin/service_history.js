﻿var settings = {
    startDate: null,
    endDate: null,
    appointments: []
};
var global_html = [];
var optsRes = [],
    optsCS = [''],
    optsDP = [''],
    optsStat = [''],
    optsTask = [''];
var calftr = "";
var startDateNew = "";
var endDateNew = "";
var filtercal = "";
var currentId = "";
var statt = "";
var roleID = "";
var tasks = [];
var current_cid = "";


var adminRoles = ["1", "9", "10", "11", "12", "13"];

function ampm(e) {

    var s = e.split(":");

    if (s[0] < 12) {
        return e + " AM";
    } else if (s[0] > 12) {
        s[0] -= 12;

        return s[0] + ":" + s[1] + " PM";
    } else {
        return e + " PM";
    }
}


function buildCal(date) {

    var facilityId = null,
        roomId = null;
    var fr = "";

    var data = {
        startDate: date.start,
        endDate: date.end
    };

    $.ajax({
        url: "Transsched/GetAdmissionCalendar",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        data: JSON.stringify(data),
        success: function (response) {
            if (response) {
                Calendar.Build({
                    startDate: data.startDate,
                    endDate: data.endDate,
                    appointments: response.Appointments
                });
            }
        }
    });

}

function generateTable(item, rowId, str_date2, class_icon, sched_time, count, resident_id) {

   
    count = count || "00";
    var html2 = "";
    var ddate = str_date2.split("/");

    var dday = ddate[1];
    var carestff = "";
  
    var status = item.activity_status == 1 ? '<span class="m-badge  m-badge--success m-badge--wide">Completed</span>' : '<span class="m-badge m-badge--primary m-badge--wide">Not Completed</span>';

    if (item.carestaff_activity_id == null && item.activity_status == 0)
        status = '<span class="m-badge  m-badge--metal m-badge--wide">N/A</span';

    if (item.carestaff_activity_id == null && item.timelapsed) {
        status = '<span class="m-badge m-badge--danger m-badge--wide">Missed</span>';
        carestff = -1;
    }
    
    if (item.reschedule_dt != null) {
        status = '<span class="m-badge  m-badge--success m-badge--wide">Reassigned / Rescheduled</span>';
    }
    if (item.carestaff_activity_id != null) {
        carestff = item.carestaff_activity_id;
    } else {
        carestff = "0";
    };

    var did = carestff + "." + item.resident_id + "." + (item.carestaff_activity_id != null ? item.carestaff_activity_id : "0");

    var date_now = new Date(str_date2);
    var act_until = new Date(item.active_until);
    //to determine comments in different days
    //date.count.resident_id
    var cid = ddate[0] + ddate[1] + ddate[2];
    cid += "." + count + "." + item.resident_id;
    
    if (item.remarks == "Deactivated Task" && date_now > act_until) {
        status = '<span class="m-badge m-badge--danger m-badge--wide">Deactivated</span>';
    }

    if (item.active_until == null || date_now < act_until) {
        html2 += "<tr>";
        html2 += "<td>" + item.activity_desc + "</td>";
        html2 += "<td>" + (item.activity_proc == null ? "" : item.activity_proc) + "</td>";
        html2 += "<td>" + item.department + "</td>";
        html2 += "<td>" + item.resident + "</td>";
        html2 += "<td>" + item.room + "</td>";
        html2 += "<td>" + item.carestaff + "</td>";
        html2 += "<td>" + str_date2 + "  " + item.start_time + "</td>";
        html2 += "<td>" + item.service_duration + "</td>";
        html2 += "<td>" + status + "</td>";
        
        if (item.active_until != null) {//deactivated
            html2 += "<td>" + (item.active_until == null ? "" : item.active_until) + "</td>";
            html2 += "<td>" + (item.acknowledgeBy == null ? "" : item.acknowledgeBy) + "</td>";
        } else if (item.activity_status == 1 && item.actual_completiondt != null) {//completed
            html2 += "<td>" + (item.actual_completiondt == null ? "" : item.actual_completiondt) + "</td>";
            html2 += "<td>" + item.acknowledged_by + "</td>";
        } else {//missed and other
            html2 += "<td>" + (item.completion_date == null ? "" : item.completion_date) + "</td>";
            html2 += "<td>" + item.acknowledged_by + "</td>";
        }

        html2 += "<td>" + item.remarks + "</td>";

        html2 += "</tr>";
    }
    tasks[cid] = item;
    return html2;

}

function generateWeeklySchedule(startDay, endDay, filtercal, focus) {

    focus = focus || "";
    tasks = [];
    var startWeek = new Date(startDay);
    var tempDate = new Date(startWeek);
    var html = "<table>";
    var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
    var months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
    var byTime;
    var html2 = "";
    var class_icon = "";

    var container = new Array();
    var data, data2;

    var endWeek = new Date(endDay);

    var str_date;
    var schedule_task;
    var sched_desc = "";
    var sched_time = "";
    var str_date2;
    var rowId;



    var status = "";
    var na = "<span class=\"na\">N/A</span>";
    var diff = Math.floor(endWeek.getTime() - startWeek.getTime());
    var day = 1000 * 60 * 60 * 24;

    var days_difference = Math.floor(diff / day);


    for (var i = 0; i <= days_difference; i++) {

        tempDate = new Date(startWeek);
        tempDate.setDate(startWeek.getDate() + parseInt(i));
        tempDate.setHours(0, 0, 0, 0);
        container = [];
        str_date2 = (tempDate.getMonth() + 1).toString() + "/" + tempDate.getDate().toString() + "/" + tempDate.getFullYear().toString();

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Staff/GetGridData?func=activity_schedule&param=" + str_date2,
            dataType: "json",
            async: false,
            success: function (m) {
                //console.log(m);
                schedule_task = m;

            }
        });

        $.each(schedule_task, function (i, item) {


            if (item.active_until != null && item.remarks == "") {
                item.remarks = "Deactivated Task";
            }

            if (currentId == item.resident_id && item.timelapsed == true) {
                data2 = {
                    'activity_id': item.activity_id,
                    'appointment_id': item.activity_schedule_id,
                    'type': 'dailytask',
                    'resident_id': item.resident_id,
                    'resident': item.resident,
                    'start': item.start,
                    'carestaff': item.carestaff_name,
                    'activity_desc': item.activity_desc,
                    'room': item.room,
                    'activity_status': item.activity_status,
                    'acknowledged_by': item.acknowledged_by,
                    'carestaff_activity_id': item.carestaff_activity_id,
                    'timelapsed': item.timelapsed,
                    'end_time': "",
                    'department': item.department,
                    'remarks': item.remarks,
                    'service_duration': item.service_duration,
                    'start_time': item.start_time,
                    'activity_proc': item.activity_proc,
                    'completion_date': item.completion_date,
                    'active_until' : item.active_until,
                    'actual_completiondt': item.actual_completiondt,
                    'acknowledgeBy': item.acknowledgeBy,
                    'reschedule_dt': item.reschedule_dt

                };

                if (data2 != "undefined") {

                    container.push(data2);
                }
            }
        })


        container.sort(function (a, b) {
            var d = a.start.split(":");
            var e = d[0] + "" + d[1];

            var f = b.start.split(":");
            var g = f[0] + "" + f[1];
            return e - g;
        });
        str_date = (tempDate.getMonth() + 1).toString() + "_" + tempDate.getDate().toString() + "_" + tempDate.getFullYear().toString();


        html2 = "";
        var count = 1;

        $.each(container, function (i, item) {
            sched_time = ampm(item.start);

            rowId = item.type + "" + item.appointment_id + "" + item.resident_id;
            if (filtercal != "All") {
                if (filtercal == "--") {
                    filtercal = "Housekeeper";
                }

                if (filtercal == "Transportation") {
                    filtercal = "transpo";
                }

                statt = item.activity_status == 1 ? 'Completed' : 'Not Completed';
                if (item.carestaff_activity_id == null && item.timelapsed) {
                    statt = "Missed";
                } else if (item.type == "transpo") {
                    statt = "";
                } else if (item.carestaff_activity_id == null && item.activity_status == 0) {
                    statt = "Not Completed";
                }

                if (item.resident_id == null) {
                    item.type = "largetask";
                    class_icon = "largetask_icon";

                } else {

                    if (item.type == "dailytask") {
                        class_icon = "dailytask_icon";
                    } else {
                        sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                        item.activity_desc = "Transportation Schedule";
                        class_icon = "transpo_icon";
                    }
                }

                if (item.resident == filtercal || item.carestaff == filtercal || item.department == filtercal || item.type == filtercal || statt == filtercal || item.activity_desc == filtercal) {

                    html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count);
                }

            } else {
                if (item.resident_id == null) {
                    item.type = "largetask";
                    class_icon = "largetask_icon";

                } else {

                    if (item.type == "dailytask") {
                        class_icon = "dailytask_icon";
                    } else {
                        sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                        item.activity_desc = "Transportation Schedule";
                        class_icon = "transpo_icon";
                    }
                }


                html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count);

            }

            count++;
        });

        html += html2;
    }

    html += "</table>";

    $(".loader").show();
    setTimeout(function () {
        $("#history_table").html(html);
        $(".loader").hide();
        $("#residentForm #hGrid tr").on("click", function () {
            $("#hGrid tr").css('background', 'white');
            $(this).css('background', '#c4efff');
        });
    }, 500)



}

var Calendar = (function () {

    var init = function (options) {

        if (options.startDate) settings.startDate = new Date(options.startDate);
        if (options.endDate) settings.endDate = new Date(options.endDate);
        if (options.appointments) {

            settings.appointments = [];
            $.each(options.appointments, function (i, item) {

                var end = null;

                if (item.end_date != null) {
                    end = new Date(item.end_date);
                }

                var loc = {
                    //admission_id: item.admission_id, resident_id: item.resident_id, name: item.name, admission_date: new Date(item.admission_date)
                    appointment_id: item.transpo_id,
                    resident_id: item.resident_id,
                    name: item.name,
                    start_date: new Date(item.date),
                    start_time: item.start_time,
                    end_time: item.end_time,
                    recurrence: item.recurrence,
                    day_of_the_week: item.day_of_the_week,
                    day_of_the_month: item.day_of_the_month,
                    end_date: end,
                    assigned_staff: item.staff_id,
                    room: item.room_name,
                    isAdmitted: item.isAdmitted
                };
                settings.appointments.push(loc);
            });
        }

    }

    var limit = function (val, max, step) {
        return val < max ? val - step : max;
    };

    return {
        Build: function (options) {
            init(options);
            if (settings.startDate == null && settings.endDate == null) return;
        }

    }
})();

$(document).ready(function () {
    $(".numeric_only").forceNumericOnly();


})