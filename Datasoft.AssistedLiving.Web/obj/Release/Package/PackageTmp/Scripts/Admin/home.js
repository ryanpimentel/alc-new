﻿$(document).ready(function () {

    if ($('#preflink').length == 0) {

        //$("#jsTreeView").append("<a id=\"preflink\" href=\"\" title=\"Preference\"><img src=\"Images/pref.png\" alt=\"\">Preference</a>");

        $("#jsTreeView").append("<a id=\"preflink\" href=\"\" title=\"Preference\"><img src=\"/ALC/Images/pref.png\" alt=\"\">Preference</a>");

        $('#preflink').click(function () {

            var $dialog_pref = $("<div><div id=\"prefCont\" ></div></div>").dialog({
                modal: true,
                closeOnEscape: false,
                title: "Preferences",
                width: 500,
                height: 200,
                close: function () {
                    $dialog_pref.dialog("destroy").remove();
                },
                buttons: {
                    "Save": {
                        click: function () {
                            var row = { Func: 'preferences', Data: { timezoneid: $('#ddlTz').val(), authTimeout: $('#authTimeout').val() } };
                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: ALC_URI.Admin + "/PostGridData",
                                data: JSON.stringify(row),
                                dataType: "json",
                                success: function (m) {
                                    if (m.result == 0) {
                                        $.growlSuccess({ message: "Preferences saved successfully!", delay: 6000 });
                                        setTimeout(function () {
                                            window.location.reload();
                                        }, 1500);
                                    } else if (m.result == -4) {
                                        $.growlWarning({ message: m.message, delay: 6000 });
                                        return;
                                    }
                                    $dialog_pref.dialog("destroy").remove();
                                }
                            });

                        },
                        class: "primaryBtn",
                        text: "Save"
                    },
                    "Cancel": function () {
                        $dialog_pref.dialog("destroy").remove();
                    }
                },
                resize: function (event, ui) {

                }
            });

            //sample how to retrieve data and populate to fields
            $.ajax({
                url: ALC_URI.Admin + "/GetNodeData",
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                type: "POST",
                data: '{id:"preferences_0"}',
                success: function (d) {
                    $('#prefCont').empty();
                    $('#prefCont').html(d);
                },
                complete: function () { },
                error: function () { }
            });
            return false;
        });
    }
});
    