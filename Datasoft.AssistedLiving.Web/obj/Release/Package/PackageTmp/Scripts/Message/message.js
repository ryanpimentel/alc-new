﻿var curent_cid ="";
var current_rid = "";
var res_name = "";
var message_container = [];
var one_to_nine = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
var ten_to_twelve = ["10", "11", "12"];

$(function () {
    

    //$(document).on("click", ".show_message", function () {
    //    debugger;
    //    $("#message").attr("cid", "");
    //    var id = $(this).attr("id");
    //    current_rid = id;
    //    $("#message").html("");
    //    $.ajax({
    //        url: 'Message/GetConversation',
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        type: "POST",
    //        data: id,
    //        success: function (d) {
    //            if (d.length != "") {
    //                message_container = [];
    //                $("#taskdetaillist").html("");
    //                var html = "";
    //                for (var i = 0; i < d.length; i++) {
    //                    var obj = JSON.parse(d[i]["g"].taskdetail);
                       
    //                    //add additional data in the future
    //                    var cid = d[i]["g"].cid;
    //                    //var datetask = cid.Spl

    //                    var month = cid.substring(0, 2);
    //                    var day = cid.substring(2, 4);
    //                    var year = cid.substring(4, 8);


    //                    //var datetimes = JSON.parse(d[i]["g"].comment);
    //                    //var timestamps = new Date(datetime.datetime);
    //                    if (d[i]["g"].read_status == "unread") {
    //                        html += "<tr style=\"font-weight: bold;\" class=\"taskdtl\" cid=\"" + d[i]["g"].cid + "\">";
    //                    } else {
    //                        html += "<tr class=\"taskdtl\" cid=\"" + d[i]["g"].cid + "\">";
    //                    }
    //                    //html += "<td>" + "DATE HERE! <br></td>";
    //                    html += "<td>" + month + "/" + day + "/" + year + "<br> " + obj.start_time + "</td>";
    //                    html += "<td>" + obj.activity_desc + "</td>";
    //                    html += "</tr>";
                       
    //                    //put all the comments in the container to avoid querying from the server again
    //                    message_container[d[i]["g"].cid] = d[i]["g"].comment;
                        
    //                }

    //                $("#taskdetaillist").append(html);
                    
    //            }
    //        }
    //    });
    //})

   
   
})

function parseComment(string) {
    var comment = "";
    var date = new Date();
    var timestamp;

    var obj = JSON.parse("[" + string + "]");
    var role = "";

    for (var i = 0; i < obj.length; i++) {
        role = obj[i].role == "Guest" ? "isGuest" : "isAdmin";

        timestamp = new Date(obj[i].datetime);
        //comment += "<span>" + (timestamp.getMonth() + 1) + "/" + timestamp.getDate() + "/" + timestamp.getFullYear() + " [" + timestamp.getHours() + ":" + timestamp.getMinutes() + "] - " + obj[i].userid + ":  " + obj[i].comment + "</span><br/>";
        comment += "<span>";
        comment += "<span style=\"font-weight:bold;font-size:12px\">" + obj[i].userid + " </span>" + "<span style=\"font-size:11px\">" + (timestamp.getMonth() + 1) + "/" + timestamp.getDate() + "/" + timestamp.getFullYear() + " [" + timestamp.getHours() + ":" + timestamp.getMinutes() + "]</span>";
        comment += "<br/>" + obj[i].comment + "</span><br/><br/>";

    }
    //comment = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "<br><br>" + comment
    comment = comment
    return comment;
}

var load_residents = function () {
    $.ajax({
        type: "POST",
        url: 'Message/GetResidentsWithComment',
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (d) {
            $("#resident_list_admin_side").html("");

            
            if (d.length != "") {
               // var string = "";

                for (var i = 0; i <= d.length - 1 ; i++) {
                    console.log(d[i]);
                    if (d[i].Unread_Count != 0) {
                        d[i].Name = d[i].Name + " (" + d[i].Unread_Count + ")";
                    }
                   
                    $('#resident_list_admin_side').append($('<option>', {
                        value: d[i].ResidentId,
                        text: d[i].Name
                    }));
                }


                //$("#resident_list_admin_side").append(string);
            }


        }
    });
    
}

$(document).ready(function () {

    var first_index = "";

    if ($(window).width() < 1024) {       
        $('#tmsgPanel').height($(window).height() - 160);
        $('#tdiv').height(($('#tmsgPanel').height() / 2) - 75);
        $('#mdiv').height(($('#tmsgPanel').height() / 2) - 90);
    } else {
        $('.tcontainer').height($(window).height() - 315);
    }

   
    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('.tcontainer').height(($('#tmsgPanel').height() / 2) - 60);
        } else {
            $('.tcontainer').height($(window).height() - 315);
        }
    })

    load_residents();
       
    $("select#resident_list_admin_side").prop('selectedIndex', 0);
    

    setTimeout(function () {
        first_index = $("select#resident_list_admin_side option:eq(0)").val();
        $("select#resident_list_admin_side")
            .val(first_index)
            .trigger('change');
    }, 1000);


    $('#resident_list_admin_side').on('change', function () {

        $("#message").attr("cid", "");
        var id = $(this).val();

        res_name = $("#resident_list_admin_side option:selected").text();
        current_rid = id;
        $("#message").html("");
        $("#ftr").show();
        $("#task_tbl").show();

        $.ajax({
            url: 'Message/GetConversation',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: id,
            success: function (d) {
                
                if (d.length != "") {
                    message_container = [];
                    $("#taskdetaillist").html("");
                    $("#date_commented").html("");
                    $("#task").html("");
                    var html = "";
                    var optDates = [''];
                    var optTasks = [''];
                    for (var i = 0; i < d.length; i++) {

                        var obj = JSON.parse(d[i]["g"].taskdetail);
                        
                        //add additional data in the future
                        var cid = d[i]["g"].cid;
                        console.log(d);
                        //var datetask = cid.Spl
                        var get_date = cid.split('.');
                        
                        var month = cid.substring(0, 2);
                        var day = cid.substring(2, 4);
                        var year = cid.substring(4, 8);

                        if (get_date[0].length == 6) {
                            month = cid.substring(0, 1);
                            day = cid.substring(1, 2);
                            year = cid.substring(2, 6);
                        }else if (get_date[0].length == 7) {
                            if (($.inArray(cid.substring(0,1), one_to_nine) !== -1) && ($.inArray(cid.substring(0, 2), ten_to_twelve) == -1)) {
                                month = cid.substring(0, 1);
                                day = cid.substring(1, 3);
                                year = cid.substring(3, 7);
                            } else {
                                month = cid.substring(0, 2);
                                day = cid.substring(2, 3);
                                year = cid.substring(3, 7);
                            }
                        }

                        
                        //var datetimes = JSON.parse(d[i]["g"].comment);
                        //var timestamps = new Date(datetime.datetime);
                        if (d[i]["g"].read_status == "unread") {
                            html += "<tr style=\"font-weight: bold;\" class=\"taskdtl\" cid=\"" + d[i]["g"].cid + "\">";
                        } else {
                            html += "<tr class=\"taskdtl\" cid=\"" + d[i]["g"].cid + "\">";
                        }
                        //html += "<td>" + "DATE HERE! <br></td>";
                        html += "<td>" + month + "/" + day + "/" + year + "<br> " + obj.start_time + "</td>";
                        html += "<td>" + obj.activity_desc + "</td>";
                        html += "</tr>";

                        //put all the comments in the container to avoid querying from the server again
                        message_container[d[i]["g"].cid] = d[i]["g"].comment;

                        

                        var dt = month + "/" + day +"/" + year;
                        //dropdown for the dates
                        if ($.inArray(dt, optDates) == -1) {
                             $('#date_commented').append($('<option>', {
                                value: cid,
                                text: dt
                            }));
                            optDates.push(dt);
                        }
                           
                        //dropdown for tasks
                        if ($.inArray(obj.activity_desc, optTasks) == -1) {
                            $('#task').append($('<option>', {
                                value: cid,
                                text: obj.activity_desc
                            }));
                            optTasks.push(obj.activity_desc);
                        }
                       
                    }
                    $("#date_commented").prepend('<option>All</option>');
                    $("#task").prepend('<option>All</option>');
                    $("#taskdetaillist").append(html);
                    $("select#date_commented").prop('selectedIndex', 0);
                    $("select#task").prop('selectedIndex', 0);
                }
            }
        });

    });
    $(document).on("click", ".taskdtl", function () {
        
        var cid = $(this).attr("cid");

        $(".taskdtl").children("td").css("background-color", "");
        $(this).children("td").css("background-color", "#E3F2FD");
        $(this).children("td").css("font-weight", "normal");


        var data = JSON.parse("[" + message_container[cid] + "]");
        var html = "";

        $("#message").attr("cid", cid);
        $("#message").html("");
        var date;
        var day;
        $(data).each(function (i, item) {
            
            date = new Date(item.datetime);
            day = item.datetime.substring(8, 10);

            html += "<span>"; //add class here to determine if guest or not **css purposes**
            html += "<span style=\"font-weight:bold;font-size:12px\">" + item.userid + " </span>" + "<span style=\"font-size:11px\">" + (date.getMonth() + 1) + "/" + day + "/" + date.getFullYear() + " [" + date.getHours() + ":" + date.getMinutes() + "] </span>";
            html += "<br/>" + item.comment;
            //html += (date.getMonth() + 1) + "/" + day + "/" + date.getFullYear() + " [" + date.getHours() + ":" + date.getMinutes() + "] " + " - " + item.userid + ":  " + item.comment;
            html += "</span><br/><br/>";
        })
        $("#message").append(html);

        //Change Unread to Read Status; unread_count = 0;
        $.ajax({
            type: "POST",
            url: 'Message/ChangeToReadStatus',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(cid),
            success: function (d) {
                console.log("Changed to Read Status");
                $.connection.hub.start().done(function () {
                    chat.server.send("", "", "");
                });
            }
        });


        //console.log(message_container[cid]);
    });
    $('#sendmessage').click(function () {
        var admmsg = $('#admMessage').val();
        var cid = $("#message").attr("cid");
        var resident_id = current_rid;

        if (cid == undefined) {
            alert("Please select a resident and task comment.")
        }

        var comment = {
            datetime: new Date(),
            comment: admmsg
        };

        data = {
            resident_id: resident_id,
            comment: comment,
            cid: cid,
        }
        $.ajax({
            url: 'GuestNotification/HandleComment',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            success: function (d) {

                //var comment;
                $("#admMessage").val('');
                if (d != null) {

                    comment = parseComment(d.comment);

                    $("#message").html(comment);
                }
               // $("#guestDiag").dialog("open");

                $.connection.hub.start().done(function () {
                    chat.server.send("#well_comment", comment, d.cid);
                });
            }
            
        });


    });

    //Filter by Date
    $(document).on("change", "#date_commented", function () {
        $("select#task").prop('selectedIndex', 0);
        var date = $("#date_commented option:selected").text();
        var filter, tbody, tr, td, i, txtValue;
        tbody = document.getElementById("taskdetaillist");
        tr = tbody.getElementsByTagName("tr");
      
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                txtValue = td.textContent || td.innerText;
                if (date != "All") {
                    if (txtValue.indexOf(date) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                } else {
                    tr[i].style.display = "table-row";
                }
            }
        
    })
   
    //Filter by Task
    $(document).on("change", "#task", function () {
        $("select#date_commented").prop('selectedIndex', 0);
        var task = $("#task option:selected").text();
        var filter, tbody, tr, td, i, txtValue;
        tbody = document.getElementById("taskdetaillist");
        tr = tbody.getElementsByTagName("tr");
       
            for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            txtValue = td.textContent || td.innerText;
            if (task != "All") {
                if (txtValue.indexOf(task) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            } else {
                tr[i].style.display = "table-row";
            }
        }
        
       
    })
    
    $(document).on("click", "#msg_btn", function () {
        load_residents();
    })
});
