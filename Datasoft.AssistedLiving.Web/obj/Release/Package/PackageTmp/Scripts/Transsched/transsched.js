﻿var isCalendarView = 0;
var isDailyView = 0;
var page = 0;
var weekly_table = "";
var info = "";
var numberOfPages = ""
var date_today = "";
var _wpresentDate = "";
var dpicker = "";
var wpicker1 = ""
var wpicker2 = ""
var fromCalendar = "";
var comp_resident_id = "";
var transchedState = {
    orig: '',
    current: ''
};
$(function () {

    var date = new Date();
    var year = date.getFullYear();
    var loadCurrentWeek = false;
    var presentDate = "";

    $("#start_date1").datepicker({
        defaultDate: new Date(year, 0, 1),
        startDate: new Date(year, 0, 1),
        endDate: new Date(year, 2, 31)
    })
    $("#start_date2").datepicker({
        defaultDate: new Date(year, 3, 1),
        startDate: new Date(year, 3, 1),
        endDate: new Date(year, 5, 30)
    })
    $("#start_date3").datepicker({
        defaultDate: new Date(year, 6, 1),
        startDate: new Date(year, 6, 1),
        endDate: new Date(year, 8, 30)
    })
    $("#start_date4").datepicker({
        defaultDate: new Date(year, 9, 1),
        startDate: new Date(year, 9, 1),
        endDate: new Date(year, 11, 31)
    })
    $(".dtpicker").datepicker().on('changeDate', function () {
        $(this).datepicker('hide');
    });
})
//$("#load_grid2").show();
var settings = {
    startDate: null,
    endDate: null,
    appointments: []
};
var global_html = [];
var optsRes = [],
    optsCS = [''],
    optsDP = [''],
    optsStat = [''],
    optsTask = [''];
var calftr = "";
var startDateNew = "";
var endDateNew = "";
var filtercal = "";
var filterbox = ""
var currentId = "";
var statt = "";
var roleID = "";
var adminRoles = ["1", "9", "10", "11", "12", "13"];
var appointmenttype = "";
var buildCalendar = function () {

    var month = $('#monthfilter').val();
    var year = $('#yrfilter').val();
    var month_year, myDate = "";

    if (dpicker != "" && fromCalendar == 0) {
        month_year = $("#curday").text();
        myDate = new Date(month_year);
        month = myDate.getMonth() + 1;
        year = myDate.getFullYear();
    }

    if (wpicker1 != "" && fromCalendar == 0) {
        month_year = $("#wstartDate").text();
        myDate = new Date(month_year);
        month = myDate.getMonth() + 1;
        year = myDate.getFullYear();
    }


    $('#monthfilter').val(month);
    $('#yrfilter').val(year);

    minDate = getMonthDateRange(year, month);
    $("#calendar").fullCalendar('destroy');
    buildCal(minDate);

    var m = moment([year, month - 1, 1]).format('YYYY-MM-DD');
    $('#calendar').fullCalendar('gotoDate', m);
}
function getMonthDateRange(year, month) {
    // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
    // array is 'year', 'month', 'day', etc\

    var startDate = moment([year, month - 1]);

    // Clone the value before .endOf()
    var endDate = moment(startDate).endOf('month');

    // make sure to call toDate() for plain JavaScript date type
    return {
        start: startDate,
        end: endDate
    };
}
function buildCal(date) {

    var facilityId = null,
        roomId = null;
    var fr = $('#filterExp').val();

    var data = {
        startDate: date.start,
        endDate: date.end
    };
    $.ajax({
        url: "Transsched/GetAdmissionCalendar",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        async: false,
        data: JSON.stringify(data),
        success: function (response) {
            if (response) {

                Calendar.Build({
                    startDate: data.startDate,
                    endDate: data.endDate,
                    appointments: response.Appointments
                });
            }
        }
    });
}
function createDatatable() {
    weekly_table = $("#weekly").DataTable({
        stateSave: true,
        "scrollY": "true",
        "scrollX": "true",
        "scrollCollapse": true,
        responsive: !0,
        pagingType: "full_numbers",
        dom: 'lBfrtip',
        "ordering": false,
        "bFilter": false,
        "bInfo": false,
        "aoColumnDefs": { "bSortable": false, "aTargets": [0, 1, 2, 3] },
        "page": 0,
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="la la-download"></i> Export to Excel',
                title: 'ALC_TaskSchedule',
                className: 'btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            }
        ]

    });
    return info = weekly_table.page.info();


}
function updateSchedule() {

    var transpo_id = $(".r_transpo_id").val();
    var date = $(".r_currentdate").val();
    var end_date = $("#r_end_date").val();
    var start_date = $(".r_date").val();
    var comments = $("#r_comments").val();
    var warning;

    date = new Date(date);
    end_date = new Date(end_date);
    start_date = new Date(start_date);

    var data = {
        transpo_id: transpo_id,
        end_date: end_date,
        comments: comments,
        date: date
    };

    if (start_date > end_date) {

        data = {
            transpo_id: transpo_id,
            comments: comments,
            date: date
        };

        if (end_date != "") {
            warning = true;
        }
    }

    $.ajax({
        url: "Transsched/UpdateSchedule",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(data),
        type: "POST",
        success: function (d) {
            var msg = "";
            if (d.result == "-1") {
                swal("Schedule update failed!","","warning");
            } else {
                if (warning) {

                   swal("End date not saved!", "", "warning");

                } else {
                    swal("Schedule updated successfully!","","success");

                    if (isCalendarView == 1) {
                        $("#monthly_view").click();
                    } else if (isDailyView == 1) {
                        filtercal = getFilter();
                        $("#weekly").DataTable().destroy();
                        generateDailySchedule(presentDate, filtercal, focus);
                        createDatatable();
                        getHeight();
                    } else {
                        filtercal = getFilter();
                        $("#weekly").DataTable().destroy();
                        generateWeeklySchedule($("#wstartDate").text(), $("#wendDate").text(), filtercal, focus);
                        createDatatable();
                        getHeight();
                    }

                }

                //$("#" + currentId).click();
                //conflict in js
                //var container = $("#weekly_schedule"),
                //    scrollTo = $("#" + currentId);

                //container.animate({
                //    scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
                //});
            };

        }
    });
}
function getFilter() {

    if (calftr == "ftrres") {

        filtercal = $('#ftrResident :selected').text();
        filterbox = $('#ftrResident');
    } else if (calftr == "ftrcs") {
        filtercal = $('#ftrStaff :selected').text();
        filterbox = $('#ftrStaff');
    } else if (calftr == "ftrdp") {
        filtercal = $('#ftrDept :selected').text();
        filterbox = $('#ftrDept');
    } else if (calftr == "ftrstat") {
        filtercal = $('#ftrStatus :selected').text();
        filterbox = $('#ftrStatus');
    } else if (calftr == "ftrtask") {
        filtercal = $('#ftrTask :selected').text();
        filterbox = $('#ftrTask');
    }
    if (filtercal == "" || filtercal == "All") {

        filtercal = "All";
    }

    return filtercal;
}
function calfilters() {
    $('#ftrResident').html(optsRes.join(''));
    $('#ftrStaff').html(optsCS.join(''));
    $('#ftrDept').html(optsDP.join(''));
    $('#ftrTask').html(optsTask.join(''));
    var usedNames = {};
    $("#ftrResident option").each(function () {
        if (usedNames[this.text]) {
            $(this).remove();
        } else {
            usedNames[this.text] = this.value;
        }
    });
    var usedNamesCS = {};
    $("#ftrStaff option").each(function () {
        if (usedNamesCS[this.text]) {
            $(this).remove();
        } else {
            usedNamesCS[this.text] = this.value;
        }
    });
    var usedNamesDP = {};
    $("#ftrDept option").each(function () {
        if (usedNamesDP[this.text]) {
            $(this).remove();
        } else {
            usedNamesDP[this.text] = this.value;
        }
    });

    var usedNamesTask = {};
    $("#ftrTask option").each(function () {
        if (usedNamesTask[this.text]) {
            $(this).remove();
        } else {
            usedNamesTask[this.text] = this.value;
        }
    });


    $("#ftrResident").html($('#ftrResident option').sort(function (x, y) {
        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
    }));
    $("#ftrStaff").html($('#ftrStaff option').sort(function (x, y) {
        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
    }));
    $("#ftrDept").html($('#ftrDept option').sort(function (x, y) {
        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
    }));
    $("#ftrTask").html($('#ftrTask option').sort(function (x, y) {
        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
    }));

    $("#ftrResident").prepend('<option>All</option>');
    $("#ftrStaff").prepend('<option>All</option>');
    $("#ftrDept").prepend('<option>All</option>');
    $("#ftrTask").prepend('<option>All</option>');

    if (filtercal != "All") {

        $("#ftrResident").get(0).selectedIndex = 0;
        $("#ftrStaff").get(0).selectedIndex = 0;
        $("#ftrDept").get(0).selectedIndex = 0;
        $("#ftrTask").get(0).selectedIndex = 0;
        filterbox.val(filtercal);
    } else {
        $("#ftrResident").get(0).selectedIndex = 0;
        $("#ftrStaff").get(0).selectedIndex = 0;
        $("#ftrDept").get(0).selectedIndex = 0;
        $("#ftrTask").get(0).selectedIndex = 0;
    }
}
function updateTask() {
    var row = $('#weekly_task').extractJson('name');
    row['resident_id'] = comp_resident_id;

    var data = {
        func: "carestaff_taskcompletion",
        data: row
    };
    var proceed = true;
    var startDate = $("#wstartDate").text();
    var endDate = $("#wendDate").text();

    //code from previous version
    if (row.No == true) {
        if (row.remarks == "") {
            $(".empty_remarks").show();
            proceed = false;
        }
    }

    //recent code but reverted to previous version
    //if (row.previous_status == "No" && row.No == false) {
    //    if (row.remarks == "") {
    //        $(".empty_remarks").show();
    //        proceed = false;
    //    }
    //}
    filtercal = getFilter();

    if (proceed) {

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Staff/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {

                if (m.result == 0) {
                    setTimeout(function () {

                        swal("Task completed successfully!","", "success");
                        $(".cancel").click();
                        $("#viewTAsk .cancel").click();

                        if (isDailyView == 1) {
                            filtercal = getFilter();
                            $("#weekly").DataTable().destroy();
                            generateDailySchedule(presentDate, filtercal, focus);

                            createDatatable();
                            getHeight();
                        } else {
                            filtercal = getFilter();
                            $("#weekly").DataTable().destroy();
                            generateWeeklySchedule($("#wstartDate").text(), $("#wendDate").text(), filtercal, focus);
                            createDatatable();
                            getHeight();
                        }

                    }, 100);


                } else {
                    swal("Completing task failed.", "", "error");
                    $(".cancel").click();
                    $("#viewTask .cancel").click();
                }

                // $("#" + currentId).trigger("click");
                getHeight();

            }
        });
    }

}
function ampm(e) {

    var s = e.split(":");

    if (s[0] < 12) {
        return e + " AM";
    } else if (s[0] > 12) {
        s[0] -= 12;

        return s[0] + ":" + s[1] + " PM";
    } else {
        return e + " PM";
    }
}
function getStaff() {
    $.ajax({
        url: "Transsched/GetStaff",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: "POST",
        success: function (d) {

            //var obj = JSON.parse(d);
            var html = "";

            var byStaff = d;
            byStaff.sort(function (a, b) {
                if (a.first_name < b.first_name) return -1;
                if (a.first_name > b.first_name) return 1;
                return 0;
            })

            for (var i = 0; i < byStaff.length; i++) {
                html += "<option value='" + byStaff[i].user_id + "'>" + byStaff[i].first_name + " " + byStaff[i].last_name + "</option>";
            }

            $("#r_staff").html(html);
        }
    })
}
function editSchedule() {

    $("#r_residents").hide();
    $("#diag_mode").val(1);
    var transpo_id = $(".r_transpo_id").val();
    var resident_id = $("#r_resident_id").val();
    var recurrence = $("#r_recurrence").text();
    var name = $(".r_resident_name").val();
    isCalendarView = 0;
    isCalendarView = $(".cal_view").val();

    resident_id = $(".r_resident_id").val();

    var biweekly = {
        'Sunday': 0,
        'Monday': 1,
        'Tuesday': 2,
        'Wednesday': 3,
        'Thursday': 4,
        'Friday': 5,
        'Saturday': 6
    };

    var biweekly_days = $("#r_day_of_the_week").text();


    if (recurrence == "biweekly" || recurrence == "weekly") {

        var d = biweekly_days.split(", ");
        var day1 = d[0];

        if (d.length > 1) {
            var day2 = d[1];
            $("#day_of_the_week1").val(biweekly[day1]).change();
            $("#day_of_the_week2").val(biweekly[day2]).change();
        } else {
            $("#day_of_the_week").val(biweekly[day1]).change();
        }

    } else if (recurrence == "monthly") {
        $("#monthly_date").val($("#r_day_of_the_month").text());
    } else if (recurrence == "quarterly") {
        var dates = $("#r_quarterly_dates").text().split(",");
        $("#start_date1").val(dates[0]).change();
        $("#start_date2").val(dates[1]).change();
        $("#start_date3").val(dates[2]).change();
        $("#start_date4").val(dates[3]).change();
    }
    $("#c_resident").show();

    $("#recurrence").val($("#r_recurrence").text()).change();
    $("#start_date").val($("#r_date").text()).change();
    $("#end_date").val($("#r_end_date").text()).change();
    $("#start_time").val($("#r_start_time").text()).change();
    $("#end_time").val($("#r_end_time").text()).change();
    $("#purpose").val($("#r_purpose").text()).change();
    $("#comment").val($("#r_comments").text()).removeAttr("disabled");
    $("#transpo_id").val(transpo_id);

    if (isCalendarView == 1) {
        $("#c_resident").html("<span><b>&nbsp;" + $("#name").text() + "</b></span>");
        $("#recurrence").val($("#r_recurrencec").text()).change();
        $("#start_date").val($("#r_datec").text()).change();
        $("#end_date").val($("#r_end_datec").text()).change();
        $("#start_time").val($("#r_start_timec").text()).change();
        $("#end_time").val($("#r_end_timec").text()).change();
        $("#purpose").val($("#r_purposec").text()).change();
        $("#comment").val($("#r_commentsc").text()).removeAttr("disabled");
    }

}
function deleteSchedule(type, d_id, row) {

    if (type == 0) {

        var transpo_id = $(".r_transpo_id").val();
        var startDate = $("#wstartDate").text();
        var endDate = $("#wendDate").text();

        swal({
            title: "Are you sure you want to delete this data?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes"
        }).then(function (e) {
            e.value &&
                $.ajax({
                    url: "Transsched/DeleteSchedule",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    type: "POST",
                    data: JSON.stringify({
                        transpo_id: transpo_id
                    }),
                    success: function (d) {


                        // setTimeout(function () {
                        toastr.success("Task deleted successfully!");
                        // }, 2000);

                        $(".cancel").click();
                        $("#viewTAsk .close").click();
                        if (isCalendarView == 1) {
                            isCalendarView = 1;
                            setTimeout(function () {

                                $("#monthly_view").click();
                            }, 500)
                        } else if (isDailyView == 1) {
                            filtercal = getFilter();
                            $("#weekly").DataTable().destroy();
                            generateDailySchedule(presentDate, filtercal, focus);
                            createDatatable();
                            getHeight();
                        } else {
                            filtercal = getFilter();
                            $("#weekly").DataTable().destroy();
                            generateWeeklySchedule($("#wstartDate").text(), $("#wendDate").text(), filtercal, focus);
                            createDatatable();
                            getHeight();
                        }
                    }
                });

        });
    } else {

        var dDate = new Date(Date.now());
        var uDate = (dDate.getMonth() + 1) + "/" + dDate.getDate() + "/" + dDate.getFullYear();
        row = $('#weekly_task').extractJson();
        //   debugger
        row.id = d_id;

        row.active_until = uDate;
        var data = {
            func: "activity_schedule",
            mode: 3,
            data: row
        };

        swal({
            title: "Are you sure you want to deactivate this task?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes"
        }).then(function (e) {
            e.value &&
                $.ajax({

                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Admin/PostGridData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {
                        if (m.result == 0) {
                            if (row.previous_status == "n/a") {
                                swal("Task deactivated successfully!", "", "success");
                            } else {
                                swal("Task deactivated successfully!", "Task has already lapsed. It will take effect on the following day. ", "success");
                            }

                            $(".cancel").click();

                            if (isDailyView == 1) {
                                filtercal = getFilter();
                                $("#weekly").DataTable().destroy();
                                generateDailySchedule(presentDate, filtercal, focus);
                                createDatatable();
                                getHeight();
                            } else {
                                filtercal = getFilter();
                                $("#weekly").DataTable().destroy();
                                generateWeeklySchedule($("#wstartDate").text(), $("#wendDate").text(), filtercal, focus);
                                createDatatable();
                                getHeight();
                            }
                        }
                    }
                });

        });

    }




}
function addSchedule(recur, mode) {

    var day_of_the_week = $("#day_of_the_week").val();
    var resident_id = $("#r_residents").val();
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    var date = $(".r_currentdate").val();
    var purpose = $("#purpose").val();
    var transpo_id = $("#transpo_id").val();
    var comment = $("#comment").val();
    var recurrence = recur;
    var start_time;
    var end_time;
    var day_of_the_month = "";
    var url = "";
    var message = "";

    //if ($("#start_time").val() == "" || $("#end_time").val()) {
    //    swal("Please complete start time and end time values.", "", "warning");
    //    return;
    //}

    var today = new Date();
    today.setHours(0, 0, 0, 0);
    var start = new Date(start_date);
    var endD = new Date(end_date);

    if (end_date) {
        end_date = new Date(end_date)
    } else {
        end_date = null
    };

    if (mode == 0) {
        url = "Transsched/AddSchedule";
        message = "Schedule added successfully!";
    } else {
        url = "Transsched/UpdateSchedule";
        message = "Schedule updated successfully!";
    }

    var form = document.getElementById('addtranspomodal_form');
    for (var i = 0; i < form.elements.length; i++) {
        if (form.elements[i].value === '' && form.elements[i].hasAttribute("required")) {
            swal("<span>Fill in all required (<span style=\"color: red;\">*</span>) fields. </span>")
            return false;
        }
    }

    if (start < today && mode == 0) {
        swal("Selected starting date has already passed. Please select again.", "", "warning");
        return;
    } else {
        if (resident_id == null) {
            resident_id = $("#cn_resident").val();
        }
        if (recurrence != "once") {
            if (endD < start) {
                swal("Selected end date is before the start date. Please select again.", "", "warning");
                return;
            }
        }

        if (recurrence == "quarterly") {
            day_of_the_month = $("#start_date1").val() + "," + $("#start_date2").val() + "," + $("#start_date3").val() + "," + $("#start_date4").val();
        }

        if (recurrence == "biweekly") {
            day_of_the_week = $("#day_of_the_week1").val() + "," + $("#day_of_the_week2").val();
        }

        if (recurrence == "monthly") {
            day_of_the_month = $("#monthly_date").val();
        }
        if (recurrence == "annually") {

            if (end_date.getFullYear() == today.getFullYear()) {
                swal("Please enter any year after " + end_date.getFullYear());
                return
            }
        }

        var s_timee = "";
        var e_timee = "";

        start_time = $("#start_time").val().split(" ");
        end_time = $("#end_time").val().split(" ");

        var start = new Date();
        var end = new Date();
        var stime = start_time[0].split(":");
        var etime = end_time[0].split(":");


        start.setHours(parseInt(stime[0]), parseInt(stime[1]), 0);
        end.setHours(parseInt(etime[0]), parseInt(etime[1]), 0);

        //if (start_time[1] == "PM") {
        //    if (parseInt(stime[0]) != 12) {
        //        s_timee = 12 + parseInt(stime[0]);
        //        start_time = s_timee + ":" + stime[1] + ":00";
        //        start.setHours(s_timee, parseInt(stime[1]), 0);
        //    } else {
        //        start_time = stime[0] + ":" + stime[1] + ":00";
        //        start.setHours(parseInt(stime[0]), parseInt(stime[1]), 0);
        //    }
        //} else {
        start_time = start_time[0] + ":00";
        //}

        //if (end_time[1] == "PM") {
        //    if (parseInt(etime[0]) != 12) {
        //        e_timee = 12 + parseInt(etime[0]);
        //        end_time = e_timee + ":" + etime[1] + ":00";
        //        end.setHours(e_timee, parseInt(etime[1]), 0, 0);
        //    } else {
        //        end_time = etime[0] + ":" + etime[1] + ":00";
        //        end.setHours(parseInt(etime[0]), parseInt(etime[1]), 0);
        //    }

        //} else 
        end_time = end_time[0] + ":00";




        date = new Date(Date.now());
        date = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();

        var data = {
            transpo_id: transpo_id,
            start_date: start_date,
            end_date: end_date,
            date: date,
            recurrence: recurrence,
            day_of_the_week: day_of_the_week,
            day_of_the_month: day_of_the_month,
            start_time: start_time,
            end_time: end_time,
            resident_id: resident_id,
            //staff_id: staff_id,
            purpose: purpose,
            comments: comment
        };

        if (start < end) {

            $.ajax({
                url: url,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                async: false,
                type: "POST",
                success: function (d) {

                    $("#weekly_wrapper").show();
                    if (d.transpo_id) {
                        setTimeout(function () {
                            swal(message,"", "success");
                            $(".cancel").click();

                            if (isCalendarView == 1) {
                                $("#monthly_view").click();

                            } else if (isDailyView == 1) {
                                filtercal = getFilter();
                                buildCalendar();
                                $("#weekly").DataTable().destroy();
                                generateDailySchedule(presentDate, filtercal, focus);
                                createDatatable();
                                getHeight();
                            } else {
                                filtercal = getFilter();
                                buildCalendar();
                                $("#weekly").DataTable().destroy();
                                generateWeeklySchedule($("#wstartDate").text(), $("#wendDate").text(), filtercal, focus);
                                createDatatable();
                                getHeight();
                            }
                        }, 150)


                    } else {

                        return false;
                        // toastr.error("An error occured.");
                    }
                    //if (mode == 0) {
                    //    buildCalendar();
                    //    calfilters();
                    //} else {

                    //    //$("#" + currentId).click();
                    //    //conflict in js
                    //    var container = $("#weekly_schedule"),
                    //        scrollTo = $("#" + currentId);

                    //    container.animate({
                    //        scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
                    //    });
                    //}
                }
            })

        } else {
            swal("Selected end time must be before the start time. Please select again.", "", "warning");
            return;
        }
    }
}
function GenerateCalendar(events) {


    $("#calendar").fullCalendar({
        editable: true,
        defaultView: 'month',
        header: {
            left: '',
            center: 'prev,title,next',
            right: ''
        },
        buttonText: {
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
        },
        eventLimit: true,
        //eventColor: '#34bfa3',
        editable: false,
        events: events,
        minTime: 0,
        maxTime: 24,
        displayEventTime: false,
        fixedWeekCount: true,
        //timeFormat: "h(:mm)t",

        eventClick: function (event, jsEvent, view) {

            var string = "";
            var e_date = new Date(Date.now());

            var data = {
                transpo_id: event.appointment_id,
                date: e_date,
                name: event.name
            };

            $(".reschedd").attr("data-target", "");

            $.ajax({
                url: "Transsched/GetScheduleData",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: JSON.stringify(data),
                success: function (response) {
                    

                    var obj = response["data"][0];
                    var isOnce = obj.recurrence;
                    var comments = response["comments"][0];


                    if (comments == "undefined" || comments == null) {
                        comments = "";
                    } else {
                        comments = comments.comment;
                    }

                    string = "<input class=\"r_currentdate\" type=\"hidden\"/><input class=\"r_resident_id\" type=\"hidden\" /><input class=\"r_resident_name\" type=\"hidden\" /><input class=\"r_transpo_id\" type=\"hidden\" />";
                    string += "<input class=\"r_recurrence\" type=\"hidden\" /><input class=\"r_day_of_the_week\" type=\"hidden\" />";
                    string += "<input class=\"r_day_of_the_month\" type=\"hidden\" /><input class=\"r_date\" type=\"hidden\" />";
                    string += "<input class=\"r_start_time\" type=\"hidden\" /><input class=\"r_end_time\" type=\"hidden\" />";
                    string += "<input class=\"r_purpose\" type=\"hidden\" /><input class=\"r_staff_id\" type=\"hidden\" /><input class=\"cal_view\" type=\"hidden\" />";
                    string += "<br/><table style=\"font-size: small;\" class=\"scheduleTable col-lg-8 col-xs-8\">";

                    string += "<td style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">Recurrence: </label></td><td><b><span class=\"resValue\" id=\"r_recurrencec\">" + obj.recurrence + "</span></b></td>";
                    string += "<td style=\"text-align: right;padding-right: 30px;\"></td>";
                    string += "</tr>";

                    string += "<tr>";
                    string += "<td style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">Start Date: </label></td><td><b><span class=\"resValue\"  id=\"r_datec\">" + obj.date + "</span></b></td>";
                    string += "<td style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">End Date: </label></td><td><b><span class=\"resValue\" id=\"r_end_datec\">" + (obj.end_date == null ? "" : obj.end_date) + "</span></b></td>";
                    string += "</tr>";

                    string += "<tr>";
                    string += "<td style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">Start Time: </label></td><td><b><span  class=\"resValue\" id=\"r_start_timec\">" + ampm(obj.start_time) + "</span></b></td>";
                    string += "<td style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">End Time: </label></td><td><b><span  class=\"resValue\" id=\"r_end_timec\">" + ampm(obj.end_time) + "</span></b></td>";
                    string += "</tr>";

                    string += "<tr>";
                    string += "<td style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">Purpose: </label></td><td colspan=\"3\"><b><span  class=\"resValue\" id=\"r_purposec\">" + obj.purpose + "</span></b></td>";
                    string += "</tr>";


                    string += "<tr>";
                    string += "<td style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">Comments / Remarks: </label></td><td colspan=\"3\"><b><span id=\"r_commentsc\">" + comments + "</span></b></td>";
                    string += "</tr>";

                    string += "</table>";


                    $('#viewEventBody').html(string);
                    $(".reassignTask").removeAttr("disabled");
                    $(".stat").html("");


                    $('#res_ID').val(obj.resident_id);


                    $(".r_resident_name").val(response.name);
                    $(".r_resident_id").val(obj.resident_id);
                    $(".r_transpo_id").val(obj.transpo_id);
                    $(".cal_view").val(1);
                    $("#name").html($("#res_ID option:selected").text());
                    $("#c_resident").html($("#res_ID option:selected").text());
                }
            });

            if ($.inArray(roleID, adminRoles) !== -1) {
                $(".forAdminOnly").show();

                $(".reassign").attr("id", 1);
                $(".reschedd").attr("id", 2);
                $(".addSched").attr("id", 3);
                $(".reassign").attr("data-target", "#diag_add_transpo");
            } else {
                $(".forAdminOnly").hide();
            }
            $('#viewEvents').modal();
        },

    });



}
var getCellDesc = function (d) {
    global_html = [];
    var html = "";
    var string = "";
    var string2 = "";
    var recurrence, cl;
    var currentdate = new Date(d);
    var end;

    var byTime = settings.appointments.slice(0);

    byTime.sort(function (a, b) {
        return a.start_time - b.start_time;
    });

    $.each(byTime, function (i, item) {
        recurrence = item.recurrence;
        cl = "cl_" + recurrence;
        string = "<a recurrence=\"" + recurrence + "\" time=\"" + item.start_time + "-" + item.end_time + "\" resname=\"" + item.name + "\" appointment_type=\"transpo\" appointmentid=\"" + item.appointment_id + "\" id=\"" + item.resident_id + "\" href=\"#\" class=\"mcl " + cl + "\">" + item.start_time + " " + ampm(item.start_time) + " - " + item.end_time + " " + ampm(item.end_time) + " " + item.name + "</a><br>";

        if (item.end_date != null) {
            end = (d <= item.end_date);
        } else {
            end = true;
        };
        if (recurrence == "once") {
            //this should be "if equal" but "==" doesn't work. idk why
            if (!(d > item.start_date) && !(d < item.start_date)) {
                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "weekly") {

            if (d >= item.start_date && d.getDay() == item.day_of_the_week && end) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "daily") {

            if (d >= item.start_date && end) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "biweekly") {

            var dw = item.day_of_the_week.split(",");



            if (((d >= item.start_date && d.getDay() == dw[0]) || (d >= item.start_date && d.getDay() == dw[1])) && end) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "monthly") {

            if (d >= item.start_date && d.getDate() == item.day_of_the_month && end) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "quarterly") {

            var qd = item.day_of_the_month.split(",");
            var date;

            $.each(qd, function (q, e) {

                date = new Date(e);

                if (d >= item.start_date && d.getMonth() == date.getMonth() && d.getDate() == date.getDate() && end) {

                    html += string;
                    global_html.push(item);
                }

            });


        } else if (recurrence == "annually") {

            date = new Date(item.start_date);

            if (d >= item.start_date && d.getMonth() == date.getMonth() && d.getDate() == date.getDate() && end) {

                html += string;
                global_html.push(item);
            }
        }


    });

    return html;
}
var Calendar = (function () {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var cellClasses = {
        1: "typeCont",
        2: "typeGen",
        3: "typeInp",
        4: "typeRout"
    };
    var icons = {
        "551": "snp.png",
        "421": "ptp.png",
        "431": "otp.png",
        "441": "stp.png",
        "561": "swp.png",
        "571": "hhap.png"
    };

    var selectedDate = "";


    var init = function (options) {

        if (options.startDate) settings.startDate = new Date(options.startDate);
        if (options.endDate) settings.endDate = new Date(options.endDate);
        if (options.appointments) {

            settings.appointments = [];
            $.each(options.appointments, function (i, item) {

                var end = null;

                if (item.end_date != null) {
                    end = new Date(item.end_date);
                }

                var loc = {
                    //admission_id: item.admission_id, resident_id: item.resident_id, name: item.name, admission_date: new Date(item.admission_date)
                    appointment_id: item.transpo_id,
                    resident_id: item.resident_id,
                    name: item.name,
                    start_date: new Date(item.date),
                    start_time: item.start_time,
                    end_time: item.end_time,
                    recurrence: item.recurrence,
                    day_of_the_week: item.day_of_the_week,
                    day_of_the_month: item.day_of_the_month,
                    end_date: end,
                    assigned_staff: item.staff_id,
                    room: item.room_name,
                    isAdmitted: item.isAdmitted
                };
                settings.appointments.push(loc);
            });
        }

    }

    var limit = function (val, max, step) {
        return val < max ? val - step : max;
    };

    //show schedule
    var renderCellHtml = function (w, d) {
        var html = "<td><table";
        if (d != null) html += " id=\"" + (d.getMonth() + 1) + "_" + d.getDate() + "_" + d.getFullYear() + "\"";
        html += " class=\"planBlock";
        if (d != null) html += " day";
        html += "\"><tr><td class=\"date\">";
        //if (w != null) html += w;
        if (d != null) html += months[d.getMonth()] + " " + d.getDate();
        html += "</td></tr><tr><td class=\"planned\"><div>";
        if (d != null) html += getCellDesc(d);
        if (w != null) html += "";
        html += "</div></table></td>";

        return html;
    }

    var renderBodyHtml = function () {

        var html = "<tr>";
        for (var i = 0; i < settings.startDate.getDay() ; i++) {
            //if (i == 0) html += renderCellHtml(1);
            html += renderCellHtml();
        }

        var day = settings.startDate;
        var week = 1;
        for (; day <= settings.endDate;) {
            //if (day.getDay() == 0) html += renderCellHtml(week);
            html += renderCellHtml(null, day);
            if (day.getDay() == 6) {
                html += "</tr><tr>";
                week += 1;
            }
            day.setDate(day.getDate() + 1);
        }

        for (var i = settings.endDate.getDay() ; i < 6; i++) {
            html += renderCellHtml();
        }
        html += "</tr>"

        return "<tbody>" + html + "</tbody>";
    }

    var renderTableHtml = function () {
        var html = "<table>";
        // html += renderHeaderHtml();
        html += renderBodyHtml();
        html += "</table>";

        return html;
    }
    var selectedTabIndex = 0;


    return {
        Build: function (options) {

            init(options);
            if (settings.startDate == null && settings.endDate == null) return;
            var str = "";
            str += renderTableHtml();



            var events = [];
            $(str).find('a').each(function () {
                if ($(this).attr("appointment_type") == "transpo") {
                    var name = $(this).attr('resname');
                    var appointment_id = $(this).attr('appointmentid');
                    var id = $(this).attr('id');
                    var recurrence = $(this).attr('recurrence');

                    //recurrence

                    var color = "";
                    if (recurrence == "daily") {
                        color = "#34bfa3c9";
                    } else if (recurrence == "weekly") {
                        color = "#24dc006b";
                    } else if (recurrence == "biweekly") {
                        color = "#00c5dcba";
                    } else if (recurrence == "quarterly") {
                        color = "#abf347de";
                    } else if (recurrence == "monthly") {
                        color = "#de4537db";
                    } else if (recurrence == "annually") {
                        color = "#fd7bb8";
                    } else { color = "#f45171c2"; }


                    //date
                    var edate = "";
                    var start_date = $(this).parents("table").attr("id");
                    var sdate = start_date.split("_");
                    start_date = sdate[2] + "-" + sdate[0] + "-" + sdate[1];

                    //time
                    var time = $(this).attr('time');
                    var t = time.split("-");
                    var start = ampm(t[0]);
                    var end = ampm(t[1]);

                    var st = "T" + t[0] + ":00";
                    var et = "T" + t[1] + ":00";
                    sdate = start_date + "" + st;
                    edate = start_date + "" + et;
                    sdate = moment(start_date, ["YYYY-MM-DDThh:mm:ss"]);
                    events.push({
                        title: start + "-" + end + " -  " + name,
                        name: name,
                        start: sdate,
                        date: start_date,
                        stime: start,
                        end: end,
                        appointment_id: appointment_id,
                        id: id,
                        allDay: false,
                        color: color
                        // time: time
                    });
                };
            });


            //$("#calendar").html("");
            $("#calendar").fullCalendar('destroy');
            $(".fc-center").hide();
            //$("#load_grid2").show();
            $(".loader").show();
            //  setTimeout(function () {
            GenerateCalendar(events);





            //  }, 500)
            $(".loader").hide();
            $(".fc-center").show();



            //$("#calendar").on("click", ".planned, .actual", function (e) {
            //    var $a = $(this);
            //    var $d = $a.closest(".day");
            //    if ($d[0]) {
            //        selectedDate = $d.attr("id");
            //        if ($a.is(".planned")) {
            //            $("#calendarMenu1").css({
            //                top: e.pageY,
            //                left: e.pageX
            //            }).slideDown(300);
            //            $("#calendarMenu2").hide();
            //        } else if ($a.is(".actual")) {
            //            $("#calendarMenu2").css({
            //                top: e.pageY,
            //                left: e.pageX
            //            }).slideDown(300);
            //            $("#calendarMenu1").hide();
            //        }
            //    }
            //});

        }



    }



})();
function generateTable(item, rowId, str_date2, class_icon, sched_time, count) {
    count = count || "00";

    var html2 = "";
    var ddate = str_date2.split("/");
    var dday = ddate[1];
    var carestff = "";
  
    var status = item.activity_status == 1 ? '<span class="m-badge  m-badge--success m-badge--wide">Completed</span>' : '<span class="m-badge m-badge--primary m-badge--wide">Not Completed</span>';

    if (item.carestaff_activity_id == null && item.activity_status == 0)
        status = '<span class="m-badge  m-badge--metal m-badge--wide">N/A</span';
    if (item.carestaff_activity_id == null && item.timelapsed) {
        status = '<span class="m-badge m-badge--danger m-badge--wide">Missed</span>';
    }

    if (item.reschedule_dt != null) {
        status = '<span class="m-badge  m-badge--success m-badge--wide">Reassigned / Rescheduled</span>';
    }

    if (item.carestaff_activity_id != null) {
        carestff = item.carestaff_activity_id;
    } else {
        carestff = "0";
    };
    var did = carestff + "." + item.resident_id + "." + (item.carestaff_activity_id != null ? item.carestaff_activity_id : "0");

    //to determine comments in different days
    //date.count.resident_id
    var cid = ddate[0] + ddate[1] + ddate[2];
    cid += "." + count + "." + item.resident_id;



    html2 += "<tr did=\"" + did + "\" cid=\"" + cid + "\" role=\"row\" class=\"odd\" arw=\"arw" + rowId + "\" date=\"" + str_date2 + "\" appointmentid=\"" + item.appointment_id + "\"  id=\"arw" + rowId + "" + dday + "\" resident_id=\"" + item.resident_id + "\" class=\"trcl\" appointment_type=\"" + item.type + "\" csid=\"" + item.carestaff_activity_id + "\">";
    html2 += "<td appointment_type=\"" + item.type + "\">" + str_date2 + "</td>";
    html2 += "<td><span class=\"arrow-right arrow_div arw" + rowId + str_date2 + "\"></span>" + sched_time + "</td>";
    html2 += "<td><span class=\"" + class_icon + "\"><span style=\"margin-left:30px;width:max-content;\">" + (item.resident == null ? "--" : item.resident) + "</span></span></td>";
    //html2 += "<td>" + (item.resident == null ? "--" : item.resident) + "</td>";
    html2 += "<td>" + (item.room != null ? (item.type == "transpo" ? "-" : item.room) : "N/A") + "</td>";
    html2 += "<td>" + item.activity_desc + "</td>";
    html2 += "<td>" + (item.carestaff != null ? (item.type == "transpo" ? "-" : item.carestaff) : "N/A") + "</td>";
    html2 += "<td>" + (item.type == "transpo" ? "-" : status) + "</td>";
    //html2 += "<td>" + item.is_active + "</td>";

    html2 += "<td style='width: 7px;text-align: center'><a  href=\"#\" class=\"actionButton m-portlet__nav-link btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill edit\" title=\"View details\"><i class=\"la la-edit \"></i></a></td>";

    html2 += "</tr>";

    tasks[cid] = item;

    return html2;

}
function generateWeeklySchedule(startDay, endDay, filtercal, focus) {

    focus = focus || "";
    loadCurrentWeek = false;
    isDailyView = 0;
    tasks = [];
    $("#weekly_wrapper").show();
    $("#calendar").hide();

    var startWeek = new Date(startDay);
    var tempDate = new Date(startWeek);
    var html = "";
    var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
    var months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
    var byTime;
    var html2 = "";
    var class_icon = "";


    var container = new Array();
    var data, data2;

    var endWeek = new Date(endDay);
    //endWeek.setDate(cdate.getDate() + (6 - current_day));
    var str_date;
    var stringdate;
    var schedule_task;
    var sched_desc = "";
    var sched_time = "";
    var str_date2;
    var rowId;
    var status = "";
    var na = "<span class=\"na\">N/A</span>";
    optsRes = [''];
    optsCS = [''];
    optsDP = [''];
    optsStat = [''];
    optsTask = [''];

    for (var i = 0; i <= 6; i++) {

        tempDate = new Date(startWeek);
        tempDate.setDate(startWeek.getDate() + parseInt(i));
        //tempDate.setHours(0, 0, 0, 0);
        var tday_ = new Date();
        tempDate.setHours(tday_.getHours(), tday_.getMinutes(), tday_.getSeconds(), 0);
        getCellDesc(tempDate);
        container = [];

        byTime = global_html.slice(0);

        $.each(byTime, function (i, item) {
            data = {
                'appointment_id': item.appointment_id,
                'activity_id': "",
                'carestaff': item.assigned_staff,
                'type': 'transpo',
                'resident_id': item.resident_id,
                'resident': item.name,
                'start': item.start_time,
                'end_time': item.end_time,
                'room': item.room,
                'activity_desc': "",
                'activity_status': "",
                'acknowledged_by': "",
                'carestaff_activity_id': "",
                'timelapsed': "",
                'department': item.department

            };

            if (data != "undefined") {
                container.push(data);
            }
            optsRes.push('<option>' + item.name + '</option>');
            optsTask.push('<option>Transportation Schedule</option>');
            optsDP.push('<option>Transportation</option>')
        })

        str_date2 = (tempDate.getMonth() + 1).toString() + "/" + tempDate.getDate().toString() + "/" + tempDate.getFullYear().toString();

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Staff/GetGridData?func=activity_schedule&param=" + str_date2,
            dataType: "json",
            async: false,
            success: function (m) {

                schedule_task = m;

            }
        });

        //$.each(schedule_task, function (i, item) {

        //    data2 = {
        //        'activity_id': item.activity_id,
        //        'appointment_id': item.activity_schedule_id,
        //        'type': 'dailytask',
        //        'resident_id': item.resident_id,
        //        'resident': item.resident,
        //        'start': item.start,
        //        'carestaff': item.carestaff_name,
        //        'activity_desc': item.activity_desc,
        //        'room': item.room,
        //        'activity_status': item.activity_status,
        //        'acknowledged_by': item.acknowledged_by,
        //        'carestaff_activity_id': item.carestaff_activity_id,
        //        'timelapsed': item.timelapsed,
        //        'end_time': "",
        //        'department': item.department
        //    };

        //    if (data2 != "undefined") {
        //        container.push(data2);
        //    }
        //    if (item.resident != null) {
        //        optsRes.push('<option>' + item.resident + '</option>');
        //        optsCS.push('<option>' + item.carestaff_name + '</option>');
        //        optsDP.push('<option>' + item.department + '</option>');
        //        optsTask.push('<option>' + item.activity_desc + '</option>');
        //    } else {
        //        optsRes.push('<option>--</option>');
        //        optsCS.push('<option>' + item.carestaff_name + '</option>');
        //        optsDP.push('<option>' + item.department + '</option>')
        //        optsTask.push('<option>' + item.activity_desc + '</option>');
        //    }

        //})
        $.each(schedule_task, function (i, item) {
            var active_until = "";
            var nDate = "";
            var newNdate = "";
            if (item.active_until != null) {
                nDate = new Date(item.active_until);
                newNdate = nDate.getMonth() + "/" + nDate.getDate() + "/" + nDate.getFullYear();
            }

            var newtempDate = tempDate.getMonth() + "/" + tempDate.getDate() + "/" + tempDate.getFullYear();
            var ttoday = tday_.getMonth() + "/" + tday_.getDate() + "/" + tday_.getFullYear();
            if (item.isAdmitted == true || item.isAdmitted == null) {
                if ((item.is_active == true) || ((item.is_active == false) && (active_until > tempDate)) || (item.is_active == false && (ttoday == newNdate && item.timelapsed == true))) {
                    //if (newNdate == newtempDate && newNdate == ttoday) {

                    //} else {
                        data2 = {
                            'activity_id': item.activity_id,
                            'appointment_id': item.activity_schedule_id,
                            'type': 'dailytask',
                            'resident_id': item.resident_id,
                            'resident': item.resident,
                            'start': item.start,
                            'carestaff': item.carestaff_name,
                            'activity_desc': item.activity_desc,
                            'room': item.room,
                            //'is_active': item.is_active,
                            'activity_status': item.activity_status,
                            'acknowledged_by': item.acknowledged_by,
                            'carestaff_activity_id': item.carestaff_activity_id,
                            'timelapsed': item.timelapsed,
                            'end_time': "",
                            'department': item.department,
                            'reschedule_dt': item.reschedule_dt
                        };

                        if (data2 != "undefined") {
                            container.push(data2);
                        }


                        if (item.resident != null) {
                            optsRes.push('<option>' + item.resident + '</option>');
                            optsCS.push('<option>' + item.carestaff_name + '</option>');
                            optsDP.push('<option>' + item.department + '</option>');
                            optsTask.push('<option>' + item.activity_desc + '</option>');
                        } else {
                            optsRes.push('<option>--</option>');
                            optsCS.push('<option>' + item.carestaff_name + '</option>');
                            optsDP.push('<option>' + item.department + '</option>')
                            optsTask.push('<option>' + item.activity_desc + '</option>');
                        }

                    //}
                }
            }

        })

        container.sort(function (a, b) {
            var d = a.start.split(":");
            var e = d[0] + "" + d[1];

            var f = b.start.split(":");
            var g = f[0] + "" + f[1];
            return e - g;
        });
        str_date = (tempDate.getMonth() + 1).toString() + "_" + tempDate.getDate().toString() + "_" + tempDate.getFullYear().toString();

        html += "<tr dhead=\"date_header\" style=\"background: #20b4c5;color: #f0f0f0;font-weight: 400;font-size: 12px;\"><td>" + tempDate.getFullYear().toString() + "  " + days[tempDate.getDay()] + ", " + months[tempDate.getMonth()] + " " + tempDate.getDate().toString() + "</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        var count = 1;
        $.each(container, function (i, item) {

            sched_time = ampm(item.start);

            rowId = item.type + "" + item.appointment_id + "" + item.resident_id;
            if (filtercal != "All") {
                if (filtercal == "--") {
                    filtercal = "Housekeeper";
                }

                if (filtercal == "Transportation") {
                    filtercal = "transpo";
                }

                statt = item.activity_status == 1 ? 'Completed' : 'Not Completed';
                if (item.carestaff_activity_id == null && item.timelapsed) {
                    statt = "Missed";
                } else if (item.type == "transpo") {
                    statt = "";
                } else if (item.carestaff_activity_id == null && item.activity_status == 0) {
                    statt = "Not Completed";
                }

                if (item.resident_id == null) {
                    item.type = "largetask";
                    class_icon = "largetask_icon";

                } else {

                    if (item.type == "dailytask") {
                        class_icon = "dailytask_icon";
                    } else {
                        sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                        item.activity_desc = "Transportation Schedule";
                        class_icon = "transpo_icon";
                    }
                }

                if (item.resident == filtercal || item.carestaff == filtercal || item.department == filtercal || item.type == filtercal || statt == filtercal || item.activity_desc == filtercal) {
                    html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count);
                }

            } else {

                if (item.resident_id == null) {
                    item.type = "largetask";
                    class_icon = "largetask_icon";

                } else {

                    if (item.type == "dailytask") {
                        class_icon = "dailytask_icon";
                    } else {
                        sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                        item.activity_desc = "Transportation Schedule";
                        class_icon = "transpo_icon";
                    }
                }


                html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count);
            }
            count++;
        });

        html += html2;
        html2 = "";
        $("#weekly_schedule").html("");
        $("#weekly_schedule").html(html);

    }

}
function generateDailySchedule(startDay, filtercal) {
    $("#weekly_wrapper").show();
    $("#calendar").hide();
    tasks = [];
    loadCurrentWeek = false;
    isDailyView = 1;
    var startWeek = new Date(startDay);
    var tempDate = new Date(startWeek);
    var html = "";
    var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
    var months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
    var byTime;
    var html2 = "";
    var class_icon = "";

    var container = new Array();
    var data, data2;

    var str_date;
    var stringdate;
    var schedule_task;
    var sched_desc = "";
    var sched_time = "";
    var str_date2;
    var rowId;

    var status = "";
    var na = "<span class=\"na\">N/A</span>";
    optsRes = [''];
    optsCS = [''];
    optsDP = [''];
    optsStat = [''];
    optsTask = [''];

    tempDate = new Date(startWeek);
    // tempDate.setDate(startWeek.getDate() + parseInt(i));// conflict in js
    var tday_ = new Date();
    tempDate.setHours(tday_.getHours(), tday_.getMinutes(), tday_.getSeconds(), 0);
    getCellDesc(tempDate);
    container = [];

    byTime = global_html.slice(0);

    $.each(byTime, function (i, item) {


        data = {
            'appointment_id': item.appointment_id,
            'activity_id': "",
            'carestaff': item.assigned_staff,
            'type': 'transpo',
            'resident_id': item.resident_id,
            'resident': item.name,
            'start': item.start_time,
            'end_time': item.end_time,
            'room': item.room,
            'activity_desc': "",
            'activity_status': "",
            'acknowledged_by': "",
            'carestaff_activity_id': "",
            'timelapsed': "",
            'department': item.department

        };

        if (data != "undefined") {
            container.push(data);
        }
        optsRes.push('<option>' + item.name + '</option>');
        optsTask.push('<option>Transportation Schedule</option>');
        optsDP.push('<option>Transportation</option>')
    })


    str_date2 = (tempDate.getMonth() + 1).toString() + "/" + tempDate.getDate().toString() + "/" + tempDate.getFullYear().toString();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Staff/GetGridData?func=activity_schedule&param=" + str_date2,
        dataType: "json",
        async: false,
        success: function (m) {

            schedule_task = m;

        }
    });

    $.each(schedule_task, function (i, item) {

        //console.log(item);
        
        var active_until = "";
        var nDate = ""
        var newNdate = "";
        if (item.active_until != null) {
            nDate = new Date(item.active_until);
            newNdate = nDate.getMonth() + "/" + nDate.getDate() +"/"+ nDate.getFullYear();
        }

       
      
        var newtempDate = tempDate.getMonth() +"/"+ tempDate.getDate() +"/"+ tempDate.getFullYear();
        var ttoday = tday_.getMonth() + "/" + tday_.getDate() + "/" + tday_.getFullYear();
        if (item.isAdmitted == true || item.isAdmitted == null) {

            if ((item.is_active == true) || ((item.is_active == false) && (nDate > tempDate)) || (item.is_active == false && (newNdate == newtempDate && item.timelapsed == true))) {
                //if (newNdate == newtempDate && newNdate == ttoday) {

                //    };
                //} else {
                    data2 = {
                        'activity_id': item.activity_id,
                        'appointment_id': item.activity_schedule_id,
                        'type': 'dailytask',
                        'resident_id': item.resident_id,
                        'resident': item.resident,
                        'start': item.start,
                        'carestaff': item.carestaff_name,
                        'activity_desc': item.activity_desc,
                        'room': item.room,
                        //'is_active': item.is_active,
                        'activity_status': item.activity_status,
                        'acknowledged_by': item.acknowledged_by,
                        'carestaff_activity_id': item.carestaff_activity_id,
                        'timelapsed': item.timelapsed,
                        'end_time': "",
                        'department': item.department,
                        'reschedule_dt': item.reschedule_dt

                    };

                    if (data2 != "undefined") {
                        container.push(data2);
                    }


                    if (item.resident != null) {
                        optsRes.push('<option>' + item.resident + '</option>');
                        optsCS.push('<option>' + item.carestaff_name + '</option>');
                        optsDP.push('<option>' + item.department + '</option>');
                        optsTask.push('<option>' + item.activity_desc + '</option>');
                    } else {
                        optsRes.push('<option>--</option>');
                        optsCS.push('<option>' + item.carestaff_name + '</option>');
                        optsDP.push('<option>' + item.department + '</option>')
                        optsTask.push('<option>' + item.activity_desc + '</option>');
                    }
                //--
                //}
            }

        }
    })


    container.sort(function (a, b) {
        var d = a.start.split(":");
        var e = d[0] + "" + d[1];

        var f = b.start.split(":");
        var g = f[0] + "" + f[1];
        return e - g;
    });
    str_date = (tempDate.getMonth() + 1).toString() + "_" + tempDate.getDate().toString() + "_" + tempDate.getFullYear().toString();

    html += "<tr dhead=\"date_header\" role=\"row\" class=\"index\" style=\"background: #20b4c5;color: #f0f0f0;font-weight: 400;font-size: 12px;\"><td>" + tempDate.getFullYear().toString() + "  " + days[tempDate.getDay()] + ", " + months[tempDate.getMonth()] + " " + tempDate.getDate().toString() + "</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";

    var count = 1;
    $.each(container, function (i, item) {
        sched_time = ampm(item.start);

        rowId = item.type + "" + item.appointment_id + "" + item.resident_id;
        if (filtercal != "All") {
            if (filtercal == "--") {
                filtercal = "Housekeeper";
            }

            if (filtercal == "Transportation") {
                filtercal = "transpo";
            }

            statt = item.activity_status == 1 ? 'Completed' : 'Not Completed';
            if (item.carestaff_activity_id == null && item.timelapsed) {
                statt = "Missed";
            } else if (item.type == "transpo") {
                statt = "";
            } else if (item.carestaff_activity_id == null && item.activity_status == 0) {
                statt = "Not Completed";
            }

            if (item.resident_id == null) {
                item.type = "largetask";
                class_icon = "largetask_icon";

            } else {

                if (item.type == "dailytask") {
                    class_icon = "dailytask_icon";
                } else {
                    sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                    item.activity_desc = "Transportation Schedule";
                    class_icon = "transpo_icon";
                }
            }

            if (item.resident == filtercal || item.carestaff == filtercal || item.department == filtercal || item.type == filtercal || statt == filtercal || item.activity_desc == filtercal) {

                html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count);
            }

        } else {
            if (item.resident_id == null) {
                item.type = "largetask";
                class_icon = "largetask_icon";

            } else {

                if (item.type == "dailytask") {
                    class_icon = "dailytask_icon";
                } else {
                    sched_time = ampm(item.start) + " -  " + ampm(item.end_time);
                    item.activity_desc = "Transportation Schedule";
                    class_icon = "transpo_icon";
                }
            }


            html2 += generateTable(item, rowId, str_date2, class_icon, sched_time, count);

        }
        count++;
    });


    html += html2;
    $("#weekly_schedule").html("");
    $("#weekly_schedule").html(html);
}
var handleComment = function (csid, currentResId, cid, did) {
    //if (csid != null && csid != "") {
    var data = {
        csid: csid,
        mode: 0,
        did: did,
        cid: cid,
        resident_id: currentResId,
        in_transched: 1
    }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "GuestNotification/HandleComment",
        dataType: "json",
        data: JSON.stringify(data),
        async: false,

        success: function (g) {

            var comment = "";
            var timestamp = "";
            var date = new Date();
            var guardian_comment = "";



            if (g != null) {

                if (g.Message == "no data available") {
                    $("#gComment_division").show();
                    $("#gComment").html("There is no comment from responsible person.");
                    //swal("No comment from responsible person.")
                    $("#gComment_div").hide();
                    return;
                }

                var s = "[" + g.comment + "]";
                var gobj = JSON.parse(s);
                for (var i = 0; i < gobj.length; i++) {

                    timestamp = new Date(gobj[i].datetime);
                    comment += "[" + timestamp.getHours() + ":" + timestamp.getMinutes() + "]  " + gobj[i].comment + "<br/>";

                }

                guardian_comment += (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "<br><br>" + comment;
                $("#gComment_division").show();
                $("#gComment").html("<span>" + guardian_comment + "</span>");
                $("#gComment_div").hide();
            } else {

                $("#gComment_division").show();
                $("#gComment").html("No comment from responsible person.");
                $("#gComment_div").hide();
            }


        }
    });
}
function getHeight() {

    $('.body-height').height($(window).height() - 250);
    $(window).resize(function () {
        $('.body-height').height($(window).height() - 250);
    })


    $('.dataTables_scrollBody').height($(window).height() - 520);
    $(window).resize(function () {
        if ($(window).height() > 400) {
            $('.dataTables_scrollBody').height($(window).height() - 520);

        } else {
            $('.dataTables_scrollBody').height($(window).height() - 180);

        }

    })

    if (isCalendarView == 1) {
        $('#transchedContainer').height($(window).height() + 500);
        $(window).resize(function () {
            $('#transchedContainer').height($(window).height() + 500);
        })
    } else {
        $('#transchedContainer').height($(window).height() - 120);
        $(window).resize(function () {
            $('#transchedContainer').height($(window).height() - 120);
        })
    }

    var scroll = new PerfectScrollbar('.dataTables_scrollBody');
    var scroll1 = new PerfectScrollbar('.portlet__body');
    $('#weekly').DataTable().columns.adjust();
}
function getHeightViewTaskModal() {
    if (appointmenttype == "transpo") {
        $('.body-height').height(220);
        $(window).resize(function () {
            $('.body-height').height(220);
        })
    } else {
        $('.body-height').height($(window).height() - 250);
        $(window).resize(function () {
            $('.body-height').height($(window).height() - 250);
        })
    }
}
$(document).ready(function () {
    $(".loader").show();
    var csid = "";
    var currentResId = "";
    var cid = "";
    var did = "";
    var current_month = $("#monthfilter").val();
    var current_year = $('#yrfilter').val();
    var currentDay = $("#curday").val();



    $(document).on("click", ".fc-prev-button", function () {

        var month = $('#monthfilter').val();
        var year = $('#yrfilter').val();
        var m = parseInt(month);
        var y = parseInt(year);
        if (month == 1) {
            year = y - 1;
            month = 12;
        } else {
            month = m - 1;
        }

        $('#monthfilter').val(month);
        $('#yrfilter').val(year);


        buildCalendar();


    });
    $(document).on("click", ".fc-next-button", function () {

        var month = $('#monthfilter').val();
        var year = $('#yrfilter').val();

        var m = parseInt(month);
        var y = parseInt(year);

        if (month == 12) {
            year = y + 1;
            month = 1;
        } else {
            month = m + 1;
        }

        $('#monthfilter').val(month);
        $('#yrfilter').val(year);

        buildCalendar();


    });
    $(document).on("click", ".fc-today-button", function () {

        $("#calendar").fullCalendar("destroy");
        $('#monthfilter').val(current_month);
        $('#yrfilter').val(current_year);
        buildCalendar();
    });
    $(document).on("click", "#view_gComment", function () {
        $("#gComment_section").show();
        handleComment(csid, currentResId, cid, did);
        cid = "";
        did = "";
    })

    $('#save_transpo').on('click', function () {

        var mode = $("#diag_mode").val();
        if ($.inArray(roleID, adminRoles) !== -1) {
            addSchedule($("#recurrence").val(), mode);
        } else {
            toastr.warning("You dont have permision to access this document. Please contact your System Administrator!");
            $("#updateTask").attr("disabled", true);
        }
    })


    // Start of Datepicker===============================================
    $('#daypicker').datepicker({
        dateFormat: 'mm-dd-yyyy',
        orientation: "bottom",
        todayHighlight: true,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    }).bind("change", function () {

        var startDate = $(this).val();
        presentDate = startDate;
        date_today = startDate;
        _wpresentDate = startDate;
        $('#curday').text(startDate);
        $(".week-picker").val("");
        $("#wstartDate").html("");
        $("#wendDate").html("");
        var date = $(this).datepicker('getDate');
        $("#monthfilter").val((date.getMonth() + 1).toString());
        $("#yrfilter").val(date.getFullYear());
        // filtercal = "All";
        filtercal = getFilter();
        $("#weekly").DataTable().destroy();

        dpicker = startDate;
        generateDailySchedule(startDate, filtercal);
        calfilters();

        createDatatable();
        weekly_table.page('first').draw('page');

        getHeight();
        $(".filtr").show();
        isCalendarView = 0;
        isDailyView = 1;
    });

    $('#weekpicker').datepicker({
        dateFormat: 'mm-dd-yyyy',
        orientation: "auto",
        todayHighlight: true,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    }).bind("change", function () {
        var startDate = $(this).val();
        _wpresentDate = startDate;
        var date = $(this).datepicker('getDate');
        startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
        endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);

        var sDate = (startDate.getMonth() + 1).toString() + "/" + startDate.getDate() + "/" + startDate.getFullYear();
        var eDate = (endDate.getMonth() + 1).toString() + "/" + endDate.getDate() + "/" + endDate.getFullYear();


        $('#curday').text("");
        $(".day-picker").val("");
        $("#monthfilter").val((startDate.getMonth() + 1).toString());
        $("#yrfilter").val(startDate.getFullYear());

        var focus = (date.getMonth() + 1).toString() + "_" + date.getDate().toString() + "_" + date.getFullYear();
        $("#weekly").DataTable().destroy();

        // filtercal = "All";
        filtercal = getFilter();
        generateWeeklySchedule(sDate, eDate, filtercal, focus);
        calfilters();

        createDatatable();
        weekly_table.page('first').draw('page');

        getHeight();

        $("#weekly_task").html("");
        //  selectCurrentWeek();
        $("#wstartDate").html(sDate);
        $("#wendDate").html(eDate);
        startDateNew = sDate;
        endDateNew = eDate;
        $("#weekpicker").val(startDateNew + " - " + endDateNew);
        $(".filtr").show();
        $(".calendarbtn").show();
        isCalendarView = 0;
        isDailyView = 0;
    });
    //End of Datepicker===============================================

    roleID = $("#roleID").val();
    $(".date").datepicker({
        'formatDate': 'yyyy-mm-dd',
        'autoclose': true
    });



    //$('.time').mask('99:99 xy');

    var minDate = new Date();

    for (var i = 2000; i < 2050; i++) {
        $('#yrfilter').append($("<option/>", {
            value: i,
            text: i
        }));
    };

    $('#monthfilter').val(minDate.getMonth() + 1);
    $('#yrfilter').val(minDate.getFullYear());

    var current_month = $("#monthfilter").val();
    var current_year = $('#yrfilter').val();

    function getResidents() {
        $.ajax({
            url: "Transsched/GetResidents",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            success: function (d) {

                var html = "";

                var byResident = d;
                byResident.sort(function (a, b) {
                    if (a.first_name < b.first_name) return -1;
                    if (a.first_name > b.first_name) return 1;
                    return 0;
                })

                for (var i = 0; i < byResident.length; i++) {
                    html += "<option value='" + byResident[i].resident_id + "'>" + byResident[i].first_name + " " + byResident[i].last_name + "</option>";
                }

                $("#r_residents").html(html);
                $("#r_residents").html($('#r_residents option').sort(function (x, y) {
                    return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                }));

                // $("#r_residents").prepend('<option></option>');

                $("#r_residents").get(0).selectedIndex = 0;

                transchedState.orig = JSON.stringify($('#diag_add_transpo').extractJson());
            }
        })
    }
    function SetFilter() {
        var curDay = $("#curday").text();
        var curDayOfWeek = $(".week-picker").val();

        var week = $("#wstartDate").text();

        if (curDay == "" && week != "") {
            $("#weekly_view").trigger("click");
            $('#curday').text("");
            $(".day-picker").val("");
        } else if (curDay != "" && week == "") {
            $("#daily_view").trigger("click");
        }
        getHeight();
    }

    $('#filterExp,#monthfilter,#yrfilter').change(function () {

        setTimeout(function () {
            buildCalendar();
        }, 100)
        isCalendarView = 1;
        $(".legend").show();
        $(".cal_header").show();
        $(".filtr").hide();
        $("#weekly_wrapper").hide();
        $("#calendar").show();
    });

    $('#ftrResident').change(function () {

        calftr = "ftrres";
        $("#load_grid").show();
        setTimeout(function () {

            SetFilter();
            filtercal = $('#ftrResident :selected').text();
            $("#ftrStaff").get(0).selectedIndex = 0;
            $("#ftrDept").get(0).selectedIndex = 0;
            $("#ftrStatus").get(0).selectedIndex = 0;
            $("#ftrTask").get(0).selectedIndex = 0;
            $("#load_grid").hide();
            $("#daypicker").val($("#curday").text());

        }, 300);
    });

    $('#ftrStaff').change(function () {
        calftr = "ftrcs";
        $("#load_grid").show();
        setTimeout(function () {
            //$("#weekly_view").trigger("click");
            SetFilter();
            $("#ftrResident").get(0).selectedIndex = 0;
            $("#ftrDept").get(0).selectedIndex = 0;
            $("#ftrStatus").get(0).selectedIndex = 0;
            $("#ftrTask").get(0).selectedIndex = 0;
            $("#load_grid").hide();
            $("#daypicker").val($("#curday").text());
        }, 300);
    });

    $('#ftrDept').change(function () {
        calftr = "ftrdp";
        $("#load_grid").show();
        setTimeout(function () {
            //$("#weekly_view").trigger("click");
            SetFilter();
            $("#ftrResident").get(0).selectedIndex = 0;
            $("#ftrStaff").get(0).selectedIndex = 0;
            $("#ftrStatus").get(0).selectedIndex = 0;
            $("#ftrTask").get(0).selectedIndex = 0;
            $("#load_grid").hide();
            $("#daypicker").val($("#curday").text());
        }, 300);
    });

    $('#ftrStatus').change(function () {
        calftr = "ftrstat";
        $("#load_grid").show();
        setTimeout(function () {
            //$("#weekly_view").trigger("click");
            SetFilter();
            $("#ftrResident").get(0).selectedIndex = 0;
            $("#ftrStaff").get(0).selectedIndex = 0;
            $("#ftrDept").get(0).selectedIndex = 0;
            $("#ftrTask").get(0).selectedIndex = 0;
            $("#load_grid").hide();
            $("#daypicker").val($("#curday").text());
        }, 300);
    });

    $('#ftrTask').change(function () {
        calftr = "ftrtask";
        $("#load_grid").show();
        setTimeout(function () {
            //$("#weekly_view").trigger("click");
            SetFilter();
            $("#ftrResident").get(0).selectedIndex = 0;
            $("#ftrStaff").get(0).selectedIndex = 0;
            $("#ftrDept").get(0).selectedIndex = 0;
            $("#ftrStatus").get(0).selectedIndex = 0;
            $("#load_grid").hide();
            $("#daypicker").val($("#curday").text());
        }, 300);
    });

    //ON CLICKS----------------------------------------------------------------------------------
    $('#btnReload').click(function () {
        fromCalendar = 0;
        buildCalendar();
        startDateNew = "";
        endDateNew = "";
        loadCurrentWeek = true;
        $(".dw_btns").css("padding-right", "0px");
        $("#weekly_view").trigger("click");

    });


    $('#btncancel').click(function () {
        $("#Onetimedate").css("display", "none");
        
    });
    $('#save_newTask').click(function () {
        $("#Onetimedate").css("display", "none");
    });



    $('#dayReload').click(function () {
        fromCalendar = 0;
        buildCalendar();
        $("#weekly").DataTable().destroy();
        $(".week-picker").val("");
        $(".day-picker").val("");
        $(".dw_btns").css("padding-right", "0px");
        $("#daily_view").trigger("click");

    });

    $("#add_schedule").on("click", function () {
        $("#transpo_label").html("<span>Add Schedule</span>");
        if ($.inArray(roleID, adminRoles) !== -1) {
           
            $("#diag_add_transpo").modal("toggle");
            $("#r_residents").val("");
            $("#cn_resident").val("");
            $("#diag_mode").val(0);
            $("#c_resident").html("");
            $("#c_resident").hide();
            $("#r_residents").show();
            $("#comment").val("").attr("disabled", "disabled");
            $("#comment_section").hide();

           

            var view_id = "r_" + $("#recurrence").val();
            $(".r_view").hide();
            $("#" + view_id).show();

            if ($("#recurrence").val() == "once") {
                $("#end_date").attr("disabled", "disabled");
            }
            //if ($("#recurrence").val() == "annually") {
            //    debugger
            //    $("#addtranspomodal_form #start_end_section").hide();
            //}
            var mode = 1;
            
            getResidents();
            getStaff();
            
           
            // saveTranspo();
        } else {
            $("#diag_add_transpo").modal("hide");
            swal("You are not allowed to add a transportation schedule.", "", "warning")
        }

    });

    $(document).on("click", ".edit", function () {

        $("#viewTask").modal("toggle");

       

        var id = $(this).closest("tr").attr("id");
        $("#weekly_schedule").find("tr").css("background-color", "white");
        $("#weekly_schedule").find("tr[dhead]").css("background-color", "#20b4c5");
        currentId = id;
        var classes = $(this).closest("tr").attr("class");
        var c = classes.split(" ");
        var resident_id = $(this).closest("tr").attr('resident_id');
        comp_resident_id = resident_id;
        var appointmentid = $(this).closest("tr").attr('appointmentid');
        appointmenttype = $(this).closest("tr").attr('appointment_type');
        getHeightViewTaskModal(appointmenttype);
        var string = "";
        var completion_status = "";
        var remarks = "";
        var date = $(this).closest("tr").attr('date');
        var obj;
        var status;
        var titleDate;
        var guardian_comment = "";
        $(".is_complete").removeAttr("disabled");

        $(".forAdminOnly").hide();
        if ($(this).closest("tr").attr("appointment_type") == "transpo") { // TRANSPORTATION
            var table_id = $(this).closest('tr').attr('date');
            var date = table_id.replace(/_/g, "/");

            string = "<input class=\"r_currentdate\" type=\"hidden\"/><input class=\"r_resident_id\" type=\"hidden\" /><input class=\"r_resident_name\" type=\"hidden\" /><input class=\"r_transpo_id\" type=\"hidden\" />";
            string += "<input class=\"r_recurrence\" type=\"hidden\" /><input class=\"r_day_of_the_week\" type=\"hidden\" />";
            string += "<input class=\"r_day_of_the_month\" type=\"hidden\" /><input class=\"r_date\" type=\"hidden\" />";
            string += "<input class=\"r_start_time\" type=\"hidden\" /><input class=\"r_end_time\" type=\"hidden\" />";
            string += "<input class=\"r_purpose\" type=\"hidden\" /><input class=\"r_staff_id\" type=\"hidden\" />";

            //Start of outer div
            string += "<div style=\"margin-left: -20px;\" class=\"col-lg-12 col-xs-12\">";

            //Only Admin and Dept Heads should be able to add/edit/delete 
            if ($.inArray(roleID, adminRoles) !== -1) {

                $(".forAdminOnly").show();
                $(".reassign").attr("id", 1);
                $(".reschedd").attr("id", 2);


                $(".reassign").attr("data-target", "#diag_add_transpo");
                $(".reschedd").attr("data-target", "");

                var addsched = "<button style=\"\" data-toggle=\"modal\" data-target=\"#diag_add_transpo\" class=\"addnewsched reassched is_complete btn btn-success reassignTask\" type=\"submit\" id=\"3\" ><span id=\"btn_label\">Create New Schedule</span></button>";

                $("#aSched").html(addsched);
                $("#aSched").css("padding", "3px");
                var delTask = ""
                $("#deleteTask").html(delTask);
                $("#btn1_label").html("<span>Edit</span>");
                $("#btn2_label").html("<span>Delete</span>");
            }

            var str_1 = "";
            var str_2 = "";

            str_1 += "<div><span class=\"resName\" id=\"r_name\"></span>";

            str_2 += "<div><p class=\"stat dateshow\">Date: <b>" + date + "</p></div>";

            $("#res_name").html(str_1);
            $(".taskLabel").html(str_2);


            //----------------------------------------------------
            string += "<br/><table style=\"font-size: small;\" class=\"scheduleTable col-lg-12 col-xs-12\">";

            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">Recurrence: </label></td><td style=\"font-size:11px\"><b><span class=\"resValue\" id=\"r_recurrence\"></span></b></td>";
            string += "<td style=\"text-align: right;padding-right: 30px;font-size:11px;\"></td>";
            string += "</tr>";

            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Start Date: </label></td><td style=\"font-size:11px\"><b><span class=\"resValue\"  id=\"r_date\"></span></b></td>";
            string += "<td id=\"endDate_lbl\" style=\"text-align: right;padding-right: 30px;\"><label class=\"form-control-label\">End Date: </label></td><td style=\"font-size:11px\"><b><span class=\"resValue\" id=\"r_end_date\"></span></b></td>";
            string += "</tr>";

            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Start Time: </label></td><td><b><span  class=\"resValue\" id=\"r_start_time\"></span></b></td>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">End Time: </label></td><td style=\"font-size:11px\"><b><span  class=\"resValue\" id=\"r_end_time\"></span></b></td>";
            string += "</tr>";

            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Purpose: </label></td><td style=\"font-size:11px\" colspan=\"3\"><b><span  class=\"resValue\" id=\"r_purpose\"></span></b></td>";
            string += "</tr>";


            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Comments / Remarks: </label></td><td <td style=\"font-size: 11px;\" colspan=\"3\"><b><span id=\"r_comments\"></span></b></td>";
            string += "</tr>";

            string += "</table>";








            //outer panel end

            if (c[0] == "mcl") {

                $("#weekly_task").html("");
                string += "</div>";
                //end of outer Div

                $("#diag_view").html(string);

            } else {

                //string += "<button class=\"button\"  type=\"submit\" onClick=\"updateSchedule()\">Save</button>";
                string += "</div>";
                //end of outer Div
                $("#diag_view").html("");
                $("#weekly_task").html(string);


                date = new Date(date);

                $(this).parent().prev('div').show();
            }

            $(".r_currentdate").val(date);

            $(".date").datepicker({
                'formatDate': 'yyyy-mm-dd',
                'autoclose': true
            });

            //$(".time").timepicker({
            //    //   showMeridian: false
            //});

            // $('.time').mask('99:99 xy');

            var data = {
                transpo_id: appointmentid,
                date: date
            };
            var dw;


            $.ajax({
                url: "Transsched/GetScheduleData",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                data: JSON.stringify(data),
                success: function (response) {

                    var obj = response["data"][0];
                    var isOnce = obj.recurrence;
                    var comments = response["comments"][0];

                    if (comments != undefined) {

                        var d1 = new Date(date);
                        var d2 = new Date(comments.current_date);

                        //i dunno how to equal T_T
                        if (!(d1 > d2) && !(d1 < d2)) {

                            $("#r_comments").html(comments.comment);

                        }

                    }

                    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

                    for (var key in obj) {
                        var value = obj[key];

                        if (obj.hasOwnProperty(key)) {

                            if (key == "recurrence" || key == "day_of_the_week") {
                                if (value == "biweekly") {
                                    dw = obj["day_of_the_week"].split(",");

                                    $("#r_day_of_the_week").html(days[dw[0]] + ", " + days[dw[1]]);

                                    $("#datesched").html("<tr><td class=\"schedTitle\"><h4><small>Day of the Week: </small><span class=\"dwv v_weekly v_biweekly resValue\"></span> <span  class=\"resValue\" id=\"r_day_of_the_week\">" + days[dw[0]] + ", " + days[dw[1]] + "</span></h4></td></tr>");

                                } else if (value == "monthly") {

                                    $("#datesched").html("<tr><td class=\"schedTitle\"><h4><small>Day of the Month: </small><span class=\"dwv v_monthly resValue\" id=\"r_day_of_the_month\">" + obj["day_of_the_week"] + "</span></h4></td>");


                                } else if (value == "quarterly") {

                                    $("#datesched").html("<tr><td class=\"schedTitle\"><h4><small>Date: <span class=\"dwv v_quarterly resValue\" id=\"r_quarterly_dates\">" + obj["day_of_the_month"] + "</span></small></h4></td></tr>");


                                } else if (value == "annually" || value == "daily") {

                                    $("#datesched").html("");


                                } else {
                                    $("#r_day_of_the_week").html(days[value]);
                                    $("#datesched").html("<tr><td class=\"schedTitle\"><h4><small>Day of the Week: </small><span class=\"dwv v_weekly v_biweekly resValue\"></span> <span  class=\"resValue\" id=\"r_day_of_the_week\">" + days[obj["day_of_the_week"]] + "</span></h4></td></tr>");
                                }

                                if (value == "once") {
                                    isOnce = true;
                                    $("#endDate_lbl").hide();
                                    $("#datesched").html("");
                                    //$("#rnd").hide();

                                } else {

                                    $("#endDate_lbl").show();
                                    $("#rnd").show();

                                }

                                $(".v_" + value).show();

                                $("#r_recurrence").html(obj["recurrence"]);

                            } else if (key == "end_date") {
                                if (value != null) {

                                    $("#r_end_date").html(value);
                                    //$("#r_end_date").removeClass("hasDatepicker");
                                    //$("#r_end_date").removeClass("date");
                                    //$("#r_end_date").attr("disabled", "disabled");
                                    $("#create_new_schedule").removeAttr("disabled");
                                    $("#enddate_notif").html("");

                                } else {

                                    $("#r_end_date").html("");
                                    if (isOnce != "once") {
                                        $("#create_new_schedule").attr("disabled", "disabled");
                                    }
                                    $("#enddate_notif").html("Please click 'Save' button to update end date.");
                                }

                                if (isOnce == "once") {

                                    $("#enddate_notif").html("N/A");
                                }

                            } else if (key == "name") {

                                $("#r_resident_name").val(value);
                                $("#r_name").html(value);

                            } else if (key == "start_time") {
                                var stime = ampm(value);

                                $("#r_start_time").html(stime);

                            } else if (key == "end_time") {

                                var etime = ampm(value);

                                $("#r_end_time").html(etime);

                            } else {


                                $("#r_" + key).html(value);

                            }

                            $(".r_" + key).val(value);

                        }
                    }
                }
            });

            if (c[0] == "mcl") {

                //conflict
                //showdiag(0, titleDate);
            }

        } else { // LARGE TASK 
            if ($(this).closest("tr").attr("appointment_type") == "largetask") {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Staff/GetGridData?func=activity_schedule&param=" + date,
                    dataType: "json",
                    async: false,
                    success: function (m) {
                        var indexes = $.map(m, function (obj, index) {
                            if (obj.activity_schedule_id == appointmentid) {
                                return index;
                            }
                        })

                        obj = m[indexes[0]];


                    }
                });

                $("#gComment_division").hide();
            } else {

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Staff/GetGridData?func=activity_schedule&param=" + date + "&param3=" + resident_id,
                    dataType: "json",
                    async: false,
                    success: function (m) {
                        var indexes = $.map(m, function (obj, index) {
                            if (obj.activity_schedule_id == appointmentid) {
                                return index;
                            }
                        })

                        obj = m[indexes[0]];


                    }
                });
                //   $("#gComment_division").hide();

                csid = $(this).closest("tr").attr("csid");
                did = $(this).closest("tr").attr("did");
                cid = $(this).closest("tr").attr("cid");
                currentResId = resident_id;

            }
            $(".is_complete").removeAttr("hidden");
            
            status = obj.activity_status == 1 ? '<span class="completed">Completed</span>' : '<span class="comp-no">Not Completed</span>';
            var cmplt = obj.activity_status == 1 ? 'Yes' : 'No';

            var prev_stat = obj.activity_status == 1 ? 'Yes' : 'No';
            //remarks = "<b><span>" + obj.remarks + "</span></b>";

            if (obj.carestaff_activity_id == null && obj.activity_status == 0)
                status = "<span class=\"na\">N/A</span>";
            remarks = "<textarea class=\"form-control is_complete\" id=\"scheduleRemarks\"  name=\"remarks\" maxlength=\"1000\"></textarea>";
            if (obj.carestaff_activity_id == null && obj.timelapsed) {
                status = '<span class="missed">Missed</span>';
                cmplt = "Missed";
                prev_stat = "Missed";
                remarks = "<textarea class=\"form-control is_complete\" id=\"scheduleRemarks\"  name=\"remarks\" maxlength=\"1000\"></textarea>";
            }
            if (obj.carestaff_activity_id == null && obj.timelapsed == false) {
                prev_stat = "n/a";
            }
            completion_status += "<tr><td colspan=\"1\"><label class=\"checkbox\"><input class=\"is_complete chck\" type=\"checkbox\" id=\"taskYes\" value=\"False\" name=\"No\">YES</label></td>";
            completion_status += "<td  colspan=\"1\"><label class=\"checkbox\"><input class=\"is_complete chck\" type=\"checkbox\" id=\"taskNo\" value=\"True\" name=\"No\">NO</label></td>";

            var str_1 = "";
            var str_2 = "";

            str_1 += "<div><span class=\"resName\">" + (obj.resident == null ? obj.department : obj.resident) + "</span></div>";

            str_2 += "<div id=\"taskdetaill\" style=\"text-align: left;\"><p class=\"stat\">Date: <b>" + date + " | </b> Status: " + status + "<br/>Acknowledged By: <b>" + (obj.acknowledged_by != "" ? obj.acknowledged_by : "N/A") + "</b></p><span id=\"admissionstatus\" style=\"color: #ab0404; font-weight: bold;\"></span></div>";

            $("#res_name").html(str_1);
            $(".taskLabel").html(str_2);
            $("#create_new_schedule").css("visibility", "hidden");
            //outer div
            string += "<div style=\"margin-left: -20px;\" class=\"col-lg-12 col-xs-12 \">";
            if ($.inArray(roleID, adminRoles) !== -1) {
                $(".forAdminOnly").show();
                var rschedID = +obj.activity_schedule_id + ", " + date + ",1";
                var rassignID = +obj.activity_schedule_id + ", " + date + ",2";
                $(".reschedd").attr("id", rschedID);
                $(".reassign").attr("id", rassignID);
                $(".reassign").attr("data-target", "#diagReschedule");
                $(".reschedd").attr("data-target", "#diagReschedule");
                var addsched = "";

                var delTask = "<button style=\"\" class=\"delTask reassched is_complete btn btn-success reassignTask\" type=\"submit\" id=\"4\" del_id=\"\" ><span id=\"btn_label\"><i class=\"la la-trash\"></i>&nbsp; Deactivate</span></button>";
                $("#aSched").html(addsched);
                $("#deleteTask").html(delTask);
                $("#deleteTask").css("padding", "3px");

                $(".delTask").attr("del_id", obj.activity_schedule_id);
                $("#btn1_label").html("<i class=\"la la-repeat\"></i>Reassign");
                $("#btn2_label").html("<i class=\"la la-random\"></i>Reschedule");

            }
            string += "<br/><table style=\"font-size: small;\" class=\"scheduleTable col-lg-12 col-xs-12\">";


            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Activity: </label></td><td style=\"font-size:11px\"><b>" + (obj.activity_desc == null ? "N/A" : obj.activity_desc) + "</b></td>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label for=\"room\"class=\"form-control-label\">Room: </label></td><td style=\"font-size:11px\"><b>" + (obj.room != null ? obj.room : "N/A") + "</b></td>";
            string += "</tr>";


            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Procedure: </label></td><td style=\"font-size:11px\"><b>" + (obj.activity_proc == null ? "N/A" : obj.activity_proc) + "</b></td>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Schedule: </label></td><td style=\"font-size:11px\"><b>" + obj.start_time + "</b></td>";
            string += "</tr>";


            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label for=\"carestaff\"class=\"form-control-label\">Carestaff: </label></td><td style=\"font-size:11px\"><b>" + (obj.carestaff_name == null ? "N/A" : obj.carestaff_name) + "</b></td>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Duration<br/> (Minutes): </label></td><td style=\"font-size:11px\"><b>" + obj.service_duration + "</b></td>";
            string += "</tr>";


            string += "<tr>";
            string += "<td style=\"text-align: right;padding-right: 15px;\"><label class=\"form-control-label\">Department: </label></td><td style=\"font-size:11px\"><b>" + (obj.department == null ? "N/A" : obj.department) + "</b></td>";
            string += "<td></td>";
            string += "</tr>";

            string += "</table>";

            //inner div
            string += "<div class=\"m-separator m-separator--dashed\"></div>";
            string += "<div style=\"font-size:medium;\" class=\"form-group m-form__group\">";

            //div_1
            string += "<div class=\"m-form__group form-group\">";

            string += "<div class=\"m-checkbox-inline\"><label for=\"\">Is this task completed? &nbsp; &nbsp;</label><label  class=\"m-checkbox tskbtn\">YES <input class=\"is_complete chck\" type=\"checkbox\" id=\"taskYes\" value=\"False\" name=\"No\"><span></span></label>";
            string += "<label class=\"m-checkbox tskbtn\">NO <input class=\"is_complete chck\" type=\"checkbox\" id=\"taskNo\" value=\"True\" name=\"No\"><span></span></label></div></div>";

            var cdate = new Date(obj.reschedule_dt);
            var dday = cdate.getDate();
            //div_2
            //string += "<div><span style='font-size:12px'><b>Remarks:</b></span> <span class=\"empty_remarks\" style=\"display:none;color:red;font-style:italic;font-size:12px\">&nbsp;** Remarks is required when completing a task previously marked as uncompleted.</span><br/>";

            //string += (obj.remarks == null ? (obj.reschedule_dt != null ? "Rescheduled - <a href=\"#\" xref=\"arwdailytask" + obj.xref + "" + obj.resident_id + "" + dday + "\" class=\"resched\">" + obj.reschedule_dt + "</a>" : "") : remarks) + "</div>";
            //string += ((obj.remarks != "") ? (obj.reschedule_dt == null ? "<div style='padding-top:5px'><span style='font-size:12px'><b>Task completion details:</b></span><textarea rows='5' readonly='readonly' class='form-control'>" + (obj.remarks == null ? "" : obj.remarks) + "</textarea></div>" : "") : obj.remarks);

            string += "<div><span style='font-size:12px'><b>Remarks:</b></span> <span class=\"empty_remarks\" style=\"display:none;color:red;font-style:italic;font-size:12px\">&nbsp;** Remarks is required</span><br/>";

            var o_remarks;
            if (obj.reschedule_dt != null && (obj.completion_type != 3 || obj.completion_type != 2)) {
                o_remarks = obj.remarks.split('-');
            }
            if (obj.is_active == false) { //if task is deactivated within the day
              
                remarks = "<textarea class=\"form-control is_complete\" id=\"scheduleRemarks\"  name=\"remarks\" maxlength=\"1000\">This task has been deactivated. - " + obj.active_until +"</textarea>"; 
            }
            string += ((obj.remarks != null || obj.remarks != "") && (obj.reschedule_dt != null && (obj.completion_type == 3 || obj.completion_type == 2)) ? o_remarks[0] + "- <a href=\"#\" xref=\"arwdailytask" + obj.xref + "" + obj.resident_id + "" + dday + "\" class=\"resched\">" + obj.reschedule_dt + "</a>" : remarks) + "</div>";
            string += (obj.remarks != "" ? "<div style='padding-top:5px'><span style='font-size:12px'><b>Task completion details:</b></span><textarea rows='5' readonly='readonly' class='form-control'>" + (obj.remarks == null ? "" : obj.remarks) + "</textarea></div>" : "");
            string += "<div><input type='hidden' name='previous_status' value='" + prev_stat + "'/></div>";



            //div_3
            string += "<div id=\"gComment_div\" style=\"margin-top: 20px;\"></div>"
            string += "<div style=\"display:none;\" id=\"gComment_division\"><br><div class=\"m-separator m-separator--dashed\"></div><span style='font-size:12px;'><b>Guest Comment/s:</b></span><br><br>";
            string += "<div id=\"gComment\"readonly class=\"form-control\" maxlength=\"1000\"></div>";

            //string += "<textarea id=\"gComment_textarea\"readonly class=\"form-control\" maxlength=\"1000\"></textarea>";
            string += "<input type=\"hidden\" id=\"CarestaffActivityId\" name=\"CarestaffActivityId\" value=\"" + obj.carestaff_activity_id + "\">";
            string += "<input type=\"hidden\" id=\"ActivityScheduleId\"name=\"ActivityScheduleId\" value=\"" + obj.activity_schedule_id + "\">";
            string += "<input type=\"hidden\" id=\"Resident\"name=\"Resident\" value=\"" + obj.resident + "\">";
            string += "<input type=\"hidden\" id=\"CarestaffName\"name=\"CarestaffName\" value=\"" + obj.carestaff_name + "\">";
            string += "<input type=\"hidden\" id=\"ActivityDesc\"name=\"ActivityDesc\" value=\"" + obj.activity_desc + "\">";
            string += "<input type=\"hidden\" id=\"completion\"name=\"completion\" value=\"" + date + "\">";
            string += "<input type=\"hidden\" id=\"id\" name=\"id\" value=\"" + id + "\">";
            string += "<input type=\"hidden\" id=\"id\" name=\"Department\" value=\"" + obj.department + "\">";
            string += "<input type=\"hidden\" id=\"\" name=\"IsMaintenance\" value=\"" + obj.is_maintenance + "\">";
            string += "<br /><br /></div>";

            //end of inner div
            string += "</div>";
            // string += "<div class=\"m-separator m-separator--dashed\"></div>";
            //end of outer div
            string += "</div>";

            $("#weekly_task").html(string);
            $("#updateTask").attr("disabled", "disabled");
            //$("#scheduleRemarks").html(obj.remarks);
            if ($.inArray(roleID, adminRoles) == -1) {

                $("div#taskdetaill").css({ "margin-left": "380px", "width": "100%" });
            }
            if ($(this).closest("tr").attr("appointment_type") == "dailytask") {
                $("#gComment_div").html("<div class=\"m-separator m-separator--dashed\"></div><a href=\"#\" id=\"view_gComment\" class=\"btn btn-accent  m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill add\" style=\"padding: 8px;\"><span><i class\"m-nav__link-icon flaticon-event-calendar-symbol\"></i><span>View Guest Comment</span></span></a>");
                $("#gComment_div").show();
            }

            var dt = new Date(date);
            var dtnow = new Date(Date.now());
            dtnow.setHours(0, 0, 0, 0);

            //from previous version starts here
          
            if (cmplt != "Missed" || +dt != +dtnow) {
                //  $(".reassched").attr("disabled", "disabled");
                //$(".reschedd").attr("disabled", "disabled");
                //$(".reassign").attr("disabled", "disabled");
                $(".reschedd").attr("hidden", "hidden");
                $(".reassign").attr("hidden", "hidden");
                $("#deleteTask").css("margin-left", "auto");
            }
            if (cmplt != "Missed") {
                //$(".is_complete").attr("disabled", "disabled");
                $(".is_complete").attr("hidden", "hidden");
            }
            // from previous version ends here

            //  $(".delTask").attr("disabled", false);
            $(".delTask").removeAttr("hidden");

            if (+dt < +dtnow || obj.carestaff_activity_id != null) {
                //  $(".delTask").attr("disabled", true);
                $(".delTask").attr("hidden", "hidden");
            }

            if (obj.is_active == false) { //if task is deactivated within the day
                $(".reschedd").attr("hidden", "hidden");
                $(".reassign").attr("hidden", "hidden");
                $(".delTask").attr("hidden", "hidden");
               
            }
            if (obj.reschedule_dt != null) {
                //$(".reschedd").attr("disabled", "disabled");
                //$(".reassign").attr("disabled", "disabled");
                $(".reschedd").attr("hidden", "hidden");
                $(".reassign").attr("hidden", "hidden");
                $(".chck").attr("disabled", "disabled");
            }
            if (obj.isAdmitted == false) {
                //$(".reassched").attr("disabled", "disabled");
                //$(".is_complete").attr("disabled", "disabled");
                $(".reschedd").attr("disabled", "disabled");
                $(".reassign").attr("disabled", "disabled");
                $(".is_complete").attr("disabled", "disabled");
                $("#admissionstatus").html("DISCHARGED");
            }

            if (obj.carestaff_activity_id != null) {
                $("#task" + cmplt).prop("checked", true);
            }


        }
        $('.chck').on('change', function () {
            $('.chck').not(this).prop('checked', false);

            var len = $(".chck:checked").length;

            if (len > 0) {
                $("#updateTask").removeAttr("disabled");
            } else {
                $("#updateTask").attr("disabled", "disabled");
            }
        });

    });

    $(document).on("click", ".reassignTask", function () {
        var id = $(this).attr('id');
        var d_id = $(this).attr('del_id');
       

        if (id == 1) { //edit Schedule
            var resident_id = $(".r_resident_id").val();
            var name = $("#r_name").text();
            $("#comment_section").show();
            if (name == "") {

                name = $("#name").text();
            }
            $("#transpo_label").html("<span>Edit Schedule</span>")

            $("#c_resident").html(name);
          
            editSchedule();
        } else if (id == 2) { //delete schedule
            var m = 0;
            deleteSchedule(m);
        } else if (id == 3) { //add new schedule
            $("#transpo_label").html("<span>Add Schedule</span>");
            getStaff();

            var resident_id = $(".r_resident_id").val();
            var name = $("#r_name").text();

            if (name == "") {
                name = $("#name").text();
            }

            $("#r_residents").hide();
            $("#c_resident").html(name);
            $("#c_resident").show();
            $("#cn_resident").val(resident_id);
            $("#comment_section").hide();
            $("#end_date").val("").attr("disabled", "disabled");
         
            // saveTranspo();
        } else if (id == 4) {
            var m = 1;
            deleteSchedule(m, d_id);

        } else { //reassign & reschedule
            var param = id.split(",");
            $(".reassign").attr("data-target", "#diagReschedule");
            $("#reassignTask").val(param[2]);
            $("#txtActivityR").attr("disabled", true);
           
            resched(param[0], param[1], param[2]);
        }
    });

    $("#recurrence").on("change", function () {
        var id = "r_" + $(this).val();
        $(".r_view").hide();
        $("#" + id).show();
        $("#end_date").removeAttr("disabled");
        if ($(this).val() == "once") {
            $("#end_date").val("").attr("disabled", "disabled");
        } else if ($(this).val() == "annually") {
            $("#start_date_lbl").html("Set Day of the Year:");
        } else {
            $("#start_date_lbl").html("Start Date:");
        }

    })
    //Start of Weekly, Daily and Monthly View===============================================================
    $("#weekly_view").on("click", function () {
        $("#calendar").hide();
        $("#weekly").DataTable().destroy();
        filtercal = getFilter();

        $(".legend").hide();
        $(".cal_header").hide();
        $(".filtr").show();
        $(".calviewftr").hide();
        $(".taskviewftr").show();
        $(".currentTaskbtn").hide();
        $(".calendarbtn").show();
        $("#diag_view").html("");
        $("#weekly_task").html("");
        var cdate = new Date();
        var current_day = cdate.getDay();
        var startWeek = new Date(cdate);
        startWeek.setDate(cdate.getDate() - current_day);
        var endWeek = new Date(cdate);
        endWeek.setDate(cdate.getDate() + (6 - current_day));

        if (startDateNew != "") {
            startWeek = startDateNew;
            endWeek = endDateNew;
        } else {
            var cdate = new Date($("#curday").text());

            var current_day = cdate.getDay();
            var startWeek = new Date(cdate);
            startWeek.setDate(cdate.getDate() - current_day);
            var endWeek = new Date(cdate);
            endWeek.setDate(cdate.getDate() + (6 - current_day));

        }

        // var currentDate = (cdate.getMonth() + 1) + "_" + cdate.getDate() + "_" + cdate.getFullYear();
        var curDay = $("#curday").text();
        var curDayOfWeek = $(".week-picker").val();

        $("#cd_label").text("Current Day");
        $("#cw_label").text("Current Week");

        var a = new Date(startWeek);
        var b = new Date(endWeek);
        $("#wstartDate").text((a.getMonth() + 1) + "/" + a.getDate() + "/" + a.getFullYear());
        $("#wendDate").text((b.getMonth() + 1) + "/" + b.getDate() + "/" + b.getFullYear());
        $("#curday").text("");
        $("#weekpicker").val("");
        $("#daypicker").val("");

        if (isCalendarView == 1 && wpicker1 != "") {

            startWeek = wpicker1;
            endWeek = wpicker2;

            $("#wstartDate").text(wpicker1);
            $("#wendDate").text(wpicker2);
            $("#weekpicker").val(wpicker1 + " - " + wpicker2);
        }

        $('.loader').show();
        setTimeout(function () {
            $("#weekly").DataTable().destroy();
            generateWeeklySchedule(startWeek, endWeek, filtercal);
            createDatatable();
            weekly_table.page('first').draw('page');
            $('.loader').hide();
            $("#weekly_wrapper").show();
        }, 100)



        loadCurrentWeek = false;
        isCalendarView = 0;
        isDailyView = 0;
        getHeight();
    });

    $("#daily_view").on("click", function () {
        $("#calendar").hide();
        $("#monthfilter").val(current_month);
        $("#yrfilter").val(current_year);
        $("#weekly").DataTable().destroy();

        filtercal = getFilter();


        $(".legend").hide();
        $(".cal_header").hide();
        $(".filtr").show();
        $(".calviewftr").hide();
        $(".taskviewftr").show();
        $(".currentTaskbtn").hide();
        $(".calendarbtn").show();
        $("#diag_view").html("");
        $("#weekly_task").html("");

        var currentDate;
        //if ($(".day-picker").val() == "") {
        if (($("#curday").text() == "") && ($("#wstartDate").text() == "")) {
            var cdate = new Date();
            currentDate = (cdate.getMonth() + 1) + "/" + cdate.getDate() + "/" + cdate.getFullYear();
        } else if (($("#curday").text() == "") && ($("#wstartDate").text() != "")) {
            if (_wpresentDate != "") {
                currentDate = _wpresentDate;
            } else {
                currentDate = $("#wstartDate").text();
            }

        } else {
            currentDate = $("#curday").text();
        }

        $("#cd_label").text("Current Day");
        $("#cw_label").text("Current Week");
        $("#wstartDate").text("");
        $("#wendDate").text("");
        $("#curday").text(currentDate);
        $("#weekpicker").val("");
        $("#daypicker").val("");

        if (isCalendarView == 1 && dpicker != "") {
            currentDate = dpicker;
            $("#daypicker").val(dpicker);
            $("#curday").text(dpicker);
        }
        generateDailySchedule(currentDate, filtercal);
        createDatatable();
        weekly_table.page('first').draw('page');
        $("#weekly_wrapper").show();


        isCalendarView = 0;
        isDailyView = 1;
        getHeight();

    });

    $("#monthly_view").on("click", function () {
        fromCalendar = 1;
        dpicker = $("#curday").text();
        wpicker1 = $("#wstartDate").text();
        wpicker2 = $("#wendDate").text();

        setTimeout(function () {
            buildCalendar();
        }, 100);

        isCalendarView = 1;
        $(".calviewftr").show();
        $(".legend").show();
        $(".cal_header").show();
        $(".filtr").hide();
        $("#weekly_wrapper").hide();
        $("#calendar").show();
        $(".calendarbtn").hide();
        $(".taskviewftr").hide();
        $(".currentTaskbtn").show();
        $("#cd_label").text("Task List Daily View");
        $("#cw_label").text("Task List Weekly View");

        getHeight();
        $(".dw_btns").css("padding-right", "5px");
    });
    //End of Weekly, Daily and Monthly View===============================================================
    $(document).on("click", ".resched", function (e) {

        var date = $(this).text().split(" ");
        var id = $(this).attr("xref");
        var inRange = true;

        var curr = new Date(date[0]); // get current date
        var resched_date = new Date(curr);
        var c = new Date(curr.setDate(curr.getDate() + 1)).toUTCString();
        var p_date = new Date(date_today);


        var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        var last = first + 6; // last day is the first day + 6

        var firstday = new Date(curr.setDate(first))
        firstday.setHours(0, 0, 0, 0);
        var lastday = new Date(curr.setDate(last));
        lastday = new Date(lastday);
        lastday.setHours(0, 0, 0, 0);
        resched_date.setHours(0, 0, 0, 0);

        if (isDailyView == 1) {
            $(".cancel").click();
            filtercal = getFilter();
            $("#weekly").DataTable().destroy();
            $("#curday").text(date[0]);
            generateDailySchedule(date[0], filtercal, focus);
            numberOfPages = createDatatable();
            page = numberOfPages.pages;
            getHeight();

            $("#daypicker").val("");

            if (page != 1) {
                for (var i = 0; i < page ; i++) {
                    var res = $("#weekly_schedule #" + id).find("td .edit");
                    if (res.length != 0) {
                        setTimeout(function () {
                            $("#weekly_schedule #" + id).find("td .edit").trigger("click");
                            $("#" + id).css("background-color", "#ebedf2");
                        }, 500);
                        return;
                    } else {
                        weekly_table.page('next').draw('page');
                    }
                }
            } else {
                setTimeout(function () {
                    $("#weekly_schedule #" + id).find("td .edit").trigger("click");
                    $("#" + id).css("background-color", "#ebedf2");
                }, 500);
            }

        } else {
            $(".cancel").click();
            $("#weekly").DataTable().destroy();
            generateWeeklySchedule(firstday, lastday, filtercal);
            numberOfPages = createDatatable();
            page = numberOfPages.pages;
            getHeight();
            $("#wstartDate").text((firstday.getMonth() + 1) + "/" + firstday.getDate() + "/" + firstday.getFullYear());
            $("#wendDate").text((lastday.getMonth() + 1) + "/" + lastday.getDate() + "/" + lastday.getFullYear());


            if (page != 1) {
                for (var i = 0; i < page; i++) {
                    var res = $("#weekly_schedule #" + id).find("td .edit");
                    if (res.length != 0) {
                        setTimeout(function () {
                            $("#weekly_schedule #" + id).find("td .edit").trigger("click");
                            $("#" + id).css("background-color", "#ebedf2");
                        }, 500);
                        return;
                    } else {
                        weekly_table.page('next').draw('page');

                    }
                }
            } else {
                setTimeout(function () {
                    $("#weekly_schedule #" + id).find("td .edit").trigger("click");
                    $("#" + id).css("background-color", "#ebedf2");
                }, 500);
            }


        }



    });

    setTimeout(function () {

        var cdate = new Date();
        var current_day = cdate.getDay();
        var startWeek = new Date(cdate);
        startWeek.setDate(cdate.getDate() - current_day);
        $("#calendarLayoutContainer").css("overflow", "auto");
        $("#calendar").hide();
        $("#dayReload").trigger("click");

        presentDate = $("#curday").text();
        calfilters();

        var scroll1 = new PerfectScrollbar('#transchedContainer');


        // getFilter();
        //conflict in js
        //var container = $("#weekly_schedule"),
        //    scrollTo = $("#" + (cdate.getMonth() + 1) + "_" + cdate.getDate() + "_" + cdate.getFullYear() + "fcs");

        //container.animate({
        //    scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
        //});

    }, 300);


    //window.addEventListener("click", function (e) {
    //    debugger
    //    var target = $(e.target);

    //    if (!(target.children('div.bootstrap-timepicker-widget').length)) {
    //        var time_valR = $("#txtTimeInR").val();
    //        var time_val = $("#txtTimeIn").val();
    //        if (time_valR == "") {
    //            $("#txtTimeInR").val("");
    //        }
    //        if (time_val == "") {
    //            $("#txtTimeIn").val("");
    //        }
    //    } 
    //});

    $('#start_time').timepicker({
        'timeFormat': 'H:i',
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true

    });
    $('#end_time').timepicker({
        'timeFormat': 'H:i',
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true

    });

});
