﻿var currentTime = "";
var currentDate = "";
var tasktype = "";
var _carestaff = "";
var dayOfweek = [];
var origState = "";
//$(function () {
    var triggerDayofWeekClick = function () {
        
        var a = $('#diagTask #txtTimeIn').val();
        if (a == null || a == '') {
            $('#diagTask #txtCarestaff').empty();
            //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
            return;
        }

        var dow = [];
        $('#diagTask .dayofweek').find('input[type="checkbox"]').each(function () {
            var d = $(this);
            if (d.is(':checked')) {
                switch (d.attr('name')) {
                    case 'Sun':
                        dow.push('0');
                        break;
                    case 'Mon':
                        dow.push('1');
                        break;
                    case 'Tue':
                        dow.push('2');
                        break;
                    case 'Wed':
                        dow.push('3');
                        break;
                    case 'Thu':
                        dow.push('4');
                        break;
                    case 'Fri':
                        dow.push('5');
                        break;
                    case 'Sat':
                        dow.push('6');
                        break;
                }
            }
        });
        bindCarestaff2(dow);

    }
    
    var bindCarestaff2 = function (dow) {
        
        if ($('#diagTask #txtTimeIn').val() == '') {
            $('#diagTask #txtCarestaff')[0].options.length = 0;
            return;
        }
        if ($('#diagTask #txtActivity').val() == '') {
            $('#diagTask #txtCarestaff')[0].options.length = 0;
            return;
        }
        if (dow == null) dow = [];

        //var s_timee = "";
        //sched_time = $('#diagTask #txtTimeIn').val().split(":");
      
        //debugger
        //if (parseInt(sched_time[0]) > 12) {
        //    var time = parseInt(sched_time[0]) - 12;
        //    if (time < 10) {
        //        time = "0" + time;
        //    }
        //    s_timee = time + ":" + sched_time[1] + " PM";
        //} else if (parseInt(sched_time[0]) == 12) {
        //    s_timee = "00:" + sched_time[1] + " AM";
        //} else {
        //    s_timee =  sched_time[0] + ":" + sched_time[1] + " AM";
        //}

        var data = {
            scheduletime: $('#diagTask #txtTimeIn').val(),
            activityid: $('#diagTask #txtActivity').val(),
            carestaffid: null,
            recurrence: dow.join(',')
        };
        data.isonetime = "1";
        data.clientdt = moment($('#diagTask #Onetimedate').val()).format("MM/DD/YYYY");        
        data.type = 1;
        
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetCarestaffByTime",
            data: JSON.stringify(data),
            async: false,
            dataType: "json",
            success: function (m) {
                $('#diagTask #txtCarestaff').empty();
                $('<option/>').val('').html('').appendTo('#diagTask #txtCarestaff');
                $.each(m, function (i, d) {
                    if (d.Name != null) {
                        $('<option/>').val(d.Id).html(d.Name).appendTo('#diagTask #txtCarestaff');
                    }
                });
            }
        });
    }
    var doAjaxTask = function (mode, row, cb) {
        
        var isValid = $('#diagTask').validate()

        //do other modes

        if (mode == 1 || mode == 2) {

            if (($('#diagTask #ddlDept').val() == 5) || ($('#diagTask #ddlDept').val() == 7)) {
                $("#diagTask #txtResidentId").removeAttr("required");
            }

            if (($('#diagTask #ddlDept').val() == 4) || ($('#diagTask #ddlDept').val() == 6)) {
                $("#diagTask #txtRoomId").removeAttr("required");
            }

            var form = document.getElementById('diagTaskmodal_form');
            for (var i = 0; i < form.elements.length; i++) {
                if (form.elements[i].value === '' && form.elements[i].hasAttribute("required")) {
                    swal("<span>Fill in all required (<span style=\"color: red;\">*</span>) fields. </span>")
                    return false;
                }
            }

            if (!isValid) {
                return;
            }
            row = $('#diagTask').extractJson();
            //validate time


            //var aTask = $('#diagTask #txtTimeIn').val().split(":");
            //var aTime = aTask[0] + ":00 " + aTask[1];
            //if (aTask[0] < 10) {
            //    aTime = "0" + aTask[0] + ":" + aTask[1];
            //} else aTime = $('#diagTask #txtTimeIn').val();

            //var s_timee = "";
            //var sched_time = $('#diagTask #txtTimeIn').val().split(":");
            

            //if (parseInt(sched_time[0]) > 12) {
            //    var time = parseInt(sched_time[0]) - 12;
            //    if (time < 10) {
            //        time = "0" + time;
            //    }

            //    s_timee = time + ":" + sched_time[1] + " PM";
            //} else if (parseInt(sched_time[0]) == 12) {
            //    s_timee = "00:" + sched_time[1] + " AM";
            //} else {
            //    s_timee = sched_time[0] + ":" + sched_time[1] + " AM";
            //}

        
            row.ScheduleTime = $('#diagTask #txtTimeIn').val();

            var dow = [];

            delete row['Sun'];
            delete row['Mon'];
            delete row['Tue'];
            delete row['Wed'];
            delete row['Thu'];
            delete row['Fri'];
            delete row['Sat'];
            delete row['recurring'];
            delete row['Activity'];
            delete row['Dept'];
            delete row['Onetimedate'];


            var isrecurring = $('#diagTask #reccur').is(':checked');
            if (isrecurring) {
                //  debugger
                row.isonetime = '0';
                $('#diagTask .dayofweek').find('input[type="checkbox"]').each(function () {
                    var d = $(this);
                    if (d.is(':checked')) {
                        switch (d.attr('name')) {
                            case 'Sun':
                                dow.push('0');
                                break;
                            case 'Mon':
                                dow.push('1');
                                break;
                            case 'Tue':
                                dow.push('2');
                                break;
                            case 'Wed':
                                dow.push('3');
                                break;
                            case 'Thu':
                                dow.push('4');
                                break;
                            case 'Fri':
                                dow.push('5');
                                break;
                            case 'Sat':
                                dow.push('6');
                                break;
                        }
                    }
                });

                if (dow.length == 0) {
                    swal('Please choose day of week schedule.');
                    return;
                }

            } else {
                
                row.isonetime = '1';
                var onetimeval = $('#diagTask #Onetimedate').val();
                if (onetimeval == '') {
                    swal("Please choose a one time date schedule.");
                    return;
                }
              
               
                var onetimeday = moment(onetimeval).day();
                if ($.isNumeric(onetimeday))
                    dow.push(onetimeday.toString());


         
                //var aTask = $('#txtTimeIn').val().split(":");
                //var aTime = "";

                //if (aTask[0] < 10 && aTask[0].length == 1) {
                //    aTime = "0" + aTask[0] + ":" + aTask[1];
                //} else aTime = $('#txtTimeIn').val();

                
            //var s_timee = "";
            //var sched_time = $('#diagTask #txtTimeIn').val().split(":");

                
            //if (parseInt(sched_time[0]) > 12) {
            //    var time = parseInt(sched_time[0]) - 12;
            //    if (time < 10) {
            //        time = "0" + time;
            //    }

            //    s_timee = time + ":" + sched_time[1] + " PM";
            //} else if (parseInt(sched_time[0]) == 12) {
            //    s_timee = "00:" + sched_time[1] + " AM";
            //} else {
            //    s_timee =  sched_time[0] + ":" + sched_time[1] + " AM";
            //}




            row.ScheduleTime = onetimeval + ' ' + $('#diagTask #txtTimeIn').val();
               
                var today = new Date(Date.now());
                var new_schedTime = new Date(row.ScheduleTime);

                if (new_schedTime < today) {
                    swal("Selected date and time have passed already. Please select again.", "", "warning");
                    return;
                }
                
            }
            row.Recurrence = dow.join(',');

            if ($('#diagTask #txtCarestaff').val() == "") {
                swal("Please select a carestaff.")
                return;
            }

        }

        if (mode == 2) {
            transchedState.current = JSON.stringify(row);
            if (transchedState.current == transchedState.orig) {
                swal("There are no changes to save.", "", "info");
                $(".loader").hide();
                return;
            }
        }
        // debugger
        var data = {
            func: "activity_schedule",
            mode: mode,
            data: row
        };


        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/PostGridData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);
            }
        });

    }
    var bindDept = function (dptid, cb) {
        var data = {
            deptid: dptid
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetServicesAndCarestaffByDeptId",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                //$('#txtCarestaff').empty();
                //$('<option/>').val('').html('').appendTo('#txtCarestaff');
                //$.each(m.Carestaffs, function (i, d) {
                //    $('<option/>').val(d.id).html(d.name).appendTo('#txtCarestaff');
                //});

                $('#diagTask #txtActivity').empty();
                $('#diagTask #viewActivity').val('');
                var opt = '<option val=""></option>';
                $.each(m.Services, function (i, d) {
                    opt += '<option title="' + (d.Proc || '') + '" value="' + d.Id + '">' + d.Desc + '</option>';
                });
                $('#diagTask #txtActivity').append(opt);
                if (typeof (cb) !== 'undefined') cb();
            }
        });
    }
    var toggleCarestafflist = function () {
        var isrecurring = $('#diagTask input[name="recurring"]:eq(0)').attr('checked');
        if (isrecurring)
            triggerDayofWeekClick();
        else
            $('#diagTask #Onetimedate').trigger('change');

        //if housekeeper or maintenance hide room dropdown
        var val = $('#diagTask #ddlDept').val();
        if (val == 5 || val == 7) {
            //$('#diagTask #liRoom, #diagTask #liRoom select').show();
            //$('#diagTask #liResident, #diagTask #liResident select').hide();
            $("#room_section").show();
            $("#resident_section").hide();
        } else {
            //$('#diagTask #liResident, #diagTask #liResident select').show();
            //$('#diagTask #liRoom, #diagTask #liRoom select').hide();
            $("#room_section").hide();
            $("#resident_section").show();
        }
    }




//});
 //Reschedule & Reassign
function resched(activity_schedule_id, date, type) {
    
    
    $('#diagReschedule #txtTimeInR').timepicker({
        // showMeridian: false, 
        timeFormat: 'hh:mm tt',
        defaultTime: '12:00',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {
            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#diagReschedule #txtCarestaffR').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }
            $('#diagReschedule #OnetimedateR').trigger('change');
        }

    });
    var id = activity_schedule_id.toString();
    //btnreassign == 2, reschedule == 1, 
    doAjax2(type, 0, {
        Id: id,
        date_created: date
    }, function (res) {
       
        currentTime = res.ScheduleTime;
        currentDate = moment(date).format("MM/DD/YYYY");

        $('#diagReschedule #txtCarestaffR').attr('xtype', type);
        $("#diagReschedule").clearFields();
        //$('#diagReschedule #txtActivityR').val(res.ActivityId);
        //$('#diagReschedule #txtActivityR').html(res.Activity);
        $('#diagReschedule #txtActivityR').empty();
        $('#diagReschedule #date_createdR').val(date);
        $('#diagReschedule #txtIdR').val(id);
        $('#diagReschedule #txtTimeInR').val(res.ScheduleTime);
        $('#diagReschedule #OnetimedateR').val(moment(date).format("MM/DD/YYYY"));

        $('<option/>').val(res.ActivityId).text(res.ActivityDesc).appendTo('#diagReschedule #txtActivityR');
        $('#viewActivityR').val(res.Activity);
         //$('<option/>').val(res.ActivityId).text(res.Activity).appendTo('#diagReschedule #txtActivityR');
         //$('#viewActivityR').val(res.ActivityDesc);

        $('#diagReschedule #txtCarestaffR').val(res.CarestaffId);
        $('#diagReschedule #txtCarestaffR').text(res.CarestaffName);
        _carestaff = res.CarestaffId;
      //  debugger
        //if housekeeper or maintenance hide room dropdown
        if (res.Dept == 5 || res.Dept == 7) {
            //$('#diagReschedule #liRoom, #diagReschedule #liRoom select').show();
            //$('#diagReschedule #liResident, #diagReschedule #liResident select').hide();
            $(".room_section").show();
            $(".resident_section").hide();
            $('#diagReschedule #txtRoomIdR, #diagReschedule #txtCarestaffR').empty();
            $('<option/>').val(res.RoomId).html(res.RoomName).appendTo('#diagReschedule #txtRoomIdR');
            
        } else {
            //$('#diagReschedule #liResident, #diagReschedule #liResident select').show();
            //$('#diagReschedule #liRoom, #diagReschedule #liRoom select').hide();
            $(".room_section").hide();
            $(".resident_section").show();
            $('#diagReschedule #txtResidentIdR, #diagReschedule #txtCarestaffR').empty();
            $('<option/>').val(res.ResidentId).html(res.ResidentName).appendTo('#diagReschedule #txtResidentIdR');
            
        }

        if (type != "2") {
        
            $("#changeCarestaff").show();
          //  $('#diagReschedule #txtCarestaffR').attr('disabled', 'disabled');
            $('#diagReschedule #OnetimedateR').show();
            $('#diagReschedule #txtTimeInR,#diagReschedule #OnetimedateR').removeAttr('disabled');

            $('#diagReschedule #OnetimedateR').datepicker({
                changeMonth: false,
                changeYear: false,
                minDate: 0,
            });

            $('#diagReschedule #OnetimedateR').change(function () {
                if (this.value == '') return;
                var dow = [];
                dow[0] = moment(this.value).day();
                var type = $('#diagReschedule #txtCarestaffR').attr('xtype');
                bindCarestaff(dow, parseInt(type));
            });
           
           
        } else {

            $("#changeCarestaff").hide();
            $('#diagReschedule #OnetimedateR').attr('disabled', 'disabled');
            $('#diagReschedule #OnetimedateR').css( 'background-color', '#f4f5f8');

            $('#diagReschedule #txtCarestaffR').removeAttr('disabled');

            $('#diagReschedule #txtTimeInR').change(function () {
                if (this.value == '') return;
                var dow = [];
                dow[0] = moment(this.value).day();
                var type = $('#diagReschedule #txtCarestaffR').attr('xtype');
                bindCarestaff(dayOfweek, parseInt(type));
            });

        }

        var title = "Reschedule Service Task";
        if (type == 2) {
            title = "Reassign Service Task";
        }
        $("#r_title").html("<span>" + title + "</span>")

        var dow = [];
        dow[0] = moment(date).day();
        bindCarestaff(dow, type);
        transchedState.orig = JSON.stringify($('#diagReschedule').extractJson('id'));
    });
    //  saveReassignRescheduleTask(2);

}
var doAjax2 = function (type, mode, row, cb) {
    //get record by id
     
    if (mode == 0) {
        var data = {
            func: "activity_schedule",
            id: row.Id
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Admin/GetFormData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (cb) cb(m);

            }
        });
        return;
    }
    //debugger;
    var isValid = $('#diagReschedule').validate();
    //do other modes
    if (mode == 1 || mode == 2) {
        if (!isValid) {
            return;
        }
        //  debugger
        row = $('#diagReschedule').extractJson();
     
        //validate time

        var dow = [];

        delete row['Onetimedate'];

        row.isonetime = '1';
        row.isRescheduled = '1';

        var onetimeval = $('#diagReschedule #OnetimedateR').val();
        if (onetimeval == '') {
            swal("Please choose a schedule date.");
            return;
        }

        var onetimeday = moment(onetimeval).day();
        if ($.isNumeric(onetimeday))
            dow.push(onetimeday.toString());

        
        //var aTask = $('#txtTimeInR').val().split(":");
        //var aTime = "";

        //if (aTask[0] < 10 && aTask[0].length == 1) {
        //    aTime = "0" + aTask[0] + ":" + aTask[1];
        //} else aTime = $('#txtTimeInR').val();
        //debugger
        
        //var s_timee = "";
        //var s = $('#diagReschedule #txtTimeInR').val().split(" ");
        //var sched_time = s[0].split(":");

        //if (s.length == 1) {
        //    if (parseInt(sched_time[0]) > 12) {
        //        var time = parseInt(sched_time[0]) - 12;
        //        if (time < 10) {
        //            time = "0" + time;
        //        }

        //        s_timee = time + ":" + sched_time[1] + " PM";
        //    } else if (parseInt(sched_time[0]) == 12) {
        //        s_timee = "00:" + sched_time[1] + " AM";
        //    } else {
        //        s_timee = "0" + sched_time[0] + ":" + sched_time[1] + " AM";
        //    }
        //} else {
        //    if (parseInt(sched_time[0]) > 12) {
        //        var time = parseInt(sched_time[0]) - 12;
        //        if (time < 10) {
        //            time = "0" + time;
        //        }

        //        s_timee = time + ":" + sched_time[1] + " PM";
        //    } else if (parseInt(sched_time[0]) == 12) {
        //        s_timee = sched_time[1] + " AM";
        //    } else {
        //        s_timee = sched_time[0] + ":" + sched_time[1] + " AM";
        //    }
        //}

        row.ScheduleTime = onetimeval + ' ' + $('#diagReschedule #txtTimeInR').val();

        row.Recurrence = dow.join(',');

        var d = moment(onetimeval).format("MM/DD/YYYY");
        //curdate = new Date();

        row.completion = d //moment(curdate).format("MM/DD/YYYY");
        row.clientdt = d //moment(curdate).format("MM/DD/YYYY");
        row.comptype = type;

        var RnewTime = $('#diagReschedule #txtTimeInR').val()
        var RnewDate = $('#diagReschedule #OnetimedateR').val()

        if (type == 1) {
            if (currentTime == RnewTime && currentDate == RnewDate) {
                swal("Please select new time and date for rescheduling.")
                return;
            }
        }

        if (($('#diagReschedule #txtCarestaffR').val()) == "") {
            swal("Please select a carestaff.");
            return;
        }
        

        var today = new Date(Date.now());
        var new_schedTime = new Date(row.ScheduleTime);

        if (new_schedTime < today) {
            swal("Selected date and time have passed already. Please select again.", "", "warning");
            return;
        }
    }

    if (mode == 2) {
        transchedState.current = JSON.stringify(row);

        if (transchedState.current == transchedState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }
    }
    
    var data = {
        func: "activity_schedule",
        data: row
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Staff/PostData",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (m) {
            if (cb) cb(m);

            var curDay = $("#curday").text();
            var curDayOfWeek = $(".week-picker").val();

            //if (curDay == "" && curDayOfWeek != "") {

            //    $("#weekly_view").trigger("click");
            //    $('#curday').text("");
            //    $(".day-picker").val("");
            //} else if (curDay != "" && curDayOfWeek == "") {

            //    $("#daily_view").trigger("click");
            //}

            $("#" + currentId).trigger("click");
            //conflict in js
            //var container = $("#appointments"),
            //    scrollTo = $("#" + currentId);

            //container.animate({
            //    scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
            //});

        }
    });
}
var bindCarestaff = function (dow, type) {

    if ($('#diagReschedule #txtTimeInR').val() == '') {
        $('#diagReschedule #txtCarestaffR')[0].options.length = 0;
        return;
    }
    if ($('#diagReschedule #txtActivityR').val() == '') {
        $('#diagReschedule #txtCarestaffR')[0].options.length = 0;
        return;
    }
    if (dow == null) dow = [];
    dayOfweek = dow;

    
    //var s_timee = "";
    //var s = $('#diagReschedule #txtTimeInR').val().split(" ");
    //var sched_time = s[0].split(":");
    //debugger
    //if (s.length == 1) {
    //    if (parseInt(sched_time[0]) > 12) {
    //        var time = parseInt(sched_time[0]) - 12;
    //        if (time < 10) {
    //            time = "0" + time;
    //        }

    //        s_timee = time + ":" + sched_time[1] + " PM";
    //    } else if (parseInt(sched_time[0]) == 12) {
    //        s_timee = "00:" + sched_time[1] + " AM";
    //    } else {
    //        s_timee = "0" + sched_time[0] + ":" + sched_time[1] + " AM";
    //    }
    //} else {
       
    //    if (parseInt(sched_time[0]) > 12) {
    //        var time = parseInt(sched_time[0]) - 12;
    //        if (time < 10) {
    //            time = "0" + time;
    //        }

    //        s_timee = time + ":" + sched_time[1] + " PM";
    //    } else if (parseInt(sched_time[0]) == 12) {
    //        s_timee = sched_time[1] + " AM";
    //    } else {
    //        s_timee =  sched_time[0] + ":" + sched_time[1] + " AM";
    //    }
    //}
   

    var data = {
        scheduletime: $('#diagReschedule #txtTimeInR').val(),
        activityid: $('#diagReschedule #txtActivityR').val(),
        carestaffid: null,
        recurrence: dow.join(',')
    };
    data.isonetime = "1";
    data.clientdt = moment($('#diagReschedule #OnetimedateR').val()).format("MM/DD/YYYY");
    data.type = type || 1;

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Admin/GetCarestaffByTime",
        data: JSON.stringify(data),
        async:false,
        dataType: "json",
        success: function (m) {

            $('#diagReschedule #txtCarestaffR').empty();
         
            $('<option/>').val('').html('').appendTo('#diagReschedule #txtCarestaffR');
            $.each(m, function (i, d) {
                if (d.Name != null) {
                    $('<option/>').val(d.Id).html(d.Name).appendTo('#diagReschedule #txtCarestaffR');
                }
            });
            
            
            
        }
        
    });
  
}

$(document).ready(function () {

    $('#diagTask #txtTimeIn').timepicker({
        // showMeridian: false,
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {
            //   debugger

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#diagTask #txtCarestaff').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }

            var isrecurring = $('#diagTask input[name="recurring"]:eq(0)').attr('checked');
            if (isrecurring)
                triggerDayofWeekClick();
            else
                $('#diagTask #Onetimedate').trigger('change');
        }
    });

    $('#save_newTask').on('click', function () {
        row = "";
        var mode = 1;
        doAjaxTask(mode, row, function (m) {

            if (m.result == 0) {
                setTimeout(function () {
                    toastr.success("Task " + (mode == 1 ? 'saved' : 'updated') + " successfully!");
                    transchedState.orig = JSON.stringify($('#diagTask').extractJson());
                    $(".cancel").click();

                    if (isDailyView == 1) {
                        filtercal = getFilter();
                        $("#weekly").DataTable().destroy();
                        generateDailySchedule(presentDate, filtercal, focus);
                        createDatatable();
                        getHeight();
                        calfilters();
                    } else {
                        filtercal = getFilter();
                        $("#weekly").DataTable().destroy();
                        generateWeeklySchedule($("#wstartDate").text(), $("#wendDate").text(), filtercal, focus);
                        createDatatable();
                        getHeight();
                        calfilters();
                    }

                })

               
            } else if (m.result == -2) {
                var resVisible = $('#txtResidentId').is(':visible');
                //there is already a schedule for a particular resident or room
                toastr.warning("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".");
            }
        });
    });

    $('#btnAddTask').on('click', function () {
        
        if ($.inArray(roleID, adminRoles) !== -1) {
            $("#diagTask").modal("toggle");
            var dept = $('#diagTask #ddlDept').val();
            $('#diagTask').clearFields();
            $('#diagTask input[xid="hdCarestaffId"]').val('');
            $('#diagTask input[name="recurring"]:eq(0)').attr('checked', 'checked');
            $('#diagTask input[name="recurring"]:eq(1)').removeAttr('checked');
            $('#diagTask #Onetimedate').attr('disabled', 'disabled');
            $('#diagTask .dayofweek').find('input[type="checkbox"]').removeAttr('disabled', 'disabled');

            //if housekeeper or maintenance hide room dropdown
            $('#diagTask #ddlDept').val(dept);
            if (dept == 5 || dept == 7) {
                //$('#diagTask #liRoom, #diagTask #liRoom select').show();
                //$('#diagTask #liResident, #diagTask #liResident select').hide();
                $("#room_section").show();
                $("#resident_section").hide();
            } else {
                //$('#diagTask #liResident, #diagTask #liResident select').show();
                //$('#diagTask #liRoom, #diagTask #liRoom select').hide();
                $("#room_section").hide();
                $("#resident_section").show();
            }
            
            transchedState.orig = JSON.stringify($('#diagTask').extractJson());
        } else {
            $("#diagTask").modal("hide");
            swal("You are not allowed to add a schedule task.", "", "warning")

        }

    });
    $('#diagTask #Onetimedate').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#diagTask #Onetimedate').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        bindCarestaff2(dow);
    });

    $('#diagTask #txtTimeIn').change(function () {
        if ($('#onetime').is(':checked') == true && $('#diagTask #Onetimedate').val() != "") {
            var dow = [];
            dow[0] = moment($('#diagTask #Onetimedate').val()).day();
            bindCarestaff2(dow);
        } else if ($('#reccur').is(':checked') == true) {
            triggerDayofWeekClick();
        }

    });
 
    $('#diagTask #txtActivity').change(function () {
        var dis = $(this);
        var title = dis.find('option:selected').attr('title') || '';
        dis.attr('title', title);
        $('#diagTask #viewActivity').val(title);
        $('#diagTask #txtCarestaff')[0].options.length = 0;
        toggleCarestafflist();
    });

    $('#diagTask input[name=recurring]').change(function () {
        $('#diagTask #txtCarestaff')[0].options.length = 0;
    });

    if ($('#diagTask #ddlDept').length > 0) {
        $('#diagTask #ddlDept').change(function () {
            var val = $(this).val();
            //$('#txtRoomId,#txtResidentId').val('');
            $('#diagTask #txtCarestaff')[0].options.length = 0;
            bindDept(val, toggleCarestafflist);
        });
    }

    $('#diagTask input[name="recurring"]').click(function () {
        var d = $(this);
        if (d.val() == 1) {
            $('#diagTask table.dayofweek input').removeAttr('disabled');
            $('#diagTask #Onetimedate').attr('disabled', 'disabled').val('');
        } else {
            $('#diagTask table.dayofweek input').attr('disabled', 'disabled');
            $('#diagTask table.dayofweek').clearFields();
            $('#diagTask #Onetimedate').removeAttr('disabled');
        }
    });

    $('#diagTask table.dayofweek input').click(triggerDayofWeekClick);

    $('#save_re').on('click', function () {
        
        var mode = 2;
        var type = parseInt($('#diagReschedule #txtCarestaffR').attr('xtype'));
        var row = $("#diagReschedule").extractJson();
        row.comptype = type;
        doAjax2(type, mode, row, function (m) {
            

            var typemsg = "rescheduled";
            if (type != 1) typemsg = "reassigned";

            if (m.result == 0) {
                setTimeout(function () {
                    toastr.success("Task " + typemsg + " successfully!", { timeOut: 1000 });
                    transchedState.orig = JSON.stringify($('#diagReschedule').extractJson());
                    $(".cancelt").click();
                    $(".cancel").click();

                    if (isDailyView == 1) {
                        filtercal = getFilter();
                        $("#weekly").DataTable().destroy();
                        generateDailySchedule(presentDate, filtercal, focus);
                        createDatatable();
                        getHeight();
                    } else {
                        filtercal = getFilter();
                        $("#weekly").DataTable().destroy();
                        generateWeeklySchedule($("#wstartDate").text(), $("#wendDate").text(), filtercal, focus);
                        createDatatable();
                        getHeight();
                    }
                }, 500);
                

            } else if (m.result == -2) {
                var resVisible = $('#txtResidentIdR').is(':visible');
                toastr.error("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", { timeOut: 1000 });
                //there is already a schedule for a particular resident or room
            } else {
                var resVisible = $('#txtResidentIdR').is(':visible');
                toastr.error("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", { timeOut: 1000 });
            }
        });
    });
    $('#diagReschedule #OnetimedateR').datepicker({
        changeMonth: false,
        changeYear: false,
        minDate: 0,
    });

    $('#diagReschedule #OnetimedateR').change(function () {
        if (this.value == '') return;
        var dow = [];
        dow[0] = moment(this.value).day();
        var type = $('#diagReschedule #txtCarestaffR').attr('xtype');
        bindCarestaff(dow, parseInt(type));
    });
    $('#diagReschedule #txtTimeInR').change(function () {
        
        var dow = [];
        dow[0] = moment($('#diagReschedule #OnetimedateR').val()).day();
        bindCarestaff(dow);


    });
    $('#diagReschedule #txtTimeInR').timepicker({
        'timeFormat': 'H:i',
        defaultTime: '',
        showPeriod: true,
        showLeadingZero: true,
        onClose: function (a, b) {

            if (a == b) return;

            if (a == null || a == '' || a == "__:__ __") {
                $('#diagReschedule #txtCarestaffR').empty();
                //$('<option/>').val('').html('Please select schedule time.').appendTo('#txtCarestaff');
                return;
            }
            $('#diagReschedule #OnetimedateR').trigger('change');
        }

    });

 //   $('#diagReschedule #txtTimeInR').mask('99:99 xy');
    $('#changedCstaff').on('change', function () {
      //  $('.chck').not(this).prop('checked', false);

        var len = $("#changedCstaff:checked").length;
        if (len > 0) {
            $("#txtCarestaffR").removeAttr("disabled");
        } else {
            $("#txtCarestaffR").attr("disabled", "disabled");
        }
    });
    
    $('#close_task').on('click', function () {
        
        transchedState.current = JSON.stringify($('#diagTask').extractJson());
      
        if (transchedState.current != transchedState.orig) {
            var id = $(this).attr("modalid");
            swal({
                title: "Are you sure you want to close without saving your data?",
                text: "You won't be able to revert this!",
                type: "error",
                showCancelButton: !0,
                confirmButtonText: "Yes"
            }).then(function (e) {
                e.value && closeModal(id)

            })
        } else {
            $(".cancel").trigger('click');
           
        }
    });
    $('#close_add_transpo').on('click', function () {
    
    transchedState.current = JSON.stringify($('#diag_add_transpo').extractJson());
        if (transchedState.current != transchedState.orig) {
            var id = $(this).attr("modalid");
            swal({
                title: "Are you sure you want to close without saving your data?",
                text: "You won't be able to revert this!",
                type: "error",
                showCancelButton: !0,
                confirmButtonText: "Yes"
            }).then(function (e) {
                e.value && closeModal(id)

            })
        } else {
            $(".cancel").trigger('click');
           
        }
    });
    $('#close_diagReschedule').on('click', function () {

            transchedState.current = JSON.stringify($('#diagReschedule').extractJson('id'));            
            if (transchedState.current != transchedState.orig) {
                
                var id = $(this).attr("modalid");
                swal({
                    title: "Are you sure you want to close without saving your data?",
                    text: "You won't be able to revert this!",
                    type: "error",
                    showCancelButton: !0,
                    confirmButtonText: "Yes"
                }).then(function (e) {
                    e.value && closeModal(id)

                })
            } else {
                
                $(".cancelt").trigger('click');
            }
    });
    
});
//var saveReassignRescheduleTask = function (mode) {



//}


//var showdiag2 = function (mode) {
//    debugger

//    $('.save').on('click', function () {
//        var type = parseInt($('#diagReschedule #txtCarestaffR').attr('xtype'));
//        row = "";
//        doAjax2(type, mode, row, function (m) {
//            var typemsg = "rescheduled";
//            if (type != 1) typemsg = "reassigned";

//            if (m.result == 0) {
//                toastr.success("Task " + typemsg + " successfully!", { timeOut: 5000 });

//                $(".cancel").click();

//            } else if (m.result == -2) {
//                var resVisible = $('#txtResidentIdR').is(':visible');
//                toastr.success("Schedule time for this task has a conflict for this " + (resVisible ? "resident" : "room") + " on " + m.message + ".", { timeOut: 6000 });
//                //there is already a schedule for a particular resident or room
//            }
//        });
//    });

//}