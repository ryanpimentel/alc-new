﻿var settings = { startDate: null, endDate: null, appointments: [] };
var global_html = [];

function updateSchedule() {

    var transpo_id = $(".r_transpo_id").val();
    var date = $(".r_currentdate").val();
    var end_date = $("#r_end_date").val();
    var start_date = $(".r_date").val();
    var comments = $("#r_comments").val();
    var warning;

    date = new Date(date);

    var data = {
        transpo_id: transpo_id,
        end_date: end_date,
        comments: comments,
        date: date
    };

    if (start_date > end_date) {

        data = {
            transpo_id: transpo_id,
            comments: comments,
            date: date
        };

        if (end_date != "") {
            warning = true;
        }
    }

    $.ajax({
        url: "Transsched/UpdateSchedule",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(data),
        type: "POST",
        success: function (d) {
            var msg = "";
            if (d.result == "-1") {
                $.growlError({ message: "Schedule update failed!", delay: 6000 });
            } else {

                if (warning) {
                    $.growlWarning({ message: "End date not saved!", delay: 6000 });
                } else {
                    $.growlSuccess({ message: "Schedule updated successfully!", delay: 6000 });
                }

            };

        }
    })
}

function ampm(e) {

    var s = e.split(":");

    if (s[0] < 11) {
        return "AM";
    } else {
        return "PM"
    }

}
function getStaff() {
    $.ajax({
        url: "Transsched/GetStaff",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        type: "POST",
        success: function (d) {

            var obj = JSON.parse(d);
            var html = "";


            for (var i = 0; i < obj.length; i++) {
                html += "<option value='" + obj[i].user_id + "'>" + obj[i].first_name + " " + obj[i].last_name + "</option>";
            }

            $("#r_staff").html(html);
        }
    })
}

function create_new_schedule() {

    getStaff();

    var resident_id = $(".r_resident_id").val();
    var name = $("#r_name").text();


    if ($("#diag_transpo").hasClass("ui-dialog-content") && $("#diag_transpo").dialog("isOpen")) {
        $("#diag_transpo").dialog("close");
    }

    $("#r_residents").hide();
    $("#c_resident").html(name);
    $("#c_resident").show();

    $("#diag_add_transpo").dialog("open");

    $("#cn_resident").val(resident_id);


}

function generateWeeklySchedule(startDay, endDay) {

    $("#weekly").show();
    $("#calendar").hide();

    //var cdate = new Date(cdate);
    //var current_day = cdate.getDay();
    var startWeek = new Date(startDay);
    //startWeek.setDate(cdate.getDate() - current_day);
    var tempDate = new Date(startWeek);
    var html = "<table>";
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var byTime;
    var html2 = "";

    var endWeek = new Date(endDay);
    //endWeek.setDate(cdate.getDate() + (6 - current_day));

    var str_date;

    for (var i = 0; i <= 6; i++) {

        tempDate = new Date(startWeek);
        tempDate.setDate(startWeek.getDate() + parseInt(i));
        tempDate.setHours(0, 0, 0, 0);
        getCellDesc(tempDate);

        byTime = global_html.slice(0);

        byTime.sort(function (a, b) {
            var d = a.start_time.split(":");
            var e = d[0] + "" + d[1];

            var f = b.start_time.split(":");
            var g = f[0] + "" + f[1];
            return e - g;
        });

        str_date = (tempDate.getMonth() + 1).toString() + "_" + tempDate.getDate().toString() + "_" + tempDate.getFullYear().toString();

        //html += "<span class=\"weekday\">" + days[i] + "</span>";

        html += "<tr><td>";
        html += "<span class=\"weekday\">" + tempDate.toDateString() + "</span>";
        html += "<hr></td></tr>";
        html += "<tr><td>";
        html += "<table id=\"" + str_date + "\" class=\"weekly_table\">";

        $.each(byTime, function (i, item) {
            html2 += "<tr><td>";
            html2 += "<div class=\"arrow-right arrow_div\"></div>";
            html2 += "<div class=\"after_arrow_div\"><a appointmentid=\"" + item.appointment_id + "\" id=\"" + item.resident_id + "\" href=\"#\" class=\"wcl\" ><span class=\"stime\">" + item.start_time + " " + ampm(item.start_time) + " - " + item.end_time + " " + ampm(item.end_time) + "</span> " + item.name + "</a><br/>";
            html2 += "</div></td></tr>";
        });


        html += html2;
        html += "</table><br/><br/>";
        html += "</td></tr>";
        html2 = "";

    }

    html += "</table>"
    $("#weekly_schedule").html(html);

}


var getCellDesc = function (d) {
    global_html = [];
    var html = "";
    var string = "";
    var string2 = "";
    var recurrence, cl;
    var currentdate = new Date(d);
    var end;

    var byTime = settings.appointments.slice(0);

    byTime.sort(function (a, b) {
        return a.start_time - b.start_time;
    });

    $.each(byTime, function (i, item) {

        recurrence = item.recurrence;
        cl = "cl_" + recurrence;
        string = "<a appointmentid=\"" + item.appointment_id + "\" id=\"" + item.resident_id + "\" href=\"#\" class=\"mcl " + cl + "\">" + item.start_time + " " + ampm(item.start_time) + " - " + item.end_time + " " + ampm(item.end_time) + " " + item.name + "</a><br>";

        if (item.end_date != null) {
            end = (d <= item.end_date);
        } else {
            end = true;
        };

        if (recurrence == "once") {
            //this should be "if equal" but "==" doesn't work. idk why

            if (!(d > item.start_date) && !(d < item.start_date)) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "weekly") {

            if (d >= item.start_date && d.getDay() == item.day_of_the_week && end) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "daily") {

            if (d >= item.start_date && end) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "biweekly") {

            var dw = item.day_of_the_week.split(",");



            if (((d >= item.start_date && d.getDay() == dw[0]) || (d >= item.start_date && d.getDay() == dw[1])) && end) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "monthly") {

            if (d >= item.start_date && d.getDate() == item.day_of_the_month && end) {

                html += string;
                global_html.push(item);
            }

        } else if (recurrence == "quarterly") {

            var qd = item.day_of_the_month.split(",");
            var date;

            $.each(qd, function (q, e) {

                date = new Date(e);

                if (d >= item.start_date && d.getMonth() == date.getMonth() && d.getDate() == date.getDate() && end) {

                    html += string;
                    global_html.push(item);
                }

            });


        } else if (recurrence == "annually") {

            date = new Date(item.start_date);

            if (d >= item.start_date && d.getMonth() == date.getMonth() && d.getDate() == date.getDate() && end) {

                html += string;
                global_html.push(item);
            }
        }


    });

    return html;
}

var Calendar = (function () {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var cellClasses = { 1: "typeCont", 2: "typeGen", 3: "typeInp", 4: "typeRout" };
    var icons = { "551": "snp.png", "421": "ptp.png", "431": "otp.png", "441": "stp.png", "561": "swp.png", "571": "hhap.png" };

    var selectedDate = "";

    var init = function (options) {

        if (options.startDate) settings.startDate = new Date(options.startDate);
        if (options.endDate) settings.endDate = new Date(options.endDate);
        if (options.appointments) {

            settings.appointments = [];
            $.each(options.appointments, function (i, item) {

                var end = null;

                if (item.end_date != null) {
                    end = new Date(item.end_date);
                }

                var loc = {
                    //admission_id: item.admission_id, resident_id: item.resident_id, name: item.name, admission_date: new Date(item.admission_date)
                    appointment_id: item.transpo_id,
                    resident_id: item.resident_id,
                    name: item.name,
                    start_date: new Date(item.date),
                    start_time: item.start_time,
                    end_time: item.end_time,
                    recurrence: item.recurrence,
                    day_of_the_week: item.day_of_the_week,
                    day_of_the_month: item.day_of_the_month,
                    end_date: end
                };
                settings.appointments.push(loc);
            });
        }

    }

    var limit = function (val, max, step) {
        return val < max ? val - step : max;
    };

    var renderHeaderHtml = function () {
        var html = '';//"<td>Week</td>";
        for (var i = 0; i < days.length; i++) {
            html += "<td>" + days[i] + "</td>";
        }

        return "<thead><tr>" + html + "</tr><thead>";
    };


    //show schedule


    var renderCellHtml = function (w, d) {
        var html = "<td><table";
        if (d != null) html += " id=\"" + (d.getMonth() + 1) + "_" + d.getDate() + "_" + d.getFullYear() + "\"";
        html += " class=\"planBlock";
        if (d != null) html += " day";
        html += "\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td class=\"date\">";
        //if (w != null) html += w;
        if (d != null) html += months[d.getMonth()] + " " + d.getDate();
        html += "</td></tr><tr><td class=\"planned\"><div>";
        if (d != null) html += getCellDesc(d);
        if (w != null) html += "";
        html += "</div></table></td>";//</td></tr><tr><td class=\"actual\"><div>";
        //html += "</div></td></tr><tr><td class=\"actual\"><div>";
        //if (d != null) html += getCellIcons(d, true);
        //if (w != null) html += "Actual";
        //html += "</div></td></tr></table></td>";

        return html;
    }

    var renderBodyHtml = function () {
        var html = "<tr>";
        for (var i = 0; i < settings.startDate.getDay() ; i++) {
            //if (i == 0) html += renderCellHtml(1);
            html += renderCellHtml();
        }

        var day = settings.startDate;
        var week = 1;
        for (; day <= settings.endDate;) {
            //if (day.getDay() == 0) html += renderCellHtml(week);
            html += renderCellHtml(null, day);
            if (day.getDay() == 6) {
                html += "</tr><tr>";
                week += 1;
            }
            day.setDate(day.getDate() + 1);
        }

        for (var i = settings.endDate.getDay() ; i < 6; i++) {
            html += renderCellHtml();
        }
        html += "</tr>"

        return "<tbody>" + html + "</tbody>";
    }

    var renderTableHtml = function () {
        var html = "<table class=\"clearFloats03\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
        html += renderHeaderHtml();
        html += renderBodyHtml();
        html += "</table>";

        return html;
    }
    var selectedTabIndex = 0;

    var showdiag = function (en) {

        $("#diag_transpo").dialog({
            zIndex: 0,
            autoOpen: false,
            modal: true,
            title: "Transportation Schedule",
            dialogClass: "companiesDialog",
            open: function () {
            },
            close: function () {

                $("#diag_view").html("");
            }
        });

        $("#diag_transpo").dialog("option", {
            width: $(window).width() * 0.3,
            height: $(window).height() - 350,
            position: "center",
            buttons: {
                "Save": {
                    click: function () {
                        updateSchedule();
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $("#diag_transpo").dialog("close");
                }
            },
            resize: function (event, ui) {
                //var diag = $('.ui-dialog-content');
                //diag = diag.find('#residentForm').parents('.ui-dialog-content');
                //var height = 0;
                //height = diag.eq(0).height() || 0;
                //if (height <= 0)
                //    height = diag.eq(1).height() || 0;
                //$("#AdmissionContainer,#residentForm").height(height);
                //$('#AdmissionContent').height(height - 20);
                //$('#tabs-1,#tabs-2,#tabs-3,#tabs-4,#tabs-5').height(height - 30);
            }
        }).dialog("open");
    }

    return {
        Build: function (options) {
            init(options);
            if (settings.startDate == null && settings.endDate == null) return;
            $("#calendar").html(renderTableHtml());
            $("#calendar").hide();
            $("#calendar").on("click", ".planned, .actual", function (e) {
                var $a = $(this);
                var $d = $a.closest(".day");
                if ($d[0]) {
                    selectedDate = $d.attr("id");
                    if ($a.is(".planned")) {
                        $("#calendarMenu1").css({ top: e.pageY, left: e.pageX }).slideDown(300);
                        $("#calendarMenu2").hide();
                    }
                    else if ($a.is(".actual")) {
                        $("#calendarMenu2").css({ top: e.pageY, left: e.pageX }).slideDown(300);
                        $("#calendarMenu1").hide();
                    }
                }
            });
            $(document).on("click", ".mcl, .wcl", function (e) {

                var classes = $(this).attr("class");
                var c = classes.split(" ");
                var resident_id = $(this).attr('id');
                var transpo_id = $(this).attr('appointmentid');
                var string = "";

                $("#r_comments").val("");

                $(".dwv").hide();
                $(".arrow-right").hide();

                var table_id = $(this).closest('table').attr('id');
                var date = table_id.replace(/_/g, "/");

                string = "<input class=\"r_currentdate\" type=\"hidden\"/><input class=\"r_resident_id\" type=\"hidden\" /><input class=\"r_resident_name\" type=\"hidden\" /><input class=\"r_transpo_id\" type=\"hidden\" />";
                string += "<input class=\"r_recurrence\" type=\"hidden\" /><input class=\"r_day_of_the_week\" type=\"hidden\" />";
                string += "<input class=\"r_day_of_the_month\" type=\"hidden\" /><input class=\"r_date\" type=\"hidden\" />";
                string += "<input class=\"r_start_time\" type=\"hidden\" /><input class=\"r_end_time\" type=\"hidden\" />";
                string += "<input class=\"r_purpose\" type=\"hidden\" /><input class=\"r_staff_id\" type=\"hidden\" />";

                string += "<p style=\"line-height: 1.5em\">";

                string += "Name: <span id=\"r_name\"></span><br />";
                string += "Recurrence: <span id=\"r_recurrence\"></span><br />";

                string += "<span class=\"dwv v_weekly v_biweekly\">Day of the Week: <span id=\"r_day_of_the_week\"></span><br /></span>";

                string += "<span class=\"dwv v_monthly\">Day of the Month:</span>";
                string += "<span class=\"dwv v_quarterly\">Date:</span>";
                string += "<span class=\"dwv v_quarterly v_monthly\" id=\"r_day_of_the_month\"></span><br /><br /><br />";

                string += "Start Date: <span id=\"r_date\"></span><br />";


                string += "<span id=\"rnd\">End Date: </span><input type=\"text\" class=\"date\" id=\"r_end_date\" tabindex=\"-1\" /><br />";
                string += "<i id=\"enddate_notif\"></i><br /><br />";

                string += "Start Time: <span id=\"r_start_time\"></span><br />";
                string += "End Time: <span id=\"r_end_time\"></span><br /><br />";
                string += "Purpose: <span id=\"r_purpose\"></span><br />";
                string += "Assigned Staff: <span id=\"r_staff_id\"></span><br />";
                string += "</p><br /><br />";

                string += "<span>Comments / Remarks:</span><br /><br />";
                string += "<textarea id=\"r_comments\" rows=\"10\" cols=\"50\"></textarea><br /><br /><br />";
                string += "<button id=\"create_new_schedule\" value=\"Create New Schedule\" onClick=\"create_new_schedule()\">Create New Schedule</button>";

                if (c[0] == "mcl") {
                    $("#weekly_task").html("");
                    $("#diag_view").html(string);

                } else {

                    string += "<br /><br /><button type=\"submit\" onClick=\"updateSchedule()\">Save</button>";
                    $("#diag_view").html("");
                    $("#weekly_task").html(string);
                    date = new Date(date);

                    $(this).parent().prev('div').show();
                }


                $(".r_currentdate").val(date);

                $(".date").datepicker({
                    'formatDate': 'yyyy-mm-dd',
                    'autoclose': true
                });

                $(".time").timepicker();


                var data = { transpo_id: transpo_id, date: date };
                var dw;

                $.ajax({
                    url: "Transsched/GetScheduleData",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (response) {

                        var obj = response["data"][0];

                        var comments = response["comments"][0];
                        if (comments != undefined) {

                            var d1 = new Date(date);
                            var d2 = new Date(comments.current_date);
                            //i dunno how to equal T_T
                            if (!(d1 > d2) && !(d1 < d2)) {

                                $("#r_comments").val(comments.comment);

                            }

                        }

                        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

                        for (var key in obj) {

                            var value = obj[key];

                            if (obj.hasOwnProperty(key)) {
                                if (key == "recurrence" || key == "day_of_the_week") {
                                    if (value == "biweekly") {
                                        dw = obj["day_of_the_week"].split(",");

                                        $("#r_day_of_the_week").html(days[dw[0]] + ", " + days[dw[1]]);

                                    } else {
                                        $("#r_day_of_the_week").html(days[value]);
                                    }

                                    if (value == "once") {

                                        $("#r_end_date").hide();
                                        //$("#rnd").hide();

                                    } else {

                                        $("#r_end_date").show();
                                        $("#rnd").show();

                                    }


                                    $(".v_" + value).show();

                                    $("#r_recurrence").html(obj["recurrence"]);

                                } else if (key == "end_date") {

                                    if (value != null) {

                                        $("#r_end_date").val(value);
                                        $("#r_end_date").removeClass("hasDatepicker");
                                        $("#r_end_date").removeClass("date");
                                        $("#r_end_date").attr("disabled", "disabled");
                                        $("#create_new_schedule").removeAttr("disabled");
                                        $("#enddate_notif").html("");


                                    } else {

                                        $("#r_end_date").val("");
                                        $("#r_end_date").addClass("hasDatepicker");
                                        $("#r_end_date").addClass("date");
                                        $("#r_end_date").removeAttr("disabled");
                                        $("#create_new_schedule").attr("disabled", "disabled");
                                        $("#enddate_notif").html("Please click 'Save' button to update end date.");
                                    }

                                } else if (key == "name") {

                                    $("#r_resident_name").val(value);
                                    $("#r_name").html(value);

                                } else {


                                    $("#r_" + key).html(value);

                                }

                                $(".r_" + key).val(value);

                            }
                        }
                    }
                });



                if (c[0] == "mcl") {
                    showdiag(0);
                }
                e.preventDefault();
                return false;
            });
            //  createPopupMenus();
        }
    }
})();

$(document).ready(function () {


    $(".date").datepicker({
        'formatDate': 'yyyy-mm-dd',
        'autoclose': true
    });

    $(".time").timepicker();

    var buildCalendar = function () {

        var month = $('#monthfilter').val();
        var year = $('#yrfilter').val();
        minDate = getMonthDateRange(year, month);
        buildCal(minDate);
    }


    var minDate = new Date();

    for (var i = 2000; i < 2050; i++) {
        $('#yrfilter').append($("<option/>", {
            value: i,
            text: i
        }));
    };

    $('#monthfilter').val(minDate.getMonth() + 1);
    $('#yrfilter').val(minDate.getFullYear());

    $("#calendarLayoutContainer").layout({
        center: {
            paneSelector: "#calendar",
            closable: false,
            slidable: false,
            resizable: true,
            spacing_open: 0
        },
        north: {
            paneSelector: "#raToolbar",
            closable: false,
            slidable: false,
            resizable: false,
            spacing_open: 0
        }
    });
    function buildCal(date) {
        var facilityId = null, roomId = null;
        var fr = $('#filterExp').val();

        var data = { startDate: date.start, endDate: date.end };
        $.ajax({
            url: "Transsched/GetAdmissionCalendar",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            success: function (response) {

                if (response) {
                    Calendar.Build({
                        startDate: data.startDate,
                        endDate: data.endDate,
                        appointments: response.Appointments
                    });
                }
            }
        });

    }

    function getMonthDateRange(year, month) {
        // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
        // array is 'year', 'month', 'day', etc
        var startDate = moment([year, month - 1]);

        // Clone the value before .endOf()
        var endDate = moment(startDate).endOf('month');

        // just for demonstration:
        //console.log(startDate.toDate());
        //console.log(endDate.toDate());

        // make sure to call toDate() for plain JavaScript date type
        return { start: startDate, end: endDate };
    }

    function getResidents() {
        $.ajax({
            url: "Transsched/GetResidents",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            success: function (d) {

                var obj = JSON.parse(d);
                var html = "";


                for (var i = 0; i < obj.length; i++) {
                    html += "<option value='" + obj[i].resident_id + "'>" + obj[i].first_name + " " + obj[i].last_name + "</option>";
                }

                $("#r_residents").html(html);
            }
        })
    }

    function addSchedule(recur) {

        var resident_id = $("#r_residents").val();
        var recurrence = recur;
        var start_date = $("#start_date").val();
        var start_time;
        var end_time;
        var day_of_the_week = $("#day_of_the_week").val();
        var day_of_the_month = "";
        var staff_id = $("#r_staff").val();
        var purpose = $("#purpose").val();


        if (resident_id == null) {
            resident_id = $("#cn_resident").val();
        }

        if (recurrence == "quarterly") {
            day_of_the_month = $("#start_date1").val() + "," + $("#start_date2").val() + "," + $("#start_date3").val() + "," + $("#start_date4").val();
        }

        if (recurrence == "biweekly") {
            day_of_the_week = $("#day_of_the_week1").val() + "," + $("#day_of_the_week2").val();
        }

        if (recurrence == "monthly") {
            day_of_the_month = $("#monthly_date").val();
        }

        start_time = $("#start_time").val();
        end_time = $("#end_time").val();

        var data = {
            start_date: start_date,
            recurrence: recurrence,
            day_of_the_week: day_of_the_week,
            day_of_the_month: day_of_the_month,
            start_time: start_time,
            end_time: end_time,
            resident_id: resident_id,
            staff_id: staff_id,
            purpose: purpose
        };

        if (start_time < end_time) {

            $.ajax({
                url: "Transsched/AddSchedule",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                type: "POST",
                success: function (d) {
                    $("#diag_add_transpo").dialog("close");
                    buildCalendar();
                    $("#weekly").show();
                }
            })

        } else {

            alert("Invalid timespan.");

        }
    }

    buildCalendar();

    $('#btnReload').click(function () {
        buildCalendar();
        $("#weekly_view").trigger("click");
        $(".week-picker").val("");
    })

    $('#filterExp,#monthfilter,#yrfilter').change(function () {
        buildCalendar();
        $("#weekly").hide();
        setTimeout(function () {
            $("#calendar").show();
        }, 300
       );
    });

    $("#diag_add_transpo").dialog({
        zIndex: 0,
        autoOpen: false,
        modal: true,
        title: "Add Schedule",
        dialogClass: "companiesDialog",
        width: $(window).width() * 0.3,
        height: $(window).height() - 350,
        position: "center",
        buttons: {
            "Save": {
                click: function () {

                    addSchedule($("#recurrence").val());

                },
                class: "primaryBtn",
                text: "Save"
            },
            "Cancel": function () {
                $("#diag_add_transpo").dialog("close");
            }
        },
        resize: function (event, ui) {
            //var diag = $('.ui-dialog-content');
            //diag = diag.find('#residentForm').parents('.ui-dialog-content');
            //var height = 0;
            //height = diag.eq(0).height() || 0;
            //if (height <= 0)
            //    height = diag.eq(1).height() || 0;
            //$("#AdmissionContainer,#residentForm").height(height);
            //$('#AdmissionContent').height(height - 20);
            //$('#tabs-1,#tabs-2,#tabs-3,#tabs-4,#tabs-5').height(height - 30);
        },
        open: function () { }
    });

    $("#add_schedule").on("click", function () {
        $("#r_residents").val("");
        $("#cn_resident").val("");
        $("#c_resident").html("");
        $("#c_resident").hide();
        $("#r_residents").show();

        getResidents();
        getStaff();

        var view_id = "r_" + $("#recurrence").val();
        $(".r_view").hide();
        $("#" + view_id).show();

        $("#diag_add_transpo").dialog("open");

    });
    $("#recurrence").on("change", function () {
        var id = "r_" + $(this).val();
        $(".r_view").hide();
        $("#" + id).show();

    })




    $("#weekly_view").on("click", function () {

        $("#diag_view").html("");
        $("#weekly_task").html("");
        var cdate = new Date();
        var current_day = cdate.getDay();
        var startWeek = new Date(cdate);
        startWeek.setDate(cdate.getDate() - current_day);

        var endWeek = new Date(cdate);
        endWeek.setDate(cdate.getDate() + (6 - current_day));

        generateWeeklySchedule(startWeek, endWeek);

        $("#wstartDate").text((startWeek.getMonth() + 1) + "/" + startWeek.getDate() + "/" + startWeek.getFullYear());
        $("#wendDate").text((endWeek.getMonth() + 1) + "/" + endWeek.getDate() + "/" + endWeek.getFullYear());

    });


    $("#monthly_view").on("click", function () {

        $("#weekly").hide();
        $("#calendar").show();


    });

    setTimeout(function () {

        var cdate = new Date();
        var current_day = cdate.getDay();
        var startWeek = new Date(cdate);
        startWeek.setDate(cdate.getDate() - current_day);

        $("#weekly_view").trigger("click");
    }, 300
   );


    //$.ajax({
    //    url: "Admin/GetNodeData",
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "html",
    //    type: "POST",
    //    data: '{id:"Admission_0"}',
    //    success: function (d) {
    //        $("#diag_transpo").empty();
    //        $("#diag_transpo").html(d);
    //        $("input:checkbox").on('click', function () {
    //            // in the handler, 'this' refers to the box clicked on
    //            var $box = $(this);
    //            if ($box.is(":checked")) {
    //                // the name of the box is retrieved using the .attr() method
    //                // as it is assumed and expected to be immutable
    //                var group = "input:checkbox[name='" + $box.attr("name") + "']";
    //                // the checked state of the group/box on the other hand will change
    //                // and the current value is retrieved using .prop() method
    //                $(group).prop("checked", false);
    //                $box.prop("checked", true);
    //            } else {
    //                $box.prop("checked", false);
    //            }
    //        });
    //        $('#txtHospitalPhone,#txtPhysicianPhone,#txtPhysicianFax,#txtPharmacyPhone').mask("(999)999-9999");
    //        $(".content-datepicker").datepicker({
    //            changeMonth: true,
    //            changeYear: true,
    //            yearRange: "-100:+0"
    //        });
    //        $("#txtDischargeDate").datepicker({
    //            changeMonth: true,
    //            changeYear: true,
    //            yearRange: "-100:+0"
    //        });//("option", "dateFormat", "mm/dd/yy");
    //        var SearchText = "Search resident name...";
    //        $("#txtResidentName").val(SearchText).blur(function (e) {
    //            if ($(this).val().length == 0)
    //                $(this).val(SearchText);
    //        }).focus(function (e) {
    //            if ($(this).val() == SearchText)
    //                $(this).val("");
    //        }).autocomplete({
    //            source: function (request, response) {
    //                $.ajax({
    //                    url: "Admin/Search",
    //                    contentType: "application/json; charset=utf-8",
    //                    dataType: "json",
    //                    type: "POST",
    //                    data: '{func: "search_resident", name:"' + request.term + '"}',
    //                    success: function (data) {
    //                        response($.map(data, function (item) {
    //                            return {
    //                                label: item.name,
    //                                value: item.id
    //                            }
    //                        }));
    //                    }
    //                });
    //            },
    //            delay: 200,
    //            minLength: 0,
    //            focus: function (event, ui) {
    //                $("#txtResidentName").val(ui.item.label);
    //                $('#txtResidentId').val(ui.item.value);
    //                return false;
    //            },
    //            change: function (event, ui) {
    //                var dis = $(this);
    //                if (dis.val() == '' || dis.val() == "Search resident name...") {
    //                    $("#addResident").hide();
    //                    return false;
    //                }
    //                $("#addResident").toggle(!ui.item);
    //            },
    //            select: function (event, ui) {
    //                return false;
    //            }
    //        }).on('focus', function () {
    //            $(this).autocomplete('search', $(this).val());
    //        }).data("ui-autocomplete")._renderItem = function (ul, item) {
    //            return $("<li></li>")
    //                    .data("item.autocomplete", item)
    //                    .append("<a>" + item.label + "</a>")
    //                    .appendTo(ul);
    //        };
    //        $('#addResident').click(function () {
    //            var row = { name: $('#txtResidentName').val() };
    //            var data = { func: "smart_add_resident", mode: 1, data: row };
    //            $.ajax({
    //                url: "Admin/PostGridData",
    //                contentType: "application/json; charset=utf-8",
    //                dataType: "json",
    //                type: "POST",
    //                data: JSON.stringify(data),
    //                success: function (data) {
    //                    if (data.result == 0) {
    //                        $('#txtResidentId').val(data.message);
    //                        $.growlSuccess({ message: "Resident saved successfully!", delay: 6000 });
    //                        $(this).hide();
    //                    }
    //                    //else if (m.result == -1) {
    //                    //    $.growlError({ message: "Resident saved successfully!", delay: 6000 });
    //                    //}
    //                }
    //            });
    //        })
    //        $.each(_FR.Rooms, function (index, value) {
    //            $('#room_facility').append($('<option>', { value: 'room_' + value.Id.toString(), text: value.Name }));
    //        });


    //    }
    //});

});
$(document).click(function (e) {
    if (!$(e.target).closest(".day")[0] && !$(e.target).is(".popupMenu")[0])
        $(".popupMenu").slideUp(300);
});