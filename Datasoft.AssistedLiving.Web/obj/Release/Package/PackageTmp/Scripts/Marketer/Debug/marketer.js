﻿var Dashboard = { BuildPendingAdmission: function () { } }

$(document).ready(function () {
    function capsFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    var buildPendingAdmission = function () {
        var data = { func: "dashboard_preadmission_nav", id: "0" };
        $.ajax({
            url: ALC_URI.Admin + "/GetFormData",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (res) {
                var x = [];
                $.each(res, function () {
                    var name = this.Fname + " ";
                    if (this.MI != '')
                        name += this.MI + " ";
                    name += this.Lname;
                    x.push("<tr class=''><td style='text-align:center;width:20px'><input name='chkresidents' value='" + this.ID + "' type='checkbox'/></td><td>");
                    x.push("<a id='" + this.ID + "' tab='" + this.Step + "'>" + name + "</a>");
                    x.push("</td><td>");
                    x.push(this.PreAdmissionDate);
                    x.push("</td><td>");
                    x.push(this.AdmissionStatus);
                    x.push("</td></tr>");
                });

                $('#dataTable tbody').html(x.join(''));

                // Usage: $form.find('input[type="checkbox"]').shiftSelectable();
                // replace input[type="checkbox"] with the selector to match your list of checkboxes

                $.fn.shiftSelectable = function () {
                    var lastChecked,
                        $boxes = this;
                    $boxes.click(function (evt) {
                        if (!lastChecked) {
                            lastChecked = this;
                            return;
                        }

                        if (evt.shiftKey) {
                            var start = $boxes.index(this),
                                end = $boxes.index(lastChecked);
                            $boxes.slice(Math.min(start, end), Math.max(start, end) + 1)
                                .attr('checked', lastChecked.checked)
                                .trigger('change');
                        }

                        lastChecked = this;
                    });
                };

                $('#dataTable').find('input[name="chkresidents"]').shiftSelectable();
            }
        });
    }
    Dashboard.BuildPendingAdmission = buildPendingAdmission;
    buildPendingAdmission();

    $('#dataTable tbody').on('click', 'a', function () {
        var dis = $(this);
        var rid = parseInt(dis.attr('id'));
        var tidx = parseInt(dis.attr('tab'));
        if (isNaN(tidx))
            tidx = 1;
        var cltidx = tidx - 1;
        $('#preAdmissionLink').trigger('click');
        var interval = setInterval(function () {
            var tab = $("#PlacementForm");
            if (tab[0]) {
                clearInterval(interval);
                tab.tabs('option', 'disabled', []);
                tab.tabs('option', 'selected', cltidx);

                var data = { func: "preplacement_wizard", id: rid.toString() };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: ALC_URI.Admin + "/GetFormData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        res.residentIdField = rid;
                        $(".form").mapJson(res, 'id');
                    }
                });
            }
        });
    });

    $('#admitResidents').click(function () {
        var values = [];
        $.each($('#dataTable').find('input[name="chkresidents"]'), function () {
            if ($(this).is(':checked'))
                values.push($(this).val());
        });
        var ids = values.join(',');
        if (values.length <= 0) {
            alert("Please select resident(s) to admit first and try again.");
            return;
        }
        if (!confirm("Are you sure you want to admit selected resident(s)?")) return;

        var data = { Func: 'dashboard_pending_admission', Data: ids };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Home + "/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                if (m.Result == 0) {
                    $.growlSuccess({ message: m.Message + " new resident(s) has been admitted successfully!", delay: 6000 });
                    buildPendingAdmission();
                }
            }
        });

    });

    var resize = function () {
        var wH = $(window).height() - 137;
        $("#Home .box-left").height(wH - 50);
        $("#Home table.box").width($(window).width() - 20);
        //$('#divdataTable').height($('#dashTabsContent').height() - 54);

        var bh = $('#Home .box-left').height();

        $('.content-body-blue,.content-body-red,.content-body-purple,.content-body-lime').height((bh / 2) - 72);
        //$('.content-body-lime').height(bh - 70);
    };
    resize();
    resize();
    $(window).resize(resize);

    Dashboard.LoadWidgets = function () {
        $([1, 2, 3, 4, 5, 6, 7, 8, 9]).each(function () {
            var type = this;
            var params = { func: "referrals", type: type };
            $('.content-body-lime tbody:eq(0)').html(''); //FOR APPEND TO WORK FOR STATUS 8 AND NINE (ABC)
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Referral + "/GetFormData",
                data: JSON.stringify(params),
                dataType: "json",
                success: function (m) {
                    var elems = [];
                    var cls = 'class="blue"';
                    if (type == 3)
                        cls = 'class="blue-followup"';
                    else if (type == 4)
                        cls = 'class="red"';
                    else if (type == 5)
                        cls = 'class="red-followup"';
                    else if (type == 6)
                        cls = 'class="purple"';
                    else if (type == 7)
                        cls = 'class="purple-followup"';
                    else if (type == 8 || type == 9) //COMBINED STATUS 8 AND NINE (ABC)
                        cls = 'class="lime';
                    else if (type == 1) //CHANGED FROM 9 TO STATUS 1 (ABC)
                        cls = 'class="lime-followup"';

                    $(m).each(function () {
                        var t_cls = cls;
                        if (type == 8 || type == 9) {
                            var iscomplete = this.movein_status != null && this.movein_status == 1 ? true : false;
                            if (iscomplete)
                                t_cls += ' complete"';
                            else
                                t_cls += ' incomplete"';
                        }
                        elems.push('<tr><td><a resid="' + this.resid + '" id="' + this.refid + '" ' + t_cls + '>' + this.name + '</a></td></tr>');
                    });



                    if (type == 2) {
                        //new leads
                        $('.content-body-blue tbody:eq(0)').html($(elems.join('')));
                    } else if (type == 3) {
                        //new leads followup
                        $('.content-body-blue tbody:eq(1)').html($(elems.join('')));
                    } else if (type == 4) {
                        //tour schedule
                        $('.content-body-red tbody:eq(0)').html($(elems.join('')));
                    } else if (type == 5) {
                        //tour schedule followup
                        $('.content-body-red tbody:eq(1)').html($(elems.join('')));
                    } else if (type == 6) {
                        //assessment completion
                        $('.content-body-purple tbody:eq(0)').html($(elems.join('')));
                    } else if (type == 7) {
                        //assessment completion followup
                        $('.content-body-purple tbody:eq(1)').html($(elems.join('')));
                    }
                    else if (type == 8 || type == 9) {
                        //movein schedule ----> modified(by ABC): MOVEIN FOR SCHEDULE AND FOLLOWUP
                        $('.content-body-lime tbody:eq(0)').append($(elems.join('')));
                    } else if (type == 1) {
                        //movein schedule followup ----> modified(by ABC): RECENT ADMISSIONS
                        $('.content-body-lime tbody:eq(1)').html($(elems.join('')));
                    }
                }
            });
        });
    };
    Dashboard.LoadWidgets();

    var tab = 1;
    var pagecount = 1;
    var assessment = function (dparam, win_type, cs) {
        var residentId = dparam.resid;
        var referralId = dparam.refid;



        var $canceldialog;
        var $dialog = $("<div><div id=\"assessment_content\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Assessment Wizard",
            dialogClass: "assessmentDialog",
            width: $(window).width() - 250,
            height: $(window).height() - 100,
            close: function () {
                $dialog.dialog("destroy").remove();
            },
            beforeClose: function (e, ui) {
                $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
                    modal: true,
                    title: "Confirmation",
                    width: 400,
                    buttons: {
                        "Yes": function () {
                            $canceldialog.dialog("destroy").remove();
                            $dialog.dialog("destroy").remove();
                            //window.location = ClientUrls["Home"];
                        },
                        "No": function () {
                            $canceldialog.dialog("destroy").remove();
                        }
                    }
                });
                return false;
            },
            buttons: {
                "Save": {
                    click: function () {
                        var row = { Func: 'assessment_wizard' };
                        row.Mode = ($("#AssessmentId").val() == "") ? 1 : 2;

                        var isValid = true;
                        isValid = $('.form').validate();
                        if (!isValid) {
                            return;
                        }
                        row.Data = $('.form').extractJson('id');


                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: ALC_URI.Marketer + "/PostGridData",
                            data: JSON.stringify(row),
                            dataType: "json",
                            success: function (m) {

                                $.growlSuccess({ message: "Assessment has been saved.", delay: 6000 });
                                $dialog.dialog("destroy").remove();
                                if (typeof (Dashboard.LoadWidgets) !== 'undefined') {
                                    Dashboard.LoadWidgets();
                                    Dashboard.HideDialog = function () {
                                        $dialog.dialog("destroy").remove();
                                    }
                                }
                            }
                        });

                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Print": {
                    click: function () {
                        var ts = $("#AssessmentForm").tabs('option', 'selected');
                        if (ts == 6)
                            window.open(ALC_URI.Marketer + "/PrintMorseFallScale?id=" + $("#AssessmentId").val(), '_blank');
                        else
                            window.open(ALC_URI.Marketer + "/PrintAssessment?id=" + $("#AssessmentId").val(), '_blank');
                    },
                    id: "AssessmentPrint",
                    text: "Print"
                },
                "Cancel": function () {
                    $dialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {


                var diag = $('.ui-dialog-content');
                diag = diag.find('#AssessmentForm').parents('.ui-dialog-content');
                var height = 0;
                height = diag.eq(0).height() || 0;
                if (height <= 0)
                    height = diag.eq(1).height() || 0;
                $("#assessmentContainer,#AssessmentForm").height(height);
                $('#assessmentContent,#assessmentTreeContainer').height(height - 20);
                $('#tabs-1b,#tabs-2b,#tabs-3b,#tabs-4b,#tabs-5b,#tabs-6b,#tabs-7b').height(height - 30);
            }
        });
        $.ajax({
            url: ALC_URI.Admin + "/GetNodeData",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            data: (win_type == 'wiz') ? '{id:"assessmentwizard_0"}' : '{id:"assessment_0"}',
            success: function (d) {
                $("#assessment_content").empty();
                $("#assessment_content").html(d);
                $('#update-status').click(function (e) {
                    ShowUPSDiag(dparam, cs);
                    return;
                });
                var params = { func: "assessment_wizard", id: residentId };

                $.ajax({
                    url: ALC_URI.Admin + "/GetNodeData",
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    type: "POST",
                    data: '{id:"morsefallscale_0"}',
                    success: function (d) {
                        $("#mfs-container").empty();
                        $("#mfs-container").html(d);
                    },
                    complete: function () { },
                    error: function () { }
                });

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: ALC_URI.Marketer + "/GetFormData",
                    data: JSON.stringify(params),
                    dataType: "json",
                    success: function (m) {
                        if (m.Result == 1) {
                            $("#FirstName").val(m.Data.FirstName);
                            $("#LastName").val(m.Data.LastName);
                            $("#MiddleInitial").val(m.Data.MiddleInitial);
                            var info = $.cookie('ALCInfo');
                            if (info != '') {
                                var ins = info.split('|');
                                var name = '';
                                if (ins.length > 1) {
                                    name = ins[1];
                                }
                                $("#CompletedBy").val(capsFirstLetter(name));
                            }
                        }
                        else {
                            //$(".form").serializeArray().map(function (x) { m[x.name] = x.value; },'id');
                            $(".form").mapJson(m.Data, 'id');
                        }

                        $("#AssResidentId").val(residentId);
                        $("#AssReferralId").val(referralId);
                        for (var ps in m.Points) {
                            $("#" + ps).val(m.Points[ps]);
                            $($("#" + ps).parents().children()[1]).html(m.Points[ps])
                        }
                        if (win_type == 'wiz') {

                            tab = 1;
                            pagecount = 1;
                            var $foot = $('<div style="padding-top:5px;float:left;"> <label><strong>Total Score: <span id="label-TotalScore">0</span></strong> </label></div> ' +
                                    '<div style="padding:0;"> <center> <input class="navi-button" id="ass-wiz-prev" type="button" value="<<" /> ' +
                                    ' <span><label id="pager" style="float:none;">Page  <input type="number" id="pager-no" min ="1" max="34"  /> of 34</label></span> <input class="navi-button" id="ass-wiz-next" type="button" value=">>" /> </center> </div>');

                            $('.ui-dialog-buttonpane').append($foot);
                            $("#label-TotalScore").text(m.Data.TotalScore);
                            loadPager();

                        }
                        if (!$("#AssessmentId").val()) {
                            $('#AssessmentPrint').prop("disabled", true).addClass("ui-state-disabled");
                        }
                    }
                });

            },
            complete: function () { },
            error: function () { }
        });
        return false;
    };
    $(document).on('click', '#ass-wiz-next', function (e) {
        var $current = $("#tabs-" + tab.toString() + 'b .container-display');
        var $next = $("#tabs-" + tab.toString() + 'b .container-display').next('.container');
        if ($next.length == 0) {
            if (tab == 7)
                return;
            tab++;
            $next = $("#tabs-" + tab.toString() + 'b .container').first('.container');
        }
        $("#AssessmentForm").tabs("option", "selected", tab - 1);
        $next.addClass('container-display');
        $current.removeClass('container-display');
        pagecount++;
        loadPager();
    });
    $(document).on('click', '#ass-wiz-prev', function (e) {
        var $current = $("#tabs-" + tab.toString() + 'b .container-display');
        var $prev = $("#tabs-" + tab.toString() + 'b .container-display').prev('.container');
        if ($prev.length == 0) {
            if (tab == 1)
                return;
            tab--;
            $prev = $("#tabs-" + tab.toString() + 'b .container').last('.container');
        }
        $("#AssessmentForm").tabs("option", "selected", tab - 1);
        $prev.addClass('container-display');
        $current.removeClass('container-display');
        pagecount--;
        loadPager();
    });

    var loadPager = function () {
        //var pagesize = $('.container').length; 56
        $("#pager-no").val(pagecount);
    }
    function assessmentWizardGoToPage(page) {
        var $current = $("#tabs-" + tab.toString() + 'b .container-display');
        var gotopage = page;
        if (gotopage == pagecount)
            return;
        if (gotopage > 34 || gotopage < 1) {
            $("#pager-no").val(pagecount);
            return;
        }
        else
            $("#pager-no").val(gotopage);
        var x = 0;
        var tab_col = {
            1: x += $('#tabs-1b .container').length,
            2: x += $('#tabs-2b .container').length,
            3: x += $('#tabs-3b .container').length,
            4: x += $('#tabs-4b .container').length,
            5: x += $('#tabs-5b .container').length,
            6: x += $('#tabs-6b .container').length,
            7: x += $('#tabs-7b .container').length,
        };
        for (var i = 1; i <= 7; i++) {
            if (gotopage <= tab_col[i]) {
                tab = i;
                pagecount = gotopage;
                var $go = $("#tabs-" + tab.toString() + 'b .container').slice(((tab_col[i] - ((i == 1) ? 0 : tab_col[i - 1])) - (tab_col[i] - pagecount)) - 1).slice(0, 1);

                $("#AssessmentForm").tabs("option", "selected", tab - 1);
                $go.addClass('container-display');
                $current.removeClass('container-display');
                return;
            }
        }
    }

    $(document).on('click', '#assessmentTreeView li.ass-sub-node a', function (e) {
        var i = $(this).attr('page');
        assessmentWizardGoToPage(i);

    });
    $(document).on('keypress', '#pager-no', function (e) {
        if (e.which == 13) {
            assessmentWizardGoToPage($("#pager-no").val());
        }
    });
    $('#Home').on('click', 'a', function () {
        var asessment_params = {
            resid: $(this).attr('resid'),
            refid: $(this).attr('id'),
            date: ($(this).text().split('(')[1]) ? $(this).text().split('(')[1].slice(0, -1) : null,
            roleID: $("#UsrRoleID").val()
        };

        var dis = $(this);
        var css = dis.attr('class');
        var rid = dis.attr('id');

        if ($(this).hasClass('purple')) {
            assessment(asessment_params, 'wiz', css.split(' ')[0]);

            return;
        }
        if ($(this).hasClass('lime-followup')) {
            admission(asessment_params);
            return;
        }

        var $canceldialog;
        var $dialog = $("<div><div id=\"referral_form\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Referral Form",
            width: $(window).width() - 300,
            height: $(window).height() - 100,
            close: function () {
                $dialog.dialog("destroy").remove();
            },
            beforeClose: function (e, ui) {
                $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
                    modal: true,
                    title: "Confirmation",
                    width: 400,
                    buttons: {
                        "Yes": function () {
                            $canceldialog.dialog("destroy").remove();
                            $dialog.dialog("destroy").remove();
                            //window.location = ClientUrls["Home"];
                        },
                        "No": function () {
                            $canceldialog.dialog("destroy").remove();
                        }
                    }
                });
                return false;
            },
            buttons: {
                "Save": {
                    click: function () {
                        $('#btnSave').trigger('click');
                        Dashboard.HideDialog = function () {
                            $dialog.dialog("destroy").remove();
                        }
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $dialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {

            }
        });

        //if (target.attr('id') == 'btnCreate')
        //    $('#Home').clearFields();
        $.get(ALC_URI.Referral + "?diag=1&attr=" + css, function (data) {

            var params = { func: "getform", id: rid };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ALC_URI.Referral + "/GetFormData",
                data: JSON.stringify(params),
                dataType: "json",
                success: function (m) {

                    $('#referral_form').html(data).mapJson(m, 'id');

                    $('#btnAddCallLog').click(function () {
                        var $dialog = $("<div><div id=\"call_log\" class=\"form-container\" style=\"padding:10px;\"><textarea rid=\"" + rid + "\" name=\"CallLog\" id=\"CallLog\" style=\"border:1px solid #a4a4a4 !important;outline:none;resize:none;width:300px;height:60px;\"/></div></div>").dialog({
                            modal: true,
                            closeOnEscape: false,
                            title: "Add Call Log",
                            width: 335,
                            height: 155,
                            resizable: false,
                            close: function () {
                                $dialog.dialog("destroy").remove();
                            },
                            beforeClose: function (e, ui) {
                                $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
                                    modal: true,
                                    title: "Confirmation",
                                    width: 400,
                                    buttons: {
                                        "Yes": function () {
                                            $canceldialog.dialog("destroy").remove();
                                            $dialog.dialog("destroy").remove();
                                            //window.location = ClientUrls["Home"];
                                        },
                                        "No": function () {
                                            $canceldialog.dialog("destroy").remove();
                                        }
                                    }
                                });
                                return false;
                            },
                            buttons: {
                                "Ok": {
                                    click: function () {
                                        var io = { func: "post_call_log" };
                                        var val = $('#CallLog').val();
                                        if (val == '') {
                                            alert('Call log is required a field. Please supply before saving.');
                                            return;
                                        }
                                        io.data = val;
                                        io.id = $('#CallLog').attr('rid');
                                        $.ajax({
                                            type: "POST",
                                            contentType: "application/json; charset=utf-8",
                                            url: ALC_URI.Referral + "/PostData",
                                            data: JSON.stringify(io),
                                            dataType: "json",
                                            success: function (m) {
                                                if (m.Result == 0) {
                                                    $('#Notes').val(m.Message);
                                                    $dialog.dialog("destroy").remove();
                                                }
                                            }
                                        });
                                        return false;

                                    },
                                    class: "primaryBtn",
                                    text: "Ok"
                                },
                                "Cancel": function () {
                                    $dialog.dialog("destroy").remove();
                                }
                            },
                            resize: function (event, ui) {

                            }
                        });
                    });
                    $('#btnShowAssessment').click(function () {
                        assessment(asessment_params, 'for', css.split(' ')[0])
                        return;
                    });
                    $('#btnShowAdmission').click(function () {
                        admission(asessment_params, true)
                        return;
                    });
                    $('#update-status').click(function (e) {
                        ShowUPSDiag(asessment_params, css.split(' ')[0]);
                        return;
                    });
                }
            });

        });

    });

    var selectedTabIndex = 0;

    var admission = function (d, hasmoveindate) {
        var residentId = d.resid;

        var referralId = d.refid;
        var _date = d.date;
        var id = 0;
        var mode = false;
        var roleID = d.roleID;
        selectedTabIndex = 0;

        var $canceldialog;
        var $dialog = $("<div><div id=\"admission_content\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Admission Wizard",
            dialogClass: "admissionDialog",
            width: $(window).width() - 150,
            height: $(window).height() - 100,
            close: function () {
                $dialog.dialog("destroy").remove();
            },
            beforeClose: function (e, ui) {
                $canceldialog = $("<div><div class=\"confirm\">Are you sure you want to close without saving your data?</div></div>").dialog({
                    modal: true,
                    title: "Confirmation",
                    width: 400,
                    buttons: {
                        "Yes": function () {
                            $canceldialog.dialog("destroy").remove();
                            $dialog.dialog("destroy").remove();
                            //window.location = ClientUrls["Home"];
                        },
                        "No": function () {
                            $canceldialog.dialog("destroy").remove();
                        }
                    }
                });
                return false;
            },
            //ADMIN CAN BE THE ONLY ONE TO ADD ADMISSION DATE (-ABC)
            show: {
                effect: "",
                complete: function () {
                    if (roleID != 1) {
                        $("#txtAdmissionDate").attr("disabled", "disabled");
                    }
                }
            },
            buttons: {
                "Save": {
                    click: function () {
                        var resName = $('#txtResidentName');
                        if (resName.val() == "Search resident name...")
                            resName.val('');

                        var isValid = $('.form').validate();
                        //do other modes
                        if (!isValid) {
                            $('#txtResidentName').val("Search resident name...");
                            return;
                        }
                        var row = $('.form').extractJson('id');
                        row.txtReferalId = referralId;
                        row.residentId = $('#admForm .admTbs #txtResidentId').val();

                        var tabindex = $("#admForm").tabs('option', 'selected');
                        var data = { func: "resident_admission", mode: mode, data: row, TabIndex: tabindex };
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Admin/PostGridData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (m.message == "Duplicate") {
                                    alert('Admission for resident already exists!');
                                    return;
                                }
                                if (mode == 1) {
                                    $('#txtAdmissionId').val(m.result);
                                }
                                var tabindex = $("#admForm").tabs('option', 'selected');
                                if (tabindex == 0) {
                                    $('#DCResidentName').val($('#txtResidentName').val());
                                    $('#ANSResidentName').val($('#txtResidentName').val());
                                    $('#AAAdmissionDate').val($('#txtAdmissionDate').val());
                                    $('#AAResidentName').val($('#txtResidentName').val());
                                    $('#RMMResidentName').val($('#txtResidentName').val());
                                    $('#DCRoomNumber').val($('#room_facility option:selected').text());
                                    $('#AARoomNo').val($('#room_facility option:selected').text());
                                    $('#RMMRoomNo').val($('#room_facility option:selected').text());
                                }
                                else if (tabindex == 2) {
                                    $('#AADOB').val($('#ANSDOB').val());
                                }
                                else if (tabindex == 3) {

                                    if (hasmoveindate) $('#RMMMoveInDate').val(_date);
                                }
                                if (selectedTabIndex > tabindex) {
                                    $("#admForm").tabs("option", "selected", selectedTabIndex);
                                }
                                else {
                                    selectedTabIndex = tabindex + 1;
                                    if (tabindex == 0) {

                                        $("#admForm").tabs('option', 'disabled', [2, 3, 4]);
                                        $("#admForm").tabs("option", "selected", 1);
                                    }
                                    else if (tabindex == 1) {

                                        $("#admForm").tabs('option', 'disabled', [3, 4]);
                                        $("#admForm").tabs("option", "selected", 2);
                                    }
                                    else if (tabindex == 2) {

                                        $("#admForm").tabs('option', 'disabled', [4]);
                                        $("#admForm").tabs("option", "selected", 3);
                                    }
                                    else if (tabindex == 3) {

                                        $("#admForm").tabs('option', 'disabled', []);
                                        $("#admForm").tabs("option", "selected", 4);
                                    }
                                    else if (tabindex == 4) {

                                        $.growlSuccess({ message: "Admission " + (mode == 1 ? 'saved' : 'updated') + " successfully!", delay: 6000 });

                                        $dialog.dialog("destroy").remove();
                                    }
                                }

                                if (typeof (Dashboard.LoadWidgets) !== 'undefined') {
                                    Dashboard.LoadWidgets();
                                }
                            }
                        });

                        //var row = { Func: 'assessment_wizard' };
                        //row.Mode = ($("#AssessmentId").val() == "") ? 1 : 2;

                        //var isValid = true;
                        //isValid = $('.form').validate();
                        //if (!isValid) {
                        //    return;
                        //}
                        //row.Data = $('.form').extractJson('id');


                        //$.ajax({
                        //    type: "POST",
                        //    contentType: "application/json; charset=utf-8",
                        //    url: ALC_URI.Admin + "/PostGridData",
                        //    data: JSON.stringify(row),
                        //    dataType: "json",
                        //    success: function (m) {

                        //        $.growlSuccess({ message: "Assessment has been saved.", delay: 6000 });
                        //        $dialog.dialog("destroy").remove();
                        //        if (typeof (Dashboard.LoadWidgets) !== 'undefined') {
                        //            Dashboard.LoadWidgets();
                        //            Dashboard.HideDialog();
                        //        }
                        //    }
                        //});

                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $dialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {
                var diag = $('.ui-dialog-content');
                diag = diag.find('#admForm').parents('.ui-dialog-content');
                var height = 0;
                height = diag.eq(0).height() || 0;
                if (height <= 0)
                    height = diag.eq(1).height() || 0;
                $("#AdmissionContainer,#admForm").height(height);
                $('#AdmissionContent').height(height - 20);
                $('#tabs-1a,#tabs-2a,#tabs-3a,#tabs-4a,#tabs-5a').height(height - 30);
            }
        });

        $.ajax({
            url: ALC_URI.Admin + "/GetNodeData",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            data: '{id:"Admission_0"}',
            success: function (d) {
                $("#admission_content").empty();
                $("#admission_content").html(d);

                $("#admForm").tabs();
                $("#admForm").tabs("option", "selected", 0);
                $("#admForm").tabs('option', 'disabled', [1, 2, 3, 4]);
                var diag = $('.ui-dialog-content');
                diag = diag.find('#admForm').parents('.ui-dialog-content');
                var height = 0;
                height = diag.eq(0).height() || 0;
                if (height <= 0)
                    height = diag.eq(1).height() || 0;
                $("#AdmissionContainer,#admForm").height(height);
                $('#AdmissionContent').height(height - 20);
                $('#tabs-1a,#tabs-2a,#tabs-3a,#tabs-4a,#tabs-5a').height(height - 30);

                $("input:checkbox").on('click', function () {
                    // in the handler, 'this' refers to the box clicked on
                    var $box = $(this);
                    if ($box.is(":checked")) {
                        // the name of the box is retrieved using the .attr() method
                        // as it is assumed and expected to be immutable
                        var group = "input:checkbox[name='" + $box.attr("name") + "']";
                        // the checked state of the group/box on the other hand will change
                        // and the current value is retrieved using .prop() method
                        $(group).prop("checked", false);
                        $box.prop("checked", true);
                    } else {
                        $box.prop("checked", false);
                    }
                });
                $('#txtHospitalPhone,#txtPhysicianPhone,#txtPhysicianFax,#txtPharmacyPhone').mask("(999)999-9999");
                $(".content-datepicker").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+0"
                });
                $("#txtDischargeDate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+0"
                });//("option", "dateFormat", "mm/dd/yy");
                var SearchText = "Search resident name...";
                $("#txtResidentName").val(SearchText).blur(function (e) {
                    if ($(this).val().length == 0)
                        $(this).val(SearchText);
                }).focus(function (e) {
                    if ($(this).val() == SearchText)
                        $(this).val("");
                }).autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: ALC_URI.Admin + "/Search",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            type: "POST",
                            data: '{func: "search_resident", name:"' + request.term + '"}',
                            success: function (data) {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.name,
                                        value: item.id
                                    }
                                }));
                            }
                        });
                    },
                    delay: 200,
                    minLength: 2,
                    focus: function (event, ui) {
                        $("#txtResidentName").val(ui.item.label);
                        $('#txtResidentId').val(ui.item.value);
                        return false;
                    },
                    select: function (event, ui) {
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + item.label + "</a>")
                            .appendTo(ul);
                };

                $.each(_FR.Rooms, function (index, value) {
                    $('#room_facility').append($('<option>', { value: 'room_' + value.Id.toString(), text: value.Name }));
                });

                var data = { func: "resident_admission", id: residentId };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: ALC_URI.Admin + "/GetFormData",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (m) {

                        $(".form").mapJson(m, 'id');
                        id = m.txtAdmissionId;
                        mode = $.isNumeric(id) ? 2 : 1;

                        if (mode == 2)
                            $("#admForm").tabs('option', 'disabled', []);
                        $('#txtResidentName').focus();

                        if ($('#ANSAppraisalID').val() == '') {
                            $('#ANSAppraisalDate').val(ALC_Util.CurrentDate).show();
                        } else {
                            //load selection appraisal dates
                            $.get(ALC_URI.Admin + "/GetAppraisalNSDates?id=" + id, function (data) {
                                $.each(data, function () {
                                    $('#SelectAppraisalDate').append($('<option>', { value: this.id, text: this.date }));
                                });
                                $('#SelectAppraisalDate').val($('#ANSAppraisalID').val()).show();
                            });

                        }

                    }
                });


            }
        });

        return false;
    };


    var ShowUPSDiag = function (en, cs) {
        var x = { "blue": 2, "blue-followup": 3, "red": 4, "red-followup": 5, "purple": 6, "purple-followup": 7, "lime": 8, "lime-followup": 9 };

        var $sdialog = $("<div><div id=\"diag\" ></div></div>").dialog({
            modal: true,
            closeOnEscape: false,
            title: "Revert Status",
            width: 400,
            height: 120,
            close: function () {
                $sdialog.dialog("destroy").remove();
            },
            buttons: {
                "Save": {
                    click: function () {
                        var data = { func: "updatestatus" };
                        var isValid = $('#txtStatus').val() != "0";

                        if (!isValid) return;
                        data.data = { residentId: en.resid, referralId: en.refid, status: $('#txtStatus').val() };
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: ALC_URI.Referral + "/PostData",
                            data: JSON.stringify(data),
                            dataType: "json",
                            success: function (m) {
                                if (m.Result == 0) {
                                    $.growlSuccess({ message: "Status updated successfully!", delay: 6000 });

                                    Dashboard.LoadWidgets();
                                    $sdialog.dialog("destroy").remove();
                                }
                            }
                        });
                        return false;
                    },
                    class: "primaryBtn",
                    text: "Save"
                },
                "Cancel": function () {
                    $sdialog.dialog("destroy").remove();
                }
            },
            resize: function (event, ui) {
            }
        });

        $("#diag").empty();
        $('#diag').html("<div style='overflow:hidden!important;padding:02px;margin:30px'><fieldset><ul><li><label style='float:none;'>Please select previous status</label><select name='Status'id='txtStatus'class='right'style='margin-top:-5px!important'></select></li></ul></fieldset></div>");
        $('#txtStatus').find('option').remove().end();
        //$('#txtStatus').append($("<option></option>").attr("value", 0).text("[Select Status]").selected().disabled);
        $('#txtStatus').append($("<option></option>").attr("value", 3).text("Referal - Follow Up"));
        $('#txtStatus').append($("<option></option>").attr("value", 4).text("Tour - Schedule"));
        $('#txtStatus').append($("<option></option>").attr("value", 6).text("Assessment - Schedule"));
        $('#txtStatus option').each(function () {
            currentItem = parseInt($(this).attr("value"), 10);
            if (currentItem >= x[cs]) {
                $(this).remove();
            }

        });

    }

});

