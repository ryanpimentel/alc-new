﻿$(window).resize(function () {
    var diff = $(window).height() - 155;
    $('.form-container').height(diff);
});
$(document).ready(function () {
    var diff = $(window).height() - 155;
    $('.form-container').height(diff);


    function getAge(birthday) {
        if (birthday == '') return '';
        var b = moment(birthday)._d;
        var ageDifMs = Date.now() - b.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    //$("#Age").forceNumericOnly(); //commented this out because it conflicts with vendor.bundle.js
    $(".refdate, #Rbirthday").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });

    //$('#TourTime').timepicker({
    //    showPeriod: true,
    //    showLeadingZero: false,
    //    showOn: 'focus'
    //});

    //$('#ContactPersonPhone,#ResidentPhone').mask("(999)999-9999"); //commented this out because it conflicts with vendor.bundle.js

    $("input:checkbox").on('click', function () {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });

    $('#Rbirthday').off('change').on('change', function () {
        //var id = this.id;
        //if (id == 'Rbirthday') {
        //    $('#ANSDOB,#txtBirthdate').val(this.value);
        //} else if (id == 'ANSDOB') {
        //    $('#AADOB,#txtBirthdate').val(this.value);
        //} else if (id == 'txtBirthdate') {
        //    $('#AADOB,#ANSDOB').val(this.value);
        //}

        var age = getAge(this.value);
        $('#Age').val(age);
        //$('#ANSAge,#ANSAge2,#residentAgeField,#applicantage,#IEAgeField,#RESAPPapplicantage').val(age);
    });

    $('#cbspecneed').click(function () {
        var d = $(this);
        if (d.prop('checked'))
            $('#SpecNeed').removeAttr('disabled');
        else {
            $('#SpecNeed').attr('disabled', 'disabled');
            $('#SpecNeed').val('');
        }
    });

    $('#btnSave').click(function () {
        $('.loader').show();
        var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        var email_address = $("#referral_modal #Email").val();

        if (email_address != "") {
            if (!email_regex.test(email_address)) {
                swal("Invalid email address.", "", "warning");
                $(".loader").hide();
                return false;
            }
        }
     
        var data = {
            func: "postform"
        };
        var isValid = $('#referral_form').validate();

        if (!isValid) return;
        data.data = $('#referral_form').extractJson('id');
        //================== Compare changes on referral form
        marketerState.current = JSON.stringify(data.data);

        if (marketerState.current == marketerState.orig) {
            swal("There are no changes to save.", "", "info");
            $(".loader").hide();
            return;
        }
        //==========================================

        //if (data.data.Lastname != null)
        data.data.Lastname = capitalizeFirstLetter(data.data.Lastname.trim());
        data.data.Firstname = capitalizeFirstLetter(data.data.Firstname.trim());

        var form = document.getElementById('referral_form');
        for (var i = 0; i < form.elements.length; i++) {
            if (form.elements[i].value === '' && form.elements[i].hasAttribute('required')) {
                swal("Fill in all required fields.")
                $('.loader').hide();
                return false;
            }
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ALC_URI.Referral + "/PostData",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (m) {
                $('.loader').hide();
                if (m.Result == 0) {
                    swal("Referral saved successfully!", "", "success")
                    Dashboard.LoadWidgets();

                    $("#referral_modal form").trigger("reset");
                    $("#referral_modal").modal("toggle");

                    //if (typeof (Dashboard.LoadWidgets) !== 'undefined') {
                    //    Dashboard.LoadWidgets();
                    //    Dashboard.HideDialog();
                    //}
                } else if (m.Result == -1) {
                    swal("Error! Identical record found!", "Error saving referral. Kindly verify with agency to avoid admission of duplicate resident.", "error");
                    return false;
                }
            }
        });
        return false;
    });

    $('#btnClear').click(function () {
        $('.form-container').clearFields();
    });

    //added by Mariel - 2017-07-19 //commented this out because it conflicts with vendor.bundle.js
    //var SearchReference = "Search Reference";
    //$("#RefSource").val(SearchReference).blur(function (e) {
    //    if ($(this).val().length == 0)
    //        $(this).val(SearchReference);
    //}).focus(function (e) {
    //    if ($(this).val() == SearchReference)
    //        $(this).val("");
    //}).autocomplete({
    //    source: function (request, response) {
    //        $.ajax({
    //            url: "Admin/Search",
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "json",
    //            type: "POST",
    //            data: '{func: "search_reference", name:"' + request.term + '"}',
    //            success: function (data) {
    //                response($.map(data, function (item) {
    //                    return {
    //                        label: item.reference,
    //                        value: item.id
    //                    }
    //                }));
    //            }
    //        });
    //    },
    //    delay: 200,
    //    minLength: 2,
    //    focus: function (event, ui) {
    //        $("#RefSource").val(ui.item.label);
    //        return false;
    //    },
    //    select: function (event, ui) {
    //        return false;
    //    }
    //}).data("ui-autocomplete")._renderItem = function (ul, item) {
    //    console.log(item);
    //    return $("<li></li>")
    //        .data("item.autocomplete", item)
    //        .append("<a>" + item.label + "</a>")
    //        .appendTo(ul);
    //};
});