﻿$(document).ready(function () {

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    var token = window.location.search.substring(1);

    if (token.length >= 1) {

        var converted_token = atob(token);
        var split1_token = converted_token.split('=');
        var agency = split1_token[0];

        $('#txtAgency').val(agency);
    }

    $('#txtPhone').mask("(999)999-9999");
    
    $('#btnSubmit').click(function () {
        var agency = $('#txtAgency').val();
        var lastname = $('#txtLastName').val();
        var firstname = $('#txtFirstName').val();
        var ssn = $('#txtSSN').val();
        var email = $('#txtEmail').val();
        var phone = $('#txtPhone').val();
        var password1 = $('#txtPassword1').val();
        var password2 = $('#txtPassword2').val();
        var captcha = $('#g-recaptcha-response').val();
        //
        //var secret = "6LcA8nMUAAAAABEYU44HoRa0Uaau1mzrEf_h0PSG"; // localhost captcha secret key
        //var secretlive = "6LcTnXMUAAAAAHXzdau5cvyL5Jgw7MsiCfKD3FuE"; //live captcha secret key
        //var a = $.post('https://www.google.com/recaptcha/api/siteverify?secret=' + secret + '&response=' + captcha);
        //var response = JSON.parse(a);
        //echo $homepage;
        

        if (agency == '' || email == '' || firstname == '' || lastname == '' || ssn == '' || phone == '' || password1 == '' || password2 == '') {
            $('#error').show().html("All fields are required.");
            return;
        }

        if (password1 != password2) {
            $('#error').show().html("Passwords do not match.");
            return;
        } else if (password1.length < 8){
            $('#error').show().html("Password must not be less than 8 characters.");
            return;
        }

        //validate email address.
        if (!validateEmail(email)) {
            $('#error').show().html("Invalid email address.");
            return;
        }

        if (captcha == "") {
            $('#error').show().html("Please check captcha form before submitting. ");
            return;        
        }

        $('#btnSubmit').attr('disabled', 'disabled').attr('class', 'primaryBtn-disabled');
        $('#loader').show();

        var data = { AgencyId: agency, LastName: lastname, FirstName: firstname, SSN: ssn, Email: email, Phone: phone, Password: password1 };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "SendGuestVerification",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (res) {
                $('#loader').hide();
                if (res.Result == -1) {
                    $('#btnSubmit').removeAttr('disabled').attr('class', 'primaryBtn');
                    $('#error').show().html(res.ErrorMsg);
                    return;
                }

                $('#enter').hide();
                $('#success').show();
            }
        });
    });

    $('#btnSubmitNP').click(function () {
        var agency = $('#agencyid').val();
        var userid = $('#userid').val();
        var pass1 = $('#txtPassword').val();
        var pass2 = $('#txtCPassword').val();
        

        if (pass1 == '' || pass2 == '') {
            $('#errorNP').show().html("New password and confirm new password are required fields.");
            return;
        }

        //validate passwords
        if (pass1 != pass2) {
            $('#errorNP').show().html("New password and confirm new password does not match.");
            return;
        }

        if (pass1.length < 8) {
            $('#errorNP').show().html("Password must be at least 8 characters");
            return;
        }

        $('#btnSubmitNP').attr('disabled', 'disabled').attr('class', 'primaryBtn-disabled');
        $('#loaderNP').show();

        var data = { Id: userid, Agency: agency, Password: pass1, ConfirmPassword: pass2 };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "../ChangePassword",
            data: JSON.stringify(data),
            dataType: "json",
            success: function (res) {
                if (res.Result == -1) {
                    $('#btnSubmitNP').removeAttr('disabled').attr('class', 'primaryBtn');
                    $('#errorNP').show().html(res.ErrorMsg);
                    return;
                }

                $('#enterNP').hide();
                $('#successNP').show();
            }
        });
    });

    $('#txtEmail,#txtAgencyID').keypress(function () {
        $('#error').hide();
    });

    $('#txtCPassword,#txtPassword').keypress(function () {
        $('#errorNP').hide();
    });

});