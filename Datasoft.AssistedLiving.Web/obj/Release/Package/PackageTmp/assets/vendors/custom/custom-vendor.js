﻿! function(t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : jQuery)
}(function (t, e) {
    function i() {
        return new Date(Date.UTC.apply(Date, arguments))
    }

    function n() {
        var t = new Date;
        return i(t.getFullYear(), t.getMonth(), t.getDate())
    }

    function r(t, e) {
        return t.getUTCFullYear() === e.getUTCFullYear() && t.getUTCMonth() === e.getUTCMonth() && t.getUTCDate() === e.getUTCDate()
    }

    function o(i, n) {
        return function() {
            return n !== e && t.fn.datepicker.deprecated(n), this[i].apply(this, arguments)
        }
    }

    function s(e) {
        var i = {};
        if (f[e] || (e = e.split("-")[0], f[e])) {
            var n = f[e];
            return t.each(p, function(t, e) {
                e in n && (i[e] = n[e])
            }), i
        }
    }
    var a = function() {
        var e = {
            get: function(t) {
                return this.slice(t)[0]
            },
            contains: function(t) {
                for (var e = t && t.valueOf(), i = 0, n = this.length; i < n; i++)
                    if (0 <= this[i].valueOf() - e && this[i].valueOf() - e < 864e5) return i;
                return -1
            },
            remove: function(t) {
                this.splice(t, 1)
            },
            replace: function(e) {
                e && (t.isArray(e) || (e = [e]), this.clear(), this.push.apply(this, e))
            },
            clear: function() {
                this.length = 0
            },
            copy: function() {
                var t = new a;
                return t.replace(this), t
            }
        };
        return function() {
            var i = [];
            return i.push.apply(i, arguments), t.extend(i, e), i
        }
    }(),
        l = function(e, i) {
            t.data(e, "datepicker", this), this._process_options(i), this.dates = new a, this.viewDate = this.o.defaultViewDate, this.focusDate = null, this.element = t(e), this.isInput = this.element.is("input"), this.inputField = this.isInput ? this.element : this.element.find("input"), this.component = !!this.element.hasClass("date") && this.element.find(".add-on, .input-group-addon, .btn"), this.component && 0 === this.component.length && (this.component = !1), this.isInline = !this.component && this.element.is("div"), this.picker = t(m.template), this._check_template(this.o.templates.leftArrow) && this.picker.find(".prev").html(this.o.templates.leftArrow), this._check_template(this.o.templates.rightArrow) && this.picker.find(".next").html(this.o.templates.rightArrow), this._buildEvents(), this._attachEvents(), this.isInline ? this.picker.addClass("datepicker-inline").appendTo(this.element) : this.picker.addClass("datepicker-dropdown dropdown-menu"), this.o.rtl && this.picker.addClass("datepicker-rtl"), this.o.calendarWeeks && this.picker.find(".datepicker-days .datepicker-switch, thead .datepicker-title, tfoot .today, tfoot .clear").attr("colspan", function(t, e) {
                return Number(e) + 1
            }), this._process_options({
                startDate: this._o.startDate,
                endDate: this._o.endDate,
                daysOfWeekDisabled: this.o.daysOfWeekDisabled,
                daysOfWeekHighlighted: this.o.daysOfWeekHighlighted,
                datesDisabled: this.o.datesDisabled
            }), this._allow_update = !1, this.setViewMode(this.o.startView), this._allow_update = !0, this.fillDow(), this.fillMonths(), this.update(), this.isInline && this.show()
        };
    l.prototype = {
        constructor: l,
        _resolveViewName: function(e) {
            return t.each(m.viewModes, function(i, n) {
                if (e === i || -1 !== t.inArray(e, n.names)) return e = i, !1
            }), e
        },
        _resolveDaysOfWeek: function(e) {
            return t.isArray(e) || (e = e.split(/[,\s]*/)), t.map(e, Number)
        },
        _check_template: function(i) {
            try {
                return i !== e && "" !== i && ((i.match(/[<>]/g) || []).length <= 0 || t(i).length > 0)
            } catch (t) {
                return !1
            }
        },
        _process_options: function(e) {
            this._o = t.extend({}, this._o, e);
            var r = this.o = t.extend({}, this._o),
                o = r.language;
            f[o] || (o = o.split("-")[0], f[o] || (o = u.language)), r.language = o, r.startView = this._resolveViewName(r.startView), r.minViewMode = this._resolveViewName(r.minViewMode), r.maxViewMode = this._resolveViewName(r.maxViewMode), r.startView = Math.max(this.o.minViewMode, Math.min(this.o.maxViewMode, r.startView)), !0 !== r.multidate && (r.multidate = Number(r.multidate) || !1, !1 !== r.multidate && (r.multidate = Math.max(0, r.multidate))), r.multidateSeparator = String(r.multidateSeparator), r.weekStart %= 7, r.weekEnd = (r.weekStart + 6) % 7;
            var s = m.parseFormat(r.format);
            r.startDate !== -1 / 0 && (r.startDate ? r.startDate instanceof Date ? r.startDate = this._local_to_utc(this._zero_time(r.startDate)) : r.startDate = m.parseDate(r.startDate, s, r.language, r.assumeNearbyYear) : r.startDate = -1 / 0), r.endDate !== 1 / 0 && (r.endDate ? r.endDate instanceof Date ? r.endDate = this._local_to_utc(this._zero_time(r.endDate)) : r.endDate = m.parseDate(r.endDate, s, r.language, r.assumeNearbyYear) : r.endDate = 1 / 0), r.daysOfWeekDisabled = this._resolveDaysOfWeek(r.daysOfWeekDisabled || []), r.daysOfWeekHighlighted = this._resolveDaysOfWeek(r.daysOfWeekHighlighted || []), r.datesDisabled = r.datesDisabled || [], t.isArray(r.datesDisabled) || (r.datesDisabled = r.datesDisabled.split(",")), r.datesDisabled = t.map(r.datesDisabled, function(t) {
                return m.parseDate(t, s, r.language, r.assumeNearbyYear)
            });
            var a = String(r.orientation).toLowerCase().split(/\s+/g),
                l = r.orientation.toLowerCase();
            if (a = t.grep(a, function(t) {
                    return /^auto|left|right|top|bottom$/.test(t)
            }), r.orientation = {
                x: "auto",
                y: "auto"
            }, l && "auto" !== l)
                if (1 === a.length) switch (a[0]) {
                    case "top":
                    case "bottom":
                        r.orientation.y = a[0];
                        break;
                    case "left":
                    case "right":
                        r.orientation.x = a[0]
                } else l = t.grep(a, function(t) {
                    return /^left|right$/.test(t)
                }), r.orientation.x = l[0] || "auto", l = t.grep(a, function(t) {
                    return /^top|bottom$/.test(t)
                }), r.orientation.y = l[0] || "auto";
            if (r.defaultViewDate instanceof Date || "string" == typeof r.defaultViewDate) r.defaultViewDate = m.parseDate(r.defaultViewDate, s, r.language, r.assumeNearbyYear);
            else if (r.defaultViewDate) {
                var c = r.defaultViewDate.year || (new Date).getFullYear(),
                    h = r.defaultViewDate.month || 0,
                    d = r.defaultViewDate.day || 1;
                r.defaultViewDate = i(c, h, d)
            } else r.defaultViewDate = n()
        },
        _events: [],
        _secondaryEvents: [],
        _applyEvents: function(t) {
            for (var i, n, r, o = 0; o < t.length; o++) i = t[o][0], 2 === t[o].length ? (n = e, r = t[o][1]) : 3 === t[o].length && (n = t[o][1], r = t[o][2]), i.on(r, n)
        },
        _unapplyEvents: function(t) {
            for (var i, n, r, o = 0; o < t.length; o++) i = t[o][0], 2 === t[o].length ? (r = e, n = t[o][1]) : 3 === t[o].length && (r = t[o][1], n = t[o][2]), i.off(n, r)
        },
        _buildEvents: function() {
            var e = {
                keyup: t.proxy(function(e) {
                    -1 === t.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) && this.update()
                }, this),
                keydown: t.proxy(this.keydown, this),
                paste: t.proxy(this.paste, this)
            };
            !0 === this.o.showOnFocus && (e.focus = t.proxy(this.show, this)), this.isInput ? this._events = [
                [this.element, e]
            ] : this.component && this.inputField.length ? this._events = [
                [this.inputField, e],
                [this.component, {
                    click: t.proxy(this.show, this)
                }]
            ] : this._events = [
                [this.element, {
                    click: t.proxy(this.show, this),
                    keydown: t.proxy(this.keydown, this)
                }]
            ], this._events.push([this.element, "*", {
                blur: t.proxy(function(t) {
                    this._focused_from = t.target
                }, this)
            }], [this.element, {
                blur: t.proxy(function(t) {
                    this._focused_from = t.target
                }, this)
            }]), this.o.immediateUpdates && this._events.push([this.element, {
                "changeYear changeMonth": t.proxy(function(t) {
                    this.update(t.date)
                }, this)
            }]), this._secondaryEvents = [
                [this.picker, {
                    click: t.proxy(this.click, this)
                }],
                [this.picker, ".prev, .next", {
                    click: t.proxy(this.navArrowsClick, this)
                }],
                [this.picker, ".day:not(.disabled)", {
                    click: t.proxy(this.dayCellClick, this)
                }],
                [t(window), {
                    resize: t.proxy(this.place, this)
                }],
                [t(document), {
                    "mousedown touchstart": t.proxy(function(t) {
                        this.element.is(t.target) || this.element.find(t.target).length || this.picker.is(t.target) || this.picker.find(t.target).length || this.isInline || this.hide()
                    }, this)
                }]
            ]
        },
        _attachEvents: function() {
            this._detachEvents(), this._applyEvents(this._events)
        },
        _detachEvents: function() {
            this._unapplyEvents(this._events)
        },
        _attachSecondaryEvents: function() {
            this._detachSecondaryEvents(), this._applyEvents(this._secondaryEvents)
        },
        _detachSecondaryEvents: function() {
            this._unapplyEvents(this._secondaryEvents)
        },
        _trigger: function(e, i) {
            var n = i || this.dates.get(-1),
                r = this._utc_to_local(n);
            this.element.trigger({
                type: e,
                date: r,
                viewMode: this.viewMode,
                dates: t.map(this.dates, this._utc_to_local),
                format: t.proxy(function(t, e) {
                    0 === arguments.length ? (t = this.dates.length - 1, e = this.o.format) : "string" == typeof t && (e = t, t = this.dates.length - 1), e = e || this.o.format;
                    var i = this.dates.get(t);
                    return m.formatDate(i, e, this.o.language)
                }, this)
            })
        },
        show: function() {
            if (!(this.inputField.prop("disabled") || this.inputField.prop("readonly") && !1 === this.o.enableOnReadonly)) return this.isInline || this.picker.appendTo(this.o.container), this.place(), this.picker.show(), this._attachSecondaryEvents(), this._trigger("show"), (window.navigator.msMaxTouchPoints || "ontouchstart" in document) && this.o.disableTouchKeyboard && t(this.element).blur(), this
        },
        hide: function() {
            return this.isInline || !this.picker.is(":visible") ? this : (this.focusDate = null, this.picker.hide().detach(), this._detachSecondaryEvents(), this.setViewMode(this.o.startView), this.o.forceParse && this.inputField.val() && this.setValue(), this._trigger("hide"), this)
        },
        destroy: function() {
            return this.hide(), this._detachEvents(), this._detachSecondaryEvents(), this.picker.remove(), delete this.element.data().datepicker, this.isInput || delete this.element.data().date, this
        },
        paste: function(e) {
            var i;
            if (e.originalEvent.clipboardData && e.originalEvent.clipboardData.types && -1 !== t.inArray("text/plain", e.originalEvent.clipboardData.types)) i = e.originalEvent.clipboardData.getData("text/plain");
            else {
                if (!window.clipboardData) return;
                i = window.clipboardData.getData("Text")
            }
            this.setDate(i), this.update(), e.preventDefault()
        },
        _utc_to_local: function(t) {
            if (!t) return t;
            var e = new Date(t.getTime() + 6e4 * t.getTimezoneOffset());
            return e.getTimezoneOffset() !== t.getTimezoneOffset() && (e = new Date(t.getTime() + 6e4 * e.getTimezoneOffset())), e
        },
        _local_to_utc: function(t) {
            return t && new Date(t.getTime() - 6e4 * t.getTimezoneOffset())
        },
        _zero_time: function(t) {
            return t && new Date(t.getFullYear(), t.getMonth(), t.getDate())
        },
        _zero_utc_time: function(t) {
            return t && i(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate())
        },
        getDates: function() {
            return t.map(this.dates, this._utc_to_local)
        },
        getUTCDates: function() {
            return t.map(this.dates, function(t) {
                return new Date(t)
            })
        },
        getDate: function() {
            return this._utc_to_local(this.getUTCDate())
        },
        getUTCDate: function() {
            var t = this.dates.get(-1);
            return t !== e ? new Date(t) : null
        },
        clearDates: function() {
            this.inputField.val(""), this.update(), this._trigger("changeDate"), this.o.autoclose && this.hide()
        },
        setDates: function() {
            var e = t.isArray(arguments[0]) ? arguments[0] : arguments;
            return this.update.apply(this, e), this._trigger("changeDate"), this.setValue(), this
        },
        setUTCDates: function() {
            var e = t.isArray(arguments[0]) ? arguments[0] : arguments;
            return this.setDates.apply(this, t.map(e, this._utc_to_local)), this
        },
        setDate: o("setDates"),
        setUTCDate: o("setUTCDates"),
        remove: o("destroy", "Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead"),
        setValue: function() {
            var t = this.getFormattedDate();
            return this.inputField.val(t), this
        },
        getFormattedDate: function(i) {
            i === e && (i = this.o.format);
            var n = this.o.language;
            return t.map(this.dates, function(t) {
                return m.formatDate(t, i, n)
            }).join(this.o.multidateSeparator)
        },
        getStartDate: function() {
            return this.o.startDate
        },
        setStartDate: function(t) {
            return this._process_options({
                startDate: t
            }), this.update(), this.updateNavArrows(), this
        },
        getEndDate: function() {
            return this.o.endDate
        },
        setEndDate: function(t) {
            return this._process_options({
                endDate: t
            }), this.update(), this.updateNavArrows(), this
        },
        setDaysOfWeekDisabled: function(t) {
            return this._process_options({
                daysOfWeekDisabled: t
            }), this.update(), this
        },
        setDaysOfWeekHighlighted: function(t) {
            return this._process_options({
                daysOfWeekHighlighted: t
            }), this.update(), this
        },
        setDatesDisabled: function(t) {
            return this._process_options({
                datesDisabled: t
            }), this.update(), this
        },
        place: function() {
            if (this.isInline) return this;
            var e = this.picker.outerWidth(),
                i = this.picker.outerHeight(),
                n = t(this.o.container),
                r = n.width(),
                o = "body" === this.o.container ? t(document).scrollTop() : n.scrollTop(),
                s = n.offset(),
                a = [0];
            this.element.parents().each(function() {
                var e = t(this).css("z-index");
                "auto" !== e && 0 !== Number(e) && a.push(Number(e))
            });
            var l = Math.max.apply(Math, a) + this.o.zIndexOffset,
                c = this.component ? this.component.parent().offset() : this.element.offset(),
                h = this.component ? this.component.outerHeight(!0) : this.element.outerHeight(!1),
                d = this.component ? this.component.outerWidth(!0) : this.element.outerWidth(!1),
                u = c.left - s.left,
                p = c.top - s.top;
            "body" !== this.o.container && (p += o), this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"), "auto" !== this.o.orientation.x ? (this.picker.addClass("datepicker-orient-" + this.o.orientation.x), "right" === this.o.orientation.x && (u -= e - d)) : c.left < 0 ? (this.picker.addClass("datepicker-orient-left"), u -= c.left - 10) : u + e > r ? (this.picker.addClass("datepicker-orient-right"), u += d - e) : this.o.rtl ? this.picker.addClass("datepicker-orient-right") : this.picker.addClass("datepicker-orient-left");
            var f = this.o.orientation.y;
            if ("auto" === f && (f = -o + p - i < 0 ? "bottom" : "top"), this.picker.addClass("datepicker-orient-" + f), "top" === f ? p -= i + parseInt(this.picker.css("padding-top")) : p += h, this.o.rtl) {
                var m = r - (u + d);
                this.picker.css({
                    top: p,
                    right: m,
                    zIndex: l
                })
            } else this.picker.css({
                top: p,
                left: u,
                zIndex: l
            });
            return this
        },
        _allow_update: !0,
        update: function() {
            if (!this._allow_update) return this;
            var e = this.dates.copy(),
                i = [],
                n = !1;
            return arguments.length ? (t.each(arguments, t.proxy(function(t, e) {
                e instanceof Date && (e = this._local_to_utc(e)), i.push(e)
            }, this)), n = !0) : (i = (i = this.isInput ? this.element.val() : this.element.data("date") || this.inputField.val()) && this.o.multidate ? i.split(this.o.multidateSeparator) : [i], delete this.element.data().date), i = t.map(i, t.proxy(function(t) {
                return m.parseDate(t, this.o.format, this.o.language, this.o.assumeNearbyYear)
            }, this)), i = t.grep(i, t.proxy(function(t) {
                return !this.dateWithinRange(t) || !t
            }, this), !0), this.dates.replace(i), this.o.updateViewDate && (this.dates.length ? this.viewDate = new Date(this.dates.get(-1)) : this.viewDate < this.o.startDate ? this.viewDate = new Date(this.o.startDate) : this.viewDate > this.o.endDate ? this.viewDate = new Date(this.o.endDate) : this.viewDate = this.o.defaultViewDate), n ? (this.setValue(), this.element.change()) : this.dates.length && String(e) !== String(this.dates) && n && (this._trigger("changeDate"), this.element.change()), !this.dates.length && e.length && (this._trigger("clearDate"), this.element.change()), this.fill(), this
        },
        fillDow: function() {
            if (this.o.showWeekDays) {
                var e = this.o.weekStart,
                    i = "<tr>";
                for (this.o.calendarWeeks && (i += '<th class="cw">&#160;</th>'); e < this.o.weekStart + 7;) i += '<th class="dow', -1 !== t.inArray(e, this.o.daysOfWeekDisabled) && (i += " disabled"), i += '">' + f[this.o.language].daysMin[e++ % 7] + "</th>";
                i += "</tr>", this.picker.find(".datepicker-days thead").append(i)
            }
        },
        fillMonths: function() {
            for (var t = this._utc_to_local(this.viewDate), e = "", i = 0; i < 12; i++) e += '<span class="month' + (t && t.getMonth() === i ? " focused" : "") + '">' + f[this.o.language].monthsShort[i] + "</span>";
            this.picker.find(".datepicker-months td").html(e)
        },
        setRange: function(e) {
            e && e.length ? this.range = t.map(e, function(t) {
                return t.valueOf()
            }) : delete this.range, this.fill()
        },
        getClassNames: function(e) {
            var i = [],
                o = this.viewDate.getUTCFullYear(),
                s = this.viewDate.getUTCMonth(),
                a = n();
            return e.getUTCFullYear() < o || e.getUTCFullYear() === o && e.getUTCMonth() < s ? i.push("old") : (e.getUTCFullYear() > o || e.getUTCFullYear() === o && e.getUTCMonth() > s) && i.push("new"), this.focusDate && e.valueOf() === this.focusDate.valueOf() && i.push("focused"), this.o.todayHighlight && r(e, a) && i.push("today"), -1 !== this.dates.contains(e) && i.push("active"), this.dateWithinRange(e) || i.push("disabled"), this.dateIsDisabled(e) && i.push("disabled", "disabled-date"), -1 !== t.inArray(e.getUTCDay(), this.o.daysOfWeekHighlighted) && i.push("highlighted"), this.range && (e > this.range[0] && e < this.range[this.range.length - 1] && i.push("range"), -1 !== t.inArray(e.valueOf(), this.range) && i.push("selected"), e.valueOf() === this.range[0] && i.push("range-start"), e.valueOf() === this.range[this.range.length - 1] && i.push("range-end")), i
        },
        _fill_yearsView: function(i, n, r, o, s, a, l) {
            for (var c, h, d, u = "", p = r / 10, f = this.picker.find(i), m = Math.floor(o / r) * r, g = m + 9 * p, v = Math.floor(this.viewDate.getFullYear() / p) * p, y = t.map(this.dates, function(t) {
                    return Math.floor(t.getUTCFullYear() / p) * p
            }), b = m - p; b <= g + p; b += p) c = [n], h = null, b === m - p ? c.push("old") : b === g + p && c.push("new"), -1 !== t.inArray(b, y) && c.push("active"), (b < s || b > a) && c.push("disabled"), b === v && c.push("focused"), l !== t.noop && ((d = l(new Date(b, 0, 1))) === e ? d = {} : "boolean" == typeof d ? d = {
                enabled: d
            } : "string" == typeof d && (d = {
                classes: d
            }), !1 === d.enabled && c.push("disabled"), d.classes && (c = c.concat(d.classes.split(/\s+/))), d.tooltip && (h = d.tooltip)), u += '<span class="' + c.join(" ") + '"' + (h ? ' title="' + h + '"' : "") + ">" + b + "</span>";
            f.find(".datepicker-switch").text(m + "-" + g), f.find("td").html(u)
        },
        fill: function() {
            var n, r, o = new Date(this.viewDate),
                s = o.getUTCFullYear(),
                a = o.getUTCMonth(),
                l = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCFullYear() : -1 / 0,
                c = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCMonth() : -1 / 0,
                h = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCFullYear() : 1 / 0,
                d = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCMonth() : 1 / 0,
                u = f[this.o.language].today || f.en.today || "",
                p = f[this.o.language].clear || f.en.clear || "",
                g = f[this.o.language].titleFormat || f.en.titleFormat;
            if (!isNaN(s) && !isNaN(a)) {
                this.picker.find(".datepicker-days .datepicker-switch").text(m.formatDate(o, g, this.o.language)), this.picker.find("tfoot .today").text(u).css("display", !0 === this.o.todayBtn || "linked" === this.o.todayBtn ? "table-cell" : "none"), this.picker.find("tfoot .clear").text(p).css("display", !0 === this.o.clearBtn ? "table-cell" : "none"), this.picker.find("thead .datepicker-title").text(this.o.title).css("display", "string" == typeof this.o.title && "" !== this.o.title ? "table-cell" : "none"), this.updateNavArrows(), this.fillMonths();
                var v = i(s, a, 0),
                    y = v.getUTCDate();
                v.setUTCDate(y - (v.getUTCDay() - this.o.weekStart + 7) % 7);
                var b = new Date(v);
                v.getUTCFullYear() < 100 && b.setUTCFullYear(v.getUTCFullYear()), b.setUTCDate(b.getUTCDate() + 42), b = b.valueOf();
                for (var _, x, w = []; v.valueOf() < b;) {
                    if ((_ = v.getUTCDay()) === this.o.weekStart && (w.push("<tr>"), this.o.calendarWeeks)) {
                        var k = new Date(+v + (this.o.weekStart - _ - 7) % 7 * 864e5),
                            C = new Date(Number(k) + (11 - k.getUTCDay()) % 7 * 864e5),
                            S = new Date(Number(S = i(C.getUTCFullYear(), 0, 1)) + (11 - S.getUTCDay()) % 7 * 864e5),
                            D = (C - S) / 864e5 / 7 + 1;
                        w.push('<td class="cw">' + D + "</td>")
                    }(x = this.getClassNames(v)).push("day");
                    var T = v.getUTCDate();
                    this.o.beforeShowDay !== t.noop && ((r = this.o.beforeShowDay(this._utc_to_local(v))) === e ? r = {} : "boolean" == typeof r ? r = {
                        enabled: r
                    } : "string" == typeof r && (r = {
                        classes: r
                    }), !1 === r.enabled && x.push("disabled"), r.classes && (x = x.concat(r.classes.split(/\s+/))), r.tooltip && (n = r.tooltip), r.content && (T = r.content)), x = t.isFunction(t.uniqueSort) ? t.uniqueSort(x) : t.unique(x), w.push('<td class="' + x.join(" ") + '"' + (n ? ' title="' + n + '"' : "") + ' data-date="' + v.getTime().toString() + '">' + T + "</td>"), n = null, _ === this.o.weekEnd && w.push("</tr>"), v.setUTCDate(v.getUTCDate() + 1)
                }
                this.picker.find(".datepicker-days tbody").html(w.join(""));
                var A = f[this.o.language].monthsTitle || f.en.monthsTitle || "Months",
                    M = this.picker.find(".datepicker-months").find(".datepicker-switch").text(this.o.maxViewMode < 2 ? A : s).end().find("tbody span").removeClass("active");
                if (t.each(this.dates, function(t, e) {
                        e.getUTCFullYear() === s && M.eq(e.getUTCMonth()).addClass("active")
                }), (s < l || s > h) && M.addClass("disabled"), s === l && M.slice(0, c).addClass("disabled"), s === h && M.slice(d + 1).addClass("disabled"), this.o.beforeShowMonth !== t.noop) {
                    var E = this;
                    t.each(M, function(i, n) {
                        var r = new Date(s, i, 1),
                            o = E.o.beforeShowMonth(r);
                        o === e ? o = {} : "boolean" == typeof o ? o = {
                            enabled: o
                        } : "string" == typeof o && (o = {
                            classes: o
                        }), !1 !== o.enabled || t(n).hasClass("disabled") || t(n).addClass("disabled"), o.classes && t(n).addClass(o.classes), o.tooltip && t(n).prop("title", o.tooltip)
                    })
                }
                this._fill_yearsView(".datepicker-years", "year", 10, s, l, h, this.o.beforeShowYear), this._fill_yearsView(".datepicker-decades", "decade", 100, s, l, h, this.o.beforeShowDecade), this._fill_yearsView(".datepicker-centuries", "century", 1e3, s, l, h, this.o.beforeShowCentury)
            }
        },
        updateNavArrows: function() {
            if (this._allow_update) {
                var t, e, i = new Date(this.viewDate),
                    n = i.getUTCFullYear(),
                    r = i.getUTCMonth(),
                    o = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCFullYear() : -1 / 0,
                    s = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCMonth() : -1 / 0,
                    a = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCFullYear() : 1 / 0,
                    l = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCMonth() : 1 / 0,
                    c = 1;
                switch (this.viewMode) {
                    case 4:
                        c *= 10;
                    case 3:
                        c *= 10;
                    case 2:
                        c *= 10;
                    case 1:
                        t = Math.floor(n / c) * c < o, e = Math.floor(n / c) * c + c > a;
                        break;
                    case 0:
                        t = n <= o && r < s, e = n >= a && r > l
                }
                this.picker.find(".prev").toggleClass("disabled", t), this.picker.find(".next").toggleClass("disabled", e)
            }
        },
        click: function(e) {
            var r, o, s, a;
            e.preventDefault(), e.stopPropagation(), (r = t(e.target)).hasClass("datepicker-switch") && this.viewMode !== this.o.maxViewMode && this.setViewMode(this.viewMode + 1), r.hasClass("today") && !r.hasClass("day") && (this.setViewMode(0), this._setDate(n(), "linked" === this.o.todayBtn ? null : "view")), r.hasClass("clear") && this.clearDates(), r.hasClass("disabled") || (r.hasClass("month") || r.hasClass("year") || r.hasClass("decade") || r.hasClass("century")) && (this.viewDate.setUTCDate(1), o = 1, 1 === this.viewMode ? (a = r.parent().find("span").index(r), s = this.viewDate.getUTCFullYear(), this.viewDate.setUTCMonth(a)) : (a = 0, s = Number(r.text()), this.viewDate.setUTCFullYear(s)), this._trigger(m.viewModes[this.viewMode - 1].e, this.viewDate), this.viewMode === this.o.minViewMode ? this._setDate(i(s, a, o)) : (this.setViewMode(this.viewMode - 1), this.fill())), this.picker.is(":visible") && this._focused_from && this._focused_from.focus(), delete this._focused_from
        },
        dayCellClick: function(e) {
            var i = t(e.currentTarget).data("date"),
                n = new Date(i);
            this.o.updateViewDate && (n.getUTCFullYear() !== this.viewDate.getUTCFullYear() && this._trigger("changeYear", this.viewDate), n.getUTCMonth() !== this.viewDate.getUTCMonth() && this._trigger("changeMonth", this.viewDate)), this._setDate(n)
        },
        navArrowsClick: function(e) {
            var i = t(e.currentTarget).hasClass("prev") ? -1 : 1;
            0 !== this.viewMode && (i *= 12 * m.viewModes[this.viewMode].navStep), this.viewDate = this.moveMonth(this.viewDate, i), this._trigger(m.viewModes[this.viewMode].e, this.viewDate), this.fill()
        },
        _toggle_multidate: function(t) {
            var e = this.dates.contains(t);
            if (t || this.dates.clear(), -1 !== e ? (!0 === this.o.multidate || this.o.multidate > 1 || this.o.toggleActive) && this.dates.remove(e) : !1 === this.o.multidate ? (this.dates.clear(), this.dates.push(t)) : this.dates.push(t), "number" == typeof this.o.multidate)
                for (; this.dates.length > this.o.multidate;) this.dates.remove(0)
        },
        _setDate: function(t, e) {
            e && "date" !== e || this._toggle_multidate(t && new Date(t)), (!e && this.o.updateViewDate || "view" === e) && (this.viewDate = t && new Date(t)), this.fill(), this.setValue(), e && "view" === e || this._trigger("changeDate"), this.inputField.trigger("change"), !this.o.autoclose || e && "date" !== e || this.hide()
        },
        moveDay: function(t, e) {
            var i = new Date(t);
            return i.setUTCDate(t.getUTCDate() + e), i
        },
        moveWeek: function(t, e) {
            return this.moveDay(t, 7 * e)
        },
        moveMonth: function(t, e) {
            if (! function(t) {
                    return t && !isNaN(t.getTime())
            }(t)) return this.o.defaultViewDate;
            if (!e) return t;
            var i, n, r = new Date(t.valueOf()),
                o = r.getUTCDate(),
                s = r.getUTCMonth(),
                a = Math.abs(e);
            if (e = e > 0 ? 1 : -1, 1 === a) n = -1 === e ? function() {
                return r.getUTCMonth() === s
            } : function() {
                return r.getUTCMonth() !== i
            }, i = s + e, r.setUTCMonth(i), i = (i + 12) % 12;
            else {
                for (var l = 0; l < a; l++) r = this.moveMonth(r, e);
                i = r.getUTCMonth(), r.setUTCDate(o), n = function() {
                    return i !== r.getUTCMonth()
                }
            }
            for (; n();) r.setUTCDate(--o), r.setUTCMonth(i);
            return r
        },
        moveYear: function(t, e) {
            return this.moveMonth(t, 12 * e)
        },
        moveAvailableDate: function(t, e, i) {
            do {
                if (t = this[i](t, e), !this.dateWithinRange(t)) return !1;
                i = "moveDay"
            } while (this.dateIsDisabled(t));
            return t
        },
        weekOfDateIsDisabled: function(e) {
            return -1 !== t.inArray(e.getUTCDay(), this.o.daysOfWeekDisabled)
        },
        dateIsDisabled: function(e) {
            return this.weekOfDateIsDisabled(e) || t.grep(this.o.datesDisabled, function(t) {
                return r(e, t)
            }).length > 0
        },
        dateWithinRange: function(t) {
            return t >= this.o.startDate && t <= this.o.endDate
        },
        keydown: function(t) {
            if (this.picker.is(":visible")) {
                var e, i, n = !1,
                    r = this.focusDate || this.viewDate;
                switch (t.keyCode) {
                    case 27:
                        this.focusDate ? (this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill()) : this.hide(), t.preventDefault(), t.stopPropagation();
                        break;
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                        if (!this.o.keyboardNavigation || 7 === this.o.daysOfWeekDisabled.length) break;
                        e = 37 === t.keyCode || 38 === t.keyCode ? -1 : 1, 0 === this.viewMode ? t.ctrlKey ? (i = this.moveAvailableDate(r, e, "moveYear")) && this._trigger("changeYear", this.viewDate) : t.shiftKey ? (i = this.moveAvailableDate(r, e, "moveMonth")) && this._trigger("changeMonth", this.viewDate) : 37 === t.keyCode || 39 === t.keyCode ? i = this.moveAvailableDate(r, e, "moveDay") : this.weekOfDateIsDisabled(r) || (i = this.moveAvailableDate(r, e, "moveWeek")) : 1 === this.viewMode ? (38 !== t.keyCode && 40 !== t.keyCode || (e *= 4), i = this.moveAvailableDate(r, e, "moveMonth")) : 2 === this.viewMode && (38 !== t.keyCode && 40 !== t.keyCode || (e *= 4), i = this.moveAvailableDate(r, e, "moveYear")), i && (this.focusDate = this.viewDate = i, this.setValue(), this.fill(), t.preventDefault());
                        break;
                    case 13:
                        if (!this.o.forceParse) break;
                        r = this.focusDate || this.dates.get(-1) || this.viewDate, this.o.keyboardNavigation && (this._toggle_multidate(r), n = !0), this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.setValue(), this.fill(), this.picker.is(":visible") && (t.preventDefault(), t.stopPropagation(), this.o.autoclose && this.hide());
                        break;
                    case 9:
                        this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill(), this.hide()
                }
                n && (this.dates.length ? this._trigger("changeDate") : this._trigger("clearDate"), this.inputField.trigger("change"))
            } else 40 !== t.keyCode && 27 !== t.keyCode || (this.show(), t.stopPropagation())
        },
        setViewMode: function(t) {
            this.viewMode = t, this.picker.children("div").hide().filter(".datepicker-" + m.viewModes[this.viewMode].clsName).show(), this.updateNavArrows(), this._trigger("changeViewMode", new Date(this.viewDate))
        }
    };
    var c = function(e, i) {
        t.data(e, "datepicker", this), this.element = t(e), this.inputs = t.map(i.inputs, function(t) {
            return t.jquery ? t[0] : t
        }), delete i.inputs, this.keepEmptyValues = i.keepEmptyValues, delete i.keepEmptyValues, d.call(t(this.inputs), i).on("changeDate", t.proxy(this.dateUpdated, this)), this.pickers = t.map(this.inputs, function(e) {
            return t.data(e, "datepicker")
        }), this.updateDates()
    };
    c.prototype = {
        updateDates: function() {
            this.dates = t.map(this.pickers, function(t) {
                return t.getUTCDate()
            }), this.updateRanges()
        },
        updateRanges: function() {
            var e = t.map(this.dates, function(t) {
                return t.valueOf()
            });
            t.each(this.pickers, function(t, i) {
                i.setRange(e)
            })
        },
        clearDates: function() {
            t.each(this.pickers, function(t, e) {
                e.clearDates()
            })
        },
        dateUpdated: function(i) {
            if (!this.updating) {
                this.updating = !0;
                var n = t.data(i.target, "datepicker");
                if (n !== e) {
                    var r = n.getUTCDate(),
                        o = this.keepEmptyValues,
                        s = t.inArray(i.target, this.inputs),
                        a = s - 1,
                        l = s + 1,
                        c = this.inputs.length;
                    if (-1 !== s) {
                        if (t.each(this.pickers, function(t, e) {
                                e.getUTCDate() || e !== n && o || e.setUTCDate(r)
                        }), r < this.dates[a])
                            for (; a >= 0 && r < this.dates[a];) this.pickers[a--].setUTCDate(r);
                        else if (r > this.dates[l])
                            for (; l < c && r > this.dates[l];) this.pickers[l++].setUTCDate(r);
                        this.updateDates(), delete this.updating
                    }
                }
            }
        },
        destroy: function() {
            t.map(this.pickers, function(t) {
                t.destroy()
            }), t(this.inputs).off("changeDate", this.dateUpdated), delete this.element.data().datepicker
        },
        remove: o("destroy", "Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead")
    };
    var h = t.fn.datepicker,
        d = function(i) {
            var n, r = Array.apply(null, arguments);
            if (r.shift(), this.each(function() {
                    var e = t(this),
                        o = e.data("datepicker"),
                        a = "object" == typeof i && i;
                    if (!o) {
                        var h = function(e, i) {
                                function n(t, e) {
                                    return e.toLowerCase()
            }
                                var r = t(e).data(),
                                    o = {},
                                    s = new RegExp("^" + i.toLowerCase() + "([A-Z])");
                                for (var a in i = new RegExp("^" + i.toLowerCase()), r) i.test(a) && (o[a.replace(s, n)] = r[a]);
                                return o
            }(this, "date"),
                            d = s(t.extend({}, u, h, a).language),
                            p = t.extend({}, u, d, h, a);
                        e.hasClass("input-daterange") || p.inputs ? (t.extend(p, {
                inputs: p.inputs || e.find("input").toArray()
            }), o = new c(this, p)) : o = new l(this, p), e.data("datepicker", o)
            }
                    "string" == typeof i && "function" == typeof o[i] && (n = o[i].apply(o, r))
            }), n === e || n instanceof l || n instanceof c) return this;
            if (this.length > 1) throw new Error("Using only allowed for the collection of a single element (" + i + " function)");
            return n
        };
    t.fn.datepicker = d;
    var u = t.fn.datepicker.defaults = {
        assumeNearbyYear: !1,
        autoclose: !1,
        beforeShowDay: t.noop,
        beforeShowMonth: t.noop,
        beforeShowYear: t.noop,
        beforeShowDecade: t.noop,
        beforeShowCentury: t.noop,
        calendarWeeks: !1,
        clearBtn: !1,
        toggleActive: !1,
        daysOfWeekDisabled: [],
        daysOfWeekHighlighted: [],
        datesDisabled: [],
        endDate: 1 / 0,
        forceParse: !0,
        format: "mm/dd/yyyy",
        keepEmptyValues: !1,
        keyboardNavigation: !0,
        language: "en",
        minViewMode: 0,
        maxViewMode: 4,
        multidate: !1,
        multidateSeparator: ",",
        orientation: "auto",
        rtl: !1,
        startDate: -1 / 0,
        startView: 0,
        todayBtn: !1,
        todayHighlight: !1,
        updateViewDate: !0,
        weekStart: 0,
        disableTouchKeyboard: !1,
        enableOnReadonly: !0,
        showOnFocus: !0,
        zIndexOffset: 10,
        container: "body",
        immediateUpdates: !1,
        title: "",
        templates: {
            leftArrow: "&#x00AB;",
            rightArrow: "&#x00BB;"
        },
        showWeekDays: !0
    },
        p = t.fn.datepicker.locale_opts = ["format", "rtl", "weekStart"];
    t.fn.datepicker.Constructor = l;
    var f = t.fn.datepicker.dates = {
        en: {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            today: "Today",
            clear: "Clear",
            titleFormat: "MM yyyy"
        }
    },
        m = {
            viewModes: [{
                names: ["days", "month"],
                clsName: "days",
                e: "changeMonth"
            }, {
                names: ["months", "year"],
                clsName: "months",
                e: "changeYear",
                navStep: 1
            }, {
                names: ["years", "decade"],
                clsName: "years",
                e: "changeDecade",
                navStep: 10
            }, {
                names: ["decades", "century"],
                clsName: "decades",
                e: "changeCentury",
                navStep: 100
            }, {
                names: ["centuries", "millennium"],
                clsName: "centuries",
                e: "changeMillennium",
                navStep: 1e3
            }],
            validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
            nonpunctuation: /[^ -\/:-@\u5e74\u6708\u65e5\[-`{-~\t\n\r]+/g,
            parseFormat: function(t) {
                if ("function" == typeof t.toValue && "function" == typeof t.toDisplay) return t;
                var e = t.replace(this.validParts, "\0").split("\0"),
                    i = t.match(this.validParts);
                if (!e || !e.length || !i || 0 === i.length) throw new Error("Invalid date format.");
                return {
                    separators: e,
                    parts: i
                }
            },
            parseDate: function(i, r, o, s) {
                function a() {
                    var t = this.slice(0, c[u].length),
                        e = c[u].slice(0, t.length);
                    return t.toLowerCase() === e.toLowerCase()
                }
                if (!i) return e;
                if (i instanceof Date) return i;
                if ("string" == typeof r && (r = m.parseFormat(r)), r.toValue) return r.toValue(i, r, o);
                var c, h, d, u, p, g = {
                    d: "moveDay",
                    m: "moveMonth",
                    w: "moveWeek",
                    y: "moveYear"
                },
                    v = {
                        yesterday: "-1d",
                        today: "+0d",
                        tomorrow: "+1d"
                    };
                if (i in v && (i = v[i]), /^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/i.test(i)) {
                    for (c = i.match(/([\-+]\d+)([dmwy])/gi), i = new Date, u = 0; u < c.length; u++) h = c[u].match(/([\-+]\d+)([dmwy])/i), d = Number(h[1]), p = g[h[2].toLowerCase()], i = l.prototype[p](i, d);
                    return l.prototype._zero_utc_time(i)
                }
                c = i && i.match(this.nonpunctuation) || [];
                var y, b, _ = {},
                    x = ["yyyy", "yy", "M", "MM", "m", "mm", "d", "dd"],
                    w = {
                        yyyy: function(t, e) {
                            return t.setUTCFullYear(s ? function(t, e) {
                                return !0 === e && (e = 10), t < 100 && (t += 2e3) > (new Date).getFullYear() + e && (t -= 100), t
                            }(e, s) : e)
                        },
                        m: function(t, e) {
                            if (isNaN(t)) return t;
                            for (e -= 1; e < 0;) e += 12;
                            for (e %= 12, t.setUTCMonth(e); t.getUTCMonth() !== e;) t.setUTCDate(t.getUTCDate() - 1);
                            return t
                        },
                        d: function(t, e) {
                            return t.setUTCDate(e)
                        }
                    };
                w.yy = w.yyyy, w.M = w.MM = w.mm = w.m, w.dd = w.d, i = n();
                var k = r.parts.slice();
                if (c.length !== k.length && (k = t(k).filter(function(e, i) {
                        return -1 !== t.inArray(i, x)
                }).toArray()), c.length === k.length) {
                    var C, S, D;
                    for (u = 0, C = k.length; u < C; u++) {
                        if (y = parseInt(c[u], 10), h = k[u], isNaN(y)) switch (h) {
                            case "MM":
                                b = t(f[o].months).filter(a), y = t.inArray(b[0], f[o].months) + 1;
                                break;
                            case "M":
                                b = t(f[o].monthsShort).filter(a), y = t.inArray(b[0], f[o].monthsShort) + 1
                        }
                        _[h] = y
                    }
                    for (u = 0; u < x.length; u++)(D = x[u]) in _ && !isNaN(_[D]) && (S = new Date(i), w[D](S, _[D]), isNaN(S) || (i = S))
                }
                return i
            },
            formatDate: function(e, i, n) {
                if (!e) return "";
                if ("string" == typeof i && (i = m.parseFormat(i)), i.toDisplay) return i.toDisplay(e, i, n);
                var r = {
                    d: e.getUTCDate(),
                    D: f[n].daysShort[e.getUTCDay()],
                    DD: f[n].days[e.getUTCDay()],
                    m: e.getUTCMonth() + 1,
                    M: f[n].monthsShort[e.getUTCMonth()],
                    MM: f[n].months[e.getUTCMonth()],
                    yy: e.getUTCFullYear().toString().substring(2),
                    yyyy: e.getUTCFullYear()
                };
                r.dd = (r.d < 10 ? "0" : "") + r.d, r.mm = (r.m < 10 ? "0" : "") + r.m, e = [];
                for (var o = t.extend([], i.separators), s = 0, a = i.parts.length; s <= a; s++) o.length && e.push(o.shift()), e.push(r[i.parts[s]]);
                return e.join("")
            },
            headTemplate: '<thead><tr><th colspan="7" class="datepicker-title"></th></tr><tr><th class="prev">' + u.templates.leftArrow + '</th><th colspan="5" class="datepicker-switch"></th><th class="next">' + u.templates.rightArrow + "</th></tr></thead>",
            contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
            footTemplate: '<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'
        };
    m.template = '<div class="datepicker"><div class="datepicker-days"><table class="table-condensed">' + m.headTemplate + "<tbody></tbody>" + m.footTemplate + '</table></div><div class="datepicker-months"><table class="table-condensed">' + m.headTemplate + m.contTemplate + m.footTemplate + '</table></div><div class="datepicker-years"><table class="table-condensed">' + m.headTemplate + m.contTemplate + m.footTemplate + '</table></div><div class="datepicker-decades"><table class="table-condensed">' + m.headTemplate + m.contTemplate + m.footTemplate + '</table></div><div class="datepicker-centuries"><table class="table-condensed">' + m.headTemplate + m.contTemplate + m.footTemplate + "</table></div></div>", t.fn.datepicker.DPGlobal = m, t.fn.datepicker.noConflict = function() {
        return t.fn.datepicker = h, this
    }, t.fn.datepicker.version = "1.8.0", t.fn.datepicker.deprecated = function(t) {
        var e = window.console;
        e && e.warn && e.warn("DEPRECATED: " + t)
    }, t(document).on("focus.datepicker.data-api click.datepicker.data-api", '[data-provide="datepicker"]', function(e) {
        var i = t(this);
        i.data("datepicker") || (e.preventDefault(), d.call(i, "show"))
    }), t(function() {
        d.call(t('[data-provide="datepicker-inline"]'))
    })
}), $.fn.datepicker.defaults.zIndexOffset = 10,
    function(t) {
        "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? t(require("jquery")) : t(jQuery)
    }(function(t, e) {
        function i() {
            return new Date(Date.UTC.apply(Date, arguments))
        }
        "indexOf" in Array.prototype || (Array.prototype.indexOf = function(t, i) {
            i === e && (i = 0), i < 0 && (i += this.length), i < 0 && (i = 0);
            for (var n = this.length; i < n; i++)
                if (i in this && this[i] === t) return i;
            return -1
        });
        var n = function(i, n) {
            var r = this;
            this.element = t(i), this.container = n.container || "body", this.language = n.language || this.element.data("date-language") || "en", this.language = this.language in o ? this.language : this.language.split("-")[0], this.language = this.language in o ? this.language : "en", this.isRTL = o[this.language].rtl || !1, this.formatType = n.formatType || this.element.data("format-type") || "standard", this.format = s.parseFormat(n.format || this.element.data("date-format") || o[this.language].format || s.getDefaultFormat(this.formatType, "input"), this.formatType), this.isInline = !1, this.isVisible = !1, this.isInput = this.element.is("input"), this.fontAwesome = n.fontAwesome || this.element.data("font-awesome") || !1, this.bootcssVer = n.bootcssVer || (this.isInput ? this.element.is(".form-control") ? 3 : 2 : this.bootcssVer = this.element.is(".input-group") ? 3 : 2), this.component = !!this.element.is(".date") && (3 === this.bootcssVer ? this.element.find(".input-group-addon .glyphicon-th, .input-group-addon .glyphicon-time, .input-group-addon .glyphicon-remove, .input-group-addon .glyphicon-calendar, .input-group-addon .fa-calendar, .input-group-addon .fa-clock-o").parent() : this.element.find(".add-on .icon-th, .add-on .icon-time, .add-on .icon-calendar, .add-on .fa-calendar, .add-on .fa-clock-o").parent()), this.componentReset = !!this.element.is(".date") && (3 === this.bootcssVer ? this.element.find(".input-group-addon .glyphicon-remove, .input-group-addon .fa-times").parent() : this.element.find(".add-on .icon-remove, .add-on .fa-times").parent()), this.hasInput = this.component && this.element.find("input").length, this.component && 0 === this.component.length && (this.component = !1), this.linkField = n.linkField || this.element.data("link-field") || !1, this.linkFormat = s.parseFormat(n.linkFormat || this.element.data("link-format") || s.getDefaultFormat(this.formatType, "link"), this.formatType), this.minuteStep = n.minuteStep || this.element.data("minute-step") || 5, this.pickerPosition = n.pickerPosition || this.element.data("picker-position") || "bottom-right", this.showMeridian = n.showMeridian || this.element.data("show-meridian") || !1, this.initialDate = n.initialDate || new Date, this.zIndex = n.zIndex || this.element.data("z-index") || e, this.title = void 0 !== n.title && n.title, this.timezone = n.timezone || function() {
                var t, e, i, n, r;
                if ((i = (null != (r = (e = (new Date).toString()).split("(")[1]) ? r.slice(0, -1) : 0) || e.split(" ")) instanceof Array) {
                    n = [];
                    for (var o = 0, s = i.length; o < s; o++)(t = null !== (r = i[o].match(/\b[A-Z]+\b/))) && r[0] && n.push(t);
                    i = n.pop()
                }
                return i
            }(), this.icons = {
                leftArrow: this.fontAwesome ? "fa-arrow-left" : 3 === this.bootcssVer ? "glyphicon-arrow-left" : "icon-arrow-left",
                rightArrow: this.fontAwesome ? "fa-arrow-right" : 3 === this.bootcssVer ? "glyphicon-arrow-right" : "icon-arrow-right"
            }, this.icontype = this.fontAwesome ? "fa" : "glyphicon", this._attachEvents(), this.clickedOutside = function(e) {
                0 === t(e.target).closest(".datetimepicker").length && r.hide()
            }, this.formatViewType = "datetime", "formatViewType" in n ? this.formatViewType = n.formatViewType : "formatViewType" in this.element.data() && (this.formatViewType = this.element.data("formatViewType")), this.minView = 0, "minView" in n ? this.minView = n.minView : "minView" in this.element.data() && (this.minView = this.element.data("min-view")), this.minView = s.convertViewMode(this.minView), this.maxView = s.modes.length - 1, "maxView" in n ? this.maxView = n.maxView : "maxView" in this.element.data() && (this.maxView = this.element.data("max-view")), this.maxView = s.convertViewMode(this.maxView), this.wheelViewModeNavigation = !1, "wheelViewModeNavigation" in n ? this.wheelViewModeNavigation = n.wheelViewModeNavigation : "wheelViewModeNavigation" in this.element.data() && (this.wheelViewModeNavigation = this.element.data("view-mode-wheel-navigation")), this.wheelViewModeNavigationInverseDirection = !1, "wheelViewModeNavigationInverseDirection" in n ? this.wheelViewModeNavigationInverseDirection = n.wheelViewModeNavigationInverseDirection : "wheelViewModeNavigationInverseDirection" in this.element.data() && (this.wheelViewModeNavigationInverseDirection = this.element.data("view-mode-wheel-navigation-inverse-dir")), this.wheelViewModeNavigationDelay = 100, "wheelViewModeNavigationDelay" in n ? this.wheelViewModeNavigationDelay = n.wheelViewModeNavigationDelay : "wheelViewModeNavigationDelay" in this.element.data() && (this.wheelViewModeNavigationDelay = this.element.data("view-mode-wheel-navigation-delay")), this.startViewMode = 2, "startView" in n ? this.startViewMode = n.startView : "startView" in this.element.data() && (this.startViewMode = this.element.data("start-view")), this.startViewMode = s.convertViewMode(this.startViewMode), this.viewMode = this.startViewMode, this.viewSelect = this.minView, "viewSelect" in n ? this.viewSelect = n.viewSelect : "viewSelect" in this.element.data() && (this.viewSelect = this.element.data("view-select")), this.viewSelect = s.convertViewMode(this.viewSelect), this.forceParse = !0, "forceParse" in n ? this.forceParse = n.forceParse : "dateForceParse" in this.element.data() && (this.forceParse = this.element.data("date-force-parse"));
            for (var a = 3 === this.bootcssVer ? s.templateV3 : s.template; - 1 !== a.indexOf("{iconType}");) a = a.replace("{iconType}", this.icontype);
            for (; - 1 !== a.indexOf("{leftArrow}");) a = a.replace("{leftArrow}", this.icons.leftArrow);
            for (; - 1 !== a.indexOf("{rightArrow}");) a = a.replace("{rightArrow}", this.icons.rightArrow);
            if (this.picker = t(a).appendTo(this.isInline ? this.element : this.container).on({
                click: t.proxy(this.click, this),
                mousedown: t.proxy(this.mousedown, this)
            }), this.wheelViewModeNavigation && (t.fn.mousewheel ? this.picker.on({
                mousewheel: t.proxy(this.mousewheel, this)
            }) : console.log("Mouse Wheel event is not supported. Please include the jQuery Mouse Wheel plugin before enabling this option")), this.isInline ? this.picker.addClass("datetimepicker-inline") : this.picker.addClass("datetimepicker-dropdown-" + this.pickerPosition + " dropdown-menu"), this.isRTL) {
                this.picker.addClass("datetimepicker-rtl");
                var l = 3 === this.bootcssVer ? ".prev span, .next span" : ".prev i, .next i";
                this.picker.find(l).toggleClass(this.icons.leftArrow + " " + this.icons.rightArrow)
            }
            t(document).on("mousedown touchend", this.clickedOutside), this.autoclose = !1, "autoclose" in n ? this.autoclose = n.autoclose : "dateAutoclose" in this.element.data() && (this.autoclose = this.element.data("date-autoclose")), this.keyboardNavigation = !0, "keyboardNavigation" in n ? this.keyboardNavigation = n.keyboardNavigation : "dateKeyboardNavigation" in this.element.data() && (this.keyboardNavigation = this.element.data("date-keyboard-navigation")), this.todayBtn = n.todayBtn || this.element.data("date-today-btn") || !1, this.clearBtn = n.clearBtn || this.element.data("date-clear-btn") || !1, this.todayHighlight = n.todayHighlight || this.element.data("date-today-highlight") || !1, this.weekStart = 0, void 0 !== n.weekStart ? this.weekStart = n.weekStart : void 0 !== this.element.data("date-weekstart") ? this.weekStart = this.element.data("date-weekstart") : void 0 !== o[this.language].weekStart && (this.weekStart = o[this.language].weekStart), this.weekStart = this.weekStart % 7, this.weekEnd = (this.weekStart + 6) % 7, this.onRenderDay = function(t) {
                var e = (n.onRenderDay || function() {
                    return []
                })(t);
                "string" == typeof e && (e = [e]);
                return ["day"].concat(e || [])
            }, this.onRenderHour = function(t) {
                var e = (n.onRenderHour || function() {
                    return []
                })(t);
                return "string" == typeof e && (e = [e]), ["hour"].concat(e || [])
            }, this.onRenderMinute = function(t) {
                var e = (n.onRenderMinute || function() {
                    return []
                })(t),
                    i = ["minute"];
                return "string" == typeof e && (e = [e]), t < this.startDate || t > this.endDate ? i.push("disabled") : Math.floor(this.date.getUTCMinutes() / this.minuteStep) === Math.floor(t.getUTCMinutes() / this.minuteStep) && i.push("active"), i.concat(e || [])
            }, this.onRenderYear = function(t) {
                var e = (n.onRenderYear || function() {
                    return []
                })(t),
                    i = ["year"];
                "string" == typeof e && (e = [e]), this.date.getUTCFullYear() === t.getUTCFullYear() && i.push("active");
                var r = t.getUTCFullYear(),
                    o = this.endDate.getUTCFullYear();
                return (t < this.startDate || r > o) && i.push("disabled"), i.concat(e || [])
            }, this.onRenderMonth = function(t) {
                var e = (n.onRenderMonth || function() {
                    return []
                })(t);
                return "string" == typeof e && (e = [e]), ["month"].concat(e || [])
            }, this.startDate = new Date(-8639968443048e3), this.endDate = new Date(8639968443048e3), this.datesDisabled = [], this.daysOfWeekDisabled = [], this.setStartDate(n.startDate || this.element.data("date-startdate")), this.setEndDate(n.endDate || this.element.data("date-enddate")), this.setDatesDisabled(n.datesDisabled || this.element.data("date-dates-disabled")), this.setDaysOfWeekDisabled(n.daysOfWeekDisabled || this.element.data("date-days-of-week-disabled")), this.setMinutesDisabled(n.minutesDisabled || this.element.data("date-minute-disabled")), this.setHoursDisabled(n.hoursDisabled || this.element.data("date-hour-disabled")), this.fillDow(), this.fillMonths(), this.update(), this.showMode(), this.isInline && this.show()
        };
        n.prototype = {
            constructor: n,
            _events: [],
            _attachEvents: function() {
                this._detachEvents(), this.isInput ? this._events = [
                    [this.element, {
                        focus: t.proxy(this.show, this),
                        keyup: t.proxy(this.update, this),
                        keydown: t.proxy(this.keydown, this)
                    }]
                ] : this.component && this.hasInput ? (this._events = [
                    [this.element.find("input"), {
                        focus: t.proxy(this.show, this),
                        keyup: t.proxy(this.update, this),
                        keydown: t.proxy(this.keydown, this)
                    }],
                    [this.component, {
                        click: t.proxy(this.show, this)
                    }]
                ], this.componentReset && this._events.push([this.componentReset, {
                    click: t.proxy(this.reset, this)
                }])) : this.element.is("div") ? this.isInline = !0 : this._events = [
                    [this.element, {
                        click: t.proxy(this.show, this)
                    }]
                ];
                for (var e, i, n = 0; n < this._events.length; n++) e = this._events[n][0], i = this._events[n][1], e.on(i)
            },
            _detachEvents: function() {
                for (var t, e, i = 0; i < this._events.length; i++) t = this._events[i][0], e = this._events[i][1], t.off(e);
                this._events = []
            },
            show: function(e) {
                this.picker.show(), this.height = this.component ? this.component.outerHeight() : this.element.outerHeight(), this.forceParse && this.update(), this.place(), t(window).on("resize", t.proxy(this.place, this)), e && (e.stopPropagation(), e.preventDefault()), this.isVisible = !0, this.element.trigger({
                    type: "show",
                    date: this.date
                })
            },
            hide: function() {
                this.isVisible && (this.isInline || (this.picker.hide(), t(window).off("resize", this.place), this.viewMode = this.startViewMode, this.showMode(), this.isInput || t(document).off("mousedown", this.hide), this.forceParse && (this.isInput && this.element.val() || this.hasInput && this.element.find("input").val()) && this.setValue(), this.isVisible = !1, this.element.trigger({
                    type: "hide",
                    date: this.date
                })))
            },
            remove: function() {
                this._detachEvents(), t(document).off("mousedown", this.clickedOutside), this.picker.remove(), delete this.picker, delete this.element.data().datetimepicker
            },
            getDate: function() {
                var t = this.getUTCDate();
                return null === t ? null : new Date(t.getTime() + 6e4 * t.getTimezoneOffset())
            },
            getUTCDate: function() {
                return this.date
            },
            getInitialDate: function() {
                return this.initialDate
            },
            setInitialDate: function(t) {
                this.initialDate = t
            },
            setDate: function(t) {
                this.setUTCDate(new Date(t.getTime() - 6e4 * t.getTimezoneOffset()))
            },
            setUTCDate: function(t) {
                t >= this.startDate && t <= this.endDate ? (this.date = t, this.setValue(), this.viewDate = this.date, this.fill()) : this.element.trigger({
                    type: "outOfRange",
                    date: t,
                    startDate: this.startDate,
                    endDate: this.endDate
                })
            },
            setFormat: function(t) {
                var e;
                this.format = s.parseFormat(t, this.formatType), this.isInput ? e = this.element : this.component && (e = this.element.find("input")), e && e.val() && this.setValue()
            },
            setValue: function() {
                var e = this.getFormattedDate();
                this.isInput ? this.element.val(e) : (this.component && this.element.find("input").val(e), this.element.data("date", e)), this.linkField && t("#" + this.linkField).val(this.getFormattedDate(this.linkFormat))
            },
            getFormattedDate: function(t) {
                return t = t || this.format, s.formatDate(this.date, t, this.language, this.formatType, this.timezone)
            },
            setStartDate: function(t) {
                this.startDate = t || this.startDate, 8639968443048e3 !== this.startDate.valueOf() && (this.startDate = s.parseDate(this.startDate, this.format, this.language, this.formatType, this.timezone)), this.update(), this.updateNavArrows()
            },
            setEndDate: function(t) {
                this.endDate = t || this.endDate, 8639968443048e3 !== this.endDate.valueOf() && (this.endDate = s.parseDate(this.endDate, this.format, this.language, this.formatType, this.timezone)), this.update(), this.updateNavArrows()
            },
            setDatesDisabled: function(e) {
                this.datesDisabled = e || [], t.isArray(this.datesDisabled) || (this.datesDisabled = this.datesDisabled.split(/,\s*/));
                var i = this;
                this.datesDisabled = t.map(this.datesDisabled, function(t) {
                    return s.parseDate(t, i.format, i.language, i.formatType, i.timezone).toDateString()
                }), this.update(), this.updateNavArrows()
            },
            setTitle: function(t, e) {
                return this.picker.find(t).find("th:eq(1)").text(!1 === this.title ? e : this.title)
            },
            setDaysOfWeekDisabled: function(e) {
                this.daysOfWeekDisabled = e || [], t.isArray(this.daysOfWeekDisabled) || (this.daysOfWeekDisabled = this.daysOfWeekDisabled.split(/,\s*/)), this.daysOfWeekDisabled = t.map(this.daysOfWeekDisabled, function(t) {
                    return parseInt(t, 10)
                }), this.update(), this.updateNavArrows()
            },
            setMinutesDisabled: function(e) {
                this.minutesDisabled = e || [], t.isArray(this.minutesDisabled) || (this.minutesDisabled = this.minutesDisabled.split(/,\s*/)), this.minutesDisabled = t.map(this.minutesDisabled, function(t) {
                    return parseInt(t, 10)
                }), this.update(), this.updateNavArrows()
            },
            setHoursDisabled: function(e) {
                this.hoursDisabled = e || [], t.isArray(this.hoursDisabled) || (this.hoursDisabled = this.hoursDisabled.split(/,\s*/)), this.hoursDisabled = t.map(this.hoursDisabled, function(t) {
                    return parseInt(t, 10)
                }), this.update(), this.updateNavArrows()
            },
            place: function() {
                if (!this.isInline) {
                    if (!this.zIndex) {
                        var e = 0;
                        t("div").each(function() {
                            var i = parseInt(t(this).css("zIndex"), 10);
                            i > e && (e = i)
                        }), this.zIndex = e + 10
                    }
                    var i, n, r, o;
                    o = this.container instanceof t ? this.container.offset() : t(this.container).offset(), this.component ? (r = (i = this.component.offset()).left, "bottom-left" !== this.pickerPosition && "top-left" !== this.pickerPosition || (r += this.component.outerWidth() - this.picker.outerWidth())) : (r = (i = this.element.offset()).left, "bottom-left" !== this.pickerPosition && "top-left" !== this.pickerPosition || (r += this.element.outerWidth() - this.picker.outerWidth()));
                    var s = document.body.clientWidth || window.innerWidth;
                    r + 220 > s && (r = s - 220), n = "top-left" === this.pickerPosition || "top-right" === this.pickerPosition ? i.top - this.picker.outerHeight() : i.top + this.height, n -= o.top, r -= o.left, this.picker.css({
                        top: n,
                        left: r,
                        zIndex: this.zIndex
                    })
                }
            },
            hour_minute: "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]",
            update: function() {
                var t, e = !1;
                arguments && arguments.length && ("string" == typeof arguments[0] || arguments[0] instanceof Date) ? (t = arguments[0], e = !0) : "string" == typeof(t = (this.isInput ? this.element.val() : this.element.find("input").val()) || this.element.data("date") || this.initialDate) && (t = t.replace(/^\s+|\s+$/g, "")), t || (t = new Date, e = !1), "string" == typeof t && (new RegExp(this.hour_minute).test(t) || new RegExp(this.hour_minute + ":[0-5][0-9]").test(t)) && (t = this.getDate()), this.date = s.parseDate(t, this.format, this.language, this.formatType, this.timezone), e && this.setValue(), this.date < this.startDate ? this.viewDate = new Date(this.startDate) : this.date > this.endDate ? this.viewDate = new Date(this.endDate) : this.viewDate = new Date(this.date), this.fill()
            },
            fillDow: function() {
                for (var t = this.weekStart, e = "<tr>"; t < this.weekStart + 7;) e += '<th class="dow">' + o[this.language].daysMin[t++ % 7] + "</th>";
                e += "</tr>", this.picker.find(".datetimepicker-days thead").append(e)
            },
            fillMonths: function() {
                for (var t = "", e = new Date(this.viewDate), i = 0; i < 12; i++) {
                    e.setUTCMonth(i), t += '<span class="' + this.onRenderMonth(e).join(" ") + '">' + o[this.language].monthsShort[i] + "</span>"
                }
                this.picker.find(".datetimepicker-months td").html(t)
            },
            fill: function() {
                if (this.date && this.viewDate) {
                    var e = new Date(this.viewDate),
                        n = e.getUTCFullYear(),
                        a = e.getUTCMonth(),
                        l = e.getUTCDate(),
                        c = e.getUTCHours(),
                        h = this.startDate.getUTCFullYear(),
                        d = this.startDate.getUTCMonth(),
                        u = this.endDate.getUTCFullYear(),
                        p = this.endDate.getUTCMonth() + 1,
                        f = new i(this.date.getUTCFullYear(), this.date.getUTCMonth(), this.date.getUTCDate()).valueOf(),
                        m = new Date;
                    if (this.setTitle(".datetimepicker-days", o[this.language].months[a] + " " + n), "time" === this.formatViewType) {
                        var g = this.getFormattedDate();
                        this.setTitle(".datetimepicker-hours", g), this.setTitle(".datetimepicker-minutes", g)
                    } else this.setTitle(".datetimepicker-hours", l + " " + o[this.language].months[a] + " " + n), this.setTitle(".datetimepicker-minutes", l + " " + o[this.language].months[a] + " " + n);
                    this.picker.find("tfoot th.today").text(o[this.language].today || o.en.today).toggle(!1 !== this.todayBtn), this.picker.find("tfoot th.clear").text(o[this.language].clear || o.en.clear).toggle(!1 !== this.clearBtn), this.updateNavArrows(), this.fillMonths();
                    var v = i(n, a - 1, 28, 0, 0, 0, 0),
                        y = s.getDaysInMonth(v.getUTCFullYear(), v.getUTCMonth());
                    v.setUTCDate(y), v.setUTCDate(y - (v.getUTCDay() - this.weekStart + 7) % 7);
                    var b = new Date(v);
                    b.setUTCDate(b.getUTCDate() + 42), b = b.valueOf();
                    for (var _, x = []; v.valueOf() < b;) v.getUTCDay() === this.weekStart && x.push("<tr>"), _ = this.onRenderDay(v), v.getUTCFullYear() < n || v.getUTCFullYear() === n && v.getUTCMonth() < a ? _.push("old") : (v.getUTCFullYear() > n || v.getUTCFullYear() === n && v.getUTCMonth() > a) && _.push("new"), this.todayHighlight && v.getUTCFullYear() === m.getFullYear() && v.getUTCMonth() === m.getMonth() && v.getUTCDate() === m.getDate() && _.push("today"), v.valueOf() === f && _.push("active"), (v.valueOf() + 864e5 <= this.startDate || v.valueOf() > this.endDate || -1 !== t.inArray(v.getUTCDay(), this.daysOfWeekDisabled) || -1 !== t.inArray(v.toDateString(), this.datesDisabled)) && _.push("disabled"), x.push('<td class="' + _.join(" ") + '">' + v.getUTCDate() + "</td>"), v.getUTCDay() === this.weekEnd && x.push("</tr>"), v.setUTCDate(v.getUTCDate() + 1);
                    this.picker.find(".datetimepicker-days tbody").empty().append(x.join("")), x = [];
                    var w = "",
                        k = "",
                        C = "",
                        S = this.hoursDisabled || [];
                    e = new Date(this.viewDate);
                    for (var D = 0; D < 24; D++) {
                        e.setUTCHours(D), _ = this.onRenderHour(e), -1 !== S.indexOf(D) && _.push("disabled");
                        var T = i(n, a, l, D);
                        T.valueOf() + 36e5 <= this.startDate || T.valueOf() > this.endDate ? _.push("disabled") : c === D && _.push("active"), this.showMeridian && 2 === o[this.language].meridiem.length ? ((k = D < 12 ? o[this.language].meridiem[0] : o[this.language].meridiem[1]) !== C && ("" !== C && x.push("</fieldset>"), x.push('<fieldset class="hour"><legend>' + k.toUpperCase() + "</legend>")), C = k, w = D % 12 ? D % 12 : 12, D < 12 ? _.push("hour_am") : _.push("hour_pm"), x.push('<span class="' + _.join(" ") + '">' + w + "</span>"), 23 === D && x.push("</fieldset>")) : (w = D + ":00", x.push('<span class="' + _.join(" ") + '">' + w + "</span>"))
                    }
                    this.picker.find(".datetimepicker-hours td").html(x.join("")), x = [], w = "", k = "", C = "";
                    var A = this.minutesDisabled || [];
                    e = new Date(this.viewDate);
                    for (D = 0; D < 60; D += this.minuteStep) - 1 === A.indexOf(D) && (e.setUTCMinutes(D), e.setUTCSeconds(0), _ = this.onRenderMinute(e), this.showMeridian && 2 === o[this.language].meridiem.length ? ((k = c < 12 ? o[this.language].meridiem[0] : o[this.language].meridiem[1]) !== C && ("" !== C && x.push("</fieldset>"), x.push('<fieldset class="minute"><legend>' + k.toUpperCase() + "</legend>")), C = k, w = c % 12 ? c % 12 : 12, x.push('<span class="' + _.join(" ") + '">' + w + ":" + (D < 10 ? "0" + D : D) + "</span>"), 59 === D && x.push("</fieldset>")) : (w = D + ":00", x.push('<span class="' + _.join(" ") + '">' + c + ":" + (D < 10 ? "0" + D : D) + "</span>")));
                    this.picker.find(".datetimepicker-minutes td").html(x.join(""));
                    var M = this.date.getUTCFullYear(),
                        E = this.setTitle(".datetimepicker-months", n).end().find(".month").removeClass("active");
                    M === n && E.eq(this.date.getUTCMonth()).addClass("active"), (n < h || n > u) && E.addClass("disabled"), n === h && E.slice(0, d).addClass("disabled"), n === u && E.slice(p).addClass("disabled"), x = "", n = 10 * parseInt(n / 10, 10);
                    var P = this.setTitle(".datetimepicker-years", n + "-" + (n + 9)).end().find("td");
                    n -= 1, e = new Date(this.viewDate);
                    for (D = -1; D < 11; D++) e.setUTCFullYear(n), _ = this.onRenderYear(e), -1 !== D && 10 !== D || _.push(r), x += '<span class="' + _.join(" ") + '">' + n + "</span>", n += 1;
                    P.html(x), this.place()
                }
            },
            updateNavArrows: function() {
                var t = new Date(this.viewDate),
                    e = t.getUTCFullYear(),
                    i = t.getUTCMonth(),
                    n = t.getUTCDate(),
                    r = t.getUTCHours();
                switch (this.viewMode) {
                    case 0:
                        e <= this.startDate.getUTCFullYear() && i <= this.startDate.getUTCMonth() && n <= this.startDate.getUTCDate() && r <= this.startDate.getUTCHours() ? this.picker.find(".prev").css({
                            visibility: "hidden"
                        }) : this.picker.find(".prev").css({
                            visibility: "visible"
                        }), e >= this.endDate.getUTCFullYear() && i >= this.endDate.getUTCMonth() && n >= this.endDate.getUTCDate() && r >= this.endDate.getUTCHours() ? this.picker.find(".next").css({
                            visibility: "hidden"
                        }) : this.picker.find(".next").css({
                            visibility: "visible"
                        });
                        break;
                    case 1:
                        e <= this.startDate.getUTCFullYear() && i <= this.startDate.getUTCMonth() && n <= this.startDate.getUTCDate() ? this.picker.find(".prev").css({
                            visibility: "hidden"
                        }) : this.picker.find(".prev").css({
                            visibility: "visible"
                        }), e >= this.endDate.getUTCFullYear() && i >= this.endDate.getUTCMonth() && n >= this.endDate.getUTCDate() ? this.picker.find(".next").css({
                            visibility: "hidden"
                        }) : this.picker.find(".next").css({
                            visibility: "visible"
                        });
                        break;
                    case 2:
                        e <= this.startDate.getUTCFullYear() && i <= this.startDate.getUTCMonth() ? this.picker.find(".prev").css({
                            visibility: "hidden"
                        }) : this.picker.find(".prev").css({
                            visibility: "visible"
                        }), e >= this.endDate.getUTCFullYear() && i >= this.endDate.getUTCMonth() ? this.picker.find(".next").css({
                            visibility: "hidden"
                        }) : this.picker.find(".next").css({
                            visibility: "visible"
                        });
                        break;
                    case 3:
                    case 4:
                        e <= this.startDate.getUTCFullYear() ? this.picker.find(".prev").css({
                            visibility: "hidden"
                        }) : this.picker.find(".prev").css({
                            visibility: "visible"
                        }), e >= this.endDate.getUTCFullYear() ? this.picker.find(".next").css({
                            visibility: "hidden"
                        }) : this.picker.find(".next").css({
                            visibility: "visible"
                        })
                }
            },
            mousewheel: function(e) {
                if (e.preventDefault(), e.stopPropagation(), !this.wheelPause) {
                    this.wheelPause = !0;
                    var i = e.originalEvent.wheelDelta,
                        n = i > 0 ? 1 : 0 === i ? 0 : -1;
                    this.wheelViewModeNavigationInverseDirection && (n = -n), this.showMode(n), setTimeout(t.proxy(function() {
                        this.wheelPause = !1
                    }, this), this.wheelViewModeNavigationDelay)
                }
            },
            click: function(e) {
                e.stopPropagation(), e.preventDefault();
                var n = t(e.target).closest("span, td, th, legend");
                if (n.is("." + this.icontype) && (n = t(n).parent().closest("span, td, th, legend")), 1 === n.length) {
                    if (n.is(".disabled")) return void this.element.trigger({
                        type: "outOfRange",
                        date: this.viewDate,
                        startDate: this.startDate,
                        endDate: this.endDate
                    });
                    switch (n[0].nodeName.toLowerCase()) {
                        case "th":
                            switch (n[0].className) {
                                case "switch":
                                    this.showMode(1);
                                    break;
                                case "prev":
                                case "next":
                                    var r = s.modes[this.viewMode].navStep * ("prev" === n[0].className ? -1 : 1);
                                    switch (this.viewMode) {
                                        case 0:
                                            this.viewDate = this.moveHour(this.viewDate, r);
                                            break;
                                        case 1:
                                            this.viewDate = this.moveDate(this.viewDate, r);
                                            break;
                                        case 2:
                                            this.viewDate = this.moveMonth(this.viewDate, r);
                                            break;
                                        case 3:
                                        case 4:
                                            this.viewDate = this.moveYear(this.viewDate, r)
                                    }
                                    this.fill(), this.element.trigger({
                                        type: n[0].className + ":" + this.convertViewModeText(this.viewMode),
                                        date: this.viewDate,
                                        startDate: this.startDate,
                                        endDate: this.endDate
                                    });
                                    break;
                                case "clear":
                                    this.reset(), this.autoclose && this.hide();
                                    break;
                                case "today":
                                    var o = new Date;
                                    (o = i(o.getFullYear(), o.getMonth(), o.getDate(), o.getHours(), o.getMinutes(), o.getSeconds(), 0)) < this.startDate ? o = this.startDate : o > this.endDate && (o = this.endDate), this.viewMode = this.startViewMode, this.showMode(0), this._setDate(o), this.fill(), this.autoclose && this.hide()
                            }
                            break;
                        case "span":
                            if (!n.is(".disabled")) {
                                var a = this.viewDate.getUTCFullYear(),
                                    l = this.viewDate.getUTCMonth(),
                                    c = this.viewDate.getUTCDate(),
                                    h = this.viewDate.getUTCHours(),
                                    d = this.viewDate.getUTCMinutes(),
                                    u = this.viewDate.getUTCSeconds();
                                if (n.is(".month") ? (this.viewDate.setUTCDate(1), l = n.parent().find("span").index(n), c = this.viewDate.getUTCDate(), this.viewDate.setUTCMonth(l), this.element.trigger({
                                    type: "changeMonth",
                                    date: this.viewDate
                                }), this.viewSelect >= 3 && this._setDate(i(a, l, c, h, d, u, 0))) : n.is(".year") ? (this.viewDate.setUTCDate(1), a = parseInt(n.text(), 10) || 0, this.viewDate.setUTCFullYear(a), this.element.trigger({
                                    type: "changeYear",
                                    date: this.viewDate
                                }), this.viewSelect >= 4 && this._setDate(i(a, l, c, h, d, u, 0))) : n.is(".hour") ? (h = parseInt(n.text(), 10) || 0, (n.hasClass("hour_am") || n.hasClass("hour_pm")) && (12 === h && n.hasClass("hour_am") ? h = 0 : 12 !== h && n.hasClass("hour_pm") && (h += 12)), this.viewDate.setUTCHours(h), this.element.trigger({
                                    type: "changeHour",
                                    date: this.viewDate
                                }), this.viewSelect >= 1 && this._setDate(i(a, l, c, h, d, u, 0))) : n.is(".minute") && (d = parseInt(n.text().substr(n.text().indexOf(":") + 1), 10) || 0, this.viewDate.setUTCMinutes(d), this.element.trigger({
                                    type: "changeMinute",
                                    date: this.viewDate
                                }), this.viewSelect >= 0 && this._setDate(i(a, l, c, h, d, u, 0))), 0 !== this.viewMode) {
                                    var p = this.viewMode;
                                    this.showMode(-1), this.fill(), p === this.viewMode && this.autoclose && this.hide()
                                } else this.fill(), this.autoclose && this.hide()
                            }
                            break;
                        case "td":
                            if (n.is(".day") && !n.is(".disabled")) {
                                c = parseInt(n.text(), 10) || 1, a = this.viewDate.getUTCFullYear(), l = this.viewDate.getUTCMonth(), h = this.viewDate.getUTCHours(), d = this.viewDate.getUTCMinutes(), u = this.viewDate.getUTCSeconds();
                                n.is(".old") ? 0 === l ? (l = 11, a -= 1) : l -= 1 : n.is(".new") && (11 === l ? (l = 0, a += 1) : l += 1), this.viewDate.setUTCFullYear(a), this.viewDate.setUTCMonth(l, c), this.element.trigger({
                                    type: "changeDay",
                                    date: this.viewDate
                                }), this.viewSelect >= 2 && this._setDate(i(a, l, c, h, d, u, 0))
                            }
                            p = this.viewMode;
                            this.showMode(-1), this.fill(), p === this.viewMode && this.autoclose && this.hide()
                    }
                }
            },
            _setDate: function(t, e) {
                var i;
                e && "date" !== e || (this.date = t), e && "view" !== e || (this.viewDate = t), this.fill(), this.setValue(), this.isInput ? i = this.element : this.component && (i = this.element.find("input")), i && i.change(), this.element.trigger({
                    type: "changeDate",
                    date: this.getDate()
                }), null === t && (this.date = this.viewDate)
            },
            moveMinute: function(t, e) {
                if (!e) return t;
                var i = new Date(t.valueOf());
                return i.setUTCMinutes(i.getUTCMinutes() + e * this.minuteStep), i
            },
            moveHour: function(t, e) {
                if (!e) return t;
                var i = new Date(t.valueOf());
                return i.setUTCHours(i.getUTCHours() + e), i
            },
            moveDate: function(t, e) {
                if (!e) return t;
                var i = new Date(t.valueOf());
                return i.setUTCDate(i.getUTCDate() + e), i
            },
            moveMonth: function(t, e) {
                if (!e) return t;
                var i, n, r = new Date(t.valueOf()),
                    o = r.getUTCDate(),
                    s = r.getUTCMonth(),
                    a = Math.abs(e);
                if (e = e > 0 ? 1 : -1, 1 === a) n = -1 === e ? function() {
                    return r.getUTCMonth() === s
                } : function() {
                    return r.getUTCMonth() !== i
                }, i = s + e, r.setUTCMonth(i), (i < 0 || i > 11) && (i = (i + 12) % 12);
                else {
                    for (var l = 0; l < a; l++) r = this.moveMonth(r, e);
                    i = r.getUTCMonth(), r.setUTCDate(o), n = function() {
                        return i !== r.getUTCMonth()
                    }
                }
                for (; n();) r.setUTCDate(--o), r.setUTCMonth(i);
                return r
            },
            moveYear: function(t, e) {
                return this.moveMonth(t, 12 * e)
            },
            dateWithinRange: function(t) {
                return t >= this.startDate && t <= this.endDate
            },
            keydown: function(t) {
                if (this.picker.is(":not(:visible)")) 27 === t.keyCode && this.show();
                else {
                    var e, i, n, r, o = !1;
                    switch (t.keyCode) {
                        case 27:
                            this.hide(), t.preventDefault();
                            break;
                        case 37:
                        case 39:
                            if (!this.keyboardNavigation) break;
                            e = 37 === t.keyCode ? -1 : 1;
                            var s = this.viewMode;
                            t.ctrlKey ? s += 2 : t.shiftKey && (s += 1), 4 === s ? (i = this.moveYear(this.date, e), n = this.moveYear(this.viewDate, e)) : 3 === s ? (i = this.moveMonth(this.date, e), n = this.moveMonth(this.viewDate, e)) : 2 === s ? (i = this.moveDate(this.date, e), n = this.moveDate(this.viewDate, e)) : 1 === s ? (i = this.moveHour(this.date, e), n = this.moveHour(this.viewDate, e)) : 0 === s && (i = this.moveMinute(this.date, e), n = this.moveMinute(this.viewDate, e)), this.dateWithinRange(i) && (this.date = i, this.viewDate = n, this.setValue(), this.update(), t.preventDefault(), o = !0);
                            break;
                        case 38:
                        case 40:
                            if (!this.keyboardNavigation) break;
                            e = 38 === t.keyCode ? -1 : 1, s = this.viewMode, t.ctrlKey ? s += 2 : t.shiftKey && (s += 1), 4 === s ? (i = this.moveYear(this.date, e), n = this.moveYear(this.viewDate, e)) : 3 === s ? (i = this.moveMonth(this.date, e), n = this.moveMonth(this.viewDate, e)) : 2 === s ? (i = this.moveDate(this.date, 7 * e), n = this.moveDate(this.viewDate, 7 * e)) : 1 === s ? this.showMeridian ? (i = this.moveHour(this.date, 6 * e), n = this.moveHour(this.viewDate, 6 * e)) : (i = this.moveHour(this.date, 4 * e), n = this.moveHour(this.viewDate, 4 * e)) : 0 === s && (i = this.moveMinute(this.date, 4 * e), n = this.moveMinute(this.viewDate, 4 * e)), this.dateWithinRange(i) && (this.date = i, this.viewDate = n, this.setValue(), this.update(), t.preventDefault(), o = !0);
                            break;
                        case 13:
                            if (0 !== this.viewMode) {
                                var a = this.viewMode;
                                this.showMode(-1), this.fill(), a === this.viewMode && this.autoclose && this.hide()
                            } else this.fill(), this.autoclose && this.hide();
                            t.preventDefault();
                            break;
                        case 9:
                            this.hide()
                    }
                    if (o) this.isInput ? r = this.element : this.component && (r = this.element.find("input")), r && r.change(), this.element.trigger({
                        type: "changeDate",
                        date: this.getDate()
                    })
                }
            },
            showMode: function(t) {
                if (t) {
                    var e = Math.max(0, Math.min(s.modes.length - 1, this.viewMode + t));
                    e >= this.minView && e <= this.maxView && (this.element.trigger({
                        type: "changeMode",
                        date: this.viewDate,
                        oldViewMode: this.viewMode,
                        newViewMode: e
                    }), this.viewMode = e)
                }
                this.picker.find(">div").hide().filter(".datetimepicker-" + s.modes[this.viewMode].clsName).css("display", "block"), this.updateNavArrows()
            },
            reset: function() {
                this._setDate(null, "date")
            },
            convertViewModeText: function(t) {
                switch (t) {
                    case 4:
                        return "decade";
                    case 3:
                        return "year";
                    case 2:
                        return "month";
                    case 1:
                        return "day";
                    case 0:
                        return "hour"
                }
            }
        };
        var r = t.fn.datetimepicker;
        t.fn.datetimepicker = function(i) {
            var r, o = Array.apply(null, arguments);
            return o.shift(), this.each(function() {
                var s = t(this),
                    a = s.data("datetimepicker"),
                    l = "object" == typeof i && i;
                if (a || s.data("datetimepicker", a = new n(this, t.extend({}, t.fn.datetimepicker.defaults, l))), "string" == typeof i && "function" == typeof a[i] && (r = a[i].apply(a, o)) !== e) return !1
            }), r !== e ? r : this
        }, t.fn.datetimepicker.defaults = {}, t.fn.datetimepicker.Constructor = n;
        var o = t.fn.datetimepicker.dates = {
            en: {
                days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                meridiem: ["am", "pm"],
                suffix: ["st", "nd", "rd", "th"],
                today: "Today",
                clear: "Clear"
            }
        },
            s = {
                modes: [{
                    clsName: "minutes",
                    navFnc: "Hours",
                    navStep: 1
                }, {
                    clsName: "hours",
                    navFnc: "Date",
                    navStep: 1
                }, {
                    clsName: "days",
                    navFnc: "Month",
                    navStep: 1
                }, {
                    clsName: "months",
                    navFnc: "FullYear",
                    navStep: 1
                }, {
                    clsName: "years",
                    navFnc: "FullYear",
                    navStep: 10
                }],
                isLeapYear: function(t) {
                    return t % 4 == 0 && t % 100 != 0 || t % 400 == 0
                },
                getDaysInMonth: function(t, e) {
                    return [31, s.isLeapYear(t) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][e]
                },
                getDefaultFormat: function(t, e) {
                    if ("standard" === t) return "input" === e ? "yyyy-mm-dd hh:ii" : "yyyy-mm-dd hh:ii:ss";
                    if ("php" === t) return "input" === e ? "Y-m-d H:i" : "Y-m-d H:i:s";
                    throw new Error("Invalid format type.")
                },
                validParts: function(t) {
                    if ("standard" === t) return /t|hh?|HH?|p|P|z|Z|ii?|ss?|dd?|DD?|mm?|MM?|yy(?:yy)?/g;
                    if ("php" === t) return /[dDjlNwzFmMnStyYaABgGhHis]/g;
                    throw new Error("Invalid format type.")
                },
                nonpunctuation: /[^ -\/:-@\[-`{-~\t\n\rTZ]+/g,
                parseFormat: function(t, e) {
                    var i = t.replace(this.validParts(e), "\0").split("\0"),
                        n = t.match(this.validParts(e));
                    if (!i || !i.length || !n || 0 === n.length) throw new Error("Invalid date format.");
                    return {
                        separators: i,
                        parts: n
                    }
                },
                parseDate: function(e, r, s, a, l) {
                    if (e instanceof Date) {
                        var c = new Date(e.valueOf() - 6e4 * e.getTimezoneOffset());
                        return c.setMilliseconds(0), c
                    }
                    if (/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(e) && (r = this.parseFormat("yyyy-mm-dd", a)), /^\d{4}\-\d{1,2}\-\d{1,2}[T ]\d{1,2}\:\d{1,2}$/.test(e) && (r = this.parseFormat("yyyy-mm-dd hh:ii", a)), /^\d{4}\-\d{1,2}\-\d{1,2}[T ]\d{1,2}\:\d{1,2}\:\d{1,2}[Z]{0,1}$/.test(e) && (r = this.parseFormat("yyyy-mm-dd hh:ii:ss", a)), /^[-+]\d+[dmwy]([\s,]+[-+]\d+[dmwy])*$/.test(e)) {
                        var h, d = /([-+]\d+)([dmwy])/,
                            u = e.match(/([-+]\d+)([dmwy])/g);
                        e = new Date;
                        for (var p = 0; p < u.length; p++) switch (g = d.exec(u[p]), h = parseInt(g[1]), g[2]) {
                            case "d":
                                e.setUTCDate(e.getUTCDate() + h);
                                break;
                            case "m":
                                e = n.prototype.moveMonth.call(n.prototype, e, h);
                                break;
                            case "w":
                                e.setUTCDate(e.getUTCDate() + 7 * h);
                                break;
                            case "y":
                                e = n.prototype.moveYear.call(n.prototype, e, h)
                        }
                        return i(e.getUTCFullYear(), e.getUTCMonth(), e.getUTCDate(), e.getUTCHours(), e.getUTCMinutes(), e.getUTCSeconds(), 0)
                    }
                    u = e && e.toString().match(this.nonpunctuation) || [], e = new Date(0, 0, 0, 0, 0, 0, 0);
                    var f, m, g, v = {},
                        y = ["hh", "h", "ii", "i", "ss", "s", "yyyy", "yy", "M", "MM", "m", "mm", "D", "DD", "d", "dd", "H", "HH", "p", "P", "z", "Z"],
                        b = {
                            hh: function(t, e) {
                                return t.setUTCHours(e)
                            },
                            h: function(t, e) {
                                return t.setUTCHours(e)
                            },
                            HH: function(t, e) {
                                return t.setUTCHours(12 === e ? 0 : e)
                            },
                            H: function(t, e) {
                                return t.setUTCHours(12 === e ? 0 : e)
                            },
                            ii: function(t, e) {
                                return t.setUTCMinutes(e)
                            },
                            i: function(t, e) {
                                return t.setUTCMinutes(e)
                            },
                            ss: function(t, e) {
                                return t.setUTCSeconds(e)
                            },
                            s: function(t, e) {
                                return t.setUTCSeconds(e)
                            },
                            yyyy: function(t, e) {
                                return t.setUTCFullYear(e)
                            },
                            yy: function(t, e) {
                                return t.setUTCFullYear(2e3 + e)
                            },
                            m: function(t, e) {
                                for (e -= 1; e < 0;) e += 12;
                                for (e %= 12, t.setUTCMonth(e); t.getUTCMonth() !== e;) {
                                    if (isNaN(t.getUTCMonth())) return t;
                                    t.setUTCDate(t.getUTCDate() - 1)
                                }
                                return t
                            },
                            d: function(t, e) {
                                return t.setUTCDate(e)
                            },
                            p: function(t, e) {
                                return t.setUTCHours(1 === e ? t.getUTCHours() + 12 : t.getUTCHours())
                            },
                            z: function() {
                                return l
                            }
                        };
                    if (b.M = b.MM = b.mm = b.m, b.dd = b.d, b.P = b.p, b.Z = b.z, e = i(e.getFullYear(), e.getMonth(), e.getDate(), e.getHours(), e.getMinutes(), e.getSeconds()), u.length === r.parts.length) {
                        p = 0;
                        for (var _ = r.parts.length; p < _; p++) {
                            if (f = parseInt(u[p], 10), g = r.parts[p], isNaN(f)) switch (g) {
                                case "MM":
                                    m = t(o[s].months).filter(function() {
                                        var t = this.slice(0, u[p].length);
                                        return t === u[p].slice(0, t.length)
                                    }), f = t.inArray(m[0], o[s].months) + 1;
                                    break;
                                case "M":
                                    m = t(o[s].monthsShort).filter(function() {
                                        var t = this.slice(0, u[p].length),
                                            e = u[p].slice(0, t.length);
                                        return t.toLowerCase() === e.toLowerCase()
                                    }), f = t.inArray(m[0], o[s].monthsShort) + 1;
                                    break;
                                case "p":
                                case "P":
                                    f = t.inArray(u[p].toLowerCase(), o[s].meridiem)
                            }
                            v[g] = f
                        }
                        var x;
                        for (p = 0; p < y.length; p++)(x = y[p]) in v && !isNaN(v[x]) && b[x](e, v[x])
                    }
                    return e
                },
                formatDate: function(e, i, n, r, a) {
                    if (null === e) return "";
                    var l;
                    if ("standard" === r) l = {
                        t: e.getTime(),
                        yy: e.getUTCFullYear().toString().substring(2),
                        yyyy: e.getUTCFullYear(),
                        m: e.getUTCMonth() + 1,
                        M: o[n].monthsShort[e.getUTCMonth()],
                        MM: o[n].months[e.getUTCMonth()],
                        d: e.getUTCDate(),
                        D: o[n].daysShort[e.getUTCDay()],
                        DD: o[n].days[e.getUTCDay()],
                        p: 2 === o[n].meridiem.length ? o[n].meridiem[e.getUTCHours() < 12 ? 0 : 1] : "",
                        h: e.getUTCHours(),
                        i: e.getUTCMinutes(),
                        s: e.getUTCSeconds(),
                        z: a
                    }, 2 === o[n].meridiem.length ? l.H = l.h % 12 == 0 ? 12 : l.h % 12 : l.H = l.h, l.HH = (l.H < 10 ? "0" : "") + l.H, l.P = l.p.toUpperCase(), l.Z = l.z, l.hh = (l.h < 10 ? "0" : "") + l.h, l.ii = (l.i < 10 ? "0" : "") + l.i, l.ss = (l.s < 10 ? "0" : "") + l.s, l.dd = (l.d < 10 ? "0" : "") + l.d, l.mm = (l.m < 10 ? "0" : "") + l.m;
                    else {
                        if ("php" !== r) throw new Error("Invalid format type.");
                        (l = {
                            y: e.getUTCFullYear().toString().substring(2),
                            Y: e.getUTCFullYear(),
                            F: o[n].months[e.getUTCMonth()],
                            M: o[n].monthsShort[e.getUTCMonth()],
                            n: e.getUTCMonth() + 1,
                            t: s.getDaysInMonth(e.getUTCFullYear(), e.getUTCMonth()),
                            j: e.getUTCDate(),
                            l: o[n].days[e.getUTCDay()],
                            D: o[n].daysShort[e.getUTCDay()],
                            w: e.getUTCDay(),
                            N: 0 === e.getUTCDay() ? 7 : e.getUTCDay(),
                            S: e.getUTCDate() % 10 <= o[n].suffix.length ? o[n].suffix[e.getUTCDate() % 10 - 1] : "",
                            a: 2 === o[n].meridiem.length ? o[n].meridiem[e.getUTCHours() < 12 ? 0 : 1] : "",
                            g: e.getUTCHours() % 12 == 0 ? 12 : e.getUTCHours() % 12,
                            G: e.getUTCHours(),
                            i: e.getUTCMinutes(),
                            s: e.getUTCSeconds()
                        }).m = (l.n < 10 ? "0" : "") + l.n, l.d = (l.j < 10 ? "0" : "") + l.j, l.A = l.a.toString().toUpperCase(), l.h = (l.g < 10 ? "0" : "") + l.g, l.H = (l.G < 10 ? "0" : "") + l.G, l.i = (l.i < 10 ? "0" : "") + l.i, l.s = (l.s < 10 ? "0" : "") + l.s
                    }
                    e = [];
                    for (var c = t.extend([], i.separators), h = 0, d = i.parts.length; h < d; h++) c.length && e.push(c.shift()), e.push(l[i.parts[h]]);
                    return c.length && e.push(c.shift()), e.join("")
                },
                convertViewMode: function(t) {
                    switch (t) {
                        case 4:
                        case "decade":
                            t = 4;
                            break;
                        case 3:
                        case "year":
                            t = 3;
                            break;
                        case 2:
                        case "month":
                            t = 2;
                            break;
                        case 1:
                        case "day":
                            t = 1;
                            break;
                        case 0:
                        case "hour":
                            t = 0
                    }
                    return t
                },
                headTemplate: '<thead><tr><th class="prev"><i class="{iconType} {leftArrow}"/></th><th colspan="5" class="switch"></th><th class="next"><i class="{iconType} {rightArrow}"/></th></tr></thead>',
                headTemplateV3: '<thead><tr><th class="prev"><span class="{iconType} {leftArrow}"></span> </th><th colspan="5" class="switch"></th><th class="next"><span class="{iconType} {rightArrow}"></span> </th></tr></thead>',
                contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
                footTemplate: '<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'
            };
        s.template = '<div class="datetimepicker"><div class="datetimepicker-minutes"><table class=" table-condensed">' + s.headTemplate + s.contTemplate + s.footTemplate + '</table></div><div class="datetimepicker-hours"><table class=" table-condensed">' + s.headTemplate + s.contTemplate + s.footTemplate + '</table></div><div class="datetimepicker-days"><table class=" table-condensed">' + s.headTemplate + "<tbody></tbody>" + s.footTemplate + '</table></div><div class="datetimepicker-months"><table class="table-condensed">' + s.headTemplate + s.contTemplate + s.footTemplate + '</table></div><div class="datetimepicker-years"><table class="table-condensed">' + s.headTemplate + s.contTemplate + s.footTemplate + "</table></div></div>", s.templateV3 = '<div class="datetimepicker"><div class="datetimepicker-minutes"><table class=" table-condensed">' + s.headTemplateV3 + s.contTemplate + s.footTemplate + '</table></div><div class="datetimepicker-hours"><table class=" table-condensed">' + s.headTemplateV3 + s.contTemplate + s.footTemplate + '</table></div><div class="datetimepicker-days"><table class=" table-condensed">' + s.headTemplateV3 + "<tbody></tbody>" + s.footTemplate + '</table></div><div class="datetimepicker-months"><table class="table-condensed">' + s.headTemplateV3 + s.contTemplate + s.footTemplate + '</table></div><div class="datetimepicker-years"><table class="table-condensed">' + s.headTemplateV3 + s.contTemplate + s.footTemplate + "</table></div></div>", t.fn.datetimepicker.DPGlobal = s, t.fn.datetimepicker.noConflict = function() {
            return t.fn.datetimepicker = r, this
        }, t(document).on("focus.datetimepicker.data-api click.datetimepicker.data-api", '[data-provide="datetimepicker"]', function(e) {
            var i = t(this);
            i.data("datetimepicker") || (e.preventDefault(), i.datetimepicker("show"))
        }), t(function() {
            t('[data-provide="datetimepicker-inline"]').datetimepicker()
        })
    }),
    //function(t, e, i) {
    //    "use strict";
    //    var n = function(e, i) {
    //        this.widget = "", this.$element = t(e), this.defaultTime = i.defaultTime, this.disableFocus = i.disableFocus, this.disableMousewheel = i.disableMousewheel, this.isOpen = i.isOpen, this.minuteStep = i.minuteStep, this.modalBackdrop = i.modalBackdrop, this.orientation = i.orientation, this.secondStep = i.secondStep, this.snapToStep = i.snapToStep, this.showInputs = i.showInputs, this.showMeridian = i.showMeridian, this.showSeconds = i.showSeconds, this.template = i.template, this.appendWidgetTo = i.appendWidgetTo, this.showWidgetOnAddonClick = i.showWidgetOnAddonClick, this.icons = i.icons, this.maxHours = i.maxHours, this.explicitMode = i.explicitMode, this.handleDocumentClick = function(t) {
    //            var e = t.data.scope;
    //            e.$element.parent().find(t.target).length || e.$widget.is(t.target) || e.$widget.find(t.target).length || e.hideWidget()
    //        }, this._init()
    //    };
    //    n.prototype = {
    //        constructor: n,
    //        _init: function() {
    //            var e = this;
    //            this.showWidgetOnAddonClick && this.$element.parent().hasClass("input-group") && this.$element.parent().hasClass("bootstrap-timepicker") ? (this.$element.parent(".input-group.bootstrap-timepicker").find(".input-group-addon").on({
    //                "click.timepicker": t.proxy(this.showWidget, this)
    //            }), this.$element.on({
    //                "focus.timepicker": t.proxy(this.highlightUnit, this),
    //                "click.timepicker": t.proxy(this.highlightUnit, this),
    //                "keydown.timepicker": t.proxy(this.elementKeydown, this),
    //                "blur.timepicker": t.proxy(this.blurElement, this),
    //                "mousewheel.timepicker DOMMouseScroll.timepicker": t.proxy(this.mousewheel, this)
    //            })) : this.template ? this.$element.on({
    //                "focus.timepicker": t.proxy(this.showWidget, this),
    //                "click.timepicker": t.proxy(this.showWidget, this),
    //                "blur.timepicker": t.proxy(this.blurElement, this),
    //                "mousewheel.timepicker DOMMouseScroll.timepicker": t.proxy(this.mousewheel, this)
    //            }) : this.$element.on({
    //                "focus.timepicker": t.proxy(this.highlightUnit, this),
    //                "click.timepicker": t.proxy(this.highlightUnit, this),
    //                "keydown.timepicker": t.proxy(this.elementKeydown, this),
    //                "blur.timepicker": t.proxy(this.blurElement, this),
    //                "mousewheel.timepicker DOMMouseScroll.timepicker": t.proxy(this.mousewheel, this)
    //            }), !1 !== this.template ? this.$widget = t(this.getTemplate()).on("click", t.proxy(this.widgetClick, this)) : this.$widget = !1, this.showInputs && !1 !== this.$widget && this.$widget.find("input").each(function() {
    //                t(this).on({
    //                    "click.timepicker": function() {
    //                        t(this).select()
    //                    },
    //                    "keydown.timepicker": t.proxy(e.widgetKeydown, e),
    //                    "keyup.timepicker": t.proxy(e.widgetKeyup, e)
    //                })
    //            }), this.setDefaultTime(this.defaultTime)
    //        },
    //        blurElement: function() {
    //            this.highlightedUnit = null, this.updateFromElementVal()
    //        },
    //        clear: function() {
    //            this.hour = "", this.minute = "", this.second = "", this.meridian = "", this.$element.val("")
    //        },
    //        decrementHour: function() {
    //            if (this.showMeridian)
    //                if (1 === this.hour) this.hour = 12;
    //                else {
    //                    if (12 === this.hour) return this.hour--, this.toggleMeridian();
    //                    if (0 === this.hour) return this.hour = 11, this.toggleMeridian();
    //                    this.hour--
    //                }
    //            else this.hour <= 0 ? this.hour = this.maxHours - 1 : this.hour--
    //        },
    //        decrementMinute: function(t) {
    //            var e;
    //            0 > (e = t ? this.minute - t : this.minute - this.minuteStep) ? (this.decrementHour(), this.minute = e + 60) : this.minute = e
    //        },
    //        decrementSecond: function() {
    //            var t = this.second - this.secondStep;
    //            0 > t ? (this.decrementMinute(!0), this.second = t + 60) : this.second = t
    //        },
    //        elementKeydown: function(t) {
    //            switch (t.which) {
    //                case 9:
    //                    if (t.shiftKey) {
    //                        if ("hour" === this.highlightedUnit) {
    //                            this.hideWidget();
    //                            break
    //                        }
    //                        this.highlightPrevUnit()
    //                    } else {
    //                        if (this.showMeridian && "meridian" === this.highlightedUnit || this.showSeconds && "second" === this.highlightedUnit || !this.showMeridian && !this.showSeconds && "minute" === this.highlightedUnit) {
    //                            this.hideWidget();
    //                            break
    //                        }
    //                        this.highlightNextUnit()
    //                    }
    //                    t.preventDefault(), this.updateFromElementVal();
    //                    break;
    //                case 27:
    //                    this.updateFromElementVal();
    //                    break;
    //                case 37:
    //                    t.preventDefault(), this.highlightPrevUnit(), this.updateFromElementVal();
    //                    break;
    //                case 38:
    //                    switch (t.preventDefault(), this.highlightedUnit) {
    //                        case "hour":
    //                            this.incrementHour(), this.highlightHour();
    //                            break;
    //                        case "minute":
    //                            this.incrementMinute(), this.highlightMinute();
    //                            break;
    //                        case "second":
    //                            this.incrementSecond(), this.highlightSecond();
    //                            break;
    //                        case "meridian":
    //                            this.toggleMeridian(), this.highlightMeridian()
    //                    }
    //                    this.update();
    //                    break;
    //                case 39:
    //                    t.preventDefault(), this.highlightNextUnit(), this.updateFromElementVal();
    //                    break;
    //                case 40:
    //                    switch (t.preventDefault(), this.highlightedUnit) {
    //                        case "hour":
    //                            this.decrementHour(), this.highlightHour();
    //                            break;
    //                        case "minute":
    //                            this.decrementMinute(), this.highlightMinute();
    //                            break;
    //                        case "second":
    //                            this.decrementSecond(), this.highlightSecond();
    //                            break;
    //                        case "meridian":
    //                            this.toggleMeridian(), this.highlightMeridian()
    //                    }
    //                    this.update()
    //            }
    //        },
    //        getCursorPosition: function() {
    //            var t = this.$element.get(0);
    //            if ("selectionStart" in t) return t.selectionStart;
    //            if (i.selection) {
    //                t.focus();
    //                var e = i.selection.createRange(),
    //                    n = i.selection.createRange().text.length;
    //                return e.moveStart("character", -t.value.length), e.text.length - n
    //            }
    //        },
    //        getTemplate: function() {
    //            var t, e, i, n, r, o;
    //            switch (this.showInputs ? (e = '<input type="text" class="bootstrap-timepicker-hour" maxlength="2"/>', i = '<input type="text" class="bootstrap-timepicker-minute" maxlength="2"/>', n = '<input type="text" class="bootstrap-timepicker-second" maxlength="2"/>', r = '<input type="text" class="bootstrap-timepicker-meridian" maxlength="2"/>') : (e = '<span class="bootstrap-timepicker-hour"></span>', i = '<span class="bootstrap-timepicker-minute"></span>', n = '<span class="bootstrap-timepicker-second"></span>', r = '<span class="bootstrap-timepicker-meridian"></span>'), o = '<table><tr><td><a href="#" data-action="incrementHour"><span class="' + this.icons.up + '"></span></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><span class="' + this.icons.up + '"></span></a></td>' + (this.showSeconds ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><span class="' + this.icons.up + '"></span></a></td>' : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><span class="' + this.icons.up + '"></span></a></td>' : "") + "</tr><tr><td>" + e + '</td> <td class="separator">:</td><td>' + i + "</td> " + (this.showSeconds ? '<td class="separator">:</td><td>' + n + "</td>" : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td>' + r + "</td>" : "") + '</tr><tr><td><a href="#" data-action="decrementHour"><span class="' + this.icons.down + '"></span></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><span class="' + this.icons.down + '"></span></a></td>' + (this.showSeconds ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><span class="' + this.icons.down + '"></span></a></td>' : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><span class="' + this.icons.down + '"></span></a></td>' : "") + "</tr></table>", this.template) {
    //                case "modal":
    //                    t = '<div class="bootstrap-timepicker-widget modal hide fade in" data-backdrop="' + (this.modalBackdrop ? "true" : "false") + '"><div class="modal-header"><a href="#" class="close" data-dismiss="modal">&times;</a><h3>Pick a Time</h3></div><div class="modal-content">' + o + '</div><div class="modal-footer"><a href="#" class="btn btn-primary" data-dismiss="modal">OK</a></div></div>';
    //                    break;
    //                case "dropdown":
    //                    t = '<div class="bootstrap-timepicker-widget dropdown-menu">' + o + "</div>"
    //            }
    //            return t
    //        },
    //        getTime: function() {
    //            return "" === this.hour ? "" : this.hour + ":" + (1 === this.minute.toString().length ? "0" + this.minute : this.minute) + (this.showSeconds ? ":" + (1 === this.second.toString().length ? "0" + this.second : this.second) : "") + (this.showMeridian ? " " + this.meridian : "")
    //        },
    //        hideWidget: function() {
    //            !1 !== this.isOpen && (this.$element.trigger({
    //                type: "hide.timepicker",
    //                time: {
    //                    value: this.getTime(),
    //                    hours: this.hour,
    //                    minutes: this.minute,
    //                    seconds: this.second,
    //                    meridian: this.meridian
    //                }
    //            }), "modal" === this.template && this.$widget.modal ? this.$widget.modal("hide") : this.$widget.removeClass("open"), t(i).off("mousedown.timepicker, touchend.timepicker", this.handleDocumentClick), this.isOpen = !1, this.$widget.detach())
    //        },
    //        highlightUnit: function() {
    //            this.position = this.getCursorPosition(), this.position >= 0 && this.position <= 2 ? this.highlightHour() : this.position >= 3 && this.position <= 5 ? this.highlightMinute() : this.position >= 6 && this.position <= 8 ? this.showSeconds ? this.highlightSecond() : this.highlightMeridian() : this.position >= 9 && this.position <= 11 && this.highlightMeridian()
    //        },
    //        highlightNextUnit: function() {
    //            switch (this.highlightedUnit) {
    //                case "hour":
    //                    this.highlightMinute();
    //                    break;
    //                case "minute":
    //                    this.showSeconds ? this.highlightSecond() : this.showMeridian ? this.highlightMeridian() : this.highlightHour();
    //                    break;
    //                case "second":
    //                    this.showMeridian ? this.highlightMeridian() : this.highlightHour();
    //                    break;
    //                case "meridian":
    //                    this.highlightHour()
    //            }
    //        },
    //        highlightPrevUnit: function() {
    //            switch (this.highlightedUnit) {
    //                case "hour":
    //                    this.showMeridian ? this.highlightMeridian() : this.showSeconds ? this.highlightSecond() : this.highlightMinute();
    //                    break;
    //                case "minute":
    //                    this.highlightHour();
    //                    break;
    //                case "second":
    //                    this.highlightMinute();
    //                    break;
    //                case "meridian":
    //                    this.showSeconds ? this.highlightSecond() : this.highlightMinute()
    //            }
    //        },
    //        highlightHour: function() {
    //            var t = this.$element.get(0),
    //                e = this;
    //            this.highlightedUnit = "hour", t.setSelectionRange && setTimeout(function() {
    //                e.hour < 10 ? t.setSelectionRange(0, 1) : t.setSelectionRange(0, 2)
    //            }, 0)
    //        },
    //        highlightMinute: function() {
    //            var t = this.$element.get(0),
    //                e = this;
    //            this.highlightedUnit = "minute", t.setSelectionRange && setTimeout(function() {
    //                e.hour < 10 ? t.setSelectionRange(2, 4) : t.setSelectionRange(3, 5)
    //            }, 0)
    //        },
    //        highlightSecond: function() {
    //            var t = this.$element.get(0),
    //                e = this;
    //            this.highlightedUnit = "second", t.setSelectionRange && setTimeout(function() {
    //                e.hour < 10 ? t.setSelectionRange(5, 7) : t.setSelectionRange(6, 8)
    //            }, 0)
    //        },
    //        highlightMeridian: function() {
    //            var t = this.$element.get(0),
    //                e = this;
    //            this.highlightedUnit = "meridian", t.setSelectionRange && (this.showSeconds ? setTimeout(function() {
    //                e.hour < 10 ? t.setSelectionRange(8, 10) : t.setSelectionRange(9, 11)
    //            }, 0) : setTimeout(function() {
    //                e.hour < 10 ? t.setSelectionRange(5, 7) : t.setSelectionRange(6, 8)
    //            }, 0))
    //        },
    //        incrementHour: function() {
    //            if (this.showMeridian) {
    //                if (11 === this.hour) return this.hour++, this.toggleMeridian();
    //                12 === this.hour && (this.hour = 0)
    //            }
    //            return this.hour === this.maxHours - 1 ? void(this.hour = 0) : void this.hour++
    //        },
    //        incrementMinute: function(t) {
    //            var e;
    //            (e = t ? this.minute + t : this.minute + this.minuteStep - this.minute % this.minuteStep) > 59 ? (this.incrementHour(), this.minute = e - 60) : this.minute = e
    //        },
    //        incrementSecond: function() {
    //            var t = this.second + this.secondStep - this.second % this.secondStep;
    //            t > 59 ? (this.incrementMinute(!0), this.second = t - 60) : this.second = t
    //        },
    //        mousewheel: function(e) {
    //            if (!this.disableMousewheel) {
    //                e.preventDefault(), e.stopPropagation();
    //                var i = e.originalEvent.wheelDelta || -e.originalEvent.detail,
    //                    n = null;
    //                switch ("mousewheel" === e.type ? n = -1 * e.originalEvent.wheelDelta : "DOMMouseScroll" === e.type && (n = 40 * e.originalEvent.detail), n && (e.preventDefault(), t(this).scrollTop(n + t(this).scrollTop())), this.highlightedUnit) {
    //                    case "minute":
    //                        i > 0 ? this.incrementMinute() : this.decrementMinute(), this.highlightMinute();
    //                        break;
    //                    case "second":
    //                        i > 0 ? this.incrementSecond() : this.decrementSecond(), this.highlightSecond();
    //                        break;
    //                    case "meridian":
    //                        this.toggleMeridian(), this.highlightMeridian();
    //                        break;
    //                    default:
    //                        i > 0 ? this.incrementHour() : this.decrementHour(), this.highlightHour()
    //                }
    //                return !1
    //            }
    //        },
    //        changeToNearestStep: function(t, e) {
    //            return t % e == 0 ? t : Math.round(t % e / e) ? (t + (e - t % e)) % 60 : t - t % e
    //        },
    //        place: function() {
    //            if (!this.isInline) {
    //                var i = this.$widget.outerWidth(),
    //                    n = this.$widget.outerHeight(),
    //                    r = t(e).width(),
    //                    o = t(e).height(),
    //                    s = t(e).scrollTop(),
    //                    a = parseInt(this.$element.parents().filter(function() {
    //                        return "auto" !== t(this).css("z-index")
    //                    }).first().css("z-index"), 10) + 10,
    //                    l = this.component ? this.component.parent().offset() : this.$element.offset(),
    //                    c = this.component ? this.component.outerHeight(!0) : this.$element.outerHeight(!1),
    //                    h = this.component ? this.component.outerWidth(!0) : this.$element.outerWidth(!1),
    //                    d = l.left,
    //                    u = l.top;
    //                this.$widget.removeClass("timepicker-orient-top timepicker-orient-bottom timepicker-orient-right timepicker-orient-left"), "auto" !== this.orientation.x ? (this.$widget.addClass("timepicker-orient-" + this.orientation.x), "right" === this.orientation.x && (d -= i - h)) : (this.$widget.addClass("timepicker-orient-left"), l.left < 0 ? d -= l.left - 10 : l.left + i > r && (d = r - i - 10));
    //                var p, f, m = this.orientation.y;
    //                "auto" === m && (p = -s + l.top - n, f = s + o - (l.top + c + n), m = Math.max(p, f) === f ? "top" : "bottom"), this.$widget.addClass("timepicker-orient-" + m), "top" === m ? u += c : u -= n + parseInt(this.$widget.css("padding-top"), 10), this.$widget.css({
    //                    top: u,
    //                    left: d,
    //                    zIndex: a
    //                })
    //            }
    //        },
    //        remove: function() {
    //            t("document").off(".timepicker"), this.$widget && this.$widget.remove(), delete this.$element.data().timepicker
    //        },
    //        setDefaultTime: function(t) {
    //            if (this.$element.val()) this.updateFromElementVal();
    //            else if ("current" === t) {
    //                var e = new Date,
    //                    i = e.getHours(),
    //                    n = e.getMinutes(),
    //                    r = e.getSeconds(),
    //                    o = "AM";
    //                0 !== r && (60 === (r = Math.ceil(e.getSeconds() / this.secondStep) * this.secondStep) && (n += 1, r = 0)), 0 !== n && (60 === (n = Math.ceil(e.getMinutes() / this.minuteStep) * this.minuteStep) && (i += 1, n = 0)), this.showMeridian && (0 === i ? i = 12 : i >= 12 ? (i > 12 && (i -= 12), o = "PM") : o = "AM"), this.hour = i, this.minute = n, this.second = r, this.meridian = o, this.update()
    //            } else !1 === t ? (this.hour = 0, this.minute = 0, this.second = 0, this.meridian = "AM") : this.setTime(t)
    //        },
    //        setTime: function(t, e) {
    //            if (t) {
    //                var i, n, r, o, s, a;
    //                if ("object" == typeof t && t.getMonth) r = t.getHours(), o = t.getMinutes(), s = t.getSeconds(), this.showMeridian && (a = "AM", r > 12 && (a = "PM", r %= 12), 12 === r && (a = "PM"));
    //                else {
    //                    if ((i = (/a/i.test(t) ? 1 : 0) + (/p/i.test(t) ? 2 : 0)) > 2) return void this.clear();
    //                    if (r = (n = t.replace(/[^0-9\:]/g, "").split(":"))[0] ? n[0].toString() : n.toString(), this.explicitMode && r.length > 2 && r.length % 2 != 0) return void this.clear();
    //                    o = n[1] ? n[1].toString() : "", s = n[2] ? n[2].toString() : "", r.length > 4 && (s = r.slice(-2), r = r.slice(0, -2)), r.length > 2 && (o = r.slice(-2), r = r.slice(0, -2)), o.length > 2 && (s = o.slice(-2), o = o.slice(0, -2)), r = parseInt(r, 10), o = parseInt(o, 10), s = parseInt(s, 10), isNaN(r) && (r = 0), isNaN(o) && (o = 0), isNaN(s) && (s = 0), s > 59 && (s = 59), o > 59 && (o = 59), r >= this.maxHours && (r = this.maxHours - 1), this.showMeridian ? (r > 12 && (i = 2, r -= 12), i || (i = 1), 0 === r && (r = 12), a = 1 === i ? "AM" : "PM") : 12 > r && 2 === i ? r += 12 : r >= this.maxHours ? r = this.maxHours - 1 : (0 > r || 12 === r && 1 === i) && (r = 0)
    //                }
    //                this.hour = r, this.snapToStep ? (this.minute = this.changeToNearestStep(o, this.minuteStep), this.second = this.changeToNearestStep(s, this.secondStep)) : (this.minute = o, this.second = s), this.meridian = a, this.update(e)
    //            } else this.clear()
    //        },
    //        showWidget: function() {
    //            this.isOpen || this.$element.is(":disabled") || (this.$widget.appendTo(this.appendWidgetTo), t(i).on("mousedown.timepicker, touchend.timepicker", {
    //                scope: this
    //            }, this.handleDocumentClick), this.$element.trigger({
    //                type: "show.timepicker",
    //                time: {
    //                    value: this.getTime(),
    //                    hours: this.hour,
    //                    minutes: this.minute,
    //                    seconds: this.second,
    //                    meridian: this.meridian
    //                }
    //            }), this.place(), this.disableFocus && this.$element.blur(), "" === this.hour && (this.defaultTime ? this.setDefaultTime(this.defaultTime) : this.setTime("0:0:0")), "modal" === this.template && this.$widget.modal ? this.$widget.modal("show").on("hidden", t.proxy(this.hideWidget, this)) : !1 === this.isOpen && this.$widget.addClass("open"), this.isOpen = !0)
    //        },
    //        toggleMeridian: function() {
    //            this.meridian = "AM" === this.meridian ? "PM" : "AM"
    //        },
    //        update: function(t) {
    //            this.updateElement(), t || this.updateWidget(), this.$element.trigger({
    //                type: "changeTime.timepicker",
    //                time: {
    //                    value: this.getTime(),
    //                    hours: this.hour,
    //                    minutes: this.minute,
    //                    seconds: this.second,
    //                    meridian: this.meridian
    //                }
    //            })
    //        },
    //        updateElement: function() {
    //            this.$element.val(this.getTime()).change()
    //        },
    //        updateFromElementVal: function() {
    //            this.setTime(this.$element.val())
    //        },
    //        updateWidget: function() {
    //            if (!1 !== this.$widget) {
    //                var t = this.hour,
    //                    e = 1 === this.minute.toString().length ? "0" + this.minute : this.minute,
    //                    i = 1 === this.second.toString().length ? "0" + this.second : this.second;
    //                this.showInputs ? (this.$widget.find("input.bootstrap-timepicker-hour").val(t), this.$widget.find("input.bootstrap-timepicker-minute").val(e), this.showSeconds && this.$widget.find("input.bootstrap-timepicker-second").val(i), this.showMeridian && this.$widget.find("input.bootstrap-timepicker-meridian").val(this.meridian)) : (this.$widget.find("span.bootstrap-timepicker-hour").text(t), this.$widget.find("span.bootstrap-timepicker-minute").text(e), this.showSeconds && this.$widget.find("span.bootstrap-timepicker-second").text(i), this.showMeridian && this.$widget.find("span.bootstrap-timepicker-meridian").text(this.meridian))
    //            }
    //        },
    //        updateFromWidgetInputs: function() {
    //            if (!1 !== this.$widget) {
    //                var t = this.$widget.find("input.bootstrap-timepicker-hour").val() + ":" + this.$widget.find("input.bootstrap-timepicker-minute").val() + (this.showSeconds ? ":" + this.$widget.find("input.bootstrap-timepicker-second").val() : "") + (this.showMeridian ? this.$widget.find("input.bootstrap-timepicker-meridian").val() : "");
    //                this.setTime(t, !0)
    //            }
    //        },
    //        widgetClick: function(e) {
    //            e.stopPropagation(), e.preventDefault();
    //            var i = t(e.target),
    //                n = i.closest("a").data("action");
    //            n && this[n](), this.update(), i.is("input") && i.get(0).setSelectionRange(0, 2)
    //        },
    //        widgetKeydown: function(e) {
    //            var i = t(e.target),
    //                n = i.attr("class").replace("bootstrap-timepicker-", "");
    //            switch (e.which) {
    //                case 9:
    //                    if (e.shiftKey) {
    //                        if ("hour" === n) return this.hideWidget()
    //                    } else if (this.showMeridian && "meridian" === n || this.showSeconds && "second" === n || !this.showMeridian && !this.showSeconds && "minute" === n) return this.hideWidget();
    //                    break;
    //                case 27:
    //                    this.hideWidget();
    //                    break;
    //                case 38:
    //                    switch (e.preventDefault(), n) {
    //                        case "hour":
    //                            this.incrementHour();
    //                            break;
    //                        case "minute":
    //                            this.incrementMinute();
    //                            break;
    //                        case "second":
    //                            this.incrementSecond();
    //                            break;
    //                        case "meridian":
    //                            this.toggleMeridian()
    //                    }
    //                    this.setTime(this.getTime()), i.get(0).setSelectionRange(0, 2);
    //                    break;
    //                case 40:
    //                    switch (e.preventDefault(), n) {
    //                        case "hour":
    //                            this.decrementHour();
    //                            break;
    //                        case "minute":
    //                            this.decrementMinute();
    //                            break;
    //                        case "second":
    //                            this.decrementSecond();
    //                            break;
    //                        case "meridian":
    //                            this.toggleMeridian()
    //                    }
    //                    this.setTime(this.getTime()), i.get(0).setSelectionRange(0, 2)
    //            }
    //        },
    //        widgetKeyup: function(t) {
    //            (65 === t.which || 77 === t.which || 80 === t.which || 46 === t.which || 8 === t.which || t.which >= 48 && t.which <= 57 || t.which >= 96 && t.which <= 105) && this.updateFromWidgetInputs()
    //        }
    //    }, t.fn.timepicker = function(e) {
    //        var i = Array.apply(null, arguments);
    //        return i.shift(), this.each(function() {
    //            var r = t(this),
    //                o = r.data("timepicker"),
    //                s = "object" == typeof e && e;
    //            o || r.data("timepicker", o = new n(this, t.extend({}, t.fn.timepicker.defaults, s, t(this).data()))), "string" == typeof e && o[e].apply(o, i)
    //        })
    //    }, t.fn.timepicker.defaults = {
    //        defaultTime: "current",
    //        disableFocus: !1,
    //        disableMousewheel: !1,
    //        isOpen: !1,
    //        minuteStep: 15,
    //        modalBackdrop: !1,
    //        orientation: {
    //            x: "auto",
    //            y: "auto"
    //        },
    //        secondStep: 15,
    //        snapToStep: !1,
    //        showSeconds: !1,
    //        showInputs: !0,
    //        showMeridian: !0,
    //        template: "dropdown",
    //        appendWidgetTo: "body",
    //        showWidgetOnAddonClick: !0,
    //        icons: {
    //            up: "glyphicon glyphicon-chevron-up",
    //            down: "glyphicon glyphicon-chevron-down"
    //        },
    //        maxHours: 24,
    //        explicitMode: !1
    //    }, t.fn.timepicker.Constructor = n, t(i).on("focus.timepicker.data-api click.timepicker.data-api", '[data-provide="timepicker"]', function(e) {
    //        var i = t(this);
    //        i.data("timepicker") || (e.preventDefault(), i.timepicker())
    //    })
    //}(jQuery, window, document), $.fn.timepicker.defaults = $.extend(!0, {}, $.fn.timepicker.defaults, {
    //    icons: {
    //        up: "la la-angle-up",
    //        down: "la la-angle-down"
    //    }
    //}),
    function(t, e) {
        if ("function" == typeof define && define.amd) define(["moment", "jquery"], function(t, i) {
            return i.fn || (i.fn = {}), e(t, i)
        });
        else if ("object" == typeof module && module.exports) {
            var i = "undefined" != typeof window ? window.jQuery : void 0;
            i || (i = require("jquery")).fn || (i.fn = {});
            var n = "undefined" != typeof window && void 0 !== window.moment ? window.moment : require("moment");
            module.exports = e(n, i)
        } else t.daterangepicker = e(t.moment, t.jQuery)
    }(this, function(t, e) {
        var i = function(i, n, r) {
            if (this.parentEl = "body", this.element = e(i), this.startDate = t().startOf("day"), this.endDate = t().endOf("day"), this.minDate = !1, this.maxDate = !1, this.dateLimit = !1, this.autoApply = !1, this.singleDatePicker = !1, this.showDropdowns = !1, this.showWeekNumbers = !1, this.showISOWeekNumbers = !1, this.showCustomRangeLabel = !0, this.timePicker = !1, this.timePicker24Hour = !1, this.timePickerIncrement = 1, this.timePickerSeconds = !1, this.linkedCalendars = !0, this.autoUpdateInput = !0, this.alwaysShowCalendars = !1, this.ranges = {}, this.opens = "right", this.element.hasClass("pull-right") && (this.opens = "left"), this.drops = "down", this.element.hasClass("dropup") && (this.drops = "up"), this.buttonClasses = "btn btn-sm", this.applyClass = "btn-success", this.cancelClass = "btn-default", this.locale = {
                direction: "ltr",
                format: t.localeData().longDateFormat("L"),
                separator: " - ",
                applyLabel: "Apply",
                cancelLabel: "Cancel",
                weekLabel: "W",
                customRangeLabel: "Custom Range",
                daysOfWeek: t.weekdaysMin(),
                monthNames: t.monthsShort(),
                firstDay: t.localeData().firstDayOfWeek()
            }, this.callback = function() {}, this.isShowing = !1, this.leftCalendar = {}, this.rightCalendar = {}, "object" == typeof n && null !== n || (n = {}), "string" == typeof(n = e.extend(this.element.data(), n)).template || n.template instanceof e || (n.template = '<div class="daterangepicker dropdown-menu"><div class="calendar left"><div class="daterangepicker_input"><input class="input-mini form-control" type="text" name="daterangepicker_start" value="" /><i class="fa fa-calendar glyphicon glyphicon-calendar"></i><div class="calendar-time"><div></div><i class="fa fa-clock-o glyphicon glyphicon-time"></i></div></div><div class="calendar-table"></div></div><div class="calendar right"><div class="daterangepicker_input"><input class="input-mini form-control" type="text" name="daterangepicker_end" value="" /><i class="fa fa-calendar glyphicon glyphicon-calendar"></i><div class="calendar-time"><div></div><i class="fa fa-clock-o glyphicon glyphicon-time"></i></div></div><div class="calendar-table"></div></div><div class="ranges"><div class="range_inputs"><button class="applyBtn" disabled="disabled" type="button"></button> <button class="cancelBtn" type="button"></button></div></div></div>'), this.parentEl = n.parentEl && e(n.parentEl).length ? e(n.parentEl) : e(this.parentEl), this.container = e(n.template).appendTo(this.parentEl), "object" == typeof n.locale && ("string" == typeof n.locale.direction && (this.locale.direction = n.locale.direction), "string" == typeof n.locale.format && (this.locale.format = n.locale.format), "string" == typeof n.locale.separator && (this.locale.separator = n.locale.separator), "object" == typeof n.locale.daysOfWeek && (this.locale.daysOfWeek = n.locale.daysOfWeek.slice()), "object" == typeof n.locale.monthNames && (this.locale.monthNames = n.locale.monthNames.slice()), "number" == typeof n.locale.firstDay && (this.locale.firstDay = n.locale.firstDay), "string" == typeof n.locale.applyLabel && (this.locale.applyLabel = n.locale.applyLabel), "string" == typeof n.locale.cancelLabel && (this.locale.cancelLabel = n.locale.cancelLabel), "string" == typeof n.locale.weekLabel && (this.locale.weekLabel = n.locale.weekLabel), "string" == typeof n.locale.customRangeLabel)) {
                (p = document.createElement("textarea")).innerHTML = n.locale.customRangeLabel;
                var o = p.value;
                this.locale.customRangeLabel = o
            }
            if (this.container.addClass(this.locale.direction), "string" == typeof n.startDate && (this.startDate = t(n.startDate, this.locale.format)), "string" == typeof n.endDate && (this.endDate = t(n.endDate, this.locale.format)), "string" == typeof n.minDate && (this.minDate = t(n.minDate, this.locale.format)), "string" == typeof n.maxDate && (this.maxDate = t(n.maxDate, this.locale.format)), "object" == typeof n.startDate && (this.startDate = t(n.startDate)), "object" == typeof n.endDate && (this.endDate = t(n.endDate)), "object" == typeof n.minDate && (this.minDate = t(n.minDate)), "object" == typeof n.maxDate && (this.maxDate = t(n.maxDate)), this.minDate && this.startDate.isBefore(this.minDate) && (this.startDate = this.minDate.clone()), this.maxDate && this.endDate.isAfter(this.maxDate) && (this.endDate = this.maxDate.clone()), "string" == typeof n.applyClass && (this.applyClass = n.applyClass), "string" == typeof n.cancelClass && (this.cancelClass = n.cancelClass), "object" == typeof n.dateLimit && (this.dateLimit = n.dateLimit), "string" == typeof n.opens && (this.opens = n.opens), "string" == typeof n.drops && (this.drops = n.drops), "boolean" == typeof n.showWeekNumbers && (this.showWeekNumbers = n.showWeekNumbers), "boolean" == typeof n.showISOWeekNumbers && (this.showISOWeekNumbers = n.showISOWeekNumbers), "string" == typeof n.buttonClasses && (this.buttonClasses = n.buttonClasses), "object" == typeof n.buttonClasses && (this.buttonClasses = n.buttonClasses.join(" ")), "boolean" == typeof n.showDropdowns && (this.showDropdowns = n.showDropdowns), "boolean" == typeof n.showCustomRangeLabel && (this.showCustomRangeLabel = n.showCustomRangeLabel), "boolean" == typeof n.singleDatePicker && (this.singleDatePicker = n.singleDatePicker, this.singleDatePicker && (this.endDate = this.startDate.clone())), "boolean" == typeof n.timePicker && (this.timePicker = n.timePicker), "boolean" == typeof n.timePickerSeconds && (this.timePickerSeconds = n.timePickerSeconds), "number" == typeof n.timePickerIncrement && (this.timePickerIncrement = n.timePickerIncrement), "boolean" == typeof n.timePicker24Hour && (this.timePicker24Hour = n.timePicker24Hour), "boolean" == typeof n.autoApply && (this.autoApply = n.autoApply), "boolean" == typeof n.autoUpdateInput && (this.autoUpdateInput = n.autoUpdateInput), "boolean" == typeof n.linkedCalendars && (this.linkedCalendars = n.linkedCalendars), "function" == typeof n.isInvalidDate && (this.isInvalidDate = n.isInvalidDate), "function" == typeof n.isCustomDate && (this.isCustomDate = n.isCustomDate), "boolean" == typeof n.alwaysShowCalendars && (this.alwaysShowCalendars = n.alwaysShowCalendars), 0 != this.locale.firstDay)
                for (var s = this.locale.firstDay; s > 0;) this.locale.daysOfWeek.push(this.locale.daysOfWeek.shift()), s--;
            var a, l, c;
            if (void 0 === n.startDate && void 0 === n.endDate && e(this.element).is("input[type=text]")) {
                var h = e(this.element).val(),
                    d = h.split(this.locale.separator);
                a = l = null, 2 == d.length ? (a = t(d[0], this.locale.format), l = t(d[1], this.locale.format)) : this.singleDatePicker && "" !== h && (a = t(h, this.locale.format), l = t(h, this.locale.format)), null !== a && null !== l && (this.setStartDate(a), this.setEndDate(l))
            }
            if ("object" == typeof n.ranges) {
                for (c in n.ranges) {
                    a = "string" == typeof n.ranges[c][0] ? t(n.ranges[c][0], this.locale.format) : t(n.ranges[c][0]), l = "string" == typeof n.ranges[c][1] ? t(n.ranges[c][1], this.locale.format) : t(n.ranges[c][1]), this.minDate && a.isBefore(this.minDate) && (a = this.minDate.clone());
                    var u = this.maxDate;
                    if (this.dateLimit && u && a.clone().add(this.dateLimit).isAfter(u) && (u = a.clone().add(this.dateLimit)), u && l.isAfter(u) && (l = u.clone()), !(this.minDate && l.isBefore(this.minDate, this.timepicker ? "minute" : "day") || u && a.isAfter(u, this.timepicker ? "minute" : "day"))) {
                        var p;
                        (p = document.createElement("textarea")).innerHTML = c;
                        o = p.value;
                        this.ranges[o] = [a, l]
                    }
                }
                var f = "<ul>";
                for (c in this.ranges) f += '<li data-range-key="' + c + '">' + c + "</li>";
                this.showCustomRangeLabel && (f += '<li data-range-key="' + this.locale.customRangeLabel + '">' + this.locale.customRangeLabel + "</li>"), f += "</ul>", this.container.find(".ranges").prepend(f)
            }
            "function" == typeof r && (this.callback = r), this.timePicker || (this.startDate = this.startDate.startOf("day"), this.endDate = this.endDate.endOf("day"), this.container.find(".calendar-time").hide()), this.timePicker && this.autoApply && (this.autoApply = !1), this.autoApply && "object" != typeof n.ranges ? this.container.find(".ranges").hide() : this.autoApply && this.container.find(".applyBtn, .cancelBtn").addClass("hide"), this.singleDatePicker && (this.container.addClass("single"), this.container.find(".calendar.left").addClass("single"), this.container.find(".calendar.left").show(), this.container.find(".calendar.right").hide(), this.container.find(".daterangepicker_input input, .daterangepicker_input > i").hide(), this.timePicker ? this.container.find(".ranges ul").hide() : this.container.find(".ranges").hide()), (void 0 === n.ranges && !this.singleDatePicker || this.alwaysShowCalendars) && this.container.addClass("show-calendar"), this.container.addClass("opens" + this.opens), void 0 !== n.ranges && "right" == this.opens && this.container.find(".ranges").prependTo(this.container.find(".calendar.left").parent()), this.container.find(".applyBtn, .cancelBtn").addClass(this.buttonClasses), this.applyClass.length && this.container.find(".applyBtn").addClass(this.applyClass), this.cancelClass.length && this.container.find(".cancelBtn").addClass(this.cancelClass), this.container.find(".applyBtn").html(this.locale.applyLabel), this.container.find(".cancelBtn").html(this.locale.cancelLabel), this.container.find(".calendar").on("click.daterangepicker", ".prev", e.proxy(this.clickPrev, this)).on("click.daterangepicker", ".next", e.proxy(this.clickNext, this)).on("mousedown.daterangepicker", "td.available", e.proxy(this.clickDate, this)).on("mouseenter.daterangepicker", "td.available", e.proxy(this.hoverDate, this)).on("mouseleave.daterangepicker", "td.available", e.proxy(this.updateFormInputs, this)).on("change.daterangepicker", "select.yearselect", e.proxy(this.monthOrYearChanged, this)).on("change.daterangepicker", "select.monthselect", e.proxy(this.monthOrYearChanged, this)).on("change.daterangepicker", "select.hourselect,select.minuteselect,select.secondselect,select.ampmselect", e.proxy(this.timeChanged, this)).on("click.daterangepicker", ".daterangepicker_input input", e.proxy(this.showCalendars, this)).on("focus.daterangepicker", ".daterangepicker_input input", e.proxy(this.formInputsFocused, this)).on("blur.daterangepicker", ".daterangepicker_input input", e.proxy(this.formInputsBlurred, this)).on("change.daterangepicker", ".daterangepicker_input input", e.proxy(this.formInputsChanged, this)).on("keydown.daterangepicker", ".daterangepicker_input input", e.proxy(this.formInputsKeydown, this)), this.container.find(".ranges").on("click.daterangepicker", "button.applyBtn", e.proxy(this.clickApply, this)).on("click.daterangepicker", "button.cancelBtn", e.proxy(this.clickCancel, this)).on("click.daterangepicker", "li", e.proxy(this.clickRange, this)).on("mouseenter.daterangepicker", "li", e.proxy(this.hoverRange, this)).on("mouseleave.daterangepicker", "li", e.proxy(this.updateFormInputs, this)), this.element.is("input") || this.element.is("button") ? this.element.on({
                "click.daterangepicker": e.proxy(this.show, this),
                "focus.daterangepicker": e.proxy(this.show, this),
                "keyup.daterangepicker": e.proxy(this.elementChanged, this),
                "keydown.daterangepicker": e.proxy(this.keydown, this)
            }) : (this.element.on("click.daterangepicker", e.proxy(this.toggle, this)), this.element.on("keydown.daterangepicker", e.proxy(this.toggle, this))), this.element.is("input") && !this.singleDatePicker && this.autoUpdateInput ? (this.element.val(this.startDate.format(this.locale.format) + this.locale.separator + this.endDate.format(this.locale.format)), this.element.trigger("change")) : this.element.is("input") && this.autoUpdateInput && (this.element.val(this.startDate.format(this.locale.format)), this.element.trigger("change"))
        };
        return i.prototype = {
            constructor: i,
            setStartDate: function(e) {
                "string" == typeof e && (this.startDate = t(e, this.locale.format)), "object" == typeof e && (this.startDate = t(e)), this.timePicker || (this.startDate = this.startDate.startOf("day")), this.timePicker && this.timePickerIncrement && this.startDate.minute(Math.round(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement), this.minDate && this.startDate.isBefore(this.minDate) && (this.startDate = this.minDate.clone(), this.timePicker && this.timePickerIncrement && this.startDate.minute(Math.round(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement)), this.maxDate && this.startDate.isAfter(this.maxDate) && (this.startDate = this.maxDate.clone(), this.timePicker && this.timePickerIncrement && this.startDate.minute(Math.floor(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement)), this.isShowing || this.updateElement(), this.updateMonthsInView()
            },
            setEndDate: function(e) {
                "string" == typeof e && (this.endDate = t(e, this.locale.format)), "object" == typeof e && (this.endDate = t(e)), this.timePicker || (this.endDate = this.endDate.add(1, "d").startOf("day").subtract(1, "second")), this.timePicker && this.timePickerIncrement && this.endDate.minute(Math.round(this.endDate.minute() / this.timePickerIncrement) * this.timePickerIncrement), this.endDate.isBefore(this.startDate) && (this.endDate = this.startDate.clone()), this.maxDate && this.endDate.isAfter(this.maxDate) && (this.endDate = this.maxDate.clone()), this.dateLimit && this.startDate.clone().add(this.dateLimit).isBefore(this.endDate) && (this.endDate = this.startDate.clone().add(this.dateLimit)), this.previousRightTime = this.endDate.clone(), this.isShowing || this.updateElement(), this.updateMonthsInView()
            },
            isInvalidDate: function() {
                return !1
            },
            isCustomDate: function() {
                return !1
            },
            updateView: function() {
                this.timePicker && (this.renderTimePicker("left"), this.renderTimePicker("right"), this.endDate ? this.container.find(".right .calendar-time select").removeAttr("disabled").removeClass("disabled") : this.container.find(".right .calendar-time select").attr("disabled", "disabled").addClass("disabled")), this.endDate ? (this.container.find('input[name="daterangepicker_end"]').removeClass("active"), this.container.find('input[name="daterangepicker_start"]').addClass("active")) : (this.container.find('input[name="daterangepicker_end"]').addClass("active"), this.container.find('input[name="daterangepicker_start"]').removeClass("active")), this.updateMonthsInView(), this.updateCalendars(), this.updateFormInputs()
            },
            updateMonthsInView: function() {
                if (this.endDate) {
                    if (!this.singleDatePicker && this.leftCalendar.month && this.rightCalendar.month && (this.startDate.format("YYYY-MM") == this.leftCalendar.month.format("YYYY-MM") || this.startDate.format("YYYY-MM") == this.rightCalendar.month.format("YYYY-MM")) && (this.endDate.format("YYYY-MM") == this.leftCalendar.month.format("YYYY-MM") || this.endDate.format("YYYY-MM") == this.rightCalendar.month.format("YYYY-MM"))) return;
                    this.leftCalendar.month = this.startDate.clone().date(2), this.linkedCalendars || this.endDate.month() == this.startDate.month() && this.endDate.year() == this.startDate.year() ? this.rightCalendar.month = this.startDate.clone().date(2).add(1, "month") : this.rightCalendar.month = this.endDate.clone().date(2)
                } else this.leftCalendar.month.format("YYYY-MM") != this.startDate.format("YYYY-MM") && this.rightCalendar.month.format("YYYY-MM") != this.startDate.format("YYYY-MM") && (this.leftCalendar.month = this.startDate.clone().date(2), this.rightCalendar.month = this.startDate.clone().date(2).add(1, "month"));
                this.maxDate && this.linkedCalendars && !this.singleDatePicker && this.rightCalendar.month > this.maxDate && (this.rightCalendar.month = this.maxDate.clone().date(2), this.leftCalendar.month = this.maxDate.clone().date(2).subtract(1, "month"))
            },
            updateCalendars: function() {
                if (this.timePicker) {
                    var t, e, i, n;
                    if (this.endDate) {
                        if (t = parseInt(this.container.find(".left .hourselect").val(), 10), e = parseInt(this.container.find(".left .minuteselect").val(), 10), i = this.timePickerSeconds ? parseInt(this.container.find(".left .secondselect").val(), 10) : 0, !this.timePicker24Hour) "PM" === (n = this.container.find(".left .ampmselect").val()) && t < 12 && (t += 12), "AM" === n && 12 === t && (t = 0)
                    } else if (t = parseInt(this.container.find(".right .hourselect").val(), 10), e = parseInt(this.container.find(".right .minuteselect").val(), 10), i = this.timePickerSeconds ? parseInt(this.container.find(".right .secondselect").val(), 10) : 0, !this.timePicker24Hour) "PM" === (n = this.container.find(".right .ampmselect").val()) && t < 12 && (t += 12), "AM" === n && 12 === t && (t = 0);
                    this.leftCalendar.month.hour(t).minute(e).second(i), this.rightCalendar.month.hour(t).minute(e).second(i)
                }
                this.renderCalendar("left"), this.renderCalendar("right"), this.container.find(".ranges li").removeClass("active"), null != this.endDate && this.calculateChosenLabel()
            },
            renderCalendar: function(i) {
                var n, r = (n = "left" == i ? this.leftCalendar : this.rightCalendar).month.month(),
                    o = n.month.year(),
                    s = n.month.hour(),
                    a = n.month.minute(),
                    l = n.month.second(),
                    c = t([o, r]).daysInMonth(),
                    h = t([o, r, 1]),
                    d = t([o, r, c]),
                    u = t(h).subtract(1, "month").month(),
                    p = t(h).subtract(1, "month").year(),
                    f = t([p, u]).daysInMonth(),
                    m = h.day();
                (n = []).firstDay = h, n.lastDay = d;
                for (var g = 0; g < 6; g++) n[g] = [];
                var v = f - m + this.locale.firstDay + 1;
                v > f && (v -= 7), m == this.locale.firstDay && (v = f - 6);
                for (var y = t([p, u, v, 12, a, l]), b = (g = 0, 0), _ = 0; g < 42; g++, b++, y = t(y).add(24, "hour")) g > 0 && b % 7 == 0 && (b = 0, _++), n[_][b] = y.clone().hour(s).minute(a).second(l), y.hour(12), this.minDate && n[_][b].format("YYYY-MM-DD") == this.minDate.format("YYYY-MM-DD") && n[_][b].isBefore(this.minDate) && "left" == i && (n[_][b] = this.minDate.clone()), this.maxDate && n[_][b].format("YYYY-MM-DD") == this.maxDate.format("YYYY-MM-DD") && n[_][b].isAfter(this.maxDate) && "right" == i && (n[_][b] = this.maxDate.clone());
                "left" == i ? this.leftCalendar.calendar = n : this.rightCalendar.calendar = n;
                var x = "left" == i ? this.minDate : this.startDate,
                    w = this.maxDate,
                    k = ("left" == i ? this.startDate : this.endDate, "ltr" == this.locale.direction ? {
                        left: "chevron-left",
                        right: "chevron-right"
                    } : {
                        left: "chevron-right",
                        right: "chevron-left"
                    }),
                    C = '<table class="table-condensed">';
                C += "<thead>", C += "<tr>", (this.showWeekNumbers || this.showISOWeekNumbers) && (C += "<th></th>"), x && !x.isBefore(n.firstDay) || this.linkedCalendars && "left" != i ? C += "<th></th>" : C += '<th class="prev available"><i class="fa fa-' + k.left + " glyphicon glyphicon-" + k.left + '"></i></th>';
                var S = this.locale.monthNames[n[1][1].month()] + n[1][1].format(" YYYY");
                if (this.showDropdowns) {
                    for (var D = n[1][1].month(), T = n[1][1].year(), A = w && w.year() || T + 5, M = x && x.year() || T - 50, E = T == M, P = T == A, I = '<select class="monthselect">', O = 0; O < 12; O++)(!E || O >= x.month()) && (!P || O <= w.month()) ? I += "<option value='" + O + "'" + (O === D ? " selected='selected'" : "") + ">" + this.locale.monthNames[O] + "</option>" : I += "<option value='" + O + "'" + (O === D ? " selected='selected'" : "") + " disabled='disabled'>" + this.locale.monthNames[O] + "</option>";
                    I += "</select>";
                    for (var N = '<select class="yearselect">', L = M; L <= A; L++) N += '<option value="' + L + '"' + (L === T ? ' selected="selected"' : "") + ">" + L + "</option>";
                    S = I + (N += "</select>")
                }
                if (C += '<th colspan="5" class="month">' + S + "</th>", w && !w.isAfter(n.lastDay) || this.linkedCalendars && "right" != i && !this.singleDatePicker ? C += "<th></th>" : C += '<th class="next available"><i class="fa fa-' + k.right + " glyphicon glyphicon-" + k.right + '"></i></th>', C += "</tr>", C += "<tr>", (this.showWeekNumbers || this.showISOWeekNumbers) && (C += '<th class="week">' + this.locale.weekLabel + "</th>"), e.each(this.locale.daysOfWeek, function(t, e) {
                        C += "<th>" + e + "</th>"
                }), C += "</tr>", C += "</thead>", C += "<tbody>", null == this.endDate && this.dateLimit) {
                    var j = this.startDate.clone().add(this.dateLimit).endOf("day");
                    w && !j.isBefore(w) || (w = j)
                }
                for (_ = 0; _ < 6; _++) {
                    C += "<tr>", this.showWeekNumbers ? C += '<td class="week">' + n[_][0].week() + "</td>" : this.showISOWeekNumbers && (C += '<td class="week">' + n[_][0].isoWeek() + "</td>");
                    for (b = 0; b < 7; b++) {
                        var F = [];
                        n[_][b].isSame(new Date, "day") && F.push("today"), n[_][b].isoWeekday() > 5 && F.push("weekend"), n[_][b].month() != n[1][1].month() && F.push("off"), this.minDate && n[_][b].isBefore(this.minDate, "day") && F.push("off", "disabled"), w && n[_][b].isAfter(w, "day") && F.push("off", "disabled"), this.isInvalidDate(n[_][b]) && F.push("off", "disabled"), n[_][b].format("YYYY-MM-DD") == this.startDate.format("YYYY-MM-DD") && F.push("active", "start-date"), null != this.endDate && n[_][b].format("YYYY-MM-DD") == this.endDate.format("YYYY-MM-DD") && F.push("active", "end-date"), null != this.endDate && n[_][b] > this.startDate && n[_][b] < this.endDate && F.push("in-range");
                        var $ = this.isCustomDate(n[_][b]);
                        !1 !== $ && ("string" == typeof $ ? F.push($) : Array.prototype.push.apply(F, $));
                        var R = "",
                            H = !1;
                        for (g = 0; g < F.length; g++) R += F[g] + " ", "disabled" == F[g] && (H = !0);
                        H || (R += "available"), C += '<td class="' + R.replace(/^\s+|\s+$/g, "") + '" data-title="r' + _ + "c" + b + '">' + n[_][b].date() + "</td>"
                    }
                    C += "</tr>"
                }
                C += "</tbody>", C += "</table>", this.container.find(".calendar." + i + " .calendar-table").html(C)
            },
            renderTimePicker: function(t) {
                if ("right" != t || this.endDate) {
                    var e, i, n, r = this.maxDate;
                    if (!this.dateLimit || this.maxDate && !this.startDate.clone().add(this.dateLimit).isAfter(this.maxDate) || (r = this.startDate.clone().add(this.dateLimit)), "left" == t) i = this.startDate.clone(), n = this.minDate;
                    else if ("right" == t) {
                        i = this.endDate.clone(), n = this.startDate;
                        var o = this.container.find(".calendar.right .calendar-time div");
                        if ("" != o.html() && (i.hour(o.find(".hourselect option:selected").val() || i.hour()), i.minute(o.find(".minuteselect option:selected").val() || i.minute()), i.second(o.find(".secondselect option:selected").val() || i.second()), !this.timePicker24Hour)) {
                            var s = o.find(".ampmselect option:selected").val();
                            "PM" === s && i.hour() < 12 && i.hour(i.hour() + 12), "AM" === s && 12 === i.hour() && i.hour(0)
                        }
                        i.isBefore(this.startDate) && (i = this.startDate.clone()), r && i.isAfter(r) && (i = r.clone())
                    }
                    e = '<select class="hourselect">';
                    for (var a = this.timePicker24Hour ? 0 : 1, l = this.timePicker24Hour ? 23 : 12, c = a; c <= l; c++) {
                        var h = c;
                        this.timePicker24Hour || (h = i.hour() >= 12 ? 12 == c ? 12 : c + 12 : 12 == c ? 0 : c);
                        var d = i.clone().hour(h),
                            u = !1;
                        n && d.minute(59).isBefore(n) && (u = !0), r && d.minute(0).isAfter(r) && (u = !0), h != i.hour() || u ? e += u ? '<option value="' + c + '" disabled="disabled" class="disabled">' + c + "</option>" : '<option value="' + c + '">' + c + "</option>" : e += '<option value="' + c + '" selected="selected">' + c + "</option>"
                    }
                    e += "</select> ", e += ': <select class="minuteselect">';
                    for (c = 0; c < 60; c += this.timePickerIncrement) {
                        var p = c < 10 ? "0" + c : c;
                        d = i.clone().minute(c), u = !1;
                        n && d.second(59).isBefore(n) && (u = !0), r && d.second(0).isAfter(r) && (u = !0), i.minute() != c || u ? e += u ? '<option value="' + c + '" disabled="disabled" class="disabled">' + p + "</option>" : '<option value="' + c + '">' + p + "</option>" : e += '<option value="' + c + '" selected="selected">' + p + "</option>"
                    }
                    if (e += "</select> ", this.timePickerSeconds) {
                        e += ': <select class="secondselect">';
                        for (c = 0; c < 60; c++) {
                            p = c < 10 ? "0" + c : c, d = i.clone().second(c), u = !1;
                            n && d.isBefore(n) && (u = !0), r && d.isAfter(r) && (u = !0), i.second() != c || u ? e += u ? '<option value="' + c + '" disabled="disabled" class="disabled">' + p + "</option>" : '<option value="' + c + '">' + p + "</option>" : e += '<option value="' + c + '" selected="selected">' + p + "</option>"
                        }
                        e += "</select> "
                    }
                    if (!this.timePicker24Hour) {
                        e += '<select class="ampmselect">';
                        var f = "",
                            m = "";
                        n && i.clone().hour(12).minute(0).second(0).isBefore(n) && (f = ' disabled="disabled" class="disabled"'), r && i.clone().hour(0).minute(0).second(0).isAfter(r) && (m = ' disabled="disabled" class="disabled"'), i.hour() >= 12 ? e += '<option value="AM"' + f + '>AM</option><option value="PM" selected="selected"' + m + ">PM</option>" : e += '<option value="AM" selected="selected"' + f + '>AM</option><option value="PM"' + m + ">PM</option>", e += "</select>"
                    }
                    this.container.find(".calendar." + t + " .calendar-time div").html(e)
                }
            },
            updateFormInputs: function() {
                this.container.find("input[name=daterangepicker_start]").is(":focus") || this.container.find("input[name=daterangepicker_end]").is(":focus") || (this.container.find("input[name=daterangepicker_start]").val(this.startDate.format(this.locale.format)), this.endDate && this.container.find("input[name=daterangepicker_end]").val(this.endDate.format(this.locale.format)), this.singleDatePicker || this.endDate && (this.startDate.isBefore(this.endDate) || this.startDate.isSame(this.endDate)) ? this.container.find("button.applyBtn").removeAttr("disabled") : this.container.find("button.applyBtn").attr("disabled", "disabled"))
            },
            move: function() {
                var t, i = {
                    top: 0,
                    left: 0
                },
                    n = e(window).width();
                this.parentEl.is("body") || (i = {
                    top: this.parentEl.offset().top - this.parentEl.scrollTop(),
                    left: this.parentEl.offset().left - this.parentEl.scrollLeft()
                }, n = this.parentEl[0].clientWidth + this.parentEl.offset().left), t = "up" == this.drops ? this.element.offset().top - this.container.outerHeight() - i.top : this.element.offset().top + this.element.outerHeight() - i.top, this.container["up" == this.drops ? "addClass" : "removeClass"]("dropup"), "left" == this.opens ? (this.container.css({
                    top: t,
                    right: n - this.element.offset().left - this.element.outerWidth(),
                    left: "auto"
                }), this.container.offset().left < 0 && this.container.css({
                    right: "auto",
                    left: 9
                })) : "center" == this.opens ? (this.container.css({
                    top: t,
                    left: this.element.offset().left - i.left + this.element.outerWidth() / 2 - this.container.outerWidth() / 2,
                    right: "auto"
                }), this.container.offset().left < 0 && this.container.css({
                    right: "auto",
                    left: 9
                })) : (this.container.css({
                    top: t,
                    left: this.element.offset().left - i.left,
                    right: "auto"
                }), this.container.offset().left + this.container.outerWidth() > e(window).width() && this.container.css({
                    left: "auto",
                    right: 0
                }))
            },
            show: function(t) {
                this.isShowing || (this._outsideClickProxy = e.proxy(function(t) {
                    this.outsideClick(t)
                }, this), e(document).on("mousedown.daterangepicker", this._outsideClickProxy).on("touchend.daterangepicker", this._outsideClickProxy).on("click.daterangepicker", "[data-toggle=dropdown]", this._outsideClickProxy).on("focusin.daterangepicker", this._outsideClickProxy), e(window).on("resize.daterangepicker", e.proxy(function(t) {
                    this.move(t)
                }, this)), this.oldStartDate = this.startDate.clone(), this.oldEndDate = this.endDate.clone(), this.previousRightTime = this.endDate.clone(), this.updateView(), this.container.show(), this.move(), this.element.trigger("show.daterangepicker", this), this.isShowing = !0)
            },
            hide: function(t) {
                this.isShowing && (this.endDate || (this.startDate = this.oldStartDate.clone(), this.endDate = this.oldEndDate.clone()), this.startDate.isSame(this.oldStartDate) && this.endDate.isSame(this.oldEndDate) || this.callback(this.startDate, this.endDate, this.chosenLabel), this.updateElement(), e(document).off(".daterangepicker"), e(window).off(".daterangepicker"), this.container.hide(), this.element.trigger("hide.daterangepicker", this), this.isShowing = !1)
            },
            toggle: function(t) {
                this.isShowing ? this.hide() : this.show()
            },
            outsideClick: function(t) {
                var i = e(t.target);
                "focusin" == t.type || i.closest(this.element).length || i.closest(this.container).length || i.closest(".calendar-table").length || (this.hide(), this.element.trigger("outsideClick.daterangepicker", this))
            },
            showCalendars: function() {
                this.container.addClass("show-calendar"), this.move(), this.element.trigger("showCalendar.daterangepicker", this)
            },
            hideCalendars: function() {
                this.container.removeClass("show-calendar"), this.element.trigger("hideCalendar.daterangepicker", this)
            },
            hoverRange: function(t) {
                if (!this.container.find("input[name=daterangepicker_start]").is(":focus") && !this.container.find("input[name=daterangepicker_end]").is(":focus")) {
                    var e = t.target.getAttribute("data-range-key");
                    if (e == this.locale.customRangeLabel) this.updateView();
                    else {
                        var i = this.ranges[e];
                        this.container.find("input[name=daterangepicker_start]").val(i[0].format(this.locale.format)), this.container.find("input[name=daterangepicker_end]").val(i[1].format(this.locale.format))
                    }
                }
            },
            clickRange: function(t) {
                var e = t.target.getAttribute("data-range-key");
                if (this.chosenLabel = e, e == this.locale.customRangeLabel) this.showCalendars();
                else {
                    var i = this.ranges[e];
                    this.startDate = i[0], this.endDate = i[1], this.timePicker || (this.startDate.startOf("day"), this.endDate.endOf("day")), this.alwaysShowCalendars || this.hideCalendars(), this.clickApply()
                }
            },
            clickPrev: function(t) {
                e(t.target).parents(".calendar").hasClass("left") ? (this.leftCalendar.month.subtract(1, "month"), this.linkedCalendars && this.rightCalendar.month.subtract(1, "month")) : this.rightCalendar.month.subtract(1, "month"), this.updateCalendars()
            },
            clickNext: function(t) {
                e(t.target).parents(".calendar").hasClass("left") ? this.leftCalendar.month.add(1, "month") : (this.rightCalendar.month.add(1, "month"), this.linkedCalendars && this.leftCalendar.month.add(1, "month")), this.updateCalendars()
            },
            hoverDate: function(t) {
                if (e(t.target).hasClass("available")) {
                    var i = e(t.target).attr("data-title"),
                        n = i.substr(1, 1),
                        r = i.substr(3, 1),
                        o = e(t.target).parents(".calendar").hasClass("left") ? this.leftCalendar.calendar[n][r] : this.rightCalendar.calendar[n][r];
                    this.endDate && !this.container.find("input[name=daterangepicker_start]").is(":focus") ? this.container.find("input[name=daterangepicker_start]").val(o.format(this.locale.format)) : this.endDate || this.container.find("input[name=daterangepicker_end]").is(":focus") || this.container.find("input[name=daterangepicker_end]").val(o.format(this.locale.format));
                    var s = this.leftCalendar,
                        a = this.rightCalendar,
                        l = this.startDate;
                    this.endDate || this.container.find(".calendar tbody td").each(function(t, i) {
                        if (!e(i).hasClass("week")) {
                            var n = e(i).attr("data-title"),
                                r = n.substr(1, 1),
                                c = n.substr(3, 1),
                                h = e(i).parents(".calendar").hasClass("left") ? s.calendar[r][c] : a.calendar[r][c];
                            h.isAfter(l) && h.isBefore(o) || h.isSame(o, "day") ? e(i).addClass("in-range") : e(i).removeClass("in-range")
                        }
                    })
                }
            },
            clickDate: function(t) {
                if (e(t.target).hasClass("available")) {
                    var i = e(t.target).attr("data-title"),
                        n = i.substr(1, 1),
                        r = i.substr(3, 1),
                        o = e(t.target).parents(".calendar").hasClass("left") ? this.leftCalendar.calendar[n][r] : this.rightCalendar.calendar[n][r];
                    if (this.endDate || o.isBefore(this.startDate, "day")) {
                        if (this.timePicker) {
                            var s = parseInt(this.container.find(".left .hourselect").val(), 10);
                            if (!this.timePicker24Hour) "PM" === (c = this.container.find(".left .ampmselect").val()) && s < 12 && (s += 12), "AM" === c && 12 === s && (s = 0);
                            var a = parseInt(this.container.find(".left .minuteselect").val(), 10),
                                l = this.timePickerSeconds ? parseInt(this.container.find(".left .secondselect").val(), 10) : 0;
                            o = o.clone().hour(s).minute(a).second(l)
                        }
                        this.endDate = null, this.setStartDate(o.clone())
                    } else if (!this.endDate && o.isBefore(this.startDate)) this.setEndDate(this.startDate.clone());
                    else {
                        if (this.timePicker) {
                            var c;
                            s = parseInt(this.container.find(".right .hourselect").val(), 10);
                            if (!this.timePicker24Hour) "PM" === (c = this.container.find(".right .ampmselect").val()) && s < 12 && (s += 12), "AM" === c && 12 === s && (s = 0);
                            a = parseInt(this.container.find(".right .minuteselect").val(), 10), l = this.timePickerSeconds ? parseInt(this.container.find(".right .secondselect").val(), 10) : 0;
                            o = o.clone().hour(s).minute(a).second(l)
                        }
                        this.setEndDate(o.clone()), this.autoApply && (this.calculateChosenLabel(), this.clickApply())
                    }
                    this.singleDatePicker && (this.setEndDate(this.startDate), this.timePicker || this.clickApply()), this.updateView(), t.stopPropagation()
                }
            },
            calculateChosenLabel: function() {
                var t = !0,
                    e = 0;
                for (var i in this.ranges) {
                    if (this.timePicker) {
                        var n = this.timePickerSeconds ? "YYYY-MM-DD hh:mm:ss" : "YYYY-MM-DD hh:mm";
                        if (this.startDate.format(n) == this.ranges[i][0].format(n) && this.endDate.format(n) == this.ranges[i][1].format(n)) {
                            t = !1, this.chosenLabel = this.container.find(".ranges li:eq(" + e + ")").addClass("active").html();
                            break
                        }
                    } else if (this.startDate.format("YYYY-MM-DD") == this.ranges[i][0].format("YYYY-MM-DD") && this.endDate.format("YYYY-MM-DD") == this.ranges[i][1].format("YYYY-MM-DD")) {
                        t = !1, this.chosenLabel = this.container.find(".ranges li:eq(" + e + ")").addClass("active").html();
                        break
                    }
                    e++
                }
                t && (this.showCustomRangeLabel ? this.chosenLabel = this.container.find(".ranges li:last").addClass("active").html() : this.chosenLabel = null, this.showCalendars())
            },
            clickApply: function(t) {
                this.hide(), this.element.trigger("apply.daterangepicker", this)
            },
            clickCancel: function(t) {
                this.startDate = this.oldStartDate, this.endDate = this.oldEndDate, this.hide(), this.element.trigger("cancel.daterangepicker", this)
            },
            monthOrYearChanged: function(t) {
                var i = e(t.target).closest(".calendar").hasClass("left"),
                    n = i ? "left" : "right",
                    r = this.container.find(".calendar." + n),
                    o = parseInt(r.find(".monthselect").val(), 10),
                    s = r.find(".yearselect").val();
                i || (s < this.startDate.year() || s == this.startDate.year() && o < this.startDate.month()) && (o = this.startDate.month(), s = this.startDate.year()), this.minDate && (s < this.minDate.year() || s == this.minDate.year() && o < this.minDate.month()) && (o = this.minDate.month(), s = this.minDate.year()), this.maxDate && (s > this.maxDate.year() || s == this.maxDate.year() && o > this.maxDate.month()) && (o = this.maxDate.month(), s = this.maxDate.year()), i ? (this.leftCalendar.month.month(o).year(s), this.linkedCalendars && (this.rightCalendar.month = this.leftCalendar.month.clone().add(1, "month"))) : (this.rightCalendar.month.month(o).year(s), this.linkedCalendars && (this.leftCalendar.month = this.rightCalendar.month.clone().subtract(1, "month"))), this.updateCalendars()
            },
            timeChanged: function(t) {
                var i = e(t.target).closest(".calendar"),
                    n = i.hasClass("left"),
                    r = parseInt(i.find(".hourselect").val(), 10),
                    o = parseInt(i.find(".minuteselect").val(), 10),
                    s = this.timePickerSeconds ? parseInt(i.find(".secondselect").val(), 10) : 0;
                if (!this.timePicker24Hour) {
                    var a = i.find(".ampmselect").val();
                    "PM" === a && r < 12 && (r += 12), "AM" === a && 12 === r && (r = 0)
                }
                if (n) {
                    var l = this.startDate.clone();
                    l.hour(r), l.minute(o), l.second(s), this.setStartDate(l), this.singleDatePicker ? this.endDate = this.startDate.clone() : this.endDate && this.endDate.format("YYYY-MM-DD") == l.format("YYYY-MM-DD") && this.endDate.isBefore(l) && this.setEndDate(l.clone())
                } else if (this.endDate) {
                    var c = this.endDate.clone();
                    c.hour(r), c.minute(o), c.second(s), this.setEndDate(c)
                }
                this.updateCalendars(), this.updateFormInputs(), this.renderTimePicker("left"), this.renderTimePicker("right")
            },
            formInputsChanged: function(i) {
                var n = e(i.target).closest(".calendar").hasClass("right"),
                    r = t(this.container.find('input[name="daterangepicker_start"]').val(), this.locale.format),
                    o = t(this.container.find('input[name="daterangepicker_end"]').val(), this.locale.format);
                r.isValid() && o.isValid() && (n && o.isBefore(r) && (r = o.clone()), this.setStartDate(r), this.setEndDate(o), n ? this.container.find('input[name="daterangepicker_start"]').val(this.startDate.format(this.locale.format)) : this.container.find('input[name="daterangepicker_end"]').val(this.endDate.format(this.locale.format))), this.updateView()
            },
            formInputsFocused: function(t) {
                this.container.find('input[name="daterangepicker_start"], input[name="daterangepicker_end"]').removeClass("active"), e(t.target).addClass("active"), e(t.target).closest(".calendar").hasClass("right") && (this.endDate = null, this.setStartDate(this.startDate.clone()), this.updateView())
            },
            formInputsBlurred: function(e) {
                if (!this.endDate) {
                    var i = this.container.find('input[name="daterangepicker_end"]').val(),
                        n = t(i, this.locale.format);
                    n.isValid() && (this.setEndDate(n), this.updateView())
                }
            },
            formInputsKeydown: function(t) {
                13 === t.keyCode && (t.preventDefault(), this.formInputsChanged(t))
            },
            elementChanged: function() {
                if (this.element.is("input") && this.element.val().length) {
                    var e = this.element.val().split(this.locale.separator),
                        i = null,
                        n = null;
                    2 === e.length && (i = t(e[0], this.locale.format), n = t(e[1], this.locale.format)), (this.singleDatePicker || null === i || null === n) && (n = i = t(this.element.val(), this.locale.format)), i.isValid() && n.isValid() && (this.setStartDate(i), this.setEndDate(n), this.updateView())
                }
            },
            keydown: function(t) {
                9 !== t.keyCode && 13 !== t.keyCode || this.hide(), 27 === t.keyCode && (t.preventDefault(), t.stopPropagation(), this.hide())
            },
            updateElement: function() {
                this.element.is("input") && !this.singleDatePicker && this.autoUpdateInput ? (this.element.val(this.startDate.format(this.locale.format) + this.locale.separator + this.endDate.format(this.locale.format)), this.element.trigger("change")) : this.element.is("input") && this.autoUpdateInput && (this.element.val(this.startDate.format(this.locale.format)), this.element.trigger("change"))
            },
            remove: function() {
                this.container.remove(), this.element.off(".daterangepicker"), this.element.removeData()
            }
        }, e.fn.daterangepicker = function(t, n) {
            var r = e.extend(!0, {}, e.fn.daterangepicker.defaultOptions, t);
            return this.each(function() {
                var t = e(this);
                t.data("daterangepicker") && t.data("daterangepicker").remove(), t.data("daterangepicker", new i(t, r, n))
            }), this
        }, i
    })

