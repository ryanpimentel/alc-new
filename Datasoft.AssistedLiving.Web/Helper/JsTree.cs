﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace DataSoft.Helpers {
    public class JsTree {
        private List<JsTreeNode> nodes = new List<JsTreeNode>();

        public List<JsTreeNode> Nodes {
            get {
                return nodes;
            }
        }
    }

    public enum NodeLevel {
        Unspecified = 0,
        Root = 1,
        Parent = 2,
        Child = 3
    }

    [DataContract()]
    public class JsTreeNode {
        [DataMember(Name = "children")]
        public List<JsTreeNode> Children = null;

        [DataMember(Name = "attr")]
        public JsTreeNodeAttribute Attribute = null;

        [DataMember(Name = "data")]
        public JsTreeNodeData Data = null;

        [DataMember(Name = "state")]
        public string State = "closed";

        public bool Expanded {
            get {
                if (State == "open")
                    return true;
                else
                    return false;
            }
        }
    }

    [DataContract()]
    public class JsTreeNodeData {
        [DataMember(Name = "title")]
        public string Title = string.Empty;

        [DataMember(Name = "icon")]
        public string ImageUrl = null;
    }

    [DataContract()]
    public class JsTreeNodeAttribute {
        [DataMember(Name = "id")]
        public string ID = string.Empty;

        [DataMember(Name = "node-level")]
        public int NodeLevel = 0;

        [DataMember(Name = "title")]
        public string Title = string.Empty;

        [DataMember(Name = "class")]
        public string Class = string.Empty;
    }

}