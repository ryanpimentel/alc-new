﻿using System;
using System.Linq;
using System.Dynamic;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using Datasoft.AssistedLiving.Security;

namespace Datasoft.Ext {
    public static class Exts {

        public const string EncryptionKey = "DSLCALCSALTED2017CC";
        public static string GetVerifyCode(int length, Random random) {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }

        public static byte[] EncryptToByte(this string clearText) {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create()) {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream()) {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)) {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    return ms.ToArray();
                }
            }
        }

        public static byte[] DecryptToByte(this string cipherText) {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create()) {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream()) {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)) {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    return ms.ToArray();
                }
            }
        }

        public static string Encrypt(this string clearText) {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create()) {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream()) {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)) {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(this string cipherText) {
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create()) {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream()) {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)) {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static ExpandoObject ToExpando(this object anonymousObject) {
            IDictionary<string, object> anonymousDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(anonymousObject);
            IDictionary<string, object> expando = new ExpandoObject();
            foreach (var item in anonymousDictionary)
                expando.Add(item);
            return (ExpandoObject)expando;
        }
        public static DateTime? ToTimezonedDateTime(this DateTime? date) {
            if (date == null || (!date.HasValue)) return null;
            //assuming all dates passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var utcOffset = new DateTimeOffset(date.Value, TimeSpan.Zero);

            return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).DateTime;
        }

        public static DateTimeOffset ToTimezonedDateTime(this DateTime date) {
            if (date == null) return DateTimeOffset.MinValue;
            //assuming all dates passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var utcOffset = new DateTimeOffset(date, TimeSpan.Zero);

            return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).DateTime;
        }

        public static DateTime? TryParseNullableDate(this String date, bool toUTC = true) {
            DateTime d;
            if (DateTime.TryParse(date, out d)) {
                if (toUTC)
                    return d.ToUniversalTime();
                return d;
            }
            return null;
        }
        public static int? TryParseNullableInt(this String val) {
            int d;
            if (int.TryParse(val, out d))
                return d;
            return null;
        }
        public static double? TryParseNullableDouble(this String val) {
            double d;
            if (double.TryParse(val, out d))
                return d;
            return null;
        }
        public static decimal? TryParseNullableDecimal(this String val) {
            decimal d;
            if (decimal.TryParse(val, out d))
                return d;
            return null;
        }
        public static bool? TryParseNullableBool(this String val) {
            bool d;
            if (string.IsNullOrEmpty(val)) return null;
            if (val == "1")
                return true;
            else if (val == "0")
                return false;
            else if (bool.TryParse(val, out d))
                return d;
            return null;
        }
        public static byte? TryParseNullableByte(this String val) {
            byte d;
            if (byte.TryParse(val, out d))
                return d;
            return null;
        }

        public static string EmptyReturnNull(this String val) {
            if (string.IsNullOrEmpty(val)) return null;
            return val;
        }

        public static string AddSpaceWhenNotNull(this String val) {
            if (!string.IsNullOrEmpty(val)) return val + " ";
            return val;
        }

        public static string ToLongDaysFormat(this string val) {
            if (!string.IsNullOrEmpty(val)) {
                string[] arr = new string[] { "0", "1", "2", "3", "4", "5", "6" };
                string[] src = val.Split(",".ToArray());
                if (arr.All(src.Contains))
                    return "Daily";
                return val.Replace("0", "Sun").Replace("1", "Mon").Replace("2", "Tue").Replace("3", "Wed").Replace("4", "Thu").Replace("5", "Fri").Replace("6", "Sat");
            }
            return val;
        }
        public static string ToShortDaysFormat(this string val) {
            if (!string.IsNullOrEmpty(val)) {
                string[] arr = new string[] { "0","1","2","3","4","5","6" };
                string[] src = val.Split(",".ToArray());
                if (arr.All(src.Contains))
                    return "Daily";
                return val.Replace("0", "Su").Replace("1", "Mo").Replace("2", "Tu").Replace("3", "We").Replace("4", "Th").Replace("5", "Fr").Replace("6", "Sa");
            }
            return val;
        }
        
    }
}