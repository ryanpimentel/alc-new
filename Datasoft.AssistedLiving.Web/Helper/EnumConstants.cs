﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataSoft.Helpers {
    public class AdminConstants {
        public const string CARESTAFF = "carestaff";
        public const string SEARCH_MEDICINE = "search_medicine";
        public const string SEARCH_ADMISSION = "search_admission";
        public const string ROOM = "room";
        public const string DEPARTMENT = "department";
        public const string DEPARTMENT_USER = "department_user";
        public const string FACILITYCONFIG = "facilityconfig";
        public const string SUPPLY = "supply";
        public const string FACILITY = "facility";
        public const string RESIDENT = "resident";
        public const string RESIDENT_MOVEIN = "resident_movein";
        public const string CSR = "csr";
        public const string CSRMEDDETAIL = "csrmeddetail";
        public const string CSRHISTORY = "csrhistory";
        public const string DESTRUCTION = "destruction";
        public const string DESTRUCTIONHISTORY = "destructionhistory";
        public const string RECEIVEMEDS = "receivemeds";
        public const string RECEIVEMEDDETAIL = "receivemeddetail";
        public const string RECEIVEMEDSHISTORY = "receivemedshistory";
        public const string RECEIVEMEDSPRESCRIPTION = "receivemedsprescription";
        public const string SMART_ADD_RESIDENT = "smart_add_resident";
        public const string RESIDENT_ADMISSION = "resident_admission";
        public const string SEARCH_RESIDENT = "search_resident";
        public const string SEARCH_CARESTAFF = "search_carestaff";
        public const string CARESTAFF_ADMISSION = "carestaff_admission";
        public const string STAFF_RESIDENT_INFO = "staff_resident_info";
        public const string PREPLACEMENT_WIZARD = "preplacement_wizard";
        public const string ASSESSMENT_WIZARD = "assessment_wizard";
        public const string DASHBOARD_PREADMISSION_NAV = "dashboard_preadmission_nav";
        public const string DASHBOARD_PENDING_ADMISSION = "dashboard_pending_admission";
        public const string USERS = "user";
        public const string USERSV2 = "userv2";
        public const string ROLES = "role";
        public const string USER_ROLES = "user_role";
        public const string RESIDENT_MEDICATION = "resident_medication";
        public const string RESIDENTADMISSIONLIST = "resident_admission_list";
        public const string FACILITY_RESERVATION = "facility_reservation";
        public const string MEDICAL_ADMINISTRATION_SCHEDULE = "mar";
        public const string MEDICAL_ADMINISTRATION_SCHEDULE_INFO = "mar_info";
        public const string CARESTAFF_SCHEDULE = "carestaff_schedule";
        public const string ACTIVITY_SCHEDULE = "activity_schedule";
        public const string ACTIVITY_SCHEDULE_SVC = "activity_schedule_svc";
        public const string ACTIVITY = "activity";
        public const string SHIFT = "shift";
        public const string SHIFT_V2 = "shift_v2";
        public const string CARESTAFF_SHIFT = "carestaff_shift";
        public const string CARESTAFF_SHIFT_V2 = "carestaff_shift_v2";
        public const string PERMISSION = "permission";
        public const string PERMISSION_DETAIL = "permission_detail";
        public const string CARESTAFF_TASK_COMPLETION = "carestaff_taskcompletion";
        public const string POINTSSETTINGS = "pointssettings";
        public const string PREFERENCES = "preferences";
        public const string DOCUMENTS = "documents";
        public const string SEARCH_REFERENCE = "search_reference"; //added by Mariel - 2017-07-17
        public const string USER_FILES = "user_files";
        public const string ACTIVITY_SCHEDULE_BY_ID = "activity_schedule_by_id";//added by Mariel - 2017-10-23
        public const string SETTINGS = "settings";
        public const string PAYERS = "payers"; // added kim 08/19/22
        public const string ALLERGY = "allergy"; // added kim 08/24/22
        public const string PHYSICIAN = "physician"; // added kim 08/19/22
        public const string PHARMACY = "pharmacy";// add kim 11/25/22
        public const string INCIDENT_REPORT = "incident_report";
        public const string DISCIPLINARY_WRITEUPS = "disciplinary_writeups";
        public const string ASSESSMENT_WIZARD_LIST = "assessment_wizard_list";
        public const string PERSONAL_DOCUMENT = "personal_document";
        public const string PROOF_INSERVICE_TRAINING = "proof_inservice_training";
        public const string RESIDENT_ADMISSION_MARKETER = "resident_admission_marketer";
        public const string FINANCIALANDOTHERPERSON = "financial_and_other";
        public const string SUPPLY_V2 = "supply_v2";
        public const string SUPPLY_ALL = "supply_all";// added kim 03/16/22
        public const string GUEST_COMMENT = "guest_comment";
        public const string CHECK_MEDICINE = "check_medicine";
        public const string EMPLOYEE_FORM_REQUEST = "empform_request";
        public const string SEARCH_PAYER = "search_payer";
        public const string SEARCH_ALLERGY = "search_allergy";
        public const string SEARCH_PHYSICIAN = "search_physician"; //added kim 11/16/22
        public const string SEARCH_PHARMACY = "search_pharmacy"; //added kim 11/25/22
        public const string SEARCH_CITY = "search_city";
        public const string SEARCH_STATE = "search_state";
        public const string SEARCH_ZIP = "search_zip";
        public const string USER_ACTIVITY = "user_activity";
        public const string RENDERED_SERVICES_REPORT = "rendered_services_report";


        //public static dynamic CHECK_MEDICINE { get; internal set; }
    }

    public enum PostMode {
        Get = 0,
        Add = 1,
        Edit = 2,
        Delete = 3,
        Deactivate = 4,
        AdmitResident=5,
        Discharge=6,
        CancelDischarge=7,
        Reactivate=8
    }
    public enum PreplacementTab {
        Appraisal = 1,
        Emergency = 2,
        Physician_Report = 3
    }


    public enum AdmissionStatus {
        Admitted_As_Resident = 1,
        Referral_For_New_Lead = 2,
        Referral_For_Follow_Up = 3,
        Tour_For_Schedule = 4,
        Tour_For_Follow_Up = 5,
        Assessment_For_Completion = 6,
        Assessment_For_Follow_Up = 7,
        Move_In_For_Schedule = 8,
        Move_In_For_Follow_Up = 9
    }

    public enum CompletionType {
        Completed = 1,
        Rescheduled = 2,
        Reassigned = 3
    }
}