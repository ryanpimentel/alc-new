﻿using System;
using System.Collections.Generic;
using Datasoft.AssistedLiving.Security;

namespace DataSoft.Helpers {
    public static class Utility {
        public static string ToTimezonedDateTime(DateTime? date, string format) {
            if (date == null || (!date.HasValue)) return null;

            if (format == null) format = "MM/dd/yyyy hh:mm tt";
            //assuming all dates passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var utcOffset = new DateTimeOffset(date.Value.ToUniversalTime(), TimeSpan.Zero);

            return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).ToString(format);
        }

        public static string ToTimezonedDateTimeFriendlyTZ(DateTime date, string format) {
            if (format == null) format = "MM/dd/yyyy hh:mm tt";

            if (date == null) return null;
            //assuming all dates passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var utcOffset = new DateTimeOffset(date.ToUniversalTime(), TimeSpan.Zero);
            var utcDate = utcOffset.ToOffset(tz.GetUtcOffset(utcOffset));
            return utcDate.ToString(format) + " " + tz.DisplayName;
        }

        public static string ToTimezonedDateTime(DateTime date, string format) {
            if (format == null) format = "MM/dd/yyyy hh:mm tt";

            if (date == null) return null;
            //assuming all dates passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var utcOffset = new DateTimeOffset(date.ToUniversalTime(), TimeSpan.Zero);

            return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).ToString(format);
        }

        public static DateTime? ToTimezonedDateTime(DateTime? date) {
            if (date == null) return null;
            //assuming all dates passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var utcOffset = new DateTimeOffset(date.Value.ToUniversalTime(), TimeSpan.Zero);

            return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).DateTime;
        }

        public static string ToTimezonedDateTime(string dateString, string format) {
            if (dateString == null) return null;
            DateTime date;
            if (!DateTime.TryParse(dateString, out date))
                return null;

            if (format == null) format = "MM/dd/yyyy hh:mm tt";
            //assuming all dates passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var utcOffset = new DateTimeOffset(date.ToUniversalTime(), TimeSpan.Zero);

            return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).ToString(format);
        }

        public static string ToTimezonedDateTime(TimeSpan? time, string format) {
            if (format == null) format = "hh:mm tt";

            if (time == null) return null;
            //assuming all times passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var now = DateTime.Now;
            var dt = new DateTime(now.Year, now.Month, now.Day, time.Value.Hours, time.Value.Minutes, time.Value.Seconds);
            //dt.AddTicks(time.Value.Ticks);
            var utcOffset = new DateTimeOffset(dt.ToUniversalTime(), TimeSpan.Zero);

            return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).ToString(format);
        }

        public static TimeSpan? ToTimezonedDateTime(TimeSpan? time) {
            if (time == null) return null;
            //assuming all times passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var now = DateTime.Now;
            var dt = new DateTime(now.Year, now.Month, now.Day);
            dt = dt.AddTicks(time.Value.Ticks);
            var utcOffset = new DateTimeOffset(dt.ToUniversalTime(), TimeSpan.Zero);

            var utc = utcOffset.ToOffset(tz.GetUtcOffset(utcOffset));
            return new TimeSpan(utc.Ticks);
        }

        public static string ToTimezonedDateTime(TimeSpan time, string format) {
            if (format == null) format = "hh:mm tt";

            if (time == null) return null;
            //assuming all times passed are UTC's
            var tz = ALCUserContext.AgencyTimeZone;
            var now = DateTime.Now;
            var dt = new DateTime(now.Year, now.Month, now.Day);
            dt = dt.AddTicks(time.Ticks);
            var utcOffset = new DateTimeOffset(dt.ToUniversalTime(), TimeSpan.Zero);

            return utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).ToString(format);
        }

        public static bool IsTimeLapseFromCurrentTime(DateTime? startDateTime, DateTime? clientTime) {
            if(clientTime == null || startDateTime == null)  return false;

            var dt = new DateTime(clientTime.Value.Year, clientTime.Value.Month, clientTime.Value.Day);
            var tt = dt.Add(startDateTime.Value.TimeOfDay);
            
            var utd = ToTimezonedDateTime(DateTime.UtcNow);
            if (tt < utd.Value)
                return true;
            
            return false;
        }

        public static string GetDateFormattedString(DateTime? date, string format) {
            if (date == null) return null;
            try {
                return date.Value.ToString(format);
            } catch {
                
            }
            return null;
        }

        public static string GetDateFormattedString(TimeSpan? date, string format) {
            if (date == null) return null;
            try {
                DateTime? dt = DateTime.Today.Add(date.Value);
                return dt.Value.ToString(format);
            } catch {

            }
            return null;
        }

        public static string GetTimeFormattedString(TimeSpan? time, string format) {
            if (time == null) return null;
            try {
                return time.Value.ToString(format.Replace(":", "\\:"));
            } catch {

            }
            return null;
        }
        public static int? GetDeptIdByDeptHeadId(int deptHeadId) {
            Dictionary<int, int> deptHeadStaff = new Dictionary<int, int>();
            deptHeadStaff.Add(9, 3);//MEDTECH/MEDTECH_DEPTHEAD
            deptHeadStaff.Add(10, 4);//CAREGIVER/CAREGIVER_DEPTHEAD
            deptHeadStaff.Add(11, 5);//HOUSEKEEPER/HOUSEKEEPER_DEPTHEAD
            deptHeadStaff.Add(12, 6);//DIETARY/DIETARY_DEPTHEAD
            deptHeadStaff.Add(13, 7);//MAINTENANCE/MAINTENANCE_DEPTHEAD
            int val = 0;
            deptHeadStaff.TryGetValue(deptHeadId, out val);
            return val == 0 ? null : (int?)val;
        }
        

        public static int[] Departments {
            get { 
                return new int[] { 4, 5, 6, 7 };
            }
        }

        public static string ToFriendlyDays(string delimitedValue) {
            string ret = string.Empty;
            if (!string.IsNullOrEmpty(delimitedValue)) {
                ret = delimitedValue.Replace("0", "Su").Replace("1","Mo").Replace("2","Tu").Replace("3","We").Replace("4","Th").Replace("5","Fr").Replace("6","Sa");
            }
            return ret;
        }
    }
}