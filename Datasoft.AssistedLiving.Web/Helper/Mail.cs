﻿using System;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace Datasoft.AssistedLiving.Web.Helper {
    public class MailHelper {
        private const int Timeout = 180000;
        private readonly string _host;
        private readonly int _port = 0;
        private readonly string _user;
        private readonly string _pass;
        private readonly bool _ssl;
        private readonly string _from;
        private readonly string _to;

        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string RecipientCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string AttachmentFile { get; set; }

        public MailHelper() {
            //MailServer - Represents the SMTP Server
            _host = ConfigurationManager.AppSettings["MailServer"];
            //Port- Represents the port number
             int.TryParse(ConfigurationManager.AppSettings["Port"], out _port);
            //MailAuthUser and MailAuthPass - Used for Authentication for sending email
            _user = ConfigurationManager.AppSettings["MailAuthUser"];
            _pass = ConfigurationManager.AppSettings["MailAuthPass"];
            _ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
            _from = ConfigurationManager.AppSettings["EmailFromAddress"];
            _to = ConfigurationManager.AppSettings["EmailToAddressOverride"];
        }

        public void Send() {
            try {
                // We do not catch the error here... let it pass direct to the caller
                Attachment att = null;
                if (string.IsNullOrEmpty(Sender))
                    Sender = _from;
                if (string.IsNullOrEmpty(Recipient))
                    Recipient = _to;
                var message = new MailMessage(Sender, Recipient, Subject, Body) { IsBodyHtml = true };
                if (RecipientCC != null) {
                    message.Bcc.Add(RecipientCC);
                }
                var smtp = new SmtpClient();
                smtp.UseDefaultCredentials = true;
                
                
                if(!string.IsNullOrEmpty(_host) && _port > 0)
                    smtp = new SmtpClient(_host, _port);
                else
                    smtp.Host = _host;

                if (!String.IsNullOrEmpty(AttachmentFile)) {
                    if (File.Exists(AttachmentFile)) {
                        att = new Attachment(AttachmentFile);
                        message.Attachments.Add(att);
                    }
                }

                if (_user.Length > 0 && _pass.Length > 0) {
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(_user, _pass);
                    smtp.EnableSsl = _ssl;
                }

                smtp.Send(message);

                if (att != null)
                    att.Dispose();
                message.Dispose();
                smtp.Dispose();
            } catch (Exception ex) {

            }
        }
    }
}