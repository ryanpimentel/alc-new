﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace DataSoft.Helpers
{
    public class JsonDataContractResult : JsonResult {
        public override void ExecuteResult(ControllerContext context) {

            this.MaxJsonLength = 50000000;

            if (context == null) {
                throw new ArgumentNullException("context");
            }
            try {
                //if datacontract object do below
                if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                       String.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase)) {
                    throw new InvalidOperationException("Get is not allowed");
                }

                HttpResponseBase response = context.HttpContext.Response;

                if (!String.IsNullOrEmpty(ContentType)) {
                    response.ContentType = ContentType;
                } else {
                    response.ContentType = "application/json";
                }
                if (ContentEncoding != null) {
                    response.ContentEncoding = ContentEncoding;
                }
                if (Data != null) {
                    // Use the DataContractJsonSerializer instead of the JavaScriptSerializer 
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(Data.GetType());
                    serializer.WriteObject(response.OutputStream, Data);
                }

            } catch {
                //do normal object
                base.ExecuteResult(context);
            }
        }
    }
}