﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace DataSoft.Helpers {
    [DataContract]
    public class JsonResponse {
        public JsonResponse(int responseResult, string message, string retrn = "0") {
            this.Result = responseResult;
            this.Message = message;
            this.Return = retrn;
        }

        public static JsonResponse GetDefault() {
            return new JsonResponse(JsonResponseResult.Success, "Ok");
        }
        [DataMember(Name = "result")]
        public int Result = 0;
        [DataMember(Name = "message")]
        public string Message = "";
        [DataMember(Name = "return")]
        public string Return = "";

    }
    
    public class JsonResponseResult {
        public const int Error = -1;
        public const int Success = 0;
        public const int Exists = -2;
        public const int InUse = -3;
        public const int Failed = -4;
    }
}