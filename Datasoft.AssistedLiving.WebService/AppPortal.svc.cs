﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Datasoft.AssistedLiving.WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AppPortal" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AppPortal.svc or AppPortal.svc.cs at the Solution Explorer and start debugging.
    public class AppPortal : IAppPortal
    {
        public UserLogin Authorize(string AgencyId, string UserId, string Password)
        {
            using (var ctx = Helper.ASLClientBaseContext(AgencyId))
            {
                var user = (from u in ctx.ALCUsers.AsEnumerable()
                            join ur in ctx.ALCUserRoles.AsEnumerable() on u.user_id equals ur.user_id
                            join r in ctx.ALCRoles.AsEnumerable() on ur.role_id equals r.role_id
                            where u.user_id == UserId && u.password == Password
                            select new
                            {
                                name = u.first_name + " " + (!string.IsNullOrEmpty(u.middle_initial) ? u.middle_initial + ". " : "") + u.last_name,
                                role = r.description,
                                u.user_id,
                                u.password,
                                ur.role_id,
                                u.is_active
                            }).SingleOrDefault();
                return new UserLogin()
                {
                    AgencyId = AgencyId,
                    UserId = UserId,
                    Name = user.name,
                    Role = user.role,
                    RoleId = user.role_id.ToString(),
                };
            }

        }
        public StaffShiftTime GetUserSST(string AgencyId, string UserId)
        {
            using (var ALS = Helper.ASLClientBaseContext(AgencyId))
            {
                var res = (from x in ALS.ALCUsers
                           join y in ALS.CarestaffShifts on x.user_id equals y.carestaff_id into y1
                           from ys in y1.DefaultIfEmpty()
                           join z in ALS.Shifts on ys.shift_id equals z.shift_id into z1
                           from zs in z1.DefaultIfEmpty()
                           where x.user_id == UserId
                           select new
                           {
                               start_time = zs.start_time,
                               end_time = zs.end_time
                           }).SingleOrDefault();
                return new StaffShiftTime()
                {
                    StartTime = res.start_time.ToString(),
                    EndTime = res.end_time.ToString()
                };
            }
        }
        public List<StaffDailyTask> GetUserSDT(string AgencyId, string UserId, string CurrentDate)
        {
            using (var ALS = Helper.ASLClientBaseContext(AgencyId))
            {
                DateTime? curDate;
                DateTime d;
                if (DateTime.TryParse(CurrentDate, out d)) curDate = d;
                else curDate = null;

                var cs = (from c in ALS.getActivityScheduleByUser(UserId, curDate, null)
                          select new StaffDailyTask()
                          {
                              ActivityDesc = c.activity_desc,
                              ActivityId = c.activity_id.ToString(),
                              ActivityProc = c.activity_proc,
                              ActivityScheduleId = c.activity_schedule_id.ToString(),
                              ActivityStatus = c.activity_status.ToString(),
                              OActivityStatus = c.oactivity_status.ToString(),
                              Department = c.department,
                              CarestaffActivityId = c.carestaff_activity_id.ToString(),
                              CompletionDate = c.completion_date.ToString(),
                              Remarks = c.remarks,
                              CarestaffName = c.carestaff_name,
                              Resident = c.resident,
                              Room = c.room,
                              ServiceDuration = c.service_duration.ToString(),
                              AcknowledgeBy = c.acknowledged_by,
                              StartTime = c.start_time.ToString(),
                              XRef = c.xref.ToString()
                          }).ToList();

                return cs;
            }
        }
    }
}
