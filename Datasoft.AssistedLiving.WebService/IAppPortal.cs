﻿using Datasoft.AssistedLiving.WebService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Datasoft.AssistedLiving.WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAppPortal" in both code and config file together.
    [ServiceContract]
    public interface IAppPortal
    {

        [OperationContract]
        UserLogin Authorize(string AgencyId, string UserId, string Password);


        [OperationContract]
        StaffShiftTime GetUserSST(string AgencyId, string UserId);

        [OperationContract]
        List<StaffDailyTask> GetUserSDT(string AgencyId, string UserId, string CurrentDate);
    }

    [DataContract]
    public class StaffShiftTime
    {
        [DataMember]
        public string StartTime { get; set; }

        [DataMember]
        public string EndTime { get; set; }

    }

    [DataContract]
    public class StaffDailyTask
    {
        [DataMember]
        public string ActivityDesc { get; set; }

        [DataMember]
        public string ActivityId { get; set; }

        [DataMember]
        public string ActivityProc { get; set; }

        [DataMember]
        public string ActivityScheduleId { get; set; }

        [DataMember]
        public string ActivityStatus { get; set; }

        [DataMember]
        public string OActivityStatus { get; set; }

        [DataMember]
        public string Department { get; set; }

        [DataMember]
        public string CarestaffActivityId { get; set; }

        [DataMember]
        public string CompletionDate { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string CarestaffName { get; set; }

        [DataMember]
        public string Resident { get; set; }

        [DataMember]
        public string Room { get; set; }

        [DataMember]
        public string ServiceDuration { get; set; }

        [DataMember]
        public string AcknowledgeBy { get; set; }

        [DataMember]
        public string StartTime { get; set; }

        [DataMember]
        public string XRef { get; set; }

    }

    [DataContract]
    public class UserLogin
    {
        [DataMember]
        public string AgencyId { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Role { get; set; }

        [DataMember]
        public string RoleId { get; set; }
    }

    public class Helper
    {
        public static ASLClientDataContext ASLClientBaseContext(string AgencyId)
        {
            string connection = "";

            using (var cx = new ALSMasterDataContext())
            {
                var agency = cx.AgencyLists.SingleOrDefault(x => x.agency_id == AgencyId);
                connection = agency.conn_string;
            }
            return new ASLClientDataContext(connection);
        }
    }
}
